<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Website dalam masa perbaikan</title>

<script type="text/javascript">
<?php
    define('BASEPATH','');
    include('cms/config/site.php');
?>
var sts = '<?php echo $config['sts_web']; ?>';
var BaseUrl = 'http://<?php echo $_SERVER['HTTP_HOST']; ?>';
if(sts == 3)
{
    window.location.assign(BaseUrl);
}
</script>
<script type="text/javascript" src="./err_page/JamAnalog/css/sylvester.js"></script>
<script type="text/javascript" src="./err_page/JamAnalog/css/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="./err_page/JamAnalog/css/cssSandpaper.js"></script>	
<script type="text/javascript" src="./err_page/JamAnalog/css/clock.js"></script>

<link href="http://fonts.googleapis.com/css?family=Ubuntu&subset=latin" rel="stylesheet" type="text/css">
<style type="text/css">
*{font-family: 'Ubuntu', sans-serif;margin:0;padding:0}
#clock{position:relative;width:600px;height:600px;margin:20px auto 0 auto;background:url(./err_page/JamAnalog/images/clockface.jpg);list-style:none}
#sec,#min,#hour{position:absolute;width:30px;height:600px;top:0px;left:285px}
#sec{background:url(./err_page/JamAnalog/images/sechand.gif);z-index:3}
#min{background:url(./err_page/JamAnalog/images/minhand.gif);z-index:2}
#hour{background:url(./err_page/JamAnalog/images/hourhand.gif);z-index:1}
#hook-just-in-case{background:#1E90FF !important; padding:10px; text-align:center !important;color:#E6E6FA !important;font:bold 16px Ubuntu, Georgia, Serif !important;position:relative;zoom:1;z-index:10000 !important;line-height:20px;}
a{color:#ccf}
a:hover{text-decoration:underline}
#hook-just-in-case p{padding:0 10px;line-height:1.7em}
</style>

</head>

<body>
    <div id="hook-just-in-case">
	Mohon maaf, Website kami saat ini dalam masa perbaikan.<br />
	Silahkan cek beberapa saat lagi, Terima kasih.
    </div>    

    <ul id="clock">	
	<li id="sec" style="-webkit-transform: rotate(18deg);"></li>

	<li id="hour" style="-webkit-transform: rotate(491deg);"></li>
	<li id="min" style="-webkit-transform: rotate(132deg);"></li>
    </ul>

</body>
</html>

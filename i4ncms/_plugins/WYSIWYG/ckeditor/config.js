/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) 
{
	// Define changes to default configuration here. For example:
	//config.language = 'id';
	//config.uiColor = '#BDEBF7';
	// config.autoGrow_maxHeight = 400;

	/* Max Karakter or Kata */
	//config.extraPlugins = 'wordcount';

	/*Set Global
	 config.wordcount = {
	    // Whether or not you want to show the Word Count
	    showWordCount: true,
	
	    // Whether or not you want to show the Char Count
	    showCharCount: false,
	
	     // Whether or not to include Html chars in the Char Count
	     countHTML: false,
	    
	    // Option to limit the characters in the Editor
	    charLimit: 'unlimited',
	  
	    // Option to limit the words in the Editor
	    wordLimit: 'unlimited'
	};
	
	config.extraPlugins: 'magicline';
	config.magicline_color: 'blue';*/

	/* Config highlight Code */
	//config.extraPlugins = 'syntaxhighlight';
	config.protectedSource.push( /<\?[\s\S]*?\?>/g );   // PHP code
	config.protectedSource.push( /<%[\s\S]*?%>/g );   // ASP code
	config.protectedSource.push( /(]+>[\s|\S]*?<\/asp:[^\>]+>)|(]+\/>)/gi );   // ASP.Net code
	config.syntaxhighlight_lang = 'php';
	//config.syntaxhighlight_lang = 'applescript','actionscript3','as3','bash','shell','sh','coldfusion','cf','cpp','c','c#','c-sharp','csharp','css','delphi','pascal','pas','diff','patch','erl','erlang','groovy','haxe','hx','java','jfx','javafx','js','jscript','javascript','perl','Perl','pl','php','text','plain','powershell','ps','posh','py','python','ruby','rails','ror','rb','sass','scss','scala','sql','ts','typescript','vb','vbnet','xml','xhtml','xslt','html';

	config.syntaxhighlight_hideControls = true;

	/* Pake OpenSource File manager [ KCFINDER ] */
	config.filebrowserBrowseUrl = '/_plugins/Finder/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = '/_plugins/Finder/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = '/_plugins/Finder/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/_plugins/Finder/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/_plugins/Finder/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = '/_plugins/Finder/kcfinder/upload.php?type=flash';

	/* Pake OpenSource File manager [ CK-FINDER ]
	config.filebrowserBrowseUrl = '/_plugins/Finder/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = '/_plugins/Finder/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/_plugins/Finder/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = '/_plugins/Finder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/_plugins/Finder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/_plugins/Finder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'; */

};

SyntaxHighlighter.autoloader(
	'applescript /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushAppleScript.js',
	'actionscript3 as3 /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushAS3.js',
	'bash shell sh /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushBash.js',
	'coldfusion cf /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushColdFusion.js',
	'cpp c /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushCpp.js',
	'c# c-sharp csharp /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushCSharp.js',
	'css /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushCss.js',
	'delphi pascal pas /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushDelphi.js',
	'diff patch /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushDiff.js',
	'erl erlang /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushErlang.js',
	'groovy /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushGroovy.js',
	'haxe hx /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushHaxe.js',
	'java /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushJava.js',
	'jfx javafx /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushJavaFX.js',
	'js jscript javascript /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushJScript.js',
	'perl Perl pl /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushPerl.js',
	'php /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushPhp.js',
	'text plain /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushPlain.js',
	'powershell ps posh /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushPowerShell.js',
	'py python /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushPython.js',
	'ruby rails ror rb /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushRuby.js',
	'sass scss /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushSass.js',
	'scala /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushScala.js',
	'sql /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushSql.js',
	'ts typescript /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushTypeScript.js',
	'vb vbnet /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushVb.js',
	'xml xhtml xslt html /_plugins/WYSIWYG/SyntaxHighlighter/scripts/shBrushXml.js'
);
//SyntaxHighlighter.config.strings.expandSource = 'Toggle';
//SyntaxHighlighter.config.strings.noBrush = 'Kann keine Zielsprache finden für: ';
//SyntaxHighlighter.config.strings.brushNotHtmlScript = 'Zielsprache wurde nicht für html-script Option konfiguriert: ';
SyntaxHighlighter.all();

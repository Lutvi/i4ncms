
<?php echo ( ! $produk_page)?'':'<div class="pagination">'.$produk_page.'</div>'; ?>

<?php
// tes custom data dari main view
echo $tes;

// Error Handling-cek kalau kosong
if ( ! empty($content) )
{
    if ($tipe_produk === 'induk')
    {
	    if($content[0]->status === 'on')
		{
			echo '<div style="height:120pxpadding:10px 0"><div style="padding:10px;height:100px;;width:98%;background:#ADD8E6;border:1px solid #A52A2A">Banner Iklan Mahal (2jt/6bulan) atau 4 Produk2 promo disini.(580x120)</div></div>';
		}
		$jml_prod = count($content);
		$i=0;
        foreach ($content as $item)
        {
			$i++;
			if ($item->status === 'on')
			{

			if(current_lang(false) === 'id')
			$nama_prod = $item->nama_prod;
			else
			$nama_prod = $item->nama_prod_en;
?>
        <h3><?php echo $nama_prod; ?></h3>
            <?php
	            $pil_warna = $this->mproduk->getAllFrontWarnaProdukById($item->id_prod);
	            if ( !empty($pil_warna) ) {
	            ?>
	            <div style="height:42px;padding:3px;display:block;border:1px solid #BFC0C5;background:#E6E6FA;">
	            <?php
		            foreach ( $pil_warna as $warna )
		            {
		                if ( !empty($warna->kode_warna) ) {
		            ?>
		            <a style="width:40px;height:40px;margin-right:3px;float:left;border:1px solid #BFC0C5;" title="<?php echo $warna->id_prod; ?>" href="#warnaProduk-<?php echo $warna->id_prod; ?>">
					<div style="width:40px;height:40px;background:#<?php echo $warna->kode_warna; ?>"></div>
		            </a>
		            <?php
		                }
		            }
		    ?>
	            </div>
		    <?php
		        }
            ?>


            <?php
            $foto = $this->mproduk->getAllImgFrontById($item->id_prod_img);
				$j = 0;
                foreach($foto as $fp)
                {
					$j++;
            ?>
	                <a  href="#<?php echo underscore($nama_prod).'_'.$i.'_'.$j;?>" data-rel="popup" data-position-to="window" data-transition="flip">
	                <img class="popphoto" src="<?php echo base_url().'_produk/thumb/thumb_'.$fp->img_src; ?>" alt="<?php echo underscore($nama_prod); ?>" />
	                </a>

					<div id="<?php echo underscore($nama_prod).'_'.$i.'_'.$j;?>" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true"  style="left:-12px;">

						<h4 style="text-align:center"><?php echo $nama_prod; ?></h4>
						<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo base_url().'_produk/medium/medium_'.$fp->img_src; ?>" style="max-height:600px;" alt="<?php echo $nama_prod; ?>">
					</div>
            <?php
                }
            ?>

            <p class="box-deskipsi-detail">
            <?php 
                if ( !empty($item->harga_spesial) )
                {
					$diskon = (!empty($item->diskon))?$item->diskon*100:0;
					if(current_lang(false) == 'id')
					{
						echo '<div class="deskripsi-harganormal-detail"><strike>Rp '.format_harga_indo($item->harga_prod).'</strike></div>';
						echo '<span>Harga : Rp  '.number_format($item->harga_spesial, 0,'','.').'</span>';
						echo '<br />';
						echo '<span>Diskon : '.$diskon.'%</span>';
					}
					else {
						echo '<div class="deskripsi-harganormal-detail"><strike>IDR '.format_harga_indo($item->harga_prod).'</strike></div>';
						echo '<span>Price : IDR '.number_format($item->harga_spesial, 0,'','.').'</span>';
						echo '<br />';
						echo '<span>Save : '.$diskon.'%</span>';
					}
                }
                else {
					if(current_lang(false) === 'id')
					echo '<span>Harga : Rp '.format_harga_indo($item->harga_prod).'</span>';
					else
					echo '<span>Price : IDR '.format_harga_indo($item->harga_prod).'</span>';
                }
                
                if ( ! empty($item->anak_untuk_jenis) )
                {
                    echo '<br />';
                    echo ucwords($item->anak_untuk_jenis).' : '.ucwords($item->nama_jenis_anak);
                }
                
                if ( empty($item->stok) )
                {
                    echo '<br />';
                    echo '<span>'.$this->lang->line('stok').' : '.$item->stok.'</span>';
                    echo '<br />';
                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
                }
                ?>
			<div class="box-btn-prod-detail">
				<a href="#right-panel" class="ui-btn ui-btn-inline ui-icon-shop ui-btn-icon-left ui-btn ui-shadow ui-mini" data-urlx="<?php echo site_url('belanja/beli'); ?>" data-produkid="<?php echo bs_kode($item->id_prod); ?>"  data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($item->harga_spesial) )?$item->harga_spesial:$item->harga_prod); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></a>
            </div>
				<?php
                if(current_lang(false) === 'id')
                {
					if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi,50);
					else
					$deskripsi = $item->deskripsi;
                }
                else {
                	if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi_en,50);
					else
					$deskripsi = $item->deskripsi_en;
                }

                echo $deskripsi;
            ?>
            </p>
            <?php if($jml_prod > 1) : ?>
			<a class="ui-btn ui-btn-inline ui-icon-info ui-btn-icon-left" href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
			<hr />
			<?php endif; ?>
<?php
			}
			else {
				echo "<h3>Produk tidak ditemukan</h3>";
				echo "<p>Data produk tidak ditemukan.</p>";
			}
        }
    }
    else {
		if($content[0]['status'] === 'on')
		{
			echo '<div style="height:120pxpadding:10px 0"><div style="padding:10px;height:100px;;width:98%;background:#ADD8E6;border:1px solid #A52A2A">Banner Iklan Mahal (2jt/6bulan) atau 4 Produk2 promo disini.(580x120)</div></div>';
		}
?>

<?php
		$i=0;
		foreach($content as $row)
		{
	        $i++;
			if ( $row['status'] === 'on')
			{
				if (current_lang(false) === 'id')
				{
					$nama_prod = humanize($row['nama_prod']);
					//echo $row->ket_id;
				} else {
					$nama_prod = humanize($row['nama_prod_en']);
					//echo $row->ket_en;
				}
?>
		        <h3><?php echo $nama_prod; ?></h3>

	            <?php
		            $pil_warna = $this->mproduk->getAllFrontWarnaProdukById($row['id_prod']);
		            if ( ! empty($pil_warna)) {
	            ?>
	            <div style="height:42px;padding:2px;display:block;border:1px solid #BFC0C5;background:#E6E6FA;">
		            <?php     
		                foreach ( $pil_warna as $warna )
		                {
		                    if (!empty($warna->kode_warna)) {
		            ?>
		            <a style="width:40px;height:40px;margin-right:3px;float:left;border:1px solid #BFBFBF;" title="<?php echo $warna->id_prod; ?>" href="#warnaProduk-<?php echo $warna->id_prod; ?>">
					<div style="width:40px;height:40px;background:#<?php echo $warna->kode_warna; ?>"></div>
		            </a>
		            <?php
		                    }
		                }
		        ?>
	            </div>
		        <?php
		            }
		        ?>

	            <?php
	            $foto = $this->mproduk->getAllImgFrontById($row['id_prod_img']);
					$j = 0;
	                foreach($foto as $fp)
	                {
		                $j++;
		                if( $j == 1)
		                {
	            ?>
			                <a href="#<?php echo underscore($nama_prod).'_'.$i.'_'.$j;?>" data-rel="popup" data-position-to="window" data-transition="flip">
			                <img class="popphoto" src="<?php echo base_url().'_produk/thumb/thumb_'.$fp->img_src; ?>" alt="<?php echo underscore($nama_prod); ?>" />
			                </a>
	
			                <div id="<?php echo underscore($nama_prod).'_'.$i.'_'.$j;?>" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true" style="left:-12px;">
								<h4 style="text-align:center"><?php echo $nama_prod; ?></h4>
								<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo base_url().'_produk/medium/medium_'.$fp->img_src; ?>" style="max-height:600px;" alt="<?php echo $nama_prod; ?>">
							</div>
	            <?php
						}
	                }
	            ?>

	            <div class="box-pdetail-list">
	            <?php 
	                if ( !empty($row['harga_spesial']) )
	                {
						$diskon = (!empty($row['diskon']))?$row['diskon']*100:0;
	                    if(current_lang(false) === 'id')
	                    {
							echo '<span class="deskripsi-harganormal-list"><strike>Rp '.format_harga_indo($row['harga_prod']).'</strike></span>';
							echo '<br />';
							echo 'Harga : Rp  '.number_format($row['harga_spesial'], 0,'','.');
							echo '<br />';
							echo 'Diskon : '.$diskon.'%';
	                    }
	                    else{
								echo '<span class="deskripsi-harganormal-list"><strike>IDR '.format_harga_indo($row['harga_prod']).'</strike></span>';
							echo '<br />';
							echo 'Price : IDR '.number_format($row['harga_spesial'], 0,'','.').'';
							echo '<br />';
							echo 'Save : '.$diskon.'%';
	                    }
	                }
	                else {
	                    if(current_lang(false) === 'id')
	                    echo 'Harga : Rp  '.format_harga_indo($row['harga_prod']);
	                    else
	                    echo 'Price : IDR '.format_harga_indo($row['harga_prod']).'';
	                }
	                
	                if ( !empty($row['anak_untuk_jenis']) )
	                {
	                    echo '<br />';
	                    echo ucwords($row['anak_untuk_jenis']).' : '.ucwords($row['nama_jenis_anak']);
	                }
	                
	                if ( empty($row['stok']) )
	                {
	                    echo '<br />';
	                    echo '<span>'.$this->lang->line('stok'). ': '.$row['stok'].'</span>';
	                    echo '<br />';
	                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
	                }
	            ?>
	            <div style="margin-bottom:20px">
	                    <a href="#right-panel" class="ui-btn ui-btn-inline ui-icon-shop ui-btn-icon-left ui-btn ui-shadow ui-mini" data-urlx="<?php echo site_url('belanja/beli'); ?>" data-produkid="<?php echo bs_kode($row['id_prod']); ?>" data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($row['harga_spesial']) )?$row['harga_spesial']:$row['harga_prod']); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></a>
	            </div>
	            <?php
	                if(current_lang(false) === 'id')
	                $desc = strip_tags($row['deskripsi'],"<p><a><br><strong><span><font><ul><li><ol>");
	                else
	                $desc = strip_tags($row['deskripsi_en'],"<p><a><br><strong><span><font><ul><li><ol>");
	                echo word_limiter($desc,20);
	            ?>
	            <br />
	            <a class="ui-btn ui-btn-inline ui-icon-info ui-btn-icon-left" href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
	            <hr />

                
<?php
			}
			else {
				echo "<h3>Produk tidak ditemukan</h3>";
				echo "<p>Data produk tidak ditemukan.</p>";
			}
		}
    }
	echo ( ! $produk_page)?'':'<div class="pagination">'.$produk_page.'</div>';
}
else {
?>
    <h3>Produk tidak ditemukan</h3>
    <p>Data produk tidak ditemukan.</p>
<?php
}
?>

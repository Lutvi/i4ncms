
<?php
// tes custom data dari main view
echo $tes;

if ( ! empty($video) )
{
?>
	<h3><?php echo $title; ?></h3>
	<p align="justify">
	<?php echo $content; ?>
	</p>
	
	<?php
	if ($per_page == 1)
	{
	?>
	    <div class="wrap-efek" style="margin:0 auto;">
	<?php
	    $jml = 1;
	    foreach ($video as $rvideo)
	    {
	    $cnt_video = $this->cms->getFrontCountVideoPlaylist($rvideo->id);
	?>
	
	<div id="z-video">
			<?php
				if($rvideo->tipe_video === 'youtube')
				$vurl = youtube_fullvideo($rvideo->src_video);
				else
				$vurl = vimeo_fullvideo($rvideo->src_video);
			?>
			<a rel="external" target="_blank" href="<?php echo $vurl;?>"><img style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo '/_media/videos/medium/'.$rvideo->gambar_video;?>" /></a>
	</div>

	        <div style="text-align:left">
	            <?php
	            if (current_lang(false) === 'id')
	            echo $rvideo->ket_id;
				else
				echo $rvideo->ket_en;
	            ?>
	
	            <?php if($cnt_video > 0): ?>
	            <h3 class="ui-bar ui-bar-a ui-corner-all">Playlist (<?php echo $cnt_video; ?>)</h3>
	            <?php endif; ?>
	        </div>
	    <?php
			if($cnt_video > 0)
			{
				$jml = 1;
				$playlist = $this->cms->getAllFrontVideoPlaylist($rvideo->id,$cnt_video,0);
				foreach ($playlist as $row)
				{

					if($row->tipe_video === 'youtube')
					$url = youtube_fullvideo($row->src_video);
					else
					$url = vimeo_fullvideo($row->src_video);
		
					// ket
					if (current_lang(false) === 'id')
					{
						$jdl = humanize($row->nama_id);
						$ket = $row->ket_id;
					} else {
						$jdl = humanize($row->nama_en);
						$ket = $row->ket_en;
					}
		?>
					<a rel="external" target="_blank" href="<?php echo $url;?>">
					<div class="ui-bar ui-bar-a">
						<div style="text-align:left;font-size:24px;color:#1E90FF"><?php echo $jdl;?></div>
						<img style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top" width="200" align="left" src="<?php echo '/_media/videos/small/small_'.$row->gambar_video;?>" />
						<div style="text-align:left;font-weight:normal;font-size:12px;"><?php echo $ket; ?></div>
					</div>
					</a>
					<br />
			<?php
					$jml++;
				}
			}
			$jml++;
	    }
	    ?>
		<?php
			echo "<div style='clear:left;text-align:center;margin-top:20px'>";
			if($rvideo->diskusi === 'on')
		    echo $this->disqus->get_html()."</div>";
		?>
	    </div>
	<?php
	}
	else
	{
		echo ( ! $video_page)?'':'<div class="pagination">'.$video_page.'</div>';
	?>

	<ul data-role="listview" data-inset="true">
	<?php
	    $jml = 1;
	    foreach ($video as $row){
	    $cnt_playlist = $this->cms->getFrontCountVideoPlaylist($row->id);
	            if (current_lang(false) === 'id')
				{
					$nama = humanize($row->nama_id);
					$ket = word_limiter(strip_tags($row->ket_id), 20);
				} else {
					$nama = humanize($row->nama_en);
					$ket = word_limiter(strip_tags($row->ket_en), 20);
				}
	?>
		<li>
			<a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
				<img style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo '/_media/videos/small/small_'.$row->gambar_video;?>" />

				<h2><?php echo $nama; ?></h2>
	            <p>
					<?php echo $ket; ?>
	            </p>
	            <p>
					<?php if($cnt_playlist > 0): ?>
					Playlist (<?php echo $cnt_playlist; ?>)
					<?php endif; ?>
	            </p>
            </a>
		</li>
	    <?php
	    $jml++;
	    }
	    ?>
	</ul>
	
	<?php
		echo ( ! $video_page)?'':'<div class="pagination">'.$video_page.'</div>';
	}
}
else {
    echo "<h3>Video Kosong</h3> Video Kosong, data tidak ditemukan";
}
?>

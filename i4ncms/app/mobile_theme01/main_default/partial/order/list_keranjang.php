
<div id="pemesanan-box">
	<h3><?php echo $title; ?></h3>
	
	<?php if(isset($info) && !empty($info)): ?>
	<div id="info-bayar-box">
	<?php foreach($info as $key => $isi): ?>
	<div  class="info"><?php echo $isi; ?></div>
	<?php endforeach; ?>
	</div>
	<?php endif; ?>
	
	<?php
	if($this->cart->total_items() > 0):
	$attributes = array('id' => 'updateBelanja');
	echo form_open('pemesanan/update', $attributes); ?>
	<fieldset class="ui-widget ui-widget-content ui-corner-all">
	<legend class="ui-widget ui-widget-header ui-corner-all">Data Keranjang</legend>
		<table class="list-keranjang" cellpadding="4" cellspacing="5" style="width:100%; font-size:12px" border="0">
		
		<tr>
		  <th>QTY</th>
		  <th>Item</th>
		  <th style="text-align:right">Harga</th>
		  <th style="text-align:right">Sub-Total</th>
		</tr>
		
		<?php $p = 1; ?>
		
		<?php foreach ($this->cart->contents() as $items): ?>
		
		<?php echo form_hidden($p.'[rowid]', $items['rowid']); ?>
		
		<tr>
		  <td><?php echo form_input(array('name' => $p.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'class' => 'qty','class' => 'qty-prod','style' => 'width:40px','autocomplete' => 'off')); ?></td>
		  <td>
				<img src="<?php echo base_url().'_produk/thumb/small_thumb_'.$this->mproduk->getImgByProdId($items['id']); ?>" align="right" />
				<?php echo $items['name']; ?>
		
				<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
		
					<p>
						<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
		
							<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
		
						<?php endforeach; ?>
					</p>
		
				<?php endif; ?>
		
		  </td>
		  <td style="text-align:right"><?php echo number_format($items['price'], 0,'','.'); ?></td>
		  <td style="text-align:right"><?php echo number_format($items['subtotal'], 0,'','.'); ?></td>
		</tr>
		
		<?php $p++; ?>
		
		<?php endforeach; ?>
		
		<tr>
		<td colspan="2"> </td>
		<td style="text-align:right;font-weight:bold"><strong>Total</strong></td>
		<td style="text-align:right;font-weight:bold">Rp.<?php echo number_format($this->cart->total(), 0,'','.'); ?></td>
		</tr>
		
		</table>
		
		<div>
			<button class="ui-state-default" id="updateKeranjang" disabled="disabled"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-cart"></span>Update Keranjang</button>
			<button class="ui-state-default" id="prosesPesan" type="button" data-url="<?php echo site_url('belanja/proses_pesan'); ?>"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-suitcase"></span>Proses Pemesanan</button>
		</div>
	</fieldset>
	<?php echo form_close(); ?>
	
	<?php else: ?>
		<p>Data Belanja Anda kosong, Proses Transaksi tidak bisa dilanjutkan.</p>
	<?php endif; ?>
</div> 

<script type="text/javascript">

$(function() {
    //pesan
    $('#prosesPesan').click(function(e){
        e.preventDefault();        
        $('#pemesanan-box').load($(this).attr('data-url'));
    });
   
	$( "#updateKeranjang, #updateKeranjang:hover" ).css({ opacity:'0.7' });
    $('.qty-prod').focus(function(){
		$('#updateKeranjang').removeAttr('disabled');
		$('#updateKeranjang').css({ opacity:'1' });
    });

});

</script>

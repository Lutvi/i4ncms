
<div id="pemesanan-box">
<h3>Proses Transaksi Selesai</h3>
    <p>
    Proses transaksi Anda telah berhasil, kami akan segera menghubungi Anda melalui Email dan SMS/Telpon untuk mengkonfirmasi pesanan Anda.
    <br />
    Mohon cek Email Anda <span style="font-weight:bold"><em><?php echo $this->session->userdata('email_beli'); ?></em></span> untuk detail transaksi dan status proses pembayaran Anda.
    </p>

    <p class="info">
		Apabila data Anda tidak valid atau tidak dapat dihubungi, maka kami akan akan membatalkan transaksi Anda secara sepihak.
    </p>

    <p>
    Terima kasih telah berbelanja di Toko kami, semoga hari Anda menyenangkan.
    </p>

    <h5>Data Transaksi Anda :</h5>
	<?php print_r($detail_order); ?>
	<hr />
	<?php print_r($total_order); ?>
	<hr />
	<?php print_r($transaksi); ?>
	<hr />
	<?php print_r($alamat_kirim); ?>
</div>


<?php
// tes custom data dari main view
echo $tes;

if ( ! empty($gambar_album) )
{
?>
<h3><?php echo $title; ?></h3>
<p align="justify">
<?php echo $content; ?>
</p>
<?php
	if ($per_page == 1)
	{
	?>
    <div class="wrap-efek" style="margin:0 auto; text-align:center;">
	<?php
	    $album = 1;
	    foreach ($gambar_album as $ralbum)
	    {
	    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($ralbum->id);
	?>
		<a href="#album_<?php echo $album;?>" data-rel="popup" data-position-to="window" data-transition="flip">
	        <img style="margin: 10px; padding: 8px; border: 1px solid #BBB; vertical-align:top;" alt="<?php echo $ralbum->alt_text_id;?>" src="<?php echo '/_media/album/small/small_'.$ralbum->gambar_cover;?>" />
		</a>

		<div id="album_<?php echo $album;?>" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true"  style="left:-12px;">
			<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo '/_media/album/medium/medium_'.$ralbum->gambar_cover;?>" style="max-height:600px;" alt="<?php echo humanize($ralbum->nama_id); ?>">
		</div>

        <div style="text-align:left;margin-bottom:20px;border:1px solid transparent">
            <?php
            if (current_lang(false) === 'id')
            echo $ralbum->ket_id;
			else
			echo $ralbum->ket_en;
            ?>

            <?php if($cnt_gallery > 0): ?>
            <h3 class="ui-bar ui-bar-a ui-corner-all">Gallery (<?php echo $cnt_gallery; ?>)</h3>
            <?php endif; ?>
        </div>

	    <?php
			if($cnt_gallery > 0)
			{
				$gallery = 1;
				$gambar_gallery = $this->cms->getAllFrontGalleryAlbum($ralbum->id,$cnt_gallery,0);
				foreach ($gambar_gallery as $row){
				?>
					<a href="#gallery_<?php echo $gallery;?>" data-rel="popup" data-position-to="window" data-transition="flip"><img width="190" style="margin: 8px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $row->alt_text_id;?>" src="<?php echo '/_media/album-gallery/small/small_'.$row->gambar_gallery;?>" /></a>
				<?php
				$gallery++;
				}
				?>

				<?php
				$gallery = 1;
				foreach ($gambar_gallery as $row){
				?>
					<div id="gallery_<?php echo $gallery;?>" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true"  style="left:-12px;">
						<?php
						if (current_lang(false) == 'id')
						{
							$nm_gal = humanize($row->nama_id);
							//echo $row->ket_id;
						} else {
							$nm_gal = humanize($row->nama_en);
							//echo $row->ket_en;
						}
						?>
						<h3><?php echo $nm_gal; ?></h3>
						<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo '/_media/album-gallery/large/large_'.$row->gambar_gallery;?>" style="max-height:600px;" alt="<?php echo $nm_gal; ?>">
					</div>
				<?php
				$gallery++;
				}
			}
		?>
        
		<?php
		$album++;
	    }
		?>
		<?php
			echo "<div style='clear:left;text-align:center;margin-top:20px'>";
			if($ralbum->diskusi === 'on')
		    echo $this->disqus->get_html()."</div>";
		?>
    </div>
	<?php
	}
	else
	{
	
	echo ( ! $album_page)?'':'<nav class="navigation-bar fixed-top"><div class="pagination">'.$album_page.'</div></nav>';
	?>

<ul data-role="listview" data-inset="true">
<?php
    $album = 1;
    foreach ($gambar_album as $row){
    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($row->id);

    if (current_lang(false) === 'id')
	{
		$nama = $row->nama_id;
		$ket = word_limiter($row->ket_id, 20);
	} else {
		$nama = $row->nama_en;
		$ket = word_limiter($row->ket_en, 20);
	}
?>
    <li>
    <a href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>"><img  style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo '/_media/album/medium/medium_'.$row->gambar_cover;?>" />
    <h2><?php echo humanize($nama); ?></h2>
    <p><?php echo strip_tags($ket); ?></p>
	<p>
		<?php if($cnt_gallery > 0): ?>
		Gallery (<?php echo $cnt_gallery; ?>)
		<?php endif; ?>
	</p>
	</a>
    </li>
<?php
}
?>
</ul>

	<?php
		echo ( ! $album_page)?'':'<div class="pagination">'.$album_page.'</div>';
	}
}
else {
    echo "<h3>Album Kosong</h3> Album Kosong, data tidak ditemukan";
}
?>

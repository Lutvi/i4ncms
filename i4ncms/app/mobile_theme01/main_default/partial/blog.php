
<?php
if ( ! empty($konten_blog) )
{
?>
	<h3><?php echo $title; ?></h3>
	<p align="justify">
	<?php echo $content; ?>
	</p>
	<?php
	if ($per_page == 1)
	{
	    foreach ($konten_blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = $row->isi_id;
			} else {
				$judul = humanize($row->judul_en);
				$isi = $row->isi_en;
			}
	?>
		<a href="#cblog" data-rel="popup" data-position-to="window" data-transition="flip"><img class="popphoto efek" width="300" style="margin: 12px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $judul;?>" src="<?php echo '/_media/blog/small/small_'.$row->gambar_cover;?>" /></a>
		<?php echo $isi; ?>

		<div id="cblog" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true"  style="left:-12px;">
			<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo '/_media/blog/medium/medium_'.$row->gambar_cover;?>" style="max-height:600px;" alt="<?php echo $judul; ?>">
		</div>

	<?php
	if($row->embed_id_album > 0)
	{
		if (current_lang(false) === 'id')
			echo '<h2 class="ui-bar ui-bar-a ui-corner-all">Album</h2><br />';
		else
			echo '<h2 class="ui-bar ui-bar-a ui-corner-all">Albums</h2><br />';

		$gambar_album = $this->cms->getAllAlbumFrontById($row->embed_id_album);
	    $album = 1;
	    foreach ($gambar_album as $ralbum){
	    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($ralbum->id);
	    if (current_lang(false) === 'id')
			$nama_album = humanize($ralbum->nama_id);
		else
			$nama_album = humanize($ralbum->nama_en);
	?>
	        <a href="#cover-<?php echo $album;?>" data-rel="popup" data-position-to="window" data-transition="flip">
		        <img class="popphoto" width="80" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $nama_album;?>" src="<?php echo '/_media/album/small/small_'.$ralbum->gambar_cover;?>" />
			</a>

			<div id="cover-<?php echo $album;?>" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true"  style="left:-12px;">
				<h3 style="padding:0;margin:0;"><?php echo $nama_album; ?></h3>
				<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo '/_media/album/medium/medium_'.$ralbum->gambar_cover;?>" style="max-height:600px;" alt="<?php echo humanize($nama_album); ?>">
			</div>
	    <?php
	    $album++;
			if($cnt_gallery > 0)
			{
				$gallery = $album;
				$gambar_gallery = $this->cms->getAllFrontGalleryAlbum($ralbum->id,$cnt_gallery,0);
				foreach ($gambar_gallery as $rgal){
					if (current_lang(false) === 'id')
						$nama_gallery = humanize($rgal->nama_id);
					else
						$nama_gallery = humanize($rgal->nama_en);
				?>

					<a href="#tampil-<?php echo $gallery;?>" data-rel="popup" data-position-to="window"  data-transition="flip"><img  class="popphoto" width="80" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $rgal->alt_text_id;?>" src="<?php echo '/_media/album-gallery/small/small_'.$rgal->gambar_gallery;?>" /></a>

					<div id="tampil-<?php echo $gallery;?>" data-role="popup" data-overlay-theme="b" data-theme="a" data-corners="true"  style="left:-12px;">
						<h3 style="padding:0;margin:0;"><?php echo $nama_gallery; ?></h3>
						<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">X</a><img class="popphoto" src="<?php echo '/_media/album-gallery/medium/medium_'.$rgal->gambar_gallery;?>" style="max-height:600px;" alt="<?php echo $nama_gallery; ?>">
					</div>
					
				<?php
				$gallery++;
				}
			}
	    }
	}
	?>
	<br /><br /><br />
	<?php
	if($row->embed_id_video > 0)
	{
		if (current_lang(false) === 'id')
			echo '<h2 class="ui-bar ui-bar-b ui-corner-all">Video</h2><br />';
		else
			echo '<h2 class="ui-bar ui-bar-b ui-corner-all">Videos</h2><br />';

	    $video = $this->cms->getAllVideoFrontById($row->embed_id_video);
	    $jml = 1;
	    foreach ($video as $rvideo)
	    {
		    $cnt_video = $this->cms->getFrontCountVideoPlaylist($rvideo->id);
		    
		    if($rvideo->tipe_video === 'youtube')
				$url_video = youtube_fullvideo($rvideo->src_video);
			else
				$url_video = vimeo_fullvideo($rvideo->src_video);
		
			// ket
			if (current_lang(false) === 'id')
			{
				$jdl_video = humanize($rvideo->nama_id);
				$ket_video = $rvideo->ket_id;
			} else {
				$jdl_video = humanize($rvideo->nama_en);
				$ket_video = $rvideo->ket_en;
			}
	?>

	<a rel="external" target="_blank" title="<?php echo $jdl_video; ?>" href="<?php echo $url_video;?>"><img width="100" class="efek" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo '/_media/videos/thumb/thumb_'.$rvideo->gambar_video;?>" /></a>

    <?php
			if($cnt_video > 0)
			{
				$jml = 1;
				$playlist = $this->cms->getAllFrontVideoPlaylist($rvideo->id,$cnt_video,0);
				foreach ($playlist as $rply){
	
				if($rply->tipe_video === 'youtube')
				$url = youtube_fullvideo($rply->src_video);
				else
				$url = vimeo_fullvideo($rply->src_video);
	
				// ket
				if (current_lang(false) === 'id')
				{
					$jdl = humanize($rply->nama_id);
					$ket = $rply->ket_id;
				} else {
					$jdl = humanize($rply->nama_en);
					$ket = $rply->ket_en;
				}
			?>
					<a rel="external" target="_blank" title="<?php echo $jdl; ?>" href="<?php echo $url;?>"><img width="100" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo '/_media/videos/thumb/thumb_'.$rply->gambar_video;?>" /></a>
				<?php
				$jml++;
				}
			}
		$jml++;
	    }
	}
	?>
	
	<?php
		echo "<div style='clear:left;text-align:center;margin-top:20px'>";
		if($row->diskusi === 'on')
	    echo $this->disqus->get_html()."</div>";
	?>
	<?php
		}
	}
	else
	{
	
	echo ( ! $blog_page)?'':'<div class="pagination">'.$blog_page.'</div>';
	?>
	
	<ul data-role="listview" data-inset="true">
	<?php
	    $blog = 1;
	    foreach ($konten_blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = word_limiter($row->isi_id, 15);
			}
			else {
				$judul = humanize($row->judul_en);
				$isi = word_limiter($row->isi_en, 15);
			}
	?>
		<li>
			<a href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
				<img style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $judul;?>" src="<?php echo '/_media/blog/thumb/thumb_'.$row->gambar_cover;?>" />
				<h2><?php echo $judul; ?></h2>
				<p><?php echo $isi; ?></p>
	        </a>
	     </li>
	    <?php
	    $blog++;
	    }
	    ?>
	</ul>
	    
	<?php
		echo ( ! $blog_page)?'':'<div class="pagination">'.$blog_page.'</div>';
	}
}
else {
    echo "<h3>Posting Kosong</h3> Posting Kosong, data tidak ditemukan";
}
?>

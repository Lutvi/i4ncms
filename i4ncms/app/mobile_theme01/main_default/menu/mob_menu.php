
<div class="ui-grid-d center" data-theme="b" style="text-align: center;">
    <div class="ui-block-a"><a href="<?php echo base_url(); ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-home ui-btn-icon-notext ui-btn-inline">Beranda</a></div>
    <div class="ui-block-b"><a href="<?php echo base_url(); ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-shop ui-btn-icon-notext ui-btn-inline">Toko</a></div>
    <div class="ui-block-c"><a href="<?php echo base_url(); ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-star ui-btn-icon-notext ui-btn-inline">Favorit</a></div>
    <div class="ui-block-d"><a href="<?php echo base_url(); ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-tag ui-btn-icon-notext ui-btn-inline">Kategori</a></div>
    <div class="ui-block-e"><a href="<?php echo base_url(); ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-gear ui-btn-icon-notext ui-btn-inline">Pengaturan</a></div>
</div>

<ul id="mnweb" data-role="listview" data-inset="true">
		<?php
		//print_r($menu);
		foreach($menu as $row): 
		$sub_menu = $this->cms->getParentMenu($parent=$row['id']);
		?>
	<!-- Level 1 -->
	<?php if( $row['link'] == 0 && $row['link'] !== 'home'): ?>
	<li data-icon="grid"><a style="font-weight:bold" href="#"><?php echo strtoupper($row['title']); ?></a></li>
	<?php endif; ?>
		<?php
		if(count($sub_menu)>0):
		echo '<ul>';
		foreach($sub_menu as $row2):
		$sub_sub_menu = $this->cms->getParentMenu($parent=$row2['id']);
		?>
		<!-- Level 2 -->
		<?php if($row2['link'] > 0): ?>
		<li><a style="font-weight:bold" href="<?php echo ($row2['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row2['link'])):'#'; ?>"><?php echo $row2['title']; ?></a>
		<?php endif; ?>
			<?php 
			if(count($sub_sub_menu)>0):
			echo '<ul>';
			foreach($sub_sub_menu as $row3):
			$sub_sub_sub_menu = $this->cms->getParentMenu($parent=$row3['id']);	
			?>
				<!-- Level 3 -->
				<?php if($row3['link'] > 0): ?>
				<li><a href="<?php echo ($row3['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row3['link'])):'#'; ?>"><?php echo $row3['title']; ?></a>
				<?php endif; ?>
					<?php 
					if(count($sub_sub_sub_menu)>0):
					echo '<ul>';
					foreach($sub_sub_sub_menu as $row4): 
					$sub_sub_sub_sub_menu = $this->cms->getParentMenu($parent=$row4['id']);
					?>
						<!-- Level 4 -->
						<?php if($row4['link'] > 0): ?>
						<li><a href="<?php echo ($row4['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row4['link'])):'#'; ?>"><?php echo $row4['title']; ?></a>
						<?php endif; ?>
					<?php endforeach; ?>
					</li></ul>
					<?php endif; ?>
			<?php endforeach; ?>
			</li></ul>
			<?php endif; ?>	
		<?php endforeach; ?>
		</li></ul>
		<?php endif; ?>	
	<?php endforeach; ?>
</ul>

<?php if( ! empty($kat_konten)): ?>
<div id="list-kat" style="margin-top:20px">
	<h4 class="ui-bar ui-bar-a ui-corner-all ui-icon-tag ui-btn-icon-right ui-shadow ui-corner-all">KATEGORI</h4>
	<div id="kat-konten" style="position:relative;margin-top:10px">
	<?php
	$attr = array('style' => 'font-size:14px;line-height:20px;');
	$j = 0;
	foreach ($kat_konten as $kat)
	{
		if (current_lang(false) === 'id')
		{
			$nama = $kat->label_id;
			$lbl = 'kategori/';
		}
		else {
			$nama = $kat->label_en;
			$lbl = 'category/';
		}

		$url_kat = base_url().current_lang().$lbl.$kat->tipe_kategori.'/'.format_txtkategori($kat->label_id);

		if($j > 0)
		$nama = ' - '.'<u>'.$nama.'</u>';
		else
		$nama = '<u>'.$nama.'</u>';

		if( $this->cms->cekKategoriKonten($kat->tipe_kategori,$kat->label_id) === TRUE)
		echo anchor($url_kat,$nama,$attr);
	$j++;
	}
	?>
	</div>
</div>
<?php endif; ?>

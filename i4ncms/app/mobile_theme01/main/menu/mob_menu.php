<!-- MENU -->
<div id="dl-menu" class="dl-menuwrapper">
	<button>Open Menu</button> 
	<ul class="dl-menu">
		<?php 
		foreach($menu as $row): 
		$sub_menu = $this->cms->getParentMenu($parent=$row['id']);
		?>
		<!-- Level 1 -->
		<?php if($row['link'] != 'home') ?>
		<li><a href="<?php echo ($row['link']=='home')?base_url():site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row['link'])); ?>"><?php echo $row['title']; ?></a>    
			<?php
			if(count($sub_menu)>0):
			echo '<ul class="dl-submenu"><li class="dl-back"><a href="#">Back</a>';
			foreach($sub_menu as $row2):
			$sub_sub_menu = $this->cms->getParentMenu($parent=$row2['id']);	
			?>
			<!-- Level 2 -->
			<li><a href="<?php echo (count($sub_sub_menu)>0)?'#':site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row2['link'])); ?>"><?php echo $row2['title']; ?></a>
				<?php 
				if(count($sub_sub_menu)>0):
				echo '<ul class="dl-submenu"><li class="dl-back"><a href="#">Back</a>';
				foreach($sub_sub_menu as $row3): 
				$sub_sub_sub_menu = $this->cms->getParentMenu($parent=$row3['id']);	
				?>
					<!-- Level 3 -->
					<li class="lv3"><a href="<?php echo (count($sub_sub_sub_menu)>0)?'#':site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row3['link'])); ?>"><?php echo $row3['title']; ?></a>
						<?php 
						if(count($sub_sub_sub_menu)>0):
						echo '<ul class="dl-submenu"><li class="dl-back"><a href="#">Back</a>';
						foreach($sub_sub_sub_menu as $row4): 
						$sub_sub_sub_sub_menu = $this->cms->getParentMenu($parent=$row4['id']);	
						?>
							<!-- Level 4 -->
							<li class="lv4"><a href="<?php echo (count($sub_sub_sub_sub_menu)>0)?'#':site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row4['link'])); ?>"><?php echo $row4['title']; ?></a>
						<?php endforeach; ?>
						</li></ul>
						<?php endif; ?>
				<?php endforeach; ?>
				</li></ul>
				<?php endif; ?>	
			</li>
			<?php endforeach; ?>
			</ul>
			<?php endif; ?>	
		</li>
		<?php endforeach; ?>
	</ul>
</div>
<!-- END MENU -->

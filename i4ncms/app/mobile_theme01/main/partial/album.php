<style type="text/css">
div#fancybox-wrap {margin-top:40px;}
</style>
<script>
$(document).ready(function() {
	$('.cover-album, .album')
	.fancybox({ centerOnScroll: true });
});

</script>
<?php
// tes custom data dari main view
//echo $tes;

if ( ! empty($gambar_album) )
{
?>
<h3><?php echo $title; ?></h3>
<p align="justify">
<?php echo $content; ?>
</p>
<?php
	if ($per_page == 1)
	{
	?>
    <div class="wrap-efek" style="margin:0 auto; text-align:center;">
	<?php
	    $album = 1;
	    foreach ($gambar_album as $ralbum){
	    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($ralbum->id);
	?>
		<a class="cover-album" href="<?php echo base_url().'/_media/album/large/large_'.$ralbum->gambar_cover;?>">
	        <img class="efek" width="660" style="margin: 10px; padding: 8px; border: 1px solid #BBB; vertical-align:top;" alt="<?php echo $ralbum->alt_text_id;?>" src="<?php echo base_url().'/_media/album/medium/medium_'.$ralbum->gambar_cover;?>" />
		</a>
		
	        <div style="text-align:left;margin-bottom:20px;border:1px solid transparent">
	            <?php
	            if (current_lang(false) === 'id')
	            echo $ralbum->ket_id;
				else
				echo $ralbum->ket_en;
	            ?>
	
	            <?php if($cnt_gallery > 0): ?>
	            <h4>Gallery (<?php echo $cnt_gallery; ?>)</h4>
	            <?php endif; ?>
	        </div>
	    <?php
			if($cnt_gallery > 0)
			{
				$gallery = 1;
				$gambar_gallery = $this->cms->getAllFrontGalleryAlbum($ralbum->id,$cnt_gallery,0);
				foreach ($gambar_gallery as $row){
				?>
					<a class="album" rel="album_group" href="<?php echo base_url().'/_media/album-gallery/large/large_'.$row->gambar_gallery;?>"><img class="efek" width="190" style="margin: 8px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $row->alt_text_id;?>" src="<?php echo base_url().'/_media/album-gallery/small/small_'.$row->gambar_gallery;?>" /></a>

					<div id="tampil-<?php echo $gallery;?>" style="display:none;">
						<?php
						if (current_lang(false) === 'id')
						{
							echo '<h3 style="padding:0;margin:0;">'.humanize($row->nama_id).'</h3>';
							echo $row->ket_id;
						} else {
							echo '<h3 style="padding:0;margin:0;">'.humanize($row->nama_en).'</h3>';
							echo $row->ket_en;
						}
						?>
					</div>
	
				<?php
				$gallery++;
				}
			}
		$album++;
	    }
		echo "<div style='clear:left'></div>";
		if($ralbum->diskusi === 'on')
	    echo $this->disqus->get_html();
	    ?>
    </div>
	<?php
	}
	else
	{
	
	echo ( ! $album_page)?'':'<nav class="navigation-bar fixed-top"><div class="pagination">'.$album_page.'</div></nav>';
	?>

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/css/elem/hover-box-style.css" />
<script type="text/javascript">
$(document).ready(function() {
	$('ul.img-thumbs li').hover(
		function(){$(".overlay", this).stop().animate({top:'0px'}, {queue:false,duration:300});}, 
		function(){$(".overlay", this).stop().animate({top:'195px'},{queue:false,duration:300});}
	);  
});
</script>
<style>
img.fotoAlbum {padding: 2px;vertical-align: top}
</style>

<ul class="img-thumbs">
<?php
    $album = 1;
    foreach ($gambar_album as $row){
    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($row->id);
?>
    <li>
    <a style="cursor:default;" href="javascript:void(0);"><img style="padding: 2px;vertical-align: top;" width="200" src="<?php echo base_url().'/_media/album/small/small_'.$row->gambar_cover;?>" /></a>
    <div class="overlay" style="width:280px;height:270px">
    <?php
	if (current_lang(false) === 'id')
	{
		$nama = $row->nama_id;
		echo '<h1>'.humanize($nama).'</h1>';
		echo word_limiter($row->ket_id, 60);
	} else {
		$nama = $row->nama_en;
		echo '<h1>'.humanize($nama).'</h1>';
		echo word_limiter($row->ket_en, 60);
	}
	?>
	<p>
	<?php if($cnt_gallery > 0): ?>
	<a href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>">Gallery (<?php echo $cnt_gallery; ?>)</a>
	<?php else: ?>
	<a href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>">Selengkapnya....</a>
	<?php endif; ?>
	</p>
    <a class="zoom" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>" rel="" title="<?php echo humanize($nama); ?>"></a>
    </div>
    </li>
<?php
}
?>
</ul>
	<?php
		echo ( ! $album_page)?'':'<div class="pagination">'.$album_page.'</div>';
	}
}
else {
    echo "<h3>Album Kosong</h3> Album Kosong, data tidak ditemukan";
}
?>

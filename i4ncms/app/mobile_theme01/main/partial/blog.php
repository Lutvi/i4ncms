<?php
// tes custom data dari main view
//echo $tes;

if(isset($snippet) && ! empty($snippet['js']) && ! empty($snippet['css'])) :
?>
		<script src="<?php echo base_url('_plugins/WYSIWYG/ckeditor/ckeditor.js'); ?>" type="text/javascript"></script>
<?php
	foreach($snippet['js'] as $item):
	if(file_exists(FCPATH.'_plugins/WYSIWYG/SyntaxHighlighter/scripts/'.$item)):
?>
		<script src="<?php echo base_url('_plugins/WYSIWYG/SyntaxHighlighter/scripts/'.$item); ?>" type="text/javascript"></script>
<?php
	endif;
	endforeach;

	foreach($snippet['css'] as $item):
	if(file_exists(FCPATH.'_plugins/WYSIWYG/SyntaxHighlighter/styles/'.$item)):
?>
		<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url('_plugins/WYSIWYG/SyntaxHighlighter/styles/'.$item); ?>"></link>
<?php
	endif;
	endforeach;
endif; 
?>
<style type="text/css">
div#fancybox-wrap {margin-top:40px;}
</style>
<script>
$(document).ready(function() {
	$('.cover-blog, .fancybox-blogalbum')
	.fancybox();

	$('.fancybox-blogmedia')
			.attr('rel', 'video_group')
			.fancybox({
			    width:680,
				height:400,
	            'hideOnOverlayClick':false,
	            'hideOnContentClick':false,
	            'showNavArrows'   : false,
			    'type': 'iframe',
			    iframe : {
			      preload: true
			    },
			    'onStart': function () { $('#fancybox-wrap').css({'margin-top':'50px !important'}); },
			    'onClosed': function () { $('#fancybox-wrap').css({'position':'fixed','top':'20px'}); }
			  });
});
</script>

<?php
if ( ! empty($konten_blog) )
{
?>
	<h3><?php echo $title; ?></h3>
	<p align="justify">
	<?php echo $content; ?>
	</p>
	<?php
	if ($per_page == 1)
	{
	?>
	    <div class="wrap-efek" style="margin:0 auto; text-align:left;">
	<?php
	    foreach ($konten_blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = $row->isi_id;
			} else {
				$judul = humanize($row->judul_en);
				$isi = $row->isi_en;
			}
	?>
		<a class="cover-blog" href="<?php echo base_url().'/_media/blog/medium/medium_'.$row->gambar_cover;?>"><img align="left" style="margin: 12px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $judul;?>" src="<?php echo base_url().'/_media/blog/small/small_'.$row->gambar_cover;?>" /></a>

		<?php echo $isi; ?>
	<?php
	if($row->embed_id_album > 0)
	{
	echo "<div style='clear:left'></div>";
	
		if (current_lang(false) === 'id')
			echo '<h2>Album</h2>';
		else
			echo '<h2>Albums</h2>';
	
		$gambar_album = $this->cms->getAllAlbumFrontById($row->embed_id_album);
	    $album = 1;
	    foreach ($gambar_album as $ralbum){
	    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($ralbum->id);
	    if (current_lang(false) === 'id')
			$nama_album = humanize($ralbum->nama_id);
		else
			$nama_album = humanize($ralbum->nama_en);
	?>
	        <a class="fancybox-blogalbum" rel="album_group" title="<?php echo $nama_album; ?>" href="<?php echo base_url().'/_media/album/medium/medium_'.$ralbum->gambar_cover;?>"><img class="efek" width="80" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $nama_album;?>" src="<?php echo base_url().'/_media/album/thumb/thumb_'.$ralbum->gambar_cover;?>" /></a>
	
	    <?php
			if($cnt_gallery > 0)
			{
				$gallery = 1;
				$gambar_gallery = $this->cms->getAllFrontGalleryAlbum($ralbum->id,$cnt_gallery,0);
				foreach ($gambar_gallery as $rgal){
					if (current_lang(false) === 'id')
						$nama_gallery = humanize($rgal->nama_id);
					else
						$nama_gallery = humanize($rgal->nama_en);
				?>
					<a class="fancybox-blogalbum" rel="album_group" title="<?php echo $nama_gallery; ?>" href="<?php echo base_url().'/_media/album-gallery/medium/medium_'.$rgal->gambar_gallery;?>"><img class="efek" width="80" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $nama_gallery;?>" src="<?php echo base_url().'/_media/album-gallery/thumb/thumb_'.$rgal->gambar_gallery;?>" /></a>
	
				<?php
				$gallery++;
				}
			}
		$album++;
	    }
	}
	?>
	
	<?php
	if($row->embed_id_video > 0)
	{
	echo "<div style='clear:left'></div>";
	
		if (current_lang(false) === 'id')
			echo '<h2>Video</h2>';
		else
			echo '<h2>Videos</h2>';
	
	    $video = $this->cms->getAllVideoFrontById($row->embed_id_video);
	    $jml = 1;
	    foreach ($video as $rvideo)
	    {
		    $cnt_video = $this->cms->getFrontCountVideoPlaylist($rvideo->id);
		    
		    if($rvideo->tipe_video === 'youtube')
				$url_video = youtube_fullvideo($rvideo->src_video);
			else
				$url_video = vimeo_fullvideo($rvideo->src_video);
		
			// ket
			if (current_lang(false) === 'id')
			{
				$jdl_video = humanize($rvideo->nama_id);
				$ket_video = $rvideo->ket_id;
			} else {
				$jdl_video = humanize($rvideo->nama_en);
				$ket_video = $rvideo->ket_en;
			}
	?>

	<a class="fancybox-blogmedia" rel="video_group" title="<?php echo $jdl_video; ?>" href="<?php echo $url_video;?>"><img width="100" class="efek" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo base_url().'/_media/videos/thumb/thumb_'.$rvideo->gambar_video;?>" /></a>

    <?php
			if($cnt_video > 0)
			{
				$jml = 1;
				$playlist = $this->cms->getAllFrontVideoPlaylist($rvideo->id,$cnt_video,0);
				foreach ($playlist as $rply){
	
				if($rply->tipe_video === 'youtube')
				$url = youtube_fullvideo($rply->src_video);
				else
				$url = vimeo_fullvideo($rply->src_video);
	
				// ket
				if (current_lang(false) === 'id')
				{
					$jdl = humanize($rply->nama_id);
					$ket = $rply->ket_id;
				} else {
					$jdl = humanize($rply->nama_en);
					$ket = $rply->ket_en;
				}
				
				?>
					<a class="fancybox-blogmedia" rel="video_group" title="<?php echo $jdl; ?>" href="<?php echo $url;?>"><img width="100" class="efek" style="margin: 3px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo base_url().'/_media/videos/thumb/thumb_'.$rply->gambar_video;?>" /></a>
	
				<?php
				$jml++;
				}
			}
		$jml++;
	    }
	}
	?>
	
	<?php
	
		echo "<div style='clear:left;text-align:center'>";
		if($row->diskusi === 'on')
	    echo $this->disqus->get_html()."</div>";
	?>
	    </div>
	
	    <?php if ($row->snippet === 'y') : ?>
		<script src="<?php echo base_url('_plugins/WYSIWYG/load_syntaxhighlighter.js'); ?>" type="text/javascript"></script>
		<? endif; ?>
	<?php
		}
	}
	else
	{
	
	echo ( ! $blog_page)?'':'<div class="pagination">'.$blog_page.'</div>';
	?>
	
	<div class="wrap-efek" style="margin:0 auto; text-align:left;">
	<?php
	    $blog = 1;
	    foreach ($konten_blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = word_limiter($row->isi_id, 80);
			}
			else {
				$judul = humanize($row->judul_en);
				$isi = word_limiter($row->isi_en, 80);
			}
	?>
	
	<style>
	.box-isi {
	   width: 100%;
	   position: relative;
	}
	.innerkontent {
	   padding:5px;
	   margin-bottom: 10px;
	   background: #E6E6FA;
	   border:1px solid #B6D9FB;
	   min-height: 150px;
	}

	.innerkontent:hover {
	   background: #E3F4E6;
	   border:1px solid #95EFD0;
	}
	</style>
		<div class="box-isi">
			<div class='innerkontent'>
			<h4 style="margin:3px 0;padding:2px 5px;border:1px dotted #F87900;color:#FFFFFF;background: #FFA500;"><?php echo $judul; ?></h4>
			<a href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
			<img class="efek" style="margin: 2px; padding: 3px; border: 1px solid #E6E6FA; vertical-align: top;" align="left" alt="<?php echo $judul;?>" src="<?php echo base_url().'/_media/blog/thumb/thumb_'.$row->gambar_cover;?>" />
			</a>
			<div>
				<?php echo $isi; ?>
	            <br />
				<a href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">Selengkapnya....</a>
	        </div>
	        </div>
	     </div>
	    <?php
	    $blog++;
	    }
	    ?>
	    </div>
	    
	<?php
		echo ( ! $blog_page)?'':'<div class="pagination">'.$blog_page.'</div>';
	}
}
else {
    echo "<h3>Posting Kosong</h3> Posting Kosong, data tidak ditemukan";
}
?>

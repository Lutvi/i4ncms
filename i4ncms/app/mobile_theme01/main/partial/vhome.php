<style>
pre, code { display:none; }
strike {color:red}
</style>

<?php
// tes custom data dari main view
//echo $tes;

// Error Handling-cek kalau kosong
if( count($produk) > 0 && $this->config->item('toko_aktif'))
{
		$jml_prod = count($produk);
        foreach ($produk as $item)
        {
?>
        <h3><?php echo (current_lang(false) === 'id' || empty($item->nama_prod_en))?$item->nama_prod:$item->nama_prod_en; ?></h3>
        <div class="box-prod-detail ui-helper-clearfix">
        
        <div class="box-img-prod-detail"> 
            <?php
            $pil_warna = $this->mproduk->getAllFrontWarnaProdukById($item->id_prod);
            if ( !empty($pil_warna) ) {
            ?>
            <div class="box-warna-prod-detail tips">
            <?php
            foreach ( $pil_warna as $warna )
            {
                if ( !empty($warna->kode_warna) ) {
            ?>
            <a class="pil-warna-detail" style="background:#<?php echo $warna->kode_warna; ?>" title="<?php echo $warna->id_prod; ?>" href="#warnaProduk-<?php echo $warna->id_prod; ?>"></a>
            <?php
                }
            }
            ?>
            </div>
            <?php
            }

            $foto = $this->mproduk->getAllImgFrontById($item->id_prod_img);
            if(current_lang(false) === 'id')
			$nama_prod = $item->nama_prod;
			else
			$nama_prod = $item->nama_prod_en;
            ?>
            <div class="multizoom<?php echo $item->id_prod; ?> thumbs"> 
                <a class="fancy" href="<?php echo base_url().'_produk/medium/medium_'.$foto[0]->img_src; ?>">
                <img src="<?php echo base_url().'_produk/thumb/thumb_'.$foto[0]->img_src; ?>" alt="<?php echo $foto[0]->alt_text_img; ?>" />
                </a>
            </div>

            <div class="box-btn-prod-detail">
				<button class="beli beli-detail" data-url="<?php echo site_url('belanja/beli'); ?>" data-produkid="<?php echo bs_kode($item->id_prod); ?>"  data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($item->harga_spesial) )?$item->harga_spesial:$item->harga_prod); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></button>
            </div>
        </div>
            <div class="clr"></div>
            <p class="box-deskipsi-detail">
            <?php 
                if ( !empty($item->harga_spesial) )
                {
					$diskon = (!empty($item->diskon))?$item->diskon*100:0;
					if(current_lang(false) == 'id')
					{
						echo '<div class="deskripsi-harganormal-detail"><strike>Rp '.number_format($item->harga_prod, 0,'','.').'</strike></div>';
						echo '<span>Harga : Rp  '.number_format($item->harga_spesial, 0,'','.').'</span>';
						echo '<br />';
						echo '<span>Diskon : '.$diskon.'%</span>';
					}
					else {
						echo '<div class="deskripsi-harganormal-detail"><strike>IDR '.number_format($item->harga_prod, 0,'','.').'</strike></div>';
						echo '<span>Price : IDR '.number_format($item->harga_spesial, 0,'','.').'</span>';
						echo '<br />';
						echo '<span>Save : '.$diskon.'%</span>';
					}
					
					
                }
                else {
					if(current_lang(false) === 'id')
					echo '<span>Harga : Rp '.number_format($item->harga_prod, 0,'','.').'</span>';
					else
					echo '<span>Price : IDR '.number_format($item->harga_prod, 0,'','.').'</span>';
                }
                
                if ( ! empty($item->nama_jenis_anak) )
                {
                    echo '<br />';
                    echo ucwords($item->anak_untuk_jenis).' : '.ucwords($item->nama_jenis_anak);
                }
                
                if ( empty($item->stok) )
                {
                    echo '<br />';
                    echo '<span>'.$this->lang->line('stok').' : '.$item->stok.'</span>';
                    echo '<br />';
                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
                }

                if(current_lang(false) === 'id')
                {
					if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi,50);
					else
					$deskripsi = $item->deskripsi;
                }
                else {
                	if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi_en,50);
					else
					$deskripsi = $item->deskripsi_en;
                }

                echo '<div class="clr"></div>'.$deskripsi;
            ?>
            </p>
            <?php if($jml_prod > 1) : ?>
			<a href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
			<?php endif; ?>
        </div>
<?php
        }
	?>
	<?php if($jml_produk > $jml_page): ?>
	<div align="center">
		<a class="tag-pill" href="<?php echo site_url(current_lang().'pdetail'); ?>">Total <?php echo ($jml_produk).' '.$this->lang->line('lbl_produk'); ?>.</a>
	</div>
	<?php endif; ?>
	<hr>
<?php
}
?>


<?php
if( count($blog) > 0)
{
?>
	<div class="wrap-efek" style="margin:0 auto; text-align:left;">
	<?php
	    foreach ($blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = word_limiter($row->isi_id, 80);
			}
			else {
				$judul = humanize($row->judul_en);
				$isi = word_limiter($row->isi_en, 80);
			}
	?>

	<style>
	.box-isi {
	   width: 100%;
	   position: relative;
	}
	.innerkontent {
	   padding:5px;
	   margin-bottom: 10px;
	   background: #E6E6FA;
	   border:1px solid #B6D9FB;
	   min-height: 150px;
	}

	.innerkontent:hover {
	   background: #E3F4E6;
	   border:1px solid #95EFD0;
	}
	</style>
		<div class="box-isi">
			<div class='innerkontent'>
			<h4 style="margin:3px 0;padding:2px 5px;border:1px dotted #F87900;color:#FFFFFF;background: #FFA500;"><?php echo $judul; ?></h4>
			<a href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
			<img class="efek" style="margin: 2px; padding: 3px; border: 1px solid #E6E6FA; vertical-align: top;" align="left" alt="<?php echo $judul;?>" src="<?php echo base_url().'/_media/blog/thumb/thumb_'.$row->gambar_cover;?>" />
			</a>
			<div>
				<?php echo $isi; ?>
	            <br />
				<a href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">Selengkapnya....</a>
	        </div>
	        </div>
	     </div>
	    <?php
	    }
	    ?>
	</div>
	<?php if($jml_blog > $jml_page): ?>
	<div align="center">
		<a class="tag-pill" href="<?php echo site_url(current_lang().'posting'); ?>">Total <?php echo ($jml_blog); ?> Posting.</a>
	</div>
	<?php endif; ?>
	<hr>
<?php
}
?>

<?php
if( count($album) > 0)
{
?>
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/css/elem/hover-box-style.css" />
	<script type="text/javascript">
	$(document).ready(function() {
		$('ul.img-thumbs li').hover(
			function(){$(".overlay", this).stop().animate({top:'0px'}, {queue:false,duration:300});}, 
			function(){$(".overlay", this).stop().animate({top:'195px'},{queue:false,duration:300});}
		);  
	});
	</script>
	<style>
	img.fotoAlbum {padding: 2px;vertical-align: top}
	</style>

	<ul class="img-thumbs">
	<?php
		foreach ($album as $row){
		$cnt_gallery = $this->cms->getFrontCountGalleryAlbum($row->id);
	?>
    <li style="width:280px">
    <a style="cursor:default;" href="javascript:void(0);"><img style="padding: 2px;vertical-align: top;" width="200" src="<?php echo base_url().'/_media/album/small/small_'.$row->gambar_cover;?>" /></a>
    <div class="overlay" style="width:280px;height:270px">
    <?php
	if (current_lang(false) === 'id')
	{
		$nama = $row->nama_id;
		echo '<h1>'.humanize($nama).'</h1>';
		echo word_limiter($row->ket_id, 60);
	} else {
		$nama = $row->nama_en;
		echo '<h1>'.humanize($nama).'</h1>';
		echo word_limiter($row->ket_en, 60);
	}
	?>
	<p>
	<?php if($cnt_gallery > 0): ?>
	<a href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>">Gallery (<?php echo $cnt_gallery; ?>)</a>
	<?php else: ?>
	<a href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>">Selengkapnya....</a>
	<?php endif; ?>
	</p>
    <a class="zoom" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>" rel="" title="<?php echo humanize($nama); ?>"></a>
    </div>
    </li>
    <?php } ?>
    </ul>

    <?php if($jml_album > $jml_page): ?>
	<div align="center">
		<a class="tag-pill" href="<?php echo site_url(current_lang().'gallery'); ?>">Total <?php echo ($jml_album).' '.$this->lang->line('lbl_album'); ?>.</a>
	</div>
	<?php endif; ?>
	<hr>
<?php
}
?>

<?php
if( count($video) > 0)
{
?>
	<div class="wrap-efek" style="margin:0 auto; text-align:center;">
	<?php
	    $jml = 1;
	    foreach ($video as $row){
	    $cnt_playlist = $this->cms->getFrontCountVideoPlaylist($row->id);
	            if (current_lang(false) === 'id')
				{
					$nama = humanize($row->nama_id);
					$ket = word_limiter(strip_tags($row->ket_id), 70);
				} else {
					$nama = humanize($row->nama_en);
					$ket = word_limiter(strip_tags($row->ket_en), 70);
				}
	?>
			<?php echo '<h4 style="text-align:left;">'.$nama.'</h4>'; ?>
	
			<a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
			<img class="efek" style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo base_url().'/_media/videos/small/small_'.$row->gambar_video;?>" />
			</a>
	
	        <div style="text-align:left;" id="tampil-<?php echo $jml;?>">
				<?php echo $ket; ?>
	            <p>
	            <?php if($cnt_playlist > 0): ?>
	            <a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Playlist (<?php echo $cnt_playlist; ?>)</a>
	            <?php else: ?>
				<a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Selengkapnya....</a>
	            <?php endif; ?>
	            </p>
	        </div>
	        <?php
	        if($jml < count($video))
	        echo '<div style="clear:left"></div><br />';
	        ?>
	    <?php
	    $jml++;
	    }
	    ?>
	    </div>
	<?php if($jml_video > $jml_page): ?>
	<div align="center">
		<a class="tag-pill" href="<?php echo site_url(current_lang().'playlist'); ?>">Total <?php echo ($jml_video).' '.$this->lang->line('lbl_video'); ?>.</a>
	</div>
	<?php endif; ?>
<?php
}
?>


<script>
$(document).ready(function() {
		/*
		 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
		*/
		function formatTitle(a, b, c, d) {
            id = (c + 1);
            return '<span id="fancybox-title-over">' + $('#tampil-' + id).html() + '</span>'
        }
		$('.fancybox-media')
			.attr('rel', 'video_group')
			.fancybox({
			    width:640,
				height:360,
				'transitionIn': 'none',
	            'transitionOut': 'none',
	            'titlePosition' : 'inside',
	            'hideOnOverlayClick':false,
	            'hideOnContentClick':false,
	            'titleFormat': formatTitle,
	            'showNavArrows' : false,
			    'type': 'iframe',
			    iframe : {
			      preload: true
			    },
			    'onStart': function () {
										/*$('#z-video').slideUp('medium');
										$('#fancybox-wrap').css({'margin-top':'50px !important'});*/ },
			    'onClosed': function () {
										/*$('#z-video').slideDown('medium');
										$('#fancybox-wrap').css({'position':'fixed','top':'20px'});*/ }
			  });

});
</script>
<style type="text/css">
div#fancybox-wrap {}
span#fancybox-title-over {background:none;padding:0;max-height:200px;overflow:auto}
</style>

<?php
// tes custom data dari main view
//echo $tes;

if ( ! empty($video) )
{
?>
	<h3><?php echo $title; ?></h3>
	<p align="justify">
	<?php echo $content; ?>
	</p>
	
	<?php
	if ($per_page == 1)
	{
	?>
	
	    <div class="wrap-efek" style="margin:0 auto; text-align:center;">
	<?php
	    $jml = 1;
	    foreach ($video as $rvideo){
	    $cnt_video = $this->cms->getFrontCountVideoPlaylist($rvideo->id);
	?>
	
	<div id="z-video">
			<?php
				if($rvideo->tipe_video === 'youtube')
				echo youtube_embed($rvideo->src_video,640,360,TRUE);
				else
				echo vimeo_embed($rvideo->src_video,640,360);
			?>
	</div>

	        <div style="text-align:left">
	            <?php
	            if (current_lang(false) === 'id')
	            echo $rvideo->ket_id;
				else
				echo $rvideo->ket_en;
	            ?>
	
	            <?php if($cnt_video > 0): ?>
	            <h4>Playlist (<?php echo $cnt_video; ?>)</h4>
	            <?php endif; ?>
	        </div>
	    <?php
			if($cnt_video > 0)
			{
				$jml = 1;
				$playlist = $this->cms->getAllFrontVideoPlaylist($rvideo->id,$cnt_video,0);
				foreach ($playlist as $row){
	
				if($row->tipe_video === 'youtube')
				$url = youtube_fullvideo($row->src_video);
				else
				$url = vimeo_fullvideo($row->src_video);
	
				// ket
				if (current_lang(false) === 'id')
				{
					$jdl = humanize($row->nama_id);
					$ket = $row->ket_id;
				} else {
					$jdl = humanize($row->nama_en);
					$ket = $row->ket_en;
				}
				
				?>
					<a class="fancybox-media" rel="video_group" href="<?php echo $url;?>"><img class="efek" width="300" style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo base_url().'/_media/videos/small/small_'.$row->gambar_video;?>" /></a>
					<div id="tampil-<?php echo $jml;?>" style="display:none;background:#E5E5E5">
						<?php
						echo '<h4>'.$jdl.'</h4><div style="text-align:left">'.$ket.'</div>';
						?>
					</div>
	
				<?php
				$jml++;
				}
			}
		$jml++;
	    }
		echo "<div style='clear:both'></div>";
		if($rvideo->diskusi === 'on')
		echo $this->disqus->get_html();
	    ?>
	    </div>
	<?php
	}
	else
	{
	
	echo ( ! $video_page)?'':'<div class="pagination">'.$video_page.'</div>';
	
	?>
		
	<div class="wrap-efek" style="margin:0 auto; text-align:center;">
	<?php
		
	    $jml = 1;
	    foreach ($video as $row){
	    $cnt_playlist = $this->cms->getFrontCountVideoPlaylist($row->id);
	            if (current_lang(false) === 'id')
				{
					$nama = humanize($row->nama_id);
					$ket = word_limiter(strip_tags($row->ket_id), 70);
				} else {
					$nama = humanize($row->nama_en);
					$ket = word_limiter(strip_tags($row->ket_en), 70);
				}
	?>
			<?php echo '<h4 style="text-align:left;">'.$nama.'</h4>'; ?>
	
			<a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
			<img class="efek" style="margin: 10px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" src="<?php echo base_url().'/_media/videos/small/small_'.$row->gambar_video;?>" />
			</a>
	
	        <div style="text-align:left;" id="tampil-<?php echo $jml;?>">
				<?php echo $ket; ?>
	            <p>
	            <?php if($cnt_playlist > 0): ?>
	            <a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Playlist (<?php echo $cnt_playlist; ?>)</a>
	            <?php else: ?>
				<a href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Selengkapnya....</a>
	            <?php endif; ?>
	            </p>
	        </div>
	        <?php
	        if($jml < count($video))
	        echo '<div style="clear:left"></div><hr />';
	        ?>
	    <?php
	    $jml++;
	    }
	    ?>
	    </div>
	
	<?php
		echo ( ! $video_page)?'':'<div class="pagination">'.$video_page.'</div>';
	}
}
else {
    echo "<h3>Video Kosong</h3> Video Kosong, data tidak ditemukan";
}
?>

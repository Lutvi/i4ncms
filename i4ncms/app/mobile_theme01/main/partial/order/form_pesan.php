<div id="pemesanan-box">
	<h3><?php echo $title; ?></h3>

	<?php if(isset($info) && !empty($info)): ?>
	<div id="info-bayar-box">
	<?php foreach($info as $key => $isi): ?>
	<div  class="info"><?php echo $isi; ?></div>
	<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if($this->cart->total_items() > 0): ?>

<?php
	if ( $this->session->userdata('kunci_id') === FALSE  )
	{
	    $nama = '';$tlp = '';$email = '';$negara = '';$prov = '';$wil = '';$pos = '';
	    $alamat = $this->config->item('format_alamat');
	}
	else {
	    $custdata = $this->orderan->getPembeliById($this->session->userdata('kunci_id'));
	    $nama = $this->cms->dekodeString($custdata->nama_lengkap);
	    $tlp = $this->cms->dekodeString($custdata->no_telpon);
	    $email = $this->cms->dekodeString($custdata->email_cust);
	    $negara = $custdata->id_negara;
	    $prov = $custdata->id_provinsi;
	    $wil = $custdata->id_wilayah;
	    $pos = $this->cms->dekodeString($custdata->kode_pos);
	    $almt = $this->cms->dekodeString($custdata->alamat_rumah);
	    $alamat = str_ireplace("<br />", "\n", $almt);
	}

	$attr = array( 'id' => 'pesan');
	echo form_open('pemesanan/cek_pesan',$attr); 
	?>

	<fieldset class="ui-widget ui-widget-content ui-corner-all">
	<legend class="ui-widget ui-widget-header ui-corner-all">Data Pengiriman</legend>
	<div class="clft">
	<label for="nama_cust">Nama <br><span class="small">Nama lengkap Anda</span> </label>
	<input type="text" name="nama_cust" id="nama_cust" maxlength="150" value="<?php echo set_value('nama_cust',$nama); ?>" />
	<?php echo form_error('nama_cust'); ?>
	</div>

	<div class="clft">
	<label for="no_telp">No.Telpon <br><span class="small">Nomor Telpon/Hp</span> </label>
	<input type="text" name="no_telp" id="no_telp" maxlength="14" value="<?php echo set_value('no_telp',$tlp); ?>" />
	<?php echo form_error('no_telp'); ?>
	</div>

	<div class="clft">
	<label for="email">Email <br><span class="small">Alamat email Anda</span> </label>
	<input type="text" name="email" id="email" maxlength="150" value="<?php echo set_value('email', $email); ?>" />
	<?php echo form_error('email'); ?>
	</div>

	<div class="clft">
	<label for="negara">Negara <br><span class="small">Negara untuk pengiriman</span> </label>
	<select id="negara" name="negara" class="mr20">
	    <option value="indonesia" <?php echo set_select('negara', 'indonesia', (set_value('negara') == 'indonesia')?TRUE:FALSE); ?> >&nbsp;Indonesia&nbsp;</option>
	</select>

	<label class="ml40-wd130" for="provinsi">Provinsi <br><span class="small">Provinsi untuk pengiriman</span> </label>
	<select id="provinsi" data-url="<?php echo site_url('list_wilayah'); ?>" name="provinsi">
	    <option value="" <?php echo set_select('provinsi', '', (set_value('provinsi') == '')?TRUE:FALSE); ?> >&nbsp;----&nbsp;</option>
	<?php
	$list_provinsi = $this->maddons->listProvinsi();
	foreach( $list_provinsi as $nama) {
	?>
	    <option value="<?php echo $nama->id_provinsi; ?>" <?php echo set_select('provinsi', $nama->id_provinsi, ($prov == $nama->id_provinsi)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->provinsi); ?>&nbsp;</option>
	<?php
	}
	?>
	</select>
	<?php echo form_error('negara'); ?>
	<?php echo form_error('provinsi'); ?>
	</div>

	<div class="clft">
	<label for="wilayah">Wilayah <br><span class="small">Wilayah untuk pengiriman</span> </label>
	<select id="wilayah" name="wilayah"  class="mr20">
	    <option value="" <?php echo set_select('wilayah', '', (set_value('wilayah') == '')?TRUE:FALSE); ?> >&nbsp;----&nbsp;</option>
	<?php
	if ( empty($wil) && empty($prov) )
	$list_wilayah = $this->maddons->listKota(set_value('provinsi'));
	else
	$list_wilayah = $this->maddons->listKotaById(set_value('provinsi',$prov));
	foreach( $list_wilayah as $nama) {
	?>
	    <option value="<?php echo $nama->id_kab_kota; ?>" <?php echo set_select('wilayah', $nama->id_kab_kota, ($wil == $nama->id_kab_kota)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->kab_kota); ?>&nbsp;</option>
	<?php
	}
	?>
	</select>
	<label class="lbl_small" for="kode_pos">KodePos <br><span class="small">KodePos pengiriman</span> </label>
	<input class="input_small" type="text" name="kode_pos" id="kode_pos" maxlength="10" value="<?php echo set_value('kode_pos',$pos); ?>" />
	<?php echo form_error('wilayah'); ?>
	<?php echo form_error('kode_pos'); ?>
	</div>

	<div class="clft">
	<label for="alamat">Alamat Lengkap <br><span class="small">Alamat lengkap pengiriman. sertakan pula RT/RW dan No.Rumah</span> </label>
	<textarea spellcheck="false" name="alamat" id="alamat"><?php echo set_value('alamat',$alamat); ?></textarea>
	<?php echo form_error('alamat'); ?>
	</div>

	<div class="btnBox">
	<button class="ui-state-default" id="listKeranjang" type="button" data-url="<?php echo site_url('belanja/list_keranjang'); ?>"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-cart"></span>Update Keranjang</button>
	<button class="ui-state-default" id="bayarSekarang"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-check"></span>Proses Pembayaran</button>
	</div>

	</fieldset>
	<?php echo form_close(); ?>

	<?php else: ?>
	    <p>Data Belanja Anda kosong, Proses Transaksi tidak bisa dilanjutkan.</p>
	<?php endif; ?>
</div>

<?php if( ENVIRONMENT == 'development' ) : ?>
<script type="text/javascript">
$(function() {

    function expForm()
    {
        window.location.reload();
    }

    //list-keranjang
    $('#listKeranjang').click(function(e){
        e.preventDefault();
        var postURL = $(this).attr('data-url');
        
        $('#pemesanan-box').load(postURL);
    });

    //list-provinsi => kota
    $('select#provinsi').change(function(e){
        e.preventDefault();
        var postURL = $(this).attr('data-url');
        
        $.ajax({
            type: "POST",
            url: postURL,
            data: ({idp:$(this).val(),Hydra:$('input[name="Hydra"]').val()}),
            success: function(data){
                $('select#wilayah').empty().html(data);
                $('#kode_pos').val('');
            },
            error : expForm
        });
    });

});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(0(){0 5(){d.f.g()}$(\'#h\').i(0(e){e.6();7 a=$(1).8(\'2-3\');$(\'#j-k\').l(a)});$(\'9#m\').n(0(e){e.6();7 b=$(1).8(\'2-3\');$.o({p:"q",3:b,2:({r:$(1).4(),c:$(\'s[t="c"]\').4()}),u:0(a){$(\'9#v\').w().x(a);$(\'#y\').4(\'\')},z:5})})});',36,36,'function|this|data|url|val|expForm|preventDefault|var|attr|select|||Hydra|window||location|reload|listKeranjang|click|pemesanan|box|load|provinsi|change|ajax|type|POST|idp|input|name|success|wilayah|empty|html|kode_pos|error'.split('|'),0,{}))
</script>
<?php endif; ?>

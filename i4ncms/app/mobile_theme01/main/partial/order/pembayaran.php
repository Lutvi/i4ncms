
<div id="pemesanan-box">
<h3><?php echo $title; ?></h3>  

<?php if(isset($info) && ! empty($info)): ?>
<div id="info-bayar-box">
<?php foreach($info as $key => $isi): ?>
<div  class="info"><?php echo $isi; ?></div>
<?php endforeach; ?>
</div>
<?php endif; ?>

    <?php if($this->cart->total_items() > 0): ?>

	<h5>Total harga : Rp. <?php echo $this->cms->format_harga_indo($this->cart->total()); ?></h5>
	<h5>Diskon Transaksi : Rp. <?php echo $this->cms->format_harga_indo($diskon_transaksi); ?></h5>
	<h5>Total yang harus Anda bayar : Rp. <?php echo $this->cms->format_harga_indo($total_transaksi); ?></h5>

    <?php
    $attributes = array('id' => 'pembayaran');
    echo form_open('pemesanan/pembayaran', $attributes); 
    ?>
    <fieldset class="ui-widget ui-widget-content ui-corner-all">
    <input type="hidden" name="uid" id="uid" maxlength="" value="" />
    <p>Pilih Metode Pembayaran Anda.</p>
    
    <div id="metodeBayar">
    <?php
        $metode = $this->config->item('metode_bayar');
        $j = 0;
        foreach( $metode as $jns => $lbl):
    ?>
        <input type="radio" id="metode<?php echo $j; ?>" data-opsi="<?php echo 'opsi'.$j; ?>" name="metode" value="<?php echo $jns; ?>" <?php echo (set_value('metode')==$jns)?'checked="checked"':''; ?> /><label for="metode<?php echo $j; ?>"><?php echo $lbl['label']; ?></label>
    <?php
        $opsi[$j] = $lbl['opsi'];
        $j++;
        endforeach;
    ?>
    </div>
    <?php echo form_error('metode'); ?>
    
    <div id="box-opsi">
    <?php echo form_error('pilihan'); ?>
        <?php for($i=0; $i < count($opsi); $i++): ?>
        <div class="opsi" id="opsi<?php echo $i; ?>">
            <?php
                $j = 1;
                foreach($opsi[$i] as $nama => $logo):
            ?>
            <div class="logo-bayar ui-widget ui-widget-content ui-corner-all" style="background:url(<?php echo base_url().'assets/images/logo-bayar/'.$logo; ?>)center no-repeat;">
            <input type="radio" id="<?php echo underscore($nama.$j); ?>" name="pilihan" value="<?php echo $nama; ?>" <?php echo (set_value('pilihan')==$nama)?'checked="checked"':''; ?> /><label for="<?php echo underscore($nama.$j); ?>"><span><?php echo $nama; ?></span></label>
            </div>
            <?php
                $j++;
                endforeach;
            ?>
        </div>
        <?php endfor; ?>
    </div>
    
    <div id="box-notes">
    <p>Berikan catatan untuk kami tentang pesanan Anda.</p>
    <textarea spellcheck="false" name="notes"><?php echo set_value('notes',''); ?></textarea>
    </div>
    
    <p>
    <button class="ui-state-default" id="lanjutBayar"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-check"></span>Pembayaran</button>
    </p>
    </fieldset>
    <?php echo form_close(); ?>
    
    <?php else: ?>
    <p>Data Belanja Anda kosong, Proses Transaksi tidak bisa dilanjutkan.</p>
    <?php endif; ?>
</div>
<script>
$(function() {
    $( "#metodeBayar" ).buttonset();
    
    $(document).ready(function(){ 
        var elm = $('input[name="metode"]:checked'),
            metode = $(elm).val(),
            opsi = $(elm).attr('data-opsi');
        
        $('.opsi').hide();
        $('#'+opsi).show();
    });
    
    $('input[name="metode"]').click(function(){ 
        var metode = $(this).val(),
            opsi = $(this).attr('data-opsi');
        
        $('.opsi').hide();
        $('#'+opsi).show();
        $('input[name="pilihan"]:checked').removeAttr("checked");
    });
    
});
</script>

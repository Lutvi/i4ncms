<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/theme02/css/produk.css" />
<style>
pre, code { display:none; }
strike {font-color:red}
</style>
<script>
$(function($) {
    $('.fancy').fancybox();

    // *:button-beli
    $('button.beli').click(function(e){
        e.preventDefault();
        var postURL = $(this).attr('data-url'),
            id_prod = $(this).attr('data-produkid'),
            harga = $(this).attr('data-harga'),
            nama = $(this).attr('data-nama');
            
        $.ajax({
            type: "POST",
            url: postURL,
            data: "id="+id_prod+"&qty="+1+"&price="+harga+"&name="+nama+"&Hydra="+$('input[name="Hydra"]').val(),
            success: function(data){
                $('#keranjang').html(data);
            },
            error : function(){ alert('Not performed.'); }
        });
    });

});
</script>

<?php echo ( ! $produk_page)?'':'<div class="pagination">'.$produk_page.'</div>'; ?>
<?php
// tes custom data dari main view
//echo $tes;

// Error Handling-cek kalau kosong
if ( ! empty($content) )
{
    if ($tipe_produk === 'induk')
    {
	    if($content[0]->status === 'on')
		{
			echo '<div style="height:110px;padding:10px 0"><div style="padding:10px;width:100%;height:90px;float:left;background:#ADD8E6;border:1px solid #A52A2A">Banner Iklan Mahal (2jt/6bulan) atau 4 Produk2 promo disini.(580x120)</div></div>';
		}
		$jml_prod = count($content);
        foreach ($content as $item)
        {
			if ($item->status === 'on')
			{
?>
        <h3><?php echo (current_lang(false) === 'id' || empty($item->nama_prod_en))?$item->nama_prod:$item->nama_prod_en; ?></h3>
        <div class="box-prod-detail ui-helper-clearfix">
        
        <div class="box-img-prod-detail"> 
            <?php
            $pil_warna = $this->mproduk->getAllFrontWarnaProdukById($item->id_prod);
            if ( !empty($pil_warna) ) {
            ?>
            <div class="box-warna-prod-detail tips">
            <?php
            foreach ( $pil_warna as $warna )
            {
                if ( !empty($warna->kode_warna) ) {
            ?>
            <a class="pil-warna-detail" style="background:#<?php echo $warna->kode_warna; ?>" title="<?php echo $warna->id_prod; ?>" href="#warnaProduk-<?php echo $warna->id_prod; ?>"></a>
            <?php
                }
            }
            ?>
            </div>
            <?php
            }

            $foto = $this->mproduk->getAllImgFrontById($item->id_prod_img);
            
            if(current_lang(false) === 'id')
			$nama_prod = $item->nama_prod;
			else
			$nama_prod = $item->nama_prod_en;
            ?>
            
            <div class="multizoom<?php echo $item->id_prod; ?> thumbs"> 
            <?php
                foreach($foto as $fp)
                {
            ?>
                <a class="fancy" href="<?php echo base_url().'_produk/medium/medium_'.$fp->img_src; ?>">
                <img src="<?php echo base_url().'_produk/thumb/thumb_'.$fp->img_src; ?>" alt="<?php echo $fp->alt_text_img; ?>" />
                </a>
            <?php
                }
            ?>
            </div>
            
            <div class="box-btn-prod-detail">
                    <button class="beli beli-detail" data-url="<?php echo site_url('belanja/beli'); ?>" data-produkid="<?php echo bs_kode($item->id_prod); ?>"  data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($item->harga_spesial) )?$item->harga_spesial:$item->harga_prod); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></button>
            </div>
        </div>
            <div class="clr"></div>
            <p class="box-deskipsi-detail">
            <?php 
                if ( !empty($item->harga_spesial) )
                {
					$diskon = (!empty($item->diskon))?$item->diskon*100:0;
					if(current_lang(false) == 'id')
					{
						echo '<div class="deskripsi-harganormal-detail"><strike>Rp '.number_format($item->harga_prod, 0,'','.').'</strike></div>';
						echo '<span>Harga : Rp  '.number_format($item->harga_spesial, 0,'','.').'</span>';
						echo '<br />';
						echo '<span>Diskon : '.$diskon.'%</span>';
					}
					else {
						echo '<div class="deskripsi-harganormal-detail"><strike>IDR '.number_format($item->harga_prod, 0,'','.').'</strike></div>';
						echo '<span>Price : IDR '.number_format($item->harga_spesial, 0,'','.').'</span>';
						echo '<br />';
						echo '<span>Save : '.$diskon.'%</span>';
					}
					
					
                }
                else {
					if(current_lang(false) === 'id')
					echo '<span>Harga : Rp '.number_format($item->harga_prod, 0,'','.').'</span>';
					else
					echo '<span>Price : IDR '.number_format($item->harga_prod, 0,'','.').'</span>';
                }
                
                if ( ! empty($item->anak_untuk_jenis) )
                {
                    echo '<br />';
                    echo ucwords($item->anak_untuk_jenis).' : '.ucwords($item->nama_jenis_anak);
                }
                
                if ( empty($item->stok) )
                {
                    echo '<br />';
                    echo '<span>'.$this->lang->line('stok').' : '.$item->stok.'</span>';
                    echo '<br />';
                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
                }

                if(current_lang(false) === 'id')
                {
					if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi,50);
					else
					$deskripsi = $item->deskripsi;
                }
                else {
                	if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi_en,50);
					else
					$deskripsi = $item->deskripsi_en;
                }

                echo '<div class="clr"></div>'.$deskripsi;
            ?>
            </p>
            <?php if($jml_prod > 1) : ?>
			<a href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
			<?php endif; ?>
        </div>

<?php
			}
			else {
				echo "<h3>Produk tidak ditemukan</h3>";
				echo "<p>Data produk tidak ditemukan.</p>";
			}
        }
    }
    else {
		if($content[0]['status'] === 'on')
		{
			echo '<div style="height:110px;padding:10px 0"><div style="padding:10px;width:100%;height:90px;float:left;background:#ADD8E6;border:1px solid #A52A2A">Banner Iklan Mahal (2jt/6bulan) atau 4 Produk2 promo disini.(580x120)</div></div>';
		}
?>
<div class="grid">
    <div class="unit">
    <?php
        $i=0;
        foreach($content as $row)
        {
        $i++;
			if ( $row['status'] === 'on')
			{
?>
    <div class="one-of-two">
    
        <h3><?php echo (current_lang(false) === 'id' || empty($row['nama_prod_en']))?$row['nama_prod']:$row['nama_prod_en']; ?></h3>
        <div class="box-prod-list">
            <?php
            $pil_warna = $this->mproduk->getAllFrontWarnaProdukById($row['id_prod']);
            if ( !empty($pil_warna) ) {
            ?>
            <div class="box-warna-prod-list tips">
            <?php     
                foreach ( $pil_warna as $warna )
                {
                    if (!empty($warna->kode_warna)) {
            ?>
            <a class="pil-warna-list" style="background:#<?php echo $warna->kode_warna; ?>" title="<?php echo $warna->id_prod; ?>" href="#warnaProduk-<?php echo $warna->id_prod; ?>"></a>
            <?php
                    }
                }
            ?>
            </div>
            <?php
            }

            $foto = $this->mproduk->getAllImgFrontById($row['id_prod_img']);
            ?>
            
            <div id="thumb-produk-<?php echo $i.$row['id_prod']; ?>" style=""> 
                <a class="fancy" href="<?php echo base_url().'_produk/medium/medium_'.$foto[0]->img_src; ?>">
                <img src="<?php echo base_url().'_produk/thumb/thumb_'.$foto[0]->img_src; ?>" alt="<?php echo $foto[0]->alt_text_img; ?>" />
                </a>
            </div>
            
            <div class="box-pdetail-list">
            <?php 
                if ( !empty($row['harga_spesial']) )
                {
					$diskon = (!empty($row['diskon']))?$row['diskon']*100:0;
                    if(current_lang(false) === 'id')
                    {
						echo '<span class="deskripsi-harganormal-list"><strike>Rp '.number_format($row['harga_prod'], 0,'','.').'</strike></span>';
						echo '<br />';
						echo 'Harga : Rp  '.number_format($row['harga_spesial'], 0,'','.');
						echo '<br />';
						echo 'Diskon : '.$diskon.'%';
                    }
                    else{
							echo '<span class="deskripsi-harganormal-list"><strike>IDR '.number_format($row['harga_prod'], 0,'','.').'</strike></span>';
						echo '<br />';
						echo 'Price : IDR '.number_format($row['harga_spesial'], 0,'','.').'';
						echo '<br />';
						echo 'Save : '.$diskon.'%';
                    }
                }
                else {
                    if(current_lang(false) === 'id')
                    echo 'Harga : Rp  '.number_format($row['harga_prod'], 0,'','.');
                    else
                    echo 'Price : IDR '.number_format($row['harga_prod'], 0,'','.').'';
                }
                
                if ( !empty($row['anak_untuk_jenis']) )
                {
                    echo '<br />';
                    echo ucwords($row['anak_untuk_jenis']).' : '.ucwords($row['nama_jenis_anak']);
                }
                
                if ( empty($row['stok']) )
                {
                    echo '<br />';
                    echo '<span>'.$this->lang->line('stok'). ': '.$row['stok'].'</span>';
                    echo '<br />';
                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
                }

                if(current_lang(false) === 'id')
                $nama_prod = $row['nama_prod'];
                else
                $nama_prod = $row['nama_prod_en'];
            ?>    
            <div class="box-btn-prod-list">
                    <button style="margin-top:15px" class="beli beli-list" data-url="<?php echo site_url('belanja/beli'); ?>" data-produkid="<?php echo bs_kode($row['id_prod']); ?>" data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($row['harga_spesial']) )?$row['harga_spesial']:$row['harga_prod']); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></button>
                    <!--<button class="detail detail-list" data-url="<?php echo site_url('belanja/beli'); ?>" data-produkid="<?php echo bs_kode($row['id_prod']); ?>"><?php echo $this->lang->line('detail_produk'); ?></button>-->
            </div>
            <?php    
                echo '<h5>'.$this->lang->line('detail_produk').'</h5>';
                if(current_lang(false) === 'id')
                $desc = strip_tags($row['deskripsi'],"<p><a><br><strong><span><font><ul><li><ol>");
                else
                $desc = strip_tags($row['deskripsi_en'],"<p><a><br><strong><span><font><ul><li><ol>");
                echo word_limiter($desc,20);
            ?>
            <br />
            <a href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
            </div>
            
            
        </div>
<?php
			}
			else {
				echo "<h3>Produk tidak ditemukan</h3>";
				echo "<p>Data produk tidak ditemukan.</p>";
			}
?>
			</div>
<?php
        }
?>
	</div>
	</div>
<?php
    }
	echo ( ! $produk_page)?'':'<div class="pagination">'.$produk_page.'</div>';
}
else {
?>
    <h3>Produk tidak ditemukan</h3>
    <p>Data produk tidak ditemukan.</p>
<?php
}
?>


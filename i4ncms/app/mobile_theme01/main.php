<!DOCTYPE html>
<html lang="<?php echo $this->config->item('language'); ?>">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta http-equiv="cleartype" content="on">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<title><?php echo $title. ' | ' . $this->config->item('site_name'); ?></title>

	<!--
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/h/apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/m/apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" href="img/l/apple-touch-icon-precomposed.png">
    <link rel="shortcut icon" href="img/l/apple-touch-icon.png">
    -->
	
	<!-- Google font -->
    <link href="http://fonts.googleapis.com/css?family=Ubuntu&subset=latin" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
	<style>
    *{
     font-family: 'Ubuntu', sans-serif;
    }
    </style>
	<link rel="stylesheet" href="<?php echo base_url(); ?>m-assets/dlmenu/css/default.css" media="screen">
	<link rel="stylesheet" href="<?php echo base_url(); ?>m-assets/dlmenu/css/component.css" media="screen">
    <!-- Main -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>m-assets/main/css/style2-theme02.css" />
	<link href="<?php echo base_url(); ?>assets/css/themes/Aristo/jquery-ui.css" rel="stylesheet" type="text/css" />
	
    <!-- UI -->
	<script src="<?php echo base_url(); ?>m-assets/dlmenu/js/modernizr.custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jqui/jquery-1.8.3-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jqui/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- MENU -->
    <script src="<?php echo base_url(); ?>m-assets/dlmenu/js/jquery.dlmenu.js"></script>

    <!--Album Media CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/plugins/fancybox/jquery.fancybox-1.3.4.css" />
    <!--Album Gallery JS-->    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/fancybox/jquery.fancybox-1.3.4.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/fancybox/jquery.mousewheel-min.js"></script>--> 

	<?php $this->load->view('global_assets/front_mobile_all_assets'); ?>

 <script>
    $(function(){
      $( '#dl-menu' ).dlmenu({
        animationClasses : { in : 'dl-animate-in-3', out : 'dl-animate-out-3' }
      });
      
      if($("div#pagination2").is(":visible")){
        $("#main_page h3").hide();
      }

      $('.tag-pill').button();
      
    });
</script>
</head>

<body>
<header class="clearfix">
	<h3><a href="<?php echo base_url(); ?>"><?php echo $this->config->item('site_name'); ?></a></h3>
	<?php if($this->uri->segment(1) === FALSE || strlen($this->uri->segment(1)) == 2): ?>
		<div class="menu_lang"><?php echo alt_site_url(); ?></div>
	<?php endif; ?>
	<?php if($tipe != 'pemesanan'): ?>
	<?php $this->load->view_theme('mob_menu', '', 'mobile_'.$this->config->item('theme_id').'/main/menu/'); ?>
	<?php endif; ?>
</header>
<section>
   <div id="main_page">
	<?php
		// tambahan untuk custom data format array(key => value)
		$data['data'] = array(
					'tes' => 'Testing data custom set dari main view.'
					);
		$this->load->view('global_content/mobile_content', $data);
	?>
	<?php if($this->config->item('toko_aktif') && $tipe != 'pemesanan'): ?>
	<h3 class="rounded ui-helper-clearfix ui-widget-header"><span style="float:left;margin:3px;margin-right:-12px" class="ui-icon ui-icon-cart"></span><?php echo $this->lang->line('keranjang'); ?></h3>
	<div id="keranjang">
		<?php echo form_open(); ?>
		<?php echo form_close(); ?>
		<?php $this->load->view_theme('keranjang','',$this->config->item('theme_id').'/ajax/'); ?>
	</div>
	<?php endif; ?>
   </div>   
</section> <!-- content -->
<footer>
	<div id="profile"><h2><span class="cr">i4n</span><span class="cb">CMS</span></h2></div>
</footer>

</body>
</html>

<!DOCTYPE html>
<html lang="<?php echo current_lang(false); ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->load->view('global_assets/front_all_meta'); ?>

	<title><?php echo $this->config->item('site_name') . ' - ' . $title; ?></title>
    <!-- Google font
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700"> -->
    <!--Include JQM and JQ-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>m-assets/jquery.mobile-1.4.0/jquery.mobile-1.4.0.min.css" />

    <!-- Main -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>m-assets/main/css/style-theme01.css" />

    <!--JQM and JQ globals-->
    <script src="<?php echo base_url(); ?>m-assets/main/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>m-assets/jquery.mobile-1.4.0/jquery.mobile-1.4.0.min.js"></script>

	<?php $this->load->view('global_assets/front_mobile_all_assets'); ?>

    <script type="text/javascript">
	$(document).bind("mobileinit", function(){
            $.extend(  $.mobile , {
                  ajaxEnabled : false,
                  autoInitializePage = false
             });
    });
	</script>
</head>
<body>

<div data-role="page" id="cms-page" data-ajax=false>
    <div data-role="header" data-theme="b" data-position="fixed">
        <a href="#left-panel" data-icon="bars" data-iconpos="notext" data-shadow="false" data-iconshadow="false">Menu</a>
        <a href="#right-panel" data-icon="shop" data-iconpos="notext" data-shadow="false" data-iconshadow="false" data-position="right"><?php echo $this->lang->line('keranjang'); ?></a>
        <h1 id="jdl-halaman"><?php echo ($tipe=='module')?'':$title; ?></h1>
        <?php if($this->uri->segment(1) === FALSE || strlen($this->uri->segment(1)) == 2): ?>
			<div class="menu_lang"><?php echo alt_site_url(); ?></div>
		<?php endif; ?>
    </div><!-- /header -->

    <div role="main" class="ui-content">
		<article>
    	<div data-role="content" id="content">
			<?php
			// tambahan untuk custom data format array(key => value)
			$data['data'] = array(
						'tes' => 'Testing data custom set dari main view.'
						);
			$this->load->view('global_content/mobile_content', $data);
			?>
        </div>
		</article>
    </div><!-- /content -->

    <div data-role="panel" id="left-panel">
        <h3><?php echo $this->config->item('site_name'); ?></h3>
        <form>
		     <label class="ui-hidden-accessible" for="cari-txt">Search:</label>
		     <input data-clear-btn="true" data-mini="true" name="cari-txt" id="cari-txt" value="" type="search">
		     <input data-mini="true" data-icon="carat-r" data-iconpos="right" value="Cari" type="submit">
	     </form>

		<?php $this->load->view_theme('mob_menu', '', 'mobile_'.$this->config->item('theme_id').'/main_default/menu/'); ?>
    </div><!-- /panel -->

    <div data-role="panel" id="right-panel" data-display="overlay" data-position="right">

        <ul data-role="listview" data-icon="false">
        	<li data-icon="delete"><a href="#" data-rel="close">Close</a></li>
        	<li data-role="list-divider"><?php echo $this->lang->line('keranjang'); ?></li>
            <li><a href="#">Twitter</a></li>
            <li><a href="#">Facebook</a></li>
            <li><a href="#">Google +</a></li>
            <li><a href="#">Mail</a></li>
        </ul>

        <div style="height:30px"></div>

        <form class="userform">

        	<h2>Login</h2>

            <label for="name">Username:</label>
            <input type="text" name="name" id="name" value="" data-clear-btn="true" data-mini="true">

            <label for="password">Password:</label>
            <input type="password" name="password" id="password" value="" data-clear-btn="true" autocomplete="off" data-mini="true">

            <div class="ui-grid-a">
                <div class="ui-block-a"><a href="#" data-rel="close" class="ui-btn ui-shadow ui-corner-all ui-btn-b ui-mini">Cancel</a></div>
                <div class="ui-block-b"><a href="#" data-rel="close" class="ui-btn ui-shadow ui-corner-all ui-btn-a ui-mini">Save</a></div>
			</div>
        </form>
    </div><!-- /panel -->

    <div data-role="footer" data-theme="b">
		<div id="profile">
			<h2><span class="cr">RedDanio</span><span class="cb">CMS</span></h2>
        </div>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>

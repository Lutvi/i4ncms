<div class="sidebar rounded" style="overflow:hidden;background:#CFE4EB;">

<h3 class="rounded ui-helper-clearfix ui-widget-header"><span style="float:left;margin:3px;margin-right:-18px" class="ui-icon ui-icon-tag"></span><?php echo ($tipe == 'blog' || $tipe == 'album' || $tipe == 'video')?$tipe.' '.$this->lang->line('lbl_tag'):$this->lang->line('lbl_tag');?></h3>

<p>
    <?php
	$mtags = $this->cms->getFrontAllTagObjId();
	$id_tag = array();
	foreach($mtags as $mtag): ?>
		<?php
		$tags = $this->cms->getTagsLabelByObjekId($mtag->id_objek,$mtag->tipe_objek);
		foreach($tags['rs_kat'] as $tag):
			if(in_array($tag->id_kategori, $id_tag))
			continue;
			if (current_lang(false) === 'id')
			{
				$nama = $tag->label_id;
			}
			else {
				$nama = $tag->label_en;
			}
			$link_url = base_url(current_lang().'tag/'.$mtag->tipe_objek.'/'.underscore($nama).config_item('url_suffix'));

			if($tipe == 'home' || $tipe == 'halaman' || $tipe == 'pembayaran' || $tipe == 'produk' || $tipe == 'module'){
			?>
				#<a class="tagsBawah" href="<?php echo $link_url; ?>" style="5px"><?php echo $nama; ?></a>&nbsp;&nbsp;
			<?php
			}
			elseif($mtag->tipe_objek == $tipe){
			?>
				#<a class="tagsBawah" href="<?php echo $link_url; ?>" style="5px"><?php echo $nama; ?></a>&nbsp;&nbsp;
			<?php
			}
			else{ continue; }

			$id_tag[] = $tag->id_kategori;
		endforeach; ?>
	<?php endforeach; ?>
</p>

</div>

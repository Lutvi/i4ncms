<?php if( ! empty($kat_konten)): ?>
<div class="sidebar rounded" style="overflow:hidden;background:#CFE4EB;">
<h3 class="rounded ui-helper-clearfix ui-widget-header"><span style="float:left;margin:3px;margin-right:-12px" class="ui-icon ui-icon-flag"></span><?php echo $this->lang->line('lbl_kategori'); ?></h3>
	<div style="padding-top:10px;padding-bottom:10px">
	<?php
	$attr = array('style' => 'font-size:14px;line-height:20px;padding-right:20px;float:left');
	foreach ($kat_konten as $kat)
	{
		if (current_lang(false) === 'id')
		{
			$nama = $kat->label_id;
			$lbl = 'kategori/';
			$lbl_tipe = $kat->tipe_kategori;
		}
		else {
			$nama = $kat->label_en;
			$lbl = 'category/';
			$lbl_tipe = ($kat->tipe_kategori == 'produk' && current_lang(false) == 'en')?'product':$kat->tipe_kategori;
		}

		$url_kat = base_url().current_lang().$lbl.$lbl_tipe.'/'.underscore($nama).'.html';

		if($this->cms->cekKategoriKonten($kat->tipe_kategori,$kat->id_kategori))
		{
			$kategori[$kat->tipe_kategori][] = anchor($url_kat,$nama,$attr);
		}
	}

		echo heading($this->lang->line('lbl_produk'), 6, 'style="clear:both"');
		if($this->config->item('toko_aktif') && ! empty($kategori['produk'])){
			foreach($kategori['produk'] as $item)
			{
				echo $item;
			}
		}

		echo heading($this->lang->line('lbl_blog'), 6, 'style="clear:both"');
		if(! empty($kategori['blog'])){
		foreach($kategori['blog'] as $item)
		{
			echo $item;
		}
		}

		echo heading($this->lang->line('lbl_album'), 6, 'style="clear:both"');
		if(! empty($kategori['album'])){
		foreach($kategori['album'] as $item)
		{
			echo $item;
		}
		}

		echo heading($this->lang->line('lbl_video'), 6, 'style="clear:both"');
		if(! empty($kategori['video'])){
		foreach($kategori['video'] as $item)
		{
			echo $item;
		}
		}
	?>
	</div>
</div>
<?php endif; ?>

<?php if( ! empty($iklan_sidebar) && $this->config->item('iklan')): ?>
<div class="sidebar rounded" style="overflow:hidden;background:#CFE4EB;">
<h3 class="rounded ui-helper-clearfix ui-widget-header"><span style="float:left;margin:3px;margin-right:-18px" class="ui-icon ui-icon-link"></span><?php echo $this->lang->line('lbl_iklan'); ?></h3>
	<div style="padding-top:10px;padding-bottom:10px">
	<?php foreach ($iklan_sidebar as $iklan): ?>
	<div style="text-align:center;padding-top:10px;padding-bottom:10px;background:#E2E8E8">
		<a class="iklan" target="_blank" href="<?php echo $iklan->url_target; ?>" title="<?php echo humanize($iklan->nama); ?>" data-upurl="shared/ajax_cek/uphits_iklan" data-idiklan="<?php echo bs_kode($iklan->id); ?>">
		<span style="display:none"><?php echo $iklan->hits; ?></span>
		<img src="<?php echo base_url().'_media/banner-iklan/medium/medium_'.$iklan->img_src; ?>" alt="<?php echo $iklan->nama; ?>" width="200" />
		</a>
	</div>
	<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>

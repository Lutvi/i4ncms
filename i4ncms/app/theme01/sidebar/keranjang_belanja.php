<?php if($this->config->item('toko_aktif')): ?>
<div id="keranjang-box" class="sidebar rounded tips" style="overflow:hidden;background:#CFE4EB;">
    <h3 class="rounded ui-helper-clearfix ui-widget-header"><span style="float:left;margin:3px;margin-right:-12px" class="ui-icon ui-icon-cart"></span><?php echo $this->lang->line('keranjang'); ?></h3>
    <div id="keranjang">
        <?php $this->load->view_theme('keranjang','',$this->config->item('theme_id').'/ajax/'); ?>
    </div>
</div>
<?php endif; ?>


<ul class="menu" id="menu">
	<?php foreach($menu as $row): ?>
	<!-- Level 1 -->
	<?php if( $row['link'] > 0 || $row['link'] === 'home'): ?>
    <li><a href="<?php echo ($row['link'] === 'home')?site_url(current_lang().'home'):site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row['link'])); ?>" class="menulink"><?php echo $row['title']; ?></a>
    <?php else: ?>
	<li><a href="javascript:void(0);" class="menulink"><?php echo $row['title']; ?></a>
    <?php endif; ?>
		<?php
        $sub_menu = $this->cms->getParentMenu($row['id']);
        if(count($sub_menu)>0):
		echo '<ul>';
		foreach($sub_menu as $row2):
		$sub_sub_menu = $this->cms->getParentMenu($row2['id']);
        ?>
        <!-- Level 2 -->
        <li><a href="<?php echo ($row2['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row2['link'])):'javascript:void(0);'; ?>" class="<?php echo (count($sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row2['title']; ?></a>
            <?php 
			if(count($sub_sub_menu)>0):
			echo '<ul>';
			foreach($sub_sub_menu as $row3): 
			$sub_sub_sub_menu = $this->cms->getParentMenu($row3['id']);	
			?>
            	<!-- Level 3 -->
                <li><a href="<?php echo ($row3['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row3['link'])):'javascript:void(0);'; ?>" class="<?php echo (count($sub_sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row3['title']; ?></a>
                	<?php 
					if(count($sub_sub_sub_menu)>0):
					echo '<ul>';
					foreach($sub_sub_sub_menu as $row4):
					$sub_sub_sub_sub_menu = $this->cms->getParentMenu($row4['id']);
					?>
						<!-- Level 4 -->
						<li><a href="<?php echo ($row4['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row4['link'])):'javascript:void(0);'; ?>"  class="<?php echo (count($sub_sub_sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row4['title']; ?></a>
							<?php 
							if(count($sub_sub_sub_sub_menu)>0):
							echo '<ul>';
							foreach($sub_sub_sub_sub_menu as $row5):
							$sub_sub_sub_sub_sub_menu = $this->cms->getParentMenu($row5['id']);
							?>
								<!-- Level 5 -->
								<li><a href="<?php echo ($row5['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row5['link'])):'javascript:void(0);'; ?>"  class="<?php echo (count($sub_sub_sub_sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row5['title']; ?></a>
							<?php endforeach; ?>
							</li></ul>
							<?php endif; ?>
					<?php endforeach; ?>
					</li></ul>
					<?php endif; ?>
			<?php endforeach; ?>
            </li></ul>
        	<?php endif; ?>	
		</li>
		<?php endforeach; ?>
        </ul>
		<?php endif; ?>	
	</li>
    <?php endforeach; ?>
</ul>
<script type="text/javascript">
	var menu=new menu.dd("menu");
	menu.init("menu","menuhover");
</script>

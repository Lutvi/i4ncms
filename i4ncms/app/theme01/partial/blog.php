<?php
if(isset($snippet) && ! empty($snippet['js']) && ! empty($snippet['css'])) :
?>
		<script src="<?php echo base_url('_plugins/WYSIWYG/ckeditor/ckeditor.js'); ?>" type="text/javascript"></script>
<?php
	foreach($snippet['js'] as $item):
	if(file_exists(FCPATH.'_plugins/WYSIWYG/SyntaxHighlighter/scripts/'.$item)):
?>
		<script src="<?php echo base_url('_plugins/WYSIWYG/SyntaxHighlighter/scripts/'.$item); ?>" type="text/javascript"></script>
<?php
	endif;
	endforeach;

	foreach($snippet['css'] as $item):
	if(file_exists(FCPATH.'_plugins/WYSIWYG/SyntaxHighlighter/styles/'.$item)):
?>
		<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url('_plugins/WYSIWYG/SyntaxHighlighter/styles/'.$item); ?>"></link>
<?php
	endif;
	endforeach;
endif;
?>
<script>
$(document).ready(function() {
	$('.cover-blog, .fancybox-blogalbum')
	.fancybox({ centerOnScroll: true });
	$('.fancybox-blogmedia')
			.attr('rel', 'video_group')
			.fancybox({
			    width:680,
				height:400,
	            'centerOnScroll': true,
	            'hideOnOverlayClick':false,
	            'hideOnContentClick':false,
	            'showNavArrows'   : false,
			    'type': 'iframe',
			    iframe : {
			      preload: true
			    },
			    'onStart': function () { $('#fancybox-wrap').css({'margin-top':'50px !important'}); },
			    'onClosed': function () { $('#fancybox-wrap').css({'position':'fixed','top':'20px'}); }
			  });
});
</script>

<?php
if ( ! empty($konten_blog) )
{
?>
	<?php
	if ($per_page == 1)
	{
	?>
    <div class="h-entry" itemscope itemType="http://schema.org/BlogPosting">
    <meta itemprop="inLanguage" content="<?php echo current_lang(false); ?>"/>
    <h1 itemprop="name" class="blog p-name"><?php echo $title; ?></h1>
	<div class="wrap-efek blogBox">
	<?php
	    foreach ($konten_blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = $row->isi_id;
			} else {
				$judul = humanize($row->judul_en);
				$isi = $row->isi_en;
			}
			$blgrate = ($row->ratting*13);
	?>
	<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, strtotime($row->tgl_terbit)); ?>"/>
	<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_revisi); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_revisi); ?></time>
    <!-- SosMed -->
		<meta itemprop="interactionCount" content="UserTweets:1203"/>
		<meta itemprop="interactionCount" content="UserComments:78"/>
		<div itemscope="" itemtype="http://data-vocabulary.org/Review-aggregate" class="mrgTop8 mrgBot10">
        	<div class="grate w64 mrgBot8">
                <span class="fill" style="width:<?php echo $blgrate; ?>px"></span>
            </div>
			<span itemprop="itemreviewed"><?php echo $this->lang->line('lbl_rating'); ?> </span>
			<span itemprop="rating" itemscope="" itemtype="http://data-vocabulary.org/Rating">
				<span itemprop="average"><?php echo $row->ratting; ?></span>/<span itemprop="best">5</span>
			</span>
			<?php echo $this->lang->line('lbl_nrating'); ?> total <span itemprop="votes"><?php echo $row->votes; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
		</div>
    <!-- End SosMed -->
	<a class="cover-blog" href="<?php echo base_url().'/_media/blog/medium/medium_'.$row->gambar_cover;?>">
		<img itemprop="thumbnailUrl" class="efek fotoBlog u-photo" width="300" alt="<?php echo $judul;?>" src="<?php echo base_url().'/_media/blog/small/small_'.$row->gambar_cover;?>" />
	</a>
	<div class="e-content" itemprop="articleBody"><?php echo $isi; ?></div>
    
	<?php
	if($row->embed_id_album > 0)
	{
		echo '<div class="clKiri"></div>';
		echo '<div itemprop="video" itemscope itemtype="http://schema.org/ImageObject">';
		$gambar_album = $this->cms->getAllAlbumFrontById($row->embed_id_album);
	    $album = 1;
	    foreach ($gambar_album as $ralbum){
	    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($ralbum->id);
	    if (current_lang(false) === 'id') {
			$nama_album = humanize($ralbum->nama_id);
			echo '<h2>Album :</h2> <div class="jdlEmbed" itemprop="name">'.$nama_album.'</div>';
		} else {
			$nama_album = humanize($ralbum->nama_en);
			echo '<h2>Albums :</h2> <div class="jdlEmbed" itemprop="name">'.$nama_album.'</div>';
		}
	?>
	        <a itemprop="contentUrl" class="fancybox-blogalbum" rel="album_group" title="<?php echo $nama_album; ?>" href="<?php echo base_url().'/_media/album/medium/medium_'.$ralbum->gambar_cover;?>">
		        <img class="efek mediaBlog" width="80" alt="<?php echo $nama_album;?>" src="<?php echo '/_media/album/thumb/thumb_'.$ralbum->gambar_cover;?>" />
	        </a>
			<?php
			if($cnt_gallery > 0)
			{
				$gallery = 1;
				$gambar_gallery = $this->cms->getAllFrontGalleryAlbum($ralbum->id,$cnt_gallery,0);
				foreach ($gambar_gallery as $rgal){
					if (current_lang(false) === 'id')
						$nama_gallery = humanize($rgal->nama_id);
					else
						$nama_gallery = humanize($rgal->nama_en);
			?>
					<a itemprop="contentUrl" class="fancybox-blogalbum" rel="album_group" title="<?php echo $nama_gallery; ?>" href="<?php echo base_url().'/_media/album-gallery/medium/medium_'.$rgal->gambar_gallery;?>">
						<img class="efek mediaBlog" width="80" alt="<?php echo $nama_gallery;?>" src="<?php echo base_url().'/_media/album-gallery/thumb/thumb_'.$rgal->gambar_gallery;?>" />
					</a>
	<?php
				$gallery++;
				}
			}
		$album++;
	    }
	  echo '</div>';
	} // endif-embed-album
	?>
	
	<?php
	if($row->embed_id_video > 0)
	{
		echo '<div class="clKiri"></div>';
		echo '<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">';
		$video = $this->cms->getAllVideoFrontById($row->embed_id_video);
	    $jml = 1;
	    foreach ($video as $rvideo)
	    {
		    $cnt_video = $this->cms->getFrontCountVideoPlaylist($rvideo->id);
		    
		    if($rvideo->tipe_video === 'youtube')
				$url_video = youtube_fullvideo($rvideo->src_video);
			else
				$url_video = vimeo_fullvideo($rvideo->src_video);
		
			// ket
			if (current_lang(false) === 'id')
			{
				$jdl_video = humanize($rvideo->nama_id);
				$ket_video = $rvideo->ket_id;
				echo '<h2>Video :</h2> <div class="jdlEmbed" itemprop="name">'.$jdl_video.'</div>';
			} else {
				$jdl_video = humanize($rvideo->nama_en);
				$ket_video = $rvideo->ket_en;
				echo '<h2>Videos :</h2> <div class="jdlEmbed" itemprop="name">'.$jdl_video.'</div>';
			}
	?>
		<a itemprop="embedUrl" class="fancybox-blogmedia" rel="video_group" title="<?php echo $jdl_video; ?>" href="<?php echo $url_video;?>">
			<img itemprop="thumbnail" width="100" class="efek mediaBlog" src="<?php echo base_url().'/_media/videos/thumb/thumb_'.$rvideo->gambar_video;?>" />
		</a>
    <?php
			if($cnt_video > 0)
			{
				$jml = 1;
				$playlist = $this->cms->getAllFrontVideoPlaylist($rvideo->id,$cnt_video,0);
				foreach ($playlist as $rply){
	
				if($rply->tipe_video === 'youtube')
				$url = youtube_fullvideo($rply->src_video);
				else
				$url = vimeo_fullvideo($rply->src_video);

				// ket
				if (current_lang(false) === 'id')
				{
					$jdl = humanize($rply->nama_id);
					$ket = $rply->ket_id;
				} else {
					$jdl = humanize($rply->nama_en);
					$ket = $rply->ket_en;
				}
	?>
					<a itemprop="embedUrl" class="fancybox-blogmedia" rel="video_group" title="<?php echo $jdl; ?>" href="<?php echo $url;?>">
						<img itemprop="thumbnail" width="100" class="efek mediaBlog" src="<?php echo base_url().'/_media/videos/thumb/thumb_'.$rply->gambar_video;?>" />
					</a>
	<?php
				$jml++;
				}
			}
		$jml++;
	    }
	  echo '</div>';
	} // endif-embed-video
	?>

<!-- Komponen Artikel -->
	<div class="clKiri"></div>
    <div id="profilPenulis">
    	<h5><?php echo $this->lang->line('lbl_ttg_penulis'); ?> :</h5>
		<img style="float:left" src="<?php echo base_url(); ?>/_media/admincms/medium/medium_<?php echo $penulis['foto']; ?>" width="110" />
		<div style="float:left;margin-left:10px">
		<span style="font-weight:bold;font-size:18px"><?php echo $penulis['nama']; ?></span>
		<br>
		<?php echo $penulis['bio']; ?>
		</div>
	</div>
    <div class="clKiri"></div>
	<?php $this->load->view_theme('tag_box', '', config_item('theme_id').'/partial/'); ?>
    <div class="clKiri"></div>
	<div id="ratingBox">
        <div class="mrgTop20">
            <?php echo $this->lang->line('lbl_rating'); ?> <span><?php echo $row->ratting;?></span>/<span>5</span>
            <?php echo $this->lang->line('lbl_nrating'); ?> <span><?php echo $row->votes;?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
        </div>
    </div>
	<!--Rating-->
    <form id="ratingForm">
        <input type="hidden" name="rt" value="<?php echo bs_kode($row->id); ?>" />
        <input type="hidden" name="tipe" value="blog" />
        <?php echo $this->lang->line('lbl_beriPeringkat'); ?> :
        <div class="starRating">
          <div>
            <div>
              <div>
                <div>
                  <input id="rating1" type="radio" name="rating" value="1">
                  <label for="rating1"><span>1</span></label>
                </div>
                <input id="rating2" type="radio" name="rating" value="2">
                <label for="rating2"><span>2</span></label>
              </div>
              <input id="rating3" type="radio" name="rating" value="3">
              <label for="rating3"><span>3</span></label>
            </div>
            <input id="rating4" type="radio" name="rating" value="4">
            <label for="rating4"><span>4</span></label>
          </div>
          <input id="rating5" type="radio" name="rating" value="5">
          <label for="rating5"><span>5</span></label>
      </div>
      </form>
      <div id="ratingRes">
      	<?php echo $this->lang->line('lbl_upPeringkat'); ?> <span id="ratingNrat"></span> <?php echo $this->lang->line('lbl_nrating'); ?> <span id="ratingNvot"></span> <?php echo $this->lang->line('lbl_jvote'); ?>
      </div>
      <div class="btsGarisBiru"></div>
	<!--End Rating-->
<!-- End Komponen Artikel --> 
    
    <?php
	//load iklan bawah
	$this->load->view_theme('iklan_konten_bawah', '', config_item('theme_id').'/partial/iklan/');
	?>
		<?php if($row->diskusi === 'on'): ?>
	    <div id="disqusBox"><?php echo $this->disqus->get_html(); ?></div>
      	<?php endif; ?>
    </div> <!-- end-.h-entry-->
    <script src="<?php echo base_url('_plugins/WYSIWYG/load_syntaxhighlighter.js'); ?>" type="text/javascript"></script>
	<?php
		} //end-foreach single-posting
	
	} //end-if layout single
	else { //Layout list
	echo ( ! $blog_page)?'':'<div class="pagination">'.$blog_page.'</div>';
	?>
    <div class="posRelatif">
    <h1 itemprop="name" class="blog p-name"><?php echo $title; ?></h1>
    <div class="p-summary tajust" itemprop="description">
		<?php echo $content; ?>
    </div>
	<div itemscope itemtype="http://schema.org/Blog" class="wrap-efek taleft mrgAuto">
	<?php
	    $blog = 1;
	    foreach ($konten_blog as $row)
	    {
			if (current_lang(false) === 'id')
			{
				$judul = humanize($row->judul_id);
				$isi = word_limiter($row->isi_id, 55);
			}
			else {
				$judul = humanize($row->judul_en);
				$isi = word_limiter($row->isi_en, 55);
			}
			$blgrate = ($row->ratting*13);
	?>
		<div itemprop="blogPosts" class="box-isi h-item" itemscope itemtype="http://schema.org/BlogPosting">
			<div class="innerkontent">
				<a class="u-url" itemprop="url" href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
					<h4 itemprop="name" class="judulBlog p-name"><?php echo substr($judul,0,60); ?></h4>
				</a>
				<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, strtotime($row->tgl_terbit)); ?>"/>
				<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_revisi); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_revisi); ?></time>
                <div class="grate w64 fright mrgTopMin5">
                    <span class="fill" style="width:<?php echo $blgrate; ?>px"></span>
                </div>
                <div class="clKanan"></div>
				<a class="u-url" itemprop="url" href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
					<img itemprop="thumbnailUrl" class="efek u-photo fotoBlog" alt="<?php echo $judul;?>" src="<?php echo base_url().'/_media/blog/thumb/thumb_'.$row->gambar_cover;?>" />
				</a>
				<div>
					<div class="p-summary" itemprop="description"><?php echo $isi; ?></div>
					<a href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>"><?php echo $this->lang->line('lbl_selengkapnya');?></a>
		        </div>
	        </div>
	     </div>
	    <?php
	    $blog++;
	    }
	    ?>
	</div>
	<?php
		echo ( ! $blog_page)?'':'<div class="pagination">'.$blog_page.'</div>';
	}
	?>
</div>
<?php
}
else {
    echo "<h3>".$this->lang->line('jdl_posting_kosong')."</h3><div class='posRelatif'><p>".$this->lang->line('lbl_posting_kosong')."</p></div>";
}
?>

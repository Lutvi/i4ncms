<style>
pre, code { display:none; }
</style>

<?php
// Error Handling-cek kalau kosong
if ( ! empty($content) && $this->config->item('toko_aktif'))
{
	//pagination
	echo ( ! $produk_page)?'':'<div class="pagination">'.$produk_page.'</div>';
    if ($tipe_produk === 'induk')
    {
		$jml_prod = count($content);
        foreach ($content as $item)
        {
			if ($item->status === 'on')
			{
				$foto = $this->mproduk->getAllImgFrontById($item->id_prod_img);
				$pil_warna = $this->mproduk->getAllFrontWarnaProdukById($item->id_prod);
				$prodrate = ($item->ratting*13);
				if(current_lang(false) === 'id')
				{
					$nama_prod = $item->nama_prod;
					if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi,50);
					else
					$deskripsi = $item->deskripsi;
				}
				else {
					$nama_prod = $item->nama_prod_en;
					if($jml_prod > 1)
					$deskripsi = word_limiter($item->deskripsi_en,50);
					else
					$deskripsi = $item->deskripsi_en;
				}
?>
	<?php if($jml_prod == 1): ?>
	<div class="h-product" itemscope itemtype="http://data-vocabulary.org/Product">
	<?php else: ?>
	<div itemscope itemtype="http://schema.org/Product">
	<div itemid="#<?php echo underscore($nama_prod); ?>" itemscope itemtype="http://schema.org/SomeProducts" class="h-product">
	<?php endif; ?>
			<h1 itemprop="name" class="produk p-name"><?php echo $nama_prod; ?></h1>
			<div class="box-prod-detail ui-helper-clearfix">
		        <div class="box-img-prod-detail">
				<?php if ( ! empty($pil_warna) ): ?>
			        <div class="box-warna-prod-detail tips">
					<?php
			            foreach ( $pil_warna as $warna )
			            {
			                if ( !empty($warna->kode_warna) )
			                {
							$pnama = $this->mproduk->getFrontLabelProdById($warna->id_prod,current_lang(false));
							$lnkwarna = site_url(current_lang().'pdetail/'.underscore($pnama));
					?>
							<a class="pil-warna-detail" style="background:#<?php echo $warna->kode_warna; ?>" title="<?php echo $pnama; ?>" href="<?php echo $lnkwarna; ?>"></a>
					<?php
			                }
			            }
					?>
			        </div>
				<?php endif; ?>

			        <?php if ( count($foto) == 0 ) : ?>
						<meta property='og:image' content='<?php echo base_url().'_produk/thumb/small_thumb_'.$fp->img_src; ?>'/>
			            <div class="box-img-detail targetarea ui-helper-clearfix">
			                <img class="u-photo" itemprop="image" id="multizoom<?php echo $item->id_prod; ?>" alt="zoomable" src="<?php echo base_url().'_produk/small/small_'.$fp->img_src; ?>"/>
			            </div>
					<?php else: ?>
						<meta property='og:image' content='<?php echo base_url().'_produk/thumb/small_thumb_'.$foto[0]->img_src; ?>'/>
			            <div class="box-img-detail targetarea ui-helper-clearfix">
			                <img id="multizoom<?php echo $item->id_prod; ?>" alt="zoomable" src=""/>
			            </div>
					<?php endif; ?>

		            <div class="multizoom<?php echo $item->id_prod; ?> thumbs">
					<?php foreach($foto as $fp) : ?>
						<a href="<?php echo base_url().'_produk/small/small_'.$fp->img_src; ?>" data-large="<?php echo base_url().'_produk/large/large_'.$fp->img_src; ?>">
						<img class="u-photo" itemprop="image" src="<?php echo base_url().'_produk/thumb/small_thumb_'.$fp->img_src; ?>" alt="<?php echo $fp->alt_text_img; ?>" />
						</a>
					<?php endforeach; ?>
		            </div>

		            <div class="box-btn-prod-detail">
						<button class="beli beli-detail" data-url="<?php echo site_url(current_lang().'belanja/beli'); ?>" data-produkid="<?php echo bs_kode($item->id_prod); ?>"  data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($item->harga_spesial) )?$item->harga_spesial:$item->harga_prod); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></button>
		            </div>
		        </div>

				<?php if($jml_prod == 1): ?>
		        <div class="box-deskipsi-detail" itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
		        <meta itemprop="currency" content="IDR" />
		        <?php else: ?>
				<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		        <?php endif; ?>
				<?php
					if ( ! empty($item->harga_spesial) ):
					$diskon = (!empty($item->diskon))?$item->diskon*100:0;
				?>
						<div class="deskripsi-harganormal-detail"><strike><?php echo $this->lang->line('lbl_rp').format_harga_indo($item->harga_prod); ?></strike></div>
						<?php echo $this->lang->line('lbl_harga') .' : ' ;?><?php echo $this->lang->line('lbl_rp'); ?><span itemprop="price"><?php echo format_harga_indo($item->harga_spesial); ?></span>
						<br />
						<span><?php echo $this->lang->line('lbl_diskon') .' : '.$diskon; ?>%</span>
				<?php else: ?>
						<?php echo $this->lang->line('lbl_harga') .' : '.$this->lang->line('lbl_rp'); ?><span itemprop="price"><?php echo format_harga_indo($item->harga_prod); ?></span>
				<?php endif; ?>

				<?php
	                if ( ! empty($item->anak_untuk_jenis) )
	                {
	                    echo '<br />';
	                    echo ucwords($item->anak_untuk_jenis).' : '.ucwords((current_lang(false) =='id')?$item->nama_jenis_anak:$item->nama_jenis_anak_en);
	                }
	
	                if ( empty($item->stok) )
	                {
	                    echo br();
	                    if($jml_prod == 1) {
							echo '<span itemprop="availability" content="out_of_stock">'.$this->lang->line('stok').' : '.$item->stok.'</span>'.' '.config_item('satuan_produk');
	                    }
						else {
							echo '<link itemprop="availability" href="http://schema.org/OutOfStock" />';
							echo $this->lang->line('stok').' : '.$item->stok.' '.config_item('satuan_produk');
						}
	                    echo br();
	                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
	                }
	                else {
						echo br();
						if($jml_prod == 1) {
							echo '<span itemprop="availability" content="in_stock">'.$this->lang->line('stok').' : <span itemprop="quantity">'.$item->stok.'</span></span>'.' '.config_item('satuan_produk');
							echo br();
							echo '<span itemprop="condition" content="new">Brand new!</span>';
						}
						else {
							echo '<link itemprop="availability" href="http://schema.org/InStock" />';
							echo $this->lang->line('stok').' : '.$item->stok.' '.config_item('satuan_produk');
						}
	                }
				?>
		        </div>
		        <div itemprop="description" class="u-details"><?php echo $deskripsi; ?></div>

		        <?php if($jml_prod == 1): ?>
					<div class="grate" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                       <span class="fill" style="width:<?php echo $prodrate; ?>px"></span>
                       <br />
                       <?php echo $this->lang->line('lbl_rating'); ?> <span itemprop="ratingValue"><?php echo $item->ratting; ?></span>/5 <?php echo $this->lang->line('lbl_nrating'); ?> <span itemprop="reviewCount"><?php echo $item->votes; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
                    </div>
				<?php else: ?>
					<div class="grate" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                       <span class="fill" style="width:<?php echo $prodrate; ?>px"></span>
                       <br />
                       <?php echo $this->lang->line('lbl_rating'); ?> <span itemprop="ratingValue"><?php echo $item->ratting; ?></span>/5 <?php echo $this->lang->line('lbl_nrating'); ?> <span itemprop="reviewCount"><?php echo $item->votes; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
                    </div>
				<?php endif; ?>

	            <?php if($jml_prod > 1) : ?>
					<a class="u-url" href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
				<?php endif; ?>
		    </div>
	<?php if($jml_prod == 1): ?>
    <!--Rating-->
    <form id="ratingForm">
        <input type="hidden" name="rt" value="<?php echo bs_kode($item->id_prod); ?>" />
        <input type="hidden" name="tipe" value="produk" />
        <?php echo $this->lang->line('lbl_beriPeringkat'); ?> :
        <div class="starRating">
          <div>
            <div>
              <div>
                <div>
                  <input id="rating1" type="radio" name="rating" value="1">
                  <label for="rating1"><span>1</span></label>
                </div>
                <input id="rating2" type="radio" name="rating" value="2">
                <label for="rating2"><span>2</span></label>
              </div>
              <input id="rating3" type="radio" name="rating" value="3">
              <label for="rating3"><span>3</span></label>
            </div>
            <input id="rating4" type="radio" name="rating" value="4">
            <label for="rating4"><span>4</span></label>
          </div>
          <input id="rating5" type="radio" name="rating" value="5">
          <label for="rating5"><span>5</span></label>
         </div>
      </form>
      <div id="ratingRes">
      	<?php echo $this->lang->line('lbl_upPeringkat'); ?> <span id="ratingNrat"></span> <?php echo $this->lang->line('lbl_nrating'); ?> <span id="ratingNvot"></span> <?php echo $this->lang->line('lbl_jvote'); ?>
      </div>
      <div class="btsGarisBiru"></div>
    <!--End Rating-->
    
	</div>
	<?php else: ?>
	</div>
	</div>
	<?php endif; ?>
			<pre>
				<code>
				$('#multizoom<?php echo $item->id_prod; ?>').addimagezoom({ 
					speed: 1000,
					imagevertcenter: true,
					magvertcenter: true,
					zoomrange: [2,3.5],
					magnifiersize: [250,250],
					magnifierpos: 'left',
					cursorshadecolor: '#fdffd5',
					cursorshade: true 
				});
				</code>
			</pre>
<?php
			}
			else {
				echo "<h3>".$this->lang->line('produk_tidak_ada')."</h3>";
				echo "<p>".$this->lang->line('info_produk_tidak_ada')."</p>";
			}
        }
    }
    else {
?>
	<div class="grid">
	<div itemscope itemtype="http://schema.org/Product" class="unit">
    <?php
        $i=0;
        foreach($content as $row)
        {
			$i++;
			if ( $row['status'] === 'on')
			{
				$pil_warna = $this->mproduk->getAllFrontWarnaProdukById($row['id_prod']);
				$foto = $this->mproduk->getAllImgFrontById($row['id_prod_img']);
				$prodrate = ($row['ratting']*13);
				if(current_lang(false) === 'id')
				{
					$nama_prod = $row['nama_prod'];
					$desc = strip_tags($row['deskripsi'],"<p><a><br><strong><span><font><ul><li><ol>");
                }
                else {
					$nama_prod = $row['nama_prod_en'];
					$desc = strip_tags($row['deskripsi_en'],"<p><a><br><strong><span><font><ul><li><ol>");
                }
?>
	<div itemid="#<?php echo underscore($nama_prod); ?>" itemscope itemtype="http://schema.org/SomeProducts" <?php echo ( count($content) > 1 )?'class="one-of-two h-product"':'style="margin-left:20px"'; ?>>
        <h1 itemprop="name" class="produk p-name"><?php echo character_limiter($nama_prod,25); ?></h1>
        <div class="box-prod-list">
            <?php if ( !empty($pil_warna) ): ?>
				<div class="box-warna-prod-list tips">
				<?php     
				foreach ( $pil_warna as $warna )
				{
					if (!empty($warna->kode_warna))
					{
					$pnama = $this->mproduk->getFrontLabelProdById($warna->id_prod,current_lang(false));
					$lnkwarna = site_url(current_lang().'pdetail/'.underscore($pnama));
			?>
					<a class="pil-warna-detail" style="background:#<?php echo $warna->kode_warna; ?>" title="<?php echo $pnama; ?>" href="<?php echo $lnkwarna; ?>"></a>
				<?php
					}
				}
				?>
				</div>
            <?php endif; ?>

            <?php if ( count($foto) > 1 ): ?>
            <div class="nav-img-list">
                <span class="ui-icon ui-icon-triangle-1-w" style="cursor:pointer; float:left;" id="prev-<?php echo $i.$row['id_prod']; ?>">&lt;|</span>
                <span class="klik-info-list">.<?php echo $this->lang->line('btn_klik_foto_prod'); ?>.</span>
                <span class="ui-icon ui-icon-triangle-1-e" style="cursor:pointer; float:right;" id="next-<?php echo $i.$row['id_prod']; ?>"></span>
            </div>
            <?php else: ?>
            <div class="nav-img-list">
                <span class="klik-info-list">.....<?php echo $this->lang->line('btn_klik_foto_prod'); ?>.....</span>
            </div>
            <?php endif; ?>
            
            <div id="thumb-produk-<?php echo $i.$row['id_prod']; ?>" class="thumbProduk"> 
            <?php foreach($foto as $fp): ?>
                <a id="fancybox-manual-<?php echo $fp->id_img.$row['id_prod']; ?>" href="javascript:;" title="<?php echo $nama_prod; ?>">
                <img class="u-photo" itemprop="image" src="<?php echo base_url().'_produk/thumb/thumb_'.$fp->img_src; ?>" alt="<?php echo $fp->alt_text_img; ?>" />
                </a>
                <pre style="display:none;">
                    <code style="display:none;">
                        $("#fancybox-manual-<?php echo $fp->id_img.$row['id_prod']; ?>").fancybox({
                            href : '<?php echo base_url().'_produk/medium/medium_'.$fp->img_src; ?>',
                            transitionOut: 'elastic',
                            transitionIn: 'elastic',
                            centerOnScroll: true
                        });
                    </code>
                </pre>
            <?php endforeach; ?>
            </div>
            
            <div class="box-pdetail-list" style="padding:10px">
				<div class="boxOffer" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <?php 
                if ( !empty($row['harga_spesial']) )
                {
					$diskon = (!empty($row['diskon']))?$row['diskon']*100:0;
				?>
				<span class="deskripsi-harganormal-list"><strike><?php echo $this->lang->line('lbl_rp').format_harga_indo($row['harga_prod']); ?></strike></span>
				<br />
				<?php echo $this->lang->line('lbl_harga') .' : '.'<span itemprop="price">'.$this->lang->line('lbl_rp').format_harga_indo($row['harga_spesial']).'</span>'; ?>
				<br />
				<?php echo $this->lang->line('lbl_diskon') .' : '.$diskon; ?>%
            <?php
                }
                else {
					echo $this->lang->line('lbl_harga') .' : '.'<span itemprop="price">'.$this->lang->line('lbl_rp').format_harga_indo($row['harga_prod']).'</span>';
                }

                if ( ! empty($row['anak_untuk_jenis']) )
                {
                    echo '<br />';
                    echo ucwords($row['anak_untuk_jenis']).' : '.ucwords((current_lang(false) =='id')?$row['nama_jenis_anak']:$row['nama_jenis_anak_en']);
                }
                
                if ( empty($row['stok']) )
                {
                    echo br();
                    echo '<span>'.$this->lang->line('stok'). ': '.$row['stok'].'</span>'.' '.config_item('satuan_produk');
                    echo br();
                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
                }
                else {
					echo br();
					echo '<link itemprop="availability" href="http://schema.org/InStock" />';
					echo '<span>'.$this->lang->line('stok'). ': '.$row['stok'].'</span>'.' '.config_item('satuan_produk');
                }
            ?>
            </div>
            <div class="grate" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
               <span class="fill" style="width:<?php echo $prodrate; ?>px"></span>
               <br />
               <?php echo $this->lang->line('lbl_rating'); ?> <span itemprop="ratingValue"><?php echo $row['ratting']; ?></span>/5 <?php echo $this->lang->line('lbl_nrating'); ?> <span itemprop="reviewCount"><?php echo $row['votes']; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
            </div>

            <div class="box-btn-prod-list">
				<button style="margin-top:15px" class="beli beli-list" data-url="<?php echo site_url(current_lang().'belanja/beli'); ?>" data-produkid="<?php echo bs_kode($row['id_prod']); ?>" data-nama="<?php echo $nama_prod; ?>" data-harga="<?php echo bs_kode(( !empty($row['harga_spesial']) )?$row['harga_spesial']:$row['harga_prod']); ?>"><?php echo $this->lang->line('masuk_keranjang'); ?></button>
            </div>

            <span itemprop="description"><?php echo word_limiter($desc,38); ?></span>
            <br />
            <a class="u-url" href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
        </div>
	    </div>
            <pre>
                <code>
                function onAfter<?php echo $i.$row['id_prod']; ?>(curr, next, opts) {
                    var index = opts.currSlide;
                    $('#prev-<?php echo $i.$row['id_prod']; ?>')[index == 0 ? 'hide' : 'show']();
                    $('#next-<?php echo $i.$row['id_prod']; ?>')[index == opts.slideCount - 1 ? 'hide' : 'show']();
                }

                $('#thumb-produk-<?php echo $i.$row['id_prod']; ?>').cycle({
                    fx:     'uncover',
                    slideExpr: 'img',
                    slideResize: 0,
                    prev:   '#prev-<?php echo $i.$row['id_prod']; ?>',
                    next:   '#next-<?php echo $i.$row['id_prod']; ?>',
                    after:   onAfter<?php echo $i.$row['id_prod']; ?>, 
                    timeout:  0,
                    easing:  'easeInOutBack'
                });
                </code>
            </pre>
<?php
			}
			else {
				echo "<h3>".$this->lang->line('produk_tidak_ada')."</h3>";
				echo "<p>".$this->lang->line('info_produk_tidak_ada')."</p>";
			}
?>
		</div>
<?php
        }
?>
	</div>
	</div>
<?php
    }
	echo ( ! $produk_page)?'':'<div class="pagination">'.$produk_page.'</div>';
}
else {
?>
    <h3><?php echo $this->lang->line('produk_tidak_ada'); ?></h3>
    <p><?php echo $this->lang->line('info_produk_tidak_ada'); ?></p>
<?php
} // end-cek toko aktif
?>

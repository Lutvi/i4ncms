
<script type="text/javascript">
$(document).ready(function() {
	$('ul.img-thumbs li').hover(
		function(){$(".overlay", this).stop().animate({top:'0px'}, {queue:false,duration:300});}, 
		function(){$(".overlay", this).stop().animate({top:'195px'},{queue:false,duration:300});}
	);
});
</script>
<?php
$whl = "#1E90FF;text-decoration:underline;"; //warna hightlight
?>
<h3><?php echo $title; ?></h3>
<div class="posRelatif">
<?php if( count($produk) > 0 && $this->config->item('toko_aktif')): ?>
<a href="<?php echo site_url(current_lang().'pdetail'); ?>">
<h2><?php echo $this->lang->line('lbl_produk'); ?></h2>
</a>
	<div class="grid">
    <div itemscope itemtype="http://schema.org/Product" class="unit">
    <?php
        $i=0;
        foreach($produk as $row)
        {
			$i++;
			$pil_warna = $this->mproduk->getAllFrontWarnaProdukById($row->id_prod);
			$foto = $this->mproduk->getAllImgFrontById($row->id_prod_img);
			$prodrate = ($row->ratting*13);
			if(current_lang(false) === 'id')
			{
				$nama_prod = $row->nama_prod;
				$desc = word_limiter(strip_tags($row->deskripsi,"<p><a><br><strong><span><font><ul><li><ol>"),55);
			}
			else {
				$nama_prod = $row->nama_prod_en;
				$desc = word_limiter(strip_tags($row->deskripsi_en,"<p><a><br><strong><span><font><ul><li><ol>"),55);
			}

			if(strlen($this->session->userdata('key_cari_front')) > 4)
			$desc = highlight_phrase($desc, $this->session->userdata('key_cari_front'), '<span style="color:'.$whl.'">', '</span>');
?>
	    <div itemid="#<?php echo underscore($nama_prod); ?>" itemscope itemtype="http://schema.org/SomeProducts" <?php echo ( count($produk) > 1 )?'class="one-of-two h-product"':'style="margin-left:20px"'; ?>>

        <a href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><h1 itemprop="name" class="produk p-name"><?php echo $nama_prod; ?></h1></a>
        <div class="box-prod-list" style="min-width: 380px;">
            <?php if ( !empty($pil_warna) ): ?>
				<div class="box-warna-prod-list tips">
				<?php     
				foreach ( $pil_warna as $warna )
				{
					if (!empty($warna->kode_warna))
					{
				?>
					<a class="pil-warna-list" style="background:#<?php echo $warna->kode_warna; ?>" title="<?php echo $warna->id_prod; ?>" href="#warnaProduk-<?php echo $warna->id_prod; ?>"></a>
				<?php
					}
				}
				?>
				</div>
            <?php endif; ?>

            <?php if ( count($foto) > 1 ): ?>
            <div class="nav-img-list">
                <span class="ui-icon ui-icon-triangle-1-w" style="cursor:pointer; float:left;" id="prev-<?php echo $i.$row->id_prod; ?>">&lt;|</span>
                <span class="klik-info-list">.<?php echo $this->lang->line('btn_klik_foto_prod'); ?>.</span>
                <span class="ui-icon ui-icon-triangle-1-e" style="cursor:pointer; float:right;" id="next-<?php echo $i.$row->id_prod; ?>"></span> 
            </div>
            <?php else: ?>
            <div class="nav-img-list">
                <span class="klik-info-list">.....<?php echo $this->lang->line('btn_klik_foto_prod'); ?>.....</span>
            </div>
            <?php endif; ?>
            
            <div id="thumb-produk-<?php echo $i.$row->id_prod; ?>" class="thumbProduk"> 

            <?php foreach($foto as $fp): ?>
                <a id="fancybox-manual-<?php echo $fp->id_img; ?>" href="javascript:return false;" title="<?php echo $nama_prod; ?>">
                <img class="u-photo" itemprop="image" src="<?php echo base_url().'_produk/thumb/thumb_'.$fp->img_src; ?>" alt="<?php echo $fp->alt_text_img; ?>" />
                </a>
                <pre class="iskrip">
                    <code>
                        $("#fancybox-manual-<?php echo $fp->id_img; ?>").fancybox({
                            href : '<?php echo base_url().'_produk/medium/medium_'.$fp->img_src; ?>',
                            transitionOut: 'elastic',
                            transitionIn: 'elastic',
                            centerOnScroll: true
                        });
                    </code>
                </pre>
            <?php endforeach; ?>
            </div>
            <div class="box-pdetail-list" style="padding:10px">
				<div class="boxOffer" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <?php
                if ( !empty($row->harga_spesial) )
                {
					$diskon = (!empty($row->diskon))?$row->diskon*100:0;
				?>
					<span class="deskripsi-harganormal-list"><strike><?php echo $this->lang->line('lbl_rp').format_harga_indo($row->harga_prod); ?></strike></span>
					<br />
					<?php echo $this->lang->line('lbl_harga') .' : '.'<span itemprop="price">'.$this->lang->line('lbl_rp').format_harga_indo($row->harga_spesial).'</span>'; ?>
					<br />
					<?php echo $this->lang->line('lbl_diskon') .' : '.$diskon; ?>%
            <?php
                }
                else {
					echo $this->lang->line('lbl_harga') .' : '.'<span itemprop="price">'.$this->lang->line('lbl_rp').format_harga_indo($row->harga_prod).'</span>';
                }
                if ( ! empty($row->anak_untuk_jenis) )
                {
                    echo '<br />';
                    echo ucwords($row->anak_untuk_jenis).' : '.ucwords((current_lang(false) =='id')?$row->nama_jenis_anak:$row->nama_jenis_anak_en);
                }
                if ( empty($row->stok) )
                {
                    echo '<br />';
                    echo '<span>'.$this->lang->line('stok'). ': '.$row->stok.'</span>';
                    echo '<br />';
                    echo '<a href="#">'.$this->lang->line('ajukan_permintaan').'</a>';
                }
                else {
					echo '<link itemprop="availability" href="http://schema.org/InStock" />';
                }
            ?>
				</div>
				<div class="grate" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
				   <span class="fill" style="width:<?php echo $prodrate; ?>px"></span>
                   <br />
                   <?php echo $this->lang->line('lbl_rating'); ?> <span itemprop="ratingValue"><?php echo $row->ratting; ?></span>/5 <?php echo $this->lang->line('lbl_nrating'); ?> <span itemprop="reviewCount"><?php echo $row->votes; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
				</div>
	            <p itemprop="description" class="u-details" align="justify"><?php echo $desc; ?></p>
	            <link itemprop="url" href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>" />
	            <a class="u-url" href="<?php echo site_url(current_lang().'pdetail/'.underscore($nama_prod)); ?>"><?php echo $this->lang->line('detail_produk'); ?></a>
	        </div>
	        </div>
            <pre class="iskrip">
                <code>
                function onAfter<?php echo $i.$row->id_prod; ?>(curr, next, opts) {
                    var index = opts.currSlide;
                    $('#prev-<?php echo $i.$row->id_prod; ?>')[index == 0 ? 'hide' : 'show']();
                    $('#next-<?php echo $i.$row->id_prod; ?>')[index == opts.slideCount - 1 ? 'hide' : 'show']();
                }

                $('#thumb-produk-<?php echo $i.$row->id_prod; ?>').cycle({
                    fx:     'uncover',
                    slideExpr: 'img',
                    slideResize: 0,
                    prev:   '#prev-<?php echo $i.$row->id_prod; ?>',
                    next:   '#next-<?php echo $i.$row->id_prod; ?>',
                    after:   onAfter<?php echo $i.$row->id_prod; ?>, 
                    timeout:  0,
                    easing:  'easeInOutBack'
                });
                </code>
            </pre>
		</div>
<?php
        }
?>
	</div>
	</div>
	<div class="clKiri"></div>
	<hr>
<?php endif; ?>


<?php if( count($blog) > 0): ?>
<a href="<?php echo site_url(current_lang().'posting'); ?>">
	<h2><?php echo $this->lang->line('lbl_blog'); ?></h2>
</a>
<div itemscope itemtype="http://schema.org/Blog">
	<?php
    $iblog = 1;
    foreach ($blog as $row)
    {
		if (current_lang(false) === 'id')
		{
			$judul = humanize($row->judul_id);
			$isi = word_limiter($row->isi_id, 55);
		}
		else {
			$judul = humanize($row->judul_en);
			$isi = word_limiter($row->isi_en, 55);
		}
		$blgrate = ($row->ratting*13);

		if(strlen($this->session->userdata('key_cari_front')) > 4)
		$isi = highlight_phrase($isi, $this->session->userdata('key_cari_front'), '<span style="color:'.$whl.'">', '</span>');
	?>
	<div itemprop="blogPosts" class="box-isi h-entry" itemscope itemtype="http://schema.org/BlogPosting">
		<div class='innerkontent'>
			<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, strtotime($row->tgl_terbit)); ?>"/>
			<a class="u-url" itemprop="url" href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
				<h4 class="judulBlog p-name" itemprop="name"><?php echo $judul; ?></h4>
			</a>
			<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_revisi); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_revisi); ?></time>
            <div class="grate w64 fright mrgTopMin5">
                <span class="fill" style="width:<?php echo $blgrate; ?>px"></span>
            </div>
			<a class="u-url" itemprop="url" href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>">
				<img itemprop="thumbnailUrl" class="efek u-photo fotoBlog" alt="<?php echo $judul;?>" src="<?php echo '/_media/blog/thumb/thumb_'.$row->gambar_cover;?>" />
			</a>
			<div class="e-content" itemprop="articleBody">
				<?php echo $isi; ?>
	        </div>

	        <link href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>" />
			<a class="u-url" href="<?php echo site_url(current_lang().'posting/'.underscore($judul)); ?>"><?php echo $this->lang->line('lbl_selengkapnya');?></a>
        </div>
    </div>
	<?php
	    $iblog++;
    }
	?>
    </div>
    <div class="btslist"></div>
    <hr>
<?php endif; ?>

<?php if( count($album) > 0 ): ?>
<a href="<?php echo site_url(current_lang().'gallery'); ?>">
<h2><?php echo $this->lang->line('lbl_album'); ?></h2>
</a>
<ul class="img-thumbs">
<?php
    foreach ($album as $row)
    {
    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($row->id);
?>
	<li class="h-entry" itemscope itemtype="http://schema.org/ImageObject">
	<a style="cursor:default;" href="javascript:void(0);"><img itemprop="thumbnail" class="u-photo fotoAlbum" width="300" src="<?php echo '/_media/album/small/small_'.$row->gambar_cover;?>" /></a>
	<div class="overlay">
		<?php
		if (current_lang(false) === 'id')
		{
			$nama = $row->nama_id;
			$isi_album = word_limiter($row->ket_id, 35);
		} else {
			$nama = $row->nama_en;
			$isi_album = word_limiter($row->ket_en, 35);
		}
		$albrate = ($row->ratting*13);

		if(strlen($this->session->userdata('key_cari_front')) > 4)
		$isi_album = highlight_phrase($isi_album, $this->session->userdata('key_cari_front'), '<span style="color:'.$whl.'">', '</span>');
		?>
		<link itemprop="url" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>" />
		<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>" />
		<h1 class="p-name" itemprop="name"><?php echo humanize($nama); ?></h1>
		<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_update); ?></time>
        <div class="grate w64 fright mrgTopMin5">
            <span class="fill" style="width:<?php echo $albrate; ?>px"></span>
        </div>
		<p class="p-summary" itemprop="description"><?php echo humanize(strip_tags($isi_album)); ?></p>
		<p>
		<?php if($cnt_gallery > 0): ?>
		<a class="u-url" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>">Gallery (<?php echo $cnt_gallery; ?>)</a>
		<?php else: ?>
		<a class="u-url" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>"><?php echo $this->lang->line('lbl_selengkapnya');?></a>
		<?php endif; ?>
		</p>
		<a class="zoom" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>" rel="" title="<?php echo humanize($nama); ?>"></a>
	</div>
	</li>
	<?php
	}
	?>
</ul>
<hr>
<?php endif; ?>


<?php if( count($video) > 0 ): ?>
<a href="<?php echo site_url(current_lang().'playlist'); ?>">
<h2><?php echo $this->lang->line('lbl_video'); ?></h2>
</a>
<div class="wrap-efek wrapBoxVideo">
	<?php
    $jml = 1;
    foreach ($video as $row)
    {
		$cnt_playlist = $this->cms->getFrontCountVideoPlaylist($row->id);
		$vidrate = ($row->ratting*13);
		if (current_lang(false) === 'id')
		{
			$nama = humanize($row->nama_id);
			$ket = word_limiter(strip_tags($row->ket_id), 80);
		} else {
			$nama = humanize($row->nama_en);
			$ket = word_limiter(strip_tags($row->ket_en), 80);
		}

		if(strlen($this->session->userdata('key_cari_front')) > 4)
		$ket = highlight_phrase($ket, $this->session->userdata('key_cari_front'), '<span style="color:'.$whl.'">', '</span>');
	?>
	<div class="h-entry" itemscope itemtype="http://schema.org/VideoObject">
		<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>" />
		<a itemprop="url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
		<h4 itemprop="name" class="tLeft p-name"><?php echo $nama; ?></h4>
		</a>
		<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_update); ?></time>
        <div class="grate w64 fright mrgTopMin5">
            <span class="fill" style="width:<?php echo $vidrate; ?>px"></span>
        </div>
		<a itemprop="url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
		<img itemprop="thumbnail" class="efek fotoVideo" src="<?php echo '/_media/videos/small/small_'.$row->gambar_video;?>" />
		</a>
        <div class="tajust" id="tampil-<?php echo $jml;?>">
			<p class="p-summary" itemprop="description"><?php echo strip_tags($ket); ?></p>
            <p>
            <?php if($cnt_playlist > 0): ?>
            <a class="u-url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Playlist (<?php echo $cnt_playlist; ?>)</a>
            <?php else: ?>
			<a class="u-url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Selengkapnya....</a>
            <?php endif; ?>
            </p>
        </div>
        <?php if($jml < count($video)): ?>
         <div class="noPlaylist"></div>
        <?php endif; ?>
    </div>
<?php
		$jml++;
    }
?>
</div>
<?php endif; ?>

</div><!-- End Kontainer-->

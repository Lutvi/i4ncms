<?php if ( isset($tag_konten) && ! empty($tag_konten['rs_kat'])): ?>
	<div class="tag-wrap" style="margin-bottom:10px">
	<h6><?php echo $this->lang->line('lbl_tag');?></h6>
	<?php foreach($tag_konten['rs_kat'] as $tag): ?>
	<?php
		if (current_lang(false) === 'id')
		{
			$nama = $tag->label_id;
			$lbl = 'tag/';
		}
		else {
			$nama = $tag->label_en;
			$lbl = 'tag/';
		}

		$link_url = base_url().current_lang().$lbl.$tipe.'/'.underscore($nama).'.html';
	?>
		<a rel="tag" class="tag-pill" href="<?php echo $link_url; ?>" style="5px"><span itemprop="keywords"><?php echo $nama; ?></span></a>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

<script>
$(document).ready(function() {
	/*
	 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
	*/
	function formatTitle(a, b, c, d) {
		id = (c + 1);
		return '<span id="fancybox-title-over">' + $('#tampil-' + id).html() + '</span>'
	}
	$('.fancybox-media')
	.attr('rel', 'video_group')
	.fancybox({
	    width:640,
		height:360,
		'transitionIn': 'none',
		'transitionOut': 'none',
		'titlePosition' : 'inside',
		'hideOnOverlayClick':false,
		'hideOnContentClick':false,
		'titleFormat': formatTitle,
		'showNavArrows' : false,
	    'type': 'iframe',
	    iframe : {
	      preload: true
	    },
	    'onStart': function () {
								/*$('#z-video').slideUp('medium');
								$('#fancybox-wrap').css({'margin-top':'50px !important'});*/ },
	    'onClosed': function () {
								/*$('#z-video').slideDown('medium');
								$('#fancybox-wrap').css({'position':'fixed','top':'20px'});*/ }
	});
});
</script>

<?php
if ( ! empty($video) )
{
?>
<style type="text/css">
div#fancybox-wrap {position:fixed; top:20px !important;}
span#fancybox-title-over {background:none;padding:0;max-height:200px;overflow:auto}
</style>
<?php if(!empty($content)): ?>
	<h1 itemprop="name" class="video p-name"><?php echo $title; ?></h1>
	<p class="p-summary" itemprop="description" align="justify">
		<?php echo $content; ?>
	</p>
<?php endif; ?>

	<?php
	if ($per_page == 1)
	{
	?>
	<div class="wrap-efek h-entry" itemscope itemtype="http://schema.org/VideoObject">
	<?php
	    $jml = 1;
	    foreach ($video as $rvideo)
	    {
			$cnt_video = $this->cms->getFrontCountVideoPlaylist($rvideo->id);
			$vidrate = ($rvideo->ratting*13);
			if (current_lang(false) === 'id') {
				$nama = humanize($rvideo->nama_id);
				$ket = $rvideo->ket_id;
			}
			else {
				$nama = humanize($rvideo->nama_id);
				$ket = $rvideo->ket_en;
			}

			if($rvideo->tipe_video === 'youtube')
			$vembed = youtube_embed($rvideo->src_video,640,360,TRUE);
			else
			$vembed = vimeo_embed($rvideo->src_video,640,360);
	?>

		<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, $rvideo->tgl_update); ?>" />
		<h1 class="album p-name" itemprop="name"><?php echo humanize($nama); ?></h1>
		<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $rvideo->tgl_update); ?>"><?php echo formatTanggal(current_lang(false), $rvideo->tgl_update); ?></time>

    <!-- SosMed -->
		<meta itemprop="interactionCount" content="UserTweets:1203"/>
		<meta itemprop="interactionCount" content="UserComments:78"/>
		<div itemscope="" itemtype="http://data-vocabulary.org/Review-aggregate" class="fright">
        	<div class="grate w64 mrgBot8">
                <span class="fill" style="width:<?php echo $vidrate; ?>px"></span>
            </div>
			<span itemprop="itemreviewed"><?php echo $this->lang->line('lbl_rating'); ?> </span>
			<span itemprop="rating" itemscope="" itemtype="http://data-vocabulary.org/Rating">
				<span itemprop="average"><?php echo $rvideo->ratting; ?></span>/<span itemprop="best">5</span>
			</span>
			<?php echo $this->lang->line('lbl_nrating'); ?> total <span itemprop="votes"><?php echo $rvideo->votes; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
		</div>
        <div class="clKanan"></div>
    <!-- End SosMed -->
		
		<div id="z-video">
			<?php echo $vembed; ?>
		</div>

        <div class="p-summary" itemprop="description" class="taleft">
            <?php echo $ket; ?>
        </div>

		<?php if($cnt_video > 0): ?>
		<h4>Playlist (<?php echo $cnt_video; ?>)</h4>
		<?php endif; ?>
	    <?php
			if($cnt_video > 0)
			{
				$jml = 1;
				$playlist = $this->cms->getAllFrontVideoPlaylist($rvideo->id,$cnt_video,0);
				foreach ($playlist as $row){
	
				if($row->tipe_video === 'youtube')
				$url = youtube_fullvideo($row->src_video);
				else
				$url = vimeo_fullvideo($row->src_video);
	
				// ket
				if (current_lang(false) === 'id')
				{
					$jdl = humanize($row->nama_id);
					$ket = $row->ket_id;
				} else {
					$jdl = humanize($row->nama_en);
					$ket = $row->ket_en;
				}
				
				?>
					<a title="<?php echo $jdl;?>" class="fancybox-media" rel="video_group" href="<?php echo $url;?>"><img class="efek fotoVideo u-photo" width="340" alt="<?php echo $jdl;?>" src="<?php echo base_url().'/_media/videos/medium/'.$row->gambar_video;?>" />
					</a>
					<div id="tampil-<?php echo $jml;?>" style="display:none;background:#E5E5E5">
						<?php
						echo '<h4>'.$jdl.'</h4><div style="text-align:left">'.$ket.'</div>';
						?>
					</div>
	
				<?php
				$jml++;
				}
			}
		$jml++;
	    }
	?>
<!-- Komponen-Bawah -->
    <div class="clKiri"></div>
    <?php $this->load->view_theme('tag_box', '', $this->config->item('theme_id').'/partial/'); ?>
    <div class="clKiri"></div>
    <!--Rating-->
    <div id="ratingBox">
        <div class="mrgTop20">
            <?php echo $this->lang->line('lbl_rating'); ?> <span><?php echo $rvideo->ratting;?></span>/<span>5</span>
            <?php echo $this->lang->line('lbl_nrating'); ?> <span><?php echo $rvideo->votes;?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
        </div>
    </div>
    <form id="ratingForm">
        <input type="hidden" name="rt" value="<?php echo bs_kode($rvideo->id); ?>" />
        <input type="hidden" name="tipe" value="video" />
        <?php echo $this->lang->line('lbl_beriPeringkat'); ?> :
        <div class="starRating">
          <div>
            <div>
              <div>
                <div>
                  <input id="rating1" type="radio" name="rating" value="1">
                  <label for="rating1"><span>1</span></label>
                </div>
                <input id="rating2" type="radio" name="rating" value="2">
                <label for="rating2"><span>2</span></label>
              </div>
              <input id="rating3" type="radio" name="rating" value="3">
              <label for="rating3"><span>3</span></label>
            </div>
            <input id="rating4" type="radio" name="rating" value="4">
            <label for="rating4"><span>4</span></label>
          </div>
          <input id="rating5" type="radio" name="rating" value="5">
          <label for="rating5"><span>5</span></label>
         </div>
      </form>
      <div id="ratingRes">
      	<?php echo $this->lang->line('lbl_upPeringkat'); ?> <span id="ratingNrat"></span> <?php echo $this->lang->line('lbl_nrating'); ?> <span id="ratingNvot"></span> <?php echo $this->lang->line('lbl_jvote'); ?>
      </div>
      <div class="btsGarisBiru"></div>
    <!--End Rating-->
	  <?php
	  	//load iklan bawah
	    $this->load->view_theme('iklan_konten_bawah', '', $this->config->item('theme_id').'/partial/iklan/');
	  ?>
      <?php if($rvideo->diskusi === 'on'): ?>
	    <div id="disqusBox"><?php echo $this->disqus->get_html(); ?></div>
      <?php endif; ?>
<!-- End Komponen-Bawah -->
	</div>
	<?php
	}
	else {

	echo ( ! $video_page)?'':'<div class="pagination">'.$video_page.'</div>';
	?>

	<div class="wrap-efek wrapBoxVideo">
	<?php
	    $jml = 1;
	    foreach ($video as $row)
	    {
		    $cnt_playlist = $this->cms->getFrontCountVideoPlaylist($row->id);
			$vidrate = ($row->ratting*13);
            if (current_lang(false) === 'id')
			{
				$nama = humanize($row->nama_id);
				$ket = word_limiter(strip_tags($row->ket_id), 70);
			} else {
				$nama = humanize($row->nama_en);
				$ket = word_limiter(strip_tags($row->ket_en), 70);
			}
	?>
		<div class="h-entry" itemscope itemtype="http://schema.org/VideoObject">
			<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>" />
			<a itemprop="url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
				<h4 itemprop="name" class="tLeft p-name"><?php echo $nama; ?></h4>
			</a>
			<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_update); ?></time>
            <div class="grate w64 fright mrgTopMin5">
                <span class="fill" style="width:<?php echo $vidrate; ?>px"></span>
            </div>
			<a itemprop="url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">
				<img itemprop="thumbnail" class="efek fotoVideo" src="<?php echo base_url().'/_media/videos/small/small_'.$row->gambar_video;?>" />
			</a>
	        <div style="text-align:left;" id="tampil-<?php echo $jml;?>">
				<p class="p-summary" itemprop="description"><?php echo strip_tags($ket); ?></p>
	            <p>
		            <?php if($cnt_playlist > 0): ?>
		            <a class="u-url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>">Playlist (<?php echo $cnt_playlist; ?>)</a>
		            <?php else: ?>
					<a class="u-url" href="<?php echo site_url(current_lang().'playlist/'.underscore($nama)); ?>"><?php echo $this->lang->line('lbl_selengkapnya');?></a>
		            <?php endif; ?>
	            </p>
	        </div>
	        <?php if($jml < count($video)): ?>
             <div class="noPlaylist"></div>
            <?php endif; ?>
	    </div>
	    <?php
	    $jml++;
	    }
	    ?>
	    </div>
	<?php
		echo ( ! $video_page)?'':'<div class="pagination">'.$video_page.'</div>';
	}
}
else {
	echo "<h3>".$this->lang->line('jdl_video_kosong')."</h3><div class='posRelatif'><p>".$this->lang->line('lbl_video_kosong')."</p></div>";
}
?>


<div id="pemesanan-box">
	<h3><?php echo $title; ?></h3>

	<?php if(isset($info) && !empty($info)): ?>
	<div id="info-bayar-box">
	<?php foreach($info as $key => $isi): ?>
	<div  class="info"><?php echo $isi; ?></div>
	<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if($this->cart->total_items() > 0): ?>

	<?php
		$attr = array( 'id' => 'pesan');
		echo form_open(current_lang().'pemesanan/cek_pesan',$attr); 
	?>

	<fieldset class="ui-widget ui-widget-content ui-corner-all">
	<legend class="ui-widget ui-widget-header ui-corner-all"><?php echo $this->lang->line('lbl_data_pengiriman'); ?></legend>
	<div class="clft">
	<label for="nama_cust"><?php echo $this->lang->line('lbl_nama_penerima'); ?><br><span class="small"><?php echo $this->lang->line('lbl_info_nama_penerima'); ?></span> </label>
	<input type="text" name="nama_cust" id="nama_cust" maxlength="150" value="<?php echo set_value('nama_cust',$nama); ?>" />
	<?php echo form_error('nama_cust'); ?>
	</div>

	<div class="clft">
	<label for="no_telp"><?php echo $this->lang->line('lbl_no_tlp_penerima'); ?><br><span class="small"><?php echo $this->lang->line('lbl_info_no_tlp_penerima'); ?></span> </label>
	<input type="text" name="no_telp" id="no_telp" maxlength="14" value="<?php echo set_value('no_telp',$tlp); ?>" />
	<?php echo form_error('no_telp'); ?>
	</div>

	<div class="clft">
	<label for="email"><?php echo $this->lang->line('lbl_email_penerima'); ?><br><span class="small"><?php echo $this->lang->line('lbl_info_email_penerima'); ?></span> </label>
	<input type="text" name="email" id="email" maxlength="150" value="<?php echo set_value('email', $email); ?>" />
	<?php echo form_error('email'); ?>
	</div>

	<div class="clft">
	<label for="negara"><?php echo $this->lang->line('lbl_negara_tujuan'); ?> <br><span class="small"><?php echo $this->lang->line('lbl_info_negara_tujuan'); ?></span> </label>
	<select id="negara" name="negara" class="mr20">
	    <option value="indonesia" <?php echo set_select('negara', 'indonesia', (set_value('negara') == 'indonesia')?TRUE:FALSE); ?> >&nbsp;Indonesia&nbsp;</option>
	</select>

	<label class="ml40-wd130" for="provinsi"><?php echo $this->lang->line('lbl_prov_tujuan'); ?> <br><span class="small"><?php echo $this->lang->line('lbl_info_prov_tujuan'); ?></span> </label>
	<select id="provinsi" data-url="<?php echo site_url('list_wilayah'); ?>" name="provinsi">
	    <option value="" <?php echo set_select('provinsi', '', (set_value('provinsi') == '')?TRUE:FALSE); ?> >&nbsp;----&nbsp;</option>
	<?php
	$list_provinsi = $this->maddons->listProvinsi();
	foreach( $list_provinsi as $nama) {
	?>
	    <option value="<?php echo $nama->id_provinsi; ?>" <?php echo set_select('provinsi', $nama->id_provinsi, ($prov == $nama->id_provinsi)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->provinsi); ?>&nbsp;</option>
	<?php
	}
	?>
	</select>
	<?php echo form_error('negara'); ?>
	<?php echo form_error('provinsi'); ?>
	</div>

	<div class="clft">
	<label for="wilayah"><?php echo $this->lang->line('lbl_wil_tujuan'); ?> <br><span class="small"><?php echo $this->lang->line('lbl_info_wil_tujuan'); ?></span> </label>
	<select id="wilayah" name="wilayah"  class="mr20">
	    <option value="" <?php echo set_select('wilayah', '', (set_value('wilayah') == '')?TRUE:FALSE); ?> >&nbsp;----&nbsp;</option>
	<?php
	if ( empty($wil) && empty($prov) )
	$list_wilayah = $this->maddons->listKota(set_value('provinsi'));
	else
	$list_wilayah = $this->maddons->listKotaById(set_value('provinsi',$prov));
	foreach( $list_wilayah as $nama) {
	?>
	    <option value="<?php echo $nama->id_kab_kota; ?>" <?php echo set_select('wilayah', $nama->id_kab_kota, ($wil == $nama->id_kab_kota)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->kab_kota); ?>&nbsp;</option>
	<?php
	}
	?>
	</select>
	<label class="lbl_small" for="kode_pos"><?php echo $this->lang->line('lbl_kdpos_tujuan'); ?> <br><span class="small"><?php echo $this->lang->line('lbl_info_kdpos_tujuan'); ?></span> </label>
	<input class="input_small" type="text" name="kode_pos" id="kode_pos" maxlength="10" value="<?php echo set_value('kode_pos',$pos); ?>" />
	<?php echo form_error('wilayah'); ?>
	<?php echo form_error('kode_pos'); ?>
	</div>

	<div class="clft">
	<label for="alamat"><?php echo $this->lang->line('lbl_alamat_tujuan'); ?> <br><span class="small"><?php echo $this->lang->line('lbl_info_alamat_tujuan'); ?></span> </label>
	<textarea spellcheck="false" name="alamat" id="alamat"><?php echo set_value('alamat',$alamat); ?></textarea>
	<?php echo form_error('alamat'); ?>
	</div>

	<div class="btnBox">
	<button class="ui-state-default" id="listKeranjang" type="button" data-url="<?php echo site_url(current_lang().'belanja/list_keranjang'); ?>"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-cart"></span><?php echo $this->lang->line('btn_update_keranjang'); ?></button>
	<button class="ui-state-default" id="bayarSekarang"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-check"></span><?php echo $this->lang->line('btn_proses_pembayaran'); ?></button>
	</div>

	</fieldset>
	<?php echo form_close(); ?>

	<div class="ui-widget ui-widget-content ui-corner-all" style="padding:8px">
	<h3><?php echo $this->lang->line('lbl_detail_pesanan'); ?></h3>
	<table class="gridtable" style="width:100%; font-size:12px;" border="0">
	<thead>
		<tr>
		  <th>QTY</th>
		  <th><?php echo $this->lang->line('lbl_item'); ?></th>
		  <th style="text-align:right"><?php echo $this->lang->line('lbl_harga'); ?></th>
		  <th style="text-align:right">Sub-Total</th>
		</tr>
	</thead>
	<tbody>
		<?php $p = 1; ?>
		<?php foreach ($this->cart->contents() as $items): ?>
		<tr valign="bottom" class="<?php echo ($p%2==1)?'satu':'dua'; ?>">
		  <td>
		  <?php echo $items['qty']; ?>
		  </td>
		  <td>
				<img src="<?php echo base_url().'_produk/thumb/small_thumb_'.$this->mproduk->getImgByProdId($items['id']); ?>" align="right" />
				<?php echo $this->mproduk->getFrontLabelProdById($items['id'], current_lang(false)); ?>
				<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
					<p>
						<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
							<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
						<?php endforeach; ?>
					</p>
				<?php endif; ?>
		  </td>
		  <td style="text-align:right"><?php echo format_harga_indo($items['price']); ?></td>
		  <td style="text-align:right"><?php echo format_harga_indo($items['subtotal']); ?></td>
		</tr>
		<?php $p++; ?>
		<?php endforeach; ?>

		<tr class="tiga">
		<td colspan="3" style="text-align:left;font-weight:bold"><strong><?php echo $this->lang->line('lbl_total_harga'); ?></strong></td>
		<td style="text-align:right;font-weight:bold"><?php echo $this->lang->line('lbl_rp'); ?> <?php echo format_harga_indo($this->cart->total()); ?></td>
		</tr>
	</table>
	</tbody>
	</div>

	<?php else: ?>
	    <p><?php echo $this->lang->line('lbl_pesanan_kosong'); ?></p>
	<?php endif; ?>
</div>

<?php if( ENVIRONMENT == 'development' ) : ?>
<script type="text/javascript">
$(function() {

    function expForm()
    {
        window.location.reload();
    }

    //list-keranjang
    $('#listKeranjang').click(function(e){
        e.preventDefault();
        var postURL = $(this).attr('data-url');
        
        $('#pemesanan-box').load(postURL);
    });

    //list-provinsi => kota
    $('select#provinsi').change(function(e){
        e.preventDefault();
        var postURL = $(this).attr('data-url');
        
        $.ajax({
            type: "POST",
            url: postURL,
            data: ({idp:$(this).val(),Hydra:$('input[name="Hydra"]').val()}),
            success: function(data){
                $('select#wilayah').empty().html(data);
                $('#kode_pos').val('');
            },
            error : expForm
        });
    });

});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(0(){0 5(){d.f.g()}$(\'#h\').i(0(e){e.6();7 a=$(1).8(\'2-3\');$(\'#j-k\').l(a)});$(\'9#m\').n(0(e){e.6();7 b=$(1).8(\'2-3\');$.o({p:"q",3:b,2:({r:$(1).4(),c:$(\'s[t="c"]\').4()}),u:0(a){$(\'9#v\').w().x(a);$(\'#y\').4(\'\')},z:5})})});',36,36,'function|this|data|url|val|expForm|preventDefault|var|attr|select|||Hydra|window||location|reload|listKeranjang|click|pemesanan|box|load|provinsi|change|ajax|type|POST|idp|input|name|success|wilayah|empty|html|kode_pos|error'.split('|'),0,{}))
</script>
<?php endif; ?>

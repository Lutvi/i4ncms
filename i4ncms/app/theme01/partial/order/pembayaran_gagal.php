
<div id="pemesanan-box">
<h3><?php echo $this->lang->line('lbl_proses_pemesanan_gagal'); ?></h3>
<?php if($this->cart->total_items() > 0): ?>
    <p><?php echo $this->lang->line('lbl_pesanan_gagal_data'); ?></p>
<?php else: ?>
    <p><?php echo $this->lang->line('lbl_pesanan_kosong'); ?></p>
<?php endif; ?>
</div>

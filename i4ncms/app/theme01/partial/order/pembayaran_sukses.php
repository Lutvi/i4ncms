
<div id="pemesanan-box">
<h3><?php echo $this->lang->line('lbl_proses_pesan_beshasil'); ?></h3>
    <p>
    <?php echo $this->lang->line('lbl_pesanan_sukses_data'); ?>
    <br />
    <?php echo $this->lang->line('lbl_mohon_cek_email'); ?> <span style="font-weight:bold"><em><?php echo bs_kode($this->session->userdata('email_beli'), TRUE); ?></em></span>.
    </p>

	<div class="ui-widget ui-widget-content ui-corner-all" style="padding:8px">
    <h5><?php echo $this->lang->line('lbl_detail_pesanan_final'); ?></h5>
	<?php print($detail_order); ?>
	</div>
</div>

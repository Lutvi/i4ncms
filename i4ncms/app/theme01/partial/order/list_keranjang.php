
<div id="pemesanan-box">
	<h3><?php echo $title; ?></h3>
	
	<?php if(isset($info) && !empty($info)): ?>
	<div id="info-bayar-box">
	<?php foreach($info as $key => $isi): ?>
	<div  class="info"><?php echo $isi; ?></div>
	<?php endforeach; ?>
	</div>
	<?php endif; ?>
	
	<?php
	if($this->cart->total_items() > 0):
	$attributes = array('id' => 'updateBelanja');
	echo form_open(current_lang().'pemesanan/update', $attributes); ?>
	<fieldset class="ui-widget ui-widget-content ui-corner-all">
	<legend class="ui-widget ui-widget-header ui-corner-all"><?php echo $this->lang->line('keranjang'); ?></legend>
		<table class="list-keranjang" cellpadding="4" cellspacing="5" style="width:100%; font-size:12px" border="0">
		
		<tr>
		  <th>QTY</th>
		  <th><?php echo $this->lang->line('lbl_item'); ?></th>
		  <th style="text-align:right"><?php echo $this->lang->line('lbl_harga'); ?></th>
		  <th style="text-align:right">Sub-Total</th>
		</tr>
		
		<?php $p = 1; ?>
		
		<?php foreach ($this->cart->contents() as $items): ?>
		
		<?php echo form_hidden($p.'[rowid]', $items['rowid']); ?>
		
		<tr>
		  <td><?php echo form_input(array('name' => $p.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'class' => 'qty','class' => 'qty-prod','style' => 'width:40px','autocomplete' => 'off')); ?></td>
		  <td>
				<img src="<?php echo base_url().'_produk/thumb/small_thumb_'.$this->mproduk->getImgByProdId($items['id']); ?>" align="right" />
				<?php echo $this->mproduk->getFrontLabelProdById($items['id'], current_lang(false)); ?>
				<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
					<p>
						<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
							<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
						<?php endforeach; ?>
					</p>
				<?php endif; ?>
		
		  </td>
		  <td style="text-align:right"><?php echo format_harga_indo($items['price']); ?></td>
		  <td style="text-align:right"><?php echo format_harga_indo($items['subtotal']); ?></td>
		</tr>
		<?php $p++; ?>
		<?php endforeach; ?>
		
		<tr>
		<td colspan="2"> </td>
		<td style="text-align:right;font-weight:bold"><strong><?php echo $this->lang->line('lbl_total_harga'); ?></strong></td>
		<td style="text-align:right;font-weight:bold"><?php echo $this->lang->line('lbl_rp'); ?> <?php echo format_harga_indo($this->cart->total()); ?></td>
		</tr>
		
		</table>
		
		<div>
			<button class="ui-state-default" id="updateKeranjang" disabled="disabled"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-cart"></span><?php echo $this->lang->line('btn_update_keranjang'); ?></button>
			<button class="ui-state-default" id="prosesPesan" type="button" data-url="<?php echo site_url(current_lang().'belanja/proses_pesan'); ?>"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-suitcase"></span><?php echo $this->lang->line('btn_proses_pemesanan'); ?></button>
		</div>
	</fieldset>
	<?php echo form_close(); ?>
	
	<?php else: ?>
		<p><?php echo $this->lang->line('lbl_pesanan_kosong'); ?></p>
	<?php endif; ?>
</div> 

<?php if( ENVIRONMENT == 'development' ) : ?>
<script type="text/javascript">

$(function() {
    //pesan
    $('#prosesPesan').click(function(e){
        e.preventDefault();        
        $('#pemesanan-box').load($(this).attr('data-url'));
    });
   
	$( "#updateKeranjang, #updateKeranjang:hover" ).css({ opacity:'0.7' });
    $('.qty-prod').focus(function(){
		$('#updateKeranjang').removeAttr('disabled');
		$('#updateKeranjang').css({ opacity:'1' });
    });

});

</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(3(){$(\'#6\').8(3(e){e.9();$(\'#a-b\').c($(d).f(\'g-h\'))});$("#2, #2:i").4({5:\'0.7\'});$(\'.j-k\').l(3(){$(\'#2\').m(\'n\');$(\'#2\').4({5:\'1\'})})});',24,24,'||updateKeranjang|function|css|opacity|prosesPesan||click|preventDefault|pemesanan|box|load|this||attr|data|url|hover|qty|prod|focus|removeAttr|disabled'.split('|'),0,{}))
</script>
<?php endif; ?>


<div id="pemesanan-box">
<h3><?php echo $title; ?></h3>  

<?php if(isset($info) && ! empty($info)): ?>
<div id="info-bayar-box">
<?php foreach($info as $key => $isi): ?>
<div  class="info"><?php echo $isi; ?></div>
<?php endforeach; ?>
</div>
<?php endif; ?>

    <?php if($this->cart->total_items() > 0): ?>
    <?php
    $attributes = array('id' => 'pembayaran');
    echo form_open(current_lang().'pemesanan/pembayaran', $attributes); 
    ?>
    <fieldset class="ui-widget ui-widget-content ui-corner-all">
    <input type="hidden" name="uid" id="uid" maxlength="" value="" />
    <input type="hidden" name="wilayah" id="wilayah" maxlength="11" value="<?php echo set_value('wilayah', $wilayah); ?>" />
    <p><?php echo $this->lang->line('lbl_pilih_pembayaran'); ?></p>
    
    <div id="metodeBayar">
    <?php
        $j = 0;
        foreach( $metode as $lbl):
    ?>
        <input type="radio" id="metode<?php echo $j; ?>" data-opsi="<?php echo 'opsi'.$j; ?>" name="metode" value="<?php echo $lbl['tipe_metode']; ?>" <?php echo (set_value('metode')==$lbl['tipe_metode'])?'checked="checked"':''; ?> /><label for="metode<?php echo $j; ?>"><?php echo $lbl['label_'.current_lang(false)]; ?></label>
    <?php
		$opsi[$j] = $lbl['tipe_metode'];
        $j++;
        endforeach;
    ?>
    </div>
    <?php echo form_error('metode'); ?>
    
    <div id="box-opsi">
    <?php echo form_error('pilihan'); ?>
        <?php
        for($i=0; $i < count($opsi); $i++):
        ?>
        <div class="opsi" id="opsi<?php echo $i; ?>">
            <?php
                foreach($vendor as $nama):
                if($nama['tipe_metode'] == $opsi[$i]):
            ?>
            <div class="logo-bayar ui-widget ui-widget-content ui-corner-all" style="background:url(<?php echo base_url().'_media/logo-bayar/'.$nama['logo']; ?>)center no-repeat;">
            <input type="radio" id="<?php echo underscore($nama['nama_vendor'].$j); ?>" name="pilihan" value="<?php echo $nama['nama_vendor']; ?>" <?php echo (set_value('pilihan')==$nama['nama_vendor'])?'checked="checked"':''; ?> /><label for="<?php echo underscore($nama['nama_vendor'].$j); ?>"><span><?php echo $nama['nama_vendor']; ?></span></label>
            </div>
            <?php
                $j++;
                endif;
                endforeach;
            ?>
        </div>
        <?php endfor; ?>
    </div>
    
    <div id="box-notes">
    <p><?php echo $this->lang->line('lbl_catatan_pemesan'); ?></p>
    <textarea spellcheck="false" name="notes"><?php echo set_value('notes',''); ?></textarea>
    </div>
    
    <p>
    <button class="ui-state-default" id="lanjutBayar"><span style="float:left;margin-right:10px" class="ui-icon ui-icon-check"></span><?php echo $this->lang->line('btn_pembayaran'); ?></button>
    </p>
    </fieldset>
    <?php echo form_close(); ?>

	<div class="ui-widget ui-widget-content ui-corner-all" style="padding:8px">
	<h3><?php echo $this->lang->line('lbl_detail_pesanan'); ?></h3>
    <table class="gridtable" style="width:100%; font-size:12px;" border="0">
		<tr>
		  <th>QTY</th>
		  <th><?php echo $this->lang->line('lbl_item'); ?></th>
		  <th style="text-align:right"><?php echo $this->lang->line('lbl_harga'); ?></th>
		  <th style="text-align:right">Sub-Total</th>
		</tr>

		<?php $p = 1; ?>
		<?php foreach ($this->cart->contents() as $items): ?>
		<tr class="<?php echo ($p%2==1)?'satu':'dua'; ?>">
		  <td>
		  <?php echo $items['qty']; ?>
		  </td>
		  <td>
				<img src="<?php echo base_url().'_produk/thumb/small_thumb_'.$this->mproduk->getImgByProdId($items['id']); ?>" align="right" />
				<?php echo $items['name']; ?>
				<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
					<p>
						<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
		
							<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
		
						<?php endforeach; ?>
					</p>
				<?php endif; ?>
		  </td>
		  <td style="text-align:right"><?php echo format_harga_indo($items['price']); ?></td>
		  <td style="text-align:right"><?php echo format_harga_indo($items['subtotal']); ?></td>
		</tr>
		
		<?php $p++; ?>
		
		<?php endforeach; ?>

	<?php if($diskon_transaksi > 0): ?>
		<tr class="empat">
		<td colspan="3" style="text-align:left;font-weight:bold"><strong><?php echo $this->lang->line('lbl_total_harga'); ?></strong></td>
		<td style="text-align:right;font-weight:bold"><?php echo format_harga_indo($this->cart->total()); ?></td>
		</tr>

		<tr class="empat">
		<td colspan="3" style="text-align:left;font-weight:bold"><strong><?php echo $this->lang->line('lbl_diskon_trans'); ?></strong></td>
		<td style="text-align:right;font-weight:bold"><?php echo format_harga_indo($diskon_transaksi); ?></td>
		</tr>
	<?php endif; ?>
		
		<tr class="tiga">
		<td colspan="3" style="text-align:left;font-weight:bold"><strong><?php echo $this->lang->line('lbl_total_bayar'); ?></strong></td>
		<td style="text-align:right;font-weight:bold"><?php echo $this->lang->line('lbl_rp'); ?> <?php echo format_harga_indo($total_transaksi); ?></td>
		</tr>
	</table>
	</div>

	<div class="ui-widget ui-widget-content ui-corner-all" style="padding:8px;margin-top:10px;color:#BFBFBF#7F7F7F">
	<h3><?php echo $this->lang->line('lbl_alamt_pengiriman'); ?></h3>
	<p>
	<?php
		foreach ($alamat_kirim as $alisi)
		{
			echo $alisi.'<br>'."\n";
		}
	?>
	</p>
	</div>
    
    <?php else: ?>
    <p><?php echo $this->lang->line('lbl_pesanan_kosong'); ?></p>
    <?php endif; ?>
</div>
<script>
$(function() {
    $( "#metodeBayar" ).buttonset();
    
    $(document).ready(function(){ 
        var elm = $('input[name="metode"]:checked'),
            metode = $(elm).val(),
            opsi = $(elm).data('opsi');
        
        $('.opsi').hide();
        $('#'+opsi).show();
    });
    
    $('input[name="metode"]').click(function(){ 
        var metode = $(this).val(),
            opsi = $(this).data('opsi');
        
        $('.opsi').hide();
        $('#'+opsi).show();
        $('input[name="pilihan"]:checked').removeAttr("checked");
    });
});
</script>

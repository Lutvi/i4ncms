<style>
ul.sitemap li {padding:3px;margin:3px}
ul.sitemapUrlKonten { padding-left:2em;font-size;14px }
span.hnourl {font-size:20px;line-height:32px;color:#3B8CBE;font-style:italic}
span.nourl {color:#D5183D;padding:3px;font-size:14px}
ul.sitemap a:link {font-size:15px}
</style>

<h1 class="video">Sitemap</h1>
<?php
$sitemap_menu = $this->config->item('sitemap_menu');
if($sitemap_menu):
?>
<ul class="sitemap">
	<?php foreach($menu as $row): ?>
	<!-- Level 1 -->
	<?php if( $row['link'] > 0 || $row['link'] === 'home'): ?>
    <li><a style="font-size:18px;line-height:32px" href="<?php echo ($row['link'] === 'home')?site_url(current_lang().'home'):site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row['link'])); ?>" class="menulink"><?php echo $row['title']; ?></a>
    <?php else: ?>
	<li><a style="font-size:18px;line-height:32px" class="menulink"><?php echo $row['title']; ?></a>
    <?php endif; ?>
		<?php
        $sub_menu = $this->cms->getParentMenu($row['id']);
        if(count($sub_menu)>0):
		echo '<ul>';
		foreach($sub_menu as $row2):
		$sub_sub_menu = $this->cms->getParentMenu($row2['id']);
        ?>
        <!-- Level 2 -->
        <li>#]/<a href="<?php echo ($row2['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row2['link'])):''; ?>" class="<?php echo (count($sub_sub_menu)>0)?'sub':'sub'; ?>"><?php echo $row2['title']; ?></a>
            <?php 
			if(count($sub_sub_menu)>0):
			echo '<ul>';
			foreach($sub_sub_menu as $row3): 
			$sub_sub_sub_menu = $this->cms->getParentMenu($row3['id']);	
			?>
            	<!-- Level 3 -->
                <li>##]/<a href="<?php echo ($row3['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row3['link'])):''; ?>" class="<?php echo (count($sub_sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row3['title']; ?></a>
                	<?php 
					if(count($sub_sub_sub_menu)>0):
					echo '<ul>';
					foreach($sub_sub_sub_menu as $row4):
					$sub_sub_sub_sub_menu = $this->cms->getParentMenu($row4['id']);
					?>
						<!-- Level 4 -->
						<li>####]/<a href="<?php echo ($row4['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row4['link'])):''; ?>"  class="<?php echo (count($sub_sub_sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row4['title']; ?></a>
							<?php 
							if(count($sub_sub_sub_sub_menu)>0):
							echo '<ul>';
							foreach($sub_sub_sub_sub_menu as $row5):
							$sub_sub_sub_sub_sub_menu = $this->cms->getParentMenu($row5['id']);
							?>
								<!-- Level 5 -->
								<li>######]/<a href="<?php echo ($row5['link'] > 0)?site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row5['link'])):''; ?>"  class="<?php echo (count($sub_sub_sub_sub_sub_menu)>0)?'sub':''; ?>"><?php echo $row5['title']; ?></a>
							<?php endforeach; ?>
							</li></ul>
							<?php endif; ?>
					<?php endforeach; ?>
					</li></ul>
					<?php endif; ?>
			<?php endforeach; ?>
            </li></ul>
        	<?php endif; ?>	
		</li>
		<?php endforeach; ?>
        </ul>
		<?php endif; ?>	
	</li>
    <?php endforeach; ?>
</ul>

<?php else: ?>

<ul class="sitemap">
	<?php foreach($menu as $row): ?>
	<!-- Level 1 -->
	<?php if( $row['link'] > 0 || $row['link'] == 'home'): ?>
    <li style="border-bottom: 1px solid #FF0018">
		<a style="font-size:20px;line-height:32px" href="<?php echo ($row['link'] === 'home')?site_url(current_lang().'home'):site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row['link'])); ?>"><?php echo $row['title']; ?></a>
    <?php else: ?>
	<li style="border-bottom: 1px solid #FF0018"><span class="hnourl"><?php echo $row['title']; ?></span>
    <?php endif; ?>
		<?php
        $sub_menu = $this->cms->getParentMenu($row['id']);
        if(count($sub_menu)>0):
		echo '<ul>';
		foreach($sub_menu as $row2):
		$sub_sub_menu = $this->cms->getParentMenu($row2['id']);
        ?>
        <!-- Level 2 -->
		<?php if($row2['link'] > 0) : ?>
		    <li>-| <a href="<?php echo site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row2['link'])); ?>"><?php echo $row2['title']; ?></a>
		<?php else: ?>
			<li>-| <span class="nourl"><?php echo $row2['title']; ?></span>
        <?php endif; ?>
            <?php
            daftarSitemapUrl($row2['title'],'sitemapUrlKonten');
			if(count($sub_sub_menu)>0):
			echo '<ul>';
			foreach($sub_sub_menu as $row3): 
			$sub_sub_sub_menu = $this->cms->getParentMenu($row3['id']);	
			?>
            	<!-- Level 3 -->
            	<?php if($row3['link'] > 0) : ?>
					<li>--| <a href="<?php echo site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row3['link'])); ?>"><?php echo $row3['title']; ?></a>
				<?php else: ?>
					<li>--| <span class="nourl"><?php echo $row3['title']; ?></span>
				<?php endif; ?>
                	<?php
                	daftarSitemapUrl($row3['title'],'sitemapUrlKonten');
					if(count($sub_sub_sub_menu)>0):
					echo '<ul>';
					foreach($sub_sub_sub_menu as $row4):
					$sub_sub_sub_sub_menu = $this->cms->getParentMenu($row4['id']);
					?>
						<!-- Level 4 -->
						<?php if($row4['link'] > 0) : ?>
							<li>---| <a href="<?php echo site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row4['link'])); ?>"><?php echo $row4['title']; ?></a>
						<?php else: ?>
							<li>---| <span class="nourl"><?php echo $row4['title']; ?></span>
						<?php endif; ?>
							<?php
							daftarSitemapUrl($row4['title'],'sitemapUrlKonten');
							if(count($sub_sub_sub_sub_menu)>0):
							echo '<ul>';
							foreach($sub_sub_sub_sub_menu as $row5):
							$sub_sub_sub_sub_sub_menu = $this->cms->getParentMenu($row5['id']);
							daftarSitemapUrl($row5['title'],'sitemapUrlKonten');
							?>
								<!-- Level 5 -->
								<?php if($row5['link'] > 0) : ?>
									<li>----| <a href="<?php echo site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row5['link'])); ?>"><?php echo $row5['title']; ?></a>
								<?php else: ?>
									<li>----| <span class="nourl"><?php echo $row5['title']; ?></span>
								<?php endif; ?>
							<?php endforeach; ?>
							</li></ul>
							<?php endif; ?>
					<?php endforeach; ?>
					</li></ul>
					<?php endif; ?>
			<?php endforeach; ?>
            </li></ul>
        	<?php endif; ?>	
		</li>
		<?php endforeach; ?>
        </ul>
		<?php endif; ?>	
	</li>
    <?php endforeach; ?>
</ul>

<?php endif; ?>

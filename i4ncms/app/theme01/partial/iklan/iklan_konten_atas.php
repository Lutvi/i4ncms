<?php if( ! empty($iklan_katas) && $this->config->item('iklan') && isset($qrcode)): ?>

<div id="katas" style="display:none;height:200px;position:relative;overflow:hidden;margin-bottom:10px">
	<div style="height:200px;width:650px;">
		<?php foreach ($iklan_katas as $iklan): ?>
		<a class="img_links iklan" target="_blank" href="<?php echo humanize($iklan->url_target); ?>" title="<?php echo humanize($iklan->nama); ?>"  data-upurl="shared/ajax_cek/uphits_iklan" data-hits="<?php echo $iklan->hits; ?>"  data-idiklan="<?php echo bs_kode($iklan->id); ?>">
		<span style="display:none"><?php echo $iklan->hits; ?></span>
		<img src="<?php echo base_url().'_media/banner-iklan/medium/medium_'.$iklan->img_src; ?>" alt="<?php echo $iklan->nama; ?>" width="650" height="200" />
		</a>
		<?php endforeach; ?>
	</div>

	<div style="position:absolute;right:0;bottom:0;border:1px solid #ADD8E6">
		    <?php if(isset($qrcode)) : ?>
				<?php echo $qrcode; ?>
			<?php endif; ?>
	</div>
</div>
<div style="clear:left;height:10px;border-top:1px solid #FFA500"></div>
<?php else: ?>

	<?php if(isset($qrcode)) : ?>
	<div style="height:130px;position:relative;;margin-bottom:10px">
		<div style="width:570px;height:126px;float:left;background:#ADD8E6;border:1px solid #A52A2A">
        	<div style="padding:8%; text-align:center">
			Pasang Iklan disini.(580x120)
            </div>
		</div>
		<div style="position:absolute;right:0;bottom:0;border:1px solid #ADD8E6">
			<?php echo $qrcode; ?>
		</div>
	</div>
	<div style="clear:left;height:10px;border-top:1px solid #FFA500"></div>
	<?php endif; ?>


<?php endif; ?>


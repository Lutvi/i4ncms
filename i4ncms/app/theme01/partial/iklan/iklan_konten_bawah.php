<?php if( ! empty($iklan_kbawah) && $this->config->item('iklan')): ?>
    <div id="kbawah">
        <?php foreach ($iklan_kbawah as $iklan): ?>
        <a class="img_links iklan" target="_blank" href="<?php echo humanize($iklan->url_target); ?>" title="<?php echo humanize($iklan->nama); ?>"  data-upurl="shared/ajax_cek/uphits_iklan" data-hits="<?php echo $iklan->hits; ?>"  data-idiklan="<?php echo bs_kode($iklan->id); ?>">
        <span style="display:none"><?php echo $iklan->hits; ?></span>
        <img src="<?php echo base_url().'_media/banner-iklan/medium/medium_'.$iklan->img_src; ?>" alt="<?php echo $iklan->nama; ?>" height="280" width="800" />
        </a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

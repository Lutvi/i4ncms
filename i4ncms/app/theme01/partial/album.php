
<?php
if ( ! empty($gambar_album) )
{
?>
<?php if(!empty($content)): ?>
	<h1 itemprop="name" class="album p-name"><?php echo $title; ?></h1>
	<div class="p-summary" itemprop="description" class="tajust">
		<?php echo $content; ?>
	</div>
<?php endif; ?>
<?php
	if ($per_page == 1)
	{
	?>
	<script>
    $(document).ready(function() {
        $('.cover-album, .fancybox-blogalbum')
        .fancybox({ centerOnScroll: true });
    });
    </script>
    <div class="h-entry" itemscope itemtype="http://schema.org/ImageObject" class="wrap-efek mrgAuto">
	<?php
	    $album = 1;
	    foreach ($gambar_album as $ralbum)
	    {
			$cnt_gallery = $this->cms->getFrontCountGalleryAlbum($ralbum->id);
			$albrate = ($ralbum->ratting*13);
			if (current_lang(false) === 'id') {
				$nama = $ralbum->nama_id;
				$isi_album = $ralbum->ket_id;
			} else {
				$nama = $ralbum->nama_en;
				$isi_album = $ralbum->ket_en;
			}
	?>
		<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, $ralbum->tgl_update); ?>" />
		<h1 class="album p-name" itemprop="name"><?php echo humanize($nama); ?></h1>
		<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $ralbum->tgl_update); ?>"><?php echo formatTanggal(current_lang(false), $ralbum->tgl_update); ?></time>

    <!-- SosMed -->
		<meta itemprop="interactionCount" content="UserTweets:1203"/>
		<meta itemprop="interactionCount" content="UserComments:78"/>
		<div itemscope="" itemtype="http://data-vocabulary.org/Review-aggregate" class="mrgTop8 mrgBot10">
        	<div class="grate w64 mrgBot8">
                <span class="fill" style="width:<?php echo $albrate; ?>px"></span>
            </div>
			<span itemprop="itemreviewed"><?php echo $this->lang->line('lbl_rating'); ?> </span>
			<span itemprop="rating" itemscope="" itemtype="http://data-vocabulary.org/Rating">
				<span itemprop="average"><?php echo $ralbum->ratting; ?></span>/<span itemprop="best">5</span>
			</span>
			<?php echo $this->lang->line('lbl_nrating'); ?> total <span itemprop="votes"><?php echo $ralbum->votes; ?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
		</div>
    <!-- End SosMed -->

		<a class="cover-album" href="<?php echo base_url().'/_media/album/large/large_'.$ralbum->gambar_cover;?>">
	        <img class="efek u-photo" width="680" style="margin: 10px; padding: 8px; border: 1px solid #BBB; vertical-align:top;" alt="<?php echo $ralbum->alt_text_id;?>" src="<?php echo base_url().'/_media/album/medium/medium_'.$ralbum->gambar_cover;?>" />
		</a>

        <div class="albumBoxDetail">
            <div class="p-summary" itemprop="description"><?php echo $isi_album; ?></div>
            <?php if($cnt_gallery > 0): ?>
            <h4>Gallery (<?php echo $cnt_gallery; ?>)</h4>
            <?php endif; ?>
        </div>
	    <div id="galleriAlbum">
	    <?php
			if($cnt_gallery > 0)
			{
				$gallery = 1;
				$gambar_gallery = $this->cms->getAllFrontGalleryAlbum($ralbum->id,$cnt_gallery,0);
				foreach ($gambar_gallery as $row){
				?>
					<a class="album" rel="album_group" href="<?php echo base_url().'/_media/album-gallery/large/large_'.$row->gambar_gallery;?>"><img class="efek u-photo" itemprop="thumbnail" width="190" style="margin: 8px; padding: 2px; border: 1px solid #BBB; vertical-align: top;" alt="<?php echo $row->alt_text_id;?>" src="<?php echo base_url().'/_media/album-gallery/small/small_'.$row->gambar_gallery;?>" /></a>
					<div id="tampil-<?php echo $gallery;?>" style="display:none;">
						<?php
						if (current_lang(false) === 'id')
						{
							echo '<h3 style="padding:0;margin:0;">'.humanize($row->nama_id).'</h3>';
							echo $row->ket_id;
						} else {
							echo '<h3 style="padding:0;margin:0;">'.humanize($row->nama_en).'</h3>';
							echo $row->ket_en;
						}
						?>
					</div>
	
				<?php
				$gallery++;
				}
			}
		?>
		</div>
		<?php
		$album++;
	    }
		?>
<!-- Komponen-Bawah -->
    <div class="clKiri"></div>
    <?php $this->load->view_theme('tag_box', '', $this->config->item('theme_id').'/partial/'); ?>
    <div class="clKiri"></div>
    <!--Rating-->
    <div id="ratingBox">
        <div class="mrgTop20">
            <?php echo $this->lang->line('lbl_rating'); ?> <span><?php echo $ralbum->ratting;?></span>/<span>5</span>
            <?php echo $this->lang->line('lbl_nrating'); ?> <span><?php echo $ralbum->votes;?></span> <?php echo $this->lang->line('lbl_jvote'); ?>
        </div>
    </div>
    <form id="ratingForm">
        <input type="hidden" name="rt" value="<?php echo bs_kode($ralbum->id); ?>" />
        <input type="hidden" name="tipe" value="album" />
        <?php echo $this->lang->line('lbl_beriPeringkat'); ?> :
        <div class="starRating">
          <div>
            <div>
              <div>
                <div>
                  <input id="rating1" type="radio" name="rating" value="1">
                  <label for="rating1"><span>1</span></label>
                </div>
                <input id="rating2" type="radio" name="rating" value="2">
                <label for="rating2"><span>2</span></label>
              </div>
              <input id="rating3" type="radio" name="rating" value="3">
              <label for="rating3"><span>3</span></label>
            </div>
            <input id="rating4" type="radio" name="rating" value="4">
            <label for="rating4"><span>4</span></label>
          </div>
          <input id="rating5" type="radio" name="rating" value="5">
          <label for="rating5"><span>5</span></label>
         </div>
      </form>
      <div id="ratingRes">
      	<?php echo $this->lang->line('lbl_upPeringkat'); ?> <span id="ratingNrat"></span> <?php echo $this->lang->line('lbl_nrating'); ?> <span id="ratingNvot"></span> <?php echo $this->lang->line('lbl_jvote'); ?>
      </div>
      <div class="btsGarisBiru"></div>
    <!--End Rating-->
	  <?php
	  	//load iklan bawah
	    $this->load->view_theme('iklan_konten_bawah', '', $this->config->item('theme_id').'/partial/iklan/');
	  ?>
      <?php if($ralbum->diskusi === 'on'): ?>
	    <div id="disqusBox"><?php echo $this->disqus->get_html(); ?></div>
      <?php endif; ?>
<!-- End Komponen-Bawah -->
    </div>
	<?php
	}
	else {
	echo ( ! $album_page)?'':'<nav class="navigation-bar fixed-top"><div class="pagination">'.$album_page.'</div></nav>';
	?>
	<script type="text/javascript">
    $(document).ready(function() {
        $('ul.img-thumbs li').hover(
            function(){$(".overlay", this).stop().animate({top:'0px'}, {queue:false,duration:300});}, 
            function(){$(".overlay", this).stop().animate({top:'195px'},{queue:false,duration:300});}
        );  
    });
    </script>
<ul class="img-thumbs">
<?php
    $album = 1;
    foreach ($gambar_album as $row)
    {
    $cnt_gallery = $this->cms->getFrontCountGalleryAlbum($row->id);
?>
    <li class="h-entry" itemscope itemtype="http://schema.org/ImageObject">
	    <a style="cursor:default;" href="javascript:void(0);"><img itemprop="thumbnail" class="u-photo fotoAlbum" width="300" src="<?php echo base_url().'/_media/album/small/small_'.$row->gambar_cover;?>" /></a>
	    <div class="overlay">
		    <?php
			if (current_lang(false) === 'id')
			{
				$nama = $row->nama_id;
				$isi_album = word_limiter($row->ket_id, 35);
			} else {
				$nama = $row->nama_en;
				$isi_album = word_limiter($row->ket_en, 35);
			}
			$albrate = ($row->ratting*13);
			?>
			<link itemprop="url" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>" />
			<meta itemprop="datePublished" content="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>" />
			<h1 class="p-name" itemprop="name"><?php echo humanize($nama); ?></h1>
			<time class="dt-updated" itemprop="dateModified" datetime="<?php echo date(DATE_RFC3339, $row->tgl_update); ?>"><?php echo formatTanggal(current_lang(false), $row->tgl_update); ?></time>
            <div class="grate w64 fright mrgTopMin5">
                <span class="fill" style="width:<?php echo $albrate; ?>px"></span>
            </div>
			<p class="p-summary" itemprop="description"><?php echo humanize(strip_tags($isi_album)); ?></p>
			<p>
			<?php if($cnt_gallery > 0): ?>
			<a class="u-url" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>">Gallery (<?php echo $cnt_gallery; ?>)</a>
			<?php else: ?>
			<a class="u-url" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>"><?php echo $this->lang->line('lbl_selengkapnya');?></a>
			<?php endif; ?>
			</p>
			<a class="zoom" href="<?php echo site_url(current_lang().'gallery/'.underscore($nama)); ?>" rel="" title="<?php echo humanize($nama); ?>"></a>
	    </div>
    </li>
	<?php
	}
	?>
</ul>

	<?php
		echo ( ! $album_page)?'':'<div class="pagination">'.$album_page.'</div>';
	}
}
else {
	echo "<h3>".$this->lang->line('jdl_album_kosong')."</h3><div class='posRelatif'><p>".$this->lang->line('lbl_album_kosong')."</p></div>";
}
?>

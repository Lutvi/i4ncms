<!DOCTYPE html>
<html lang='<?php echo current_lang(false); ?>'>

<?php if(isset($tipe) && !in_array($tipe,config_item('esc_rich'))): ?>
<head itemscope itemtype="http://schema.org/Organization" prefix="og: http://ogp.me/ns#">
<?php else: ?>
<head>
<?php endif; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--[if lte IE 8]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<?php $this->load->view('global_assets/front_all_meta'); ?>

	<link rel="shortcut icon" href="<?php echo base_url()?>favicon.ico" />
	<link rel="icon" href="<?php echo base_url()?>favicon.ico" />
	<title><?php echo $this->config->item('site_name') . ' - ' . $title; ?></title>
	<?php if ( ENVIRONMENT == 'live' || ENVIRONMENT == 'production') : ?>
	<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,800,400italic' rel='stylesheet' type='text/css'>-->
    <link href='http://fonts.googleapis.com/css?family=Tangerine&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<?php endif; ?>
	<?php $this->load->view('global_assets/front_all_css'); ?>
	<?php $this->load->view('dynamic_js'); ?>
	<?php $this->load->view('global_assets/front_all_js'); ?>
	<?php //$this->load->view('global_assets/front_all_ga'); ?>
</head>

<?php
$sitemap_menu = $this->config->item('sitemap_menu');

header('Content-type: application/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">' . "\n";

foreach($menu as $row)
{
	if( ! empty($row['link']))
	{
		if($row['link'] === 'home')
			echo '<url>' . "\n".'<loc>'.site_url(current_lang().'home').'</loc>'. "\n".'</url>' . "\n";
		else
			echo '<url>' . "\n".'<loc>'.site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row['link'])).'</loc>'. "\n".'</url>' . "\n";
	}
	$sub_menu = $this->cms->getParentMenu($row['id']);
	if(count($sub_menu)>0)
	{
		foreach($sub_menu as $row2)
		{
			$sub_sub_menu = $this->cms->getParentMenu($row2['id']);
			if($row2['link'] > 0)
			{
				echo '<url>' . "\n".'<loc>'.site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row2['link'])).'</loc>' . "\n".'</url>' . "\n";
			}
			
			if(!$sitemap_menu)
			daftarSitemapUrlXML($row2['title']);
			if(count($sub_sub_menu)>0)
			{
				foreach($sub_sub_menu as $row3)
				{
					$sub_sub_sub_menu = $this->cms->getParentMenu($row3['id']);
					if($row3['link'] > 0)
					{
						echo '<url>' . "\n".'<loc>'.site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row3['link'])).'</loc>' . "\n".'</url>' . "\n";
					}

					if(!$sitemap_menu)
					daftarSitemapUrlXML($row3['title']);
					if(count($sub_sub_sub_menu)>0)
					{
						foreach($sub_sub_sub_menu as $row4)
						{
							$sub_sub_sub_sub_menu = $this->cms->getParentMenu($row4['id']);
							if($row4['link'] > 0)
							{
								echo '<url>' . "\n".'<loc>'.site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row4['link'])).'</loc>' . "\n".'</url>' . "\n";
							}

							if(!$sitemap_menu)
							daftarSitemapUrlXML($row4['title']);
							if(count($sub_sub_sub_sub_menu)>0)
							{
								foreach($sub_sub_sub_sub_menu as $row5)
								{
									$sub_sub_sub_sub_sub_menu = $this->cms->getParentMenu($row5['id']);
									daftarSitemapUrlXML($row5['title']);
									if($row5['link'] > 0)
									{
										echo '<url>' . "\n".'<loc>'.site_url($path_halaman.'/'.$this->cms->getLabelHalamanById($row5['link'])).'</loc>' . "\n".'</url>' . "\n";
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

echo '</urlset>' . "\n";
?>


<?php $this->load->view_theme('head', '', $this->config->item('theme_id').'/main/'); ?>

<?php if(isset($tipe) && !in_array($tipe,$this->config->item('esc_rich'))): ?>
<body itemscope itemtype="http://schema.org/WebPage" class="i4nCMS">
<?php else: ?>
<body class="i4nCMS">
<?php endif; ?>

<div id="container" class="rounded">
<?php if (isset($hbanner) && ! empty($hbanner)): ?>
	<div id="header">
		<div id="hBanner" class="rounded">
		<div id="wrapSlide">

		<div id="slider1_container" style="width: 1100px; height: 400px;">
		<!-- Loading Screen -->
		<div u="loading" style="position: absolute; top: 0px; left: 0px;">
			<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;top: 0px; left: 0px; width: 100%; height: 100%;"></div>
			<div style="position: absolute; display: block; background: url(/assets/images/slide-loading.gif) no-repeat center center;top: 0px; left: 0px; width: 100%; height: 100%;"></div>
		</div>
		<!-- slides -->
		<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1100px;height: 400px; overflow: hidden;">
			<?php foreach($hbanner as $hitem): ?>
				<div>
					<div style="position: absolute; width: 500px; height: 320px; top: 10px; left: 20px; text-align: left; line-height: 1.8em; font-size: 12px;">
						<br />
						<span style="display: block; line-height: 1em; text-transform: uppercase; font-size: 52px;
							color: #FFFFFF;"><?php echo $hitem->nama; ?></span>
						<div style="display: block; line-height: 1.8em; font-size: 1.5em; color: #FFFFFF;">
							<?php echo $hitem->ket; ?>
						</div>
						<br />
						<br />
						<?php if($hitem->url_target != ''): ?>
						<a target="_blank" href="<?php echo prep_url($hitem->url_target); ?>"><?php echo $hitem->url_target; ?></a>
						<?php endif; ?>
					</div>
					<img src="<?php echo base_url(); ?>_media/banner-header/<?php echo $hitem->img_src; ?>" alt="<?php echo $hitem->nama; ?>" style="position: absolute; top: 23px; left: 520px; width: 600px; height: 320px;" />
					<img u="thumb" src="<?php echo base_url(); ?>_media/banner-header/thumb_<?php echo $hitem->img_src; ?>" alt="Thumb <?php echo $hitem->nama; ?>" />
				</div>
			<?php endforeach; ?>
		</div><!-- end slides -->
		<!-- Direction Navigator Skin Begin -->
			<!-- Arrow Left -->
			<span u="arrowleft" class="jssord07l" style="width: 50px; height: 50px; top: 123px;left: 8px;"></span>
			<!-- Arrow Right -->
			<span u="arrowright" class="jssord07r" style="width: 50px; height: 50px; top: 123px;right: 8px"></span>
		<!-- Direction Navigator Skin End -->		
			<div u="thumbnavigator" class="jssort04" style="position: absolute; width: 1100px;height: 60px; right: 0px; bottom: 0px;">
				<!-- Thumbnail Item Skin Begin -->
				<div u="slides" style="bottom: 25px; right: 30px;">
					<div u="prototype" class="p" style="position: absolute; cursor:pointer; width: 62px; height: 32px; top: 0; left: 0;">
						<div class="w">
							<thumbnailtemplate style="width: 100%; height: 100%; border: none; position: absolute; top: 0; left: 0;"></thumbnailtemplate>
						</div>
						<div class="c" style="position: absolute; background-color: #000; top: 0; left: 0">
						</div>
					</div>
				</div>
				<!-- Thumbnail Item Skin End -->
			</div>
			<!-- ThumbnailNavigator Skin End -->
		</div><!-- #slider1_container End -->

		</div><!-- End #wrapSlide -->
		</div><!-- end #hBanner -->
		<div class="clearfloat"></div>
	</div><!-- end #header -->
<?php else: ?>
	<!-- Banner standart -->
	<div id="header">
		<div id="hBanner" class="rounded">
		<div id="wrapSlide">

		<div id="slider1_container" style="width: 1100px; height: 400px;">
		<!-- Loading Screen -->
		<div u="loading" style="position: absolute; top: 0px; left: 0px;">
			<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;top: 0px; left: 0px; width: 100%; height: 100%;"></div>
			<div style="position: absolute; display: block; background: url(/assets/images/slide-loading.gif) no-repeat center center;top: 0px; left: 0px; width: 100%; height: 100%;"></div>
		</div>
		<!-- slides -->
		<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1100px;height: 400px; overflow: hidden;">
			<?php for($i=1;$i<=5;$i++): ?>
				<div>
					<div style="position: absolute; width: 500px; height: 320px; top: 10px; left: 20px; text-align: left; line-height: 1.8em; font-size: 12px;">
                            <br />
                            <span style="display: block; line-height: 1em; text-transform: uppercase; font-size: 52px;
                                color: #FFFFFF;">BANNER <?php echo $i; ?></span>
                            <div style="display: block; line-height: 1.8em; font-size: 1.5em; color: #FFFFFF;">
								Info singkat banner akan muncul disini.
                            </div>
                            <br />
                            <br />
                            
                            <a href="#">Link<?php echo $i; ?>-disini.</a>
					</div>
					<img src="<?php echo base_url(); ?>assets/images/hbanner/s<?php echo $i; ?>.png" alt="nama <?php echo $i; ?>" style="position: absolute; top: 23px; left: 520px; width: 600px; height: 320px;" />
					<img u="thumb" src="<?php echo base_url(); ?>assets/images/hbanner/s<?php echo $i; ?>t.jpg" alt="Thumb <?php echo $i; ?>" />
				</div>
			<?php endfor; ?>
		</div><!-- end slides -->
		<!-- Direction Navigator Skin Begin -->
			<!-- Arrow Left -->
			<span u="arrowleft" class="jssord07l" style="width: 50px; height: 50px; top: 123px;left: 8px;"></span>
			<!-- Arrow Right -->
			<span u="arrowright" class="jssord07r" style="width: 50px; height: 50px; top: 123px;right: 8px"></span>
		<!-- Direction Navigator Skin End -->
			<div u="thumbnavigator" class="jssort04" style="position: absolute; width: 1100px;height: 60px; right: 0px; bottom: 0px;">
				<!-- Thumbnail Item Skin Begin -->
				<div u="slides" style="bottom: 25px; right: 30px;">
					<div u="prototype" class="p" style="position: absolute; cursor:pointer; width: 62px; height: 32px; top: 0; left: 0;">
						<div class="w">
							<thumbnailtemplate style="width: 100%; height: 100%; border: none; position: absolute; top: 0; left: 0;"></thumbnailtemplate>
						</div>
						<div class="c" style="position: absolute; background-color: #000; top: 0; left: 0">
						</div>
					</div>
				</div>
				<!-- Thumbnail Item Skin End -->
			</div>
			<!-- ThumbnailNavigator Skin End -->
		</div><!-- #slider1_container End -->

		</div><!-- End #wrapSlide -->
		</div><!-- end #hBanner -->
		<div class="clearfloat"></div>
	</div><!-- end #header -->
<?php endif; ?>

  <div class="clearfloat"></div>
  <div id="topContent" class="rounded">
	<div id="topForm" style="font: 10px normal Helvetica, Arial, sans-serif; display:none;">
		<input type="hidden" value="<?php echo (isset($login)?'login':''); ?>" autocomplete="off" id="isLogin" name="isLogin"  />
		<?php $this->load->view_theme('form_container', '', $this->config->item('theme_id').'/form/'); ?>
	</div><!-- end #topForm -->

	<div id="rightTop">
		<div id="topMenu">
			<?php $this->load->view_theme('top_menu', '', $this->config->item('theme_id').'/menu/'); ?>
		</div><!-- end #topMenu -->
		<div class="clearfloat"></div>
		<div id="topBanner" style="display:none;">
			<?php $this->load->view_theme('top_banner', $banner, $this->config->item('theme_id').'/banner/'); ?>
		</div><!-- end #topBanner -->
		<div class="clearfloat"></div>
		
	</div><!-- end #rightTop -->
	<div class="clearfloat"></div>
  </div><!-- end #topContent --> 

	<?php if($this->uri->segment(1) === FALSE || strlen($this->uri->segment(1)) == 2 && $this->uri->segment(3) != 'pembayaran'): ?>
	<div style="float:right; margin:10px 20px -40px 0;">
		<div class="menu_lang"><?php echo alt_site_url(); ?></div>
	</div><div class="clearfloat"></div>
	<?php endif; ?>
  
  <div id="sidebarWraper">
	<?php
	// keranjang belanja
	$this->load->view_theme('keranjang_belanja', '', $this->config->item('theme_id').'/sidebar/');
	$this->load->view_theme('kategori_konten', '', $this->config->item('theme_id').'/sidebar/');
	$this->load->view_theme('tags_konten', '', $this->config->item('theme_id').'/sidebar/');
	$this->load->view_theme('iklan_sidebar', '', $this->config->item('theme_id').'/sidebar/');
	// run All Active Widget max = config[max-widget]
	$this->load->view('global_content/widget_frame'); 
	?>
  </div><!-- end #sidebarWraper -->

	<div id="mainContent">
	<div class="wraper">
		<?php $this->load->view_theme('iklan_konten_atas', '', $this->config->item('theme_id').'/partial/iklan/'); ?>
		<?php #tsout(breadNav($tipe,true)); ?>
		<?php echo breadNav($tipe); ?>
		<?php
		// tambahan untuk custom data format array(key => value)
		$data['data'] = array(
					'tes' => 'Testing data custom set dari main view.'
					);
		$this->load->view('global_content/content', $data);
		?>
	</div>
	</div><!-- end #mainContent -->
  
   <div class="clearfloat"></div>
   <div id="footer" class="grid_12">
	  <div class="grid_4">
	    <h4>
	      <?php echo $this->lang->line('lbl_tentang');?>
	    </h4>
	    <p>
	      <?php echo $this->config->item('sort_site_description'); ?>
	    </p>
	  </div>
	  <div class="grid_4 pull_4">
	    <?php $this->load->view_theme('tag_footer', '', $this->config->item('theme_id').'/footer/'); ?>
	  </div>
	  <div class="grid_4 push_4" style="text-align:right">
	    <?php $this->load->view_theme('logo_cms', '', $this->config->item('theme_id').'/footer/'); ?>
	  </div>
	  <div class="clearfloat"></div>
  </div><!-- end #footer -->
  
</div><!-- end #container -->
</body>
</html>


<?php if(isset($infok) && !empty($infok)): ?>
<div id="info-belanja-box">
<?php foreach($infok as $key => $isi): ?>
<div class="info"><?php echo $isi; ?></div>
<?php endforeach; ?>
</div>
<?php endif; ?>

<?php
    $attributes = array('id' => 'updateBelanja');
    echo form_open(current_lang().'belanja/update', $attributes);
?>

    <table style="width:100%; font-size:10px" border="0">
    <tr>
      <th>QTY</th>
      <th><?php echo $this->lang->line('lbl_item'); ?></th>
      <th style="text-align:right"><?php echo $this->lang->line('lbl_harga'); ?></th>
      <th style="text-align:right">Sub-Total</th>
    </tr>
    
    <?php $p = 1; ?>
    
    <?php foreach ($this->cart->contents() as $items): ?>

    <?php echo form_hidden($p.'[rowid]', $items['rowid']); ?>
    
    <tr>
      <td style="margin-top:-9px">
      <?php if ( ($this->mobiledetection->isMobile() && config_item('is_respon') === FALSE) || ENVIRONMENT === 'm-testing' ): ?>
	  <?php echo form_input(array('name' => $p.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'class' => 'qty','style' => 'width:30px; height:25px','autocomplete' => 'off')); ?>
      <?php else: ?>
      <?php echo form_input(array('name' => $p.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'class' => 'qty','style' => 'width:25px; height:11px','autocomplete' => 'off')); ?>
      <?php endif; ?>
      </td>
      <td style="cursor:pointer;padding-left:2px;padding-right:2px;">
        <a class="screenshot" rel="<?php echo base_url().'_produk/thumb/thumb_'.$this->mproduk->getImgByProdId($items['id']); ?>"  title="<?php echo $this->mproduk->getFrontLabelProdById($items['id'], current_lang(false)); ?>">
        <?php echo substr($this->mproduk->getFrontLabelProdById($items['id'], current_lang(false)),0,5).'...'; ?>
    
            <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
    
                <p>
                    <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
    
                        <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
    
                    <?php endforeach; ?>
                </p>
    
            <?php endif; ?>
		</a>
      </td>
      <td style="text-align:right"><?php echo format_harga_indo($items['price']); ?></td>
      <td style="text-align:right"><?php echo format_harga_indo($items['subtotal']); ?></td>
    </tr>
    
    <?php $p++; ?>
    
    <?php endforeach; ?>
    
    <tr>
    <td colspan="2" style="text-align:right;font-weight:bold"><strong><?php echo $this->lang->line('lbl_total_harga'); ?></strong></td>
    <td colspan="2" style="text-align:right;font-weight:bold;border-top:2px solid #D41212"><?php echo $this->lang->line('lbl_rp'); ?> <?php echo format_harga_indo($this->cart->total()); ?></td>
    </tr>
    
    </table>
    
    <p>
        <?php echo form_submit('', $this->lang->line('btn_update_keranjang'),'class="upBelanja" id="upKeranjang" disabled="disabled" style="font-size:11px"'); ?>
        <?php echo form_close(); ?>
        <?php 
        $attributes = array('id' => 'batalBelanja');
        echo form_open(current_lang().'belanja/batal', $attributes); ?>
        <?php echo form_submit('', $this->lang->line('btn_batal_keranjang'),'class="upBelanja" id="btn-belanja-batal" style="font-size:11px"'); ?>
        
        <?php
        if($this->cart->total_items() > 0)
        echo anchor(current_lang().'pemesanan',$this->lang->line('lbl_pesan_keranjang'), array('id' => 'btn-pesan-sekarang','style' => 'font-size:11px;float:right')); 
        ?>
        <?php echo form_close(); ?>
    </p>
<style>
a.screenshot { color:#1A1A1A; text-decoration:none;}
a.screenshot:hover { text-decoration:underline; }
#screenshot{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
</style>

<?php if( ENVIRONMENT == 'development' ) : ?>
<script type="text/javascript">

var screenshotPreview = function(){
	/* CONFIG */
	var xOffset = 10;
	var yOffset = 10;
	/* END CONFIG */

	$("a.screenshot").hover(function(e){
		this.t = this.title;
		this.title = "";
		var c = (this.t != "") ? "<br/>" + this.t : "";
		$("body").append("<p id='screenshot'><img src='"+ this.rel +"' alt='url preview' />"+ c +"</p>");
		$("#screenshot")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px")
			.fadeIn("fast");
    },
	function(){
		this.title = this.t;
		$("#screenshot").remove();
    });
    
	$("a.screenshot").mousemove(function(e){
		$("#screenshot")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px");
	});
}

$(function($){
    $('input.upBelanja').click(function(e){
        e.preventDefault();
        var isi = $('form').serialize(),
            postURL = $(this).parents('form').attr('action');

        $.ajax({
            type: "POST",
            url: postURL,
            data: isi,
            success: function(data){
				if(data == 'error')
				location.reload();
				else
                $('#keranjang').html(data);
            },
            error : function(){ location.reload(); }
        });
    });

    $('input.qty').focus(function () {
        $( "#upKeranjang" ).button({ disabled: false });
    });

    if($('#info-belanja-box').is(':visible')){
        $('#info-belanja-box').effect('pulsate',{},500)
        .delay(10000)
        .effect('blind',{},1000);
    }

	$( "#upKeranjang" ).button({ disabled: true });
	$( "#btn-belanja-batal" ).button({
        icons: {
            primary: "ui-icon-suitcase"
        }
    });
    $( "#btn-pesan-sekarang" ).button({
        icons: {
            primary: "ui-icon-suitcase"
        }
    });
});

// starting the script on page load
$(document).ready(function(){
	screenshotPreview();
});

</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
var screenshotPreview=function(){var xOffset=10;var yOffset=10;$("a.screenshot").hover(function(e){this.t=this.title;this.title="";var c=(this.t!="")?"<br/>"+this.t:"";$("body").append("<p id='screenshot'><img src='"+this.rel+"' alt='url preview' />"+c+"</p>");$("#screenshot").css("top",(e.pageY-xOffset)+"px").css("left",(e.pageX+yOffset)+"px").fadeIn("fast");},function(){this.title=this.t;$("#screenshot").remove();});$("a.screenshot").mousemove(function(e){$("#screenshot").css("top",(e.pageY-xOffset)+"px").css("left",(e.pageX+yOffset)+"px");});}
$(function($){$('input.upBelanja').click(function(e){e.preventDefault();var isi=$('form').serialize(),postURL=$(this).parents('form').attr('action');$.ajax({type:"POST",url:postURL,data:isi,success:function(data){if(data=='error')
location.reload();else
$('#keranjang').html(data);},error:function(){location.reload();}});});$('input.qty').focus(function(){$("#upKeranjang").button({disabled:false});});if($('#info-belanja-box').is(':visible')){$('#info-belanja-box').effect('pulsate',{},500).delay(10000).effect('blind',{},1000);}
$("#upKeranjang").button({disabled:true});$("#btn-belanja-batal").button({icons:{primary:"ui-icon-suitcase"}});$("#btn-pesan-sekarang").button({icons:{primary:"ui-icon-suitcase"}});});$(document).ready(function(){screenshotPreview();});
</script>
<?php endif; ?>

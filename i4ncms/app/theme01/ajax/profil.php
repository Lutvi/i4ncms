<div id="profil-user" title="Update <?php echo $this->lang->line('lbl_btn_profil'); ?>" style="display:none; font-size:11px;">
	<?php
		$attributes = array('class' => 'cmxform', 'id' => 'profilForm');
		echo form_open('shared/ajax_cek/up_profil', $attributes);
	?>
	<input type="hidden" name="url" id="url" value="<?php echo current_url(); ?>" />
	<input type="hidden" name="ori_name" id="ori_name" value="<?php echo bs_kode($this->session->userdata('nmusr'), TRUE); ?>" maxlength="20" />
	<input type="hidden" name="ori_username" id="ori_pusername" value="<?php echo bs_kode($this->session->userdata('usrid'), TRUE); ?>"maxlength="20" />
	<input type="hidden" name="ori_email" id="ori_pemail" value="<?php echo bs_kode($this->session->userdata('kunci'), TRUE); ?>" maxlength="60" />

	  <fieldset class="ui-widget ui-widget-content ui-corner-all" style="text-align:left">
	  <legend class="ui-widget ui-widget-header ui-corner-all">Form <?php echo $this->lang->line('lbl_btn_profil'); ?></legend>
	  <label for="name"><?php echo $this->lang->line('lbl_nama'); ?></label>
	  <input type="text" name="name" id="name" value="<?php echo bs_kode($this->session->userdata('nmusr'), TRUE); ?>" autocomplete="off" maxlength="20" />
	  <br />
	  <label for="pusername"><?php echo $this->lang->line('lbl_id'); ?></label>
	  <input type="text" name="username" id="pusername" value="<?php echo bs_kode($this->session->userdata('usrid'), TRUE); ?>" autocomplete="off" maxlength="20" />
	  <br />
	  <label for="pemail"><?php echo $this->lang->line('lbl_email'); ?></label>
	  <input  readonly="readonly" type="text" name="email" id="pemail" value="<?php echo bs_kode($this->session->userdata('kunci'), TRUE); ?>" autocomplete="off" maxlength="60" />
	  <br />
	  <label for="ppassword"><?php echo $this->lang->line('lbl_ganti_kata_sandi'); ?></label>
	  <input id="ppassword" name="password" type="password" autocomplete="off" maxlength="20" />
	  <br />
	  <label for="pconfirm_password"><?php echo $this->lang->line('lbl_ulangi_kata_sandi'); ?></label>
	  <input id="pconfirm_password" name="confirm_password" type="password" autocomplete="off" maxlength="20" />
	  <br />
	  </fieldset>
	  <div id="err_reg" class="error-txt"></div>
	</form>
</div>

    <h4>
      <?php echo $this->lang->line('lbl_tag');?>
    </h4>
    
    <p>
    <?php
	$mtags = $this->cms->getFrontAllTagObjId();
	$id_album = array();
	foreach($mtags as $mtag): ?>
		<?php
		$tags = $this->cms->getTagsLabelByObjekId($mtag->id_objek,$mtag->tipe_objek);
		foreach($tags['rs_kat'] as $tag):
		if(in_array($tag->id_kategori, $id_album))
		continue;
		if (current_lang(false) === 'id')
		{
			$nama = $tag->label_id;
			$lbl = 'tag/';
		}
		else {
			$nama = $tag->label_en;
			$lbl = 'tag/';
		}
		$link_url = base_url().current_lang().$lbl.$mtag->tipe_objek.'/'.underscore($nama).'.html';
		?>
		<a class="tagsBawah" href="<?php echo $link_url; ?>" style="5px"><?php echo $nama; ?></a>
		<?php 
		$id_album[] = $tag->id_kategori;
		endforeach; ?>
	<?php endforeach; ?>
    </p>

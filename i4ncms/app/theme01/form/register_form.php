<?php
	$attributes = array('class' => 'cmxform', 'id' => 'registerForm');
	echo form_open('shared/ajax_cek/add_user', $attributes);
?>
  <fieldset class="ui-widget ui-widget-content ui-corner-all">
  <legend class="ui-widget ui-widget-header ui-corner-all"><?php echo $this->lang->line('lbl_form_daftar'); ?></legend>
  <label for="name"><?php echo $this->lang->line('lbl_nama'); ?></label>
  <input type="text" name="name" id="name" autocomplete="off" maxlength="20" />
  <br />
  <label for="username"><?php echo $this->lang->line('lbl_id'); ?></label>
  <input type="text" name="username" id="username" autocomplete="off" maxlength="20" />
  <br />
  <label for="email"><?php echo $this->lang->line('lbl_email'); ?></label>
  <input type="text" name="email" id="email" autocomplete="off" maxlength="60" />
  <br />
  <label for="password"><?php echo $this->lang->line('lbl_kata_sandi'); ?></label>
  <input id="password" name="password" type="password" autocomplete="off" maxlength="20" />
  <br />
  <label for="confirm_password"><?php echo $this->lang->line('lbl_ulangi_kata_sandi'); ?></label>
  <input id="confirm_password" name="confirm_password" type="password" autocomplete="off" maxlength="20" />
  <br />
  <button class="submit" type="submit"><?php echo $this->lang->line('daftar'); ?></button>
  </fieldset>
  <div id="err_reg" class="error-txt"></div>
</form>

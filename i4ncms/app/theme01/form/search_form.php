<?php
	$attributes = array('class' => 'cmxform', 'id' => 'searchForm');
	echo form_open('shared/ajax_cek/cari', $attributes);
?>
  <fieldset class="ui-widget ui-widget-content ui-corner-all">
  <legend class="ui-widget ui-widget-header ui-corner-all"><?php echo $this->lang->line('lbl_form_cari'); ?></legend>
  <label for="tsch"><?php echo $this->lang->line('lbl_kata_pencarian'); ?></label>
  <input type="text" name="tsch" id="tsch" autocomplete="off" value="" maxlength="50"/>
  <br />
  <button class="submit" type="submit"><?php echo $this->lang->line('cari'); ?></button>
  </fieldset>
  <div id="err_sc" class="error-txt"></div>
</form>

<div align="center">
<?php echo anchor(current_lang().'sitemap','Sitemap'); ?>
</div>

<?php
if($this->session->userdata('usrid') && $this->session->userdata('kunci'))
$this->load->view_theme('profil', '', $this->config->item('theme_id').'/ajax/');
?>

<div id="tabs">

    <div id="timeout-info">
    Sesi Anda habis mohon refresh.
    <button id="rfp" class="submit" type="button">Refresh Halaman</button>
    </div>

  <ul>
    <li><a href="#tabs-1"><?php echo $this->lang->line('cari'); ?></a></li>
    <li><a href="#tabs-2" id="login"><?php echo $this->lang->line('masuk'); ?></a></li>
    <li><a href="#tabs-3"><?php echo $this->lang->line('daftar'); ?></a></li>
  </ul>
  <div id="tabs-1">
    <?php $this->load->view_theme('search_form', '', $this->config->item('theme_id').'/form/'); ?>
  </div>
  <div id="tabs-2">
  	<div id="loginForm" class="tsembunyi">
    <?php $this->load->view_theme('login_form', '', $this->config->item('theme_id').'/form/'); ?>
    </div>
    <div id="logout" class="tsembunyi">
		<div>
		<?php echo $this->lang->line('lbl_salam'); ?>
		<?php
		if($this->session->userdata('nmusr')) {
			echo '<span id="nmusr">'.humanize(bs_kode($this->session->userdata('nmusr'), TRUE)).'</span>';
			echo br(2);
			echo anchor( '#',$this->lang->line('lbl_btn_profil'), array('class'=>'fltlft', 'id'=>'profil') );
		}
		else {
			echo '<span id="nmusr"></span>';
		}
		?>
		</div>
		<?php echo anchor( 'logout',$this->lang->line('lbl_btn_keluar'), array('class'=>'fltrt', 'id'=>'btn-logout') ); ?>
		<div class="clearfloat"></div>
    </div>
  </div>
  <div id="tabs-3">
  	<?php $this->load->view_theme('register_form', '', $this->config->item('theme_id').'/form/'); ?>
  </div>
  
</div>

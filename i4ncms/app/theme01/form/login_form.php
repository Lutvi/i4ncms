<?php
	$attributes = array('class' => 'cmxform', 'id' => 'loginForms');
	echo form_open('shared/ajax_cek/lo_user', $attributes);
?>
  <fieldset class="ui-widget ui-widget-content ui-corner-all">
  <legend class="ui-widget ui-widget-header ui-corner-all"><?php echo $this->lang->line('lbl_form_masuk'); ?></legend>
  <label for="uame"><?php echo $this->lang->line('lbl_id'); ?></label>
  <input type="text" id="uname" name="uname" autocomplete="off" maxlength="20" />
  <br />
  <label for="upass"><?php echo $this->lang->line('lbl_kata_sandi'); ?></label>
  <input type="password" id="upass" name="upass" autocomplete="off" maxlength="20" />
  <br />
  <button class="submit" type="submit"><?php echo $this->lang->line('masuk'); ?></button>
  </fieldset>
  <div id="err_lo" class="error-txt"></div>
</form>

<?php if ( $wid_content == 'front' ): ?>
<div class="sidebar rounded">
	<h3 class="rounded"><?php echo $wid_title; ?></h3>
	<p><?php echo $wid_text; ?></p>
    <?php if ( ! empty($data_tabel) ): ?>
    <?php foreach ($data_tabel as $row): ?>
        	<strong><?php echo  $row->judul; ?></strong>
            <br  />
            <?php echo  $row->isi; ?>
        	<hr />
    <?php endforeach; ?>
    <?php else: ?>
    	<strong>Data widget ini masih kosong.</strong>
    <?php endif; ?>
</div>
<?php else: ?>
<!-- PopUp Admin Assets-->
<?php 
if ( ! empty($js) )
{
	foreach($js as $item)
	{ 
?>
<script type="text/javascript" src="<?php echo base_url().'assets/js/'.$item; ?>"></script>
<?php 
	}
}
?>
<script type="text/javascript" src="<?php echo base_url().'assets/js/widget/tbaru_widget/tbaru_widget-01.js'; ?>"></script>
<link href="<?php echo base_url().'assets/css/widget/tbaru_widget/tbaru_widget-01.css'; ?>" rel="stylesheet" type="text/css" />
<!-- End PopUp Admin Assets-->

<div id="tbaru" class="myform" style="width:90%">
	<h1><?php echo "[Admin] - " . humanize($wid_title); ?></h1>
    <?php 
	$attributes = array('class' => 'ftbaru', 'id' => 'tbaruform');
	echo form_open($this->config->item('admpath').'/atur_widget/admin_widget/tbaru_widget', $attributes); 
	?>
        <label>Judul<span class="small">Judul baru</span> </label>
        <input type="text" name="nama" value="<?php echo (validation_errors())?set_value('nama'):''; ?>" maxlength="200" size="100" />
        <br />
		<?php echo form_error('nama'); ?>
        <div style="clear:left"></div>
        <label>Isi<span class="small">Isi baru</span> </label>
        <div style="clear:left"></div>
		<?php echo form_error('isi'); ?>
        <?php echo form_fckeditor($fck_conf, (validation_errors())?set_value('isi'):''); ?>
        
		<div style="clear:left; height:30px;"></div>
		<button type="submit">Submit</button>&nbsp;&nbsp;<button type="button" id="batal-tbaru">Batal</button>
    <?php echo form_close(); ?>
</div>
<h5>List Data :</h5>
<button id="tambah-tbaru" class="control-button" style="display:none;">Tambah Data</button><button id="tutup-tbaru" class="control-button">Sembunyikan Form</button>
<div id="tabel_tbaru" style="width:90%; background:#E6E6FA; padding:10px; margin:0 auto;">
<?php if ( ! empty($data_tabel) ): ?>
<?php foreach ($data_tabel as $row): ?>
		<strong><?php echo  $row->judul; ?></strong>
		<br  />
		<?php echo  $row->isi; ?>
		<hr />
<?php endforeach; ?>
<?php else: ?>
	<strong>Data widget ini masih kosong.</strong>
<?php endif; ?>
</div>
<?php endif; ?>

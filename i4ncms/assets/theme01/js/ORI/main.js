function expForm() {
    $("#tabs").tabs({
        disabled: [0, 1, 2]
    });
    $("#searchForm").hide('fast');
    $("#loginForm").hide('fast');
    $("#registerForm").hide('fast');
    $("#timeout-info").show('fast')
}

function expForm2() {
    location.reload();
}

function validasiUpProfil() {
    if (jQuery.isFunction(jQuery.fn.validate)) {
        var f = $("input[name='Hydra']").val();
        var u = $("#username").val();
        $("#profilForm").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                },
                username: {
                    required: true,
                    minlength: 2,
                    maxlength: 20,
                    remote: {
                        url: BaseUrl + "shared/ajax_cek/up_uname",
                        type: "post",
                        data: {
                            Hydra: f,
                            username: function () {
                                return $("#pusername").val()
                            }
                        },
                        error: expForm2
                    }
                },
                email: {
                    required: true,
                    maxlength: 60,
                    email: true,
                    remote: {
                        url: BaseUrl + "shared/ajax_cek/up_uemail",
                        type: "post",
                        data: {
                            Hydra: f,
                            email: function () {
                                return $("#pemail").val()
                            }
                        },
                        error: expForm2
                    }
                },
                password: {
                    minlength: 6,
                    maxlength: 20
                },
                confirm_password: {
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#ppassword"
                }
            },
            messages: {
                name: {
                    required: vnamaKosong,
                    minlength: vnamaMin,
                    maxlength: vnamaMax
                },
                username: {
                    required: vnamaIdKosong,
                    minlength: vnamaIdMin,
                    maxlength: vnamaIdMax,
                    remote: jQuery.format(vnamaIdAda)
                },
                email: {
                    required: vemailKosong,
                    maxlength: vemailMax,
                    email: vemailSalah,
                    remote: jQuery.format(vemailAda)
                },
                password: {
                    minlength: vsandiMin,
                    maxlength: vsandiMax
                },
                confirm_password: {
                    minlength: vsandiMin,
                    maxlength: vsandiMax,
                    equalTo: vsandiSamkan
                }
            }
        });
    } else {}
}

function resetFormProfil() {
    var nama = $('#ori_name').val(),
        usrid = $('#ori_pusername').val(),
        uemail = $('#ori_pemail').val();
    $('#name').val(nama);
    $('#pusername').val(usrid);
    $('#pemail').val(uemail);
    $('#ppassword').val('');
    $('#pconfirm_password').val('');
    $('label.error').remove();
}
$(function () {
    var a = $("#isLogin").val();
    validasiUpProfil();
    if (a == 'login') {
        $("#tabs").tabs({
            disabled: [2]
        });
        $("#login").text('').text(txtKeluar);
        $("#loginForm").hide();
        $("#logout").show();
    } else {
        $("#tabs").tabs().find(".ui-tabs-nav").sortable({
            axis: 'x'
        });
        $("#login").text('').text(txtMasuk);
        $("#logout").hide();
        $("#loginForm").show();
    }
    if (jQuery.isFunction(jQuery.fn.cycle)) {
        $('#topBanner').cycle({
            fx: 'fade',
            fit: 1,
            delay: -4000,
            speed: 1500,
            timeout: 15000,
            slideExpr: 'a.img_links img'
        });
        $('#katas,#kbawah').cycle({
            fx: 'fade',
            fit: 1,
            delay: -4000,
            speed: 1500,
            timeout: 8000,
            slideExpr: 'a.img_links img'
        })
    } else {
        $('#topBanner,#katas,#kbawah').hide();
    }
    $("#profil-user").dialog({
        autoOpen: false,
        width: 350,
        resizable: false,
        position: {
            my: "center top",
            at: "center top",
            of: window
        },
        modal: true,
        create: function (event, ui) {
            $(event.target).parent().css('position', 'fixed');
        },
        buttons: [{
            text: "Update",
            click: function () {
                $("#profilForm").submit();
            }
        },
        {
            text: btnTutup,
            click: function () {
                $(this).dialog("close");
                resetFormProfil();
            }
        }],
        close: function () {
            $(this).dialog("close");
            resetFormProfil();
        }
    });
    $("#profil").button({
        icons: {
            primary: "ui-icon-person"
        }
    }).click(function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        $("#profil-user").data('url', url).dialog("open");
        return false;
    });
    $("#btn-logout").button({
        icons: {
            primary: "ui-icon-power"
        }
    });
    $(".tag-pill").button({
        icons: {
            primary: "ui-icon-tag"
        }
    });
});
$(window).load(function () {
    if ($.browser.msie) {
        if (window.PIE) {
            $('.rounded, .ui-corner-all').each(function () {
                PIE.attach(this);
            })
        }
    }
    $('#topForm').show();
    $('#topBanner,#katas,#kbawah').show();
});
$(document).ready(function () {
    var f = $("input[name='Hydra']").val();
    $('#rfp').click(function () {
        location.reload();
    });
    $('#btn-logout').click(function () {
        $.ajax({
            type: "POST",
            url: BaseUrl + "logout",
            data: {
                Hydra: f
            },
            success: function (a) {
                if (a == "false") {} else {
                    expForm2();
                }
            },
            error: expForm
        });
        return false
    });
    $('.iklan').bind('click', function(e) {
		var hit = $(e.currentTarget).find('span').text();
		if(hit == '')
		var hit = $(e.currentTarget).data('hits');
		
        $.ajax({
            type: "POST",
            url: BaseUrl + $(this).data('upurl'),
            data: {
                Hydra: f,
                hits:hit,
                iklan_id:$(this).data('idiklan')
            },
            success: function (a) {
				if( a !== 'false')
				{
					$(e.currentTarget).find('span').text(a);
					//alert(a);
				}
            },
            error: expForm
        });
    });
    $("#searchForm input:not(:submit)").addClass("ui-widget-content");
    $("#loginForms input:not(:submit)").addClass("ui-widget-content");
    $("#registerForm input:not(:submit)").addClass("ui-widget-content");
    $("#searchForm").submit(function () {
        var k = $('#tsch').val();
        $.ajax({
            type: "POST",
            url: BaseUrl + "shared/ajax_cek/cari",
            data: {
                Hydra: f,
                keyword: k
            },
            success: function (a) {
                if (a == "false") {
                    $('div#err_sc').empty().html(cariKosong);
                } else {
                    window.location.assign(BaseUrl + bhs + 'cari');
                }
            },
            error: expForm
        });
        return false;
    });
    $("#loginForms").submit(function () {
        var u = $('#uname').val(),
            p = $('#upass').val();
        if (u == '' && p == '') {
            $('div#err_lo').empty().html(namaSandiKosong);
        } else if (u == '') {
            $('div#err_lo').empty().html(namaKosong);
        } else if (p == '') {
            $('div#err_lo').empty().html(sandiKosong);
        } else {
            $.ajax({
                type: "POST",
                url: BaseUrl + "shared/ajax_cek/lo_user",
                dataType: 'json',
                data: {
                    Hydra: f,
                    uname: u,
                    upass: p
                },
                success: function (a) {
                    if (a.sts === true) {
                        $("#tabs").tabs({
                            disabled: [2]
                        });
                        $("#login").text('').text(txtKeluar);
                        $("#loginForm").hide();
                        $("#logout").show();
                        $("#nmusr").text(a.nmusr);
                        $("#isLogin").val('login');
                        $(".features_cont").show();
                        $(".no-login").hide();
                    } else {
                        $('div#err_lo').empty().html(blmTerdaftar);
                    }
                },
                error: expForm
            })
        }
        return false;
    });
    if (jQuery.isFunction(jQuery.fn.validate)) {
        $("#registerForm").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                },
                username: {
                    required: true,
                    minlength: 2,
                    maxlength: 20,
                    remote: {
                        url: BaseUrl + "shared/ajax_cek/uname",
                        type: "post",
                        data: {
                            Hydra: f,
                            username: function () {
                                return $("#username").val();
                            }
                        },
                        error: expForm
                    }
                },
                email: {
                    required: true,
                    maxlength: 60,
                    email: true,
                    remote: {
                        url: BaseUrl + "shared/ajax_cek/uemail",
                        type: "post",
                        data: {
                            Hydra: f,
                            email: function () {
                                return $("#email").val();
                            }
                        },
                        error: expForm
                    }
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                name: {
                    required: vnamaKosong,
                    minlength: vnamaMin,
                    maxlength: vnamaMax
                },
                username: {
                    required: vnamaIdKosong,
                    minlength: vnamaIdMin,
                    maxlength: vnamaIdMax,
                    remote: jQuery.format(vnamaIdAda)
                },
                email: {
                    required: vemailKosong,
                    maxlength: vemailMax,
                    email: vemailSalah,
                    remote: jQuery.format(vemailAda)
                },
                password: {
                    required: vsandilKosong,
                    minlength: vsandiMin,
                    maxlength: vsandiMax
                },
                confirm_password: {
                    required: vsandiUlang,
                    minlength: vsandiMin,
                    maxlength: vsandiMax,
                    equalTo: vsandiSamkan
                }
            },
            submitHandler: function () {
                var u = $('#username').val(),
                    e = $('#email').val(),
                    p = $('#confirm_password').val();
                $.ajax({
                    type: "POST",
                    url: BaseUrl + "shared/ajax_cek/add_user",
                    data: $("#registerForm").serialize(),
                    success: function (a) {
                        if (a == "success") {
                            $('#registerForm').remove();
                            $('#tabs-3').empty().html(daftarSukses);
                        } else if (a === "error") {
                            $('div#err_reg').empty().html(daftarGagalData);
                        } else if (a === "failed") {
                            $('div#err_reg').empty().html(daftarIlegal);
                            $('form#registerForm')[0].reset();
                        } else {
                            $('#registerForm').remove();
                            $('#tabs-3').empty().html(daftarIlegal);
                        }
                    },
                    error: expForm
                });
            }
        });
    } else {
        $("#registerForm").submit(function () {
            var u = $('#username').val(),
                e = $('#email').val(),
                p = $('#confirm_password').val();
            if (u != '' && e != '' && p != '') {
                $.ajax({
                    type: "POST",
                    url: BaseUrl + "shared/ajax_cek/add_user",
                    data: $("#registerForm").serialize(),
                    success: function (a) {
                        if (a == "success") {
                            $('form#registerForm')[0].reset();
                            $('#registerForm').hide();
                            $('#tabs-3').empty().html(daftarSukses);
                        } else if (a === "error") {
                            $('div#err_reg').empty().html(daftarGagalData);
                        } else if (a === "failed") {
                            $('div#err_reg').empty().html(daftarIlegal);
                            $('form#registerForm')[0].reset();
                        } else {
                            $('#registerForm').hide();
                            $('#tabs-3').empty().html(daftarIlegal);
                        }
                    },
                    error: function (a) {
                        if (a == "failed" || a == "error" || a == "") {
                            $('form#registerForm')[0].reset();
                            $('#registerForm').hide();
                            $("#tabs").tabs({
                                disabled: [2]
                            });
                        } else {
                            expForm
                        }
                    }
                })
            } else {
                $('div#err_reg').empty().html(formKosong);
            }
            return false;
        });
    }
    if (jQuery.isFunction(jQuery.fn.fancybox)) {
        function formatTitle(a, b, c, d) {
            id = (c + 1);
            return '<span id="fancybox-title-over">' + $('#tampil-' + id).html() + '</span>';
        }
        $("a.album").attr('rel', 'album_group').fancybox({
            'titlePosition': 'over',
            'centerOnScroll': true,
            'titleFormat': formatTitle,
            'onStart': function () {}
        });

        
    }
});
$(function () {
    $('img.efek').each(function (n) {
        n += 1;
        $(this).wrap('<figure class="tint"></figure>');
    });
});

//Jssor
jQuery(document).ready(function ($) {
	var options = {
		$AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
		$AutoPlayInterval: 9000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
		$PauseOnHover: 0,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3
		$ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
		$SlideDuration: 900,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
		$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
		//$SlideWidth: 1100,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
		$SlideHeight: 323,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
		$SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
		$DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
		$ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
		$UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, direction navigator container, thumbnail navigator container etc).
		$PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
		$DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

		$DirectionNavigatorOptions: {                       //[Optional] Options to specify and enable direction navigator or not
			$Class: $JssorDirectionNavigator$,              //[Requried] Class to create direction navigator instance
			$ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$AutoCenter: 3,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
			$Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
		},

		$ThumbnailNavigatorOptions: {
			$Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
			$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
			$AutoCenter: 1,                                 //[Optional] Auto center thumbnail items in the thumbnail navigator container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 3
			$Lanes: 1,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
			$SpacingX: 3,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
			$SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
			$DisplayPieces: 9,                              //[Optional] Number of pieces to display, default value is 1
			$ParkingPosition: 260,                          //[Optional] The offset position to park thumbnail
			$Orientation: 1,                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
			$DisableDrag: false                            //[Optional] Disable drag or not, default value is false
		}
	};

	var jssor_slider1 = new $JssorSlider$("slider1_container", options);

	//responsive code begin
	//you can remove responsive code if you don't want the slider scales while window resizes
	function ScaleSlider() {
		var bodyWidth = document.body.clientWidth;
		if (bodyWidth)
			jssor_slider1.$SetScaleWidth(Math.min(bodyWidth, 1100));
		else
			window.setTimeout(ScaleSlider, 30);
	}
		ScaleSlider();

	if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
		$(window).bind('resize', ScaleSlider);
	}
	//responsive code end
});

//Rating
$(function(){
	$('#ratingForm input').on('change', function() {
	   $.ajax({
            type: "POST",
            url: BaseUrl + "shared/ajax_cek/upratting",
            data: $('#ratingForm').serialize(),
			dataType:'json',
            success: function (a) {
                if (a == "false") {
                    alert('Error.');
                } else {
                    $('#ratingForm').hide();
					$('#ratingRes').show();
					$('#ratingNrat').html(a.nilai);
					$('#ratingNvot').html(a.jvote);
                }
            },
            error: expForm
        });
	});
});
// JavaScript Document
$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
  });
};

$(document).ready(function(){
	$('#batal-tbaru').click(function(){
		$('#tbaruform').clearForm();
		FCKeditorAPI.GetInstance('isi').SetHTML('');
		$('.error').hide();
	});
	$('#tutup-tbaru').click(function(){
		$('#tbaru').hide();
		$('#tambah-tbaru').show();
		$('#tbaruform').clearForm();
		$('.error').hide();
		$(this).hide();
	});
	$('#tambah-tbaru').click(function(){
		$('#tbaru').show();
		$('#tutup-tbaru').show();
		$(this).hide();
	});
});

// JavaScript Document

$(document).ready(function()
{
    var m = $('#isLogin').val(), h = $('#isHome').val();
        
    if (jQuery.isFunction(jQuery.fn.tabs)) {
       // Tab article
        if(h == 'home')
        {
            $("#artTabs").tabs({collapsible: true,selected: -1}).find(".ui-tabs-nav").sortable({axis:'x'});
        } else {
            $("#artTabs").tabs().find(".ui-tabs-nav").sortable({axis:'x'});
        }
        
        // Features
        if(m == '')
        {
            $(".features_cont").hide();
            $(".no-login").show();
        } else {
            $(".no-login").hide();
            $(".features_cont").show();
        }
    }
});

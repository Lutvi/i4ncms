/*
 * http://www.developerbarn.com/community/members/richyrich.9/
 */

(function ($) {
    $.fn.formatNumber = function (seperator, every, precision) {
    var val = '';
    if(this.is("span")||this.is("div")){
    val += this.text();
    }else{
    val += this.val();
    }
    val = parseFloat(val).toFixed(precision);
    val += '';
    var arr = val.split('.',2);
    var i = parseInt(arr[0]);
    if(isNaN(i)) return '';
    i = Math.abs(i);
    var n = new String(i);
    var d = arr.length > 1 ? '.' + arr[1] : '';
    var a = [];
    var nn;
    while(n.length > every)
    {
    nn = n.substr(n.length-every);
    a.unshift(nn);
    n = n.substr(0,n.length-every);
    }
    if(n.length > 0) a.unshift(n);
    n = a.join(seperator);
    if(this.is("span")||this.is("div")){
    this.text(n + d);
    }else{
    this.val(n + d);
    }
    };
})(jQuery);


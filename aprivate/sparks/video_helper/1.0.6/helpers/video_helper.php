<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * MarcoMonteiro Video Helper
 *
 * Serdar Senay (Lupelius)
 * Fix applied where all methods had unnecessary if checks for checking
 * valid ID, removed those as youtube|vimeo_id functions already check that
 * Also added vimeo to _isValidID check, and added vimeo_fullvideo method
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Marco Monteiro
 * @link			www.marcomonteiro.net
 * @version 	1.0.5
 */

// ------------------------------------------------------------------------

//error handler function just for post action
function customError($errno)
{
	$a = debug_backtrace();
	if($a[1]['function'] == 'file_get_contents')
	{
		if (isset($_POST['src_video']))
		{
			$args = $_POST['src_video'];
			echo '<div style="padding:10px;text-align:center;background:#A52A2A;color:#FFA500">';
			echo 'Oh No..! URL Video "<a href="'.$args.'" target="_blank" style="color:#FFFFFF">'.$args.'</a>" tidak ditemukan.!!</div>';
		}
		else {
			//echo '';
		}
	}
}

//set custom error handler
set_error_handler("customError");

/**
 * Get Youtube Id
 *
 * @access	public
 * @param	string	Youtube url
 * @return	string	Youtube ID
 */
if ( ! function_exists('youtube_id'))
{
	function youtube_id( $url = '')
	{
		if ( $url === '' )
		{
			return FALSE;
		}
		if (! _isValidURL( $url ))
		{
			return FALSE;
		}

		preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);

		if(!$matches) {
			return FALSE;
		}
		else {
			return $matches[0];
		}
	}
}


/**
 * Get Vimeo Id
 *
 * @access	public
 * @param	string	Vimeo url
 * @return	string	Vimeo ID
 */
if ( ! function_exists('vimeo_id'))
{
	function vimeo_id( $url = '')
	{
		if ( $url === '' )
		{
			return FALSE;
		}

		if (_isValidURL( $url ))
		{
			sscanf(parse_url($url, PHP_URL_PATH), '/%d', $vimeo_id);
		}
		else {
			preg_match_all("/(.*)(\/vimeo.com\/)(\d+)/", $url, $matches);
			$vimeo_id = $matches[3][0];
		}

		if(!empty($vimeo_id))
		return $vimeo_id;
		else
		return FALSE;
	}
}

/**
 *Get youtube video page
 *
 * @access	public
 * @param	string	Youtube url || Youtube id
 * @return	$array	url's video
 */
 if ( ! function_exists('youtube_fullvideo'))
 {
 	function youtube_fullvideo( $url_id = '' )
 	{
		$id = youtube_id( $url_id );

		$base = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		
		return ($id) ? $base.'://www.youtube.com/v/'.$id.'?rel=0&hd=1&showsearch=0&showinfo=0' : FALSE;
 	}
 }

 /**
 *Get vimeo video page
 *
 * @access	public
 * @param	string	Vimeo ID
 * @return	$array	url's video
 */
 if ( ! function_exists('vimeo_fullvideo'))
 {
 	function vimeo_fullvideo( $url_id = '' )
 	{
		$id = vimeo_id( $url_id );
		$base = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		
		return ($id) ? $base.'://player.vimeo.com/video/'.$id.'?loop=1&title=0&badge=0&byline=0' : FALSE;
 	}
 }

/**
 * Get Youtube thumbs
 *
 * @access	public
 * @param	string	Youtube url || Youtube id
 * @param 	number	1 to 4 to return a specific thumb
 * @return	array		url's to thumbs or specific thumb
 */
if ( ! function_exists('youtube_thumbs'))
{
	function youtube_thumbs( $url_id = '', $thumb = '')
	{
		if ( $url_id === '' )
		{
			return FALSE;
		}
		if ($thumb > 4 || $thumb < 1)
		{
			return FALSE;
		}
		if ( _isValidID( $url_id ) )
		{
			$id = $url_id;
		}
		else{
			$id = youtube_id( $url_id );
		}

		$result = array(
			'1' => 'http://img.youtube.com/vi/'.$id.'/0.jpg',
			'2' => 'http://img.youtube.com/vi/'.$id.'/1.jpg',
			'3' => 'http://img.youtube.com/vi/'.$id.'/2.jpg',
			'4' => 'http://img.youtube.com/vi/'.$id.'/3.jpg'
		);

		if ( $thumb == '' ){
			return $result;
		}
		else
		{
			return $result[$thumb];
		}
	}
}



/**
 * Get Vimeo thumbs
 *
 * @access	public
 * @param	string		Vimeo url || Vimeo id
 * @param 	number 		1 to 3 to return a specific thumb
 * @return	array 		url's to thumbs or specific thumb
 */
if ( ! function_exists('vimeo_thumbs'))
{
	function vimeo_thumbs( $url_id = '', $thumb = '')
	{
		if ( $url_id == '' )
		{
			return FALSE;
		}
		if ( $thumb < 1 || $thumb > 3 )
		{
			return FALSE;
		}
		if ( !_isValidURL( $url_id ) )
		{
			$id = $url_id;
		}
		else{
			$id = vimeo_id( $url_id );
		}

		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));

		$result = array(
			'3' => $hash[0]['thumbnail_small'],
			'2' => $hash[0]['thumbnail_medium'],
			'1' => $hash[0]['thumbnail_large']
		);


 		if ( $thumb == '' ){
			return $result;
		}
		else
		{
			return $result[$thumb];
		}
	}
}



/**
 * Get Youtube embed
 *
 * @access	public
 * @param	string		Youtube url || Youtube id
 * @param 	number 		width
 * @param   number 		height
 * @param   boolean 		old embed / default = FALSE
 * @param   boolean 		HD / default = FALSE / The width and height will not be used if passed
 * @param   boolean 		https / default = FALSE
 * @param   boolean 		suggested videos / default = FALSE
 * @return	string   	embebed code
 */

if ( ! function_exists('youtube_embed'))
{
	function youtube_embed( $url_id = '', $width = '', $height = '',
	$hd = FALSE, $old_embed = FALSE, $suggested = FALSE)
	{
		$id = youtube_id( $url_id );
		$base = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");

		//Contruct the old embed code
		if ( $old_embed )
		{
			if ( $hd )
			{
				$embed = '<object width="640" height="360">';
			}
			else
			{
				$embed = '<object width="'.$width.'" height="'.$height.'">';
			}
			$embed .= '<param name="movie" value="';
			$embed .= $base.'://www.youtube-nocookie.com/v/'.$id.'?wmode=transparent&amp;version=3&amp;hl=en_US&amp;fs=1&amp;showinfo=0';
			if ( $suggested )
			{
				$embed .= 'rel=0&amp;';
			}
			if ( $hd )
			{
				$embed .= 'hd=1';
			}
			$embed .= '"></param>';
			$embed .= '<param name="allowFullScreen" value="true"></param>';
			$embed .= '<param name="allowscriptaccess" value="always"></param>';
			$embed .= '<embed width="640" height="360" src="';
			$embed .= $base.'://www.youtube-nocookie.com/v/'.$id.'?wmode=transparent&amp;version=3&amp;hl=en_US&amp;fs=1&amp;showinfo=0';
			if ( $hd )
			{
				$embed .= '&amp;hd=1';
			}
			$embed .= '" type="application/x-shockwave-flash" ';
			if ( $hd )
			{
				$embed .= ' width="640" height="360" ';
			}
			else
			{
				$embed .= 'width="'.$width.'" height="'.$height.'" ';
			}
			$embed .= 'allowscriptaccess="always" allowfullscreen="true"></embed>';
			$embed .= '</object>';
		}
		//Contruct the new embed code
		else
		{
			$embed = '<iframe style="z-index:100" ';
			if ( $hd )
			{
				$embed .= ' width="640" height="360" ';
			}
			else
			{
				$embed .= 'width="'.$width.'" height="'.$height.'" ';
			}
			$embed .= 'src="';
			$embed .= $base.'://www.youtube.com/embed/'.$id;

			if ( $suggested )
			{
				$embed .= '?wmode=transparent&rel=1&fs=1&showinfo=0';
			}
			else{
				$embed .= '?wmode=transparent&rel=0&fs=1&showinfo=0';
			}

			if ( $hd )
			{
				$embed .= '&ap=%2526fmt%3D18&ap=%2526fmt%3D22';
			}

			$embed .= '" frameborder="0" allowfullscreen></iframe>';
		}
		return $embed;
	}
}



/**
 * Get Vimeo embed
 *
 * @access	public
 * @param	string		Vimeo url || Vimeo id
 * @param 	number 		width
 * @param   number 		height
 * @param   boolean 		color
 * @param   boolean 		autoplay / default = FALSE
 * @return	string   	embebed code
 */

if ( ! function_exists('vimeo_embed'))
{
	function vimeo_embed( $url_id = '', $width ='640', $height = '360',
	$color = '', $title = FALSE, $autoplay = FALSE)
	{
		$id = vimeo_id( $url_id );
		$base = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");

		$embed = '<iframe src="'.$base.'://player.vimeo.com/video/'.$id.'?byline=0&amp;portrait=0&amp;title=0&amp;';
		if ( $color != '' )
		{
			$embed .= 'color='.$color.'&amp;';
		}
		if ( $autoplay )
		{
			$embed .= 'autoplay=1';
		}

		$embed .= '" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

		return $embed;
	}
}


/**
 * Validate URL
 * This URL could have http or just www
 *
 * @access private
 * @param string 		Youtube URL
 * @return preg_match
 */
if ( ! function_exists('_isValidURL'))
{
	function _isValidURL($url = '')
	{
		return preg_match('/^(http|https):\/\/([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?/i', $url);
	}
}


/**
 * Validate ID
 * Check if the id is valid or not
 *
 * @access private
 * @param string 		Youtube ID OR Vimeo ID
 * @return boolean
 */
if ( ! function_exists('_isValidID'))
{
	function _isValidID($id = '', $vimeo=FALSE)
	{
		if ($vimeo)
		$get_http_response_code = get_http_response_code('http://vimeo.com/' . $id);
		else
		$get_http_response_code = get_http_response_code('http://gdata.youtube.com/feeds/api/videos/' . $id);

		if ( $get_http_response_code == 200 )
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

function get_http_response_code($domain)
{
	$headers = get_headers($domain);

	return substr($headers[0], 9, 3);
}

// ------------------------------------------------------------------------

/* End of file video_helper.php */
/* Location: ./application/helpers/video_helper.php */

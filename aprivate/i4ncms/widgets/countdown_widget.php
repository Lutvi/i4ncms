<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countdown_widget extends Widget {

	public function display($args)
	{
		$data['wid_title'] = $args['wid_title'];
		if ( empty($args['wid_content']) )
		{
			$data['wid_content'] = 'front';
		}

		$this->load->view_theme('countdown_widget_view', $data,'widgets/');
	}
}

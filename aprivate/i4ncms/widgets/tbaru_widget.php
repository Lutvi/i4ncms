<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tbaru_widget extends Widget {
	
	public function display($args) {
		
		$this->load->model('widgets/model_tbaru_widget','tbaru');
		$this->load->helper('form');
		
		$data['wid_title'] = $args['wid_title'];
		$data['wid_text'] = '';
		if ( empty($args['wid_content']) )
		{
			$data['data_tabel'] = $this->tbaru->getAllEntries();
			$data['wid_content'] = 'front';
		}
		else {
			$data['wid_content'] = 'admin';
			$data['js'] = $args['js'];
			$data['fck_conf'] = array('name' => 'isi','id' => 'isi','toolbarset' => 'Basic','width' => '90%','height' => '200px');
		}
		
		if ( $_POST && $this->uri->segment(1) === $this->config->item('admpath') )
		{
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required|callback__ada_cek');
			$this->form_validation->set_rules('isi', 'Isi', 'trim|required');
			if ( $this->form_validation->run($this) == FALSE )
			{
				$data['title'] = "Terjadi Kesalahan, Gagal tambahkan data baru.!";
			}
			else {
				if ( ! $this->tbaru->addData() )
				{
					$data['title'] = "Gagal tambahkan data baru.";
				}
				else {
					$data['title'] = "Sukses data telah tersimpan.";
				}
			}
			$data['data_tabel'] = $this->tbaru->getAllEntries();
		}
		
		$this->load->view_theme('tbaru_widget_view', $data,'widgets/');
	}
	
	function _ada_cek($str)
	{
		if ( $this->tbaru->adaNama($str) == FALSE )
		{
			$this->form_validation->set_message('_ada_cek', 'Nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

}

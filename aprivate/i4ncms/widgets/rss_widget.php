<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rss_widget extends Widget {

	public function display($args) {
		// set external / internal
		$data['tipe_rss'] = 'internal';
		// external feed
			//$feed = 'http://detikinet.com/index.php/detik.feed';
			//$feed = 'http://reddanio.16mb.com/mod_rss/index';
		// internal feed
			$feed = RSSURL.'mod_rss/index';
		$this->cisimplepie->set_feed_url($feed);
		$this->cisimplepie->init();
		$data['rss_items'] = $this->cisimplepie->get_items();
		$data['wid_title'] = $args['wid_title'];

		$this->load->view('widgets/rss', $data);
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mod_rss.php
 * 
 * Copyright 2014 Inp <fun9uy5@gmail.com>
 * 
 * Modul untuk generate RSS Document Powered by : RDS-Corp.
 * Lisensi : Non-commercial, harus mendapat persetujuan dari author.
 * 
 */

class Mod_rss extends FrontController {

	function __construct()
	{
		parent::__construct();
		//load helper
		$this->load->helper(array('xml','text','url','string'));
		//To Enable RSS Content without compressing content
		$this->config->set_item('compress_output', FALSE);

		$this->extra_url = $this->uri->slash_segment(3, 'both').$this->uri->segment(4).($this->uri->segment(5)?$this->uri->slash_segment(5, 'both'):'').$this->uri->segment(6);

		//header('Content-type: text/xml');
		header("Content-type: application/xml");
	}

	function index()
	{
		$data['feed_name'] = config_item('site_name');
		$data['encoding'] = 'utf-8';
		$data['feed_url'] = RSSURL.current_lang().'rss2';
		$data['page_description'] = config_item('sort_site_description');
		$data['page_language'] = current_lang(FALSE);
		$data['creator_email'] = config_item('owner_email');
		$data['extra_url'] = $this->extra_url;

		if($this->uri->segment(3) == 'posting')
		{
			$nama = humanize($this->uri->segment(4));
			$data['konten_blog'][] = $this->cms->getAllBlogFrontByNama($nama);
		}
		elseif($this->uri->segment(3) == 'gallery')
		{
			$nama = humanize($this->uri->segment(4));
			$data['gambar_album'][] = $this->cms->getAllAlbumFrontByNama($nama);
		}
		elseif($this->uri->segment(3) == 'playlist')
		{
			$nama = humanize($this->uri->segment(4));
			$data['video'][] = $this->cms->getAllVideoFrontByNama($nama);
		}
		else {
			if($this->uri->segment(5) == 'kategori' || $this->uri->segment(5) == 'category' || $this->uri->segment(6) == 'kategori' || $this->uri->segment(6) == 'category')
			{
				$tipe = $this->uri->segment(3,'blog');
				$data = array_merge($this->_getKontenKategori($tipe), $data);
			}
			elseif($this->uri->segment(5) == 'tag' || $this->uri->segment(6) == 'tag')
			{
				$tipe = $this->uri->segment(3,'blog');
				$tag = $this->uri->segment(4);
				$data = array_merge($this->_getKontenTag($tipe,$tag), $data);
			}
			else {
				if($this->uri->segment(3) === FALSE || $this->uri->segment(3) == 'home') {
					$data['konten_blog'] = $this->cms->cariHomeKonten(FALSE,'blog',$this->jknt_home,0);
					$data['gambar_album'] = $this->cms->cariHomeKonten(FALSE,'album',$this->jknt_home,0);
					$data['video'] = $this->cms->cariHomeKonten(FALSE,'video',$this->jknt_home,0);
				}
				else
					$data = array_merge($this->_getKonten(FALSE), $data);
			}
		}

		xml_convert(format_rss2($data));
	}

	function atom()
	{
		$data['feed_name'] = config_item('site_name');
		$data['encoding'] = 'utf-8';
		$data['feed_url'] = RSSURL.current_lang().'atom';
		$data['page_description'] = config_item('sort_site_description');
		$data['page_language'] = current_lang(FALSE);
		$data['creator_email'] = config_item('owner_email');
		$data['extra_url'] = $this->extra_url;

		if($this->uri->segment(3) == 'posting')
		{
			$nama = humanize($this->uri->segment(4));
			$data['konten_blog'][] = $this->cms->getAllBlogFrontByNama($nama);
		}
		elseif($this->uri->segment(3) == 'gallery')
		{
			$nama = humanize($this->uri->segment(4));
			$data['gambar_album'][] = $this->cms->getAllAlbumFrontByNama($nama);
		}
		elseif($this->uri->segment(3) == 'playlist')
		{
			$nama = humanize($this->uri->segment(4));
			$data['video'][] = $this->cms->getAllVideoFrontByNama($nama);
		}
		else {
			if($this->uri->segment(5) == 'kategori' || $this->uri->segment(5) == 'category' || $this->uri->segment(6) == 'kategori' || $this->uri->segment(6) == 'category')
			{
				$tipe = $this->uri->segment(3,'blog');
				$data = array_merge($this->_getKontenKategori($tipe), $data);
			}
			elseif($this->uri->segment(5) == 'tag' || $this->uri->segment(6) == 'tag')
			{
				$tipe = $this->uri->segment(3,'blog');
				$tag = $this->uri->segment(4);
				$data = array_merge($this->_getKontenTag($tipe,$tag), $data);
			}
			else {
				if($this->uri->segment(3) === FALSE || $this->uri->segment(3) == 'home') {
					$data['konten_blog'] = $this->cms->cariHomeKonten(FALSE,'blog',$this->jknt_home,0);
					$data['gambar_album'] = $this->cms->cariHomeKonten(FALSE,'album',$this->jknt_home,0);
					$data['video'] = $this->cms->cariHomeKonten(FALSE,'video',$this->jknt_home,0);
				}
				else
					$data = array_merge($this->_getKonten(FALSE), $data);
			}
		}

		xml_convert(format_atom($data));
	}

	function rdf()
	{
		$data['feed_name'] = config_item('site_name');
		$data['encoding'] = 'utf-8';
		$data['feed_url'] = RSSURL.current_lang().'rdf';
		$data['page_description'] = config_item('sort_site_description');
		$data['page_language'] = current_lang(FALSE);
		$data['creator_email'] = config_item('owner_email');
		$data['extra_url'] = $this->extra_url;

		if($this->uri->segment(3) == 'posting')
		{
			$nama = humanize($this->uri->segment(4));
			$data['konten_blog'][] = $this->cms->getAllBlogFrontByNama($nama);
		}
		elseif($this->uri->segment(3) == 'gallery')
		{
			$nama = humanize($this->uri->segment(4));
			$data['gambar_album'][] = $this->cms->getAllAlbumFrontByNama($nama);
		}
		elseif($this->uri->segment(3) == 'playlist')
		{
			$nama = humanize($this->uri->segment(4));
			$data['video'][] = $this->cms->getAllVideoFrontByNama($nama);
		}
		else {
			if($this->uri->segment(5) == 'kategori' || $this->uri->segment(5) == 'category' || $this->uri->segment(6) == 'kategori' || $this->uri->segment(6) == 'category')
			{
				$tipe = $this->uri->segment(3,'blog');
				$data = array_merge($this->_getKontenKategori($tipe), $data);
			}
			elseif($this->uri->segment(5) == 'tag' || $this->uri->segment(6) == 'tag')
			{
				$tipe = $this->uri->segment(3,'blog');
				$tag = $this->uri->segment(4);
				$data = array_merge($this->_getKontenTag($tipe,$tag), $data);
			}
			else {
				if($this->uri->segment(3) === FALSE || $this->uri->segment(3) == 'home') {
					$data['konten_blog'] = $this->cms->cariHomeKonten(FALSE,'blog',$this->jknt_home,0);
					$data['gambar_album'] = $this->cms->cariHomeKonten(FALSE,'album',$this->jknt_home,0);
					$data['video'] = $this->cms->cariHomeKonten(FALSE,'video',$this->jknt_home,0);
				}
				else
					$data = array_merge($this->_getKonten(FALSE), $data);
			}
		}

		xml_convert(format_rdf($data));
	}

}

/* End of file mod_rss.php */
/* Location: ./application/modules/mod_product/controllers/mod_rss.php */

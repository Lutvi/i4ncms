<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_artikel extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('artikelmodel','artikel');
		$this->load->library('pagination');
	}

	function list_artikel()
	{
		//echo $this->session->userdata('fbasis');
		
		if($this->session->userdata('fbasis') == current_lang().'home')
		{
			$config['base_url'] = base_url().current_lang().$this->uri->segment(2,$this->session->userdata('fbasis'));
			$config['uri_segment'] = 3;
		}
		else {
			$config['base_url'] = base_url().$this->session->userdata('fbasis');
			$config['uri_segment'] = 5;
		}
		
        if ( ($this->mobiledetection->isMobile() || ENVIRONMENT === 'm-testing') && ! $this->config->item('is_respon') ){
            $config['num_links'] = 2;
            $config['per_page'] = 1;
            $config['total_rows'] = $this->artikel->getCountEntries() - 1;
        }else{
            $config['num_links'] = 4;
            $config['per_page'] = 5;
            $config['total_rows'] = $this->artikel->getCountEntries();
        }
        
        $config['next_link'] = '&rsaquo;';
		$config['prev_link'] = '&lsaquo;';
		
        $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		if($this->session->userdata('fbasis') == current_lang().'home')
		{
			$offset = $this->uri->segment(3,0);
		} else {
			$offset = $this->uri->segment(5,0);
		}
		
		$data['artikel'] = $this->artikel->getAllEntries($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		
		$this->load->view('artikel',$data);
	}
	
	function test()
	{
		$this->config->set_item('compress_output', FALSE);
		
		$attr = array('target' => '_blank');
		print_r("TESTING..!");
		$a = anchor('home','HOME',$attr);
		print_r($a);
	}
}

/* End of file mod_artikel.php */
/* Location: ./application/modules/mod_artikel/controllers/mod_artikel.php */

<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Artikelmodel extends CI_Model {

    var $tbl_artikel   = 'tbl_mod_artikel';

    function __construct()
    {
        // Call the Model constructor
		$this->db = $this->load->database('default', TRUE);
        parent::__construct();
    }
    
    function getAllEntries($limit, $offset)
    {
        $this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_artikel);
        if($query->num_rows > 0)
		{
			return $query->result();
		}else{
			return NULL;
		}
    }
	
	function getAllEntriesMob()
    {
		$query = $this->db->get($this->tbl_artikel);
        if($query->num_rows > 0)
		{
			return $query->result();
		}else{
			return NULL;
		}
    }
	
	function getCountEntries()
	{
		 $query = $this->db->get($this->tbl_artikel);
		 return $query->num_rows(); 
	}
	
	function getSelectedPost($id)
	{
		$query = $this->db->get_where($this->tbl_artikel, array('id' => $id));
		return $query;
	}

}

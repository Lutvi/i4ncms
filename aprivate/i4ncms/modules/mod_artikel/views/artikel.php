<?php if(is_array($artikel)): // cek array-data ?>
<input type="hidden" readonly="true" value="<?php echo $this->session->userdata('fbasis'); ?>" id="isHome" />
    <?php if ( ($this->mobiledetection->isMobile()  && $this->config->item('is_respon') === FALSE) || ENVIRONMENT === 'm-testing' ): ?>
        <div id="<?php echo ($this->config->item('slider_menu') === TRUE)?'pagination':'pagination2'; ?>" data-role="pagination"><?php echo $page; ?></div>
        </div>
            <?php foreach ($artikel as $rows): ?>
            <h2 class="judul-art"><?php echo $rows->judul_id; ?></h2>
            <div class="artWrap"><?php echo $rows->isi_id; ?></div>
            <?php endforeach; ?> 
        </div>
    <?php else: ?>
        <div id="pagination"><?php echo $page; ?></div>
            <div id="artTabs" style="clear:left;" class="rounded">
             <ul style="height:34px;" class="rounded">
             <?php foreach ($artikel as $row): ?>
                <li class="rounded"><a href="#artTabs-<?php echo $row->id; ?>"><?php echo $row->judul_id; ?></a></li>
             <?php endforeach; ?> 
             </ul>   
             <?php foreach ($artikel as $rows): ?>
             <div id="artTabs-<?php echo $rows->id; ?>" class="rounded">
             <div class="no-login">Silahkan masuk dahulu, untuk menggunakan fitur artikel.</div>
             <div style="font-size:12px; display:none;" class="features_cont">
             <?php
                echo br();
                echo anchor('extra/'. $rows->id .'/generate_pdf','Unduh PDF', array('class' => 'features'));
                echo nbs(3).'|'.nbs(3);
                $att_email = array(
                      'class'	   => 'features',
                      'width'      => '500',
                      'height'     => '300',
                      'scrollbars' => 'yes',
                      'status'     => 'no',
                      'resizable'  => 'yes',
                      'screenx'    => '0',
                      'screeny'    => '0'
                    );
                echo anchor_popup('features/form_send_post/'.$rows->id,'Kirim ke Email', $att_email);
                echo nbs(3).'|'.nbs(3);
                $att_print = array(
                      'class'	   => 'features',
                      'width'      => '800',
                      'height'     => '600',
                      'scrollbars' => 'yes',
                      'status'     => 'no',
                      'resizable'  => 'yes',
                      'screenx'    => '0',
                      'screeny'    => '0'
                    );
        
                echo anchor_popup('features/print_post/'.$rows->id, 'Cetak artikel', $att_print);
             ?>
             </div>
                    <h2 class="judul-art"><?php echo $rows->judul_id; ?></h2>
                    <div class="artWrap"><?php echo $rows->isi_id; ?></div>
             </div>
             <?php endforeach; ?>  
            </div><!-- end #art_tabs -->
        <?php
    endif; // mobile cek
else:
?>
	<div id="artTabs" style="clear:left; margin-top:20px; text-align:center;" class="rounded">
	No Results
	</div>
<?php
endif; // cek array-data
?>

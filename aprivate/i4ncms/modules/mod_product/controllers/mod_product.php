<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_product extends MY_Controller {

	var $mod_view_path = 'modules/mod_product/views/';
	var $mod_url_path = 'mod_product/mod_product/';
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		//print_r($this->db->get('tbl_artikel'));
		$this->tampil_produk();
	}
	
	function tampil_produk()
	{
		$this->load->library('table');
		$query = $this->db->query("SELECT id,id_kategori,judul_id,author FROM tbl_mod_artikel");
		$data['tbl'] = $this->table->generate($query); 
		
		$this->load->view('product',$data);
	}
}

/* End of file mod_product.php */
/* Location: ./application/modules/mod_product/controllers/mod_product.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_admin_product extends MY_Controller {

	var $mod_view_path = 'modules/mod_product/views/';
	var $mod_url_path = 'mod_product/mod_admin_product/';
	
	function __construct()
	{
		parent::__construct();
		$this->config->set_item('compress_output', FALSE);
	}

	function index()
	{
		$this->load->view('admin_product');		
	}
	
	function cek()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		/*$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');*/

		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}
		else {
			echo $this->input->post('username');
		}
	}
	
	function tambah_data()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}
		else {
			echo $this->input->post('username');
		}	
	}
	
}

/* End of file mod_admin_product.php */
/* Location: ./application/modules/mod_product/controllers/mod_admin_product.php */

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// front
$route['product'] = "mod_product/mod_product/tampil_produk";

// admin
$route['admin/product'] = "mod_product/mod_admin_product/index";


/* End of file routes.php */
/* Location: ./application/modules/mod_product/config/routes.php */
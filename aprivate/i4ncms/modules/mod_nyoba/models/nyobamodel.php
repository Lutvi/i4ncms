<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Nyobamodel extends MY_Model {

    var $tbl_tes1   = 'tbl_mod_testing1';
    var $tbl_tes2   = 'tbl_mod_testing2';

    function __construct()
    {
        // Call the Model constructor
		$this->db = $this->load->database('default', TRUE);
        parent::__construct();
    }

}

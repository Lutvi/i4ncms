<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_nyoba extends MY_Controller {

	var $mod_view_path = 'modules/mod_nyoba/views/';
	var $mod_url_path = 'mod_product/mod_nyoba/';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('nyobamodel','mnyoba');
	}

	function index()
	{
		//print_r($this->db->get('tbl_artikel'));
		$this->tampil();
	}
	
	function tampil()
	{
		$this->load->library('table');
		$query = $this->db->query("SELECT id,id_kategori,judul_id,author FROM tbl_mod_artikel");
		$data['tbl'] = $this->table->generate($query); 

		$this->load->view('nyoba',$data);
	}
}

/* End of file mod_nyoba.php */
/* Location: ./application/modules/mod_nyoba/controllers/mod_nyoba.php */

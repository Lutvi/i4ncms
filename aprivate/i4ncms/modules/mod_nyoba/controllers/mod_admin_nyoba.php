<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_admin_nyoba extends MY_Controller {

	var $mod_view_path = 'modules/mod_nyoba/views/';
	var $mod_url_path = 'mod_product/mod_admin_nyoba/';

	function __construct()
	{
		parent::__construct();
		$this->config->set_item('compress_output', FALSE);
	}

	function index()
	{
		echo "Nyoba module Admin";
	}

}

/* End of file mod_admin_nyoba.php */
/* Location: ./application/modules/mod_product/controllers/mod_admin_nyoba.php */

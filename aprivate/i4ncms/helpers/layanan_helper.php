<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/* Detect IP user lalu
 * Ambil Nama kota | region | kode negara
 */
function detail_lokasi_ip($ip='')  
{
	//cek koneksi
	//if(cek_koneksi_internet() === TRUE)
	if(ENVIRONMENT == 'production' || ENVIRONMENT == 'live')
	{
		//get the JSON result from hostip.info
		if($ip === '')
		$json = file_get_contents("http://ipinfo.io/json", NULL, NULL);
		else
		$json = file_get_contents("http://ipinfo.io/".$ip."/json", NULL, NULL);
	}
	else {
		$json = '';
	}

	if( ! empty($json))
	{
		$result = json_decode($json, TRUE);
		/*return
			Array ( [ip] => 139.0.157.39 [hostname] => No Hostname [city] => Jakarta
		 * [region] => Jakarta Raya [country] => ID [loc] => -6.1743999999999915,106.82940000000002
		 * [org] => AS23700 Linknet-Fastnet ASN ) 
		 */

		if(! empty($result['city']))
		{
			$lok = $result['city'];
		}
		elseif( ! empty($result['region']) || ! empty($result['country']))
		{
			if( ! empty($result['region']))
				$lok = $result['region'];
			else
				$lok = $result['country'];
		}
		else {
			$lok = 'lainnya';
		}

		return $lok;
	}
	else {
		return 'lainnya';
	}
}

<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Alternative languages helper
*
* Returns a string with links to the content in alternative languages
*
* version 0.2
* @author Luis <luis@piezas.org.es>
* @modified by Ionut <contact@quasiperfect.eu>
*
* version 1.0
* for RDS use only..
* @modified by INP 2014 <fun9uy5@gmail.com>
*/

function alt_site_url($uri = '')
{
    $CI =& get_instance();
    $actual_lang = $CI->uri->segment(1);
    $languages = $CI->config->item('lang_desc');
    $languages_useimg = $CI->config->item('lang_useimg');
    $ignore_lang = $CI->config->item('lang_ignore');
    if (empty($actual_lang))
    {
        $uri=$ignore_lang.$CI->uri->uri_string();
        $actual_lang=$ignore_lang;
    }
    else
    {
        if (!array_key_exists($actual_lang,$languages))
        {
            $uri = $ignore_lang.$CI->uri->uri_string();
            $actual_lang = $ignore_lang;
        }
        else
        {
            $uri = $CI->uri->uri_string();
            $uri = substr_replace($uri,'',0,0);
        }
    }

    $alt_url = '<ul class="subnav">';
    //i use ul because for me formating a list from css is easy
    foreach ($languages as $lang=>$lang_desc)
    {
	    if (empty($actual_lang))
	    {
			$actual_lang = $CI->config->item('language_abbr');
	    }
	    
        if ($actual_lang != $lang)
        {
            $alt_url .= '<li><a href="'.$CI->config->item('base_url');
            if ($lang == $ignore_lang)
            {
                $new_uri = preg_replace('/(.*?)'.$actual_lang.'\/i','',$uri);
                $new_uri = substr_replace($new_uri,'',0,0);
            }
            else
            {
				$nama_halaman = $CI->session->userdata('nama_halaman');
				$pg_basis = $CI->session->userdata('pgbasis');

				$tipepg = $CI->session->userdata('tipepg');
				$nmpg = $CI->session->userdata('nmpg');

				if($CI->uri->segment(1) === false)
				{
					$new_uri = $lang.'/home';
				}
				//-Halaman,kategori = id
				elseif($CI->uri->segment(1) === 'id' && ($CI->uri->segment(2) === 'halaman' || $CI->uri->segment(2) === 'kategori'))
				{
					if ($CI->uri->segment(2) === 'halaman')
						$new_uri = preg_replace('/('.$actual_lang.')\/halaman\/(.*)/i',$lang.'/pages/'.$nama_halaman,$uri);
					elseif ($CI->uri->segment(2) === 'kategori') {
						if ($CI->uri->segment(5))
						$new_uri = preg_replace('/('.$actual_lang.')\/kategori\/(.*)\/(.*)\/(.*)/i',$lang.'/category/'.$tipepg.'/'.$nmpg.'/$4',$uri);
						else
						$new_uri = preg_replace('/('.$actual_lang.')\/kategori\/(.*)\/(.*)/i',$lang.'/category/'.$tipepg.'/'.$nmpg,$uri);
					}
				}
				//-Halaman,kategori = en
				elseif($CI->uri->segment(1) === 'en' && ($CI->uri->segment(2) === 'pages' || $CI->uri->segment(2) === 'category'))
				{
					if($CI->uri->segment(2) === 'pages')
						$new_uri = preg_replace('/('.$actual_lang.')\/pages\/(.*)/i',$lang.'/halaman/'.$nama_halaman,$uri);
					elseif ($CI->uri->segment(2) === 'category') {
						if ($CI->uri->segment(5))
						$new_uri = preg_replace('/('.$actual_lang.')\/category\/(.*)\/(.*)\/(.*)/i',$lang.'/kategori/'.$tipepg.'/'.$nmpg.'/$4',$uri);
						else
						$new_uri = preg_replace('/('.$actual_lang.')\/category\/(.*)\/(.*)/i',$lang.'/kategori/'.$tipepg.'/'.$nmpg,$uri);
					}
				}
				//-Tag = id|en
				elseif(($CI->uri->segment(1) === 'id' || $CI->uri->segment(1) === 'en') && $CI->uri->segment(2) === 'tag')
				{
					if ($CI->uri->segment(5))
					$new_uri = preg_replace('/('.$actual_lang.')\/tag\/(.*)\/(.*)\/(.*)/i',$lang.'/tag/'.$tipepg.'/'.$nmpg.'/$4',$uri);
					else
					$new_uri = preg_replace('/('.$actual_lang.')\/tag\/(.*)\/(.*)/i',$lang.'/tag/'.$tipepg.'/'.$nmpg,$uri);
				}
				//-Detail Konten = id|en
				elseif($CI->uri->segment(2) === 'pdetail' || $CI->uri->segment(2) === 'posting' || $CI->uri->segment(2) === 'gallery' || $CI->uri->segment(2) === 'playlist')
				{
					$new_uri = preg_replace('/('.$actual_lang.')(.*)\/(.*)/i',$lang.'$2/'.$nama_halaman,$uri);
				}
				//-Lainnya = id|en|bukan-multi-lang
				else
				{
					if($CI->uri->segment(3) === FALSE)
					{
						$new_uri = preg_replace('/('.$actual_lang.')(.*)/i',$lang.'/'.$nama_halaman,$uri);
					}
					elseif($CI->uri->segment(4) === FALSE)
					{
						$new_uri = preg_replace('/('.$actual_lang.')(.*)/i',$lang.$pg_basis.$nama_halaman,$uri);
					}
					else {
						if($CI->uri->segment(2) === 'kategori' || $CI->uri->segment(2) === 'category')
							$new_uri = preg_replace('/('.$actual_lang.')(.*)/i',$lang.'$2',$uri);
						else
							$new_uri = preg_replace('/('.$actual_lang.')(.*)\/(.*)\/(.*)/i',$lang.$pg_basis.$nama_halaman.'/$4',$uri);
					}
                }
            }

            $alt_url .= $new_uri.$CI->config->item('url_suffix').'">';
            if ($languages_useimg)
            {
                $alt_url .= $lang_desc.'<img src="'.base_url().'assets/images/flag/'.$lang.'.gif" alt="'.$lang_desc.'" align="absmidle" style="margin-left:5px;" /></a></li>';
            }
            else {
                $alt_url .= $lang_desc.'</a></li>';
            }
        }
    }

    $alt_url .= '</ul>';
    return $alt_url;
}

function current_lang($path=TRUE)
{
	$CI =& get_instance();

	if(strlen($CI->uri->segment(1)) <= 2 && in_array($CI->uri->segment(1),array('id','en')))
	{
		$actual_lang = $CI->uri->segment(1,$CI->config->item('language_abbr'));
    }
    else {
		$actual_lang = $CI->config->item('language_abbr');
    }

	if($path === TRUE)
    return $actual_lang.'/';
    else
    return $actual_lang;
}

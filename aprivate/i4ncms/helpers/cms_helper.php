<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * cms_helper.php
 * 
 * Copyright 2014 Inp <fun9uy5@gmail.com>
 * 
 * Helper untuk sistem CMS Powered by : RDS-Corp.
 * Lisensi : Non-commercial, harus mendapat persetujuan dari author.
 * 
 */

// format kirim email user
function kirim_email_user($pengirim=array(),$kepada='',$judul='',$isi='',$ajax=FALSE)
{
	if(ENVIRONMENT == 'production' || ENVIRONMENT == 'live')
	{
		$CI =& get_instance();
		$CI->load->helper('url');
		$CI->load->library('email');
		$CI->load->model('logmodel','logs');

		if(empty($pengirim))
		$CI->email->from($CI->config->item('info_email'), $CI->config->item('info_name').' '.$CI->config->item('site_name'));
		else
		$CI->email->from($pengirim['email'], $pengirim['nama']);

		$CI->email->to($kepada);
		$CI->email->subject($judul);
		$CI->email->message($isi);

		if ( ! $CI->email->send())
		{
			$t = '[::PERINGATAN::]';
			$k = 'GAGAL Kirim Email, Konfigurasi Email perlu diperbaiki.<br>'.$CI->email->print_debugger();
			$CI->logs->tambahLog($t,'','',current_url(),$rqst='kirim email - '.serialize($_POST),$k);
			$CI->email->clear();

			if($ajax)
			return FALSE;
			else
			redirect('/error', 'refresh');
		}
		else {
			return TRUE;
		}
	}
	else {
		return TRUE;
	}
}

function format_isi_email_update_profil($bhs='',$nama='',$usrid='',$pass='',$email='')
{
	$CI =& get_instance();

	if($bhs == 'id')
	{
		$isi  = 'Kepada : '.$nama.'<br>'."\n";
		$isi .= 'Telah terjadi perubahan pada data akun Anda, pada : '.date('d/m/Y h:i A').'<br>'."\n";
		$isi .= 'Kami telah menperbaharui data akun Anda dalam sistem kami.<br>'."\n";
		$isi .= 'Berikut detail login untuk akun Anda.<br>'."\n";
		$isi .= 'Nama Lengkap : '.$nama.'<br>'."\n";
		$isi .= 'Akun ID      : '.$usrid.'<br>'."\n";
		$isi .= 'Kata Sandi   : '.$pass.'<br>'."\n";
		$isi .= 'Email        : '.$email.'<br>'."\n";
		$isi .= '<hr>Ini hanyalah email notifikasi untuk perubahan data Akun Anda.<br>'."\n";
		$isi .= '<p>'.$CI->config->item('site_name').'.</p>';
	}
	else {
		$isi  = 'To : '.$nama.'<br>'."\n";
		$isi .= 'There has been a change on your account, on : '.date('Y/m/d h:i A').'<br>'."\n";
		$isi .= 'We have updated your account in our system.<br>'."\n";
		$isi .= 'Here\'s the login details for your account.<br>'."\n";
		$isi .= 'Full Name    : '.$nama.'<br>'."\n";
		$isi .= 'Account ID   : '.$usrid.'<br>'."\n";
		$isi .= 'Passwords    : '.$pass.'<br>'."\n";
		$isi .= 'Email        : '.$email.'<br>'."\n";
		$isi .= '<hr>This is just a notification email for your account data changes.<br>'."\n";
		$isi .= '<p>'.$CI->config->item('site_name').'.</p>';
	}

	return $isi;
}

function format_isi_email_pembeli_baru($bhs='',$nama='',$usrid='',$pass='')
{
	$CI =& get_instance();

	if($bhs == 'id')
	{
		$isi = 'Kepada : '.$nama.'<br>'."\n";
		$isi .= 'Terima kasih telah berbelanja di Toko kami, untuk kenyamanan bertransaksi Anda dilain waktu.<br>'."\n";
		$isi .= 'Kami telah mendaftarkan Akun untuk Anda dalam sistem kami.<br>'."\n";
		$isi .= 'Berikut detail login untuk akun Anda.<br>'."\n";
		$isi .= 'Akun ID    : '.$usrid.'<br>'."\n";
		$isi .= 'Kata Sandi : '.$pass.'<br>'."\n";
		$isi .= '<hr>Mohon simpan baik-baik Akun Anda, Untuk keamanan Anda bisa merubah kata sandi Anda melalui profil akun Anda.<br>'."\n";
		$isi .= '<p>Hormat kami,<br>'.$CI->config->item('site_name').'.</p>';
	}
	else {
		$isi = 'To : '.$nama.'<br>'."\n";
		$isi .= 'Thank you for shopping at our store, for the convenience you next time.<br>'."\n";
		$isi .= 'We have registered to your account in our system.<br>'."\n";
		$isi .= 'Here\'s the login details for your account.<br>'."\n";
		$isi .= 'Account ID  : '.$usrid.'<br>'."\n";
		$isi .= 'Passwords   : '.$pass.'<br>'."\n";
		$isi .= '<hr>Please keep your account secure, to be safe you can change the account password through your account profile.<br>'."\n";
		$isi .= '<p>Sincerely,<br>'.$CI->config->item('site_name').'.</p>';
	}

	return $isi;
}

function format_isi_email_registrasi($bhs='',$nama='',$usrid='',$pass='',$email='')
{
	$CI =& get_instance();

	if($bhs == 'id')
	{
		$isi  = 'Kepada : '.$nama.'<br>'."\n";
		$isi .= 'Terima kasih telah melakukan registrasi pada website kami.<br>'."\n";
		$isi .= 'Kami telah mendaftarkan Akun untuk Anda dalam sistem kami.<br>'."\n";
		$isi .= 'Berikut detail login untuk akun Anda.<br>'."\n";
		$isi .= 'Akun ID    : '.$usrid.'<br>'."\n";
		$isi .= 'Kata Sandi : '.$pass.'<br>'."\n";
		$isi .= 'Email      : '.$email.'<br>'."\n";
		$isi .= '<hr>Mohon simpan baik-baik Akun Anda, Anda bisa merubah detail login Anda melalui profil akun Anda.<br>'."\n";
		$isi .= '<p>Hormat kami,<br>'.$CI->config->item('site_name').'.</p>';
	}
	else {
		$isi  = 'To : '.$nama.'<br>'."\n";
		$isi .= 'Thank you for signup on our website.<br>'."\n";
		$isi .= 'We have registered your account in our system.<br>'."\n";
		$isi .= 'Here\'s the login details for your account.<br>'."\n";
		$isi .= 'Account ID : '.$usrid.'<br>'."\n";
		$isi .= 'Passwords  : '.$pass.'<br>'."\n";
		$isi .= 'Email      : '.$email.'<br>'."\n";
		$isi .= '<hr>Please keep your account secure, to be safe you can change the account password through your account profile.<br>'."\n";
		$isi .= '<p>Sincerely,<br>'.$CI->config->item('site_name').'.</p>';
	}

	return $isi;
}

function format_isi_email_transaksi($bhs='',$status='',$metode='',$via='',$nama='',$trx='',$detail=array(),$sub_total='',$diskon=0,$total='',$alamat='',$no_resi='')
{
	$CI =& get_instance();
	$CI->load->helper('typography');

	$isi = '<style>h3,h4 {margin-top:5px;margin-bottom:5px;padding:0;} td span {color:#1749D4;} .info { padding-top:10px;color:#4D4D4D } em {color:#D44C17;text-decoration:underline} address {color:#1441D7}</style>';

	if($bhs == 'id')
	{
		// ID
		$isi .= 'Kepada : '.$nama.'<br>'."\n";
		$isi .= 'Terima kasih telah melakukan pemesanan di Toko kami.<br>'."\n";
		$isi .= 'Berikut ini adalah detail pesanan Anda :<br>'."\n\n";
		$isi .= '<h3>No Struk : '.$trx.'</h3>'."\n";
		$isi .= '<h4>Status Transaksi : '.humanize($status).'</h4>'."\n";
		$isi .= '<table class="gridtable" border="0" width="100%" cellpadding="3" cellspacing="2">';
		$isi .= '<thead><tr style="background:#0EE7FF;color:#3873AC">';
		$isi .= '<th>No Struk</th><th>Tgl Pesan</th><th>Metode Bayar</th><th>Nama Produk</th><th>Jumlah Pesan</th><th>Total Harga</th>';
		$isi .= '</tr></thead>'."\n";
		$isi .= '<tbody>';
		$i=0;
		foreach ($detail as $item)
		{
			$isi .= '<tr style="color:#528CC5;'.($i%2==1?'background:#E3F1FF"':'background:#EDEDED').'">';
				$isi .= '<td>';
				$isi .= $item['no_struk'];
				$isi .= '</td>';
				$isi .= '<td align="center">';
				$isi .= date('d-m-Y h:i A', strtotime($item['tgl_transaksi']));
				$isi .= '</td>';
				$isi .= '<td align="center">';
				$isi .= ucwords($item['metode_bayar']).' - '.$item['bayar_melalui'];
				$isi .= '</td>';
				$isi .= '<td>';
				$isi .= $item['detail_produk'];
				$isi .= '</td>';
				$isi .= '<td align="right">';
				$isi .= format_harga_indo($item['jml_pesan']).' '.$CI->config->item('satuan_produk');
				$isi .= '</td>';
				$isi .= '<td align="right">';
				$isi .= format_harga_indo($item['total_harga']);
				$isi .= '</td>';
			$isi .= '</tr>'."\n";
			$i++;
		}

		if($diskon > 0){
			$isi .= '<tr style="background:#2BE1FF;">';
			$isi .= '<td>Sub Total</td>';
			$isi .= '<td colspan="5" align="right">';
			$isi .= '<span class="total">'.format_harga_indo($sub_total).'</span>';
			$isi .= '</td>'."\n";

			$isi .= '<tr style="background:#2BE1FF;">';
			$isi .= '<td> Diskon Transaksi </td>';
			$isi .= '<td colspan="5" align="right">';
			$isi .= '<span class="diskon">'.format_harga_indo($diskon).'</span>';
			$isi .= '</td>'."\n";
		}

		$isi .= '<tr style="background:#2BC9FF;">';
		$isi .= '<td>Total Pembayaran </td>';
		$isi .= '<td colspan="5" align="right">';
		$isi .= '<span class="total">Rp. '.format_harga_indo($total).'</span>';
		$isi .= '</td>'."\n";

		$isi .= '</tbody>'."\n";
		$isi .= '</table>'."\n";

		$isi .= '<p><u>Alamat pengiriman :</u><br>'."\n";
		foreach ($alamat as $alisi)
		{
			$isi .= $alisi.'<br>'."\n";
		}
		$isi .= '</p>'."\n";
		
		if($metode == 'transfer')
		{
			$isi .= '<p><u>Berikut ini adalah No Rekening Bank Kami :</u><br>'."\n";
			$bayar_via = $CI->config->item('rek_bank');
		}
		elseif($metode == 'giro') {
			$isi .= '<p><u>Berikut ini adalah Alamat Tujuan Giro Kami :</u><br>'."\n";
			$bayar_via = $CI->config->item('giro_alamat');
		}
		elseif($metode == 'cod') {
			$isi .= '<p><u>Berikut ini adalah ID Pegawai Kurir Kami :</u><br>'."\n";
			$bayar_via = $CI->config->item('kurir_cod');
		}
		else {
			$isi .= '<p><u>Berikut ini adalah Detail Pembayaran Online Kami :</u><br>'."\n";
			$bayar_via = $CI->config->item('online_id');
		}

		if (is_array($via)) {
			foreach ($via as $detail)
			{
				$isi .= '<strong>'.ucwords($detail['nama_vendor']).'</strong><br>'."\n".jadi_br($detail['detail']).'<br>'."\n";
			}
		}
		$isi .= '</p>'."\n";
		$isi .= '<div class="info">'."\n";
		if($metode != 'online')
		$isi .= 'Mohon segera konfirmasikan pembayaran Anda, setelah pembayaran dilakukan.'.'<br>'."\n";
		$isi .= 'Jika terdapat kesalahan alamat kirim mohon segera hubungi kami.'.'<br>'."\n";
		$isi .= '<em><small>Apabila data Anda tidak valid atau tidak dapat dihubungi, maka kami akan membatalkan pesanan Anda secara sepihak.</small></em><br>'."\n";
		$isi .= '<em><small>Mohon catat No.Struk(TRX), sebelum Anda menghubungi CS kami.</small></em>'."\n";
		$isi .= '</div>'."\n";
		$isi .= '<address>'.$CI->config->item('cs_name').' : '.$CI->config->item('cs_email').' / '.$CI->config->item('cs_telpon').'</address>'."\n";
		$isi .= '<p>Terima kasih telah berbelanja di Toko kami, semoga hari Anda menyenangkan.</p>'."\n";
		$isi .= '<p>Hormat kami,<br>'.$CI->config->item('site_name').'.</p>';
	}
	else {
		// EN
		$isi .= 'To : '.$nama.'<br>'."\n";
		$isi .= 'Thank you for your orders at our store.<br>'."\n";
		$isi .= 'Here are the details of your order:<br>'."\n\n";
		$isi .= '<h3>TRX Number : '.$trx.'</h3>'."\n";
		$isi .= '<h4>Order Status : '.humanize($status).'</h4>'."\n";
		$isi .= '<table class="gridtable" border="0" width="100%" cellpadding="3" cellspacing="2">';
		$isi .= '<thead><tr style="background:#0EE7FF;color:#3873AC">';
		$isi .= '<th>TRX Number</th><th>Date</th><th>Payment Method</th><th>Product</th><th>Qty</th><th>Total Price</th>';
		$isi .= '</tr></thead>'."\n";
		$isi .= '<tbody>';
		$i=0;
		foreach ($detail as $item)
		{
			$isi .= '<tr style="color:#528CC5;'.($i%2==1?'background:#E3F1FF"':'background:#EDEDED').'">';
				$isi .= '<td>';
				$isi .= $item['no_struk'];
				$isi .= '</td>';
				$isi .= '<td align="center">';
				$isi .= date('d-m-Y h:i A', strtotime($item['tgl_transaksi']));
				$isi .= '</td>';
				$isi .= '<td align="center">';
				$isi .= ucwords($item['metode_bayar']).' - '.$item['bayar_melalui'];
				$isi .= '</td>';
				$isi .= '<td>';
				$isi .= $item['detail_produk'];
				$isi .= '</td>';
				$isi .= '<td align="right">';
				$isi .= format_harga_indo($item['jml_pesan']).' '.$CI->config->item('satuan_produk');
				$isi .= '</td>';
				$isi .= '<td align="right">';
				$isi .= format_harga_indo($item['total_harga']);
				$isi .= '</td>';
			$isi .= '</tr>'."\n";
			$i++;
		}

		if($diskon > 0){
			$isi .= '<tr style="background:#2BE1FF;">';
			$isi .= '<td>Sub Total</td>';
			$isi .= '<td colspan="5" align="right">';
			$isi .= '<span class="total">'.format_harga_indo($sub_total).'</span>';
			$isi .= '</td>'."\n";

			$isi .= '<tr style="background:#2BE1FF;">';
			$isi .= '<td> Discount </td>';
			$isi .= '<td colspan="5" align="right">';
			$isi .= '<span class="diskon">'.format_harga_indo($diskon).'</span>';
			$isi .= '</td>'."\n";
		}

		$isi .= '<tr style="background:#2BC9FF;">';
		$isi .= '<td>Total Payment </td>';
		$isi .= '<td colspan="5" align="right">';
		$isi .= '<span class="total">Rp. '.format_harga_indo($total).'</span>';
		$isi .= '</td>'."\n";

		$isi .= '</tbody>'."\n";
		$isi .= '</table>'."\n";

		$isi .= '<p><u>Shipping address :</u><br>'."\n";
		foreach ($alamat as $alisi)
		{
			$isi .= $alisi.'<br>'."\n";
		}
		$isi .= '</p>'."\n";
		
		if($metode == 'transfer')
		{
			$isi .= '<p><u>Here is Our Bank Account for transfers :</u><br>'."\n";
			$bayar_via = $CI->config->item('rek_bank');
		}
		elseif($metode == 'giro') {
			$isi .= '<p><u>Here is the Destination Address of our Giro post :</u><br>'."\n";
			$bayar_via = $CI->config->item('giro_alamat');
		}
		elseif($metode == 'cod') {
			$isi .= '<p><u>Here is a Our Courier Employee ID :</u><br>'."\n";
			$bayar_via = $CI->config->item('kurir_cod');
		}
		else {
			$isi .= '<p><u>Our Online Payment Details :</u><br>'."\n";
			$bayar_via = $CI->config->item('online_id');
		}

		if (is_array($via)) {
			foreach ($via as $detail)
			{
				$isi .= '<strong>'.ucwords($detail['nama_vendor']).'</strong><br>'."\n".jadi_br($detail['detail']).'<br>'."\n";
			}
		}
		
		$isi .= '</p>'."\n";
		$isi .= '<div class="info">'."\n";
		if($metode != 'online')
		$isi .= 'Please confirm your payment immediately after pay this order.'.'<br>'."\n";
		$isi .= 'If the shipping address is incorrect please contact us.'.'<br>'."\n";
		$isi .= '<em><small>If your data is invalid or our staff can not contact you, we will be canceling your order.</small></em><br>'."\n";
		$isi .= '<em><small>Please remember your TRX Number before contact our CS.</small></em>'."\n";
		$isi .= '</div>'."\n";
		$isi .= '<address>'.$CI->config->item('cs_name').' : '.$CI->config->item('cs_email').' / '.$CI->config->item('cs_telpon').'</address>'."\n";
		$isi .= '<p>Thank you for shopping at our store, have a nice day.</p>'."\n";
		$isi .= '<p>Sincerely ,<br>'.$CI->config->item('site_name').'.</p>';
	}

	return $isi;
}

/* RSS FORMAT */

//====RSS Konten Valid W3C
function format_rss2($data=array())
{
	$CI =& get_instance();
	$bhs = current_lang(false);
	echo '<?xml version="1.0"?>'."\n";
	echo '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'."\n";

	echo '<channel>'."\n";
	echo '<title>'.$data['feed_name'].'</title>'."\n";
	echo '<link>'.$data['feed_url'].'</link>'."\n";
	echo '<description>'.$data['page_description'].'</description>'."\n";
	echo '<language>'.current_lang(false).'</language>'."\n";
	echo '<atom:link href="'.$data['feed_url'].$data['extra_url'].'" rel="self" type="application/rss+xml"/>'."\n";
	echo '<image>'."\n";
	echo    '<url>'. base_url('assets/images/watermark-logo/medium-logo.jpg') .'</url>'."\n";
	echo    '<title>'.$data['feed_name'].'</title>'."\n";
	echo    '<link>'.$data['feed_url'].'</link>'."\n";
	echo '</image>'."\n";

	if(!empty($data['konten_blog'])){
		foreach($data['konten_blog'] as $item) {
			echo '<item>'."\n";
			echo	'<title>'.xml_convert(($bhs=='id')?$item->judul_id:$item->judul_en).'</title>'."\n";
			echo	'<description><![CDATA[<img src="'.base_url().'_media/blog/thumb/thumb_'.$item->gambar_cover.'" /> <p>'.character_limiter(strip_tags(($bhs=='id')?$item->isi_id:$item->isi_en), 200).'</p>]]></description>'."\n";
			echo	'<link>'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'</link>'."\n";
			echo	'<pubDate>'.date(DATE_RSS, $item->tgl_revisi).'</pubDate>'."\n";
			echo	'<guid>'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'</guid>'."\n";
			echo '</item>'."\n";
		}
	}

	if(!empty($data['gambar_album'])){
		foreach($data['gambar_album'] as $item) {
			echo '<item>'."\n";
			echo	'<title>'.xml_convert(($bhs=='id')?$item->nama_id:$item->nama_en).'</title>'."\n";
			echo	'<description><![CDATA[<img src="'.base_url().'_media/album/thumb/thumb_'.$item->gambar_cover.'" /> <p>'.character_limiter(strip_tags(($bhs=='id')?$item->ket_id:$item->ket_en), 200).'</p>]]></description>'."\n";
			echo	'<link>'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</link>'."\n";
			echo	'<pubDate>'.date(DATE_RSS, $item->tgl_update).'</pubDate>'."\n";
			echo	'<guid>'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</guid>'."\n";
			echo '</item>'."\n";
		}
	}

	if(!empty($data['video'])){
		foreach($data['video'] as $item) {
			echo '<item>'."\n";
			echo	'<title>'.xml_convert(($bhs=='id')?$item->nama_id:$item->nama_en).'</title>'."\n";
			echo	'<description><![CDATA[<img src="'.base_url().'_media/videos/thumb/thumb_'.$item->gambar_video.'" /> <p>'.character_limiter(strip_tags(($bhs=='id')?$item->ket_id:$item->ket_en), 200).'</p>]]></description>'."\n";
			echo	'<link>'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</link>'."\n";
			echo	'<pubDate>'.date(DATE_RSS, $item->tgl_update).'</pubDate>'."\n";
			echo	'<guid>'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</guid>'."\n";
			echo '</item>'."\n";
		}
	}

	echo '</channel>'."\n";
	echo '</rss>'."\n";
}

function format_atom($data=array())
{
	$CI =& get_instance();
	$bhs = current_lang(false);
	echo '<?xml version="1.0" encoding="utf-8"?>
		<feed xmlns="http://www.w3.org/2005/Atom">
		<id>'.$data['feed_url'].$data['extra_url'].'</id>
		<title>'.$data['feed_name'].'</title>
		  <link rel="self" href="'.$data['feed_url'].$data['extra_url'].'"/>
		  <link href="'.base_url().'" />
		  <updated>'.date(DATE_RFC3339).'</updated>
		  <author>
		    <name>'.config_item('owner_name').'</name>
		  </author>'."\n";

	if(!empty($data['konten_blog'])){
		foreach($data['konten_blog'] as $item) {
	echo '<entry>'."\n".
		    '<category term="blog"/>'."\n".
		    '<id>'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'</id>'."\n".
		    '<title type="html">'.htmlentities(($bhs=='id')?$item->judul_id:$item->judul_en).'</title>'."\n".
		    '<link rel="alternate" href="'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'"/>'."\n".
		    '<updated>'.date(DATE_ATOM, $item->tgl_revisi).'</updated>'."\n";
	echo	'<summary type="html">'.htmlentities(character_limiter(($bhs=='id')?$item->isi_id:$item->isi_en, 400)).'</summary>'."\n";
	echo	'<content>'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'</content>'."\n";
	echo	'<author>
				<name>'.config_item('owner_name').'</name>
			</author>'."\n";
echo	'</entry>'."\n";
		}
	}

	if(!empty($data['gambar_album'])){
		foreach($data['gambar_album'] as $item) {
	echo '<entry>
		    <category term="album"/>
		    <id>'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</id>
		    <title>'.xml_convert(($bhs=='id')?$item->nama_id:$item->nama_en).'</title>
		    <link rel="alternate" href="'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'"/>
		    <updated>'.date(DATE_RFC3339, $item->tgl_update).'</updated>'."\n";
	echo	'<summary type="html">'.htmlentities(character_limiter(($bhs=='id')?$item->ket_id:$item->ket_en, 400)).'</summary>'."\n";
	echo	'<content>'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</content>'."\n";
	echo	'<author>
				<name>'.config_item('owner_name').'</name>
			</author>'."\n";
echo	'</entry>'."\n";
		}
	}

	if(!empty($data['video'])){
		foreach($data['video'] as $item) {
	echo '<entry>
		    <category term="video"/>
		    <id>'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</id>
		    <title>'.xml_convert(($bhs=='id')?$item->nama_id:$item->nama_en).'</title>
		    <link rel="alternate" href="'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'"/>
		    <updated>'.date(DATE_RFC3339, $item->tgl_update).'</updated>'."\n";
	echo	'<summary type="html">'.htmlentities(character_limiter(($bhs=='id')?$item->ket_id:$item->ket_en, 400)).'</summary>'."\n";
	echo	'<content>'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</content>'."\n";
	echo	'<author>
				<name>'.config_item('owner_name').'</name>
			</author>'."\n";
echo	'</entry>'."\n";
		}
	}

	echo '</feed>';
}

function format_rdf($data=array())
{
	$CI =& get_instance();
	$bhs = current_lang(false);
	echo	'<?xml version="1.0"?>
			<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://purl.org/rss/1.0/">
			<channel rdf:about="'.$data['feed_url'].$data['extra_url'].'">
			<title>'.$data['feed_name'].'</title>
			<link>'.$data['feed_url'].$data['extra_url'].'</link>
			<description>'.$data['page_description'].'</description>
			<image rdf:resource="'. base_url('assets/images/watermark-logo/medium-logo.jpg') .'" />
			<items>
			<rdf:Seq>';
		    if(!empty($data['konten_blog'])){
				foreach($data['konten_blog'] as $item) {
					echo '<rdf:li resource="'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'" />';
				}
		    }
		    if(!empty($data['gambar_album'])){
				foreach($data['gambar_album'] as $item) {
					echo '<rdf:li resource="'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'" />';
				}
		    }
		    if(!empty($data['video'])){
				foreach($data['video'] as $item) {
					echo '<rdf:li resource="'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'" />';
				}
		    }
	echo   	'</rdf:Seq>
			</items>
			</channel>'."\n";
	if(!empty($data['konten_blog'])){
		foreach($data['konten_blog'] as $item) {
	echo	'<item rdf:about="'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'">
				<title>'.xml_convert(($bhs=='id')?$item->judul_id:$item->judul_en).'</title>
				<link>'.base_url().$bhs.'/'.'posting/'.underscore(($bhs=='id')?$item->judul_id:$item->judul_en).'</link>
				<description><![CDATA[<img src="'.base_url().'_media/blog/thumb/thumb_'.$item->gambar_cover.'" /> <p>'.character_limiter(strip_tags(($bhs=='id')?$item->isi_id:$item->isi_en), 200).'</p>]]></description>
			</item>'."\n";
		}
	}
	if(!empty($data['gambar_album'])){
		foreach($data['gambar_album'] as $item) {
	echo	'<item rdf:about="'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'">
				<title>'.xml_convert(($bhs=='id')?$item->nama_id:$item->nama_en).'</title>
				<link>'.base_url().$bhs.'/'.'gallery/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</link>
				<description><![CDATA[<img src="'.base_url().'_media/album/thumb/thumb_'.$item->gambar_cover.'" /> <p>'.character_limiter(strip_tags(($bhs=='id')?$item->ket_id:$item->ket_en), 200).'</p>]]></description>
			</item>'."\n";
		}
	}
	if(!empty($data['video'])){
		foreach($data['video'] as $item) {
	echo	'<item rdf:about="'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'">
				<title>'.xml_convert(($bhs=='id')?$item->nama_id:$item->nama_en).'</title>
				<link>'.base_url().$bhs.'/'.'playlist/'.underscore(($bhs=='id')?$item->nama_id:$item->nama_en).'</link>
				<description><![CDATA[<img src="'.base_url().'_media/videos/thumb/thumb_'.$item->gambar_video.'" /> <p>'.character_limiter(strip_tags(($bhs=='id')?$item->ket_id:$item->ket_en), 200).'</p>]]></description>
			</item>'."\n";
		}
	}
	echo 	'</rdf:RDF>';
}

//===RSS PRODUK by:Google xml
//Belum di pakai
function format_prod_rss2($data=array())
{
	$CI =& get_instance();
	$bhs = current_lang(false);

	echo '<?xml version="1.0"?>'."\n";
	echo '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'."\n";
		echo '<channel>'."\n";
			echo '<title>'.$data['feed_name'].'</title>'."\n";
			echo '<link>'.$data['feed_url'].$data['extra_url'].'</link>'."\n";
			echo '<description>'.$data['page_description'].'</description>'."\n";

				//<!-- First example shows what attributes are required and recommended for items that are not in the apparel category -->
			echo '<item>'."\n";
					//<!-- The following attributes are always required -->
				echo '<title>LG Flatron M2262D 22" Full HD LCD TV</title>'."\n";
				echo '<link>http://www.example.com/electronics/tv/LGM2262D.html</link>'."\n";
				echo '<description>Attractively styled and boasting stunning picture quality, the LG Flatron M2262D 22&quot; Full HD LCD TV is an excellent television/monitor. The LG Flatron M2262D 22&quot; Full HD LCD TV sports a widescreen 1080p panel, perfect for watching movies in their original format, whilst also providing plenty of working space for your other applications. You&#39;ll also experience an excellent sound quality with SRS TruSurround HD technology and built-in stereo speakers. Enjoy a broad range of free-to-air digital television channels and digital radio stations on the LG Flatron M2262D thanks to its built-in DVB-T Freeview tuner. Hook up the LG Flatron M2262D 22&quot; Full HD LCD TV to most external devices with ease, thanks to its comprehensive range of ports including 2 HDMI ports, 2 SCART sockets, a DVI connector, a VGA connector and USB connectivity - allowing you to input your own multimedia files.</description>'."\n";
				echo '<g:id>TV_123456</g:id>'."\n";
				echo '<g:condition>used</g:condition>'."\n";
				echo '<g:price>159.00 USD</g:price>'."\n";
				echo '<g:availability>in stock</g:availability>'."\n";
				echo '<g:image_link>http://images.example.com/TV_123456.png</g:image_link>'."\n";
				echo '<g:shipping>'."\n";
					echo '<g:country>US</g:country>'."\n";
					echo '<g:service>Standard</g:service>'."\n";
					echo '<g:price>14.95 USD</g:price>'."\n";
				echo '</g:shipping>'."\n";
					
					//<!-- The following attributes are required because this item is not apparel or a custom good -->
				echo '<g:gtin>8808992787426</g:gtin>'."\n";
				echo '<g:brand>LG</g:brand>'."\n";
				echo '<g:mpn>M2262D-PC</g:mpn>'."\n";
					
					//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
				echo '<g:product_type>Consumer Electronics &gt; TVs &gt; Flat Panel TVs</g:product_type>'."\n";
			echo '</item>'."\n";
				
				//<!-- Second example demonstrates the use of CDATA sections instead of entities to deal with special characters. Note that CDATA sections can be used for any attribute -->
			echo '<item>'."\n";
					//<!-- The following attributes are always required -->
				echo '<title><![CDATA[Merlin: Series 3 - Volume 2 - 3 DVD Box set]]></title>'."\n";
				echo '<link><![CDATA[http://www.example.com/media/dvd/?sku=384616&src=gshopping&lang=en]]></link>'."\n";
				echo '<description>'."<![CDATA[Episodes 7-13 from the third series of the BBC fantasy drama set in the mythical city of Camelot, telling the tale of the relationship between the young King Arthur (Bradley James) & Merlin (Colin Morgan), the wise sorcerer who guides him to power and beyond. Episodes are: 'The Castle of Fyrien', 'The Eye of the Phoenix', 'Love in the Time of Dragons', 'Queen of Hearts', 'The Sorcerer's Shadow', 'The Coming of Arthur: Part 1' & 'The Coming of Arthur: Part 2']]>".'</description>'."\n";
				echo '<g:id>DVD-0564738</g:id>'."\n";
				echo '<g:condition>new</g:condition>'."\n";
				echo '<g:price>11.99 USD</g:price>'."\n";
				echo '<g:availability>preorder</g:availability>'."\n";
				echo '<g:image_link><![CDATA[http://images.example.com/DVD-0564738?size=large&format=PNG]]></g:image_link>'."\n";
				echo '<g:shipping>'."\n";
					echo '<g:country>US</g:country>'."\n";
					echo '<g:service>Express Mail</g:service>'."\n";
					echo '<g:price>3.80 USD</g:price>'."\n";
				echo '</g:shipping>'."\n";
					
					//<!-- The following attributes are required because this item is not apparel or a custom good -->
				echo '<g:gtin>5030697019233</g:gtin>'."\n";
				echo '<g:brand>BBC</g:brand>'."\n";
					
					//<!-- The following attribute is required because this item is in the 'Media' category -->
				echo '<g:google_product_category><![CDATA[Media > DVDs & Movies > Television Shows]]></g:google_product_category>'."\n";
					
					//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
				echo '<g:product_type><![CDATA[DVDs & Movies > TV Series > Fantasy Drama]]></g:product_type>'."\n";
			echo '</item>'."\n";
	
				//<!-- Third example shows how to include multiple images and shipping values -->
			echo '<item>'."\n";
					//<!-- The following attributes are always required -->
				echo '<title>Dior Capture R60/80 XP Restoring Wrinkle Creme Rich Texture 30ml</title>'."\n";
				echo '<link>http://www.example.com/perfumes/product?Dior%20Capture%20R6080%20XP</link>'."\n";
				echo '<description>Dior Capture R60/80 XP Ultimate Wrinkle Creme reinvents anti-wrinkle care by protecting and relaunching skin cell activity to encourage faster, healthier regeneration.</description>'."\n";
				echo '<g:id>PFM654321</g:id>'."\n";
				echo '<g:condition>new</g:condition>'."\n";
				echo '<g:price>46.75 USD</g:price>'."\n";
				echo '<g:availability>in stock</g:availability>'."\n";
				echo '<g:image_link>http://images.example.com/PFM654321_1.jpg</g:image_link>'."\n";
				echo '<g:shipping>'."\n";
					echo '<g:country>US</g:country>'."\n";
					echo '<g:service>Standard Rate</g:service>'."\n";
					echo '<g:price>4.95 USD</g:price>'."\n";
				echo '</g:shipping>'."\n";
				echo '<g:shipping>'."\n";
					echo '<g:country>US</g:country>'."\n";
					echo '<g:service>Next Day</g:service>'."\n";
					echo '<g:price>8.50 USD</g:price>'."\n";
				echo '</g:shipping>'."\n";
					
					//<!-- The following attributes are required because this item is not apparel or a custom good -->
				echo '<g:gtin>3348900839731</g:gtin>'."\n";
				echo '<g:brand>Dior</g:brand>'."\n";
					
					//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
				echo '<g:product_type>Health &amp; Beauty &gt; Personal Care &gt; Cosmetics &gt; Skin Care &gt; Lotion</g:product_type>'."\n";
				echo '<g:additional_image_link>http://images.example.com/PFM654321_2.jpg</g:additional_image_link>'."\n";
				echo '<g:additional_image_link>http://images.example.com/PFM654321_3.jpg</g:additional_image_link>'."\n";
			echo '</item>'."\n";
				
				//<!-- Fourth example shows what attributes are required and recommended for items that are in the apparel category -->
			echo '<item>'."\n";
					//<!-- The following attributes are always required -->
				echo '<title>Roma Cotton Rich Bootcut Jeans with Belt - Size 8 Standard</title>'."\n";
				echo '<link>http://www.example.com/clothing/women/Roma-Cotton-Bootcut-Jeans/?extid=CLO-29473856</link>'."\n";
				echo '<description>Comes with the belt. A smart pair of bootcut jeans in stretch cotton. The flower print buckle belt makes it extra stylish.</description>'."\n";
				echo '<g:id>CLO-29473856-1</g:id>'."\n";
				echo '<g:condition>new</g:condition>'."\n";
				echo '<g:price>29.50 USD</g:price>'."\n";
				echo '<g:availability>out of stock</g:availability>	';
				echo '<g:image_link>http://images.example.com/CLO-29473856-front.jpg</g:image_link>'."\n";
	
				//<!-- For use in combination with the account level shipping setting -->
				echo '<g:shipping_weight>750 g</g:shipping_weight>'."\n"; 
				
					//<!-- The following attributes are required because this item is apparel -->
				echo '<g:google_product_category>Apparel &amp; Accessories &gt; Clothing &gt; Jeans</g:google_product_category>'."\n";
				echo '<g:gender>Female</g:gender>'."\n";
				echo '<g:age_group>Adult</g:age_group>'."\n";
				echo '<g:color>Navy</g:color>'."\n";
				echo '<g:size>8 Standard</g:size>'."\n";
					
					//<!-- The following attribute is required because this item has variants -->
				echo '<g:item_group_id>CLO-29473856</g:item_group_id>'."\n";
					
					//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
				echo '<g:brand>M&amp;S</g:brand>'."\n";
				echo '<g:mpn>B003J5F5EY</g:mpn>'."\n";
				echo '<g:product_type>Womens Clothing &gt; Jeans &gt; Bootcut Jeans</g:product_type>'."\n";
				echo '<g:additional_image_link>http://images.example.com/CLO-29473856-side.jpg</g:additional_image_link>'."\n";
				echo '<g:additional_image_link>http://images.example.com/CLO-29473856-back.jpg</g:additional_image_link>'."\n";
			echo '</item>'."\n";
				
				//<!-- This is a variant of the last item (same 'item group id'). In this case the variant is only by size, but the item could be repeated in the same way for other variants -->
			echo '<item>'."\n";
					//<!-- The following attributes are always required -->
				echo '<title>Roma Cotton Rich Bootcut Jeans with Belt - Size 8 Tall</title>'."\n";
				echo '<link>http://www.example.com/clothing/women/Roma-Cotton-Bootcut-Jeans/?extid=CLO-29473856</link>'."\n";
				echo '<description>Comes with the belt. A smart pair of bootcut jeans in stretch cotton. The flower print buckle belt makes it extra stylish.</description>'."\n";
				echo '<g:id>CLO-29473856-2</g:id>'."\n";
				echo '<g:condition>new</g:condition>'."\n";
				echo '<g:price>29.50 USD</g:price>'."\n";
				echo '<g:availability>in stock</g:availability>'."\n";
				echo '<g:image_link>http://images.example.com/CLO-29473856-front.jpg</g:image_link>'."\n";
	
					//<!-- For use in combination with the account level weight based shipping setting -->
				echo '<g:shipping_weight>820 g</g:shipping_weight> ';
	
					//<!-- The following attributes are required because this item is apparel -->
				echo '<g:google_product_category>Apparel &amp; Accessories &gt; Clothing &gt; Jeans</g:google_product_category>'."\n";
				echo '<g:gender>Female</g:gender>'."\n";
				echo '<g:age_group>Adult</g:age_group>'."\n";
				echo '<g:color>Navy</g:color>'."\n";
				echo '<g:size>8 Tall</g:size>'."\n";
					
					//<!-- The following attribute is required because this item has variants -->
				echo '<g:item_group_id>CLO-29473856</g:item_group_id>'."\n";
					
					//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
				echo '<g:brand>M&amp;S</g:brand>'."\n";
				echo '<g:mpn>B003J5F5EY</g:mpn>'."\n";
				echo '<g:product_type>Womens Clothing &gt; Jeans &gt; Bootcut Jeans</g:product_type>'."\n";
				echo '<g:additional_image_link>http://images.example.com/CLO-29473856-side.jpg</g:additional_image_link>'."\n";
				echo '<g:additional_image_link>http://images.example.com/CLO-29473856-back.jpg</g:additional_image_link>'."\n";
			echo '</item>'."\n";
				
				//<!-- Fifth example demonstrates the use of the sale price attributes  -->
			echo '<item>'."\n";
					//<!-- The following attributes are always required -->
				echo '<title>Tenn Cool Flow Ladies Long Sleeved Cycle Jersey</title>'."\n";
				echo '<link>http://www.example.com/clothing/sports/product?id=CLO1029384&amp;src=gshopping&amp;popup=false</link>'."\n";
				echo '<description>A ladies cycling jersey designed for the serious cyclist, tailored to fit a feminine frame. This sporty, vibrant red, black and white jersey is constructed of a special polyester weave that is extremely effective at drawing moisture away from your body, helping to keep you dry.  With an elasticised, gripping waist, it will stay in place for the duration of your cycle, and wont creep up like many other products. It has two elasticised rear pockets and the sleeves are elasticated to prevent creep-up.</description>'."\n";
				echo '<g:id>CLO-1029384</g:id>'."\n";
				echo '<g:condition>new</g:condition>'."\n";
				echo '<g:price>33.99 USD</g:price>'."\n";
				echo '<g:availability>available for order</g:availability>'."\n";
				echo '<g:image_link>http://images.example.com/CLO-1029384.jpg</g:image_link>'."\n";
				echo '<g:shipping>'."\n";
					echo '<g:country>US</g:country>'."\n";
					echo '<g:service>Standard Free Shipping</g:service>'."\n";
					echo '<g:price>0 USD</g:price>'."\n";
				echo '</g:shipping>'."\n";
					
					//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
				echo '<g:gtin>5060155240282</g:gtin>'."\n";
				echo '<g:product_type>Sporting Goods &gt; Outdoor Recreation &gt; Cycling &gt; Bicycle Clothing &gt; Bicycle Jerseys</g:product_type>'."\n";
				echo '<g:gender>Female</g:gender>'."\n";
				echo '<g:age_group>Adult</g:age_group>'."\n";
				//<!-- Indicates all the colours found on the garment in order of dominance -->
				echo '<g:color>Black/Red/White</g:color>'."\n";
				echo '<g:size>M</g:size>'."\n";
					
					//<!-- The following demonstrate the use of the 'sale price' and 'sale price effective date' and attributes -->
				echo '<g:sale_price>25.49 USD</g:sale_price>'."\n";
				echo '<g:sale_price_effective_date>2011-09-01T16:00-08:00/2011-09-03T16:00-08:00</g:sale_price_effective_date>'."\n";
			echo '</item>'."\n";
		echo '</channel>'."\n";
	echo '</rss>'."\n";
}

function format_prod_atom($data=array())
{
	$CI =& get_instance();
	$bhs = current_lang(false);

	echo '<?xml version="1.0"?>'."\n";
	echo '<feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0">'."\n";
		echo '<title>Example - Online Store</title>'."\n";
		echo '<link rel="self" href="http://www.example.com"/>'."\n";
		echo '<updated>20011-07-11T12:00:00Z</updated> '."\n";
			
			//<!-- First example shows what attributes are required and recommended for items that are not in the apparel category -->
		echo '<entry>'."\n";
				//<!-- The following attributes are always required -->
			echo '<title>LG Flatron M2262D 22" Full HD LCD TV</title>'."\n";
			echo '<link href="http://www.example.com/electronics/tv/LGM2262D.html"/>'."\n";
			echo '<summary>Attractively styled and boasting stunning picture quality, the LG Flatron M2262D 22&quot; Full HD LCD TV is an excellent television/monitor. The LG Flatron M2262D 22&quot; Full HD LCD TV sports a widescreen 1080p panel, perfect for watching movies in their original format, whilst also providing plenty of working space for your other applications. You&#39;ll also experience an excellent sound quality with SRS TruSurround HD technology and built-in stereo speakers. Enjoy a broad range of free-to-air digital television channels and digital radio stations on the LG Flatron M2262D thanks to its built-in DVB-T Freeview tuner. Hook up the LG Flatron M2262D 22&quot; Full HD LCD TV to most external devices with ease, thanks to its comprehensive range of ports including 2 HDMI ports, 2 SCART sockets, a DVI connector, a VGA connector and USB connectivity - allowing you to input your own multimedia files.</summary>'."\n";
			echo '<g:id>TV_123456</g:id>'."\n";
			echo '<g:condition>used</g:condition>'."\n";
			echo '<g:price>159.00 USD</g:price>'."\n";
			echo '<g:availability>in stock</g:availability>'."\n";
			echo '<g:image_link>http://images.example.com/TV_123456.png</g:image_link>'."\n";
			echo '<g:shipping>'."\n";
				echo '<g:country>US</g:country>'."\n";
				echo '<g:service>Standard</g:service>'."\n";
				echo '<g:price>14.95 USD</g:price>'."\n";
			echo '</g:shipping>'."\n";
				
				//<!-- The following attributes are required because this item is not apparel or a custom good -->
			echo '<g:gtin>8808992787426</g:gtin>'."\n";
			echo '<g:brand>LG</g:brand>'."\n";
			echo '<g:mpn>M2262D-PC</g:mpn>'."\n";
				
				//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
			echo '<g:product_type>Consumer Electronics &gt; TVs &gt; Flat Panel TVs</g:product_type>'."\n";
		echo '</entry>'."\n";
			
			//<!-- Second example demonstrates the use of CDATA sections instead of entities to deal with special characters. Note that CDATA sections can be used for any attribute value but not for the link href value -->
		echo '<entry>'."\n";
				//<!-- The following attributes are always required -->
			echo '<title><![CDATA[Merlin: Series 3 - Volume 2 - 3 DVD Box set]]></title>'."\n";
			echo '<link href="http://www.example.com/media/dvd/?sku=384616&amp;src=gshopping&amp;lang=en"/>'."\n";
			echo '<summary>'."<![CDATA[Episodes 7-13 from the third series of the BBC fantasy drama set in the mythical city of Camelot, telling the tale of the relationship between the young King Arthur (Bradley James) & Merlin (Colin Morgan), the wise sorcerer who guides him to power and beyond. Episodes are: 'The Castle of Fyrien', 'The Eye of the Phoenix', 'Love in the Time of Dragons', 'Queen of Hearts', 'The Sorcerer's Shadow', 'The Coming of Arthur: Part 1' & 'The Coming of Arthur: Part 2']]>".'</summary>'."\n";
			echo '<g:id>DVD-0564738</g:id>'."\n";
			echo '<g:condition>new</g:condition>'."\n";
			echo '<g:price>11.99 USD</g:price>'."\n";
			echo '<g:availability>preorder</g:availability>'."\n";
			echo '<g:image_link><![CDATA[http://images.example.com/DVD-0564738?size=large&format=PNG]]></g:image_link>'."\n";
			echo '<g:shipping>'."\n";
				echo '<g:country>US</g:country>'."\n";
				echo '<g:service>Express Mail</g:service>'."\n";
				echo '<g:price>3.80 USD</g:price>'."\n";
			echo '</g:shipping>'."\n";
				
				//<!-- The following attributes are required because this item is not apparel or a custom good -->
			echo '<g:gtin>5030697019233</g:gtin>'."\n";
			echo '<g:brand>BBC</g:brand>'."\n";
				
				//<!-- The following attribute is required because this item is in the 'Media' category -->
			echo '<g:google_product_category><![CDATA[Media > DVDs & Movies > Television Shows]]></g:google_product_category>'."\n";
				
				//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
			echo '<g:product_type><![CDATA[DVDs & Movies > TV Series > Fantasy Drama]]></g:product_type>'."\n";
		echo '</entry>'."\n";
			
			//<!-- Third example shows how to include multiple images and shipping values -->
		echo '<entry>'."\n";
				//<!-- The following attributes are always required -->
			echo '<title>Dior Capture R60/80 XP Restoring Wrinkle Creme Rich Texture 30ml</title>'."\n";
			echo '<link href="http://www.example.com/perfumes/product?Dior%20Capture%20R6080%20XP"/>'."\n";
			echo '<summary>Dior Capture R60/80 XP Ultimate Wrinkle Creme reinvents anti-wrinkle care by protecting and relaunching skin cell activity to encourage faster, healthier regeneration.</summary>'."\n";
			echo '<g:id>PFM654321</g:id>'."\n";
			echo '<g:condition>new</g:condition>'."\n";
			echo '<g:price>46.75 USD</g:price>'."\n";
			echo '<g:availability>in stock</g:availability>'."\n";
			echo '<g:image_link>http://images.example.com/PFM654321_1.jpg</g:image_link>'."\n";
			echo '<g:shipping>'."\n";
				echo '<g:country>US</g:country>'."\n";
				echo '<g:service>Standard Rate</g:service>'."\n";
				echo '<g:price>4.95 USD</g:price>'."\n";
			echo '</g:shipping>'."\n";
			echo '<g:shipping>'."\n";
				echo '<g:country>US</g:country>'."\n";
				echo '<g:service>Next Day</g:service>'."\n";
				echo '<g:price>8.50 USD</g:price>'."\n";
			echo '</g:shipping>'."\n";
				
				//<!-- The following attributes are required because this item is not apparel or a custom good -->
			echo '<g:gtin>3348900839731</g:gtin>'."\n";
			echo '<g:brand>Dior</g:brand>'."\n";
				
				//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
			echo '<g:product_type>Health &amp; Beauty &gt; Personal Care &gt; Cosmetics &gt; Skin Care &gt; Lotion</g:product_type>'."\n";
			echo '<g:additional_image_link>http://images.example.com/PFM654321_2.jpg</g:additional_image_link>'."\n";
			echo '<g:additional_image_link>http://images.example.com/PFM654321_3.jpg</g:additional_image_link>'."\n";
		echo '</entry>'."\n";
			
			//<!-- Fourth example shows what attributes are required and recommended for items that are in the apparel category -->
		echo '<entry>'."\n";
				//<!-- The following attributes are always required -->
			echo '<title>Roma Cotton Rich Bootcut Jeans with Belt - Size 8 Standard</title>'."\n";
			echo '<link href="http://www.example.com/clothing/women/Roma-Cotton-Bootcut-Jeans/?extid=CLO-29473856"/>'."\n";
			echo '<summary>Comes with the belt. A smart pair of bootcut jeans in stretch cotton. The flower print buckle belt makes it extra stylish.</summary>'."\n";
			echo '<g:id>CLO-29473856-1</g:id>'."\n";
			echo '<g:condition>new</g:condition>'."\n";
			echo '<g:price>29.50 USD</g:price>'."\n";
			echo '<g:availability>out of stock</g:availability>	'."\n";
			echo '<g:image_link>http://images.example.com/CLO-29473856-front.jpg</g:image_link>'."\n";
	
				//<!-- For use in combination with the account level shipping setting -->
			echo '<g:shipping_weight>750 g</g:shipping_weight>'."\n";
			
				//<!-- The following attributes are required because this item is apparel -->
			echo '<g:google_product_category>Apparel &amp; Accessories &gt; Clothing &gt; Jeans</g:google_product_category>'."\n";
			echo '<g:gender>Female</g:gender>'."\n";
			echo '<g:age_group>Adult</g:age_group>'."\n";
			echo '<g:color>Navy</g:color>'."\n";
			echo '<g:size>8 Standard</g:size>'."\n";
				
				//<!-- The following attribute is required because this item has variants -->
			echo '<g:item_group_id>CLO-29473856</g:item_group_id>'."\n";
				
				//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
			echo '<g:brand>M&amp;S</g:brand>'."\n";
			echo '<g:mpn>B003J5F5EY</g:mpn>'."\n";
			echo '<g:product_type>Womens Clothing &gt; Jeans &gt; Bootcut Jeans</g:product_type>'."\n";
			echo '<g:additional_image_link>http://images.example.com/CLO-29473856-side.jpg</g:additional_image_link>'."\n";
			echo '<g:additional_image_link>http://images.example.com/CLO-29473856-back.jpg</g:additional_image_link>'."\n";
		echo '</entry>'."\n";
			
			//<!-- This is a variant of the last item (same 'item group id'). In this case the variant is only by size, but the item could be repeated in the same way for other variants -->
		echo '<entry>'."\n";
				//<!-- The following attributes are always required -->
			echo '<title>Roma Cotton Rich Bootcut Jeans with Belt - Size 8 Tall</title>'."\n";
			echo '<link href="http://www.example.com/clothing/women/Roma-Cotton-Bootcut-Jeans/?extid=CLO-29473856"/>'."\n";
			echo '<summary>Comes with the belt. A smart pair of bootcut jeans in stretch cotton. The flower print buckle belt makes it extra stylish.</summary>'."\n";
			echo '<g:id>CLO-29473856-2</g:id>'."\n";
			echo '<g:condition>new</g:condition>'."\n";
			echo '<g:price>29.50 USD</g:price>'."\n";
			echo '<g:availability>in stock</g:availability>'."\n";
			echo '<g:image_link>http://images.example.com/CLO-29473856-front.jpg</g:image_link>'."\n";
	
				//<!-- For use in combination with the account level weight based shipping setting -->
			echo '<g:shipping_weight>820 g</g:shipping_weight>'."\n";
			
				//<!-- The following attributes are required because this item is apparel -->
			echo '<g:google_product_category>Apparel &amp; Accessories &gt; Clothing &gt; Jeans</g:google_product_category>'."\n";
			echo '<g:gender>Female</g:gender>'."\n";
			echo '<g:age_group>Adult</g:age_group>'."\n";
			echo '<g:color>Navy</g:color>'."\n";
			echo '<g:size>8 Tall</g:size>'."\n";
				
				//<!-- The following attribute is required because this item has variants -->
			echo '<g:item_group_id>CLO-29473856</g:item_group_id>'."\n";
				
				//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
			echo '<g:brand>M&amp;S</g:brand>'."\n";
			echo '<g:mpn>B003J5F5EY</g:mpn>'."\n";
			echo '<g:product_type>Women Clothing &gt; Jeans &gt; Bootcut Jeans</g:product_type>'."\n";
			echo '<g:additional_image_link>http://images.example.com/CLO-29473856-side.jpg</g:additional_image_link>'."\n";
			echo '<g:additional_image_link>http://images.example.com/CLO-29473856-back.jpg</g:additional_image_link>'."\n";
		echo '</entry>'."\n";
			
			//<!-- Fifth example demonstrates the use of the sale price attributes  -->
		echo '<entry>'."\n";
				//<!-- The following attributes are always required -->
			echo '<title>Tenn Cool Flow Ladies Long Sleeved Cycle Jersey</title>'."\n";
			echo '<link href="http://www.example.com/clothing/sports/product?id=CLO1029384&amp;src=gshopping&amp;popup=false"/>'."\n";
			echo '<summary>A ladies cycling jersey designed for the serious cyclist, tailored to fit a feminine frame. This sporty, vibrant red, black and white jersey is constructed of a special polyester weave that is extremely effective at drawing moisture away from your body, helping to keep you dry.  With an elasticised, gripping waist, it will stay in place for the duration of your cycle, and wont creep up like many other products. It has two elasticised rear pockets and the sleeves are elasticated to prevent creep-up.</summary>'."\n";
			echo '<g:id>CLO-1029384</g:id>'."\n";
			echo '<g:condition>new</g:condition>'."\n";
			echo '<g:price>33.99 USD</g:price>'."\n";
			echo '<g:availability>available for order</g:availability>'."\n";
			echo '<g:image_link>http://images.example.com/CLO-1029384.jpg</g:image_link>'."\n";
			echo '<g:shipping>'."\n";
				echo '<g:country>US</g:country>'."\n";
				echo '<g:service>Standard Free Shipping</g:service>'."\n";
				echo '<g:price>0 USD</g:price>'."\n";
			echo '</g:shipping>'."\n";
				
				//<!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
			echo '<g:gtin>5060155240282</g:gtin>'."\n";
			echo '<g:product_type>Sporting Goods &gt; Outdoor Recreation &gt; Cycling &gt; Bicycle Clothing &gt; Bicycle Jerseys</g:product_type>'."\n";
			echo '<g:gender>Female</g:gender>'."\n";
			echo '<g:age_group>Adult</g:age_group>'."\n";
	
			//<!-- Indicates all the colours found on the garment in order of dominance -->
			echo '<g:color>Black/Red/White</g:color>'."\n";
			echo '<g:size>M</g:size>'."\n";
				
				//<!-- The following demonstrate the use of the 'sale price' and 'sale price effective date' and attributes -->
			echo '<g:sale_price>25.49 USD</g:sale_price>'."\n";
			echo '<g:sale_price_effective_date>2011-09-01T16:00-08:00/2011-09-03T16:00-08:00</g:sale_price_effective_date>'."\n";
		echo '</entry>'."\n";
	echo '</feed>'."\n";
}

function format_prod_rdf($data=array())
{
	$CI =& get_instance();
	$bhs = current_lang(false);

	echo '<?xml version="1.0"?>'."\n";
	echo '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'."\n";
	echo 'xmlns="http://purl.org/rss/1.0/"'."\n";
	echo 'xmlns:g="http://base.google.com/ns/1.0">'."\n";
	echo '<channel rdf:about="http://www.example.com/RSS1.xml">'."\n";
			echo '<title>The name of your data feed</title>'."\n";
			echo '<link>http://www.example.com</link>'."\n";
			echo '<description>A description of your content</description>'."\n";
			echo '<items>'."\n";
				echo '<rdf:Seq>'."\n";
					echo '<rdf:li rdf:resource="http://www.example.com/item1-info-page.html"/>'."\n";
				echo '</rdf:Seq>'."\n";
			echo '</items>'."\n";
		echo '</channel>'."\n";
		echo '<item rdf:about="http://www.example.com/item1-info-page.html">'."\n";
			echo '<title>Red wool sweater</title>'."\n";
			echo '<link>http://www.example.com/item1-info-page.html</link>'."\n";
			echo '<description>Comfortable and soft, this sweater will keep you warm on those cold winter nights.</description>'."\n";
			echo '<g:image_link>http://www.example.com/image1.jpg</g:image_link>'."\n";
			echo '<g:price>25</g:price>'."\n";
			echo '<g:condition>new</g:condition>'."\n";
			echo '<g:id>1a </g:id>'."\n";
		echo '</item>'."\n";
	echo '</rdf:RDF>'."\n";
}
/* End RSS FORMAT */

/* MIcs */
function cek_koneksi_internet($timeout=2,$url='') 
{
	//$CI =& get_instance();
	$status = TRUE;
	
	if(ENVIRONMENT == 'development' || ENVIRONMENT == 'm-testing' || ENVIRONMENT == 'testing' || ENVIRONMENT == 'staging')
	{
		if (empty($url))
		$conn = @fsockopen("www.php.net", 80, $errno, $errstr, $timeout);
		else
		$conn = @fsockopen($url, 80, $errno, $errstr, $timeout);
		
		if ($conn)
		{
			fclose($conn);
		}
		else
		{
			$status = FALSE;
		}
	}
	
	return $status; 
}

function tsout($in='',$label='',$format=TRUE)
{
	if($format)
	print('DATA <strong>'.strtoupper($label).'</strong>: <small><pre>'.print_r($in, TRUE).'</pre></small>');
	else
	print('<small><pre>'.print_r($in, TRUE).'</pre></small>');
}

function cpy($asal='', $tujuan='', $fm=FILE_CMS_MODE, $dm=DIR_CMS_MODE)
{
	$result=false;
    if(is_dir($asal)) {
        $dir_handle=opendir($asal);
        while($file=readdir($dir_handle)){
            if($file!="." && $file!=".."){
                if(is_dir($asal.DS.$file)){
                    if( ! is_dir($tujuan.DS.$file)){
                        $result = mkdir($tujuan.DS.$file, $dm);
                    }
                    $result = cpy($asal.DS.$file, $tujuan.DS.$file);
                } else {
                    $result = copy($asal.DS.$file, $tujuan.DS.$file);
                    #chmod($tujuan.DS.$file, $fm);
                }
            }
        }
        closedir($dir_handle);
    } else {
		if(is_file($tujuan))
        $result = copy($asal, $tujuan);
        #chmod($tujuan, $fm);
    }

    return $result;
}

function smartCopy($source='', $dest='', $options=array('folderPermission'=>DIR_CMS_MODE,'filePermission'=>FILE_CMS_MODE))
{
	$result=false;
	if (is_file($source)) {
		if ($dest[strlen($dest)-1]=='/') {
			if (!file_exists($dest)) {
				#cmfcDirectory::makeAll($dest,$options['folderPermission'],true);
				//return $result;
			}
			$__dest=$dest."/".basename($source);
		} else {
			$__dest=$dest;
		}
		$result=copy($source, $__dest);
		chmod($__dest,$options['filePermission']);
	   
	} elseif(is_dir($source)) {
		if ($dest[strlen($dest)-1]=='/') {
			if ($source[strlen($source)-1]=='/') {
				//Copy only contents
			} else {
				//Change parent itself and its contents
				$dest=$dest.basename($source);
				@mkdir($dest,$options['folderPermission']);
				#chmod($dest,$options['filePermission']);
			}
		} else {
			if ($source[strlen($source)-1]=='/') {
				//Copy parent directory with new name and all its content
				@mkdir($dest,$options['folderPermission']);
				#chmod($dest,$options['filePermission']);
			} else {
				//Copy parent directory with new name and all its content
				@mkdir($dest,$options['folderPermission']);
				#chmod($dest,$options['filePermission']);
			}
		}

		$dirHandle=opendir($source);
		while($file=readdir($dirHandle))
		{
			if($file!="." && $file!="..")
			{
				 if(!is_dir($source."/".$file)) {
					$__dest=$dest."/".$file;
				} else {
					$__dest=$dest."/".$file;
				}
				//echo "$source/$file ||| $__dest<br />";
				$result=smartCopy($source."/".$file, $__dest, $options);
			}
		}
		closedir($dirHandle);
	   
	} else {
		$result=false;
	}
	return $result;
}

function hapus_file($file='')
{
	if (file_exists($file) && is_writable($file))
		unlink($file);
}

// buka data terenkripsi
function dekodeString($str='')
{
	$CI =& get_instance();
	$string = $CI->encrypt->decode($str,$CI->config->item('pkey'));
	return $string;
}

// enkripsi data
function enkodeString($str='')
{
	$CI =& get_instance();
	$string = $CI->encrypt->encode($str,$CI->config->item('pkey'));
	return $string;
}

//format <br>
function jadi_br($str='')
{
	$fbr = str_replace("\n","<br />", $str);
	return $fbr;
}

function jadi_nl($str='')
{
	$fnl = str_replace("\\n", "\n",$str);
	return $fnl;
}

function jdjdl($str='')
{
	$str = str_replace('_', ' ', $str);
	$str = ucwords($str);
	return $str;
}

//format bersihkan spasi yg double
function bersih_spasi($str='')
{
	return preg_replace('/\s\s+/', ' ', $str);
}

// format string
function format_keyword_pke_spasi($str='',$cekspasi=FALSE)
{
	$CI =& get_instance();
	$CI->load->helper(array('text','string'));

	$str = rip_tags(humanize($str), TRUE);
	$str = word_limiter($str, 50, '');
	$keyword = rtrim(trim($str),",");
	$keyword = preg_replace('/\.+/', ',', $keyword);
	$keyword = preg_replace('/\s+,/',',', strtolower($keyword));
	$keyword = reduce_multiples($keyword,",",TRUE);

	if($cekspasi===true)
	$keyword = explode(" ",$keyword);
	else
	$keyword = explode(",",$keyword);

	$keyword = array_unique($keyword);

	return implode(",", $keyword);
	#return $str;
}

function format_keyword($str='')
{
	$CI =& get_instance();
	$CI->load->helper(array('text','string'));

	$str = rip_tags($str);
	$keyword = word_limiter(strip_tags($str), 50, '');
	$keyword = rtrim(trim($str),",");
	$keyword = preg_replace('/\.+/', '', $keyword);
	$keyword = preg_replace('/\s+/', ',', strtolower($keyword));
	$keyword = reduce_multiples($keyword,",",TRUE);

	$wordArray = explode(",",$keyword);
	$keyword = array_unique($wordArray);

	return implode(",", $keyword);
}

function rip_tags($string='',$koma=FALSE)
{
	$string = strip_tags($string);
	$string = html_entity_decode($string);
	$string = htmlentities($string, ENT_QUOTES);
	$string = urldecode($string);

	if($koma === TRUE)
	$string = preg_replace('/[^A-Z a-z 0-9 -]/', ',', $string);

	#$string = preg_replace('/ +/', ' ', $string);
	$string = trim($string);
    $string = preg_replace ('/<[^>]*>/', ' ', $string);
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
   
    return $string;
}

function format_pilkategori($str='')
{
	$kategori = trim($str);
	$kategori = preg_replace('/(?<=\\w)(?=[A-Z])/', ' $1', $kategori);

	return $kategori;
}

function format_txtkategori($str='')
{
	$kategori = ucwords($str);
	$kategori = preg_replace('/\s+/', '', $kategori);

	return $kategori;
}

/*
 * @param $kat => $_POST['kat']
*/
function format_simpan_kategori_halaman($kat='')
{
	//jadi array
	$tag = json_decode($kat, true);

	if(is_array($tag) && count($tag) > 0)
	{
		foreach($tag as $item)
		{
			$convtag[] = format_txtkategori($item);
		}

		$tags = implode(",",$convtag);
		return $tags;
	}
	else {
		return NULL;
	}
}

function format_kategori($str='', $jadi_array=FALSE)
{
	$CI =& get_instance();
	$CI->load->helper(array('string'));

	$kategori = preg_replace('/\s+/', '', $str);
	$kategori = reduce_multiples($kategori,",",TRUE);
	$wordArray = explode(",",$kategori);
	$kategori = array_unique($wordArray);

	if ($jadi_array === TRUE)
	return $kategori;
	else
	return implode(",", $kategori);
}

function format_json_pilkategori($str='')
{
	$kategori = format_pilkategori($str);
	$kategori = explode(",", $kategori);
	return json_encode($kategori);
}

function format_array_idtag($tag=array())
{
	$ftag = json_decode($tag, true);
	return $ftag;
}

function trim_judul($str)
{
	$trim = preg_replace('#\s+#', ' ', $str);
	return ucwords(trim($trim));
}

// format rupiah
function format_harga_indo($nilai='')
{
	return number_format($nilai, 0,'','.');
}

function format_tgl_indo($tgl='')
{
	return date('d-m-Y H:i:s', strtotime($tgl));
}

function format_tgl_sql($tgl='')
{
	return date('Y-m-d H:i:s', strtotime($tgl));
}

function format_tgl_indo_jq($tgl='')
{
	return date('d-m-Y', strtotime($tgl));
}

function format_tgl_jq($tgl='')
{
	return date('Y-m-d', strtotime($tgl));
}

function bersihkanUrlPaginasi($str='')
{
	return preg_replace('/\b.(?:html)\b/i','',$str);
}

function format_string_aman($str='')
{
	$txt = filter_var($str, FILTER_SANITIZE_STRING);
	$txt = trim($txt);
	return $txt;
}

function format_nilai_k($nilai=0)
{
	if($nilai >= 1000)
	$hasil = round(($nilai/1000), $nilai >= 10000?2:1, PHP_ROUND_HALF_UP).'<sup>K</sup>';
	else if($nilai >= 1000000)
	$hasil = round(($nilai/1000000), 3, PHP_ROUND_HALF_UP).'<sup>M</sup>';
	else
	$hasil = $nilai;

	return $hasil;
}

function formatTanggal($bhs='id', $date=null)
{
	$array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
	$array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
	if($date == null) {
		$hari = $array_hari[date('N')];
		$tanggal = date ('j');
		$bulan = date('m');
		$tahun = date('Y');
	} else {
		if(!is_numeric($date))
		$date = strtotime($date);
		$hari = $array_hari[date('N',$date)];
		$tanggal = date ('j', $date);
		$bulan = date('m');
		$tahun = date('Y',$date);
	}
	$formatTanggal = $hari . ", " . $tanggal .".". $bulan .".". $tahun;

	if($bhs=='id')
	return $formatTanggal;
	else
	return date('l, Y.m.d');
}

function daftarSitemapUrl($menu='',$ul_cls='')
{
	echo '<ul class="'.$ul_cls.'">';
		$CI =& get_instance();
		$attr = array();
		$halaman = $CI->cms->getAllHalamanFrontByNama(underscore($menu));
	
		if( ! empty($halaman->kategori) && $halaman->tipe == 'produk' && $CI->config->item('ecom_aktif')) {
			$cnt = $CI->mproduk->getCountFrontProdukByKategori($halaman->kategori);
			$daftar = $CI->mproduk->getAllSitemapFrontProdukByKategori(current_lang(false),$halaman->kategori,$cnt);
			$taut = current_lang().'pdetail/';
		}
		elseif( ! empty($halaman->kategori) && $halaman->tipe == 'blog') {
			$cnt = $CI->cms->getCountAllBlogFrontByKategori($halaman->kategori);
			$daftar = $CI->cms->getAllSitemapBlogFrontByKategori(current_lang(false),$halaman->kategori,$cnt);
			$taut = current_lang().'posting/';
		}
		elseif ( ! empty($halaman->kategori) && $halaman->tipe == 'album') {
			$cnt = $CI->cms->getCountAllAlbumFrontByKategori($halaman->kategori);
			$daftar = $CI->cms->getAllSitemapAlbumFrontByKategori(current_lang(false),$halaman->kategori,$cnt);
			$taut = current_lang().'gallery/';
		}
		elseif ( ! empty($halaman->kategori) && $halaman->tipe == 'video') {
			$cnt = $CI->cms->getCountAllVideoFrontByKategori($halaman->kategori);
			$daftar = $CI->cms->getAllSitemapVideoFrontByKategori(current_lang(false),$halaman->kategori,$cnt);
			$taut = current_lang().'playlist/';
		}
	
		if( isset($daftar))
		{
			foreach($daftar as $item)
			{
				$url = $taut.underscore($item->judul);
				echo '<li> --| '.anchor($url, $item->judul, array('title' => $item->judul)).'</li>';
			}
		}
	echo '</ul>';
}

function daftarSitemapUrlXML($menu='')
{
	$CI =& get_instance();
	$attr = array();
	$halaman = $CI->cms->getAllHalamanFrontByNama(underscore($menu));

	if( ! empty($halaman->kategori) && $halaman->tipe == 'produk') {
		$cnt = $CI->mproduk->getCountFrontProdukByKategori($halaman->kategori);
		$daftar = $CI->mproduk->getAllSitemapFrontProdukByKategori(current_lang(false),$halaman->kategori,$cnt);
		$taut = current_lang().'pdetail/';
	}
	elseif( ! empty($halaman->kategori) && $halaman->tipe == 'blog') {
		$cnt = $CI->cms->getCountAllBlogFrontByKategori($halaman->kategori);
		$daftar = $CI->cms->getAllSitemapBlogFrontByKategori(current_lang(false),$halaman->kategori,$cnt);
		$taut = current_lang().'posting/';
	}
	elseif ( ! empty($halaman->kategori) && $halaman->tipe == 'album') {
		$cnt = $CI->cms->getCountAllAlbumFrontByKategori($halaman->kategori);
		$daftar = $CI->cms->getAllSitemapAlbumFrontByKategori(current_lang(false),$halaman->kategori,$cnt);
		$taut = current_lang().'gallery/';
	}
	elseif ( ! empty($halaman->kategori) && $halaman->tipe == 'video') {
		$cnt = $CI->cms->getCountAllVideoFrontByKategori($halaman->kategori);
		$daftar = $CI->cms->getAllSitemapVideoFrontByKategori(current_lang(false),$halaman->kategori,$cnt);
		$taut = current_lang().'playlist/';
	}

	if( isset($daftar))
	{
		foreach($daftar as $item)
		{
			$url = $taut.underscore($item->judul);
			echo '<url>' . "\n";
			echo '<loc>'.site_url($url).'</loc>' . "\n";
			echo '</url>' . "\n";
		}
	}
}

function breadNav($tipe='',$jd_array=false,$url='')
{
	$CI =& get_instance();
	if(empty($url))
	$url = $CI->uri->uri_string();

	$pecah = explode("/", $url);
	$base = base_url().current_lang();
	$bhs = current_lang(false);
	$ext = config_item('url_suffix');
	$jml = 0;
	$purl = '';
	$nokon = array('home','cari','sitemap','pemesanan');
	$escl = array_merge(config_item('reserved_halaman'), $nokon);
	$yeskon = array('album','albums','gallery','video','videos','playlist','blog','posting','pdetail','produk','products');

	$link1 = '';
	$nav = '<nav class="breadcrumbs small no-phone">';
	$nav .= '<style>nav.breadcrumbs ul li {float:left}</style>';
	$nav .= '<ul>';

	//Home
	if(!in_array($tipe, $nokon))
	$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="'.base_url().'"><i class="icon-home"></i><span itemprop="title">Home</span></a></li>';
	$arr[] = array(base_url(),'Home');

	//Halaman
	if($CI->uri->segment(2) == 'halaman' || $CI->uri->segment(2) == 'pages')
	{
		if($CI->session->userdata('pbasis') && $tipe=='produk')
		$purl = base_url($CI->session->userdata('pbasis')).$ext;

		if($CI->session->userdata('blogbasis') && $tipe=='blog')
		$purl = base_url($CI->session->userdata('blogbasis')).$ext;

		if($CI->session->userdata('albbasis') && $tipe=='album')
		$purl = base_url($CI->session->userdata('albbasis')).$ext;

		if($CI->session->userdata('vidbasis') && $tipe=='video')
		$purl = base_url($CI->session->userdata('vidbasis')).$ext;

		if($tipe=='halaman')
		$purl = base_url($url).$ext;

		$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$purl.'"><span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></a></li>';
		$arr[] = array($purl,humanize($CI->uri->segment(3)));
		
		if($tipe=='module')
		{
			$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></li>';
			$arr[] = array('',humanize($CI->uri->segment(3)));
		}
	}


	foreach($pecah as $link)
	{
		$txt_kat = '';
		$kat_url = '';
		$txt_tag = '';
		$tag_url = '';
		if(in_array($link, $escl))
		continue;

		if(($link == 'produk' || $link == 'product' || $link == 'pdetail') && $tipe=='produk')
			$link1 = 'pdetail';
		if(($link == 'album' || $link == 'albums' || $link == 'gallery') && $tipe=='album')
			$link1 = 'gallery';
		if(($link == 'video' || $link == 'videos' || $link == 'playlist') && $tipe=='video')
			$link1 = 'playlist';
		if(($link == 'blog' || $link == 'posting') && $tipe='blog')
			$link1 = 'posting';

		//Basis|Kategori|Tag
		if(!empty($link1))
		{
			if(!in_array($link, $nokon))
			{
				if($jml == 0 && $CI->uri->segment(2) != 'halaman' && $CI->uri->segment(2) != 'pages') {
					if($tipe=='produk' && $bhs == 'en') {
						$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$base.$link1.$ext.'"><span itemprop="title">'.humanize('product').'</span></a></li>';
						$lbl_tipe = humanize('product');
					} else {
						$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$base.$link1.$ext.'"><span itemprop="title">'.humanize($tipe).'</span></a></li>';
						$lbl_tipe = humanize($tipe);
					}
					$arr[] = array($base.$link1.$ext,humanize($lbl_tipe));
				}
				else {
					if($jml == 0) {
						$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></li>';
						$arr[] = array('',humanize($CI->uri->segment(3)));
					}
				}

				//Link Kategori konten by session kat_konten
				if($jml == 0 && $CI->session->userdata('kat_konten'))
				{
					$txt_kat = $CI->session->userdata('kat_konten');
					if($tipe == 'produk')
					{
						if($bhs === 'id')
						$kat_url = base_url(current_lang().'kategori/produk/'.underscore($txt_kat));
						else
						$kat_url = base_url(current_lang().'category/product/'.underscore($txt_kat));
					}

					if($tipe == 'blog')
					{
						if($bhs === 'id')
						$kat_url = base_url(current_lang().'kategori/blog/'.underscore($txt_kat));
						else
						$kat_url = base_url(current_lang().'category/blog/'.underscore($txt_kat));
					}

					if($tipe == 'album')
					{
						if($bhs === 'id')
						$kat_url = base_url(current_lang().'kategori/album/'.underscore($txt_kat));
						else
						$kat_url = base_url(current_lang().'category/album/'.underscore($txt_kat));
					}

					if($tipe == 'video')
					{
						if($bhs === 'id')
						$kat_url = base_url(current_lang().'kategori/video/'.underscore($txt_kat));
						else
						$kat_url = base_url(current_lang().'category/video/'.underscore($txt_kat));
					}

					$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$kat_url.$ext.'"><span itemprop="title">'.$txt_kat.'</span></a></li>';
					$arr[] = array($kat_url.$ext,$txt_kat);

					//unset aja klo udah dipake
					$CI->session->unset_userdata('kat_konten');
				}

				//Link Kategori konten by session kat_konten
				if($jml == 0 && $CI->session->userdata('tag_konten'))
				{
					$txt_tag = $CI->session->userdata('tag_konten');

					if($tipe == 'blog')
						$tag_url = base_url(current_lang().'tag/blog/'.underscore($txt_tag));

					if($tipe == 'album')
						$tag_url = base_url(current_lang().'tag/album/'.underscore($txt_tag));

					if($tipe == 'video')
						$tag_url = base_url(current_lang().'tag/video/'.underscore($txt_tag));

					$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a rel="tag" itemprop="url" href="'.$tag_url.$ext.'"><span itemprop="title">#'.$txt_tag.'</span></a></li>';
					$arr[] = array($tag_url.$ext,'#'.$txt_tag);

					//unset aja klo udah dipake
					$CI->session->unset_userdata('tag_konten');
				}
			}

			//Link konten halaman by session *basis
			if($jml == 1 && $CI->session->userdata('pbasis') && $tipe=='produk')
			{
				$bsurl = base_url($CI->session->userdata('pbasis')).$ext;
				$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$bsurl.'"><span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></a></li>';
				$arr[] = array($bsurl,humanize($CI->uri->segment(3)));
			}
			if($jml == 1 && $CI->session->userdata('blogbasis') && $tipe=='blog')
			{
				$bsurl = base_url($CI->session->userdata('blogbasis')).$ext;
				$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$bsurl.'"><span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></a></li>';
				$arr[] = array($bsurl,humanize($CI->uri->segment(3)));
			}
			if($jml == 1 && $CI->session->userdata('albbasis') && $tipe=='album')
			{
				$bsurl = base_url($CI->session->userdata('albbasis')).$ext;
				$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$bsurl.'"><span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></a></li>';
				$arr[] = array($bsurl,humanize($CI->uri->segment(3)));
			}
			if($jml == 1 && $CI->session->userdata('vidbasis') && $tipe=='video')
			{
				$bsurl = base_url($CI->session->userdata('vidbasis')).$ext;
				$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <a itemprop="url" href="'.$bsurl.'"><span itemprop="title">'.humanize($CI->uri->segment(3)).'</span></a></li>';
				$arr[] = array($bsurl,humanize($CI->uri->segment(3)));
			}

			if($jml == 1 && in_array($link1, $pecah)){
				$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <span itemprop="title">'.humanize($link).'</span></li>';
				$arr[] = array('',humanize($link));
			}
		}
		else {
			$nav .= '';
			continue;
		}

		$jml++;

		if($jml == 3){
			$nav .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;›&nbsp; <span itemprop="title">'.word_limiter(humanize($link),5).'</span></li>';
			$arr[] = array('',word_limiter(humanize($link),5));
		}
	}
	$nav .= '</ul>';
	$nav .= '</nav>';
	$nav .= '<div style="clear:left"></div>';

	if($jd_array)
	return $arr;
	else
	return $nav;
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_tbaru_widget extends CI_Model {

	var $tbl_tbaru_widget   = 'tbl_wid_tbaru';

    function __construct()
    {
        // Call the Model constructor
		$this->db = $this->load->database('default', TRUE);
        parent::__construct();
    }
	
	function getAllEntries()
    {
		$query = $this->db->get($this->tbl_tbaru_widget);
		if($query->num_rows > 0)
		{
			return $query->result();
		}else{
			return NULL;
		}
    }
	
	function adaNama($str)
	{
		$query = $this->db->query("SELECT * FROM " . $this->tbl_tbaru_widget . " WHERE judul = '".$str."'");
		if ($query->num_rows() > 0)
		{
		   return FALSE;
		}else{
		   return TRUE;
		}
	}
	
	function addData()
	{
		$data = array(
				'judul' => $this->input->post('nama'),
				'isi' => $this->input->post('isi')
		);
		
		$str = $this->input->post('nama');
		$query = $this->db->query("SELECT * FROM " . $this->tbl_tbaru_widget . " WHERE judul = '".$str."'");
		if ($query->num_rows() > 0)
		{
		   return FALSE;
		}else{
		   $this->db->insert($this->tbl_tbaru_widget ,$data);
		   return TRUE;
		}
	}
	
}

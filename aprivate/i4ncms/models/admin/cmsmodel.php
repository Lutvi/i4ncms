<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

Class CmsModel extends MY_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /* Menu */
    function maxPosMenu($menu_parent='')
    {
        $this->db->select_max('posisi');

        if(empty($menu_parent))
        {
			$query = $this->db->get($this->tbl_menu);
        }
        else {
			$this->db->order_by("parentid", "asc");
			$this->db->where('parentid', $menu_parent);
			$query = $this->db->get($this->tbl_menu);
        }
        
        $row = $query->row();
        $pos = $row->posisi;
        $pos++;

        if ($pos > $this->maxPosMenuInduk())
        {
			return $pos;
		}
		else {
			$this->db->select_max('posisi');

			$this->db->where('id', $menu_parent);
			$query = $this->db->get($this->tbl_menu);
			$row = $query->row();
			$pos = $row->posisi;
			$pos++;

			return $pos;
		}
    }

    function maxPosMenuInduk()
    {
        $this->db->select_max('posisi');
        $query = $this->db->get_where($this->tbl_menu,array('parentid' => 0));
        $row = $query->row();
        $pos = $row->posisi;
        $pos++;

		return $pos;
    }

    function getLevelMenuByParent($pid='')
    {
		$this->db->select('level');
		$query = $this->db->get_where($this->tbl_menu,array('id' => $pid),1);
		if($query->num_rows() > 0)
		{
	        $row = $query->row();
			return $row->level+1;
		}
		else {
			return 0;
		}
    }

    function posHapusMenuUpdate($pos='')
    {
		$this->db->order_by("parentid", "asc");
		$this->db->where('posisi >',$pos);
		$query = $this->db->get($this->tbl_menu);
		
		foreach($query->result() as $row)
		{
			// update posisi
			$idmn = $row->id;
			$data = array('posisi' => $pos);
			$this->db->update($this->tbl_menu, $data, array('id' => $idmn));
			$pos++;
		}
    }

    function posHapusMenuUpdateRapih()
    {
		$this->db->order_by("parentid", "asc");
		#$this->db->where('posisi >',$pos);
		$query = $this->db->get($this->tbl_menu);
		$pos=1;
		foreach($query->result() as $row)
		{
			// update posisi
			$idmn = $row->id;
			$data = array('posisi' => $pos);
			$this->db->update($this->tbl_menu, $data, array('id' => $idmn));
			$pos++;
		}
    }

    function posMenuUpdate($pos='')
    {
		$this->db->order_by("parentid", "asc");
		$this->db->where('posisi >=',$pos);

		$query = $this->db->get($this->tbl_menu);
		$pos = $pos+1;

		foreach($query->result() as $row)
		{
			// update posisi
			$idmn = $row->id;
			$data = array('posisi' => $pos);
			$this->db->update($this->tbl_menu, $data, array('id' => $idmn));
			$pos++;
		}
    }

    function addMenu($id_hal='')
    {
		if($this->input->post('menu_parent') === '0')
		{
			$pos = $this->maxPosMenuInduk();
		}
		else {
			$pos = $this->maxPosMenu($this->input->post('menu_parent'));
		}

		$this->posMenuUpdate($pos);
		$level = $this->getLevelMenuByParent($this->input->post('menu_parent'));

        $data = array(
                'title_id' => ucfirst($this->input->post('nama_id')),
                'title_en' => ucfirst($this->input->post('nama_en')),
                'link' => ( ! empty($id_hal))?$id_hal:$this->input->post('link_menu'),
                'parentid' => $this->input->post('menu_parent'),
                'level' => $level,
                'status' => 1,
                'posisi' => $pos
        );

        $this->db->where('title_id', $this->input->post('nama_id'));
		$this->db->or_where('title_en', $this->input->post('nama_en')); 
        $query = $this->db->get($this->tbl_menu);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else{
           if( ! $this->db->insert($this->tbl_menu,$data))
           return FALSE;
           else
           return TRUE;
        }
    }

    function upMenuPos()
    {
        if( $this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('mnid') )
        {
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_menu, $data);
            }

            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id', $this->input->post('mnid'));
                $this->db->update($this->tbl_menu, $data);
            }

            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function adaMenu($str='')
    {
		$this->db->where('title_id', $str);
		$this->db->or_where('title_en', $str); 
        $query = $this->db->get($this->tbl_menu);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function getAllMenu($limit=0, $offset=0)
    {
        $this->db->limit($limit, $offset);

        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_menu);
        return $query->result();
    }
    
    function getCountMenu()
    {
        $query = $this->db->get($this->tbl_menu);
        return $query->num_rows(); 
    }

    function getParentName($parent)
    {
        $this->db->select('title_'.current_lang(FALSE).' AS title');
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get_where($this->tbl_menu, array('id'=>$parent), 1);
        if ($query->num_rows() > 0)
        {
           $row = $query->row(); 
           return $row->title;
        }
        else {
            return NULL;
        }
    }

    function getTitleMenu()
    {
        $this->db->select('id, level, title_'.current_lang(FALSE).' AS title');
        $this->db->order_by("posisi", "asc");
        
        $query = $this->db->get($this->tbl_menu);
        return $query->result();
    }

	// func ini relasi dengan model halaman
	function upNamaMenu()
    {
		if($this->input->post('tipe') == 'top')
		{
			$data = array(
				   'title_id' => ucfirst($this->input->post('nama_id')),
				   'title_en' => ucfirst($this->input->post('nama_en')),
				   'link' => 0
				);
		}
		else {
			$data = array(
			   'title_id' => ucfirst($this->input->post('nama_id')),
			   'title_en' => ucfirst($this->input->post('nama_en')),
			   'link' => $this->input->post('id_halaman')
			);
		}

        $this->db->where('link', $this->input->post('id_halaman'));
        
        if( ! $this->db->update($this->tbl_menu, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    function upMenuStat()
    {
		if($this->input->post('parentid') === '0')
		{
			$data = array(
				   'status' => $this->input->post('status'),
				   'title_id' => ucfirst($this->input->post('nama_id')),
				   'title_en' => ucfirst($this->input->post('nama_en')),
				   'parentid' => 0,
				   'level' => 0
				);
		}
		else {
			$level = $this->getLevelMenuByParent($this->input->post('parentid'));

			$data = array(
				   'status' => $this->input->post('status'),
				   'title_id' => ucfirst($this->input->post('nama_id')),
				   'title_en' => ucfirst($this->input->post('nama_en')),
				   'parentid' => $this->input->post('parentid'),
				   'level' => $level
				);
		}

		$this->db->where('id', $this->input->post('menu_id'));

		if( ! $this->db->update($this->tbl_menu, $data) )
		{
			return FALSE;
		}
		else {
			if ($this->input->post('halaman') > 0)
			{
				$label_id = underscore($this->input->post('nama_id'));
				$label_en = underscore($this->input->post('nama_en'));
				$data = array(
							'label_id' => $label_id,
							'label_en' => $label_en
						);
				$this->db->where('id_halaman', $this->input->post('halaman'));
				if( ! $this->db->update($this->tbl_halaman, $data) )
				return FALSE;
				else
				return TRUE;
			}
			else {
				return TRUE;
			}
		}
    }

    function adaMenuUpdate($str='',$nama_ori='',$bhs='')
    {
		$this->db->select('id');

		if($bhs === 'id')
		{
			$this->db->where('title_id', $str);
			$this->db->where_not_in('title_id', $nama_ori);
		}
		else {
			$this->db->where('title_en', $str);
			$this->db->where_not_in('title_en', $nama_ori);
		}
		
        $query = $this->db->get($this->tbl_menu);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function hapusAnakMenu($pid='')
    {
		$qp = $this->db->get_where($this->tbl_menu, array('parentid' => $pid));
		if ($qp->num_rows() > 0)
        {
			foreach($qp->result() as $row)
			{
				$id = $row->id;
				$pos = $row->posisi;
				// update posisi
				#$this->posHapusMenuUpdate($pos);

				$qcek = $this->db->get_where($this->tbl_menu, array('parentid' => $id));
				if ($qcek->num_rows() > 0)
				{
					// ulangi sampai habis
					$this->hapusAnakMenu($id);
				}
				$this->db->delete($this->tbl_menu, array('id' => $id));

				// hapus halaman
				if(is_numeric($row->link) && $row->link !== 0)
				{
					$this->db->delete($this->tbl_halaman, array('id_halaman' => $row->link));
				}
			}
			$this->posHapusMenuUpdateRapih();
        }
    }

    function hapusMenu($id='')
    {
		if(empty($id))
        $id = $this->input->post('id_hps');
        
        $qp = $this->db->get_where($this->tbl_menu, array('id' => $id));
        if ($qp->num_rows() > 0 && $id !== 1)
        {
            $row = $qp->row();
			$pos = $row->posisi;

			// hapus anak
			$this->hapusAnakMenu($id);

			// update posisi
            #$this->posHapusMenuUpdate($pos);

            // hapus id
            if( ! $this->db->delete($this->tbl_menu, array('id' => $id)) )
            {    
                return FALSE; 
            }
            else {
				$this->posHapusMenuUpdateRapih();
				// hapus halaman
				if(is_numeric($row->link) && ! is_null($row->link))
				{
					$this->db->delete($this->tbl_halaman, array('id_halaman' => $row->link));
				}
				return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
    /* end Menu */
    
    /* Halaman */
    function addHalaman()
    {
        $label_id = underscore($this->input->post('nama_id'));
        $label_en = underscore($this->input->post('nama_en'));
        $keyword_id = format_keyword($this->input->post('keyword_id'));
        $keyword_en = format_keyword($this->input->post('keyword_en'));
        $description_id = ucfirst(rip_tags($this->input->post('description_id')));
        $description_en = ucfirst(rip_tags($this->input->post('description_en')));
        $kategori = NULL;
        
        if($this->input->post('tipe') == 'module')
        {
            $data = array(
                    'label_id' => $label_id,
                    'label_en' => $label_en,
                    'seo_keyword_id' => $keyword_id,
                    'seo_keyword_en' => $keyword_en,
                    'seo_description_id' => $description_id,
                    'seo_description_en' =>  $description_en,
                    'isi_id' => NULL,
					'isi_en' => NULL,
                    'tipe' => $this->input->post('tipe'),
                    'id_module' => $this->input->post('module_id'),
                    'status' => 'on'
            );
        }
        elseif($this->input->post('tipe') == 'produk')
        {
            //reduce_multiples
            if($this->input->post('tipe_prod') === 'by_kategori')
            $kategori = format_simpan_kategori_halaman($this->input->post('kategori'));
            
            $data = array(
                    'label_id' => $label_id,
                    'label_en' => $label_en,
                    'seo_keyword_id' => $keyword_id,
                    'seo_keyword_en' => $keyword_en,
                    'seo_description_id' => $description_id,
                    'seo_description_en' =>  $description_en,
                    'tipe' => $this->input->post('tipe'),
                    'kategori' => $kategori,
                    'id_produk' => ($this->input->post('tipe_prod') === 'by_id')?$this->input->post('id_produk'):0,
                    'status' => 'on'
            );
        }
        elseif($this->input->post('tipe') == 'blog')
        {
			//reduce_multiples
            $kategori = format_simpan_kategori_halaman($this->input->post('kategori_blog'));

            $data = array(
                'label_id' => $label_id,
                'label_en' => $label_en,
                'seo_keyword_id' => $keyword_id,
				'seo_keyword_en' => $keyword_en,
				'seo_description_id' => $description_id,
                'seo_description_en' =>  $description_en,
                'isi_id' => NULL,
				'isi_en' => NULL,
                'tipe' => $this->input->post('tipe'),
                'kategori' => $kategori,
                'status' => 'on'
            );
        }
        elseif($this->input->post('tipe') == 'album')
        {
            //reduce_multiples
            if($this->input->post('tipe_album') === 'by_kategori')
            $kategori = format_simpan_kategori_halaman($this->input->post('kategori_album'));

            $data = array(
                    'label_id' => $label_id,
                    'label_en' => $label_en,
                    'seo_keyword_id' => $keyword_id,
                    'seo_keyword_en' => $keyword_en,
                    'seo_description_id' => $description_id,
                    'seo_description_en' =>  $description_en,
                    'isi_id' => $this->input->post('isi_id'),
					'isi_en' => $this->input->post('isi_en'),
                    'tipe' => $this->input->post('tipe'),
                    'kategori' => $kategori,
                    'id_album' => ($this->input->post('tipe_album') === 'by_id')?$this->input->post('id_album'):0,
                    'status' => 'on'
            );
        }
        elseif($this->input->post('tipe') == 'video')
        {
            //reduce_multiples
            if($this->input->post('tipe_video') === 'by_kategori')
            $kategori = format_simpan_kategori_halaman($this->input->post('kategori_video'));
            
            $data = array(
                    'label_id' => $label_id,
                    'label_en' => $label_en,
                    'seo_keyword_id' => $keyword_id,
                    'seo_keyword_en' => $keyword_en,
                    'seo_description_id' => $description_id,
                    'seo_description_en' =>  $description_en,
                    'isi_id' => $this->input->post('isi_id'),
					'isi_en' => $this->input->post('isi_en'),
                    'tipe' => $this->input->post('tipe'),
                    'kategori' => $kategori,
                    'id_video' => ($this->input->post('tipe_video') === 'by_id')?$this->input->post('id_video'):0,
                    'status' => 'on'
            );
        }
        else {
            $data = array(
                'label_id' => $label_id,
                'label_en' => $label_en,
                'seo_keyword_id' => $keyword_id,
				'seo_keyword_en' => $keyword_en,
				'seo_description_id' => $description_id,
                'seo_description_en' =>  $description_en,
                'isi_id' => html_entity_decode($this->input->post('isi_id')),
				'isi_en' => html_entity_decode($this->input->post('isi_en')),
                'tipe' => $this->input->post('tipe'),
                'status' => 'on'
            );
        }

        if ($this->input->post('tipe') !== 'top')
        {
			if ( ! $this->db->insert($this->tbl_halaman,$data))
			{
				return FALSE;
			}
			else {
				if( ! $this->addMenu($this->db->insert_id()))
				return FALSE;
				else
				return TRUE;
			}
        }
        else {
			if( ! $this->addMenu(0))
		    return FALSE;
		    else
		    return TRUE;
        }
    }

    function adaHalaman($str='',$bhs='id')
    {
        $cari = in_array($str, $this->config->item('reserved_halaman'));

        if($bhs === 'id')
        $this->db->where('label_id', $str);
        else
        $this->db->where('label_en',$str);

        $query = $this->db->get($this->tbl_halaman);
        if ($query->num_rows() > 0 || $cari === TRUE)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function getCountHalaman()
    {
        $query = $this->db->get($this->tbl_halaman);
        return $query->num_rows();
    }

    function getAllHalaman($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("tipe", "asc");
        $query = $this->db->get($this->tbl_halaman);
        return $query->result();
    }

    function getTipeHalamanById($id)
    {
        $this->db->select('tipe');
        $query = $this->db->get_where($this->tbl_halaman,array('id_halaman' => $id));
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            return $row->tipe;
        }
        else {
            return NULL;
        }
    }

    function getSelectHalaman()
    {
        $this->db->select('id_halaman, label_id, tipe');
        $this->db->where('status','on');
        $this->db->order_by("tipe", "asc");
        $query = $this->db->get($this->tbl_halaman);
        return $query->result();
    }

    function adaUpdateHalaman($str='',$nama_ori='',$bhs='')
    {
        $cari = in_array($str, $this->config->item('reserved_halaman'));
        $this->db->select('id_halaman');

        if($bhs === 'id')
		{
			$this->db->where('label_id', $str);
			$this->db->where_not_in('label_id', $nama_ori);
		}
		else {
			$this->db->where('label_en', $str);
			$this->db->where_not_in('label_en', $nama_ori);
		}
        
        $query = $this->db->get($this->tbl_halaman);
        if ($query->num_rows() > 0 || $cari === TRUE)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function getAllHalamanAdminById($id)
    {
        $query = $this->db->get_where($this->tbl_halaman,array('id_halaman' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getLabelHalamanById($id)
    {
		if(current_lang(false) === 'id')
        $this->db->select('label_id');
        else
        $this->db->select('label_en');
        $query = $this->db->get_where($this->tbl_halaman,array('id_halaman' => $id));
        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            if(current_lang(false) === 'id')
            return $row->label_id;
            else
            return $row->label_en;
        }
        else {
            return NULL;
        }
    }

    function upHalamanStat()
    {
        $data = array(
               'status' => $this->input->post('status')
            );
        $this->db->where('id_halaman', $this->input->post('halaman_id'));
        if ( ! $this->db->update($this->tbl_halaman, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function upHalamanForm()
    {
		$label_id = underscore($this->input->post('nama_id'));
        $label_en = underscore($this->input->post('nama_en'));
        $keyword_id = format_keyword($this->input->post('keyword_id'));
        $keyword_en = format_keyword($this->input->post('keyword_en'));
        $description_id = ucfirst(rip_tags($this->input->post('description_id')));
        $description_en = ucfirst(rip_tags($this->input->post('description_en')));
        $kategori = NULL;

        if ( $this->input->post('tipe') == 'top' )
        {
			$this->db->where('id_halaman', $this->input->post('id_halaman'));
			if( ! $this->db->delete($this->tbl_halaman))
			{
			 return FALSE;
			}
			else {
			 return TRUE;
			}
        }
        else {
			if($this->input->post('tipe') == 'module')
			{
				$data = array(
						'label_id' => $label_id,
						'label_en' => $label_en,
						'isi_id' => NULL,
						'isi_en' => NULL,
						'seo_keyword_id' => $keyword_id,
						'seo_keyword_en' => $keyword_en,
						'seo_description_id' => $description_id,
						'seo_description_en' => $description_en,
						'tipe' => $this->input->post('tipe'),
						'kategori' => NULL,
						'id_module' => $this->input->post('module_id'),
						'id_produk' => 0,
						'id_album' => 0,
						'id_video' => 0,
						'status' => 'on'
					);
			}
			elseif($this->input->post('tipe') == 'produk')
			{
				if($this->input->post('tipe_prod') === 'by_kategori')
				$kategori = format_simpan_kategori_halaman($this->input->post('kategori'));
				
				$data = array(
						'label_id' => $label_id,
						'label_en' => $label_en,
						'isi_id' => NULL,
						'isi_en' => NULL,
						'seo_keyword_id' => $keyword_id,
						'seo_keyword_en' => $keyword_en,
						'seo_description_id' => $description_id,
						'seo_description_en' => $description_en,
						'tipe' => $this->input->post('tipe'),
						'kategori' => $kategori,
						'id_module' => 0,
						'id_produk' => ($this->input->post('tipe_prod') === 'by_id')?$this->input->post('id_produk'):0,
						'id_album' => 0,
						'id_video' => 0,
						'status' => 'on'
					);
			}
			elseif($this->input->post('tipe') == 'blog')
			{
				$kategori = format_simpan_kategori_halaman($this->input->post('kategori_blog'));
				$data = array(
						'label_id' => $label_id,
						'label_en' => $label_en,
						'isi_id' => NULL,
						'isi_en' => NULL,
						'seo_keyword_id' => $keyword_id,
						'seo_keyword_en' => $keyword_en,
						'seo_description_id' => $description_id,
						'seo_description_en' => $description_en,
						'tipe' => $this->input->post('tipe'),
						'kategori' => $kategori,
						'id_module' => 0,
						'id_produk' => 0,
						'id_album' => 0,
						'id_video' => 0,
						'status' => 'on'
				);
			}
			elseif($this->input->post('tipe') == 'album')
			{
				if($this->input->post('tipe_album') === 'by_kategori')
				$kategori = format_simpan_kategori_halaman($this->input->post('kategori_album'));

				$data = array(
						'label_id' => $label_id,
						'label_en' => $label_en,
						'isi_id' => $this->input->post('isi_id'),
						'isi_en' => $this->input->post('isi_en'),
						'seo_keyword_id' => $keyword_id,
						'seo_keyword_en' => $keyword_en,
						'seo_description_id' => $description_id,
						'seo_description_en' => $description_en,
						'tipe' => $this->input->post('tipe'),
						'kategori' => $kategori,
						'id_module' => 0,
						'id_produk' => 0,
						'id_album' => ($this->input->post('tipe_album') === 'by_id')?$this->input->post('id_album'):0,
						'id_video' => 0,
						'status' => 'on'
				);
			}
			elseif($this->input->post('tipe') == 'video')
			{
				if($this->input->post('tipe_video'))
				$kategori = format_simpan_kategori_halaman($this->input->post('kategori_video'));

				$data = array(
						'label_id' => $label_id,
						'label_en' => $label_en,
						'isi_id' => $this->input->post('isi_id'),
						'isi_en' => $this->input->post('isi_en'),
						'seo_keyword_id' => $keyword_id,
						'seo_keyword_en' => $keyword_en,
						'seo_description_id' => $description_id,
						'seo_description_en' => $description_en,
						'tipe' => $this->input->post('tipe'),
						'kategori' => $kategori,
						'id_module' => 0,
						'id_produk' => 0,
						'id_album' => 0,
						'id_video' => ($this->input->post('tipe_video') === 'by_id')?$this->input->post('id_video'):0,
						'status' => 'on'
				);
			}
			else {
				$data = array(
						'label_id' => $label_id,
						'label_en' => $label_en,
						'isi_id' => $this->input->post('isi_id'),
						'isi_en' => $this->input->post('isi_en'),
						'seo_keyword_id' => $keyword_id,
						'seo_keyword_en' => $keyword_en,
						'seo_description_id' => $description_id,
						'seo_description_en' => $description_en,
						'tipe' => 'halaman',
						'kategori' => NULL,
						'id_module' => 0,
						'id_produk' => 0,
						'id_album' => 0,
						'id_video' => 0,
						'status' => 'on'
					);
			}

			$this->db->where('id_halaman', $this->input->post('id_halaman'));
			if( ! $this->db->update($this->tbl_halaman, $data))
			{
			 return FALSE;
			}
			else {
			 return TRUE;
			}
        }
    }
    /* End Halaman */
    
    /* Album */
    function upPosAlbumBaru($id)
    {
		$this->db->select('id');
		$this->db->where('id !=', $id);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_album);
        $pos = 2;
        if ($query->num_rows() > 0)
        {
			foreach($query->result() as $row)
			{
				$this->db->update($this->tbl_album, array('posisi' => $pos), array('id' => $row->id));
				$pos++;
			}
        }
    }

    function addAlbum()
    {
        $this->load->helper('inflector');
        
        $nama_img = $this->upload->file_name;

        $data = array(
			'tgl_update' => time(),
            'kategori_id' => $this->input->post('kategori_album'),
            'nama_id' => $this->input->post('nama_id'),
            'nama_en' => $this->input->post('nama_en'),
            'alt_text_id' => ucwords($this->input->post('nama_id')),
            'alt_text_en' => ucwords($this->input->post('nama_en')),
            'ket_id' => $this->input->post('ket_id'),
            'ket_en' => $this->input->post('ket_en'),
            'gambar_cover' => $nama_img,
            'posisi' => 1,
            'status' => 'on',
            'utama' => $this->input->post('utama'),
            'diskusi' => $this->input->post('diskusi'),
            'ratting' => $this->input->post('ratting'),
            'votes' => mt_rand(5, 15)
        );

        $str_id = $this->input->post('nama_id');
        $str_en = $this->input->post('nama_en');
        $this->db->where(array('nama_id'=>$str_id));
        $this->db->or_where(array('nama_en'=>$str_en));
        $query = $this->db->get($this->tbl_album);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           if ( ! $this->db->insert($this->tbl_album,$data))
           {
				return FALSE;
           }
           else {
				// up pos baru
				$this->upPosAlbumBaru($this->db->insert_id());
				// tambah tag
				if( $this->input->post('tag') )
				{
					$ftag = format_array_idtag($this->input->post('tag'));
					$this->cms->addTagKonten($this->db->insert_id(), $ftag, 'album');
				}
				return TRUE;
           }
        }
    }

    function adaAlbum($str)
    {
        $this->db->where(array('nama_id'=>$str));
        $this->db->or_where(array('nama_en'=>$str));
        $query = $this->db->get($this->tbl_album);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function adaUpdateAlbum($str='',$nama_ori='',$bhs='')
    {
        $this->db->select('id');
        if($bhs === 'id')
		{
			$this->db->where('nama_id', $str);
			$this->db->where_not_in('nama_id', $nama_ori);
		}
		else {
			$this->db->where('nama_en', $str);
			$this->db->where_not_in('nama_en', $nama_ori);
		}
        
        $query = $this->db->get($this->tbl_album);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function getCountAlbum()
    {
        $query = $this->db->get($this->tbl_album);
        return $query->num_rows(); 
    }

    function getAllAlbum($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_album);
        return $query->result();
    }

    function getAllAlbumAdminById($id)
    {
        $query = $this->db->get_where($this->tbl_album,array('id' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getAlbumSelect($cek=FALSE)
    {
		if($cek === TRUE)
		{
			$this->db->select('id_album');
			$this->db->distinct('id_album');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get($this->tbl_gallery);
			if($query->num_rows > 0)
			{
				$album = $query->result_array();
			}
			else {
				$album = array();
			}

			foreach($album as $ada)
			{
				$id_album[] = $ada['id_album'];
			}

			if (isset($id_album) && ! empty($id_album))
			{
				$this->db->select('id,nama_id,nama_en');
				$this->db->where('status','on');
				$this->db->where_not_in('id',$id_album);
				$this->db->order_by("posisi", "asc");
				$query = $this->db->get($this->tbl_album);
				if($query->num_rows > 0)
				{
					return $query->result_array();
				}
				else {
					return NULL;
				}
			}
			else {
				return NULL;
			}
		}
		else {
			$this->db->select('id,nama_id,nama_en');
			$this->db->where('status','on');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get($this->tbl_album);
			if($query->num_rows > 0)
			{
				return $query->result_array();
			}
			else {
				return NULL;
			}
		}
    }

    function upAlbumStat()
    {
        $data = array(
               'status' => $this->input->post('status'),
               'utama' => $this->input->post('utama'),
               'diskusi' => $this->input->post('komentar')
            );
        $this->db->where('id', $this->input->post('album_id'));
        if( ! $this->db->update($this->tbl_album, $data) )
        {
            return FALSE;
        }
        else {
            $data = array(
               'status' => $this->input->post('status')
            );
            $this->db->where('id_album', $this->input->post('album_id'));
	        if( ! $this->db->update($this->tbl_gallery, $data) )
	        {
	            return FALSE;
	        }
	        else {
	            return TRUE;
	        }
        }
    }

    function upAlbumPos()
    {
        if($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('albid') )
        {   
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_album, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id', $this->input->post('albid'));
                $this->db->update($this->tbl_album, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    function upAlbumForm()
    {
        if($this->input->post('status_ganti') == 'yes')
        {
            $nama_img = $this->upload->file_name;

            $data = array(
				'tgl_update' => time(),
                'kategori_id' => $this->input->post('kategori_album'),
                'ratting' => $this->input->post('ratting'),
                'nama_id' => $this->input->post('nama_id'),
                'nama_en' => $this->input->post('nama_en'),
                'alt_text_id' => ucwords($this->input->post('nama_id')),
                'alt_text_en' => ucwords($this->input->post('nama_en')),
                'ket_id' => $this->input->post('ket_id'),
                'ket_en' => $this->input->post('ket_en'),
                'gambar_cover' => $nama_img
            );
        }
        else {
            $data = array(
				'tgl_update' => time(),
                'kategori_id' => $this->input->post('kategori_album'),
                'ratting' => $this->input->post('ratting'),
                'nama_id' => $this->input->post('nama_id'),
                'nama_en' => $this->input->post('nama_en'),
                'alt_text_id' => ucwords($this->input->post('nama_id')),
                'alt_text_en' => ucwords($this->input->post('nama_en')),
                'ket_id' => $this->input->post('ket_id'),
                'ket_en' => $this->input->post('ket_en')
            );
        }

        $this->db->where('id', $this->input->post('id_album'));
        if( ! $this->db->update($this->tbl_album, $data))
        {
			return FALSE;
        }
		else {
			//if( $this->input->post('tag') == '' || ($this->input->post('tag') !== $this->input->post('tag_ori')) )
			if( $this->input->post('tag') !== $this->input->post('tag_ori') )
			{
				$ftag = format_array_idtag($this->input->post('tag'));
				$this->cms->addTagKonten($this->input->post('id_album'), $ftag, 'album',TRUE);
				return TRUE;
			}
			else {
				return TRUE;
			}
		}
    }

    function hapusAlbum()
    {
        $id = $this->input->post('id_hps');
        $qp = $this->db->get_where($this->tbl_album, array('id' => $id), 1);
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();
            $pos = $rp->posisi;
            
            $this->db->order_by("posisi", "asc");
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_album);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idalb = $row->id;
                $data = array('posisi' => $pos);
                $this->db->update($this->tbl_album, $data, array('id' => $idalb));
                
                $pos++;
            }
            
            //hapus gambar
			hapus_file("./_media/album/large/large_". $rp->gambar_cover);
			hapus_file( "./_media/album/medium/medium_". $rp->gambar_cover);
			hapus_file("./_media/album/small/small_". $rp->gambar_cover);
			hapus_file("./_media/album/thumb/thumb_". $rp->gambar_cover);

			//hapus gallery
			$galery = $this->db->get_where($this->tbl_gallery,array('id_album'=>$id));
			if($galery->num_rows() > 0)
			{
				foreach($galery->result() as $anak)
				{
					$id_gal= $anak->id;
					$this->hapusAlbumGallery($id_gal);
				}
			}

			//hapus menu - halaman
			$this->db->select('id_halaman');
			$halaman = $this->db->get_where($this->tbl_halaman,array('id_album'=>$id));
			if($halaman->num_rows() > 0)
			{
				foreach( $halaman->result() as $hal)
				{
					// hapus menu
					$menu = $this->db->get_where($this->tbl_menu,array('link'=>$hal->id_halaman));
					if($menu->num_rows() > 0)
					{
						foreach( $menu->result() as $mnu)
						{
							$this->hapusMenu($mnu->id);
						}
					}
				}
			}
            
            // hapus id
            if ( ! $this->db->delete($this->tbl_album, array('id' => $id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
    /* End Album */

    /* Gallery */
    function getCountGallery()
    {
         $query = $this->db->get($this->tbl_gallery);
         return $query->num_rows(); 
    }

    function getNamaGalleryAlbumById($id='')
    {
		$this->db->select('nama_id,nama_en');
		$this->db->where('id', $id, 1);
        $query = $this->db->get($this->tbl_album);
        $row = $query->row();
        return $row;
    }

    function getCountGalleryAlbum($id='')
    {
		$this->db->where('id_album', $id);
		$query = $this->db->get($this->tbl_gallery);
		return $query->num_rows();
    }

    function getAllGalleryAlbum($id='',$limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $this->db->where('id_album', $id);
        $query = $this->db->get($this->tbl_gallery);
        return $query->result();
    }

    function getAlbumGalleryAdminById($id)
    {
        $query = $this->db->get_where($this->tbl_gallery,array('id' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

	function adaGallery($str)
    {
        $this->db->where(array('nama_id'=>$str));
        $this->db->or_where(array('nama_en'=>$str));
        $query = $this->db->get($this->tbl_gallery);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function adaUpdateGallery($str='',$nama_ori='',$bhs='')
    {
        $this->db->select('id');
        if($bhs === 'id')
		{
			$this->db->where('nama_id', $str);
			$this->db->where_not_in('nama_id', $nama_ori);
		}
		else {
			$this->db->where('nama_en', $str);
			$this->db->where_not_in('nama_en', $nama_ori);
		}
        
        $query = $this->db->get($this->tbl_gallery);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function upPosGalleryBaru($pid,$id)
    {
		$this->db->select('id');
		$this->db->where('id_album', $pid);
		$this->db->where('id !=', $id);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_gallery);
        $pos = 2;
        if ($query->num_rows() > 0)
        {
			foreach($query->result() as $row)
			{
				$this->db->update($this->tbl_gallery, array('posisi' => $pos), array('id' => $row->id));
				$pos++;
			}
        }
    }
    
	function addGallery()
    {
        $nama_img = $this->upload->file_name;

        $data = array(
            'id_album' => $this->input->post('parent_album'),
            'nama_id' => $this->input->post('nama_id'),
            'nama_en' => $this->input->post('nama_en'),
            'alt_text_id' => ucwords($this->input->post('nama_id')),
            'alt_text_en' => ucwords($this->input->post('nama_en')),
            'ket_id' => $this->input->post('ket_id'),
            'ket_en' => $this->input->post('ket_en'),
            'gambar_gallery' => $nama_img,
            'posisi' => 1,
            'status' => 'on'
        );

		$str_id = $this->input->post('nama_id');
		$str_en = $this->input->post('nama_en');
		$this->db->where(array('nama_id'=>$str_id));
		$this->db->or_where(array('nama_en'=>$str_en));
		$query = $this->db->get($this->tbl_gallery);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           if ( ! $this->db->insert($this->tbl_gallery,$data))
           {
				return FALSE;
           }
           else {
				// up pos baru
				$this->upPosGalleryBaru($this->input->post('parent_album'),$this->db->insert_id());
				return TRUE;
           }
        }
    }

    function upAlbumGalleryForm()
    {
        if($this->input->post('status_ganti') == 'yes')
        {
            $nama_img = $this->upload->file_name;

            $data = array(
                'nama_id' => $this->input->post('nama_id'),
                'nama_en' => $this->input->post('nama_en'),
                'alt_text_id' => ucwords($this->input->post('nama_id')),
                'alt_text_en' => ucwords($this->input->post('nama_en')),
                'ket_id' => $this->input->post('ket_id'),
                'ket_en' => $this->input->post('ket_en'),
                'gambar_gallery' => $nama_img,
            );
        }
        else {
            $data = array(
				'nama_id' => $this->input->post('nama_id'),
                'nama_en' => $this->input->post('nama_en'),
                'alt_text_id' => ucwords($this->input->post('nama_id')),
                'alt_text_en' => ucwords($this->input->post('nama_en')),
                'ket_id' => $this->input->post('ket_id'),
                'ket_en' => $this->input->post('ket_en')
            );
        }

        $this->db->where('id', $this->input->post('id_album'));
        if( ! $this->db->update($this->tbl_gallery, $data))
        {
         return FALSE;
        }
        else {
         return TRUE;
        }
    }

    function upAlbumGalleryStat()
    {
        $data = array(
               'status' => $this->input->post('status')
            );
        $this->db->where('id', $this->input->post('album_id'));
        if( ! $this->db->update($this->tbl_gallery, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function upAlbumGalleryPos()
    {
        if($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('albid') )
        {   
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('id_album', $this->input->post('parent_album'));
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_gallery, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id_album', $this->input->post('parent_album'));
                $this->db->where('id', $this->input->post('albid'));
                $this->db->update($this->tbl_gallery, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function hapusAlbumGallery($id='')
    {
		if(empty($id))
        $id = $this->input->post('id_hps');
        
        $qp = $this->db->get_where($this->tbl_gallery, array('id' => $id),1);
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();
            $pos = $rp->posisi;
            $albid = $rp->id_album;
            
            $this->db->order_by("posisi", "asc");
            $this->db->where('id_album',$albid);
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_gallery);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idmn = $row->id;
                $idp = $row->id_album;
                $data = array('posisi' => $pos);
                
                $this->db->where('id_album', $idp);
                $this->db->where('id', $idmn);
                $this->db->update($this->tbl_gallery, $data);
                $pos++;
            }
            
            //hapus gambar
			hapus_file("./_media/album-gallery/large/large_". $rp->gambar_gallery);
			hapus_file( "./_media/album-gallery/medium/medium_". $rp->gambar_gallery);
			hapus_file("./_media/album-gallery/small/small_". $rp->gambar_gallery);
			hapus_file("./_media/album-gallery/thumb/thumb_". $rp->gambar_gallery);
            
            // hapus id
            if ( ! $this->db->delete($this->tbl_gallery, array('id' => $id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
    /* End Gallery */

	/* Video */
    function upPosVideoBaru($id)
    {
		$this->db->select('id');
		$this->db->where('id !=', $id);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_video);
        $pos = 2;
        if ($query->num_rows() > 0)
        {
			foreach($query->result() as $row)
			{
				$this->db->update($this->tbl_video, array('posisi' => $pos), array('id' => $row->id));
				$pos++;
			}
        }
    }
    
    function addVideo($nama_file,$tipe_video)
    {
        $data = array(
			'tgl_update' => time(),
            'kategori_id' => $this->input->post('kategori_video'),
            'nama_id' => ucfirst($this->input->post('nama_id')),
            'nama_en' => ucfirst($this->input->post('nama_en')),
            'tipe_video' => $tipe_video,
            'src_video' => $this->input->post('src_video'),
            'gambar_video' => $nama_file,
            'ket_id' => $this->input->post('ket_id'),
            'ket_en' => $this->input->post('ket_en'),
            'posisi' => 1,
            'status' => 'on',
            'utama' => $this->input->post('utama'),
            'diskusi' => $this->input->post('diskusi'),
            'ratting' => $this->input->post('ratting'),
            'votes' => mt_rand(5, 15)
        );

        $str_id = $this->input->post('nama_id');
        $str_en = $this->input->post('nama_en');
        $this->db->where(array('nama_id'=>$str_id));
        $this->db->or_where(array('nama_en'=>$str_en));
        $query = $this->db->get($this->tbl_video);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           if ( ! $this->db->insert($this->tbl_video,$data))
           {
				return FALSE;
           }
           else {
				// up pos baru
				$this->upPosVideoBaru($this->db->insert_id());
				// tambah tag
				if( $this->input->post('tag'))
				{
					$ftag = format_array_idtag($this->input->post('tag'));
					$this->cms->addTagKonten($this->db->insert_id(), $ftag, 'video');
				}
				return TRUE;
           }
        }
    }
    
    function adaVideo($str='', $bhs='id')
    {
		if($bhs==='id')
        $this->db->where(array('nama_id'=>$str));
        else
        $this->db->where(array('nama_en'=>$str));

        $query = $this->db->get($this->tbl_video);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function adaUpdateVideo($str='',$nama_ori='',$bhs='id')
    {
        $this->db->select('id');
        if($bhs === 'id')
		{
			$this->db->where('nama_id', $str);
			$this->db->where_not_in('nama_id', $nama_ori);
		}
		else {
			$this->db->where('nama_en', $str);
			$this->db->where_not_in('nama_en', $nama_ori);
		}
        
        $query = $this->db->get($this->tbl_video);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function getAllVideo($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_video);
        return $query->result();
    }
    
    function getAllVideoAdminById($id)
    {
        $query = $this->db->get_where($this->tbl_video,array('id' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getVideoSelect($cek=FALSE)
    {
		if($cek === TRUE)
		{
			$this->db->select('id_video');
			$this->db->distinct('id_video');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get($this->tbl_playlist);
			if($query->num_rows > 0)
			{
				$video = $query->result_array();
			}
			else {
				$video = array();
			}

			foreach($video as $ada)
			{
				$id_video[] = $ada['id_video'];
			}

			//return $id_video;
			if (isset($id_video) && ! empty($id_video))
			{
				$this->db->select('id,nama_id,nama_en');
				$this->db->where('status','on');
				$this->db->where_not_in('id',$id_video);
				$this->db->order_by("posisi", "asc");
				$query = $this->db->get($this->tbl_video);
				if($query->num_rows > 0)
				{
					return $query->result_array();
				}
				else {
					return NULL;
				}
			}
			else {
				return NULL;
			}
		}
		else {
			$this->db->select('id,nama_id,nama_en');
			$this->db->where('status','on');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get($this->tbl_video);
			if($query->num_rows > 0)
			{
				return $query->result_array();
			}
			else {
				return NULL;
			}
		}
    }

    function upVideoStat()
    {
        $data = array(
               'utama' => $this->input->post('utama'),
               'status' => $this->input->post('status'),
               'diskusi' => $this->input->post('komentar')
            );
        $this->db->where('id', $this->input->post('video_id'));
        if( ! $this->db->update($this->tbl_video, $data) )
        {
            return FALSE;
        }
        else {
            //return TRUE;
            $data = array(
               'status' => $this->input->post('status')
            );
            $this->db->where('id_video', $this->input->post('video_id'));
	        if( ! $this->db->update($this->tbl_playlist, $data) )
	        {
	            return FALSE;
	        }
	        else {
	            return TRUE;
	        }
        }
    }

    function upVideoPos()
    {
        if($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('vdoid') )
        {   
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_video, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id', $this->input->post('vdoid'));
                $this->db->update($this->tbl_video, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function upVideoForm($ubah=FALSE,$nama_file='',$tipe_video='')
    {
		if ($ubah === TRUE)
		{
			$data = array(
				'tgl_update' => time(),
	            'kategori_id' => $this->input->post('kategori_video'),
	            'ratting' => $this->input->post('ratting'),
	            'nama_id' => ucfirst($this->input->post('nama_id')),
	            'nama_en' => ucfirst($this->input->post('nama_en')),
	            'tipe_video' => $tipe_video,
	            'src_video' => $this->input->post('src_video'),
	            'gambar_video' => $nama_file,
	            'ket_id' => $this->input->post('ket_id'),
	            'ket_en' => $this->input->post('ket_en')
	        );
		}
		else {
			$data = array(
				'tgl_update' => time(),
				'kategori_id' => $this->input->post('kategori_video'),
				'ratting' => $this->input->post('ratting'),
				'nama_id' => $this->input->post('nama_id'),
				'nama_en' => $this->input->post('nama_en'),
				'ket_id' => $this->input->post('ket_id'),
				'ket_en' => $this->input->post('ket_en')
			);
		}

        $this->db->where('id', $this->input->post('id_video'));
        if( ! $this->db->update($this->tbl_video, $data))
        {
			return FALSE;
        }
        else {
			if( ! empty($nama_file))
			{
				//hapus thumbs lama
                hapus_file("./_media/videos/medium/". $this->input->post('gambar'));
                hapus_file("./_media/videos/small/small_". $this->input->post('gambar'));
                hapus_file("./_media/videos/thumb/thumb_". $this->input->post('gambar'));
			}

			if( $this->input->post('tag') !== $this->input->post('tag_ori') )
			{
				$ftag = format_array_idtag($this->input->post('tag'));
				$this->cms->addTagKonten($this->input->post('id_video'), $ftag, 'video',TRUE);
				return TRUE;
			}
			else {
				return TRUE;
			}
        }
    }

    function getCountVideo()
    {
         $query = $this->db->get($this->tbl_video);
         return $query->num_rows(); 
    }

    function hapusVideo()
    {
        $id = $this->input->post('id_hps');
        
        $qp = $this->db->get_where($this->tbl_video, array('id' => $id));
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();
            $pos = $rp->posisi;
            
            $this->db->order_by("posisi", "asc"); 
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_video);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idmn = $row->id;
                $data = array('posisi' => $pos);
                $this->db->update($this->tbl_video, $data, array('id' => $idmn));
                
                $pos++;
            }

            //hapus thumbs
			hapus_file("./_media/videos/medium/". $rp->gambar_video);
			hapus_file("./_media/videos/small/small_". $rp->gambar_video);
			hapus_file("./_media/videos/thumb/thumb_". $rp->gambar_video);

            //hapus playlist
            $playlist = $this->db->get_where($this->tbl_playlist,array('id_video' => $id));
            if( $playlist->num_rows > 0)
            {
				foreach($playlist->result() as $row)
				{
					$id_playlist = $row->id;
					$this->hapusVideoPlaylist($id_playlist);
				}
            }

            //hapus menu - halaman
            $this->db->select('id_halaman');
			$halaman = $this->db->get_where($this->tbl_halaman,array('id_video'=>$id));
			if($halaman->num_rows() > 0)
			{
				foreach( $halaman->result() as $hal)
				{
					// hapus menu
					$menu = $this->db->get_where($this->tbl_menu,array('link'=>$hal->id_halaman));
					if($menu->num_rows() > 0)
					{
						foreach( $menu->result() as $mnu)
						{
							$this->hapusMenu($mnu->id);
						}
					}
				}
			}

            // hapus id
            if ( ! $this->db->delete($this->tbl_video, array('id' => $id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
	/* End Video */

    /* Playlist */
    function getCountPlaylist()
    {
         $query = $this->db->get($this->tbl_playlist);
         return $query->num_rows(); 
    }
    
    function getCountVideoPlaylist($id='')
    {
		 $this->db->where('id_video', $id);
         $query = $this->db->get($this->tbl_playlist);
         return $query->num_rows(); 
    }

    function getNamaVideoPlaylistById($id='')
    {
		$this->db->select('nama_id,nama_en');
		$this->db->where('id', $id, 1);
        $query = $this->db->get($this->tbl_video);
        $row = $query->row();
        return $row;
    }

    function getAllVideoPlaylist($id='',$limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $this->db->where('id_video', $id);
        $query = $this->db->get($this->tbl_playlist);
        return $query->result();
    }

    function getVideoPlaylistAdminById($id)
    {
        $query = $this->db->get_where($this->tbl_playlist,array('id' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

	function adaPlaylist($str='', $bhs='id')
    {
		if($bhs==='id')
        $this->db->where(array('nama_id'=>$str));
        else
        $this->db->where(array('nama_en'=>$str));
        
        $query = $this->db->get($this->tbl_playlist);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function adaUpdatePlaylist($str='',$nama_ori='',$bhs='id')
    {
        $this->db->select('id');
        if($bhs === 'id')
		{
			$this->db->where('nama_id', $str);
			$this->db->where_not_in('nama_id', $nama_ori);
		}
		else {
			$this->db->where('nama_en', $str);
			$this->db->where_not_in('nama_en', $nama_ori);
		}
        
        $query = $this->db->get($this->tbl_playlist);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function upPosPlaylistBaru($pid,$id)
    {
		$this->db->select('id');
		$this->db->where('id_video', $pid);
		$this->db->where('id !=', $id);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_playlist);
        $pos = 2;
        if ($query->num_rows() > 0)
        {
			foreach($query->result() as $row)
			{
				$this->db->update($this->tbl_playlist, array('posisi' => $pos), array('id' => $row->id));
				$pos++;
			}
        }
    }
    
	function addPlaylist($nama_file,$tipe_video)
    {
        $data = array(
            'id_video' => $this->input->post('parent_video'),
            'nama_id' => ucfirst($this->input->post('nama_id')),
            'nama_en' => ucfirst($this->input->post('nama_en')),
            'tipe_video' => $tipe_video,
            'src_video' => $this->input->post('src_video'),
            'gambar_video' => $nama_file,
            'ket_id' => $this->input->post('ket_id'),
            'ket_en' => $this->input->post('ket_en'),
            'posisi' => 1,
            'status' => 'on'
        );

        $str_id = $this->input->post('nama_id');
        $str_en = $this->input->post('nama_en');
        $this->db->where(array('nama_id'=>$str_id));
        $this->db->or_where(array('nama_en'=>$str_en));
        $query = $this->db->get($this->tbl_playlist);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           if ( ! $this->db->insert($this->tbl_playlist,$data))
           {
				return FALSE;
           }
           else {
				// up pos baru
				$this->upPosPlaylistBaru($this->input->post('parent_video'),$this->db->insert_id());
				return TRUE;
           }
        }
    }

	function upVideoPlaylistForm($ubah=FALSE,$nama_file='',$tipe_video='')
    {
		if ($ubah === TRUE)
		{
			$data = array(
	            'nama_id' => ucfirst($this->input->post('nama_id')),
	            'nama_en' => ucfirst($this->input->post('nama_en')),
	            'tipe_video' => $tipe_video,
	            'src_video' => $this->input->post('src_video'),
	            'gambar_video' => $nama_file,
	            'ket_id' => $this->input->post('ket_id'),
	            'ket_en' => $this->input->post('ket_en')
	        );
		}
		else {
			$data = array(
				'nama_id' => $this->input->post('nama_id'),
				'nama_en' => $this->input->post('nama_en'),
				'ket_id' => $this->input->post('ket_id'),
				'ket_en' => $this->input->post('ket_en')
			);
		}

        $this->db->where('id', $this->input->post('id_playlist'));
        if( ! $this->db->update($this->tbl_playlist, $data))
        {
			return FALSE;
        }
        else {
			if( ! empty($nama_file))
			{
				//hapus thumbs lama
                hapus_file("./_media/videos/medium/". $this->input->post('gambar'));
                hapus_file("./_media/videos/small/small_". $this->input->post('gambar'));
                hapus_file("./_media/videos/thumb/thumb_". $this->input->post('gambar'));
			}
			return TRUE;
        }
    }
    
    function upVideoPlaylistStat()
    {
        $data = array(
               'status' => $this->input->post('status')
            );
        $this->db->where('id', $this->input->post('video_id'));
        if( ! $this->db->update($this->tbl_playlist, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function upVideoPlaylistPos()
    {
        if($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('vdoid') )
        {   
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('id_video', $this->input->post('parent_video'));
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_playlist, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id_video', $this->input->post('parent_video'));
                $this->db->where('id', $this->input->post('vdoid'));
                $this->db->update($this->tbl_playlist, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function hapusVideoPlaylist($id='')
    {
		if(empty($id))
        $id = $this->input->post('id_hps');
        
        $qp = $this->db->get_where($this->tbl_playlist, array('id' => $id),1);
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();
            $pos = $rp->posisi;
            $vdoid = $rp->video_id;
            
            $this->db->order_by("posisi", "asc");
            $this->db->where('id_video',$vdoid);
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_playlist);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idmn = $row->id;
                $idp = $row->video_id;
                $data = array('posisi' => $pos);
                
                $this->db->where('id_video', $idp);
                $this->db->where('id', $idmn);
                $this->db->update($this->tbl_playlist, $data);
                $pos++;
            }

            //hapus thumbs
			hapus_file("./_media/videos/medium/". $rp->gambar_video);
			hapus_file("./_media/videos/small/small_". $rp->gambar_video);
			hapus_file("./_media/videos/thumb/thumb_". $rp->gambar_video);

            // hapus id
            if ( ! $this->db->delete($this->tbl_playlist, array('id' => $id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
    /* End Playlist */

    /* Blog */
    function addBlog()
    {
        $this->load->helper('inflector');
        
        $nama_img = $this->upload->file_name;
        $keyword_id = format_keyword($this->input->post('keyword_id'));
        $keyword_en = format_keyword($this->input->post('keyword_en'));
        
        $data = array(
            'kategori_id' => $this->input->post('kategori_blog'),
            'embed_id_album' => $this->input->post('embed_album'),
            'embed_id_video' => $this->input->post('embed_video'),
            'gambar_cover' => $nama_img,
            'judul_id' => ucfirst($this->input->post('judul_id')),
            'judul_en' => ucfirst($this->input->post('judul_en')),
            'isi_id' => $this->input->post('isi_id'),
            'isi_en' => $this->input->post('isi_en'),
            'id_penulis' => $this->session->userdata('idmu'),
            'tgl_terbit' => date('Y-m-d H:i:s'),
            'tgl_revisi' => time(),
            'keyword_id' => $keyword_id,
            'keyword_en' => $keyword_en,
            'deskripsi_id' => $this->input->post('description_id'),
            'deskripsi_en' => $this->input->post('description_en'),
            'ratting' => $this->input->post('ratting'),
            'votes' => mt_rand(5, 15),
            'snippet' => ($this->input->post('snippet') == 'y')?$this->input->post('snippet'):'n',
            'headline' => $this->input->post('headline'),
            'diskusi' => $this->input->post('diskusi'),
            'status' => $this->input->post('status')
        );

        $str_id = $this->input->post('judul_id');
        $str_en = $this->input->post('judul_en');
        $this->db->where(array('judul_id'=>$str_id));
        $this->db->or_where(array('judul_en'=>$str_en));
        $query = $this->db->get($this->tbl_blog);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           if ( ! $this->db->insert($this->tbl_blog,$data))
           {
				return FALSE;
           }
           else {
				$ftag = format_array_idtag($this->input->post('tag'));
				$this->cms->addTagKonten($this->db->insert_id(), $ftag, 'blog');
				return TRUE;
           }
        }
    }

    function adaBlog($str)
    {
        $this->db->where(array('judul_id'=>$str));
        $this->db->or_where(array('judul_en'=>$str));
        $query = $this->db->get($this->tbl_blog);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }

    function adaUpdateBlog($str='',$nama_ori='',$bhs='')
    {
        $this->db->select('id');
        if($bhs === 'id')
		{
			$this->db->where('judul_id', $str);
			$this->db->where_not_in('judul_id', $nama_ori);
		}
		else {
			$this->db->where('judul_en', $str);
			$this->db->where_not_in('judul_en', $nama_ori);
		}
        
        $query = $this->db->get($this->tbl_blog);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }
    
    function getAllBlog($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->tbl_blog);
        return $query->result();
    }
    
    function getAllBlogAdminById($id)
    {
        $query = $this->db->get_where($this->tbl_blog,array('id' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function upBlogStat()
    {
        $data = array(
				'headline' => $this->input->post('headline'),
				'diskusi' => $this->input->post('diskusi'),
				'status' => $this->input->post('status')
            );
        $this->db->where('id', $this->input->post('blog_id'));
        if( ! $this->db->update($this->tbl_blog, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function upBlogForm()
    {
		$keyword_id = format_keyword($this->input->post('keyword_id'));
        $keyword_en = format_keyword($this->input->post('keyword_en'));

        if($this->input->post('status_ganti') == 'yes')
        {
            $nama_img = $this->upload->file_name;

            $data = array(
				'kategori_id' => $this->input->post('kategori_blog'),
				'embed_id_album' => $this->input->post('embed_album'),
				'embed_id_video' => $this->input->post('embed_video'),
				'gambar_cover' => $nama_img,
				'judul_id' => ucfirst($this->input->post('judul_id')),
	            'judul_en' => ucfirst($this->input->post('judul_en')),
				'isi_id' => $this->input->post('isi_id'),
				'isi_en' => $this->input->post('isi_en'),
				//'id_penulis' => $this->session->userdata('idmu'),
				'tgl_revisi' => time(),
				'keyword_id' => $keyword_id,
				'keyword_en' => $keyword_en,
				'deskripsi_id' => $this->input->post('description_id'),
	            'deskripsi_en' => $this->input->post('description_en'),
	            'ratting' => $this->input->post('ratting'),
				'snippet' => ($this->input->post('snippet') == 'y')?$this->input->post('snippet'):'n',
				'headline' => $this->input->post('headline'),
				'diskusi' => $this->input->post('diskusi'),
				'status' => $this->input->post('status')
			);
        }
        else {
            $data = array(
				'kategori_id' => $this->input->post('kategori_blog'),
				'embed_id_album' => $this->input->post('embed_album'),
				'embed_id_video' => $this->input->post('embed_video'),
				'judul_id' => ucfirst($this->input->post('judul_id')),
	            'judul_en' => ucfirst($this->input->post('judul_en')),
				'isi_id' => $this->input->post('isi_id'),
				'isi_en' => $this->input->post('isi_en'),
				//'id_penulis' => $this->session->userdata('idmu'),
				'tgl_revisi' => time(),
				'keyword_id' => $keyword_id,
				'keyword_en' => $keyword_en,
				'deskripsi_id' => $this->input->post('description_id'),
	            'deskripsi_en' => $this->input->post('description_en'),
	            'ratting' => $this->input->post('ratting'),
				'snippet' => ($this->input->post('snippet') == 'y')?$this->input->post('snippet'):'n',
				'headline' => $this->input->post('headline'),
				'diskusi' => $this->input->post('diskusi'),
				'status' => $this->input->post('status')
			);
        }

        $this->db->where('id', $this->input->post('id_blog'));
        if( ! $this->db->update($this->tbl_blog, $data))
        {
			return FALSE;
        }
		else {
			if( $this->input->post('tag') !== $this->input->post('tag_ori') )
			{
				$ftag = format_array_idtag($this->input->post('tag'));
				$this->cms->addTagKonten($this->input->post('id_blog'), $ftag, 'blog',TRUE);
				return TRUE;
			}
			else {
				return TRUE;
			}
		}
    }
    
    function getCountBlog()
    {
         $query = $this->db->get($this->tbl_blog);
         return $query->num_rows(); 
    }
    
    function hapusBlog()
    {
        $id = $this->input->post('id_hps');
        $qp = $this->db->get_where($this->tbl_blog, array('id' => $id), 1);
        if ($qp->num_rows() > 0)
        {
			$rp = $qp->row();

            //hapus gambar
			hapus_file( "./_media/blog/medium/medium_". $rp->gambar_cover);
			hapus_file("./_media/blog/small/small_". $rp->gambar_cover);
			hapus_file("./_media/blog/thumb/thumb_". $rp->gambar_cover);
            
            // hapus id
            if ( ! $this->db->delete($this->tbl_blog, array('id' => $id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
    /* End Blog */


    /* Widgets */
    function cntRec()
    {
        $query = $this->db->get($this->tbl_widget);
        return $query->num_rows();
    }
    
    function maxPosWidget()
    {
        $this->db->select_max('posisi');
        $query = $this->db->get($this->tbl_widget);
        $row = $query->row();
        return $row->posisi;
    }
    
    function uploadAddWidget($new)
    {
        $query = $this->db->get_where($this->tbl_widget, array('nama_widget'=>$new['nama_widget']));
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else{
           $this->db->insert($this->tbl_widget ,$new);
           return TRUE;
        }
    }
    
    function addWidget()
    {
        $pos = $this->maxPosWidget()+1;
        $data = array(
                'nama_widget' => $this->input->post('widget_name'),
                'judul_widget' => $this->input->post('judul_widget'),
                'judul_widget_en' => $this->input->post('judul_widget_en'),
                'css' => $this->input->post('widget_css'),
                'js' => $this->input->post('widget_js'),
                'html' => $this->input->post('widget_html'),
                'posisi' => $pos,
                'status' => $this->input->post('widget_status'),
                'path_admin_widget' => $this->input->post('widget_path'),
                'deskripsi' => $this->input->post('widget_desc')
        );
        
        $str = $this->input->post('widget_name');
        $query = $this->db->get_where($this->tbl_widget, array('nama_widget'=>$str));
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else{
           $this->db->insert($this->tbl_widget ,$data);
           return TRUE;
        }
    }
    
    function upWidgetPos()
    {
        if ( $this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('wid') ) 
        {
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_widget, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id_widget', $this->input->post('wid'));
                $this->db->update($this->tbl_widget, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    function upWidgetStat()
    {
        $data = array(
               'judul_widget' => $this->input->post('judul_widget'),
               'judul_widget_en' => $this->input->post('judul_widget_en'),
               'status' => $this->input->post('status')
            );

        $this->db->where('id_widget', $this->input->post('widget_id'));
        if ( ! $this->db->update($this->tbl_widget, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    function adaWidget($str)
    {
        $query = $this->db->get_where($this->tbl_widget, array('nama_widget'=>$str));
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else{
           return TRUE;
        }
    }
    
    function getAllWidget($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_widget);
        return $query->result();
    }
    
    function getCountWidget()
    {
         $query = $this->db->get($this->tbl_widget);
         return $query->num_rows(); 
    }
    
    function getWidgetAsetsAdmin()
    {
        $this->db->select('nama_widget, css, js');
        $query = $this->db->get($this->tbl_widget);
        return $query->result_array();
    }
    
    function getWidget()
    {
        $this->db->select('nama_widget,css,js');
        $this->db->order_by("posisi", "asc"); 
        $query = $this->db->get_where($this->tbl_widget, array('status'=>1));
        return $query->result_array();
    }
    
    function getWidgetName()
    {
        $this->db->select('nama_widget,judul_widget,judul_widget_en');
        $query = $this->db->get_where($this->tbl_widget, array('status'=>1));
        return $query->result_array();
    }
    
    function getWidgetHtml($nama)
    {
        $this->db->select('html');
        $query = $this->db->get_where($this->tbl_widget, array('nama_widget'=>$nama));
        return $query->row();
    }

    function hapusWidget()
    {
        $this->load->helper('directory');
        $id = $this->input->post('id_hps');

        $qp = $this->db->get_where($this->tbl_widget, array('id_widget' => $id));
        if ($qp->num_rows() > 0 && $id !== '3')
        {
            $rp = $qp->row();
            $pos = $rp->posisi;
            $nm = $rp->nama_widget;
            $wtbl = $rp->tabel_data;
            
            $this->db->order_by("posisi", "asc"); 
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_widget);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idw = $row->id_widget;
                $data = array('posisi' => $pos);
                $this->db->update($this->tbl_widget, $data, array('id_widget' => $idw));

                $pos++;
            }

            // Bersihkan file assets
            $this->load->helper('file');
            if (is_dir('./assets/js/widget/'.$nm))
            {
				$map = directory_map('./assets/js/widget/'.$nm , FALSE, TRUE);
				if (count($map) > 0)
                delete_files('./assets/js/widget/'.$nm.'/', TRUE);
                rmdir('./assets/js/widget/'.$nm.'/');
            }
            if (is_dir('./assets/css/widget/'.$nm))
            {
				$map = directory_map('./assets/css/widget/'.$nm , FALSE, TRUE);
				if (count($map) > 0)
                delete_files('./assets/css/widget/'.$nm.'/', TRUE);
                rmdir('./assets/css/widget/'.$nm.'/');
            }
            
            // Bersihkan file app
			hapus_file( APPPATH ."/widgets/". $nm . EXT );
			hapus_file( APPPATH ."/models/widgets/model_". $nm . EXT );
			hapus_file( CMSPATH ."/widgets/". $nm . "_view" . EXT );
			hapus_file( CMSPATH ."/widgets/". $nm . EXT );
            
            // hapus tabel datanya 
            if ( ! empty($wtbl))
            {
				$this->load->dbforge();
				$this->dbforge->drop_table($wtbl);
            }

            // hapus id
            $this->db->delete($this->tbl_widget, array('id_widget' => $id)); 

            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function hapusWidgetByNama($nama='',$wtbl='')
    {
        // hapus tabel datanya 
        if ( ! empty($wtbl))
        {
			$this->load->dbforge();
			$this->dbforge->drop_table($wtbl);
        }

        // hapus row
        $this->db->delete($this->tbl_widget, array('nama_widget' => $nama));
    }
    /* end Widget*/


    /* Modules */
    function maxPosModule()
    {
        $this->db->select_max('posisi');
        $query = $this->db->get($this->tbl_module);
        $row = $query->row();
        return $row->posisi;
    }
    
    function uploadAddModule($new)
    {
        $query = $this->db->get_where($this->tbl_module, array('nama_module'=>$new['nama_module']));
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           $this->db->insert($this->tbl_module ,$new);
           return TRUE;
        }
    }
    
    function addModule()
    {
        $pos = $this->maxPosModule()+1;
        $data = array(
                'nama_module' => $this->input->post('module_name'),
                'css' => $this->input->post('module_css'),
                'js' => $this->input->post('module_js'),
                'html' => $this->input->post('module_html'),
                'posisi' => $pos,
                'status' => $this->input->post('module_status'),
                'path_front_module' => $this->input->post('module_path'),
                'path_admin_module' => '',
                'deskripsi' => $this->input->post('module_desc')
        );
        
        $str = $this->input->post('module_name');
        $query = $this->db->get_where($this->tbl_module, array('nama_module'=>$str));
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           $this->db->insert($this->tbl_module,$data);
           return TRUE;
        }
    }
    
    function upModPos()
    {
        if ( $this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('modid') ) 
        {
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_module, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id_module', $this->input->post('modid'));
                $this->db->update($this->tbl_module, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    function upModuleStat()
    {
        $data = array(
               'status' => $this->input->post('status')
            );
        $this->db->where('id_module', $this->input->post('module_id'));
        if ( ! $this->db->update($this->tbl_module, $data) )
        {
         return FALSE;
        }
        else {
         return TRUE;
        }
    }
    
    function adaModule($str)
    {
        $query = $this->db->get_where($this->tbl_module, array('nama_module'=>$str));
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
    }
    
    function getAllModule($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_module);
        return $query->result();
    }
    
    function getCountModule()
    {
        $query = $this->db->get($this->tbl_module);
        return $query->num_rows();
    }
    
    function getModuleAsetsAdmin()
    {
        $this->db->select('id_module, nama_module, css, js');
        $query = $this->db->get($this->tbl_module);
        return $query->result_array();
    }
    
    function getModule()
    {
        $this->db->select('id_module,nama_module,css,js');
        $query = $this->db->get_where($this->tbl_module, array('status'=>1));
        return $query->result_array();
    }
    
    function getModuleName()
    {
        $this->db->select('nama_module,path_front_module');
        $this->db->order_by("posisi", "asc"); 
        $query = $this->db->get_where($this->tbl_module, array('status'=>1));
        return $query->result_array();
    }
    
    function getNamaModById($id)
    {
        $this->db->select('nama_module');
        $query = $this->db->get_where($this->tbl_module,array('id_module' => $id));
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            return $row->nama_module;
        }
        else {
            return NULL;
        }
    }
    
    function getModuleAsetsById($id)
    {
        $this->db->select('id_module, nama_module, css, js');
        $query = $this->db->get_where($this->tbl_module, array('id_module' => $id, 'status' => 1));
        return $query->result_array();
    }
    
    function hapusModule()
    {
        $this->load->helper('directory');
        $id = $this->input->post('id_hps');
        
        $qp = $this->db->get_where($this->tbl_module, array('id_module' => $id));
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();
            $pos = $rp->posisi;
            $nm = $rp->nama_module;
            $wtbl = $rp->tabel_data;
            
            $this->db->order_by("posisi", "asc"); 
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_module);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idmo = $row->id_module;
                $data = array('posisi' => $pos);
                $this->db->update($this->tbl_module, $data, array('id_module' => $idmo));
                
                $pos++;
            }

            //hapus menu - halaman
            $this->db->select('id_halaman');
			$halaman = $this->db->get_where($this->tbl_halaman,array('id_module'=>$id));
			if($halaman->num_rows() > 0)
			{
				foreach( $halaman->result() as $hal)
				{
					// hapus menu
					$menu = $this->db->get_where($this->tbl_menu,array('link'=>$hal->id_halaman));
					if($menu->num_rows() > 0)
					{
						foreach( $menu->result() as $mnu)
						{
							$this->hapusMenu($mnu->id);
						}
					}
				}
			}

            // Bersihkan file assets
            $this->load->helper('file');
            if (is_dir('./assets/js/modul/'.$nm))
            {
				$map = directory_map('./assets/js/modul/'.$nm , FALSE, TRUE);
				if (count($map) > 0)
                delete_files('./assets/js/modul/'.$nm.'/', TRUE);
                rmdir('./assets/js/modul/'.$nm.'/');
            }
            if (is_dir('./assets/css/modul/'.$nm))
            {
				$map = directory_map('./assets/css/modul/'.$nm , FALSE, TRUE);
				if (count($map) > 0)
                delete_files('./assets/css/modul/'.$nm.'/', TRUE);
                rmdir('./assets/css/modul/'.$nm.'/');
            }
            
            // Bersihkan file app
            if (is_dir(APPPATH ."modules/mod_". $nm))
            {
				$map = directory_map('./' . APPPATH . "modules/mod_" .  $nm , FALSE, TRUE);
				if (count($map) > 0)
                delete_files('./' . APPPATH . "modules/mod_" . $nm, TRUE);
                rmdir('./'. APPPATH ."modules/mod_". $nm);
            }
            
            // hapus tabel datanya
            if ( ! empty($wtbl))
            {
                $wtbl = explode(",", $wtbl);
                for ($i=0; $i < count($wtbl); $i++)
                {
                    $this->load->dbforge();
                    $this->dbforge->drop_table($wtbl[$i]);
                }
            }

			//hapus menu - halaman
            $this->db->select('id_halaman');
			$halaman = $this->db->get_where($this->tbl_halaman,array('id_module'=>$id));
			if($halaman->num_rows() > 0)
			{
				foreach( $halaman->result() as $hal)
				{
					// hapus menu
					$menu = $this->db->get_where($this->tbl_menu,array('link'=>$hal->id_halaman));
					if($menu->num_rows() > 0)
					{
						foreach( $menu->result() as $mnu)
						{
							$this->hapusMenu($mnu->id);
						}
					}
				}
			}

            // hapus id
            $this->db->delete($this->tbl_module, array('id_module' => $id));
            
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    /* End Modules */

}
/* End Class */

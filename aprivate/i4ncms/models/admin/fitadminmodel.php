<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

Class FitAdminModel extends MY_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }

/* Menu */
    function getCountCariMenu($cari='')
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('title_id', $cari);
		$this->db->or_like('title_en', $cari);
        $query = $this->db->get($this->tbl_menu);
        return $query->num_rows(); 
    }

    function getCariMenu($cari='',$limit=0, $offset=0)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('title_id', $cari);
		$this->db->or_like('title_en', $cari);
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_menu);
        if($query->num_rows > 0)
        {
            return $query->result();
        }
    }
/* End Menu */


/* Halaman */
	function getCountCariHalaman($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('label_id', $cari);
		$this->db->or_like('label_en', $cari);
		$query = $this->db->get($this->tbl_halaman);
        return $query->num_rows();
	}
	
    function getCariHalaman($cari='',$limit=0,$offset=0)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('label_id', $cari);
		$this->db->or_like('label_en', $cari);
		$this->db->limit($limit, $offset);
        $this->db->order_by("tipe", "asc");
        $query = $this->db->get($this->tbl_halaman);
        if($query->num_rows > 0)
        {
            return $query->result();
        }
    }
/* End Halaman */


/* Album */
	function getCountCariAlbum($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_id', $cari);
		$this->db->or_like('nama_en', $cari);
		$query = $this->db->get($this->tbl_album);
        return $query->num_rows();
	}
	
    function getCariAlbum($cari='',$limit=0,$offset=0)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_id', $cari);
		$this->db->or_like('nama_en', $cari);
		$this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_album);
		return $query->result();
    }
//Gallery===================|
	function getCountCariGallery($cari='',$id='')
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->where('id_album', $id);
		$this->db->like('nama_id', $cari);
		$query = $this->db->get($this->tbl_gallery);
		return $query->num_rows();
    }

    function getCariGallery($cari='',$id='',$limit, $offset)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->where('id_album', $id);
		$this->db->like('nama_id', $cari);
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_gallery);
        return $query->result();
    }
/* End Album */

/* Video */
	function getCountCariVideo($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_id', $cari);
		$this->db->or_like('nama_en', $cari);
		$query = $this->db->get($this->tbl_video);
        return $query->num_rows();
	}
	
    function getCariVideo($cari='',$limit=0,$offset=0)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_id', $cari);
		$this->db->or_like('nama_en', $cari);
		$this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_video);
        return $query->result();
    }
//Playlist===================|
	function getCountCariPlaylist($cari='',$id='')
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->where('id_video', $id);
		$this->db->like('nama_id', $cari);
		$query = $this->db->get($this->tbl_playlist);
		return $query->num_rows();
    }

    function getCariPlaylist($cari='',$id='',$limit, $offset)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->where('id_video', $id);
		$this->db->like('nama_id', $cari);
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_playlist);
        return $query->result();
    }
/* End Video */

/* Blog */
	function getCountCariBlog($cari='')
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('judul_id', $cari);
		$this->db->or_like('judul_en', $cari);
		$query = $this->db->get($this->tbl_blog);
		return $query->num_rows();
    }

    function getCariBlog($cari='',$limit, $offset)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('judul_id', $cari);
		$this->db->or_like('judul_en', $cari);
        $this->db->limit($limit, $offset);
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->tbl_blog);
        return $query->result();
    }
/* End Blog */

/* Produk */
	function getCountCariProduk($cari='',$id_induk='')
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->select('id_prod');
		$this->db->like('nama_prod', $cari);
		if(!empty($id_induk))
		$this->db->where('parent_id_prod',$id_induk);
		else
		$this->db->where('parent_id_prod',NULL);
		$query = $this->db->get($this->tbl_produk);
		return $query->num_rows();
    }

    function getCariProdukInduk($cari='',$limit, $offset)
    {
		$cari = humanize(format_string_aman($cari));
        $this->db->select('id_prod,kode_prod,promo,id_prod_img,nama_prod,nama_prod_en,diskon,harga_prod,harga_spesial,stok,total_hits,status');
        $this->db->like('nama_prod', $cari);
		$this->db->where('parent_id_prod',NULL);
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->order_by('id_prod','desc');
		$query = $this->db->get($this->tbl_produk);
		return $query->result();
    }

    function getCariProdukAnak($cari='',$id_induk=0, $limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
        $this->db->like('nama_prod', $cari);
        $this->db->where('parent_id_prod',(int)$id_induk);
		$this->db->order_by('id_prod','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$query = $this->db->get($this->tbl_produk);
		return $query->result();
	}
/* End Produk */


/* Orderan */
	function getCountCariOrderProses($cari='',$proses = 'pembayaran')
	{
		$cari = humanize(format_string_aman($cari));
		if($proses === 'batal')
		$this->db->where('status_orderan','batal');
		else
		$this->db->where('status_orderan','proses');
		
		if($proses === 'pembayaran')
		$this->db->where('status_transaksi','pembayaran');
		if($proses === 'pending')
		$this->db->where('status_transaksi','pending');
		if($proses === 'pemaketan')
		$this->db->where('status_transaksi','pemaketan');
		if($proses === 'pengiriman')
		$this->db->where('status_transaksi','pengiriman');

		$this->db->like('no_struk', $cari);
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order);
		return $query->num_rows(); 
	}
	
	function getCariOrderanProses($cari='',$limit=0, $offset=0, $proses='pembayaran')
	{
		$cari = humanize(format_string_aman($cari));
		settype($offset, 'integer');
		settype($limit, 'integer');
		
		if($proses === 'batal')
		$this->db->where('status_orderan','batal');
		else
		$this->db->where('status_orderan','proses');
		
		if ( $proses === 'pembayaran')
		$this->db->where('status_transaksi','pembayaran');
		if ( $proses === 'pending')
		$this->db->where('status_transaksi','pending');
		if ( $proses === 'pemaketan')
		$this->db->where('status_transaksi','pemaketan');
		if ( $proses === 'pengiriman')
		$this->db->where('status_transaksi','pengiriman');
		
		$this->db->like('no_struk', $cari);
		$this->db->order_by('status_transaksi','desc');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order);
		
		return $query->result();
	}

	function getCountCariOrderBatal($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('no_struk', $cari);
		$this->db->where('status_orderan','batal');
		$this->db->order_by('status_transaksi','desc');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order_batal);
		return $query->num_rows(); 
	}

	function getCariOrderanBatal($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('no_struk', $cari);
		$this->db->where('status_orderan','batal');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->group_by('no_struk','desc'); 
		$query = $this->db->get($this->tbl_order_batal);
		return $query->result();
	}

	function getCountCariOrderSukses($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('no_struk', $cari);
		$this->db->where('status_orderan','sukses');
		$this->db->order_by('status_transaksi','desc');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order_sukses);
		return $query->num_rows(); 
	}

	function getCariOrderanSukses($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('no_struk', $cari);
		$this->db->where('status_orderan','sukses');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->group_by('no_struk','desc'); 
		$query = $this->db->get($this->tbl_order_sukses);
		return $query->result();
	}
/* End Orderan */


/* Pelanggan */
	function getCountCariPelanggan($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$hasil = array();

		$this->db->select('id,nama_lengkap');
		$daftar = $this->db->get($this->tbl_pembeli);
		foreach($daftar->result() as $row)
		{
			if(preg_match("/$cari/i", dekodeString($row->nama_lengkap)))
			$hasil[] = $row->id;
		}
		return count($hasil);
	}

	function getCariPelanggan($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$hasil = array();

		$this->db->select('id,nama_lengkap');
		$daftar = $this->db->get($this->tbl_pembeli);
		foreach($daftar->result() as $row)
		{
			if(preg_match("/$cari/i", dekodeString($row->nama_lengkap)))
			$hasil[] = $row->id;
		}

		if(count($hasil) > 0)
		{
			$this->db->select('id,tgl_daftar,tgl_update_data,id_cust_uname,nama_lengkap,email_cust,no_telpon,status_cust,status_rss,cust_level,jml_transaksi,total_transaksi,jumlah_transaksi_gagal,total_transaksi_gagal,jml_poin,prioritas_cust');
			$this->db->where_in('id', array_values($hasil));
			$this->db->order_by('id','desc');
			$this->db->limit((int)$limit, (int)$offset);
			$query = $this->db->get($this->tbl_pembeli);
			return $query->result();
		}
		else
			return NULL;
	}
/* End Pelanggan */


/* Iklan */
	function getCountCariIklan($cari='',$posisi='')
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_id', $cari);
		$this->db->where('posisi', $posisi);
		$query = $this->db->get($this->tbl_banner_iklan);
		return $query->num_rows();
    }
    
	function getCariIklan($cari='',$posisi='',$limit=0,$offset=0)
    {
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_id', $cari);
		$this->db->where('posisi', $posisi);
		$this->db->limit($limit,$offset);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_banner_iklan);
        return $query->result();
    }
/* End Iklan */


/* Widget */
	function getCountCariWidget($cari='')
    {
    	$cari = humanize(format_string_aman($cari));
    	$this->db->like('nama_widget', $cari);
		$this->db->or_like('judul_widget', $cari);
		$this->db->or_like('judul_widget_en', $cari);
		$query = $this->db->get($this->tbl_widget);
        return $query->num_rows(); 
    }

    function getCariWidget($cari='',$limit, $offset)
    {
    	$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_widget', $cari);
		$this->db->or_like('judul_widget', $cari);
		$this->db->or_like('judul_widget_en', $cari);
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_widget);
        return $query->result();
    }
/* End Widget */

/* Module */
	function getCountCariModule($cari='')
    {
    	$cari = humanize(format_string_aman($cari));
    	$this->db->like('nama_module', $cari);
		$this->db->or_like('deskripsi', $cari);
		$query = $this->db->get($this->tbl_module);
        return $query->num_rows();
    }

    function getCariModule($cari='',$limit, $offset)
    {
    	$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_module', $cari);
		$this->db->or_like('deskripsi', $cari);
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_module);
        return $query->result();
    }
/* End Module */

/* Log */
	function getCountCariLog($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('tipe_error', $cari);
		$this->db->or_like('ip_port', $cari);
		$this->db->or_like('user_agent', $cari);
		$query = $this->db->get($this->tbl_log);
		return $query->num_rows(); 
	}

	function lihatCariLog($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('tipe_error', $cari);
		$this->db->or_like('ip_port', $cari);
		$this->db->or_like('user_agent', $cari);
		$this->db->limit($limit, $offset);
		$this->db->order_by("tgl", "desc");
		$query = $this->db->get($this->tbl_log);
		return $query->result();
	}

	function getCountCariLogUser($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('ip', $cari);
		$this->db->or_like('lokasi', $cari);
		$this->db->or_like('tipe_halaman', $cari);
		$this->db->or_like('url_asal', $cari);
		$this->db->or_like('detail_platform', $cari);
		$query = $this->db->get($this->tbl_ref_user);
		return $query->num_rows(); 
	}

	function lihatCariLogUser($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('ip', $cari);
		$this->db->or_like('lokasi', $cari);
		$this->db->or_like('tipe_halaman', $cari);
		$this->db->or_like('url_asal', $cari);
		$this->db->or_like('detail_platform', $cari);
		$this->db->limit($limit, $offset);
		$this->db->order_by("tgl_kujungan", "desc");
		$query = $this->db->get($this->tbl_ref_user);
		return $query->result();
	}

	function getCountCariLogRobot($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('ip', $cari);
		$this->db->or_like('tipe_halaman', $cari);
		$this->db->or_like('url_asal', $cari);
		$this->db->or_like('url_target', $cari);
		$this->db->or_like('detail_robot', $cari);
		$query = $this->db->get($this->tbl_log_robot);
		return $query->num_rows(); 
	}

	function lihatCariLogRobot($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('ip', $cari);
		$this->db->or_like('tipe_halaman', $cari);
		$this->db->or_like('url_asal', $cari);
		$this->db->or_like('url_target', $cari);
		$this->db->or_like('detail_robot', $cari);
		$this->db->limit($limit, $offset);
		$this->db->order_by("tgl_kujungan", "desc");
		$query = $this->db->get($this->tbl_log_robot);
		return $query->result();
	}
/* End Log */

/* Metode Bayar */
	function getCountCariMetodeBayarTransfer($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'transfer');
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->num_rows(); 
	}

	function lihatCariMetodeBayarTransfer($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'transfer');
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->result();
	}

	function getCountCariMetodeBayarOnline($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'online');
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->num_rows(); 
	}

	function lihatCariMetodeBayarOnline($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'online');
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->result();
	}

	function getCountCariMetodeBayarGiro($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'giro');
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->num_rows(); 
	}

	function lihatCariMetodeBayarGiro($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'giro');
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->result();
	}

	function getCountCariMetodeBayarCod($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'cod');
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->num_rows(); 
	}

	function lihatCariMetodeBayarCod($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->where('tipe_metode', 'cod');
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->result();
	}
/* End Metode Bayar */


/* Kurir Pengiriman */
	function getCountCariKurir($cari='')
    {
    	$cari = humanize(format_string_aman($cari));
    	$this->db->like('nama_vendor', $cari);
		$this->db->or_like('nama_layanan', $cari);
		$query = $this->db->get($this->tbl_kurir);
		return $query->num_rows();
    }

    function getCariKurir($cari='',$limit, $offset)
    {
    	$cari = humanize(format_string_aman($cari));
		$this->db->like('nama_vendor', $cari);
		$this->db->or_like('nama_layanan', $cari);
        $this->db->limit($limit, $offset);
        $query = $this->db->get($this->tbl_kurir);
        return $query->result();
    }
/* End Kurir Pengiriman */

/* Kategori */
	function getCountCariKategori($cari='')
    {
    	$cari = humanize(format_string_aman($cari));
    	$this->db->like('label_id', $cari);
		$this->db->or_like('label_en', $cari);
		$query = $this->db->get($this->tbl_kategori);
		//return $query->num_rows();
		return $this->db->last_query();
    }

    function getCariKategori($cari='',$limit, $offset)
    {
    	$cari = humanize(format_string_aman($cari));
		$this->db->like('label_id', $cari);
		$this->db->or_like('label_en', $cari);
        $this->db->limit($limit, $offset);
        $query = $this->db->get($this->tbl_kategori);
        return $query->result();
    }
/* End Kategori */

}

/*
	fitadminmodel.php
	Model fitur admin
*/

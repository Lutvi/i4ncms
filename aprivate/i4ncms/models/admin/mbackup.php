<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Mbackup extends MY_Model {

	function __construct()
    {
        parent::__construct();
    }

/* Utilitas Super Admin */
	function addHistoryBackup($aksi='')
	{
		$tgl = date('dmY', time());
		$ts = time();
		$admid = $this->session->userdata('idmu');
		$ket = 'Aksi '.humanize($aksi).' Backup dilakukan oleh '. $this->session->userdata('nama_admin');
		$data = array('tgl' => $tgl, 'ts' => $ts, 'aksi' => $aksi, 'admid' => $admid, 'ket' => $ket);

		//cek
		$query = $this->db->get_where($this->tbl_bck, array('tgl' => $tgl), 1);
		if($query->num_rows() > 0)
		$this->db->update($this->tbl_bck, $data, array('tgl' => $tgl));
		else
		$this->db->insert($this->tbl_bck, $data);
	}

	function getCountAllBackup()
	{
		return $this->db->count_all($this->tbl_bck);
	}

	function getCountHistoryBackupByTgl($tgl='')
	{
		if(empty($tgl))
		$tgl = date("dmY");

		$query = $this->db->get_where($this->tbl_bck, array('tgl' => $tgl), 1);
		return $query->num_rows();
	}

	function getStsBackupByTgl($tgl='')
	{
		if(empty($tgl))
		$tgl = date("dmY");

		$this->db->select('aksi,status');
		$query = $this->db->get_where($this->tbl_bck, array('tgl' => $tgl), 1);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}

	function upStsBackupByTgl($tgl='',$sts='')
	{
		$ket = 'Aksi '.humanize($sts).' File Backup dilakukan oleh '. $this->session->userdata('nama_admin');
		$data = array('status' => $sts, 'ket' => $ket);
		$this->db->update($this->tbl_bck, $data, array('tgl' => $tgl));
	}

	function geAlltHistoryBackup()
	{
		$this->db->select('tgl,ts');
		$this->db->order_by('ts','desc');
		$query = $this->db->get($this->tbl_bck);
		return $query->result();
	}

	function getDependModTbl()
	{
		$tbl = array();
		$this->db->select('tabel_data');
		$query = $this->db->get($this->tbl_module);
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				if( ! empty($row->tabel_data) || ! is_null($row->tabel_data))
				$tbl = array_merge($tbl,explode(",", $row->tabel_data));
			}
		}
		return $tbl;
	}

	function getDependWidTbl()
	{
		$tbl = array();
		$this->db->select('tabel_data');
		$query = $this->db->get($this->tbl_widget);
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				if( ! empty($row->tabel_data) || ! is_null($row->tabel_data))
				$tbl = array_merge($tbl,explode(",", $row->tabel_data));
			}
		}
		return $tbl;
	}

	function stopWebsite($clear=FALSE)
	{
		$nama = 'sts_web';
		$nilai = '2';
		$tipe ='angka';
		$ket = 'Tentukan status website opsi, 1=Offline, 2=Perbaikan, 3=Online';
		$this->simpanConfig($nama,$nilai,$tipe,$ket);

		$nama = 'super_aktif';
		$nilai = '1';
		$tipe ='boolean';
		$ket = 'Tentukan status Proteksi Level Admin, TRUE=On, FALSE=Off';
		$this->simpanConfig($nama,$nilai,$tipe,$ket);

		if ($clear === TRUE)
		{
			// Hapus session Front user
			$user = array('kunci_id'=>'','kunci'=>'','email_beli'=>'','usrid'=>'','nmusr'=>'','alamat_id'=>'',
					  'trx_order'=>'','total_transaksi'=>'','diskon_transaksi'=>'');
			$this->session->unset_userdata($user);
			$this->load->library('cart');
			$this->cart->destroy();
		}
	}

	function startWebsite()
	{
		$nama = 'sts_web';
		$nilai = '3';
		$tipe ='angka';
		$ket = 'Tentukan status website opsi, 1=Offline, 2=Perbaikan, 3=Online';
		$this->simpanConfig($nama,$nilai,$tipe,$ket);

		$nama = 'super_aktif';
		$nilai = '0';
		$tipe ='boolean';
		$ket = 'Tentukan status Proteksi Level Admin, TRUE=On, FALSE=Off';
		$this->simpanConfig($nama,$nilai,$tipe,$ket);
	}

	function restore_dbnya($src='')
	{
		ini_set('memory_limit', '1026M'); //1GB
		ini_set('zlib.output_compression', 0);
		set_time_limit(0);

		if(file_exists($src) && $this->session->userdata('level') === bs_kode('superman'))
		{
			$backup = file_get_contents($src);
			$sql_clean = '';
			foreach (explode("\n", $backup) as $line){
				if(isset($line[0]) && $line[0] != "#"){
					$sql_clean .= $line."\n";
				}
			}
			$this->db->trans_begin();
			foreach (explode(";\n", $sql_clean) as $sql){
				$sql = trim($sql);
				if($sql) 
				{
					$this->db->query($sql);
				}
			}
			//Cek Transaksi
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			    return FALSE;
			}
			else {
			    $this->db->trans_commit();
			    return TRUE;
			}
		}
		else {
			return FALSE;
		}
	}

	function cleanFileBackup()
	{
		$this->load->helper('file');
		$this->db->select('id,tgl,ts');
		$query = $this->db->get($this->tbl_bck);
		$cek = TRUE;
		if($query->num_rows() > 0) {
			$cek_ada = 0;
			foreach($query->result() as $row)
			{
				if(date('Y',$row->ts) === date('Y'))
				$cek_ada++;

				if(date('Y',$row->ts) !== date('Y') && $cek_ada > 0) {
					if(is_dir('./_xbck/pack/'.$row->tgl) && is_writable('./_xbck/pack/'.$row->tgl)){
						if(delete_files('./_xbck/pack/'.$row->tgl,TRUE))
						$cek = $this->db->delete($this->tbl_bck, array('id' => $row->id));
					}
				}
			}
		}
		return $cek;
	}

	function simpanConfig($nama='',$nilai='',$tipe='',$ket='')
    {
        $query = $this->db->get_where($this->tbl_web_config,array('nama_config'=>$nama),1);

        if($query->num_rows() > 0)
        {
            $data = array( 'nilai_config' => $nilai );
            $this->db->where('nama_config', $nama);
            if( ! $this->db->update($this->tbl_web_config,$data))
                return FALSE;
            else
                return TRUE;
        }
        else {
            $data = array(
               'nama_config' => $nama,
               'nilai_config' => $nilai,
               'tipe_data' => $tipe,
               'ket' => $ket
            );

            if( ! $this->db->insert($this->tbl_web_config,$data))
                return FALSE;
            else
                return TRUE;
        }
    }

}

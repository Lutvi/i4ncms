<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Webmodel extends MY_Model {

    function __construct()
    {
        parent::__construct();
    }

    /* statistik */
	function getLokasiStatistik($nilai=TRUE)
	{
		if($nilai)
		$this->db->select_sum('hits');
		else
		$this->db->select('lokasi');

		$this->db->group_by("lokasi"); 
        $query = $this->db->get($this->tbl_ref_user);
        
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else {
            return array();
        }
	}

	function getBrowserStatistik($nilai=TRUE)
	{
		if($nilai)
		$this->db->select_sum('hits');
		else
		$this->db->select('browser');

		$this->db->group_by("browser"); 
        $query = $this->db->get($this->tbl_ref_user);
        
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else {
            return array();
        }
	}

	function getHalamanStatistik($nilai=TRUE)
	{
		if($nilai)
		$this->db->select_sum('hits');
		else
		$this->db->select('tipe');

		$this->db->group_by("tipe"); 
        $query = $this->db->get($this->tbl_halaman);
        
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else {
            return array();
        }
	}
    /* end ststistik */

    /*Config*/
    function simpanConfig($nama='',$nilai='',$tipe='',$ket='')
    {
        $query = $this->db->get_where($this->tbl_web_config,array('nama_config'=>$nama),1);

        if($query->num_rows() > 0)
        {
            $data = array( 'nilai_config' => $nilai );
            $this->db->where('nama_config', $nama);
            if( ! $this->db->update($this->tbl_web_config,$data))
                return FALSE;
            else
                return TRUE;
        }
        else {
            $data = array(
               'nama_config' => $nama,
               'nilai_config' => $nilai,
               'tipe_data' => $tipe,
               'ket' => $ket
            );

            if( ! $this->db->insert($this->tbl_web_config,$data))
                return FALSE;
            else
                return TRUE;
        }
    }
    
    function getAllTemaWeb()
    {
        $this->db->where('versi', $this->config->item('cms_version'));
        $query = $this->db->get($this->tbl_tema);
        
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else {
            return array();
        }
    }
    
    function upTemaWeb($tema='')
    {
        $this->db->where('status', 'on');
        $this->db->update($this->tbl_tema, array( 'status' => 'off'));

        $aktif = array( 'status' => 'on');
        $this->db->where('nama_tema', trim($tema));
        if( ! $this->db->update($this->tbl_tema, $aktif) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    function addTemaWeb($data=array())
    {
        $cek = $data['nama_tema'];
        
        $this->db->select('nama_tema');
        $this->db->where('nama_tema', $cek);
        $query = $this->db->get($this->tbl_tema);
        
        if($query->num_rows() > 0)
        {
            return FALSE;
        }
        else {
            if ( ! $this->db->insert($this->tbl_tema, $data))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }     
    }
    
    function upOpsiTema($nilai='')
    {
        $aktif = array( 'nilai_opsi' => $nilai);
        $this->db->where('status', 'on');
        if( ! $this->db->update($this->tbl_tema, $aktif))
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

// Kategori
	 function getCountKategoriKonten()
    {
         $query = $this->db->get($this->tbl_kategori);
         return $query->num_rows(); 
    }
    
    function getAllKategoriKonten($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("tipe_kategori", "asc");
        $query = $this->db->get($this->tbl_kategori);
        return $query->result();
    }

    function cekUpKategori($str='',$ori='',$tipe='',$bhs='')
	{
		$this->db->select('id_kategori');

		if($bhs==='id')
		{
			$this->db->where('label_id',$str);
			$this->db->where_not_in('label_id',$ori);
		}
		else {
			$this->db->where('label_en',$str);
			$this->db->where_not_in('label_en',$ori);
		}

		$this->db->where('tipe_kategori', $tipe);
		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function upLabelKategori()
	{
        $data = array(
               'label_id' => ucwords($this->input->post('nama_id')),
               'label_en' => ucwords($this->input->post('nama_en'))
            );
        $this->db->where('id_kategori', (int)$this->input->post('katid'));
        if( ! $this->db->update($this->tbl_kategori, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
	}
/* End Edit Kategori */


/* IKLAN */
    function getCountIklan($posisi='')
    {
		$this->db->where('posisi', $posisi);
		$query = $this->db->get($this->tbl_banner_iklan);

		return $query->num_rows();
    }
    
	function getAllIklan($posisi='',$limit=0,$offset=0)
    {
		$this->db->where('posisi', $posisi);
		$this->db->limit($limit,$offset);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_banner_iklan);
        return $query->result();
    }

    function getIklanById($id='')
    {
		$this->db->where('id', (int)$id);
		$this->db->limit(1);
        $query = $this->db->get($this->tbl_banner_iklan);
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }
    
    function addIklan()
	{
		$nama_img = $this->upload->file_name;
		$data = array(
					'nama_pengiklan' => $this->input->post('nama_pengiklan'),
					'nama_id' => $this->input->post('nama_id'),
					'nama_en' => $this->input->post('nama_en'),
					'img_src' => $nama_img,
					'url_target' => $this->input->post('url_target'),
					'posisi' => $this->input->post('posisi'),
					'tgl_terbit' => format_tgl_sql($this->input->post('tgl_terbit')),
					'tgl_berakhir' => format_tgl_sql($this->input->post('tgl_berakhir')),
					'status' => $this->input->post('status'),
					'tampil' => 0,
					'hits' => 0,
					'kredit' => $this->input->post('kredit')
				);

		if ( ! $this->db->insert($this->tbl_banner_iklan, $data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function upIklanStat()
    {
        $data = array(
               'status' => $this->input->post('status', TRUE)
            );
        $this->db->where('id', (int)$this->input->post('iklan_id', TRUE));
        if( ! $this->db->update($this->tbl_banner_iklan, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function upIklanForm()
    {
        if($this->input->post('status_ganti') == 'yes')
        {
            $nama_img = $this->upload->file_name;

            $data = array(
                'nama_pengiklan' => $this->input->post('nama_pengiklan'),
				'nama_id' => $this->input->post('nama_id'),
				'nama_en' => $this->input->post('nama_en'),
				'img_src' => $nama_img,
				'url_target' => $this->input->post('url_target'),
				'posisi' => $this->input->post('posisi'),
				'tgl_terbit' => format_tgl_sql($this->input->post('tgl_terbit')),
				'tgl_berakhir' => format_tgl_sql($this->input->post('tgl_berakhir')),
				'status' => $this->input->post('status'),
				'kredit' => $this->input->post('kredit')
            );
        }
        else {
            $data = array(
                'nama_pengiklan' => $this->input->post('nama_pengiklan'),
				'nama_id' => $this->input->post('nama_id'),
				'nama_en' => $this->input->post('nama_en'),
				'url_target' => $this->input->post('url_target'),
				'posisi' => $this->input->post('posisi'),
				'tgl_terbit' => format_tgl_sql($this->input->post('tgl_terbit')),
				'tgl_berakhir' => format_tgl_sql($this->input->post('tgl_berakhir')),
				'status' => $this->input->post('status'),
				'kredit' => $this->input->post('kredit')
            );
        }

        $this->db->where('id', $this->input->post('id_iklan'));
        if( ! $this->db->update($this->tbl_banner_iklan, $data))
        {
			return FALSE;
        }
		else {
			//hapus gambar
			if($this->input->post('status_ganti') == 'yes')
			{
				hapus_file("./_media/banner-iklan/medium/medium_". $this->input->post('gambar'));
				hapus_file("./_media/banner-iklan/thumb/thumb_". $this->input->post('gambar'));
			}
			return TRUE;
		}
    }

    function hapusIklan()
    {
        $id = $this->input->post('id_hps', TRUE);
        $qp = $this->db->get_where($this->tbl_banner_iklan, array('id' => (int)$id), 1);
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();

            if(time() > strtotime($rp->tgl_berakhir))
            {
				// hapus id
				if ( ! $this->db->delete($this->tbl_banner_iklan, array('id' => $id)))
				{
					return FALSE;
				}
				else {
					//hapus gambar
					hapus_file("./_media/banner-iklan/medium/medium_". $rp->img_src);
					hapus_file("./_media/banner-iklan/thumb/thumb_". $rp->img_src);
					return TRUE;
				}
            }
            else {
				return FALSE;
			}
        }
        else {
            return FALSE;
        }
    }
    /* End IKLAN */


	/* Banner Header */
	function maxHbanner()
    {
        $this->db->select_max('posisi');
        $query = $this->db->get($this->tbl_banner_header);
        $row = $query->row();
        if($row->posisi > 0)
        return $row->posisi+1;
        else
        return 1;
    }

	function getCountHbanner()
    {
		return $this->db->count_all($this->tbl_banner_header);
    }
    
	function getAllHbanner($limit=0,$offset=0)
    {
		$this->db->limit($limit,$offset);
		$this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_banner_header);
        return $query->result();
    }
    
	function addHbanner($pos=0)
	{
		$nama_img = $this->upload->file_name;
		$data = array(
					'nama_id' => $this->input->post('nama_id'),
					'nama_en' => $this->input->post('nama_en'),
					'ket_id' => $this->input->post('ket_id'),
					'ket_en' => $this->input->post('ket_en'),
					'url_target' => $this->input->post('url_target'),
					'img_src' => $nama_img,
					'posisi' => $this->maxHbanner(),
					'status' => $this->input->post('status')
				);
		
		if ( ! $this->db->insert($this->tbl_banner_header, $data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function upHbannerPos()
    {
        if($this->input->post('posisi', TRUE) && $this->input->post('old_posisi', TRUE) && $this->input->post('hbid', TRUE) )
        {
            if($this->input->post('old_posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('old_posisi')
                    );
                $this->db->where('posisi', $this->input->post('posisi'));
                $this->db->update($this->tbl_banner_header, $data);
            }
            if($this->input->post('posisi'))
            {
                $data = array(
                       'posisi' => $this->input->post('posisi')
                    );
                $this->db->where('id', $this->input->post('hbid'));
                $this->db->update($this->tbl_banner_header, $data);
            }
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function upHbannerStat()
    {
        $data = array(
               'status' => $this->input->post('status', TRUE)
            );
        $this->db->where('id', $this->input->post('hbanner_id', TRUE));
        if( ! $this->db->update($this->tbl_banner_header, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function getHbannerById($id='')
	{
		$this->db->where('id', (int)$id);
		$query = $this->db->get($this->tbl_banner_header,1);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	//Up
	function upFormBanner()
	{
		if($this->input->post('status_ganti') == 'yes')
		{
			$nama_img = $this->upload->file_name;
			$data = array(
					'nama_id' => $this->input->post('nama_id'),
					'nama_en' => $this->input->post('nama_en'),
					'ket_id' => $this->input->post('ket_id'),
					'ket_en' => $this->input->post('ket_en'),
					'url_target' => $this->input->post('url_target'),
					'img_src' => $nama_img,
					'status' => $this->input->post('status')
				);
		}
		else {
			$data = array(
					'nama_id' => $this->input->post('nama_id'),
					'nama_en' => $this->input->post('nama_en'),
					'ket_id' => $this->input->post('ket_id'),
					'ket_en' => $this->input->post('ket_en'),
					'url_target' => $this->input->post('url_target'),
					'status' => $this->input->post('status')
				);
		}

		$this->db->where('id', $this->input->post('id_banner'));
        if( ! $this->db->update($this->tbl_banner_header, $data))
        {
			return FALSE;
        }
		else {
			return TRUE;
		}
	}

    function hapusHbanner()
    {
        $id = $this->input->post('id_hps');
        $qp = $this->db->get_where($this->tbl_banner_header, array('id' => (int)$id), 1);
        if ($qp->num_rows() > 0)
        {
            $rp = $qp->row();
            $pos = $rp->posisi;

            $this->db->order_by("posisi", "asc");
            $this->db->where('posisi >',$pos);
            $query = $this->db->get($this->tbl_banner_header);
            
            foreach($query->result() as $row)
            {
                // update posisi
                $idalb = $row->id;
                $data = array('posisi' => $pos);
                $this->db->update($this->tbl_banner_header, $data, array('id' => $idalb));
                
                $pos++;
            }

            // hapus id
            if ( ! $this->db->delete($this->tbl_banner_header, array('id' => $id)))
            {
                return FALSE;
            }
            else {
				//hapus gambar
	            hapus_file("./_media/banner-header/". $rp->img_src);
	            hapus_file("./_media/banner-header/thumb_". $rp->img_src);
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
    }
	/* End Banner Header */

}

<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Blogmodel extends CI_Model {

	var $tbl_artikel   = 'tbl_mod_artikel';

	function __construct()
	{
		// Call the Model constructor
		$this->db = $this->load->database('default', TRUE);
		parent::__construct();
	}

	function getAllEntries($limit=0, $offset=0)
	{
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_artikel);
		return $query->result();
	}

	function getAllEntriesMob()
	{
		$query = $this->db->get($this->tbl_artikel);
		return $query->result();
	}

	function getCountEntries()
	{
		 $query = $this->db->get($this->tbl_artikel);
		 return $query->num_rows(); 
	}

	function getSelectedPost($id)
	{
		$query = $this->db->get_where($this->tbl_artikel, array('id' => $id));
		return $query;
	}

}

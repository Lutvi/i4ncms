<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Produkmodel extends MY_Model {

	function __construct()
	{
		parent::__construct();
	}

/* Produk */

	function getListAktifProdukInduk()
	{
		$this->db->select('id_prod,nama_prod');
		$this->db->order_by('nama_prod','asc');
		$this->db->where('parent_id_prod',NULL);
		$this->db->where('status','on');
		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else {
			return NULL;
		}
	}
	
	function getProdukInduk($limit=0, $offset=0)
	{
		$this->db->select('id_prod,kode_prod,promo,id_prod_img,nama_prod,nama_prod_en,diskon,harga_prod,harga_spesial,stok,total_hits,status');
		$this->db->order_by('id_prod','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->where('parent_id_prod',NULL);
		$query = $this->db->get($this->tbl_produk);
		return $query->result();
	}

	function getProdukAnak($id_induk=0, $limit=0, $offset=0)
	{
		$this->db->order_by('id_prod','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->where('parent_id_prod',(int)$id_induk);
		$query = $this->db->get($this->tbl_produk);
		return $query->result();
	}

	function getCountProduk()
	{
		$query = $this->db->get($this->tbl_produk);
		return $query->num_rows(); 
	}
	
	function getCountIndukProduk()
	{
		$query = $this->db->get_where($this->tbl_produk, array('parent_id_prod' => NULL));
		return $query->num_rows();
	}

	function getCountAnakProduk($id_induk=0)
	{
		$this->db->where('parent_id_prod', (int)$id_induk);
		$query = $this->db->get($this->tbl_produk);
		return $query->num_rows(); 
	}

	function getIndukProduk($pid=NULL)
	{
		$this->db->select('id_prod,nama_prod,nama_prod_en');

		if ( ! empty($pid)){
			$this->db->where('parent_id_prod', NULL);
			$this->db->limit(1);
		}
		else {
			$this->db->where('status','on');
			$this->db->where('parent_id_prod', NULL);
		}

		$query = $this->db->get($this->tbl_produk);
		return $query->result();
	}

	function getPropertiIndukById($pid=0)
	{
		$this->db->select('nama_prod,nama_prod_en,kategori_id,harga_prod,harga_spesial,diskon,deskripsi,deskripsi_en');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function getAllPropertiIndukById($pid=0)
	{
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function getAllDetailProdById($pid='')
	{
		$this->db->select('nama_prod,nama_prod_en,kode_prod,kode_warna,harga_prod,harga_spesial,diskon');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			$query = $this->db->get_where($this->tbl_produk_lama, array('id_prod' => (int)$pid), 1);
			if ($query->num_rows() > 0)
				return $query->row();
			else
				return NULL;
		}
	}

	/* Insert */
	function cekNamaProduk($nama='',$bhs='id')
	{
		$this->db->select('nama_prod');
		if($bhs === 'id')
		$this->db->where('nama_prod',$nama);
		else
		$this->db->where('nama_prod_en',$nama);

		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function cekNamaProdukAnak($str='')
	{
		$induk = $this->getPropertiIndukById($this->input->post('parent_id_prod'));
		$nama = $induk->nama_prod.'-'.$str;

		$this->db->select('nama_prod');
		$this->db->where('nama_prod',$nama);
		$this->db->or_where('nama_prod_en',$nama);

		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function cekKodeProduk($str='')
	{
		$this->db->select('kode_prod');
		$this->db->where('kode_prod',$str);
		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function tambahProduk()
	{
		$kategori = $this->input->post('kategori');
		$tipe = $this->input->post('radio');

		if ($tipe === 'induk')
		{
			$data = array(
							'nama_prod' => ucfirst($this->input->post('nama_prod')),
							'nama_prod_en' => ucfirst($this->input->post('nama_prod_en')),
							'kode_prod' => trim($this->input->post('kode_prod')),
							'id_prod_img' => underscore('induk '.$this->input->post('nama_prod')),
							'promo' => $this->input->post('promo'),
							'kategori_id' => $kategori,
							'kode_warna' => ($this->input->post('kode-warnaInduk') !== '')?$this->input->post('kode-warnaInduk'):'',
							'harga_prod' => $this->input->post('harga'),
							'harga_spesial' => $this->input->post('harga_spesial'),
							'diskon' => ($this->input->post('diskon') !== '')?$this->input->post('diskon'):0,
							'stok' => $this->input->post('stok'),
							'deskripsi' => $this->input->post('deskripsi'),
							'deskripsi_en' => $this->input->post('deskripsi_en'),
							'ratting' => $this->input->post('ratting'),
							'votes' => mt_rand(1, 20),
							'tgl_update_admin' => '00000-00-00',
							'status' => $this->input->post('status')
						);
				
				
			if ( ! $this->db->insert($this->tbl_produk,$data))
			{
				return FALSE;
			}
			else {
				return TRUE;
			}

		}

		if($tipe === 'anak')
		{
			$induk = $this->getPropertiIndukById($this->input->post('parent_id_prod'));

			$data = array(
							'parent_id_prod' => $this->input->post('parent_id_prod'),
							'nama_prod' => $induk->nama_prod.'-'.ucfirst($this->input->post('nama_anak')),
							'nama_prod_en' => $induk->nama_prod_en.'-'.ucfirst($this->input->post('nama_anak_en')),
							'anak_untuk_jenis' => $this->input->post('anak_untuk'),
							'nama_jenis_anak' => ucfirst($this->input->post('nama_anak')),
							'nama_jenis_anak_en' => ucfirst($this->input->post('nama_anak_en')),
							'kode_prod' => $this->input->post('kode_prod_anak'),
							'id_prod_img' => underscore('anak '.$induk->nama_prod.' '.$this->input->post('nama_anak')),
							'promo' => $this->input->post('promo'),
							'kategori_id' => ($kategori !== '')?$kategori:$induk->kategori_id,
							'kode_warna' => ($this->input->post('kode-warnaAnak') !== '')?$this->input->post('kode-warnaAnak'):'',
							'harga_prod' => ($this->input->post('cek-harga-ikut-induk') === 'y')?$induk->harga_prod:$this->input->post('harga'),
							'harga_spesial' => ($this->input->post('cek-harga-ikut-induk') === 'y')?$induk->harga_spesial:$this->input->post('harga_spesial'),
							'diskon' => ($this->input->post('diskon') !== '')?$this->input->post('diskon'):$induk->diskon,
							'stok' => $this->input->post('stok'),
							'deskripsi' => ($this->input->post('cek-ikut-induk') !== 'y')?$this->input->post('deskripsi'):$induk->deskripsi,
							'deskripsi_en' => ($this->input->post('cek-ikut-induk') !== 'y')?$this->input->post('deskripsi_en'):$induk->deskripsi_en,
							'ratting' => $this->input->post('ratting'),
							'votes' => mt_rand(1, 20),
							'tgl_update_admin' => '00000-00-00',
							'status' => $this->input->post('status')
						);

			if ( ! $this->db->insert($this->tbl_produk,$data))
			{
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
	}

	function tambahProdukAnak()
	{
		$kategori = $this->input->post('kategori');
		$induk = $this->getPropertiIndukById($this->input->post('parent_id_prod'));

		$data = array(
						'parent_id_prod' => $this->input->post('parent_id_prod'),
						'nama_prod' => $induk->nama_prod.'-'.ucfirst($this->input->post('nama_anak')),
						'nama_prod_en' => $induk->nama_prod_en.'-'.ucfirst($this->input->post('nama_anak_en')),
						'anak_untuk_jenis' => $this->input->post('anak_untuk'),
						'nama_jenis_anak' => ucfirst($this->input->post('nama_anak')),
						'nama_jenis_anak_en' => ucfirst($this->input->post('nama_anak_en')),
						'kode_prod' => $this->input->post('kode_prod_anak'),
						'id_prod_img' => underscore('anak '.$induk->nama_prod.' '.$this->input->post('nama_anak')),
						'promo' => $this->input->post('promo'),
						'kategori_id' => ($kategori !== '')?$kategori:$induk->kategori_id,
						'kode_warna' => ($this->input->post('kode-warnaAnak') !== '')?$this->input->post('kode-warnaAnak'):'',
						'harga_prod' => ($this->input->post('harga') !== '0')?$this->input->post('harga'):$induk->harga_prod,
						'harga_spesial' => ($this->input->post('harga_spesial') !== '0')?$this->input->post('harga_spesial'):$induk->harga_spesial,
						'diskon' => ($this->input->post('diskon') !== '')?$this->input->post('diskon'):$induk->diskon,
						'stok' => $this->input->post('stok'),
						'deskripsi' => ($this->input->post('cek-ikut-induk') !== 'y')?$this->input->post('deskripsi'):$induk->deskripsi,
						'deskripsi_en' => ($this->input->post('cek-ikut-induk') !== 'y')?$this->input->post('deskripsi_en'):$induk->deskripsi_en,
						'ratting' => $this->input->post('ratting'),
						'votes' => mt_rand(1, 20),
						'tgl_update_admin' => '00000-00-00',
						'status' => $this->input->post('status') 
					);

		if ( ! $this->db->insert($this->tbl_produk,$data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	/* Update */
	function cekNamaProdukUpdate($nama='',$nama_ori='',$bhs='id')
	{
		$this->db->select('nama_prod');
		if($bhs === 'id')
		{
			$this->db->where('nama_prod',$nama);
			$this->db->where_not_in('nama_prod', $nama_ori);
		}
		else {
			$this->db->where('nama_prod_en',$nama);
			$this->db->where_not_in('nama_prod_en', $nama_ori);
		}

		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function cekNamaProdukAnakUpdate($str='',$nama_ori='',$bhs='id')
	{
		$induk = $this->getPropertiIndukById($this->input->post('parent_id_prod'));
		$nama = $induk->nama_prod.'-'.$str;
		$nama_ori = $induk->nama_prod.'-'.$nama_ori;

		$this->db->select('nama_prod');
		if($bhs === 'id')
		{
			$this->db->where('nama_prod',$nama);
			$this->db->where_not_in('nama_prod', $nama_ori);
		}
		else {
			$this->db->where('nama_prod_en',$nama);
			$this->db->where_not_in('nama_prod_en', $nama_ori);
		}

		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function cekKodeProdukUpdate($str='',$str_ori='')
	{
		$this->db->select('kode_prod');
		$this->db->where('kode_prod',$str);
		$this->db->where_not_in('kode_prod', $str_ori);
		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function updateProdukInduk()
	{
		$kategori = $this->input->post('kategori');

		$id = $this->input->post('id_prod');
		$data = array(
						'nama_prod' => ucfirst($this->input->post('nama_prod')),
						'nama_prod_en' => ucfirst($this->input->post('nama_prod_en')),
						'kode_prod' => $this->input->post('kode_prod'),
						'promo' => $this->input->post('promo'),
						'kategori_id' => $kategori,
						'kode_warna' => ($this->input->post('kode-warnaInduk') !== '')?$this->input->post('kode-warnaInduk'):'',
						'harga_prod' => $this->input->post('harga'),
						'harga_spesial' => $this->input->post('harga_spesial'),
						'diskon' => $this->input->post('diskon'),
						'stok' => $this->input->post('stok'),
						'deskripsi' => $this->input->post('deskripsi'),
						'deskripsi_en' => $this->input->post('deskripsi_en'),
						'ratting' => $this->input->post('ratting'),
						'tgl_update_admin' => date('Y-m-d H:i:s'),
						'di_update_oleh' => $this->session->userdata('idmu')
					);

		if (is_numeric($id))
		{
			$this->db->where('id_prod', $id);
			if ( ! $this->db->update($this->tbl_produk, $data))
			{
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		else {
			return FALSE;
		}
	}

	function updateProdukAnak($kategori='')
	{
		$kategori = $this->input->post('kategori');
		$id = $this->input->post('id_prod');

		$induk = $this->getPropertiIndukById($this->input->post('parent_id_prod'));
		$data = array(
						'nama_prod' => $induk->nama_prod.'-'.ucfirst($this->input->post('nama_anak')),
						'nama_prod_en' => $induk->nama_prod_en.'-'.ucfirst($this->input->post('nama_anak_en')),
						'nama_jenis_anak' => ucfirst($this->input->post('nama_anak')),
						'nama_jenis_anak_en' => ucfirst($this->input->post('nama_anak_en')),
						'kode_prod' => $this->input->post('kode_prod'),
						'promo' => $this->input->post('promo'),
						'kategori_id' => $kategori,
						'kode_warna' => ($this->input->post('kode-warna') !== '')?$this->input->post('kode-warna'):'',
						'harga_prod' => $this->input->post('harga'),
						'harga_spesial' => $this->input->post('harga_spesial'),
						'diskon' => $this->input->post('diskon'),
						'stok' => $this->input->post('stok'),
						'deskripsi' => $this->input->post('deskripsi'),
						'deskripsi_en' => $this->input->post('deskripsi_en'),
						'ratting' => $this->input->post('ratting'),
						'tgl_update_admin' => date('Y-m-d H:i:s'),
						'di_update_oleh' => $this->session->userdata('idmu')
					);

		if (is_numeric($id))
		{
			if ($this->db->update($this->tbl_produk, $data, array('id_prod' => $id)))
			{
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}

	function upProdukStat()
	{
		$data = array(
			   'promo' => $this->input->post('promo'),
			   'stok' => $this->input->post('stok'),
			   'status' => $this->input->post('status'),
			   'tgl_update_admin' => date('Y-m-d H:i:s')
			);

		if ($this->input->post('id_anak'))
		{
			$id = $this->input->post('prod_id');
			if ($this->db->update($this->tbl_produk, $data, array('id_prod' => (int)$id)))
			{
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			$id = $this->input->post('prod_id');
			if ( ! $this->db->update($this->tbl_produk, $data, array('id_prod' => (int)$id)))
			{
				return FALSE;
			}
			else {
				$data = array(
				   'status' => $this->input->post('status'),
				   'tgl_update_admin' => date('Y-m-d H:i:s')
				);
				if ($this->db->update($this->tbl_produk, $data, array('parent_id_prod' => (int)$id)))
				{
					return TRUE;
				}
				else {
					return FALSE;
				}
			}
		}
	}

	/* Delete */
	function hapusProduk($pid=0)
	{
		$sts = FALSE;

		$this->db->where('id_prod', (int)$pid);
		$this->db->or_where('parent_id_prod', (int)$pid);
		$query = $this->db->get($this->tbl_produk);

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				// cek di tabel order
				$data = array();
				$qorder = $this->db->get_where($this->tbl_order, array('id_prod' => $row->id_prod));
				$qorder_batal = $this->db->get_where($this->tbl_order_batal, array('id_prod' => $row->id_prod));
				$qorder_sukses = $this->db->get_where($this->tbl_order_sukses, array('id_prod' => $row->id_prod));

				if ($qorder->num_rows() > 0 || $qorder_batal->num_rows() > 0 || $qorder_sukses->num_rows() > 0)
				{
					$prod_img = time().'-'.$row->id_prod_img;
					$data = array(
							'id_prod' => $row->id_prod,'parent_id_prod' => $row->parent_id_prod,'nama_prod' => $row->nama_prod,
							'nama_prod_en' => $row->nama_prod_en,'anak_untuk_jenis' => $row->anak_untuk_jenis,
							'nama_jenis_anak' => $row->nama_jenis_anak,'nama_jenis_anak_en' => $row->nama_jenis_anak_en,
							'kode_prod' => $row->kode_prod,'id_prod_img' => $prod_img,
							'promo' => $row->promo,'kategori_id' => $row->kategori_id,'kode_warna' => $row->kode_warna,
							'harga_prod' => $row->harga_prod,'harga_spesial' => $row->harga_spesial,'diskon' => $row->diskon,
							'stok' => $row->stok,'total_hits' => $row->total_hits,
							'deskripsi' => $row->deskripsi,'deskripsi_en' => $row->deskripsi_en,
							'ratting' => $row->ratting,
							'votes' => $row->votes,
							'tgl_update_admin' => $row->tgl_update_admin,'status' => $row->status
						);
					$this->db->insert($this->tbl_produk_lama, $data);
					$this->hapusAllImgByIdProdImg($row->id_prod_img, TRUE, $prod_img);

					if ($row->parent_id_prod === $pid)
					{
						if ($this->db->delete($this->tbl_produk, array('id_prod' => $row->id_prod)))
							$sts = TRUE;
					} else {
						$this->upHalamanProduk($pid);
						if ($this->db->delete($this->tbl_produk, array('id_prod' => (int)$pid)))
							$sts = TRUE;
					}
				}
				else {
					$this->hapusAllImgByIdProdImg($row->id_prod_img);
					if ($row->parent_id_prod === $pid)
					{
						if ($this->db->delete($this->tbl_produk, array('id_prod' => $row->id_prod)))
							$sts = TRUE;
					} else {
						$this->upHalamanProduk($pid);
						if ($this->db->delete($this->tbl_produk, array('id_prod' => (int)$pid)))
							$sts = TRUE;
					}
				}
			}

			//hapus menu - halaman
			$this->db->select('id_halaman');
			$halaman = $this->db->get_where($this->tbl_halaman,array('id_produk' => (int)$pid));
			if($halaman->num_rows() > 0)
			{
				foreach( $halaman->result() as $hal)
				{
					// hapus menu
					$menu = $this->db->get_where($this->tbl_menu,array('link'=>$hal->id_halaman));
					if($menu->num_rows() > 0)
					{
						foreach( $menu->result() as $mnu)
						{
							$this->hapusMenu($mnu->id);
						}
					}
				}
			}

		}

		return $sts;
	}

	function upHalamanProduk($pid=0)
	{
		// update tbl halaman jika produk terpilih
		$qhl = $this->db->get_where($this->tbl_halaman, array('id_produk' => (int)$pid));
		if ($qhl->num_rows() > 0)
		{
			$hl = $qhl->row();
			$hlid = $hl->id_halaman;
			// hapus menu
			$menu = $this->db->get_where($this->tbl_menu,array('link'=>$hlid));
			if($menu->num_rows() > 0)
			{
				$mnu = $menu->row();
				$id_menu = $mnu->id;
				$this->cms->hapusMenu($id_menu);
			}
		}
	}

/* End Produk */
	
	function getAllImgAdminById($id_img='')
	{
		$this->db->select('id_img,img_src,alt_text_img');
		$this->db->order_by('alt_text_img','asc');
		$query = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_img));
		return $query->result();
	}

	function getImgDetailProdById($id_prod='')
	{
		$this->db->select('id_prod_img');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => $id_prod), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			$id_prod_img = $row->id_prod_img;
			return $this->getImgDetailProd($id_prod_img);
		}
		else {
			$this->db->select('id_prod_img');
			$query2 = $this->db->get_where($this->tbl_produk_lama, array('id_prod' => $id_prod), 1);
			if ($query2->num_rows() > 0)
			{
				$row = $query2->row();
				$id_prod_img = $row->id_prod_img;
				return $this->getImgDetailProd($id_prod_img);
			}
			else {
				return NULL;
			}
		}
	}

	function getImgDetailProd($id_prod_img='')
	{
		$this->db->select('img_src');
		$imgquery = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_prod_img), 1);
		if ($imgquery->num_rows() > 0)
		{
			$imgrow = $imgquery->row();
			return $imgrow->img_src;
		}
		else {
			$imglamaquery = $this->db->get_where($this->tbl_img_produk_lama, array('id_prod_img' => $id_prod_img), 1);
			if ($imglamaquery->num_rows() > 0)
			{
				$imglamarow = $imglamaquery->row();
				return $imglamarow->img_src;
			}
			else {
				return NULL;
			}
		}
	}
	
	function getImgByProdId($id_img='')
	{
		$this->db->select('id_prod_img');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => $id_img), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			$id_prod_img = $row->id_prod_img;
			//return $id_prod_img;
			$this->db->select('img_src');
			//$this->db->order_by('id_prod_img','asc');
			$imgquery = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_prod_img), 1);
			if ($imgquery->num_rows() > 0)
			{
			   $imgrow = $imgquery->row();
			   return $imgrow->img_src;
			}
			else {
				return NULL;
			}
		}
		else {
			return NULL;
		}
	}
	
	function getImgById($id_img='')
	{
		$this->db->select('img_src');
		$this->db->order_by('id_prod_img','asc');
		$query = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_img), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->img_src;
		}
		else {
			return NULL;
		}
	}

	function getAltImgById($id_img='')
	{
		$this->db->select('alt_text_img');
		$this->db->order_by('id_prod_img','asc');
		$query = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_img), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return substr($row->alt_text_img,11);
		}
		else {
			return NULL;
		}
	}

	function tambahImgProduk($fnm='')
	{
		$tipe = $this->input->post('radio');
		
		if ($tipe === 'induk')
		{
			if ($this->input->post('nama_prod'))
			{
				$id_img = underscore('induk_'.$this->input->post('nama_prod'));
			}
			else {
				$id_img = $this->input->post('id_prod_img');
			}
		}

		if ($tipe === 'anak')
		{
			if ($this->input->post('nama_anak'))
			{
				$induk = $this->getPropertiIndukById(trim($this->input->post('parent_id_prod')));
				$id_img = underscore('anak_'.$induk->nama_prod.'_'.$this->input->post('nama_anak'));
			}
			else {
				$id_img = $this->input->post('id_prod_img');
			}
		}
			$alt = explode(".", $fnm);
			$alt_img = substr($alt[0], 11);
			
			$data = array(
						'id_prod_img' => $id_img,
						'alt_text_img' => humanize($alt_img),
						'img_src' => $fnm
					);
				
		if ( ! $this->db->insert($this->tbl_img_produk,$data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function hapusAllImgByIdProdImg($id_prod_img, $sisa = FALSE, $prod_img = '')
	{
		$query = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_prod_img));
		if ($query->num_rows() > 0)
		{
			$sts = FALSE;
			foreach ($query->result() as $key => $row)
			{
				if ($key === 0 && $sisa === TRUE) 
				{
					$data = array(
							'id_img' => $row->id_img,
							'tgl_update' => $row->tgl_update,
							'id_prod_img' => $prod_img,
							'alt_text_img' => $row->alt_text_img,
							'img_src' => $row->img_src
							);
					if ($this->db->insert($this->tbl_img_produk_lama, $data))
					{
						if ($this->db->delete($this->tbl_img_produk, array('id_img' => $row->id_img)))
						{
							$sts = TRUE;
						} else {
							$sts = FALSE;
						}
							//hapus foto besarnya saja
							hapus_file("./_produk/large/large_". $row->img_src);
					} else {
						$sts = FALSE;
					}
				}
				else {
					//hapus foto
					hapus_file("./_produk/large/large_". $row->img_src);
					hapus_file("./_produk/medium/medium_". $row->img_src);
					hapus_file("./_produk/small/small_". $row->img_src);
					hapus_file("./_produk/thumb/thumb_". $row->img_src);
					hapus_file("./_produk/thumb/small_thumb_". $row->img_src);

					if ($this->db->delete($this->tbl_img_produk, array('id_img' => $row->id_img)))
						$sts = TRUE;
					else
						$sts = FALSE;
				}
			}
			return $sts;
		}
		else {
			return TRUE;
		}
	} 

	function hapusByImgId($id)
	{
		$query = $this->db->get_where($this->tbl_img_produk, array('id_img' => $id), 1);
		if ($query->num_rows() > 0 && is_numeric($id))
		{
			$row = $query->row();
			$row->alt_text_img;

			//hapus foto
			hapus_file("./_produk/large/large_". $row->img_src);
			hapus_file("./_produk/medium/medium_". $row->img_src);
			hapus_file("./_produk/small/small_". $row->img_src);
			hapus_file("./_produk/thumb/thumb_". $row->img_src);
			hapus_file("./_produk/thumb/small_thumb_". $row->img_src);
   
			if ( ! $this->db->delete($this->tbl_img_produk, array('id_img' => $id)))
				return FALSE;
			else
				return TRUE;
		}
		else {
			return FALSE;
		}
	}	
/* End Img Produk */

}

<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Pelangganmodel extends MY_Model {

	function __construct()
	{
		parent::__construct();
	}

	/* SELECT */
	function getAllPelanggan($limit=0, $offset=0)
	{
		$this->db->select('id,tgl_daftar,tgl_update_data,id_cust_uname,nama_lengkap,email_cust,no_telpon,status_cust,status_rss,cust_level,jml_transaksi,total_transaksi,jumlah_transaksi_gagal,total_transaksi_gagal,jml_poin,prioritas_cust');
		$this->db->order_by('id','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$query = $this->db->get($this->tbl_pembeli);
		return $query->result();
	}

	function getCountAllPelanggan()
	{
		$this->db->select('id');
		$query = $this->db->get($this->tbl_pembeli);
		return $query->num_rows();
	}

	function getPelangganById($pid=0)
	{
		$this->db->where('id',(int)$pid);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_pembeli);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	function getNamaPelangganById($id_cust=0)
	{
		$this->db->select('nama_lengkap');
		$this->db->where('id',(int)$id_cust);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_pembeli);
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return dekodeString($row->nama_lengkap);
		}
		else {
			return NULL;
		}
	}

	function getEmailPelangganById($id_cust=0)
	{
		$this->db->select('email_cust');
		$this->db->where('id',(int)$id_cust);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_pembeli);
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return dekodeString($row->email_cust);
		}
		else {
			return NULL;
		}
	}
	/* End SELECT */

	/* Update */
	function upStatPelangganById($cid=0)
	{
		$data = array (
							'status_cust' => $this->input->post('status'),
							'status_rss' => $this->input->post('rss'),
							'prioritas_cust' => $this->input->post('prioritas')
						);
		if ( ! $this->db->update($this->tbl_pembeli,$data, array('id' => (int)$cid)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function upDetailPelangganById($cid=0)
	{
		$nama = trim($this->input->post('nama_lengkap'));
		$cust_email = trim($this->input->post('email'));
		$no_telpon = trim($this->input->post('no_telpon'));
		$kode_pos = trim($this->input->post('kode_pos'));
		$alamat_rumah = trim(str_ireplace("\\n", "<br />",$this->input->post('alamat_rumah')));

		/* enkripsi */
		$ec_email = enkodeString(trim($cust_email));
		$ec_nama = enkodeString($nama);
		$ec_tlp = enkodeString($no_telpon);
		$ec_kode_pos = enkodeString($kode_pos);
		$ec_alamat = enkodeString(trim($alamat_rumah));

		$id_negara = trim($this->input->post('negara'));
		$id_provinsi = trim($this->input->post('provinsi'));
		$id_wilayah = trim($this->input->post('wilayah'));

		$data = array (
							'status_cust' => $this->input->post('status_cust'),
							'status_rss' => $this->input->post('status_rss'),
							'prioritas_cust' => $this->input->post('prioritas_cust'),
							'nama_lengkap' => $ec_nama,
							'email_cust' => $ec_email,
							'no_telpon' => $ec_tlp,
							'id_negara' => $id_negara,
							'id_provinsi' => $id_provinsi,
							'id_wilayah' => $id_wilayah,
							'kode_pos' => $ec_kode_pos,
							'alamat_rumah' => $ec_alamat
						);
		if ( ! $this->db->update($this->tbl_pembeli, $data, array('id' => (int)$cid)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	/* End Update */

	/* Delete */
	function hapusPelanggan($cid='')
	{
		$this->db->where('id_cust',(int)$cid);
		$query = $this->db->get($this->tbl_order);
		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else {
			if ( ! $this->db->delete($this->tbl_pembeli, array('id' => (int)$cid)))
			return FALSE;
			else
			return TRUE;
		}
	}
	/* End Delete */

}

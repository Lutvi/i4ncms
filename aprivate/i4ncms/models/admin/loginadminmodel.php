<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class LoginAdminmodel extends MY_Model {

    var $tbl_adm = 'tbl_sys_admin_cms';
    var $col_id = 'id';
    var $col_nama = 'nama_lengkap';
    var $col_uname = 'id_adm_uname';
    var $col_pass = 'adm_kunci_pas';
    var $col_email = 'adm_email';
    var $col_foto = 'foto';
    var $col_level = 'level';
    var $col_status = 'status';
    var $col_pembuat = 'pembuatnya';
    var $col_biografi = 'biografi';
    
    function __construct()
    {
        parent::__construct();
    }

    function cekAdmLogin()
    {
        $query = $this->db->get($this->tbl_adm);

        $cek = FALSE;
        if($query->num_rows() > 0)
        {
			foreach($query->result_array() as $user) {
				if(preg_match("/\b".dekodeString($user[$this->col_pass])."\b/i", trim($this->input->post('knci', TRUE))) && preg_match("/\b".dekodeString($user[$this->col_uname])."\b/i", trim($this->input->post('uid', TRUE))))
				{
					if(dekodeString($user[$this->col_status]) === "on")
					{
						// buat session
						$admdata = array(
							'level' => $user[$this->col_level],
							'nama_admin' => dekodeString($user[$this->col_nama]),
							'kunci_adm' => $user[$this->col_email],
							'idses' => $user[$this->col_uname],
							'idmu' => bs_kode($user[$this->col_id]),
							'admkunci'  => $user[$this->col_pass],
							'admemail'  => $user[$this->col_email],
							'ademinaja' => enkodeString($this->input->post('uid', TRUE)),
							'admusrid'  => bs_kode($this->input->post('uid', TRUE))
						);

						if(bs_kode($user[$this->col_level], TRUE) === "superman")
						$admdata['superman'] = bs_kode('letsflying');
						if(bs_kode($user[$this->col_level], TRUE) === "batman")
						$admdata['batman'] = bs_kode('spyingthis');
						if(bs_kode($user[$this->col_level], TRUE) === "ironman")
						$admdata['ironman'] = bs_kode('flashingtheair');
						if(bs_kode($user[$this->col_level], TRUE) === "spiderman")
						$admdata['spiderman'] = bs_kode('makesomebase');

						$this->session->set_userdata($admdata);
						$this->upTglLogin($user[$this->col_id]);
						return TRUE;
						break;
					}
					else {
						return 'banned';
						break;
					}
				}
				else {
					$cek = FALSE;
				}
			}
        }
        else {
             return FALSE;
        }
        return $cek;
    }

/* SELECT */
    function cekAdaUname($str='')
    {
        $this->db->select($this->col_uname);
        $query = $this->db->get($this->tbl_adm);

        $cek = TRUE;
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $user) {
                if(preg_match("/\b".dekodeString($user[$this->col_uname])."\b/i", trim($str)))
                {
                   return FALSE;
                   break;
                }
            }
        }else{
            $cek = TRUE;
        }

        return $cek;
    }

    function cekAdaEmail($str='')
    {
        $this->db->select($this->col_email);
        $query = $this->db->get($this->tbl_adm);

        $cek = TRUE;
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $email) {
                if(preg_match("/\b".dekodeString($email[$this->col_email])."\b/i", trim($str)))
                {   
                   return FALSE;
                   break;
                }
            }
        }else{
            $cek = TRUE;
        }

        return $cek;
    }

    function cekUpAdaUname($str='',$ori='')
    {
        $this->db->select($this->col_uname);
        $query = $this->db->get($this->tbl_adm);

        $cek = TRUE;
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $user) {
                if(preg_match("/\b".dekodeString($user[$this->col_uname])."\b/i", trim($str)))
                {
				   if(preg_match("/\b".dekodeString($user[$this->col_uname])."\b/i", trim($ori)))
				   $cek = TRUE;
                   else
                   $cek = FALSE;
                }
            }
        }
        else {
            $cek = TRUE;
        }

        return $cek;
    }

    function cekUpAdaEmail($str='',$ori='')
    {
        $this->db->select($this->col_email);
        $query = $this->db->get($this->tbl_adm);

        $cek = TRUE;
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $email) {
                if(preg_match("/\b".dekodeString($email[$this->col_email])."\b/i", trim($str)))
                {
				   if(preg_match("/\b".dekodeString($email[$this->col_email])."\b/i", trim($ori)))
                   $cek = TRUE;
                   else
                   $cek = FALSE;
                }
            }
        }
        else {
            $cek = TRUE;
        }

        return $cek;
    }
    
	function getAllAdmin($limit=0, $offset=0)
	{
		if (bs_kode($this->session->userdata('level'), TRUE) == 'superman')
		{
			if(!$this->_cekIsOptimusPrime()) {
				$this->db->where($this->col_level.' != ', bs_kode('superman'));
				$this->db->or_where($this->col_pembuat, $this->session->userdata('idmu'));
			}
			else{
				$this->db->where($this->col_uname.' != ', $this->session->userdata('idses'));
			}
		}
		else {
			$this->db->where($this->col_pembuat, $this->session->userdata('idmu'));
			$this->db->where($this->col_uname.' != ', $this->session->userdata('idses'));
		}

		$this->db->order_by('tgl_masuk','desc');

		$this->db->limit((int)$limit, (int)$offset);
		$query = $this->db->get($this->tbl_adm);
		return $query->result();
	}

	function getCountAllAdmin()
	{
		$this->db->select('id');
		if (bs_kode($this->session->userdata('level'), TRUE) == 'superman')
		{
			if(!$this->_cekIsOptimusPrime()) {
				$this->db->where($this->col_level.' != ', bs_kode('superman'));
				$this->db->or_where($this->col_pembuat, $this->session->userdata('idmu'));
			}
			else{
				$this->db->where($this->col_uname.' != ', $this->session->userdata('idses'));
			}
		}
		else {
			$this->db->where($this->col_pembuat, $this->session->userdata('idmu'));
			$this->db->where($this->col_uname.' != ', $this->session->userdata('idses'));
		}
		$query = $this->db->get($this->tbl_adm);
		return $query->num_rows();
	}

	function _cekIsOptimusPrime(){
		$this->db->select('id,pembuatnya');
		$this->db->where($this->col_id, bs_kode($this->session->userdata('idmu'),true));
		$this->db->where($this->col_uname, $this->session->userdata('idses'));
		$query = $this->db->get($this->tbl_adm,1);
		if($query->num_rows() > 0 ) {
			$row = $query->row();
			if( $row->pembuatnya == bs_kode('danioreiro'))
			return TRUE;
			else
			return FALSE;
		} else {
			return FALSE;
		}
	}

/* Fitur Cari AdminCMS */
	function getCountCariAdmin($cari='')
	{
		$cari = humanize(format_string_aman($cari));
		$hasil = array();
		
		$this->db->select('id,nama_lengkap');
		if (bs_kode($this->session->userdata('level'), TRUE) == 'superman')
		{
			$this->db->where($this->col_level.' != ', bs_kode('superman'));
			$this->db->or_where($this->col_pembuat, $this->session->userdata('idmu'));
		}
		else {
			$this->db->where($this->col_pembuat, $this->session->userdata('idmu'));
			$this->db->where($this->col_uname.' != ', $this->session->userdata('idses'));
		}

		$daftar = $this->db->get($this->tbl_adm);
		foreach($daftar->result() as $row)
		{
			if(preg_match("/$cari/i", dekodeString($row->nama_lengkap)))
			$hasil[] = $row->id;
		}
		
		return count($hasil);
	}

	function getCariAdmin($cari='',$limit=0, $offset=0)
	{
		$cari = humanize(format_string_aman($cari));
		$hasil = array();
		
		$this->db->select('id,nama_lengkap');
		if (bs_kode($this->session->userdata('level'), TRUE) == 'superman')
		{
			$this->db->where($this->col_level.' != ', bs_kode('superman'));
			$this->db->or_where($this->col_pembuat, $this->session->userdata('idmu'));
		}
		else {
			$this->db->where($this->col_pembuat, $this->session->userdata('idmu'));
			$this->db->where($this->col_uname.' != ', $this->session->userdata('idses'));
		}

		$daftar = $this->db->get($this->tbl_adm);
		foreach($daftar->result() as $row)
		{
			if(preg_match("/$cari/i", dekodeString($row->nama_lengkap)))
			$hasil[] = $row->id;
		}

		if(count($hasil) > 0)
		{
			$this->db->where_in('id', array_values($hasil));
			$this->db->order_by('tgl_masuk','desc');
			$this->db->limit((int)$limit, (int)$offset);
			$query = $this->db->get($this->tbl_adm);
			return $query->result();
		}
		else
			return NULL;
	}
/* End Fitur Cari AdminCMS */

	function getAdminById($id=0)
	{
		$this->db->where('id',(int)$id);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_adm);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}
/* End SELECT */

/* TAMBAH */
	function addAdminCms()
	{
		$foto = $this->upload->file_name;
		$data = array(
					$this->col_nama => enkodeString($this->input->post('nama_lengkap', TRUE)),
					$this->col_uname => enkodeString($this->input->post('adm_id', TRUE)),
					$this->col_pass => enkodeString($this->input->post('adm_kunci', TRUE)),
					$this->col_email => enkodeString($this->input->post('adm_email', TRUE)),
					$this->col_foto => $foto,
					$this->col_level => $this->input->post('level', TRUE),
					$this->col_status => enkodeString('on'),
					$this->col_pembuat => $this->input->post('id_pembuat', TRUE),
					$this->col_biografi => bs_kode($this->input->post('biografi', TRUE))
				);

		if ( ! $this->db->insert($this->tbl_adm,$data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
/* END TAMBAH */

/* UPDATE */
	function upProfilAdminForm()
	{
		$id = bs_kode($this->input->post('id_admin'), TRUE);
		if($this->input->post('status_ganti') == 'yes')
        {
			$foto = $this->upload->file_name;
			$data = array(
						$this->col_nama => enkodeString($this->input->post('nama_lengkap', TRUE)),
						$this->col_uname => enkodeString($this->input->post('adm_id', TRUE)),
						$this->col_pass => enkodeString($this->input->post('adm_kunci', TRUE)),
						$this->col_email => enkodeString($this->input->post('adm_email', TRUE)),
						$this->col_foto => $foto,
						$this->col_biografi => bs_kode($this->input->post('biografi', TRUE))
					);
		}
		else {
			$data = array(
						$this->col_nama => enkodeString($this->input->post('nama_lengkap', TRUE)),
						$this->col_uname => enkodeString($this->input->post('adm_id', TRUE)),
						$this->col_pass => enkodeString($this->input->post('adm_kunci', TRUE)),
						$this->col_email => enkodeString($this->input->post('adm_email', TRUE)),
						$this->col_biografi => bs_kode($this->input->post('biografi', TRUE))
					);
		}

		$this->db->where($this->col_id, (int)$id);
        if( ! $this->db->update($this->tbl_adm, $data))
		{
			return FALSE;
		}
		else {
			//hapus gambar
			if($this->input->post('status_ganti') == 'yes')
			{
				hapus_file("./_media/admincms/medium/medium_". $this->input->post('gambar'));
				hapus_file("./_media/admincms/thumb/thumb_". $this->input->post('gambar'));
			}
			return TRUE;
		}
	}
	
	function upAdminCmsForm()
	{
		$id = bs_kode($this->input->post('id_admin'), TRUE);
		if($this->input->post('status_ganti') == 'yes')
        {
			$foto = $this->upload->file_name;
			$data = array(
						$this->col_nama => enkodeString($this->input->post('nama_lengkap', TRUE)),
						$this->col_uname => enkodeString($this->input->post('adm_id', TRUE)),
						$this->col_pass => enkodeString($this->input->post('adm_kunci', TRUE)),
						$this->col_email => enkodeString($this->input->post('adm_email', TRUE)),
						$this->col_foto => $foto,
						$this->col_level => $this->input->post('level', TRUE),
						$this->col_status => enkodeString('on'),
						$this->col_biografi => bs_kode($this->input->post('biografi', TRUE))
					);
		}
		else {
			$data = array(
						$this->col_nama => enkodeString($this->input->post('nama_lengkap', TRUE)),
						$this->col_uname => enkodeString($this->input->post('adm_id', TRUE)),
						$this->col_pass => enkodeString($this->input->post('adm_kunci', TRUE)),
						$this->col_email => enkodeString($this->input->post('adm_email', TRUE)),
						$this->col_level => $this->input->post('level', TRUE),
						$this->col_status => enkodeString('on'),
						$this->col_biografi => bs_kode($this->input->post('biografi', TRUE))
					);
		}

		$this->db->where($this->col_id, $id);
        if( ! $this->db->update($this->tbl_adm, $data))
		{
			return FALSE;
		}
		else {
			//hapus gambar
			if($this->input->post('status_ganti') == 'yes')
			{
				hapus_file("./_media/admincms/medium/medium_". $this->input->post('gambar'));
				hapus_file("./_media/admincms/thumb/thumb_". $this->input->post('gambar'));
			}
			return TRUE;
		}
	}

	function upTglLogin($id=0)
	{
		$data = array(
               'tgl_masuk' => time()
            );
        $this->db->where('id', (int)$id);
        if ( ! $this->db->update($this->tbl_adm, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

	function upAdminStat()
	{
		$data = array(
               'status' => enkodeString($this->input->post('status', TRUE))
            );
        $id = $this->input->post('id', TRUE);
        $this->db->where('id', (int)$id);
        if ( ! $this->db->update($this->tbl_adm, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
	}
/* END UPDATE */

/* HAPUS */
	function hapusAdminCms()
	{
		$id = $this->input->post('id_hps', TRUE);
		// hapus id
		if ( ! $this->db->delete($this->tbl_adm, array('id' => (int)$id)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
/* End HAPUS */

}

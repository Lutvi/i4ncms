<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Newslettermodel extends MY_Model {

    
    function __construct()
    {
        parent::__construct();
    }

    function getAllUserRss($limit, $offset)
    {
		$this->db->select('id,nama_lengkap,email_cust,total_transaksi,kode_kupon,jml_poin,prioritas_cust');
		$this->db->limit($limit, $offset);
		$query = $this->db->get_where($this->tbl_pembeli, array('status_rss' => 'on'));
		return $query->result();
    }

    function getCountNewsletter()
    {
		$query = $this->db->get_where($this->tbl_pembeli, array('status_rss' => 'on'));
        return $query->num_rows(); 
    }
    
}

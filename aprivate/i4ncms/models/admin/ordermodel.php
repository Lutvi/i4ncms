<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Ordermodel extends MY_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
/* Orderan */
	/* Select */
	function getFrontAllOrderanByTrx($trx='',$email=FALSE)
	{
		if($email)
		$this->db->select('id_cust,id_alamat,no_struk,tgl_transaksi,metode_bayar,bayar_melalui,detail_produk,jml_pesan,total_harga,status_transaksi');

		$this->db->order_by('total_harga','desc');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order);
		if($query->num_rows > 0)
		{
			if($email)
			return $query->result_array();
			else
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	/* PROSES */
	function getAllOrderanProses($limit=0, $offset=0, $proses='pembayaran')
	{
		settype($offset, 'integer');
		settype($limit, 'integer');
		
		if($proses === 'batal')
		$this->db->where('status_orderan','batal');
		else
		$this->db->where('status_orderan','proses');
		
		if ( $proses === 'pembayaran')
		$this->db->where('status_transaksi','pembayaran');
		if ( $proses === 'pending')
		$this->db->where('status_transaksi','pending');
		if ( $proses === 'pemaketan')
		$this->db->where('status_transaksi','pemaketan');
		if ( $proses === 'pengiriman')
		$this->db->where('status_transaksi','pengiriman');
		
		
		$this->db->order_by('status_transaksi','desc');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order);
		
		return $query->result();
	}
	
	function getCountOrderProses($proses = 'pembayaran')
	{
		if($proses === 'batal')
		$this->db->where('status_orderan','batal');
		else
		$this->db->where('status_orderan','proses');
		
		if($proses === 'pembayaran')
		$this->db->where('status_transaksi','pembayaran');
		if($proses === 'pending')
		$this->db->where('status_transaksi','pending');
		if($proses === 'pemaketan')
		$this->db->where('status_transaksi','pemaketan');
		if($proses === 'pengiriman')
		$this->db->where('status_transaksi','pengiriman');
		
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order);
		return $query->num_rows(); 
	}
	
	function getAllOrderanByTrx($trx='')
	{
		$this->db->order_by('total_harga','desc');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order);
		return $query->result();
	}
	
	function getCountOrderanByTrx($trx='')
	{
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order);
		return $query->num_rows();
	}
	
	function getTotalOrderanByTrx($trx='')
	{
		$this->db->select('count(no_struk) as jml_produk');
		$this->db->select_sum('jml_pesan');
		$this->db->select_sum('total_harga');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order);
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		} 
	}
	/* End PROSES */
	
	/* BATAL */
	function getAllOrderanBatal($limit=0, $offset=0)
	{
		$this->db->where('status_orderan','batal');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->group_by('no_struk','desc'); 
		$query = $this->db->get($this->tbl_order_batal);
		return $query->result();
	}
	
	function getCountOrderBatal()
	{
		$this->db->where('status_orderan','batal');
		$this->db->order_by('status_transaksi','desc');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order_batal);
		return $query->num_rows(); 
	}
	
	function getAllOrderanBatalByTrx($trx='')
	{
		$this->db->order_by('total_harga','asc');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order_batal);
		return $query->result();
	}
	
	function getCountOrderanBatalByTrx($trx='')
	{
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order_batal);
		return $query->num_rows();
	}
	
	function getTotalOrderanBatalByTrx($trx='')
	{
		$this->db->select('count(no_struk) as jml_produk');
		$this->db->select_sum('jml_pesan');
		$this->db->select_sum('total_harga');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order_batal);
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		} 
	}
	/* END BATAL */
	
	/* SUKSES */
	function getAllOrderanSukses($limit=0, $offset=0)
	{
		$this->db->where('status_orderan','sukses');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->limit((int)$limit, (int)$offset);
		$this->db->group_by('no_struk','desc'); 
		$query = $this->db->get($this->tbl_order_sukses);
		return $query->result();
	}
	
	function getCountOrderSukses()
	{
		$this->db->where('status_orderan','sukses');
		$this->db->order_by('status_transaksi','desc');
		$this->db->order_by('tgl_transaksi','desc');
		$this->db->group_by('no_struk','desc');
		$query = $this->db->get($this->tbl_order_sukses);
		return $query->num_rows(); 
	}
	
	function getAllOrderanSuksesByTrx($trx='')
	{
		$this->db->order_by('total_harga','asc');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order_sukses);
		return $query->result();
	}
	
	function getCountOrderanSuksesByTrx($trx='')
	{
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order_sukses);
		return $query->num_rows();
	}
	
	function getTotalOrderanSuksesByTrx($trx='')
	{
		$this->db->select('count(no_struk) as jml_produk');
		$this->db->select_sum('jml_pesan');
		$this->db->select_sum('total_harga');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order_sukses);
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		} 
	}
	/* END SUKSES */
	
	function cekStokProdukById($pid=0)
	{
		$this->db->select('stok');
		$this->db->where('id_prod', (int)$pid);
		$query = $this->db->get($this->tbl_produk);
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return $row->stok;
		}
		else {
			return NULL;
		}
	}
	/* End Select */

	/* Update TRX */
	function upTrxStatusTrans($trx='', $sts_trans='')
	{
		$this->db->where('no_struk',$trx);
		$qry = $this->db->get($this->tbl_order);
		if($qry->num_rows > 0)
		{
			foreach ($qry->result() as $row)
			{
				$data = array ( 
								'status_transaksi' => $sts_trans,
								'status_orderan' => ($sts_trans === 'sukses')?'sukses':'proses'
								 );

				if( $sts_trans === 'sukses' )
				{
					$data_sukses = array(
						'id' => $row->id,
						'no_resi_pengiriman' => $row->no_resi_pengiriman,
						'id_cust' => $row->id_cust,
						'id_alamat' => $row->id_alamat,
						'no_struk' => $row->no_struk,
						'metode_bayar' => $row->metode_bayar,
						'bayar_melalui' => $row->bayar_melalui,
						'id_prod' => $row->id_prod,
						'detail_produk' => $row->detail_produk,
						'tgl_transaksi' => $row->tgl_transaksi,
						'jml_pesan' => $row->jml_pesan,
						'total_harga' => $row->total_harga,
						'notes_cust' => $row->notes_cust
					);
					
					if( ! $this->db->insert($this->tbl_order_sukses,$data_sukses))
					{
						$sts = FALSE;
					}
					else {
						if( ! $this->db->delete($this->tbl_order, array('id' => $row->id)))
						{
							$sts = FALSE;
						} else {
							$sts = TRUE;
						}
					}
				}
				else {
					if( ! $this->db->update($this->tbl_order, $data, array('id' => $row->id)))
					{
						$sts = FALSE;
					}
					else {
						$sts = TRUE;
					}
				}
			}
			
			return $sts;
		}
		else {
			return TRUE;
		}
	}
	
	function upTrxStatusOrder($trx='', $sts_trans='', $sts_order='', $no_resi='')
	{
		$this->db->where('no_struk',$trx);
		$qry = $this->db->get($this->tbl_order);
		if($qry->num_rows > 0)
		{
			foreach ($qry->result() as $row)
			{
				$data = array ( 
								'no_resi_pengiriman' => ucwords($no_resi),
								'status_transaksi' => ($sts_order === 'batal')?'pending':$sts_trans,
								'status_orderan' => ($sts_trans === 'sukses' && $sts_order !== 'batal')?'sukses':$sts_order
								 );
								 
				if( $sts_trans === 'sukses' && $sts_order !== 'batal' )
				{
					$data_sukses = array(
						'id' => $row->id,
						'no_resi_pengiriman' => ucwords($no_resi),
						'id_cust' => $row->id_cust,
						'id_alamat' => $row->id_alamat,
						'no_struk' => $row->no_struk,
						'metode_bayar' => $row->metode_bayar,
						'bayar_melalui' => $row->bayar_melalui,
						'id_prod' => $row->id_prod,
						'detail_produk' => $row->detail_produk,
						'tgl_transaksi' => $row->tgl_transaksi,
						'jml_pesan' => $row->jml_pesan,
						'total_harga' => $row->total_harga,
						'notes_cust' => $row->notes_cust
					);
					
					if( ! $this->db->insert($this->tbl_order_sukses,$data_sukses))
					{
						$sts = FALSE;
					}
					else {
						if( ! $this->db->delete($this->tbl_order, array('id' => $row->id)))
						{
							$sts = FALSE;
						} else {
							$sts = TRUE;
						}
					}
				}
				else {
					if( ! $this->db->update($this->tbl_order, $data, array('id' => $row->id)))
					{
						$sts = FALSE;
					}
					else {
						$sts = TRUE;
					}
				}
			}
			
			return $sts;
		}
		else {
			return TRUE;
		}
	}
	
	function upTrxBatalTransaksi($trx='')
	{
		$sts = TRUE;

		$this->db->where('no_struk',$trx);
		$qry = $this->db->get($this->tbl_order);
		if($qry->num_rows > 0)
		{
			foreach ($qry->result() as $row)
			{
				$data = array ( 
								'status_transaksi' => 'pending',
								'status_orderan' => 'batal'
								 );
				if( ! $this->db->update($this->tbl_order, $data, array('id' => $row->id)))
				{
					$sts = FALSE;
				}
			}
			
			return $sts;
		}
	}
	/* End SET Order */
	

	/* Delete */
	function hapusOrderBatal($trx='',$cid='',$jml_trans='',$ttl_trans='')
	{
		//$this->db->select('id_prod,jml_pesan');
		$this->db->where('no_struk',$trx);
		$query = $this->db->get($this->tbl_order);
		if($query->num_rows > 0)
		{
			$stat = TRUE;
			foreach ($query->result() as $row)
			{
				// Update Stok Produk
				$data = array();
				$this->db->select('stok');
				$qprod = $this->db->get_where($this->tbl_produk, array('id_prod' => $row->id_prod));
				if ($qprod->num_rows() > 0)
				{
					$stok_row = $qprod->row();
					$stok = ($stok_row->stok + $row->jml_pesan);
					$data = array('stok' => $stok);
					if ( ! $this->db->update($this->tbl_produk, $data, array('id_prod' => $row->id_prod))) 
					{
						$stat = FALSE;
					}
				}
				
				// Insert Order Batal
				$data = array(
					'id' => $row->id,
					'no_resi_pengiriman' => $row->no_resi_pengiriman,
					'id_cust' => $row->id_cust,
					'id_alamat' => $row->id_alamat,
					'no_struk' => $row->no_struk,
					'metode_bayar' => $row->metode_bayar,
					'bayar_melalui' => $row->bayar_melalui,
					'id_prod' => $row->id_prod,
					'detail_produk' => $row->detail_produk,
					'tgl_transaksi' => $row->tgl_transaksi,
					'jml_pesan' => $row->jml_pesan,
					'total_harga' => $row->total_harga,
					'notes_cust' => $row->notes_cust
				);
				
				if( ! $this->db->insert($this->tbl_order_batal,$data))
				{
					$stat = FALSE;
				}
			}
			
			//Update Pembeli
			$this->db->select('jml_transaksi,total_transaksi,jumlah_transaksi_gagal,total_transaksi_gagal');
			$qcust = $this->db->get_where($this->tbl_pembeli, array('id' => (int)$cid));
			if($qcust->num_rows() > 0)
			{
				$qcust = $qcust->row();
				$jtrans = ($qcust->jml_transaksi - $jml_trans);
				$ttlorder = ($qcust->total_transaksi - $ttl_trans);
				$jgagal = ($qcust->jumlah_transaksi_gagal + $jml_trans);
				$ttlgagal = ($qcust->total_transaksi_gagal + $ttl_trans);
				$data = array(
								'jml_transaksi' => $jtrans, 'total_transaksi' => $ttlorder, 
								'jumlah_transaksi_gagal' => $jgagal, 'total_transaksi_gagal' => $ttlgagal
							);
				if( ! $this->db->update($this->tbl_pembeli, $data, array('id' => (int)$cid))) {
					$stat = FALSE;
				}
			}

			//Delete TRX
			if( ! $this->db->delete($this->tbl_order, array('no_struk' => $trx)))
			{
				$stat = FALSE;
			}
			
			return $stat;
		}
		else {
			return FALSE;
		}
	}
	/* End Delete */

	/* Diskon Transaksi */
	function getDiskonTransaksiByTrx($trx='')
	{
		$this->db->where('no_struk', $trx);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_diskon);
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return $row->total_diskon;
		}
		else {
			return 0;
		}
	}
	/* End Diskon Transaksi */
	
/* End Orderan */

/* Alamat */
	/* Select */
	function getNamaPembeliByAlamatId($alid=0)
	{
		$this->db->select('nama_lengkap');
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_alamat_kirim,array('id_alamat' => (int)$alid));
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return dekodeString($row->nama_lengkap);
		}
		else {
			return NULL;
		}
	}
	
	function getAllAlamatById($alid=0)
	{
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_alamat_kirim,array('id_alamat' => (int)$alid));
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	function cekAdaAlamat($id_cust=0,$nama='',$cust_email='',$no_telpon='',$kode_pos,$id_negara='',$id_provinsi='',$id_wilayah='',$alamat_rumah='')
	{
		$query = $this->db->get_where($this->tbl_alamat_kirim, array('id_cust' => (int)$id_cust));

		$cek = TRUE;
		if($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $kirim)
			{
				if(dekodeString($kirim['nama_lengkap']) === $nama && dekodeString($kirim['email_cust']) === $cust_email && dekodeString($kirim['no_telpon']) === $no_telpon && dekodeString($kirim['kode_pos']) === $kode_pos && $kirim['id_negara'] === $id_negara && $kirim['id_provinsi'] === $id_provinsi && $kirim['id_wilayah'] === $id_wilayah && dekodeString($kirim['alamat_rumah']) === $alamat_rumah)
				{
					$this->session->set_userdata('alamat_id',bs_kode($kirim['id_alamat']));
					$cek = FALSE;
					//break;
				}
			}
		}
		else {
			$cek = TRUE;
		}

		return $cek;
	}
	/* End Select */

	/* Delete */
	function cleanAlamat()
	{
		$stat = TRUE;
		$pqry = $this->db->query("SELECT DISTINCT(id_alamat) as alamat FROM ". $this->tbl_order);
		if($pqry->num_rows > 0)
		{
			foreach ( $pqry->result() as $row )
			{
				$di_order[] = $row->alamat;
			}
		}
		else {
			$di_order = array();
		}
		
		$sqry = $this->db->query("SELECT DISTINCT(id_alamat) as alamat FROM ". $this->tbl_order_sukses);
		if($sqry->num_rows > 0)
		{
			foreach ($sqry->result() as $row)
			{
				$s_order[] = $row->alamat;
			}
		}
		else {
			$s_order = array();
		}
		
		$bqry = $this->db->query("SELECT DISTINCT(id_alamat) as alamat FROM ". $this->tbl_order_batal);
		if($bqry->num_rows > 0)
		{
			foreach ($bqry->result() as $row)
			{
				$b_order[] = $row->alamat;
			}
		} 
		else {
			$b_order = array();
		}
		
		$almt_order = array_merge($di_order,$s_order,$b_order);

		if(count($almt_order)>0){
		$falamat = implode(",", $almt_order);
		if( ! $this->db->query("DELETE FROM " . $this->tbl_alamat_kirim . " WHERE id_alamat NOT IN(".$falamat.")"))
		{
			$stat = FALSE;
		}
		}
		
		return $stat;
	}
	/* End Delete */

/* End Alamat */

/* Pembeli */ 
	/* Select */
	function cekAdaPembeli($cust_id='',$cust_email='')
	{
		$this->db->select('id, nama_lengkap, id_cust_uname, email_cust');
		$query = $this->db->get($this->tbl_pembeli);

		$cek = TRUE;
		if($query->num_rows() > 0)
		{
			if($this->session->userdata('kunci_id') === FALSE )
			{
				foreach ($query->result_array() as $cust)
				{
					if(dekodeString($cust['email_cust']) == $cust_email )
					{
						$custdata = array(
							'kunci_id' => bs_kode($cust['id']),
							'usrid'  => bs_kode($cust_id),
							'kunci'  => bs_kode($cust_email),
							'nmusr'  => bs_kode(dekodeString($cust['nama_lengkap']))
						);
						$this->session->set_userdata($custdata);
						$cek = FALSE;
						//break;
					}
				}
			}
			else {
				$cek = FALSE;
			}
		}
		else {
			$cek = TRUE;
		}
		
		return $cek;
	}

	function getTotalOrderCust($cid=0)
	{
		$this->db->select('jml_transaksi, total_transaksi');
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_pembeli,array('id' => (int)$cid));
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}
	/* End Select */
	
	/* Update */
	function updateAlamatOrderan()
	{
		/* Enkripsi Alamat */
		$nama_lengkap = enkodeString(trim($this->input->post('nama_lengkap', TRUE)));
		$email_cust = enkodeString(trim($this->input->post('email_cust', TRUE)));
		$no_telpon = enkodeString(trim($this->input->post('no_telpon', TRUE)));
		$kode_pos = enkodeString(trim($this->input->post('kode_pos', TRUE)));
		$alamat_rumah = enkodeString(str_ireplace("\\n", "<br />",$this->input->post('alamat_rumah', TRUE)));

		$id_negara = trim($this->input->post('negara', TRUE));
		$id_provinsi = trim($this->input->post('provinsi', TRUE));
		$id_wilayah = trim($this->input->post('wilayah', TRUE));

		$data = array(
					'nama_lengkap' => $nama_lengkap,
					'email_cust' => $email_cust,
					'no_telpon' => $no_telpon,
					'id_negara' => $id_negara,
					'id_provinsi' => $id_provinsi,
					'id_wilayah' => $id_wilayah,
					'kode_pos' => $kode_pos,
					'alamat_rumah' => $alamat_rumah
				);

		$id_alamat = trim($this->input->post('id_alamat', TRUE));

		if( ! $this->db->update($this->tbl_alamat_kirim, $data, array('id_alamat' => (int)$id_alamat)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function upJmlOrderCust($cid=0)
	{
		// total
		$dtcust = $this->getTotalOrderCust((int)$cid);
		$jml_trans = (int) ( $dtcust->jml_transaksi + (int)$this->cart->total_items() );
		$ttl_trans = (int) ( $dtcust->total_transaksi + (int)$this->cart->total() );
		
		$data = array (
					'jml_transaksi' => (int)$jml_trans,
					'total_transaksi' => (int)$ttl_trans
					);
		if( ! $this->db->update($this->tbl_pembeli, $data, array('id' => (int)$cid)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	/* End Update */
/* End Pembeli */

/* Metode Bayar */
	// 4tipe = transfer,online,giro,cod
	function getCountMetodeBayarByTipe($tipe='')
	{
		$this->db->where('tipe_metode', $tipe);
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->num_rows();
	}

	function getAllMetodeBayarByTipe($tipe='',$limit=0,$offset=0)
	{
		$this->db->where('tipe_metode', $tipe);
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_metode_bayar);
		return $query->result();
	}

	function getMetodeBayarById($id='')
	{
		$this->db->where('id', (int)$id);
		$query = $this->db->get($this->tbl_metode_bayar,1);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	function cekVendor($nama='',$ori='')
	{
		$this->db->where('nama_vendor', $nama);
		if( ! empty($ori))
		$this->db->where_not_in('nama_vendor', $ori);
		$query = $this->db->get($this->tbl_metode_bayar);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
	}

	//Tambah Metode Pembayaran
	function tambahMetodeBayar()
	{
		$logo = $this->upload->file_name;
		$id_wil = format_simpan_kategori_halaman($this->input->post('wilayah'));

		$data = array(
					'id_wilayah' => $id_wil,
					'tipe_metode' => $this->input->post('tipe'),
					'nama_vendor' => $this->input->post('nama_vendor'),
					'label_id' => $this->_format_label_bayar($this->input->post('tipe')),
					'label_en' => $this->_format_label_bayar($this->input->post('tipe'),'en'),
					'logo' => $logo,
					'detail' => $this->input->post('detail'),
					'status' => $this->input->post('status')
				);
		//cek metode online
		if($this->input->post('tipe') == 'online')
		{
			$gateway = array(
							'id_merchant' => $this->input->post('id_merchant'),
							'redirect_url' => $this->input->post('redirect_url'),
							'sukses_url' => $this->input->post('sukses_url'),
							'hash_key' => $this->input->post('hash_key')
						);
			$data = array_merge($data,$gateway);
		}

		if( ! $this->db->insert($this->tbl_metode_bayar, $data) )
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	//UPDATE
	function upMetodeBayarStat()
	{
		$id = $this->input->post('metode_id');
		$data = array('status' => $this->input->post('status'));

		if( ! $this->db->update($this->tbl_metode_bayar, $data, array('id' => (int)$id)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function upMetodeBayar()
	{
		$id_wil = format_simpan_kategori_halaman($this->input->post('wilayah'));
		if($this->input->post('status_ganti') == 'yes')
		{
			$logo = $this->upload->file_name;
			$data = array(
					'id_wilayah' => $id_wil,
					'tipe_metode' => $this->input->post('tipe'),
					'nama_vendor' => $this->input->post('nama_vendor'),
					'label_id' => $this->_format_label_bayar($this->input->post('tipe')),
					'label_en' => $this->_format_label_bayar($this->input->post('tipe'),'en'),
					'logo' => $logo,
					'detail' => $this->input->post('detail'),
					'status' => $this->input->post('status')
				);
		}
		else {
			$data = array(
					'id_wilayah' => $id_wil,
					'tipe_metode' => $this->input->post('tipe'),
					'nama_vendor' => $this->input->post('nama_vendor'),
					'label_id' => $this->_format_label_bayar($this->input->post('tipe')),
					'label_en' => $this->_format_label_bayar($this->input->post('tipe'),'en'),
					'detail' => $this->input->post('detail'),
					'status' => $this->input->post('status')
				);
		}
		//cek metode online
		if($this->input->post('tipe') == 'online')
		{
			$gateway = array(
							'id_merchant' => $this->input->post('id_merchant'),
							'redirect_url' => $this->input->post('redirect_url'),
							'sukses_url' => $this->input->post('sukses_url'),
							'hash_key' => $this->input->post('hash_key')
						);
			$data = array_merge($data,$gateway);
		}

		$this->db->where('id', $this->input->post('id_metode'));
        if( ! $this->db->update($this->tbl_metode_bayar, $data))
        {
			return FALSE;
        }
		else {
			return TRUE;
		}
	}

	//Hapus
	function hapusMetodeBayar()
	{
		$id = $this->input->post('id_hps');
        $qp = $this->db->get_where($this->tbl_metode_bayar, array('id' => (int)$id), 1);
        if ($qp->num_rows() > 0)
        {
			$rp = $qp->row();

			//hapus logo
			hapus_file( "./_media/logo-bayar/". $rp->logo);
			hapus_file("./_media/logo-bayar/thumb_". $rp->logo);

			// hapus id
            if ( ! $this->db->delete($this->tbl_metode_bayar, array('id' => (int)$id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
	}
	/* helper */
	function _format_label_bayar($tipe='',$bhs='id')
	{
		switch($tipe) {
			case  "transfer" :
				if($bhs == 'id')
				return 'Transfer Bank';
				else
				return 'Bank Transfers';
				break;
			case "online" :
				return 'Online';
				break;
			case "giro" :
				if($bhs == 'id')
				return 'Giro';
				else
				return 'Giro Post';
				break;
			case "cod" :
				return 'COD';
				break;
		}
	}
/* End Metode Bayar */


/* Kurir Pengiriman */
	function tambahKurir()
	{
		$logo = $this->upload->file_name;
		$id_wil = format_simpan_kategori_halaman($this->input->post('wilayah'));

		$data = array(
					'nama_vendor' => $this->input->post('nama_vendor'),
					'nama_layanan' => $this->input->post('nama_layanan'),
					'url_trace' => $this->input->post('url_trace'),
					'id_wilayah' => $id_wil,
					'biaya_ongkir' => $this->input->post('biaya_ongkir'),
					'waktu_barang_sampai' => $this->input->post('waktu_barang_sampai'),
					'logo' => $logo,
					'detail' => $this->input->post('detail'),
					'status' => $this->input->post('status')
				);

		if( ! $this->db->insert($this->tbl_kurir, $data) )
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function cekVendorKurir($nama='',$ori='')
	{
		$this->db->where('nama_vendor', $nama);
		if( ! empty($ori))
		$this->db->where_not_in('nama_vendor', $ori);
		$query = $this->db->get($this->tbl_kurir);
        if ($query->num_rows() > 0)
        {
           return FALSE;
        }
        else {
           return TRUE;
        }
	}

	function getCountKurirPengiriman()
	{
		$query = $this->db->get($this->tbl_kurir);
		return $query->num_rows();
	}

	function getAllKurirPengiriman($limit=0,$offset=0)
	{
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->tbl_kurir);
		return $query->result();
	}

	function getKurirPengirimanById($id='')
	{
		$this->db->where('id', (int)$id);
		$query = $this->db->get($this->tbl_kurir,1);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	//UPDATE
	function upKurirPengirimanStat()
	{
		$id = $this->input->post('kurir_id');
		$data = array('status' => $this->input->post('status'));

		if( ! $this->db->update($this->tbl_kurir, $data, array('id' => (int)$id)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function upKurirPengiriman()
	{
		$id_wil = format_simpan_kategori_halaman($this->input->post('wilayah'));
		if($this->input->post('status_ganti') == 'yes')
		{
			$logo = $this->upload->file_name;
			$data = array(
					'nama_vendor' => $this->input->post('nama_vendor'),
					'nama_layanan' => $this->input->post('nama_layanan'),
					'url_trace' => $this->input->post('url_trace'),
					'id_wilayah' => $id_wil,
					'biaya_ongkir' => $this->input->post('biaya_ongkir'),
					'waktu_barang_sampai' => $this->input->post('waktu_barang_sampai'),
					'logo' => $logo,
					'detail' => $this->input->post('detail'),
					'status' => $this->input->post('status')
				);
		}
		else {
			$data = array(
					'nama_vendor' => $this->input->post('nama_vendor'),
					'nama_layanan' => $this->input->post('nama_layanan'),
					'url_trace' => $this->input->post('url_trace'),
					'id_wilayah' => $id_wil,
					'biaya_ongkir' => $this->input->post('biaya_ongkir'),
					'waktu_barang_sampai' => $this->input->post('waktu_barang_sampai'),
					'detail' => $this->input->post('detail'),
					'status' => $this->input->post('status')
				);
		}

		$this->db->where('id', $this->input->post('id_kurir'));
        if( ! $this->db->update($this->tbl_kurir, $data))
        {
			return FALSE;
        }
		else {
			return TRUE;
		}
	}

	//Hapus
	function hapusKurirPengiriman()
	{
		$id = $this->input->post('id_hps');
        $qp = $this->db->get_where($this->tbl_kurir, array('id' => (int)$id), 1);
        if ($qp->num_rows() > 0)
        {
			$rp = $qp->row();

			//hapus logo
			hapus_file( "./_media/logo-kurir/". $rp->logo);
			hapus_file("./_media/logo-kurir/thumb_". $rp->logo);

			// hapus id
            if ( ! $this->db->delete($this->tbl_kurir, array('id' => (int)$id)))
            {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return FALSE;
        }
	}

/* End Kurir Pengiriman */


}
/* EOF OrderModel.php */

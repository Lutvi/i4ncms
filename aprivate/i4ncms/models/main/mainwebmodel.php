<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Mainwebmodel extends MY_Model {

    function __construct()
    {
        parent::__construct();
    }

	function initConfigWeb()
    {
        //$this->db->select('nama_config,nilai_config,tipe_data');
        //$query = $this->db->get($this->tbl_web_config);
        $query = $this->db->query("SELECT nama_config,nilai_config,tipe_data FROM ".$this->tbl_web_config.";");
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else {
            return array();
        }
    }

    function getTemaWeb()
    {
        /*$this->db->select('nama_tema,is_responsive,slider_menu,opsi_conf,nilai_opsi');
        $this->db->where('status', 'on');
        $this->db->where('versi', $this->config->item('cms_version'));
        $query = $this->db->get($this->tbl_tema);*/

        $query = $this->db->query("SELECT nama_tema,is_responsive,slider_menu,opsi_conf,nilai_opsi FROM ".$this->tbl_tema." WHERE status = 'on' AND versi = '".$this->config->item('cms_version')."';");
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else {
            return array();
        }
    }

}

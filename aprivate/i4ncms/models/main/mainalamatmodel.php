<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class MainAlamatmodel extends MY_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getAllAlamatById($alid=0)
	{
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_alamat_kirim,array('id_alamat' => (int)$alid));
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

}

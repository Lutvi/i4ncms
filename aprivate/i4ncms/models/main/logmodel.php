<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Logmodel extends MY_Model {
	
	function __construct()
    {
		parent::__construct();
    }

/* Log comming user */
	function tambahLogRef($tipe_halaman = '')
	{
		$this->load->helper('layanan');

		if(empty($tipe_halaman))
		$tipe_halaman = 'form||statis';

		$data = array(
						'log_tgl' => date('Y-m-d', time()),
						'url_asal' => $this->agent->referrer(),
						'url_target' => current_url(),
						'tipe_halaman' => $tipe_halaman,
						'ip' => $this->input->ip_address(),
						'host' => gethostbyaddr($this->input->ip_address()),
						'port' => $_SERVER['REMOTE_PORT'],
						'lokasi' => detail_lokasi_ip($this->input->ip_address()),
						'browser' => $this->agent->browser(),
						'detail_platform' => $this->agent->agent_string().'|'.$this->agent->platform()
					);

		#$adakah = $this->db->get_where($this->tbl_ref_user,array('log_tgl' => date('Y-m-d'),'url_asal' => $this->agent->referrer(),'ip' => $this->input->ip_address(),'browser' => $this->agent->browser()),1);
		$adakah = $this->db->get_where($this->tbl_ref_user,array('log_tgl' => date('Y-m-d'),'ip' => $this->input->ip_address()),1);
		if($adakah->num_rows() > 0)
		{
			$ref = $adakah->row();
			$data['hits'] = $ref->hits+1;
			$this->db->where('id', $ref->id);
			$this->db->update($this->tbl_ref_user, $data);
		}
		else{
			$data['hits'] = 1;
			$this->db->insert($this->tbl_ref_user,$data);
		}
	}

	function lihatLogUser($limit=0, $offset=0)
	{
		$this->db->limit($limit, $offset);
		$this->db->order_by("tgl_kujungan", "desc");
		$query = $this->db->get($this->tbl_ref_user);
		return $query->result();
	}

	function getCountLogUser()
	{
		$query = $this->db->get($this->tbl_ref_user);
		return $query->num_rows(); 
	}

	function detailLogUser($id='')
	{
		$query = $this->db->get_where($this->tbl_ref_user, array('id' => $id));
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}
/* End Log comming user */

/* Log comming robot */
	function tambahLogRobot($tipe_halaman = '')
	{
		if(empty($tipe_halaman))
		$tipe_halaman = 'form||statis';

		$data = array(
						'url_asal' => $this->agent->referrer(),
						'url_target' => current_url(),
						'tipe_halaman' => $tipe_halaman,
						'ip' => $this->input->ip_address(),
						'port' => $_SERVER['REMOTE_PORT'],
						'detail_robot' => $this->agent->robot()
					);
		$adakah = $this->db->get_where($this->tbl_log_robot,array('ip' => $this->input->ip_address(),'url_target' => current_url()),1);
		if($adakah->num_rows() > 0)
		{
			$rob = $adakah->row();
			$this->db->where('id', $rob->id);
			$this->db->update($this->tbl_log_robot, $data);
		}
		else
		$this->db->insert($this->tbl_log_robot,$data);
	}

	function lihatLogRobot($limit=0, $offset=0)
	{
		$this->db->limit($limit, $offset);
		$this->db->order_by("tgl_kujungan", "desc");
		$query = $this->db->get($this->tbl_log_robot);
		return $query->result();
	}

	function getCountLogRobot()
	{
		$query = $this->db->get($this->tbl_log_robot);
		return $query->num_rows(); 
	}

	function detailLogRobot($id='')
	{
		$query = $this->db->get_where($this->tbl_log_robot, array('id' => $id));
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}
/* End Log comming robot */

/* Log error */
	function lihatLog($limit=0, $offset=0)
	{
		//$this->db->select('id,tipe_error,tgl,url,ket');
		$this->db->limit($limit, $offset);
		$this->db->order_by("tgl", "desc");
		$query = $this->db->get($this->tbl_log);
		return $query->result();
	}
	
	function detailLog($id='')
	{
		$query = $this->db->get_where($this->tbl_log, array('id' => $id));
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}
	
	function getCountLog()
	{
		$query = $this->db->get($this->tbl_log);
		return $query->num_rows(); 
	}
	
	function tambahLog($tipe,$ip,$brow,$page,$rqst,$ket)
	{
		$data = array(
						'tipe_error' => $tipe,
						'ip_port' => $ip,
						'user_agent' => $brow,
						'url' => $page,
						'data' => $rqst,
						'ket' => $ket
					);
		$this->db->insert($this->tbl_log,$data);
	}

	function hapusLog($id='')
	{
		if(empty($id))
		$id = (int)$this->input->post('id_hps', TRUE);
		if ( ! $this->db->delete($this->tbl_log, array('id' => (int)$id)))
			return FALSE;
		else
			return TRUE;
	}
/* End Log error */

	// hapus log lama bila data lebih dari 100.000 baris
	function bersihLog($awal='',$akhir='')
	{
		$jml = $this->db->count_all($this->tbl_log);
		if($jml > 100000)
		{
			$q = "SELECT id FROM " . $this->tbl_log . " WHERE tgl >= '" . $awal ."' AND tgl <= '" . $akhir . "'";
			$query = $this->db->query($q);
			foreach($query->result() as $lama) {
				$this->db->delete($this->tbl_log, array('id' => $lama->id));
				/*$this->db->where('id', $lama->id);
				$this->db->delete($this->tbl_log);*/
			}
		}
	}
}

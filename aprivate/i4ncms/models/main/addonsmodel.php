<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Addonsmodel extends MY_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function getNamaProvinsiById($id)
    {
        $this->db->select('provinsi');
        $this->db->limit(1);
        $this->db->where('id_provinsi',(int)$id);
        $query = $this->db->get($this->tbl_provinsi);
        if ($query->num_rows > 0)
        {
            $row = $query->row();
            return $row->provinsi;
        }
        else {
            return NULL;
        }
    }
    
    function getNamaKotaById($id)
    {
        $this->db->select('kab_kota');
        $this->db->limit(1);
        $this->db->where('id_kab_kota',(int)$id);
        $query = $this->db->get($this->tbl_kota);
        if ($query->num_rows > 0)
        {
            $row = $query->row();
            return $row->kab_kota;
        }
        else {
            return NULL;
        }
    }
 
 /* Provisi Indonesia */   
    function listProvinsi()
    {
        $this->db->select('id_provinsi,kode_provinsi,provinsi');
        $query = $this->db->get($this->tbl_provinsi);
        return $query->result();
    }
    
    function listKota()
    {
        $this->db->select('id_kab_kota,id_provinsi,kab_kota');
        
        $query = $this->db->get($this->tbl_kota);
        return $query->result();
    }
    
    function listKotaById($idp=0)
    {
        $this->db->select('id_kab_kota,id_provinsi,kab_kota');
        
        $query = $this->db->get_where($this->tbl_kota, array('id_provinsi' => (int)$idp));
        return $query->result();
    }
    
 /* End Provisi Indonesia */   

}

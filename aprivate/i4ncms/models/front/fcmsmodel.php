<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

Class FcmsModel extends MY_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

//MENU
    function getParentMenu($parent='')
    {
		if($this->config->item('cache_front'))
		{
			$this->db->cache_on();
			if( ! $this->config->item('toko_aktif'))
			{
				$sql = "SELECT `id`, `link`, `title_".current_lang(FALSE)."` AS title, tipe, parentid 
						FROM ".$this->tbl_menu." JOIN ".$this->tbl_halaman." ON ".$this->tbl_menu.".link = ".$this->tbl_halaman.".`id_halaman` OR ".$this->tbl_menu.".`link` = $parent
						WHERE ".$this->tbl_halaman.".`tipe` !=  'produk'
						AND ".$this->tbl_menu.".`parentid` = $parent
						AND ".$this->tbl_menu.".`status` = 1
						GROUP BY ".$this->tbl_menu.".`id`
						ORDER BY ".$this->tbl_menu.".`posisi` ASC";

				$query = $this->db->query($sql);
			}
			else {
				$this->db->select('id,link,title_'.current_lang(FALSE).' AS title');
				$this->db->order_by("posisi", "asc");
				$query = $this->db->get_where($this->tbl_menu,array('parentid'=>$parent,'status'=>'1'));
			}
			$this->db->cache_off();
		}
		else {
			if( ! $this->config->item('toko_aktif'))
			{
				$sql = "SELECT `id`, `link`, `title_".current_lang(FALSE)."` AS title, tipe, parentid 
						FROM ".$this->tbl_menu." JOIN ".$this->tbl_halaman." ON ".$this->tbl_menu.".link = ".$this->tbl_halaman.".`id_halaman` OR ".$this->tbl_menu.".`link` = $parent
						WHERE ".$this->tbl_halaman.".`tipe` !=  'produk'
						AND ".$this->tbl_menu.".`parentid` = $parent
						AND ".$this->tbl_menu.".`status` = 1
						GROUP BY ".$this->tbl_menu.".`id`
						ORDER BY ".$this->tbl_menu.".`posisi` ASC";

				$query = $this->db->query($sql);
			}
			else {
				$this->db->select('id,link,title_'.current_lang(FALSE).' AS title');
				$this->db->order_by("posisi", "asc");
				$query = $this->db->get_where($this->tbl_menu,array('parentid'=>$parent,'status'=>'1'));
			}
		}

        return $query->result_array();
    }

    function getParentName($parent)
    {
		if($this->config->item('cache_front'))
		{
			$this->db->cache_on();
			$this->db->select('title_'.current_lang(FALSE).' AS title');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get_where($this->tbl_menu, array('id'=>$parent), 1);
			$this->db->cache_off();
		}
		else {
			$this->db->select('title_'.current_lang(FALSE).' AS title');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get_where($this->tbl_menu, array('id'=>$parent), 1);
		}

        if ($query->num_rows() > 0)
        {
           $row = $query->row(); 
           return $row->title;
        }
        else {
            return NULL;
        }
    }

    function getTitleMenu()
    {
		if($this->config->item('cache_front'))
		{
			$this->db->cache_on();
			$this->db->select('id, level, title_'.current_lang(FALSE).' AS title');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get($this->tbl_menu);
			$this->db->cache_off();
		}
		else {
			$this->db->select('id, level, title_'.current_lang(FALSE).' AS title');
			$this->db->order_by("posisi", "asc");
			$query = $this->db->get($this->tbl_menu);
		}

		return $query->result();
    }

//HALAMAN
    function getAllHalamanFrontById($id)
    {
        $this->db->where('status','on');
        $query = $this->db->get_where($this->tbl_halaman,array('id_halaman' => $id));
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }
    
    function getAllHalamanFrontByNama($nama)
    {
        $this->db->where('status','on');
        $this->db->where('label_id',$nama);
        $this->db->or_where('label_en',$nama);
        $query = $this->db->get($this->tbl_halaman);
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getAllSitemapHalamanFrontByNama($nama)
    {
        $this->db->where('status','on');
        $this->db->where('label_id',$nama);
        $this->db->or_where('label_en',$nama);
        $query = $this->db->get($this->tbl_halaman);
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

//-Cek Produk
    function cekLabelHalamanProdukById($id=0)
    {
		$this->db->select('id_halaman');
		$this->db->where('id_halaman', (int)$id);

		if( ! $this->config->item('toko_aktif'))
		$this->db->where('tipe != ','produk', 1);

		$query = $this->db->get($this->tbl_halaman);
		return $query->num_rows();
    }
//-END Cek Produk


    function getLabelHalamanById($id=0)
    {
		if(current_lang(false) === 'id')
        $this->db->select('label_id AS label');
        else
        $this->db->select('label_en AS label');
        $query = $this->db->get_where($this->tbl_halaman,array('id_halaman' => (int)$id), 1);
        if ($query->num_rows() > 0)
        {
			$row = $query->row();
			return $row->label;
        }
        else {
            return NULL;
        }
    }


//BLOG
    function getAllBlogFrontById($id='')
    {
        $this->db->where('status','on');
        $this->db->order_by("tgl_terbit", "desc");
        $query = $this->db->get_where($this->tbl_blog,array('id' => $id),1);
        return $query->result();
    }

    function getAllBlogFrontByNama($nama)
    {
        $this->db->where('status','on');
        $this->db->where('judul_id',$nama);
        $this->db->or_where('judul_en',$nama);
        $query = $this->db->get($this->tbl_blog,1);
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getCountBlogFrontHandler()
    {
		$this->db->select('id');
        $this->db->where('status','on');
        $query = $this->db->get($this->tbl_blog);
        return $query->num_rows();
    }

    function getAllBlogFrontHandler($limit=0,$offset=0)
    {
        $this->db->where('status','on');
        $this->db->order_by('headline', 'asc');
        $query = $this->db->get($this->tbl_blog,$limit,$offset);
        return $query->result();
    }

    function getCountAllBlogFrontByKategori($kategori='')
    {
		$kategori = format_kategori($kategori, TRUE);

		$this->db->select('id');
		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("tgl_terbit", "desc");
        $query = $this->db->get($this->tbl_blog);
        if ($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else {
            return NULL;
        }
    }

    function getAllBlogFrontByKategori($kategori='', $limit=0, $offset=0)
    {
		$kategori = format_kategori($kategori, TRUE);

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("tgl_terbit", "desc");
        $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_blog);
        return $query->result();
    }

    function getAllSitemapBlogFrontByKategori($bhs='id',$kategori='', $limit=0, $offset=0)
    {
		$kategori = format_kategori($kategori, TRUE);

		if($bhs == 'id')
		$this->db->select('id,judul_id AS judul');
		else
		$this->db->select('id,judul_en AS judul');

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("tgl_terbit", "desc");
        $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_blog);
        return $query->result();
    }

    function getCountAllTagsBlogFrontById($id=array())
    {
        $this->db->where('status','on');
        $this->db->order_by("tgl_terbit", "desc");
        $this->db->where_in('id',$id);
        $query = $this->db->get($this->tbl_blog);
        if ($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else {
            return NULL;
        }
    }

    function getAllTagsBlogFrontById($id=array(),$limit=0,$offset=0)
    {
        $this->db->where('status','on');
        $this->db->order_by("tgl_terbit", "desc");
        $this->db->limit((int)$limit, (int)$offset);
        $this->db->where_in('id',$id);
        $query = $this->db->get($this->tbl_blog);
        return $query->result();
    }

//ALBUM
    function getAllAlbumFrontById($id='')
    {
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get_where($this->tbl_album,array('id' => $id),1);
        return $query->result();
    }

    function getAllAlbumFrontByNama($nama)
    {
        $this->db->where('status','on');
        $this->db->where('nama_id',$nama);
        $this->db->or_where('nama_en',$nama);
        $query = $this->db->get($this->tbl_album,1);
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getCountAlbumFrontHandler()
    {
		$this->db->select('id');
        $this->db->where('status','on');
        $query = $this->db->get($this->tbl_album);
        return $query->num_rows();
    }

    function getAllAlbumFrontHandler($limit=0,$offset=0)
    {
        $this->db->where('status','on');
        $this->db->order_by('utama', 'asc');
        $query = $this->db->get($this->tbl_album,$limit,$offset);
        return $query->result();
    }

    function getCountAllAlbumFrontByKategori($kategori='')
    {
		$kategori = format_kategori($kategori, TRUE);

		$this->db->select('id');
		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_album);
        if ($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else {
            return NULL;
        }
    }

    function getAllAlbumFrontByKategori($kategori='', $limit=0, $offset=0)
    {
		$kategori = format_kategori($kategori, TRUE);

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_album);
        return $query->result();
    }

    function getAllSitemapAlbumFrontByKategori($bhs='id',$kategori='', $limit=0, $offset=0)
    {
		$kategori = format_kategori($kategori, TRUE);

		if($bhs == 'id')
		$this->db->select('id,nama_id AS judul');
		else
		$this->db->select('id,nama_en AS judul');

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_album);
        return $query->result();
    }

    function getCountAllTagsAlbumFrontById($id=array())
    {
        $this->db->where('status','on');
        $this->db->where_in('id',$id);
        $query = $this->db->get($this->tbl_album);
        if ($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else {
            return NULL;
        }
    }

    function getAllTagsAlbumFrontById($id=array(),$limit=0,$offset=0)
    {
        $this->db->where('status','on');
        $this->db->where_in('id',$id);
        $this->db->order_by("posisi", "asc");
         $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_album);
        return $query->result();
    }

//ALBUM-GALLERY
    function getNamaGalleryAlbumById($id='')
    {
		$this->db->select('nama_id,nama_en');
		$this->db->where('id', $id, 1);
        $query = $this->db->get($this->tbl_album);
        $row = $query->row();
        return $row;
    }

    function getFrontCountGalleryAlbum($id='')
    {
		 $this->db->where('id_album', $id);
		 $this->db->where('status', 'on');
         $query = $this->db->get($this->tbl_gallery);
         return $query->num_rows(); 
    }

    function getAllFrontGalleryAlbum($id='',$limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $this->db->where('id_album', $id);
        $this->db->where('status', 'on');
        $query = $this->db->get($this->tbl_gallery);
        return $query->result();
    }


//VIDEO
    function getAllVideoFrontById($id='')
    {
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get_where($this->tbl_video,array('id' => $id),1);
        return $query->result();
    }

    function getAllVideoFrontByNama($nama='')
    {
        $this->db->where('status','on');
        $this->db->where('nama_id',$nama);
        $this->db->or_where('nama_en',$nama);
        $query = $this->db->get($this->tbl_video,1);
        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else {
            return NULL;
        }
    }

    function getCountVideoFrontHandler()
    {
		$this->db->select('id');
        $this->db->where('status','on');
        $query = $this->db->get($this->tbl_video);
        return $query->num_rows();
    }

    function getAllVideoFrontHandler($limit=0,$offset=0)
    {
        $this->db->where('status','on');
        $this->db->order_by('utama', 'asc');
        $query = $this->db->get($this->tbl_video,$limit,$offset);
        return $query->result();
    }

    function getCountAllVideoFrontByKategori($kategori='')
    {
		$kategori = format_kategori($kategori, TRUE);

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $query = $this->db->get($this->tbl_video);
        if ($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else {
            return NULL;
        }
    }

    function getAllVideoFrontByKategori($kategori='', $limit=0, $offset=0)
    {
		$kategori = format_kategori($kategori, TRUE);

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_video);
        return $query->result();
    }

    function getAllSitemapVideoFrontByKategori($bhs='id',$kategori='', $limit=0, $offset=0)
    {
		$kategori = format_kategori($kategori, TRUE);

		if($bhs == 'id')
		$this->db->select('id,nama_id AS judul');
		else
		$this->db->select('id,nama_en AS judul');

		$this->db->where_in('kategori_id', $kategori);
        $this->db->where('status','on');
        $this->db->order_by("posisi", "asc");
        $this->db->limit((int)$limit, (int)$offset);
        $query = $this->db->get($this->tbl_video);
        return $query->result();
    }

    function getCountAllTagsVideoFrontById($id=array())
    {
        $this->db->where('status','on');
        $this->db->where_in('id',$id);
        $query = $this->db->get($this->tbl_video);
        if ($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else {
            return NULL;
        }
    }

    function getAllTagsVideoFrontById($id=array(),$limit=0,$offset=0)
    {
        $this->db->where('status','on');
        $this->db->limit((int)$limit, (int)$offset);
        $this->db->where_in('id',$id);
        $query = $this->db->get($this->tbl_video);
        return $query->result();
    }

//VIDEO-PLAYLIST
    function getNamaVideoPlaylistById($id='')
    {
		$this->db->select('nama_id,nama_en');
		$this->db->where('id', $id, 1);
        $query = $this->db->get($this->tbl_video);
        $row = $query->row();
        return $row;
    }

    function getFrontCountVideoPlaylist($id='')
    {
		 $this->db->where('id_video', $id);
		 $this->db->where('status', 'on');
         $query = $this->db->get($this->tbl_playlist);
         return $query->num_rows(); 
    }

    function getAllFrontVideoPlaylist($id='',$limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $this->db->where('id_video', $id);
        $this->db->where('status', 'on');
        $query = $this->db->get($this->tbl_playlist);
        return $query->result();
    }

//WIDGET
    function getAllAktifWidget($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("posisi", "asc");
        $this->db->where('status',1);
        $query = $this->db->get($this->tbl_widget);
        return $query->result();
    }

    function cntRec()
    {
        $query = $this->db->get($this->tbl_widget);
        return $query->num_rows();
    }

    function getWidget()
    {
        $this->db->select('nama_widget,css,js');
        $this->db->order_by("posisi", "asc"); 
        $query = $this->db->get_where($this->tbl_widget, array('status'=>1));
        return $query->result_array();
    }
    
    function getWidgetName()
    {
        $this->db->select('nama_widget,judul_widget,judul_widget_en');
        $query = $this->db->get_where($this->tbl_widget, array('status'=>1));
        return $query->result_array();
    }
    
    function getWidgetHtml($nama)
    {
        $this->db->select('html');
        $query = $this->db->get_where($this->tbl_widget, array('nama_widget'=>$nama));
        return $query->row();
    }


//MODUL
	function getModuleName()
    {
        $this->db->select('nama_module,path_front_module');
        $this->db->order_by("posisi", "asc"); 
        $query = $this->db->get_where($this->tbl_module, array('status'=>1));
        return $query->result_array();
    }

    function getModule()
    {
        $this->db->select('id_module,nama_module,css,js');
        $query = $this->db->get_where($this->tbl_module, array('status'=>1));
        return $query->result_array();
    }
    
    function getAllFrontMod()
    {
        $this->db->select('path_front_module');
        $query = $this->db->get_where($this->tbl_module,array('status' => 1));
        return $query->result();
    }
    
    function getFrontModById($id)
    {
        $this->db->select('path_front_module');
        $query = $this->db->get_where($this->tbl_module,array('id_module' => $id, 'status' => 1));
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            return $row->path_front_module;
        }
        else {
            return NULL;
        }
    }

    function getModuleAsetsById($id)
    {
        $this->db->select('id_module, nama_module, css, js');
        $query = $this->db->get_where($this->tbl_module, array('id_module' => $id, 'status' => 1));
        return $query->result_array();
    }

}

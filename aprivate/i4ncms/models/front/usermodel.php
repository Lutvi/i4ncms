<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Usermodel extends MY_Model {

    var $tbl_usr = 'tbl_ecom_pembeli';
    var $col_uname = 'id_cust_uname';
    var $col_pass = 'kunci_pas';
    var $col_email = 'email_cust';
    
    function __construct()
    {
        parent::__construct();
    }
    
    function cekLogin()
    {
        $this->db->select($this->col_pass.','.$this->col_uname.','.$this->col_email.',id,nama_lengkap');
        $query = $this->db->get($this->tbl_usr);
        
        $cek = FALSE;
        if($query->num_rows() > 0)
        {
			foreach($query->result_array() as $user) {
				if(preg_match("/\b".dekodeString($user[$this->col_pass])."\b/i", trim($this->input->post('upass', TRUE))) && preg_match("/\b".dekodeString($user[$this->col_uname])."\b/i", trim($this->input->post('uname', TRUE))))
				{
					// buat session
					$custdata = array(
						'kunci_id' => bs_kode($user['id']),
						'usrid'  => bs_kode(dekodeString($user[$this->col_uname])),
						'kunci'  => bs_kode(dekodeString($user[$this->col_email])),
						'kuncix'  => $user[$this->col_email],
						'nmusr'  => bs_kode(dekodeString($user['nama_lengkap']))
					);
					$this->session->set_userdata($custdata);
					return TRUE;
					break;
				}
				else {
					$cek = FALSE;
				}
			}
        }
        else {
            $cek = FALSE;
        }

        return $cek;
    }
    
    function cekAdaUname($update=FALSE)
    {
        $this->db->select($this->col_uname);
        $query = $this->db->get($this->tbl_usr);

        $username = underscore(trim($this->input->post('username', TRUE)));
        
        $cek = TRUE;
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $user) {
				if($update === TRUE)
				{
					if($username != bs_kode($this->session->userdata('usrid'), TRUE))
					{
						if(dekodeString($user[$this->col_uname]) == $username)
						$cek = FALSE;
						//break;
					}
				}
				else {
					if(dekodeString($user[$this->col_uname]) == $username)
					{
					   $cek = FALSE;
					   //break;
					}
                }
            }
        }
        else {
            $cek = TRUE;
        }

        return $cek;
    }
      
    function cekAdaEmail($update=FALSE)
    {
        $this->db->select($this->col_email);
        $query = $this->db->get($this->tbl_usr);

        $email_txt = trim($this->input->post('email', TRUE));

        $cek = TRUE;
        if($query->num_rows() > 0)
        {
            foreach($query->result_array() as $email) {
				if($update === TRUE)
				{
					if($email_txt != bs_kode($this->session->userdata('kunci'), TRUE))
					{
						if(dekodeString($email[$this->col_email]) == $email_txt)
						$cek = FALSE;
						//break;
					}
				}
				else {
					if(dekodeString($email[$this->col_email]) == $email_txt)
					{
					   $cek = FALSE;
					   //break;
					}
                }
            }
        }
        else {
            $cek = TRUE;
        }

        return $cek;
    }
    
    function tambahUser()
    {
        if($this->cekAdaUname() === TRUE && $this->cekAdaEmail() === TRUE)
        {
            // kirim email untuk user_id dan passwordnya
			$judul = 'Detail Akun website '.$this->config->item('site_name');
			$kunci_pas = word_censor($this->input->post('password', TRUE), array($this->input->post('password', TRUE)));
			$isi = format_isi_email_registrasi(current_lang(false),$this->input->post('name', TRUE),underscore($this->input->post('username', TRUE)),$kunci_pas,$this->input->post('email', TRUE));

            $data = array(
						'tgl_daftar' => date('Y-m-d H:i:s'),
						'nama_lengkap' => enkodeString(trim($this->input->post('name', TRUE))),
                        $this->col_uname => enkodeString(trim(underscore($this->input->post('username', TRUE)))),
                        $this->col_email => enkodeString(trim($this->input->post('email', TRUE))),
                        $this->col_pass => enkodeString(trim($this->input->post('password', TRUE))),
                        'no_telpon' => 0,
                        'id_negara' => 'indonesia',
                        'id_provinsi' => 0,
                        'id_wilayah' => 0,
                        'kode_pos' => 0,
                        'alamat_rumah' => enkodeString($this->config->item('format_alamat'))
                    );

            if(kirim_email_user(array(), trim($this->input->post('email', TRUE)),$judul,$isi,TRUE))
            {
				$this->db->insert($this->tbl_usr,$data);
				return TRUE;
			}
			else {
				return FALSE;
			}
        }
        else {
            log_message('error', '[::WARNING::] Gagal menyimpan data registrasi User, Duplikasi data terjadi melewati validasi sebelumnya.');
            $k = '[::WARNING::] Gagal menyimpan data registrasi User, Duplikasi data terjadi melewati validasi sebelumnya.';
            $this->logs->tambahLog(date("d-m-Y H:i:s"),$this->input->ip_address() . " : " . $_SERVER['REMOTE_PORT'],$this->input->user_agent(),$_SERVER['REQUEST_URI'],serialize($_REQUEST),$k);
            return FALSE;
        }
    }

    function updateUser()
    {
		$data = array(
			'nama_lengkap' => enkodeString(trim($this->input->post('name', TRUE))),
			$this->col_uname => enkodeString(trim(underscore($this->input->post('username', TRUE)))),
			$this->col_email => enkodeString(trim($this->input->post('email', TRUE)))
		);

		if( $this->input->post('password', TRUE) !== '')
		{
			$data[$this->col_pass] = enkodeString(trim($this->input->post('password', TRUE)));
			$kunci_pas = word_censor($this->input->post('password', TRUE), array($this->input->post('password', TRUE)));
		}
		else {
			$kunci_pas = '########';
		}

		$judul = 'Update Akun website '.$this->config->item('site_name');
		$isi = format_isi_email_update_profil(current_lang(false),$this->input->post('name', TRUE),underscore($this->input->post('username', TRUE)),$kunci_pas,$this->input->post('email', TRUE));

		if(kirim_email_user(array(), trim($this->input->post('ori_email', TRUE)),$judul,$isi,TRUE))
		{
			$id = bs_kode($this->session->userdata('kunci_id'), TRUE);
			if ( ! $this->db->update($this->tbl_usr,$data, array('id' => (int)$id)))
			{
				return FALSE;
			}
			else {
				//update data session
				$custdata = array(
					'usrid'  => bs_kode(underscore($this->input->post('username', TRUE))),
					'kunci'  => bs_kode(trim($this->input->post('email', TRUE))),
					'nmusr'  => bs_kode(trim($this->input->post('name', TRUE)))
				);
				$this->session->set_userdata($custdata);

				return TRUE;
			}
		}
		else {
			return FALSE;
		}
    }

}

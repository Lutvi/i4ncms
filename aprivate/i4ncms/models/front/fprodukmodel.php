<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Fprodukmodel extends MY_Model {

	function __construct()
	{
		parent::__construct();
	}

/* Produk */
	function cekStokProduk($pid=0)
	{
		$this->db->select('stok');
		$this->db->where('id_prod',(int)$pid);
		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function getCountFrontProdukById($pid=0)
	{
		$this->db->select('id_prod,parent_id_prod,status');
		$sql = "id_prod IN(".(int)$pid.") OR parent_id_prod IN(".(int)$pid.") ";
		$sql .= "AND status IN('on') ";
		if ($this->config->item('cek_stok_front') === TRUE)
		$sql .= "AND stok > 0 ";
		$this->db->where($sql);

		$this->db->order_by('id_prod','asc');
		$query = $this->db->get($this->tbl_produk);
		return $query->num_rows;
	}

	function getAllFrontProdukById($pid=0,$limit=0,$offset=0)
	{
		$this->db->select('id_prod,kode_prod,id_prod_img,nama_prod,nama_prod_en,kode_prod,tgl_update,promo,kategori_id,diskon,harga_prod,harga_spesial,kode_warna,deskripsi,deskripsi_en,stok,total_hits,ratting,votes,status');
		$sql = "id_prod IN(".(int)$pid.") OR parent_id_prod IN(".(int)$pid.") ";
		$sql .= "AND status IN('on') ";
		if ($this->config->item('cek_stok_front') === TRUE)
		$sql .= "AND stok > 0 ";
		$this->db->where($sql);

		$this->db->order_by('id_prod','asc');
		$this->db->limit((int)$limit, (int)$offset);
		$query = $this->db->get($this->tbl_produk);

		return $query->result();
	}

	function getCountFrontProdukHandler($ttl=0)
	{
		$this->db->select('id_prod');
		$sql = "status = 'on' ";
		if ($this->config->item('cek_stok_front') === TRUE)
		$sql .= "AND stok > 0 ";
		$this->db->where($sql);

		$query = $this->db->get($this->tbl_produk);
		return $query->num_rows();
	}

	function getAllFrontProdukHandler($limit=0,$offset=0)
	{
		$this->db->select('id_prod,kode_prod,id_prod_img,nama_prod,nama_prod_en,anak_untuk_jenis,nama_jenis_anak,nama_jenis_anak_en,kode_prod,tgl_update,promo,kategori_id,diskon,harga_prod,harga_spesial,kode_warna,deskripsi,deskripsi_en,stok,total_hits,ratting,votes,status');
		$sql = "status = 'on' ";
		if ($this->config->item('cek_stok_front') === TRUE)
		$sql .= "AND stok > 0 ";
		$this->db->where($sql);

		$this->db->order_by('promo', 'asc');
		$query = $this->db->get($this->tbl_produk, $limit, $offset);

		return $query->result();
	}

	function getCountFrontProdukByKategori($kategori='')
	{
		$kategori = format_kategori($kategori, TRUE);

		$this->db->select('id_prod,status');
		$this->db->where_in('kategori_id', $kategori);
		$this->db->where('status', 'on');
		if ($this->config->item('cek_stok_front') === TRUE)
		$this->db->where('stok >', '0');
		$this->db->order_by('id_prod','asc');
		$query = $this->db->get($this->tbl_produk);
		
		return $query->num_rows();
	}

	function getFrontProdukByKategori($kategori='', $limit=0, $offset=0, $tautan = FALSE)
	{
		if ($tautan === TRUE)
		{
			$q = "SELECT id_prod,kode_prod,id_prod_img,nama_prod,nama_prod_en,anak_untuk_jenis,nama_jenis_anak,nama_jenis_anak_en,kode_prod,tgl_update,promo,kategori_id,diskon,harga_prod,harga_spesial,kode_warna,deskripsi,deskripsi_en,stok,total_hits,ratting,votes,status
					 FROM ".$this->tbl_produk." WHERE concat(',', kategori_id, ',') LIKE concat(', %".trim($kategori)."%,' )";
			$query = $this->db->query($q);
		}
		else {
			$kategori = format_kategori($kategori, TRUE);

			$this->db->select('id_prod,kode_prod,id_prod_img,nama_prod,nama_prod_en,anak_untuk_jenis,nama_jenis_anak,nama_jenis_anak_en,kode_prod,tgl_update,promo,kategori_id,diskon,harga_prod,harga_spesial,kode_warna,deskripsi,deskripsi_en,stok,total_hits,ratting,votes,status');
			$this->db->where_in('kategori_id', $kategori);
			$this->db->where('status', 'on');
			if ($this->config->item('cek_stok_front') === TRUE)
			$this->db->where('stok >', '0');
			$this->db->order_by('id_prod','asc');
			$this->db->limit((int)$limit, (int)$offset);

			$query = $this->db->get($this->tbl_produk);
		}
  
		if ($query->num_rows > 0)
		{
			return $query->result_array();
		}
		else {
			return NULL;
		}
	}

	function getAllSitemapFrontProdukByKategori($bhs='id',$kategori='', $limit=0, $offset=0)
	{
		$kategori = format_kategori($kategori, TRUE);

		$this->db->select('id_prod,kode_prod,id_prod_img,nama_prod,nama_prod_en');

		if($bhs == 'id')
		$this->db->select('id_prod,nama_prod AS judul');
		else
		$this->db->select('id_prod,nama_prod_en AS judul');
		
		$this->db->where_in('kategori_id', $kategori);
		$this->db->where('status', 'on');
		if ($this->config->item('cek_stok_front') === TRUE)
		$this->db->where('stok >', '0');
		$this->db->order_by('id_prod','asc');
		$this->db->limit((int)$limit, (int)$offset);

		$query = $this->db->get($this->tbl_produk);
  
		return $query->result();
	}

	function getPropertiIndukById($pid=0)
	{
		$this->db->select('nama_prod,nama_prod_en,kode_prod,tgl_update,promo,kategori_id,harga_prod,harga_spesial,diskon,deskripsi,deskripsi_en,ratting,votes');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function getAllPropertiIndukById($pid=0)
	{
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function getAllDetailProdById($pid='')
	{
		$this->db->select('nama_prod,nama_prod_en,kode_prod,kode_warna,harga_prod,harga_spesial,diskon,ratting,votes');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			$query = $this->db->get_where($this->tbl_produk_lama, array('id_prod' => (int)$pid), 1);
			if ($query->num_rows() > 0)
				return $query->row();
			else
				return NULL;
		}
	}

	function getAllDetailFrontProdByNama($nama='')
	{
		$this->db->select('id_prod,nama_prod,nama_prod_en,kode_prod,tgl_update,promo,kategori_id,kode_warna,deskripsi,deskripsi_en,ratting,votes');
		$this->db->where('nama_prod', $nama);
		$this->db->or_where('nama_prod_en', $nama);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function getFrontLabelProdById($id='',$bhs='id')
	{
		if($bhs === 'id')
		{
			$this->db->select('nama_prod');
		}
		else {
			$this->db->select('nama_prod_en');
		}
		
		$this->db->where('id_prod', (int)$id);

		$this->db->limit(1);
		$query = $this->db->get($this->tbl_produk);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			if($bhs === 'id')
			return $row->nama_prod;
			else
			return $row->nama_prod_en;
		}
		else {
			return NULL;
		}
	}

	function upProdukHits($pid=0)
	{
		$this->db->select('total_hits');
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => (int)$pid));
		if ($query->num_rows() > 0)
		{
		   $row = $query->row();
   
		   $data = array( 'total_hits' => $row->total_hits+1 );
		   $this->db->update($this->tbl_produk, $data, array('id_prod' => (int)$pid));
		} 
	}

	function getAllFrontWarnaProdukById($id=0)
	{
		$cek = $this->db->get_where($this->tbl_produk, array('id_prod' => $id,'parent_id_prod !=' => ''));
		$this->db->select('id_prod,kode_warna');
		$this->db->order_by('id_prod','asc');
		if ($this->config->item('cek_stok_front') === TRUE)
		{
			$this->db->where('stok !=','0');
		}
		$this->db->where('status','on');
		$this->db->where('id_prod', (int)$id);
		if($cek->num_rows()>0){
			foreach($cek->result() as $row){
				$partid = $row->parent_id_prod;
				$this->db->or_where('id_prod', $partid);
			}
		}
		else
		$this->db->or_where('parent_id_prod', (int)$id);

		$query = $this->db->get($this->tbl_produk);
		return $query->result();
	}

	function getAllImgFrontById($id_img='')
	{
		$this->db->select('id_img,img_src,alt_text_img');
		$this->db->order_by('alt_text_img','asc');
		$query = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_img));
		return $query->result();
	}

	function getAltImgById($id_img='')
	{
		$this->db->select('alt_text_img');
		$this->db->order_by('id_prod_img','asc');
		$query = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_img), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return substr($row->alt_text_img,11);
		}
		else {
			return NULL;
		}
	}

	function getImgByProdId($id_img='')
	{
		$this->db->select('id_prod_img');
		$query = $this->db->get_where($this->tbl_produk, array('id_prod' => $id_img), 1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			$id_prod_img = $row->id_prod_img;
			//return $id_prod_img;
			$this->db->select('img_src');
			//$this->db->order_by('id_prod_img','asc');
			$imgquery = $this->db->get_where($this->tbl_img_produk, array('id_prod_img' => $id_prod_img), 1);
			if ($imgquery->num_rows() > 0)
			{
			   $imgrow = $imgquery->row();
			   return $imgrow->img_src;
			}
			else {
				return NULL;
			}
		}
		else {
			return NULL;
		}
	}

}

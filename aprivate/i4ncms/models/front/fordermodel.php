<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class Fordermodel extends MY_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getPembeliById($cid=0)
	{
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_pembeli,array('id' => (int)$cid));
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	function getNamaPembeliById($cid=0)
	{
		$this->db->select('nama_lengkap');
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_pembeli,array('id' => (int)$cid));
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return dekodeString($row->nama_lengkap);
		}
		else {
			return NULL;
		}
	}

	function tambahPembeli()
	{
		/* Sys generated scure pass */
		//$kunci_pas = '123456';
		$nama = $this->input->post('nama_cust', TRUE);
		$kunci_pas = random_string('alnum', 6);

		$cust_email = $this->input->post('email', TRUE);
		//$cust_id = strstr($cust_email, '@', true);
		$cust_id = underscore($nama);

		$no_telpon = $this->input->post('no_telp', TRUE);
		$kode_pos = $this->input->post('kode_pos', TRUE);
		$id_negara = $this->input->post('negara', TRUE);
		$id_provinsi = $this->input->post('provinsi', TRUE);
		$id_wilayah = $this->input->post('wilayah', TRUE);
		$alamat_rumah = trim(str_ireplace("\\n", "<br />",$this->input->post('alamat', TRUE)));

		/* enkripsi */
		$ec_uname = enkodeString($cust_id);
		$ec_pass = enkodeString($kunci_pas);
		$ec_email = enkodeString(trim($cust_email));
		$ec_nama = enkodeString($nama);
		$ec_tlp = enkodeString($no_telpon);
		$ec_kode_pos = enkodeString($kode_pos);
		$ec_alamat = enkodeString(trim($alamat_rumah));

		/* set email kirim */
		$this->session->set_userdata('email_beli',bs_kode($cust_email));

		$data = array(
					'nama_lengkap' => $ec_nama,
					'email_cust' => $ec_email,
					'no_telpon' => $ec_tlp,
					'id_negara' => $id_negara,
					'id_provinsi' => $id_provinsi,
					'id_wilayah' => $id_wilayah,
					'kode_pos' => $ec_kode_pos,
					'alamat_rumah' => $ec_alamat
				);

		if($this->cekAdaPembeli($cust_id, $cust_email) === TRUE)
		{
			$pembeli = array_merge($data, array('tgl_daftar' => date('Y-m-d H:i:s'),'id_cust_uname' => $ec_uname,'kunci_pas' => $ec_pass));

			// kirim email untuk user_id dan passwordnya
			$judul = 'Detail Akun website '.$this->config->item('site_name');
			$isi = format_isi_email_pembeli_baru(current_lang(false),$nama,$cust_id,$kunci_pas);
			kirim_email_user(array(), $cust_email,$judul,$isi);

			if ( ! $this->db->insert($this->tbl_pembeli,$pembeli) )
			{
				return FALSE;
			}
			else {
				// tambahkan session auto login.
				$custdata = array(
					'kunci_id' => bs_kode($this->db->insert_id()),
					'usrid'  => bs_kode($cust_id),
					'kunci'  => bs_kode($cust_email),
					'nmusr'  => bs_kode($nama)
				);
				$this->session->set_userdata($custdata);

				$alamat = array_merge($data, array('id_cust' => bs_kode($this->session->userdata('kunci_id'), TRUE), 'tgl_buat' => date('Y-m-d H:i:s'), 'status_alamat' => 'utama'));
				if ( ! $this->db->insert($this->tbl_alamat_kirim,$alamat) )
				{
					return FALSE;
				}
				else {
					$this->session->set_userdata('alamat_id',bs_kode($this->db->insert_id()));
					return TRUE;
				}
			}
		}
		else {
			if ( $this->cekAdaAlamat(bs_kode($this->session->userdata('kunci_id'), TRUE),$nama,$cust_email,$no_telpon,$kode_pos,$id_negara,$id_provinsi,$id_wilayah,$alamat_rumah) === TRUE)
			{
				$alamat = array_merge($data,array('id_cust' => bs_kode($this->session->userdata('kunci_id'), TRUE), 'tgl_buat' => date('Y-m-d H:i:s'), 'status_alamat' => 'alternatif'));
				if ( $this->db->insert($this->tbl_alamat_kirim,$alamat) )
				{
					$this->session->set_userdata('alamat_id',bs_kode($this->db->insert_id()));
					$status = TRUE;
				}
				else {
					$status = FALSE;
				}
				if ( ! $this->db->update($this->tbl_pembeli, $data, array('id' => bs_kode($this->session->userdata('kunci_id'), TRUE))) )
				{
					$status = FALSE;
				}
				else {
					$status = TRUE;
				}
				
				return $status;
			}
			else {
				if ( ! $this->db->update($this->tbl_pembeli, $data, array('id' => bs_kode($this->session->userdata('kunci_id'), TRUE))) )
				{
					return FALSE;
				}
				else {
					return TRUE;
				}
			}
		}
	}

	function cekAdaPembeli($cust_id='',$cust_email='')
	{
		$this->db->select('id, nama_lengkap, id_cust_uname, email_cust');
		$query = $this->db->get($this->tbl_pembeli);

		$cek = TRUE;
		if($query->num_rows() > 0)
		{
			if($this->session->userdata('kunci_id') === FALSE )
			{
				foreach ($query->result_array() as $cust)
				{
					if(dekodeString($cust['email_cust']) == $cust_email )
					{
						$custdata = array(
							'kunci_id' => bs_kode($cust['id']),
							'usrid'  => bs_kode($cust_id),
							'kunci'  => bs_kode($cust_email),
							'nmusr'  => bs_kode(dekodeString($cust['nama_lengkap']))
						);
						$this->session->set_userdata($custdata);
						$cek = FALSE;
						//break;
					}
				}
			}
			else {
				$cek = FALSE;
			}
		}
		else {
			$cek = TRUE;
		}
		
		return $cek;
	}

	function cekAdaAlamat($id_cust=0,$nama='',$cust_email='',$no_telpon='',$kode_pos,$id_negara='',$id_provinsi='',$id_wilayah='',$alamat_rumah='')
	{
		$query = $this->db->get_where($this->tbl_alamat_kirim, array('id_cust' => (int)$id_cust));

		$cek = TRUE;
		if($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $kirim)
			{
				if(dekodeString($kirim['nama_lengkap']) === $nama && dekodeString($kirim['email_cust']) === $cust_email && dekodeString($kirim['no_telpon']) === $no_telpon && dekodeString($kirim['kode_pos']) === $kode_pos && $kirim['id_negara'] === $id_negara && $kirim['id_provinsi'] === $id_provinsi && $kirim['id_wilayah'] === $id_wilayah && dekodeString($kirim['alamat_rumah']) === $alamat_rumah)
				{
					$this->session->set_userdata('alamat_id',bs_kode($kirim['id_alamat']));
					$cek = FALSE;
					//break;
				}
			}
		}
		else {
			$cek = TRUE;
		}

		return $cek;
	}

	function cekStokProdukById($pid=0)
	{
		$this->db->select('stok');
		$this->db->where('id_prod', (int)$pid);
		$query = $this->db->get($this->tbl_produk);
		if($query->num_rows > 0)
		{
			$row = $query->row();
			return $row->stok;
		}
		else {
			return NULL;
		}
	}

	function upJmlOrderCust($cid=0)
	{
		// total
		$dtcust = $this->getTotalOrderCust((int)$cid);
		$jml_trans = (int) ( $dtcust->jml_transaksi + (int)$this->cart->total_items() );
		$ttl_trans = (int) ( $dtcust->total_transaksi + (int)$this->cart->total() );
		
		$data = array (
					'jml_transaksi' => (int)$jml_trans,
					'total_transaksi' => (int)$ttl_trans
					);
		if( ! $this->db->update($this->tbl_pembeli, $data, array('id' => (int)$cid)))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function getTotalOrderCust($cid=0)
	{
		$this->db->select('jml_transaksi, total_transaksi');
		$this->db->limit(1);
		$query = $this->db->get_where($this->tbl_pembeli,array('id' => (int)$cid));
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else {
			return NULL;
		}
	}

	function tambahOrderById($sts='pembayaran')
	{
		/*Sys generated code*/
		$this->db->select_max('id');
		$qtrx = $this->db->get($this->tbl_order);
		if($qtrx->num_rows > 0)
		{
			$mx_trx = $qtrx->row();
			$trx_ke = ($mx_trx->id + 1);
		} else {
			$trx_ke = 1;
		}

		$no_struk = 'TRX'.date('my').$trx_ke;
		$metode = $this->input->post('metode',TRUE);
		$pilihan = $this->input->post('pilihan',TRUE);
		$notes = strip_tags($this->input->post('notes',TRUE));

		// cek keranjang
		if($this->cart->total_items() > 0 && $this->session->userdata('kunci_id'))
		{
			if( ! empty($diskon))
			{
				$bayar = '';
			}

			foreach ($this->cart->contents() as $items)
			{
				$data = array(
						'id_cust' => (int)bs_kode($this->session->userdata('kunci_id'), TRUE),
						'id_alamat' => (int)bs_kode($this->session->userdata('alamat_id'), TRUE),
						'no_struk' => $no_struk,
						'metode_bayar' => $metode,
						'bayar_melalui' => $pilihan,
						'id_prod' => (int)$items['id'],
						'detail_produk' => $this->mproduk->getFrontLabelProdById($items['id']),
						'tgl_transaksi' => date('Y-m-d H:i:s'),
						'jml_pesan' => (int)$items['qty'],
						'total_harga' => (int)$items['subtotal'],
						'status_transaksi' => ($metode === 'online')?'pending':$sts,
						'notes_cust' => $notes
					);

				if($this->db->insert($this->tbl_order,$data))
				{
					//update stok produk
					$pstok = $this->cekStokProdukById((int)$items['id']);
					$jstok = (int) ($pstok - (int)$items['qty']);

					$data = array('stok' => $jstok);
					if( ! $this->db->update($this->tbl_produk, $data, array('id_prod' => (int)$items['id'])) )
					{
						return FALSE;
					}
				}
				else {
					return FALSE;
				}
			}

			//set session trx
			$this->session->set_userdata('trx_order', $no_struk);

			//masukkan diskon transaksi jika > 0
			if( bs_kode($this->session->userdata('diskon_transaksi'), TRUE) > 0)
			{
				$diskon = array('no_struk' => $no_struk, 'total_diskon' => bs_kode($this->session->userdata('diskon_transaksi'), TRUE));
				if( ! $this->db->insert($this->tbl_diskon, $diskon))
				return FALSE;
			}

			//update transaksi cust
			if( ! $this->upJmlOrderCust(bs_kode($this->session->userdata('kunci_id'), TRUE)))
			{
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		else {
			return FALSE;
		}
	}

	// frontpage order
	function getFrontAllOrderanByTrx($trx='',$email=FALSE)
	{
		if($email)
		$this->db->select('id_cust,id_alamat,no_struk,tgl_transaksi,metode_bayar,bayar_melalui,detail_produk,jml_pesan,total_harga,status_transaksi');

		$this->db->order_by('total_harga','desc');
		$this->db->where('no_struk', $trx);
		$query = $this->db->get($this->tbl_order);
		if($query->num_rows > 0)
		{
			if($email)
			return $query->result_array();
			else
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	// Metode Bayar
	function getMetodeBayarByWilayah($id_wilayah=0)
	{
		$sql1 = "SELECT id,tipe_metode,label_id,label_en
				FROM ".$this->tbl_metode_bayar." WHERE status = 'on' AND (id_wilayah = '' OR id_wilayah IS NULL)
				GROUP BY tipe_metode";
		$query1 = $this->db->query($sql1);
		$rs1 = $query1->result_array();

		$sql2 = "SELECT id,tipe_metode,label_id,label_en
				FROM ".$this->tbl_metode_bayar." WHERE status = 'on' AND FIND_IN_SET('$id_wilayah',id_wilayah)
				GROUP BY tipe_metode";
		$query2 = $this->db->query($sql2);
		$rs2 = $query2->result_array();

		return array_merge($rs1,$rs2);
	}

	function getVendorBayarByWilayah($id_wilayah=0)
	{
		$sql1 = "SELECT id,tipe_metode,nama_vendor,logo
				FROM ".$this->tbl_metode_bayar." WHERE status = 'on' AND (id_wilayah = '' OR id_wilayah IS NULL)";
		$query1 = $this->db->query($sql1);
		$rs1 = $query1->result_array();

		$sql2 = "SELECT id,tipe_metode,nama_vendor,logo
				FROM ".$this->tbl_metode_bayar." WHERE status = 'on' AND FIND_IN_SET('$id_wilayah',id_wilayah)";
		$query2 = $this->db->query($sql2);
		$rs2 = $query2->result_array();

		return array_merge($rs1,$rs2);
	}

	function getDetailVendorBayarByNama($nama='')
	{
		$this->db->where('nama_vendor',$nama);
		$query = $this->db->get('tbl_ecom_metode_bayar',1);

		return $query->result_array();
	}

}

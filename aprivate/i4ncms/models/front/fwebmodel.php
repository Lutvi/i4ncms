<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class FWebmodel extends MY_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getCssTema($tipe=FALSE)
    {
        if( ! $tipe)
        {
            $this->db->select('css');
        }
        else {
            $this->db->select('css_ie');
        }
        $this->db->where('nama_tema', $this->config->item('theme_id'));
        $this->db->where('status', 'on');
        $this->db->where('versi', $this->config->item('cms_version'));
        
        $query = $this->db->get($this->tbl_tema);
        
        if($query->num_rows() > 0)
        {
            $row = $query->row(); 
            if( ! $tipe)
            {
                return $row->css;
            }
            else {
                return $row->css_ie;
            }
        }
        else {
            return array();
        }
    }
    
    function getJsTema($tipe=FALSE)
    {
        if( ! $tipe)
        {
            $this->db->select('js');
        }
        else {
            $this->db->select('js_ie');
        }
        $this->db->where('nama_tema', $this->config->item('theme_id'));
        $this->db->where('status', 'on');
        $this->db->where('versi', $this->config->item('cms_version'));
        
        $query = $this->db->get($this->tbl_tema);
        
        if($query->num_rows() > 0)
        {
            $row = $query->row(); 
            if( ! $tipe )
            {
                return $row->js;
            }
            else {
                return $row->js_ie;
            }
        }
        else {
            return array();
        }
    }

    function getFrontAllHbanner()
    {
		if(current_lang(false) === 'id')
		$this->db->select('img_src, nama_id AS nama, ket_id AS ket, url_target');
		else
		$this->db->select('img_src, nama_en AS nama, ket_en AS ket, url_target');
		$this->db->order_by("posisi", "asc");
		$this->db->where('status', 'on');
        $query = $this->db->get($this->tbl_banner_header);
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else {
            return NULL;
        }
    }

    function getFrontAllIklan($posisi='',$limit=5)
    {
		if(current_lang(false) === 'id')
		$this->db->select('id,tampil,hits,tgl_berakhir,img_src,url_target, nama_id AS nama');
		else
		$this->db->select('id,tampil,hits,tgl_berakhir,img_src,url_target, nama_en AS nama');
		$this->db->order_by("tgl_terbit", "desc");

		$this->db->where('posisi', $posisi);
		$this->db->where('status', 'on');
		$this->db->limit($limit);
        $query = $this->db->get($this->tbl_banner_iklan);
        if($query->num_rows() > 0)
        {
			foreach($query->result() as $row)
			{
				//cek expired
				if(time() > strtotime($row->tgl_berakhir))
				{
					$status = array( 'status' => 'off' );
					$this->db->where('id', $row->id);
					$this->db->update($this->tbl_banner_iklan, $status);
				}

				$data = array( 'tampil' => $row->tampil+1 );
				if($this->config->item('iklan'))
				{
					$this->db->where('id', $row->id);
					$this->db->update($this->tbl_banner_iklan, $data);
				}
				else {
					$this->db->where('posisi', 'atas');
					$this->db->where('id', $row->id);
					$this->db->update($this->tbl_banner_iklan, $data);
				}
			}

            return $query->result();
        }
        else {
            return NULL;
        }
    }

    function upIklanHits($id='',$hit='')
    {
		$id = bs_kode($id, TRUE);
        $data = array(
               'hits' => (int)$hit + 1
            );
        $this->db->where('id', (int)$id);
        if( ! $this->db->update($this->tbl_banner_iklan, $data) )
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

}

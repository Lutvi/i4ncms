<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class  MY_Controller  extends  MX_Controller  {

	public $data = array();
	public $maxAt = 10;
	public $swb;
	public $bahasa;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('main/mainwebmodel','mweb');
		$this->load->model('main/mainalamatmodel','alamat');
		$this->load->model('main/logmodel','logs');
		$this->load->model('main/addonsmodel','maddons');

		//if($this->uri->segment(1) == 'admin')
		//$this->config->set_item('csrf_protection', FALSE);

		#$this->_sycConfignya();
	}

	// Sinkronisasi Konfigurasi website
	function _sycConfignya()
	{
		$conf = $this->mweb->initConfigWeb();
		foreach ($conf as $item)
		{
			if($this->config->item($item['nama_config']) !== $item['nilai_config'])
			{
				// set tipe datanya
				if( $item['tipe_data'] === 'string' )
				$this->config->set_item($item['nama_config'], $item['nilai_config']);
				if( $item['tipe_data'] === 'angka' )
				$this->config->set_item($item['nama_config'], (int)$item['nilai_config']);
				if( $item['tipe_data'] === 'desimal' )
				$this->config->set_item($item['nama_config'], abs($item['nilai_config']));
				if( $item['tipe_data'] === 'array' )
				$this->config->set_item($item['nama_config'], (array)$item['nilai_config']);
				if( $item['tipe_data'] === 'boolean' )
				$this->config->set_item($item['nama_config'], (bool)$item['nilai_config']);
				if( $item['tipe_data'] === 'json' )
				$this->config->set_item($item['nama_config'], json_encode($item['nilai_config']));
				if( $item['tipe_data'] === 'serialize' )
				$this->config->set_item($item['nama_config'], unserialize($item['nilai_config']));
			}
		}

		// Tema
		$tema = $this->mweb->getTemaWeb();
		foreach ($tema as $item)
		{
			$this->config->set_item('theme_id', $item['nama_tema']);
			$this->config->set_item('is_respon', (bool)$item['is_responsive']);
			$this->config->set_item('slider_menu', (bool)$item['slider_menu']);
			$this->config->set_item('opsi_conf_tema', json_decode($item['opsi_conf'], true, 5));
			//$this->config->set_item('opsi_conf_tema', $item['opsi_conf']);
			$this->config->set_item('nilai_opsi_tema', json_decode($item['nilai_opsi'], true));
		}
	}

	// jalankan Pengaturan Bahasa
	function _jalankanBahasanya()
	{
		$this->config->set_item('language_abbr', current_lang(false));
		$lang_uri_abbr = config_item('lang_uri_abbr');
		$user_lang = $lang_uri_abbr[current_lang(false)];
		$this->config->set_item('language', $user_lang);
		$this->lang->load('frontpage', $user_lang);
		//$this->load->helper('language');
		//print_r(config_item('language_abbr'));
	}

	function _deteksiAsalUser($tipe_halaman = '')
	{
		if ($this->agent->is_referral())
		{
			// cek dulu lah..!
			#preg_match('@^(?:http://|https://)?([^/]+)@i', $this->agent->referrer(), $cocok);
			// simpan datanya di database
			#if( $cocok[1] !== $_SERVER['HTTP_HOST'])
			$this->logs->tambahLogRef($tipe_halaman);
		}
		if($this->agent->is_robot())
		{
			$this->logs->tambahLogRobot($tipe_halaman);
		}
	}

	// redirect ke https hanya untuk browser aja
	function _jalankanHTTPS($cek=FALSE)
	{
		if ($this->agent->is_browser() || $this->agent->is_mobile() || $this->uri->segment(2) !== 'sitemap')
		{
			if ($cek)
			{
				if ($this->config->item('pake_ssl'))
				{
					if ( ! isset( $_SERVER["HTTPS"] ) || $_SERVER['HTTPS'] != "on")
					{
						$redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
						header("Location: $redirect");
					}
					else {
						return;
					}
				}
				else {
					if ( isset( $_SERVER["HTTPS"] ) && $_SERVER['HTTPS'] == "on")
					{
						unset($_SERVER['HTTPS']);
						$redirect = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
						header("Location: $redirect");
					}
					else {
						return;
					}
				}
			}
			else {
				if ( ! isset( $_SERVER["HTTPS"] ) || $_SERVER['HTTPS'] !== "on")
				{
					$redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					header("Location: $redirect");
				}
			}
		}
		else {
			if ( isset( $_SERVER["HTTPS"] ) && $_SERVER['HTTPS'] === "on")
			{
				unset($_SERVER['HTTPS']);
				$redirect = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
				header("Location: $redirect");
			}
		}
	}

	// Cache halaman front only
	function _getCachenya($tipe='',$datax=array())
	{
		// log asalnya user
		$this->_deteksiAsalUser($tipe);

		$halaman_cache = array('blog','album','video','halaman','module','statis');
		if ( $this->config->item('cache_front') === TRUE)
		{
			if(in_array($tipe, $halaman_cache)) {
				/* cache halaman */
				$this->load->driver('cache', array('adapter' => 'file'));

				$nm_file = $this->uri->segment(1,'bs')."-".$this->uri->segment(2,'p0')."-".$this->uri->segment(3,'p1')."-".$this->uri->segment(4,'p2')."-".$this->uri->segment(5,'p3')."-".$this->uri->segment(6,'p4');
				$cached_item = $tipe.'/'.$nm_file;

				if ( ! is_dir(config_item('cache_path').$tipe) )
				mkdir(config_item('cache_path').$tipe, DIR_CMS_MODE);

				if ( ! $isi_konten = $this->cache->get($cached_item))
				{
					$isi_konten = $datax;
					$this->cache->save($cached_item, $datax, config_item('lama_cache_front'));
				}

				return $this->cache->get($cached_item);
			}
			else {
				return $datax;
			}
		}
		else {
			return $datax;
		}
	}

	// mencari nilai offset untuk pagination dgn set "use_page_numbers" => TRUE
	function getOffsetPage($segment, $per='')
	{
		if ( $segment === FALSE || $segment == 1 || $segment == 0 )
		{
			$offset = 0;
		}
		else {
			$per_page = ($per==''?$this->adm_per_halaman:$per);
			$offset = ($segment-1)*$per_page;
		}

		return $offset;
	}

	function _getNomorPage($offset)
	{
		preg_match_all('/^page+([\d]+)$/', $offset, $matches);
		if ( ! empty($matches[1][0]))
		return $matches[1][0];
		else
		return '';
	}

	public function list_wilayah()
    {
        $data = $this->maddons->listKotaById(trim($this->input->post('idp', TRUE)));
        foreach ($data as $isi)
        {
            echo '<option id="wilayah" name="wilayah" value="'.$isi->id_kab_kota.'">&nbsp;'. humanize($isi->kab_kota) .'&nbsp;</option>'."\n";
        }
    }

	// cek-keranjang
	function _cek_stok_order()
	{
		$info = array();
		if ($this->cart->total_items() > 0)
		{
			$i=0;
			foreach ($this->cart->contents() as $items)
			{
				$keranjang = array();
				$cek = $this->mproduk->cekStokProduk((int)$items['id']);
				
				if ($cek === NULL)
				{
					if (current_lang(false) === 'id')
					$info['cso'.$i] = 'Mohon maaf, Produk '.$items['name'].' sudah tidak tersedia di katalog.';
					else
					$info['cso'.$i] = 'Sorry, '.$items['name'].' is not available for order.';

					$keranjang = array(
								   'rowid' => $items['rowid'],
								   'qty'   => 0
								);
					$this->cart->update($keranjang);
				}
				else {
					if ($cek->stok <= 0)
					{
						if (current_lang(false) === 'id')
						$info['cso'.$i] = 'Mohon maaf, Produk '.$items['name'].' sudah dipesan oleh pengunjung lain.';
						else
						$info['cso'.$i] = 'Sorry, '.$items['name'].' is selling to another custommer.';

						$keranjang = array(
									   'rowid' => $items['rowid'],
									   'qty'   => 0
									);
						$this->cart->update($keranjang);
					}
				}
				$i++;
			}
		}

		return $info;
	}

	function _cek_stok()
	{
		$info = array();
		$i=0;
		foreach ($this->cart->contents() as $items)
		{
			$keranjang = array();
			$cek = $this->mproduk->cekStokProduk((int)$items['id']);

			if ($cek === NULL)
			{
				if (current_lang(false) === 'id')
				$info['cs'.$i] = 'Mohon maaf, Produk '.$items['name'].' sudah tidak tersedia di katalog.';
				else
				$info['cs'.$i] = 'Sorry, '.$items['name'].' is not available for order.';
				
				$keranjang = array(
							   'rowid' => $items['rowid'],
							   'qty'   => 0
							);
				$this->cart->update($keranjang);
			}
			else {
				if ($items['qty'] > $cek->stok && $cek->stok > 0)
				{
					if (current_lang(false) === 'id')
					$info['cs'.$i] = 'Mohon maaf, Produk '.$items['name'].' hanya tersedia '.$cek->stok.' unit.';
					else
					$info['cs'.$i] = 'Sorry, '.$items['name'].' just '.$cek->stok.' units in our stock.';
					
					$keranjang = array(
								   'rowid' => $items['rowid'],
								   'qty'   => $cek->stok
								);
					$this->cart->update($keranjang);
				}
			}

			$i++;
		}

		return $info;
	}

	//upgrade valid email
	function _ini_valid_email($email)
	{
		// source : http://edoceo.com/exemplar/php-email-address-validation
		// Match Email Address Pattern
		// note : fungsi hanya berjalan saat online, jika env=live tambahkan fungsi ini pada validation email
		if(ENVIRONMENT == 'production' || ENVIRONMENT == 'live')
		{
			if (preg_match('/^([\w\.\%\+\-]+)@([a-z0-9\.\-]+\.[a-z]{2,6})$/i',trim($email),$m)) 
			{
				if ( (checkdnsrr($m[2],'MX') == TRUE) || (checkdnsrr($m[2],'A') == TRUE) ) 
				{
					return TRUE;
				}
				else {
					$this->form_validation->set_message('_ini_valid_email', $this->lang->line('email_salah'));
					return FALSE;
				}
			}
			else {
				$this->form_validation->set_message('_ini_valid_email', $this->lang->line('email_salah'));
				return FALSE;
			}
		}
		else {
			return TRUE;
		}
	}

	// convert string status web
	function _stWebStr()
	{
		if($this->config->item('sts_web') == 3)
		{
			$swb = 'Online';
		}
		elseif($this->config->item('sts_web') == 2)
		{
			$swb = 'Perbaikan';
		}
		else {
			$swb = 'Offline';
		}

		return $swb;
	}

	// format SEO
	function _getSEO($keyword='',$description='',$extra='')
	{
		$extra = word_limiter(strip_tags($extra), 50,'');
		$description = word_limiter(strip_tags($description), 50);

		if(str_word_count($description) < 50 || empty($description))
		$description = $description.' '.$extra;

		if(str_word_count($keyword) < 25 || empty($keyword))
		$keyword = $keyword.' '.$extra.' '.$description;

		$data['seo'] = array(
			array('name' => 'keywords', 'content' => word_limiter(format_keyword(entities_to_ascii($keyword)),true,50,'')),
			array('name' => 'description', 'content' => word_limiter(ucfirst(rip_tags(entities_to_ascii($description))),50))
		);
		$data['mikro_dt_desc'] = word_limiter(ucfirst(rip_tags(entities_to_ascii($description))),50,'');

		return $data;
	}

	// buat img QR-CODE
	function _buatImgQrCode($url='',$size=0,$margin=0,$style='')
	{
		$this->load->spark('gc-qrcode/1.0.2');

		// set nilai
		$url = (empty($url))?base_url():$url;
		$size = ($size == 0)?130:$size;
		$margin = ($margin == 0)?1:$margin;

		$this->gc_qrcode->size($size)
                ->data($url)
                ->output_encoding('UTF-8')
                ->error_correction_level('L')
                ->margin($margin);

		$img = $this->gc_qrcode->img(array('align'=>'right','style'=>$style));

		return $img;
	}

	// untuk order
	function _buildAlamatKirim($alid)
	{
		$row = $this->alamat->getAllAlamatById($alid);

		$build['fnama'] = '<span style="font-weight:bold;color:#1A1A1A">'.ucfirst(dekodeString($row->nama_lengkap)).'</span> ('.dekodeString($row->email_cust).')';
		$build['fnotlp'] = dekodeString($row->no_telpon);
		$build['frumah'] = dekodeString($row->alamat_rumah);
		$build['fkdpos'] = dekodeString($row->kode_pos);
		/* addons */
		$build['fwil'] = $this->maddons->getNamaKotaById($row->id_wilayah).' - '.$this->maddons->getNamaProvinsiById($row->id_provinsi);
		$build['fnegara'] = strtoupper($row->id_negara);

		return $build;
	}

	// untuk email order
	function _buildAlamatKirimEmail($alid)
	{
		$row = $this->alamat->getAllAlamatById($alid);

		$build['fnama'] = 'A/N <strong>'.ucfirst(dekodeString($row->nama_lengkap)).'</strong>';
		//$build['fnotlp'] = dekodeString($row->no_telpon);
		$build['frumah'] = dekodeString($row->alamat_rumah);
		$build['fkdpos'] = dekodeString($row->kode_pos);
		/* addons */
		$build['fwil'] = $this->maddons->getNamaKotaById($row->id_wilayah).' - '.$this->maddons->getNamaProvinsiById($row->id_provinsi);
		$build['fnegara'] = strtoupper($row->id_negara);

		return $build;
	}

	// hitung diskon transaksi disini
	function _cekDiskonTransaksi($id_provinsi='')
	{
		$in_provinsi = array(28,29,30,31,32,33);
		if( ! in_array($id_provinsi, $in_provinsi))
		{
			$diskon = 1000;
		}
		else {
			$diskon = 0;
		}

		$total = $this->cart->total() - $diskon;

		$this->session->set_userdata('total_transaksi', bs_kode($total));
		$this->session->set_userdata('diskon_transaksi', bs_kode($diskon));

		return $diskon;
	}

	// untuk profil pelanggan
	function _buildAlamat($alamat=array())
	{
		if ( ! empty($alamat))
		{
			$build['fnama'] = 'A/N : <span style="font-weight:bold;color:#1A1A1A">'.ucfirst(dekodeString($alamat->nama_lengkap)).'</span> ('.dekodeString($alamat->email_cust).')';
			$build['fnotlp'] = dekodeString($alamat->no_telpon);
			$build['frumah'] = dekodeString($alamat->alamat_rumah);
			$build['fkdpos'] = dekodeString($alamat->kode_pos);
			/* addons */
			$build['fwil'] = $this->maddons->getNamaKotaById($alamat->id_wilayah).' - '.$this->maddons->getNamaProvinsiById($alamat->id_provinsi);
			$build['fnegara'] = strtoupper($alamat->id_negara);

			return $build;
		}
		else {
			return NULL;
		}
	}

/* MICS */
	//hapus cache
	function _hapus_cache($tipe='web')
	{
		$this->load->helper('file');

		if($tipe == 'web')
		delete_files('./_tmp/web/');
	}

	// cek besar data directory dlm satuan byte
	function _cekbesar($path='')
	{
		$this->load->helper('file');
		$b = get_dir_file_info($path, FALSE);

		$tl = 0;
		foreach( $b as $key => $item )
		{
			$tl += $item['size'];
		}

		return $this->_filesize_formatted((int)$tl);
	}

	function _filesize_formatted($size=0)
	{
		$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$power = $size > 0 ? floor(log($size, 1024)) : 0;
		return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
	}

	// copy file
	function _pindah_file($file, $pindah)
	{
		if ( ! copy($file, $pindah) ) 
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	// total kegagalan
	function _jum_gagal()
	{
		if( ! $this->session->userdata('ggl'))
		{
			$n =1;
		}
		else {
			//reset if max
			if($this->session->userdata('ggl') > config_item('max_pr'))
			$n =1;
			else
			$n = $this->session->userdata('ggl')+1;
		}

		$this->session->set_userdata('ggl', $n);
		return $n;
	}
/* END MICS */

/* TRACE USER */
	// tracking report by ip
	function track($sts=FALSE,$ket='',$max=0)
	{
		$ip = $this->input->ip_address() . " : " . $_SERVER['REMOTE_PORT'];
		$brow = $this->input->user_agent();
		$page = $_SERVER['REQUEST_URI'];
		$tgl = date("d-m-Y H:i:s");
		$gtw = $_SERVER['SERVER_PROTOCOL'] . " - " . $_SERVER['GATEWAY_INTERFACE'];
		$inpoh = "";

		if($ket != '')
		$inpoh .= "[<b>Error</b>] <span style='color:red'>". $ket . "</span><br />\n";

		if($this->input->is_cli_request())
		{
			$inpoh .= "[<b>CLI</b>] <span style='color:red'>". $tgl . "</span> [<small>" . $brow . "</small>] - <b>" . $ip . "</b> : <span style='color:blue'>" . $page . "</span><br />\n";
			$inpoh .= "[<b>Get-Data</b>] <span style='color:green'>". serialize($_REQUEST) . "</span><br />\n";
			$inpoh .=  $gtw . "<hr />\n";
			$rqst = serialize($_REQUEST);
			$tipe = "CLI-Request";
			$this->_catat($inpoh,TRUE);
			$this->logs->tambahLog($tipe,$ip,$brow,$page,$rqst,$ket);
		}
		elseif($this->input->post(NULL, TRUE)) {
			$inpoh .= "[<b>POST</b>] <span style='color:red'>". $tgl . "</span> [<small>" . $brow . "</small>] - <b>" . $ip . "</b> : <span style='color:blue'>" . $page . "</span><br />\n";
			$inpoh .= "[<b>POST-Data</b>] <span style='color:green'>". serialize($_POST) . "</span><br />\n";
			$inpoh .=  $gtw . "<hr />\n";
			$rqst = serialize($_POST);
			$tipe = "POST";
			#$this->_catat($inpoh,TRUE);
			$this->logs->tambahLog($tipe,$ip,$brow,$page,$rqst,$ket);
		}
		elseif($this->input->get(NULL, TRUE)) {
			$inpoh .= "[<b>GET</b>] <span style='color:red'>". $tgl . "</span> [<small>" . $brow . "</small>] - <b>" . $ip . "</b> : <span style='color:blue'>" . $page . "</span><br />\n";
			$inpoh .= "[<b>Get-Data</b>] <span style='color:green'>". serialize($_GET) . "</span><br />\n";
			$inpoh .=  $gtw . "<hr />\n";
			$rqst = serialize($_GET);
			$tipe = "GET";
			$this->_catat($inpoh);
			$this->logs->tambahLog($tipe,$ip,$brow,$page,$rqst,$ket);
		}
		elseif($this->input->is_ajax_request()) {
			$inpoh .= "[<b>AJAX</b>] <span style='color:red'>". $tgl . "</span> [<small>" . $brow . "</small>] - <b>" . $ip . "</b> : <span style='color:blue'>" . $page . "</span><br />\n";
			$inpoh .= "[<b>Request-Data</b>] <span style='color:green'>". serialize($_REQUEST) . "</span><br />\n";
			$inpoh .=  $gtw . "<hr />\n";
			$rqst = serialize($_REQUEST);
			$tipe = "AJAX-Request";
			$this->_catat($inpoh,TRUE);
			$this->logs->tambahLog($tipe,$ip,$brow,$page,$rqst,$ket);
		}
		elseif( ! $this->input->cookie(config_item('csrf_cookie_name'), TRUE)) {
			$inpoh .= "[<b>No-Set-Token</b>] <span style='color:red'>". $tgl . "</span> [<small>" . $brow . "</small>] - <b>" . $ip . "</b> : <span style='color:blue'>" . $page . "</span><br />\n";
			$inpoh .= "[<b>Cookies-Data</b>] <span style='color:green'>". serialize($_COOKIE) . "</span><br />\n";
			$inpoh .=  $gtw . "<hr />\n";
			$rqst = serialize($_COOKIE);
			$tipe = "No-Set-Token";
			#$this->_catat($inpoh);
			$this->logs->tambahLog($tipe,$ip,$brow,$page,$rqst,$ket);
		}
		else {
			$inpoh .= "[<b>404</b>] <span style='color:red'>". $tgl . "</span> [<small>" . $brow . "</small>] - <b>" . $ip . "</b> : <span style='color:blue'>" . $page . "</span><br />\n";
			$inpoh .= "[<b>Request-Data</b>] <span style='color:green'>". tsout($_REQUEST) . "</span><br />\n";
			$inpoh .=  $gtw . "<hr />\n";
			$rqst = "<br />\nCLIENT_DATA : ".tsout($_REQUEST);
			$tipe = "404-Not Found";

			if($this->agent->is_browser() || $this->agent->is_mobile()) {
				$this->_catat($inpoh);
				#$this->logs->tambahLog($tipe,$ip,$brow,$page,serialize($_COOKIE),$ket);
			}
		}

		// Send email Alert
		if($sts===TRUE && $max > 0 && cek_koneksi_internet() === TRUE)
		{
			if( ! $this->_laporgan($inpoh))
			{
				// send error directly to email (PHP Funct)
				if( ENVIRONMENT == 'live')
				error_log("Big trouble, we're underattack.! \n".config_item('site_name') ."\n".$inpoh, 1, config_item('author_email'));
			}
		}
	}

	function _catat($inpoh='',$gembok=FALSE)
	{
		if(file_exists("./ips.html")) 
		{
			if(is_writable("./ips.html"))
			{
				if(filesize('ips.html') > 1500000 && $gembok === FALSE)
				{
					$buka = fopen("ips.html", "wb");
				}
				else {
					$buka = fopen("ips.html", "ab");
				}
				fwrite($buka, $inpoh);
				fclose($buka);
			}
		}
	}

	function _laporgan($inpoh='')
	{
		$this->load->library('email');

		$this->email->from($this->config->item('info_email') , 'security-noreply');
		$this->email->to($this->config->item('author_email'));
		$this->email->cc($this->config->item('webmaster_email'));
		if($this->config->item('owner_info') === TRUE)
		{
			//$this->email->cc(config_item('owner_email'));
			$this->email->bcc(config_item('owner_email'));
		}
		$this->email->subject('[INFO] Site Security - ' . $this->config->item('site_name'));
		$this->email->message($inpoh);

		if(file_exists("./ips.html")) {
			$this->email->attach('ips.html');
		}

		if( ! $this->email->send())
		{
			$debug = "<span style='color:red;font-weight:bold;'>GAGAL Kirim Email</span> : Konfigurasi Email perlu diperbaiki." . "<hr />\n";
			$this->_catat($debug);
			$t = '[::PERINGATAN::]';
			$k = 'GAGAL Kirim Email, Konfigurasi Email perlu diperbaiki.';
			log_message('error', $t . ' ' .$k);
			$this->logs->tambahLog($t,$ip='',$brow='',$page='',$rqst='email settings',$k);

			return FALSE;
		}
		else {
			return TRUE;
		}
	}
/* END TRACE USER */

}
// end-class core/MY_Controller

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminController extends MY_Controller {

	public $web_master; // val : id-master(rahasia)
	public $super_admin; // val : TRUE or FALSE
	public $login; // val : 1 or NULL
	// set per halaman
	public $adm_per_halaman = 20; //20
	
	public $jq_style = 'Aristo'; // ui-style default: Aristo | reddanio | cupertino | start-edit | ymetro | ymetro2
	public $frame_jq_style = 'Aristo'; // frame ui-dialog default: Aristo | pepper-grinder | cupertino

	

	public function __construct()
	{
		parent::__construct();
		#$CI =& get_instance();

		// To Enable AJAX POST without compressing content
		$this->config->set_item('compress_output', FALSE);

		// Admin CMS
		$this->load->model('admin/loginadminmodel','adminlogin');
		$this->load->model('admin/cmsmodel','cms');
		$this->load->model('admin/produkmodel','mproduk');

		// Fitur
		$this->load->model('admin/fitadminmodel','admfitur');
		$this->load->model('admin/webmodel','web');
		$this->load->model('admin/mbackup','bckp');

		// Extra
		$this->load->model('admin/ordermodel','orderan');
		$this->load->model('admin/pelangganmodel','pelanggan');
		$this->load->model('admin/newslettermodel','subcribe');

		// string status web
		$this->swb = $this->_stWebStr();

		//fix tabs
		$ortab = array('order','cariorder','tblcariorder');
		if( ! in_array($this->uri->segment(2),$ortab))
		$this->session->unset_userdata('aktif_tab');

		$bantab = array('banner','caribanner','tblcaribanner');
		if( ! in_array($this->uri->segment(2),$bantab))
		$this->session->unset_userdata('aktif_tab_banner');

		$logtab = array('log_user','carilog_user','tblcarilog_user');
		if( ! in_array($this->uri->segment(2),$logtab))
		$this->session->unset_userdata('aktif_tab_log');

		$mbtab = array('atur_metode_bayar','carimetode_bayar','tblcarimetode_bayar');
		if( ! in_array($this->uri->segment(2),$mbtab))
		$this->session->unset_userdata('aktif_tab_metode_bayar');

		/* RUN Admin Mode */
		$this->login = $this->_cekLoginAdm();
		$this->super_admin = $this->_isSupAdmin();
		$this->session->set_userdata('admperpage', $this->adm_per_halaman);

		//redirect robot ke home
		if($this->agent->is_robot())
		{
			$this->logs->tambahLogRobot('admin');
			unset($_SERVER['HTTPS']);
			$redirect = "http://".$_SERVER['HTTP_HOST'];
			header("Location: $redirect");
		}

		//$this->_jalankanHTTPS();
	}

	// config super aktif -> cek di controller
	function _isSupAdmin()
	{
		if ( $this->config->item('super_aktif') === TRUE )
		{
			$sa = $this->session->userdata('superman');
			$lvl = $this->session->userdata('level');
			if( ! empty($sa) && bs_kode($lvl, TRUE) == 'superman')
			{
				$admin = TRUE;
			}
			else {
				$admin = FALSE;
			}
		}
		else {
			$admin = $this->_cekSupAdmin();
		}

		return $admin;
	}

	// cek super admin level batman
	function _cekSupAdmin()
	{
		$sa = $this->session->userdata('superman');
		$adm = $this->session->userdata('batman');
		$entry = $this->session->userdata('ironman');
		$marketing = $this->session->userdata('spiderman');
		if( ! empty($sa) || ! empty($adm) || ! empty($entry) || ! empty($marketing) )
		{
			if( ! empty($sa))
			{
				if($sa === bs_kode('letsflying'))
				return TRUE;
				else
				return FALSE;
			}

			if( ! empty($adm))
			{
				if($adm === bs_kode('spyingthis'))
				return TRUE;
				else
				return FALSE;
			}

			if( ! empty($entry))
			{
				if($entry === bs_kode('flashingtheair'))
				return TRUE;
				else
				return FALSE;
			}

			if( ! empty($marketing))
			{
				if($marketing === bs_kode('makesomebase'))
				return TRUE;
				else
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}

	// cek login admin
	function _cekLoginAdm()
	{
		$lvl = $this->session->userdata('level');
		$usr = $this->session->userdata('kunci_adm');
		$idm = $this->session->userdata('idmu');
		$idadm = $this->session->userdata('admusrid');
		$adm = $this->session->userdata('ademinaja');
		if( empty($lvl) || empty($usr) || empty($idm) || empty($idadm) )
		{
			$login = NULL;
		}
		else {
			if(dekodeString($adm) === bs_kode($idadm,TRUE))
			$login = 1;
			else
			$login = NULL;
		}

		return $login;
	}

	/* CEK STATUS LOGIN */
	function _cekStsLoginAdm()
	{
		if(is_null($this->_cekLoginAdm()))
		{
			//clean cache db
			if(config_item('cache_front') === TRUE)
			$this->db->cache_delete_all();

			redirect($this->config->item('admpath').'/login','refresh');
			#header("Location: https://".$_SERVER['HTTP_HOST']."/".$this->config->item('admpath')."/login");
		}
		else {
			$this->login = 1;
		}
	}

	// Config style pagination admin
	function _adminPagination()
	{
		$config['first_link'] = '&lsaquo;&lsaquo;';
		$config['first_tag_open'] = '<span class="page gradient">';
		$config['first_tag_close'] = '</span>';
		$config['last_link'] = '&rsaquo;&rsaquo;';
		$config['last_tag_open'] = '<span class="page gradient">';
		$config['last_tag_close'] = '</span>';
		$config['num_links'] = 4;
		$config['next_link'] = '&rsaquo;';
		$config['next_tag_open'] = '<span class="page gradient">';
		$config['next_tag_close'] = '</span>';
		$config['prev_link'] = '&lsaquo;';
		$config['prev_tag_open'] = '<span class="page gradient">';
		$config['prev_tag_close'] = '</span>';
		$config['num_tag_open'] = '<span class="page gradient">';
		$config['num_tag_close'] = '</span>';
		$config['cur_tag_open'] = '<span class="page gradient active">';
		$config['cur_tag_close'] = '</span>';
		$config['use_page_numbers'] = TRUE; // Use page number for segment instead of offset

		return $config;
	}

	/* fix pagination path yang pake tab=>[order,banner, dll] */
	function _getSegment($seg='')
	{
		if($this->uri->segment(3) === $seg)
			return 4;
		elseif($this->uri->segment(4) === $seg)
			return 5;
		else
			return 6;
	}

	function tutup_dialog($info='',$timeout=3000)
	{
		$data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array('themes/'.$this->jq_style.'/jquery-ui.css');
        $data['info'] = $info;
        $data['timeout'] = $timeout;

        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tutup_dialog',$data);
	}

	function tutup_dialog_gagal($info='',$timeout=5000)
	{
		$data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array('themes/'.$this->jq_style.'/jquery-ui.css');
        $data['info'] = $info;
        $data['timeout'] = $timeout;

        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tutup_dialog_gagal',$data);
	}

	function _hpsQryCache($tipe='')
	{
		/*$this->load->helper('file');
		
		if($tipe === 'produk') {
			delete_files('./_tmp/tbl-cache/id+petail', TRUE);
			delete_files('./_tmp/tbl-cache/en+petail', TRUE);
		}
		if($tipe === 'blog') {
			delete_files('./_tmp/tbl-cache/id+posting', TRUE);
			delete_files('./_tmp/tbl-cache/en+posting', TRUE);
		}
		if($tipe === 'album') {
			delete_files('./_tmp/tbl-cache/id+gallery', TRUE);
			delete_files('./_tmp/tbl-cache/en+gallery', TRUE);
		}
		if($tipe === 'video') {
			delete_files('./_tmp/tbl-cache/id+playlist', TRUE);
			delete_files('./_tmp/tbl-cache/en+playlist', TRUE);
		}*/
	}

	function _getAdminAssets($module=FALSE,$widget=FALSE)
    {
        $data['meta'] = array(
            array('name' => 'robots', 'content' => 'noindex nofollow'),
            array('name' => 'author', 'content' => $this->config->item('author_name')),
			array('name' => 'application-name', 'content' => $this->config->item('site_name')),
			array('name' => 'distribution', 'content' => $this->config->item('site_distribution')),
			array('name' => 'generator', 'content' => $this->config->item('generator_site'))
        );
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','themes/'.$this->jq_style.'/jquery-ui.css','elem/table-style.css');
        $data['css_ie'] = array('elem/IEmain-min.css');
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js');
        $data['js_ie'] = array('PIE-1.0.0/PIE.js');

        if($module===TRUE)
        $data['assets_module'] =  $this->cms->getModuleAsetsAdmin();
        else
        $data['assets_module'] =  array();

        if($widget===TRUE)
        $data['assets_widget'] =  $this->cms->getWidgetAsetsAdmin();
        else
        $data['assets_widget'] =  array();

        return $data;
    }

	function _getKeywordList($kat=array(),$bhs='id')
	{
		if (empty($_GET['term'])) exit;
		$q = strtolower($_GET["term"]);
		// remove slashes if they were magically added
		if (get_magic_quotes_gpc()) $q = stripslashes($q);

		$result = array();
		$index = 1;
		if( $bhs === 'id')
		{
			foreach($kat as $items)
			{
				if (strpos(strtolower($items->label_id), $q) !== false) {
					array_push($result, array("id"=>$index, "label"=>$items->label_id, "value" => format_keyword($items->label_id)));
				}
				if (count($result) > 11)
					break;
				$index++;
			}
		}
		else {
			foreach($kat as $items)
			{
				if (strpos(strtolower($items->label_en), $q) !== false) {
					array_push($result, array("id"=>$index, "label"=>$items->label_en, "value" => format_keyword($items->label_en)));
				}
				if (count($result) > 11)
					break;
				$index++;
			}
		}

		return $result;
	}

	public function list_keyword_id()
	{
		$kat = $this->cms->getKeyword();
		$result = $this->_getKeywordList($kat);

		echo json_encode($result);
	}

	public function list_keyword_en()
	{
		$bhs = 'en';
		$kat = $this->cms->getKeyword($bhs);
		$result = $this->_getKeywordList($kat,$bhs);

		echo json_encode($result);
	}

	public function tambah_keyword()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nama_kat_id', 'Nama kategori ID', 'trim|required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('nama_kat_en', 'Nama kategori EN', 'trim|required|min_length[3]|max_length[50]');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$data = array('status'=>'Gagal, Data yang dikirim tidak valid.');
		}
		else {
			if( ! $this->cms->cekKeyword($this->input->post('nama_kat_id'),'id'))
			{
				$data = array('status'=>'Gagal, Nama keyword "'.ucwords($this->input->post('nama_kat_id')).'" sudah ada.');
			}
			elseif( ! $this->cms->cekKeyword($this->input->post('nama_kat_en'),'en'))
			{
				$data = array('status'=>'Gagal, Nama keyword "'.ucwords($this->input->post('nama_kat_en')).'" sudah ada.');
			}
			else {
				if( ! $this->cms->addKeyword() )
				{
					$data = array('status'=>'Gagal menambahkan data ke database.');
				}
				else {
					//echo 'sukses';
					$data = array('status'=>'sukses', 'list'=>array(0,7));
				}
			}
		}
		echo json_encode($data);
	}

	function list_tag()
    {
		$tipe = $this->uri->segment(4,'');
		$count = $this->cms->getTags($tipe);
		if(count($count)>0){
		foreach($count as $row)
		{
			$data[] = array( 'id' => (int)$row->id_kategori,
							'name' => (string)$row->label_id );
		}
		}
		else {
		$data[0] = array( 'id' => '',
							'name' => '' );
		}
		echo json_encode($data);
    }

    function list_kategori()
    {
		$tipe = $this->uri->segment(4,'');
		$count = $this->cms->getKategori($tipe);
		foreach($count as $row)
		{
			$data[] = array( 'id' => (int)$row->id_kategori,
							'name' => (string)$row->label_id );
		}
		echo json_encode($data);
    }

    function list_idwilayah()
    {
		$count = $this->maddons->listKota();
		foreach($count as $row)
		{
			$data[] = array( 'id' => (int)$row->id_kab_kota,
							'name' => (string)humanize(rip_tags($row->kab_kota)) );
		}
		echo json_encode($data);
    }

    function editorBlogBagus($id='contenx')
	{
		$this->load->helper('ckeditor');
 
		//Ckeditor's configuration
		$data[$id] = array(

			//ID of the textarea that will be replaced
			'id' 	=> 	$id,
			'path'	=>	'_plugins/WYSIWYG/ckeditor',

			//Optionnal values
			'config' => array(
								'language' => 'id',
								'height' => 450,
								'magicline_color' => '#F3007D',
								'extraPlugins' => 'syntaxhighlight',
								'toolbar' 	=> 	array(
													array('Maximize', 'ShowBlocks','-','Source', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'Preview', 'Print', '-', 'Templates'),
													array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat','-','Link','Unlink', 'Anchor'),
													array('Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
													array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'),
													array('Syntaxhighlight'),
													array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
													array('Styles', 'Format', 'Font', 'FontSize','TextColor', 'BGColor')
											)
						)
		);

		return $data;
	}

	function editorBagus($id='contenx')
	{
		$this->load->helper('ckeditor');
 
		//Ckeditor's configuration
		$data[$id] = array(

			//ID of the textarea that will be replaced
			'id' 	=> 	$id,
			'path'	=>	$this->config->item('ckeditor_basepath'),

			//Optionnal values
			'config' => array(
								'language' => 'id',
								'height' => 450,
								'magicline_color' => '#F3007D',
								'toolbar' 	=> 	array(
													array('Maximize', 'ShowBlocks','-','Source', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'Preview', 'Print', '-', 'Templates'),
													array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat','-','Link','Unlink', 'Anchor'),
													array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
													array('Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
													array( 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ),
													array('Styles', 'Format', 'Font', 'FontSize','TextColor', 'BGColor'),
													array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt')
											)
						)
			);

		return $data;
	}

	function editorNormal($id='contenx')
	{
		$this->load->helper('ckeditor');
 
		//Ckeditor's configuration
		$data[$id] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	$id,
			'path'	=>	$this->config->item('ckeditor_basepath'),
 
			//Optionnal values set toolbar
			'config' => array(
							'language' => 'id',
							'height' => 300,
							'magicline_color' => '#F3007D',
							'toolbar' 	=> 	array(
												array('Maximize', 'ShowBlocks','-','Source', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'Preview', 'Print', '-', 'Templates'),
												
												array('Bold','Italic','-','NumberedList','BulletedList','-','Link','Unlink'),
												array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'),
												array('Format', 'Font', 'FontSize','TextColor','BGColor'),
												array('Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
												array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt')
											)
						)
		);

		return $data;
	}

	function editorMini($id='contenx')
	{
		$this->load->helper('ckeditor');
 
		//Ckeditor's configuration
		$data[$id] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	$id,
			'path'	=>	$this->config->item('ckeditor_basepath'),
 
			//Optionnal values set toolbar
			'config' => array(
							'language' => 'id',
							'height' => 200,
							'magicline_color' => '#F3007D',
							'toolbar' 	=> 	array(
												array('Maximize', 'ShowBlocks','-','Source','-','Bold','Italic','-','NumberedList','BulletedList','-','Link','Unlink','-', 'Scayt'),
												array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
												array('Format', 'Font', 'FontSize' ,'TextColor','BGColor')
											)
						)
		);

		return $data;
	}

	function editorMinimal($id='contenx')
	{
		$this->load->helper('ckeditor');
 
		//Ckeditor's configuration
		$data[$id] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	$id,
			'path'	=>	$this->config->item('ckeditor_basepath'),
 
			//Optionnal values set toolbar
			'config' => array(
							'language' => 'id',
							'height' => 200,
							'magicline_color' => '#F3007D',
							'toolbar' 	=> 	array(
												array('ShowBlocks','-','Source','-','Bold','Italic'),
												array('Format', 'Font', 'FontSize' ,'TextColor','BGColor')
											)
						)
		);

		return $data;
	}

}

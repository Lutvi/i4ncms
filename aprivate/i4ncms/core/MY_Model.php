<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

class MY_Model extends CI_Model {

	/* SYS */
	var $tbl_admin = 'tbl_sys_admin_cms';
	var $tbl_ref_user = 'tbl_sys_referrer_user';
	var $tbl_log_robot = 'tbl_sys_log_robot';
	var $tbl_log = 'tbl_sys_err_log_users';
	var $tbl_web_config = 'tbl_sys_web_config';
	var $tbl_bck = 'tbl_sys_backup';
	var $tbl_sesi = 'tbl_sys_ci_sessions';
	var $tbl_tema = 'tbl_cms_tema';
	var $tbl_kategori = 'tbl_cms_kategori';
	var $tbl_tag = 'tbl_cms_relasi_tag';
	/* End SYS */

	/* ADD-ONS */
	var $tbl_provinsi = 'tbl_excms_addon_provinsi';
	var $tbl_kota = 'tbl_excms_addon_kab_kota';
	/* End ADD-ONS */

	/* CMS */
	var $tbl_banner_header = 'tbl_cms_banner_atas';
	var $tbl_banner_iklan = 'tbl_cms_banner_iklan';
	var $tbl_widget = 'tbl_cms_widget';
	var $tbl_module = 'tbl_cms_module';
	var $tbl_menu = 'tbl_cms_menu';
	var $tbl_halaman = 'tbl_cms_halaman';
	var $tbl_album = 'tbl_cms_album_cover';
	var $tbl_gallery = 'tbl_cms_album_gallery';
	var $tbl_video = 'tbl_cms_video';
	var $tbl_playlist = 'tbl_cms_video_playlist';
	var $tbl_blog = 'tbl_cms_posting';
	/* End CMS */

	/* ORDERAN */
	var $tbl_order = 'tbl_ecom_orderan';
	var $tbl_diskon = 'tbl_ecom_diskon_transaksi';
	var $tbl_order_sukses = 'tbl_ecom_orderan_sukses';
	var $tbl_order_batal = 'tbl_ecom_orderan_batal';
	var $tbl_pembeli = 'tbl_ecom_pembeli';
	var $tbl_alamat_kirim = 'tbl_ecom_alamat_kirim';
	var $tbl_metode_bayar = 'tbl_ecom_metode_bayar';
	var $tbl_kurir = 'tbl_ecom_kurir_pengiriman';
	/* End ORDERAN */

	/* PRODUK */
	var $tbl_produk = 'tbl_ecom_produk';
	var $tbl_produk_lama = 'tbl_ecom_produk_lama';
	var $tbl_produk_anak = 'tbl_ecom_produk_anak';
	var $tbl_img_produk = 'tbl_ecom_img_produk';
	var $tbl_img_produk_lama = 'tbl_ecom_img_produk_lama';
	/* End PRODUK */

	function __construct()
	{
		parent::__construct();
		#$CI =& get_instance(); 
		$this->load->library('encrypt');

		//load default db settings
		//$this->db = $this->load->database('default', TRUE);
		//$this->db->cache_off();
	}

/*VISITOR*/
    function jmlVisitor()
    {
		$this->db->select_sum('hits');
		$ref = $this->db->get($this->tbl_ref_user);
		$jref = $ref->row();

		$this->db->select_sum('hits');
		$hal = $this->db->get($this->tbl_halaman);
		$jhal= $ref->row();

		$ttl = ($jref->hits+$jhal->hits);
		
		return $ttl;
    }

    function upRattingKonten($tipe='',$id='',$nilai=0)
    {
		// tentukan nama tabel
		if( $tipe == 'album' )
		$tbl = $this->tbl_album;
		if( $tipe == 'video' )
		$tbl = $this->tbl_video;
		if( $tipe == 'blog' )
		$tbl = $this->tbl_blog;
		if( $tipe == 'produk' )
		$tbl = $this->tbl_produk;

		$id = bs_kode($id,true);

		$this->db->select('ratting,votes');
		if( $tipe == 'produk' )
		$query = $this->db->get_where($tbl, array('id_prod' => (int)$id), 1);
		else
		$query = $this->db->get_where($tbl, array('id' => (int)$id), 1);
        if ($query->num_rows() == 1)
        {
			$row = $query->row();
			$vote = $row->votes;
			$rating = round($row->ratting,3);

			$n1 = $vote+$nilai;
			if($vote <= 3)
			$rate = ($rating+$nilai*$vote+1)/($vote+1);
			elseif($vote <= 20)
			$rate = ($rating*$vote+$nilai)/($vote+1);
			else{
			$rate = ($rating*$vote+1)+($nilai);
			$rate = $rate/($vote+1);
			}

			if ($rate > 5) $rate = 5.00;
			if ($rate < 0.99) $rate = 1.00;

			$data = array( 'ratting' => round($rate, 2), 'votes' => $vote+1);
			if( $tipe == 'produk' )
			$this->db->update($tbl, $data, array('id_prod' => (int)$id));
			else
			$this->db->update($tbl, $data, array('id' => (int)$id));
			
			$arr = array('nilai' => round($rate, 1, PHP_ROUND_HALF_UP), 'jvote' => $vote+1);
			return json_encode($arr);
        }
    }
/* GLOBAL Model */


/* Home Konten */
	function cariHomeKonten($count=FALSE, $tipe='', $limit=100, $offset=0)
	{
		if( $tipe === 'produk')
		{
			if (current_lang(false) === 'id')
			{
				$this->db->select('id_prod,parent_id_prod,nama_prod,anak_untuk_jenis,nama_jenis_anak,kode_prod,id_prod_img,tgl_update,promo,kode_warna,harga_prod,harga_spesial,diskon,stok,deskripsi,ratting,votes');
			}
			else {
				$this->db->select('id_prod,parent_id_prod,nama_prod_en,anak_untuk_jenis,nama_jenis_anak_en,kode_prod,id_prod_img,tgl_update,promo,kode_warna,harga_prod,harga_spesial,diskon,stok,deskripsi_en,ratting,votes');
			}

			$sql = "status = 'on' ";
			if (config_item('cek_stok_front') === TRUE)
			$sql .= "AND stok > 0 ";
			$this->db->where($sql);

			$this->db->order_by('promo', 'asc');
			#$this->db->order_by('tgl_update', 'desc');
			$hasil = $this->db->get($this->tbl_produk, $limit, $offset);
		}
		if( $tipe === 'blog')
		{
			$this->db->where('status', 'on');
			if (current_lang(false) == 'id')
			{
				$this->db->select('id,embed_id_album,embed_id_video,gambar_cover,judul_id,isi_id,tgl_terbit,tgl_revisi,hits,headline,status,ratting,votes');
			}
			else {
				$this->db->select('id,embed_id_album,embed_id_video,gambar_cover,judul_en,isi_en,tgl_terbit,tgl_revisi,hits,headline,status,ratting,votes');
			}

			$this->db->order_by('headline', 'asc');
			$this->db->order_by('tgl_revisi', 'desc');
			$hasil = $this->db->get($this->tbl_blog, $limit, $offset);
		}
		if( $tipe === 'album')
		{
			$this->db->where('status', 'on');
			if (current_lang(false) === 'id')
			{
				$this->db->select('id,tgl_update,nama_id,ket_id,gambar_cover,posisi,utama,status,hits,ratting,votes');
			}
			else {
				$this->db->select('id,tgl_update,nama_en,ket_en,gambar_cover,posisi,utama,status,hits,ratting,votes');
			}

			$this->db->order_by('utama', 'asc');
			$this->db->order_by('tgl_update', 'desc');
			$hasil = $this->db->get($this->tbl_album, $limit, $offset);
		}
		if( $tipe === 'video')
		{
			$this->db->where('status', 'on');
			if (current_lang(false) === 'id')
			{
				$this->db->select('id,tgl_update,nama_id,ket_id,src_video,gambar_video,posisi,utama,status,hits,ratting,votes');
			}
			else {
				$this->db->select('id,tgl_update,nama_en,ket_en,src_video,gambar_video,posisi,utama,status,hits,ratting,votes');
			}

			$this->db->order_by('utama', 'asc');
			$this->db->order_by('tgl_update', 'desc');
			$hasil = $this->db->get($this->tbl_video, $limit, $offset);
		}

		//return $this->db->last_query();
		if($count)
		return $hasil->num_rows() > 100 ? "100+" : $hasil->num_rows();
		else
		return $hasil->result();
	}
/* === End Home Konten */


/* Cari Konten */
	function cariKonten($tipe='',$keyword='', $limit=0, $offset=0)
	{
		$keyword = $this->db->escape_like_str($keyword);
		$keyword = explode(" ", $keyword);
		$hasil = array();

		foreach( $keyword as $kata)
		{
			if( $tipe === 'produk')
			{
				$this->db->where('status', 'on');
				if (current_lang(false) === 'id')
				{
					$this->db->select('id_prod,parent_id_prod,nama_prod,anak_untuk_jenis,nama_jenis_anak,kode_prod,id_prod_img,tgl_update,promo,kode_warna,harga_prod,harga_spesial,diskon,stok,deskripsi,ratting,votes');
					$this->db->like('nama_prod', $kata);
					$this->db->or_like('nama_jenis_anak', $kata);
				}
				else {
					$this->db->select('id_prod,parent_id_prod,nama_prod_en,anak_untuk_jenis,nama_jenis_anak_en,kode_prod,id_prod_img,tgl_update,promo,kode_warna,harga_prod,harga_spesial,diskon,stok,deskripsi_en,ratting,votes');
					$this->db->like('nama_prod_en', $kata);
					$this->db->or_like('nama_jenis_anak_en', $kata);
				}

				$this->db->or_like('kategori_id', $kata);
				$this->db->order_by('promo', 'asc');
				$kumpul = $this->db->get($this->tbl_produk, $limit, $offset);

				$hasil = array_merge($hasil, $kumpul->result());
			}
			if( $tipe === 'blog')
			{
				$this->db->where('status', 'on');
				if (current_lang(false) == 'id')
				{
					$this->db->select('id,embed_id_album,embed_id_video,gambar_cover,judul_id,isi_id,tgl_terbit,tgl_revisi,hits,headline,status,ratting,votes');
					$this->db->like('judul_id', $kata);
					$this->db->or_like('isi_id', $kata);
				}
				else {
					$this->db->select('id,embed_id_album,embed_id_video,gambar_cover,judul_en,isi_en,tgl_terbit,tgl_revisi,hits,headline,status,ratting,votes');
					$this->db->like('judul_en', $kata);
					$this->db->or_like('isi_en', $kata);
				}

				$this->db->or_like('kategori_id', $kata);
				$this->db->order_by('headline', 'asc');
				$kumpul = $this->db->get($this->tbl_blog, $limit, $offset);

				$hasil = array_merge($hasil, $kumpul->result());
			}
			if( $tipe === 'album')
			{
				$this->db->where('status', 'on');
				if (current_lang(false) === 'id')
				{
					$this->db->select('id,tgl_update,nama_id,ket_id,gambar_cover,posisi,utama,status,hits,ratting,votes');
					$this->db->like('nama_id', $kata);
					$this->db->or_like('ket_id', $kata);
				}
				else {
					$this->db->select('id,tgl_update,nama_en,ket_en,gambar_cover,posisi,utama,status,hits,ratting,votes');
					$this->db->like('nama_en', $kata);
					$this->db->or_like('ket_en', $kata);
				}

				$this->db->or_like('kategori_id', $kata);
				$this->db->order_by('posisi', 'asc');
				$kumpul = $this->db->get($this->tbl_album, $limit, $offset);

				$hasil = array_merge($hasil, $kumpul->result());
			}
			if( $tipe === 'video')
			{
				$this->db->where('status', 'on');
				if (current_lang(false) === 'id')
				{
					$this->db->select('id,tgl_update,nama_id,ket_id,src_video,gambar_video,posisi,utama,status,hits,ratting,votes');
					$this->db->like('nama_id', $kata);
					$this->db->or_like('ket_id', $kata);
				}
				else {
					$this->db->select('id,tgl_update,nama_en,ket_en,src_video,gambar_video,posisi,utama,status,hits,ratting,votes');
					$this->db->like('nama_en', $kata);
					$this->db->or_like('ket_en', $kata);
				}

				$this->db->or_like('kategori_id', $kata);
				$this->db->order_by('posisi', 'asc');
				$kumpul = $this->db->get($this->tbl_video, $limit, $offset);

				$hasil = array_merge($hasil, $kumpul->result());
			}
		}

		//return $this->db->last_query();
		return array_unique($hasil,SORT_REGULAR);
	}
/* === End Cari Konten */

/* Kategori */
	function getNamaKategoriById($id=0,$bhs='id')
	{
		$this->db->select('label_id,label_en,tipe_kategori');
		$this->db->where('id_kategori',(int)$id);
		$query = $this->db->get($this->tbl_kategori,1);
		if ($query->num_rows > 0)
		{
			$row = $query->row();
			if($bhs === 'id')
			return $row->label_id;
			else
			return $row->label_en;
		}
		else {
			return NULL;
		}
	}

    function getKategori($tipe='')
	{
		$this->db->select('id_kategori,label_id,label_en,tipe_kategori');
		if( ! empty($tipe))
		$this->db->where('tipe_kategori',$tipe);
		$this->db->where('status','on');
		$this->db->order_by("label_id", "asc");
		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	function getKategoriKonten($tipe='')
	{
		$this->db->select('id_kategori,label_id,label_en,tipe_kategori');
		if( ! empty($tipe))
		$this->db->where('tipe_kategori',$tipe);
		$this->db->where('status','on');
		$this->db->order_by("tipe_kategori", "asc");
		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			if($tipe == 'produk' && ! $this->config->item('toko_aktif'))
			return NULL;
			else
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	function cekKategoriKonten($tipe='',$nama_id='')
	{
		$cek = FALSE;
		$nama_id = format_txtkategori($nama_id);
		if($tipe === 'produk')
		{
			$this->db->where('kategori_id',$nama_id);
			$query = $this->db->get($this->tbl_produk);
			if ($query->num_rows > 0 && $this->config->item('toko_aktif'))
			$cek = TRUE;
			else
			return FALSE;
		}
		elseif($tipe === 'blog')
		{
			$this->db->where('kategori_id',$nama_id);
			$query = $this->db->get($this->tbl_blog);
			if ($query->num_rows > 0)
			$cek = TRUE;
		}
		elseif($tipe === 'album')
		{
			$this->db->where('kategori_id',$nama_id);
			$query = $this->db->get($this->tbl_album);
			if ($query->num_rows > 0)
			$cek = TRUE;
		}
		else {
			$this->db->where('kategori_id',$nama_id);
			$query = $this->db->get($this->tbl_video);
			if ($query->num_rows > 0)
			$cek = TRUE;
		}

		return $cek;
	}

	function getkatIdByNamaKat($label='',$tipe='')
	{
		$label = trim_judul(humanize($label));

		$this->db->select('id_kategori,label_id,label_en');
		if(!empty($tipe))
		$this->db->where('tipe_kategori',$tipe);
		$this->db->where('label_id',$label);
		$this->db->or_where('label_en',$label);
		$this->db->limit(1);
		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			return $query->row();
		}
		else {
			return NULL;
		}
	}

	function cekKategori($str='',$bhs='',$tipe='')
	{
		$this->db->select('label_id');

		if($bhs==='id')
		$this->db->where('label_id',trim($str));
		else
		$this->db->where('label_en',trim($str));
		if( ! empty($tipe))
		$this->db->where('tipe_kategori', $tipe);

		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function addKategori($tipe='')
	{
		$kat_id = ucwords(strtolower($this->input->post('nama_kat_id')));
		$kat_en = ucwords(strtolower($this->input->post('nama_kat_en')));
		
		$data = array(
				'label_id' => trim($kat_id),
				'label_en' => trim($kat_en),
				'tipe_kategori' => $tipe
				);

		if ( ! $this->db->insert($this->tbl_kategori,$data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
/* End Kategori */


/* Keyword */
	function getKeyword($bhs='id')
	{
		if($bhs==='id')
		$sql = "SELECT DISTINCT label_id FROM " . $this->tbl_kategori ." WHERE status ='on'";
		else
		$sql = "SELECT DISTINCT label_en FROM " . $this->tbl_kategori ." WHERE status ='on'";

		$query = $this->db->query($sql);
		if ($query->num_rows > 0)
		{
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	function cekKeyword($str='',$bhs='')
	{
		$this->db->select('label_id');
		if($bhs==='id')
		$this->db->where('label_id',trim($str));
		else
		$this->db->where('label_en',trim($str));

		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function addKeyword()
	{
		$kat_id = ucwords($this->input->post('nama_kat_id'));
		$kat_en = ucwords($this->input->post('nama_kat_en'));

		$data = array(
				'label_id' => trim($kat_id),
				'label_en' => trim($kat_en),
				'status' => 'on'
				);
		
		if ( ! $this->db->insert($this->tbl_kategori,$data))
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
/* End Keyword */


/* Tag Konten Front */
	function getFrontObjIdByIdKat($tipe='',$id_kat='')
	{
		if($this->config->item('cache_front'))
		{
			$this->db->cache_on();
			$this->db->select('id_objek');
			if( ! empty($tipe))
			$this->db->where('tipe_objek',$tipe);
			$this->db->where('id_kategori',$id_kat);

			$this->db->group_by("id_objek", "asc");
			//$this->db->distinct();
			$query = $this->db->get($this->tbl_tag);
			$this->db->cache_off();
		}
		else {
			$this->db->select('id_objek');
			if( ! empty($tipe))
			$this->db->where('tipe_objek',$tipe);
			$this->db->where('id_kategori',$id_kat);

			$this->db->group_by("id_objek", "asc");
			//$this->db->distinct();
			$query = $this->db->get($this->tbl_tag);
		}

		if ($query->num_rows > 0)
		{
			foreach($query->result_array() as $row)
			{
				$id_obj[] = $row['id_objek'];
			}
			return $id_obj;
		}
		else {
			return NULL;
		}
	}

	function getFrontAllTagObjId()
	{
		if($this->config->item('cache_front'))
		{
			$this->db->cache_on();
			$this->db->select('id_objek,tipe_objek');
			$this->db->group_by("id_kategori");
			$this->db->order_by("tipe_objek", "desc");
			$query = $this->db->get($this->tbl_tag);
			$this->db->cache_off();
		}
		else {
			$this->db->select('id_objek,tipe_objek');
			$this->db->group_by("id_kategori");
			$this->db->order_by("tipe_objek", "desc");
			$query = $this->db->get($this->tbl_tag);
		}

		if ($query->num_rows > 0)
		{
			return $query->result();
		}
		else {
			return NULL;
		}
	}
/* End Tag Konten Front */

/* Tag Konten */
	function addTagKonten($id_obj='',$tag=array(),$tipe='',$update=FALSE)
	{
		// clean update relasinya
		if($update === TRUE){
		$this->db->delete($this->tbl_tag, array('id_objek' => $id_obj,'tipe_objek' => $tipe));
		//Hapus idbjek = 0 jika ada.
		$this->db->delete($this->tbl_tag, array('id_objek' => 0,'tipe_objek' => $tipe));
		}

		if ( count($tag) > 0 )
		{
			for($i=0;$i < count($tag);$i++)
			{
				$query = $this->db->get_where($this->tbl_kategori,array('id_kategori' => $tag[$i],'tipe_kategori' => $tipe),1);
				if( $query->num_rows() > 0)
				{
					$kat = $query->row();
					$data = array(
							'id_objek' => $id_obj,
							'tipe_objek' => $tipe,
							'id_kategori' => $kat->id_kategori
					);
					$this->db->insert($this->tbl_tag,$data);
				}
			}
		}
	}
	
	function lastRowId($tbl='',$idrow='')
	{
		$this->db->select_max($idrow);
		$query = $this->db->get($tbl);
		if( $query->num_rows() > 0){
		$row = $query->row_array();
		return $row[idrow]+1;
		}
		else
		return 1;
	}

	function getTags($tipe='')
	{
		$this->db->select('id_kategori,label_id');
		if( ! empty($tipe))
		{
			$this->db->where('tipe_kategori',$tipe);
		}
		else {
			return NULL;
		}

		$this->db->order_by("label_id", "asc"); 
		$query = $this->db->get($this->tbl_kategori);
		if ($query->num_rows > 0)
		{
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	function getTagsIdJsonByObjekId($id_obj='',$tipe='')
	{
		$this->db->select('id_kategori');
		$this->db->where('tipe_objek',$tipe);
		$this->db->where('id_objek',$id_obj);
		$query = $this->db->get($this->tbl_tag);
		if ($query->num_rows > 0)
		{
			foreach($query->result() as $row)
			{
				$in_tags[] = (int)$row->id_kategori;
			}

			return json_encode($in_tags);
		}
		else {
			return '[]';
		}
	}

	function getTagsLabelByObjekId($id_obj='',$tipe='')
	{
		$this->db->select('id_kategori');

		$this->db->where('tipe_objek',$tipe);
		$this->db->where('id_objek',$id_obj);
		$query = $this->db->get($this->tbl_tag);
		if ($query->num_rows > 0)
		{
			foreach($query->result() as $row)
			{
				$in_tags[] = $row->id_kategori;
			}

			$this->db->select('id_kategori,label_id,label_en');
			$this->db->where_in('id_kategori', $in_tags);
			$kategori = $this->db->get($this->tbl_kategori);

			$data = array('rs_kat' => $kategori->result(), 'id_konten' => $id_obj, 'tipe' => $tipe);

			return $data;
		}
		else {
			return NULL;
		}
	}
/* End Tag Konten */


/* Hits Halaman */
	function upHalamanHits($hid='')
    {
		$this->db->select('hits');
		$query = $this->db->get_where($this->tbl_halaman,array('id_halaman' => $hid));
        if ($query->num_rows() > 0)
        {
			$row = $query->row();
			$hits = $row->hits+1;

			$data = array(
				   'hits' => $hits
				);
			if ( ! $this->db->update($this->tbl_halaman, $data, array('id_halaman' => $hid)) )
			{
				return FALSE;
			}
			else {
				return TRUE;
			}
        }
        else {
           return FALSE;
        }
    }
/* End Hits Halaman */


/* Hits Konten
 * var
 * @ $obj_id = (id_objek_konten)
 * @ $tipe = ('blog','album','gallery','video','playlist')
 * return nothing
 * */
	function upHitsKonten($obj_id='',$tipe='')
    {
		// tentukan nama tabel
		if( $tipe === 'album' )
		$tbl = $this->tbl_album;
		elseif( $tipe === 'gallery' )
		$tbl = $this->tbl_gallery;
		elseif( $tipe === 'video' )
		$tbl = $this->tbl_video;
		elseif( $tipe === 'playlist' )
		$tbl = $this->tbl_playlist;
		else
		$tbl = $this->tbl_blog;

		$this->db->select('hits');
		$query = $this->db->get_where($tbl, array('id' => $obj_id), 1);
        if ($query->num_rows() > 0)
        {
			$row = $query->row();
			$hits = $row->hits+1;

			$data = array( 'hits' => $hits );
			$this->db->update($tbl, $data, array('id' => $obj_id));
        }
    }
/* End Hits Konten */


/* Profil Penulis */
	function getProfilPenulisById($id='')
	{
		$profil = array();
		$id = bs_kode($id, true);
		$tbl = 'tbl_sys_admin_cms';
		$this->db->select('nama_lengkap,adm_email,foto,biografi');
		$pen = $this->db->get($tbl, array('id' => (int)$id), 1);
		$sup = $this->db->get($tbl, array('id' => 1), 1);
		if ($pen->num_rows() > 0)
			$row = $pen->row();
		else
			$row = $sup->row();

		$profil['nama'] = dekodeString($row->nama_lengkap);
		$profil['email'] = dekodeString($row->adm_email);
		$profil['foto'] = $row->foto;
		$profil['bio'] = bs_kode($row->biografi, true);
		return $profil;
	}
/* End Profil Penulis */

/* End GLOBAL Model */

}

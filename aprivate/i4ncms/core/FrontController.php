<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontController extends MY_Controller {

	// ui-style default: reddanio | blitzer | hot-sneaks | humanity | overcast | pepper-grinder | ui-lightness | ymetro2
	public $jq_style = 'reddanio';
	
	// set paging konten halaman frontpage
	public $per_halaman = 10; //16

	//jml home
	public $jknt_home = 8;

	public function __construct()
	{
		parent::__construct();
		//$CI =& get_instance();

		/* Debuging Mode */
		if ( ENVIRONMENT == 'development' )
		{
			//$this->config->set_item('compress_output', FALSE);
			//$this->output->enable_profiler(TRUE);
		}

		//model front
		$this->load->model('front/fcmsmodel','cms');
		$this->load->model('front/fprodukmodel','mproduk');
		$this->load->model('front/fwebmodel','web');

		$this->load->model('front/fordermodel','orderan');
		$this->load->model('front/usermodel','user');

		//cek konfigurasi https
		$this->_jalankanHTTPS(TRUE);
		//bahasa disesuaikan
		$this->_jalankanBahasanya();
		//load lib
		$this->load->library('cart');
		$this->load->spark('friendly-template/1.1.2');
		$this->load->spark('mobiledetection/1.0.1');
		$this->load->spark('disqus-spark/0.0.7');
		$this->load->spark('ci-simplepie/1.0.1/');

		$this->config->set_item('global_xss_filtering', TRUE); // just make sure xss is running
		$this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		$this->input->get(NULL, TRUE); // returns all GET items with XSS filter

		//clean session
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			if(! $this->config->item('toko_aktif'))
			$this->cart->destroy();

			$this->keluar();
		}
	}

	// hapus session user
	public function keluar()
	{
		$user = array('kunci_id'=>'','kunci'=>'','email_beli'=>'','usrid'=>'','nmusr'=>'','alamat_id'=>'',
					  'trx_order'=>'','total_transaksi'=>'','diskon_transaksi'=>'');
		$this->session->unset_userdata($user);
	}

	// cek login front user
	function _cekLogin()
	{
		//$dt_user = 
		$id = $this->session->userdata('kunci_id');
		$emusr = $this->session->userdata('kunci');
		$demusr = $this->session->userdata('kuncix');
		$nm = $this->session->userdata('nmusr');
		if(empty($id) || empty($emusr) || empty($demusr) || empty($nm))
		{
			return NULL;
		}
		else {
			if(bs_kode($emusr, TRUE) == dekodeString($demusr) )
			return 1;
			else
			return NULL;
		}
	}

	function _clean_sesi($tipe='')
	{
		if($tipe === 'produk')
		$ses_konten = array('kblogbasis'=>'','kvidbasis'=>'','kalbbasis'=>'','blogbasis'=>'');
		if($tipe === 'blog')
		$ses_konten = array('kpbasis'=>'','kvidbasis'=>'','kalbbasis'=>'');
		if($tipe === 'album')
		$ses_konten = array('kpbasis'=>'','kblogbasis'=>'','kvidbasis'=>'','blogbasis'=>'');
		if($tipe === 'video')
		$ses_konten = array('kpbasis'=>'','kblogbasis'=>'','kalbbasis'=>'','blogbasis'=>'');

		$this->session->unset_userdata($ses_konten);
	}

	// assets front
	function _getFrontAssets()
	{
		$data['seo_default'] = array(
			array('name' => 'keywords', 'content' => format_keyword(entities_to_ascii($this->config->item('site_keyword_'.current_lang(false))))),
			array('name' => 'description', 'content' => rip_tags(entities_to_ascii($this->config->item('site_description_'.current_lang(false)))))
		);
		$data['mikro_dt_desc'] = rip_tags(entities_to_ascii($this->config->item('site_description_'.current_lang(false))));

		$data['css_tema'] = $this->web->getCssTema();
		$data['js_tema'] = $this->web->getJsTema();
		if($this->config->item('theme_id') == 'theme01')
		{
			$data['js'] = array('plugins/modernizr.custom.normal.js','jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js');
			$data['js_ie'] = array('PIE-1.0.0/PIE.js');
			$data['css'] = array('elem/reset-min.css','themes/'.$this->jq_style.'/jquery-ui.css');
			$data['css_ie'] = array('elem/IEmain-min.css');
			$data['js_ie'] = array('PIE-1.0.0/PIE.js');
		}
		else {
			$data['js'] = array('plugins/modernizr.custom.normal.js','jqui/jquery.min.js');
			$data['css'] = array();
			$data['js_ie'] = array();
			$data['css_ie'] = array();
		}

		//assets widget
		$data['assets_widget'] =  $this->cms->getWidget();
		//banner iklan default
		$data['banner'] = array('bannerfans_3328813.jpg','bannerfans.jpg');

		// status login
		$data['login'] = $this->_cekLogin();
		$data['hbanner'] = $this->web->getFrontAllHbanner();
		// Menu
		$data['lang'] = $this->bahasa;
		$data['menu'] = $this->cms->getParentMenu(0);
		// path untuk link menu
		$data['path_halaman'] = $this->lang->line('path_halaman');
		//sidebar
		$data['kat_konten'] = $this->cms->getKategoriKonten();
		$data['iklan_sidebar'] = $this->web->getFrontAllIklan('sidebar');
		$data['iklan_atas'] = $this->web->getFrontAllIklan('atas');
		$data['iklan_katas'] = $this->web->getFrontAllIklan('konten_atas');
		$data['iklan_kbawah'] = $this->web->getFrontAllIklan('konten_bawah');
		// widgets
		$widgets = $this->cms->getWidgetName();
		if( is_array($this->_runWidgetnya($widgets)))
		$data = array_merge($data, $this->_runWidgetnya($widgets));

		return $data;
	}

	// run widget
	function _runWidgetnya($widgets=array())
	{
		if( ! empty($widgets))
		{
			foreach ($widgets as $row)
			{
				$wn = $row['nama_widget'];
				if (file_exists(APPPATH.'widgets/'.$wn.EXT))
				{
					if (current_lang(FALSE) === 'id')
					$jdl_widget = $row['judul_widget'];
					else
					$jdl_widget = $row['judul_widget_en'];

					$data[$wn] = $this->template->widget($wn, array("wid_title"=> $jdl_widget));
				}
				else {
					$data[$wn] = NULL;
				}
			}

			return $data;
		}
	}

// konten kategori
	function _getKontenKategori($tipe='')
	{
		$this->load->library('pagination');
		$extra_seo = '';
		$data['tipe'] = $tipe;
		$data['kat_konten'] = $this->cms->getKategoriKonten();

		//cari idkategori by nama
		$label = $this->uri->segment(4,NULL);
		$kat = $this->cms->getkatIdByNamaKat($label,$tipe);

		$judul_id = underscore($kat->label_id);
		$judul_en = underscore($kat->label_en);

		$pgdata = array(
                   'tipepg'  	=> (current_lang(false) === 'id' && $tipe == 'produk')?'product':$tipe,
                   'nmpg'		=> (current_lang(false) === 'id')?$judul_en:$judul_id
               );
        $this->session->set_userdata($pgdata);

		if(!empty($kat) && $tipe === 'blog')
		{
			$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			// by blog
			if (current_lang(false) === 'id')
			$this->session->set_userdata('kblogbasis',current_lang().'kategori/blog/'.$judul_id);
			else
			$this->session->set_userdata('kblogbasis',current_lang().'category/blog/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('kblogbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->cms->getCountAllBlogFrontByKategori($kat->id_kategori);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['blog_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];

			if ($this->cms->getCountAllBlogFrontByKategori($kat->id_kategori) > 0)
			$data['konten_blog'] = $this->cms->getAllBlogFrontByKategori($kat->id_kategori,$config['per_page'],$offset);
			else
			$data['konten_blog'] = array();

			$data['content'] = '';

			if(current_lang(false) === 'id')
			{
				
				if( ! empty($data['konten_blog']))
				{
					$data['title'] = 'Kategori Blog - '.$kat->label_id;
					$extra_seo = $data['title'].' '.$data['konten_blog'][0]->isi_id;
				} else {
					$data['title'] = 'Kategori Blog Kosong';
					$data['content'] = 'Kategori blog <u>'.$kat->label_id.'</u> masih kosong.';
				}
			} else {
				if( ! empty($data['konten_blog']))
				{
					$data['title'] = 'Blog Categories - '.$kat->label_en;
					$extra_seo = $data['title'].' '.$data['konten_blog'][0]->isi_en;
				} else {
					$data['title'] = 'Blog Categories not available';
					$data['content'] = 'Blog Categories for <u>'.$kat->label_en.'</u> not available at this moment.';
				}
			}
		}
		elseif(!empty($kat) && $tipe === 'album')
		{
			$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			if (current_lang(false) === 'id')
			$this->session->set_userdata('kalbbasis',current_lang().'kategori/album/'.$judul_id);
			else
			$this->session->set_userdata('kalbbasis',current_lang().'category/album/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('kalbbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->cms->getCountAllAlbumFrontByKategori($kat->id_kategori);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['album_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];
			if($this->cms->getCountAllAlbumFrontByKategori($kat->id_kategori) > 0)
			$data['gambar_album'] = $this->cms->getAllAlbumFrontByKategori($kat->id_kategori,$config['per_page'],$offset);
			else
			$data['gambar_album'] =  array();

			if (current_lang(false) === 'id')
			{
				$data['title'] = 'Kategori Album - '.$kat->label_id;
				$data['content'] = '';
				if( ! empty($data['gambar_album']))
				$extra_seo = $data['title'].' '.$data['gambar_album'][0]->ket_id;
			}
			else {
				$data['title'] = 'Album Categories - '.$kat->label_en;
				$data['content'] = '';
				if( ! empty($data['gambar_album']))
				$extra_seo = $data['title'].' '.$data['gambar_album'][0]->ket_en;
			}
		}
		elseif(!empty($kat) && $tipe === 'video')
		{

			if (current_lang(false) === 'id')
			$this->session->set_userdata('kvidbasis',current_lang().'kategori/video/'.$judul_id);
			else
			$this->session->set_userdata('kvidbasis',current_lang().'category/video/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('kvidbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->cms->getCountAllVideoFrontByKategori($kat->id_kategori);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['video_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];

			if($this->cms->getCountAllVideoFrontByKategori($kat->id_kategori) > 0)
			$data['video'] = $this->cms->getAllVideoFrontByKategori($kat->id_kategori,$config['per_page'],$offset);
			else
			$data['video'] =  array();

			if (current_lang(false) === 'id')
			{
				$data['title'] = 'Kategori Video - '.$kat->label_id;
				$data['content'] = '';
				if( ! empty($data['video']))
				$extra_seo = $data['title'].' '.$data['video'][0]->ket_id;
				else
				$data['content'] = 'Kategori video <u>'.$kat->label_id.'</u> masih kosong.';
			}
			else {
				$data['title'] = 'Video Categories - '.$kat->label_en;
				$data['content'] = '';
				if( ! empty($data['video']))
				$extra_seo = $data['title'].' '.$data['video'][0]->ket_en;
				else
				$data['content'] = 'Category videos for <u>'.$kat->label_en.'</u> not available at this moment.';
			}
		}
		elseif(!empty($kat) && $tipe === 'produk')
		{

			$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');
			$data['tipe_produk'] = 'kategori';

			if (current_lang(false) === 'id')
			$this->session->set_userdata('kpbasis',current_lang().'kategori/produk/'.$judul_id);
			else
			$this->session->set_userdata('kpbasis',current_lang().'category/product/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('kpbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->mproduk->getCountFrontProdukByKategori($kat->id_kategori);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['produk_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];

			if ($this->mproduk->getCountFrontProdukByKategori($kat->id_kategori) > 0)
			$data['content'] = $this->mproduk->getFrontProdukByKategori($kat->id_kategori,$config['per_page'],$offset);
			else
			$data['content'] = array();

			if (current_lang(false) === 'id')
			{
				$data['title'] = 'Kategori Produk - '.$kat->label_id;
				if( ! empty($data['content']))
				$extra_seo = $data['title'].' '.$data['content'][0]['deskripsi'];
			}
			else{
				$data['title'] = 'Product Categories - '.$kat->label_en;
				if( ! empty($data['content']))
				$extra_seo = $data['title'].' '.$data['content'][0]['deskripsi_en'];
			}
		}
		else {
			$data['content'] = '';
			$data['title'] = 'Kategori Kosong';
		}

		//QrCode
		if(! isset($data['qrcode']) && ! empty($data['konten_blog']) || ! empty($data['gambar_album']) || ! empty($data['video']))
		$data['qrcode'] = $this->_buatImgQrCode(current_url());

		$data = array_merge($data, $this->_getSEO('','',$extra_seo));

		return $data;
	}

// konten Tag
	function _getKontenTag($tipe='',$tag='')
	{
		$extra_seo = '';
		$data['tipe'] = $tipe;
		$data['kat_konten'] = $this->cms->getKategoriKonten();

		if(!empty($tag))
		{
			$kat = $this->cms->getkatIdByNamaKat($tag);
			$id_obj = $this->cms->getFrontObjIdByIdKat($tipe,$kat->id_kategori);
	
			$judul_id = underscore($kat->label_id);
			$judul_en = underscore($kat->label_en);
	
			$pgdata = array(
	                   'tipepg'  	=> (current_lang(false) === 'id' && $tipe == 'produk')?'product':$tipe,
	                   'nmpg'		=> (current_lang(false) === 'id')?$judul_en:$judul_id
	               );
	        $this->session->set_userdata($pgdata);
        }

		if(!empty($id_obj) && $tipe === 'blog')
		{
			$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			// by blog
			$this->load->library('pagination');

			if (current_lang(false) === 'id')
			$this->session->set_userdata('tblogbasis',current_lang().'tag/blog/'.$judul_id);
			else
			$this->session->set_userdata('tblogbasis',current_lang().'tag/blog/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('tblogbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->cms->getCountAllTagsBlogFrontById($id_obj);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['blog_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];

			if ($this->cms->getCountAllTagsBlogFrontById($id_obj) > 0)
			$data['konten_blog'] = $this->cms->getAllTagsBlogFrontById($id_obj,$config['per_page'],$offset);
			else
			$data['konten_blog'] = array();

			$data['content'] = '';

			if(current_lang(false) === 'id')
			{
				if( ! empty($data['konten_blog']))
				{
					$data['title'] = 'Tag Blog - '.humanize($judul_id);
					$extra_seo = $data['title'].' '.$data['konten_blog'][0]->isi_id;
				} else {
					$data['title'] = 'Tag Blog Kosong';
					$data['content'] = 'Tag blog <u>'.format_pilkategori($tag).'</u> masih kosong.';
				}
			} else {
				if( ! empty($data['konten_blog']))
				{
					$data['title'] = 'Blog Tag - '.humanize($judul_en);
					$extra_seo = $data['title'].' '.$data['konten_blog'][0]->isi_en;
				} else {
					$data['title'] = 'Tag not available';
					$data['content'] = 'This Tag for <u>'.format_pilkategori($tag).'</u> not available at this moment.';
				}
			}
		}
		elseif(!empty($id_obj) && $tipe === 'album')
		{
			$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			$this->load->library('pagination');

			if (current_lang(false) === 'id')
			$this->session->set_userdata('talbbasis',current_lang().'tag/album/'.$judul_id);
			else
			$this->session->set_userdata('talbbasis',current_lang().'tag/album/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('talbbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->cms->getCountAllTagsAlbumFrontById($id_obj);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['album_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];

			if($this->cms->getCountAllTagsAlbumFrontById($id_obj) > 0)
			$data['gambar_album'] = $this->cms->getAllTagsAlbumFrontById($id_obj,$config['per_page'],$offset);
			else
			$data['gambar_album'] =  array();

			if (current_lang(false) === 'id')
			{
				$data['title'] = 'Tag Album - '.humanize($judul_id);
				$data['content'] = '';
				if( ! empty($data['gambar_album']))
				$extra_seo = $data['title'].' '.$data['gambar_album'][0]->ket_id;
			}
			else {
				$data['title'] = 'Album Tags - '.humanize($judul_en);
				$data['content'] = '';
				if( ! empty($data['gambar_album']))
				$extra_seo = $data['title'].' '.$data['gambar_album'][0]->ket_en;
			}
		}
		elseif(!empty($id_obj) && $tipe === 'video')
		{
			$this->load->library('pagination');

			if (current_lang(false) === 'id')
			$this->session->set_userdata('tvidbasis',current_lang().'tag/video/'.$judul_id);
			else
			$this->session->set_userdata('tvidbasis',current_lang().'tag/video/'.$judul_en);

			$config['base_url'] = base_url().$this->session->userdata('tvidbasis');
			$config['per_page'] = $this->per_halaman; 
			$config['uri_segment'] = 5;

			$config['total_rows'] = $this->cms->getCountAllTagsVideoFrontById($id_obj);
			$config = array_merge($config, $this->_frontPagination());
			if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
			{
				$this->pagination->initialize($config);
				$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
				$offset = $this->getOffsetPage($segment, $config['per_page']);
			}
			else {
				unset($config['prefix']);
				unset($config['suffix']);
				$this->pagination->initialize($config);
				$offset = $this->uri->segment($config['uri_segment'],0);
			}

			$data['video_page'] =  $this->pagination->create_links();
			$data['per_page'] = $config['per_page'];

			if($this->cms->getCountAllTagsVideoFrontById($id_obj) > 0)
			$data['video'] = $this->cms->getAllTagsVideoFrontById($id_obj,$config['per_page'],$offset);
			else
			$data['video'] =  array();

			if (current_lang(false) === 'id')
			{
				$data['title'] = 'Tag Video - '.humanize($judul_id);
				$data['content'] = '';
				if( ! empty($data['video']))
				$extra_seo = $data['title'].' '.$data['video'][0]->ket_id;
				else
				$data['content'] = 'Tag video <u>'.$judul_id.'</u> masih kosong.';
			}
			else {
				$data['title'] = 'Video Tags - '.humanize($judul_en);
				$data['content'] = '';
				if( ! empty($data['video']))
				$extra_seo = $data['title'].' '.$data['video'][0]->ket_en;
				else
				$data['content'] = 'Tags for <u>'.$judul_en.'</u> not available at this moment.';
			}
		}
		else {
			$data['title'] = $this->lang->line('lbl_halaman_kosong');
			$data['content'] = $this->lang->line('lbl_data_kosong');
		}

		//QrCode
		if(! isset($data['qrcode']) && ! empty($data['konten_blog']) || ! empty($data['gambar_album']) || ! empty($data['video']))
		$data['qrcode'] = $this->_buatImgQrCode(current_url());

		$data = array_merge($data, $this->_getSEO('','',$extra_seo));

		return $data;
	}

	// Config style pagination front
	function _frontPagination()
	{
		if($this->config->item('theme_id') == 'tema_metro')
		{
			$config['prefix'] = 'page';
			$config['suffix'] = '.html';
			$config['first_link'] = '<i class="icon-first-2"></i>';
			$config['first_tag_open'] = '<li class="first">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '<i class="icon-last-2"></i>';
			$config['last_tag_open'] = '<li class="last">';
			$config['last_tag_close'] = '</li>';
			$config['num_links'] = 4;
			$config['next_link'] = '<i class="icon-next"></i>';
			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = '<i class="icon-previous"></i>';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a>';
			$config['cur_tag_close'] = '</a></li>';
			$config['use_page_numbers'] = TRUE; // Use page number for segment instead of offset
		}
		else {
			$config['prefix'] = 'page';
			$config['suffix'] = '.html';
			$config['first_link'] = '&lsaquo;&lsaquo;';
			$config['first_tag_open'] = '<span class="page gradient">';
			$config['first_tag_close'] = '</span>';
			$config['last_link'] = '&rsaquo;&rsaquo;';
			$config['last_tag_open'] = '<span class="page gradient">';
			$config['last_tag_close'] = '</span>';
			$config['num_links'] = 4;
			$config['next_link'] = '&rsaquo;';
			$config['next_tag_open'] = '<span class="page gradient">';
			$config['next_tag_close'] = '</span>';
			$config['prev_link'] = '&lsaquo;';
			$config['prev_tag_open'] = '<span class="page gradient">';
			$config['prev_tag_close'] = '</span>';
			$config['num_tag_open'] = '<span class="page gradient">';
			$config['num_tag_close'] = '</span>';
			$config['cur_tag_open'] = '<span class="page gradient active">';
			$config['cur_tag_close'] = '</span>';
			$config['use_page_numbers'] = TRUE; // Use page number for segment instead of offset
		}

		return $config;
	}

// konten halaman
	function _getKonten($up_hits=TRUE)
	{
		// Cari halaman
		$id = $this->uri->segment(3,NULL);
		$halaman = $this->cms->getAllHalamanFrontByNama($id);

		// Ambil-Kontennya
		if (empty($halaman) || $id == NULL)
		{
			if ($this->uri->segment(1) == 'pemesanan' || $this->uri->segment(2) == 'pemesanan')
			{
				$data['noindex'] = TRUE;
				$data['tipe'] = 'pemesanan';
				$data['info'] = array_merge($this->_cek_stok(),$this->_cek_stok_order());

				$this->session->unset_userdata('pgbasis', '');
				$this->session->unset_userdata('pbasis', '');
				$this->session->unset_userdata('blogbasis', '');
				$this->session->unset_userdata('kpbasis', '');
				$this->session->unset_userdata('albbasis', '');
				$this->session->unset_userdata('kvidbasis', '');
				$this->session->unset_userdata('kalbbasis', '');

				if ($this->uri->segment(2) == 'update' || $this->uri->segment(3) == 'update')
				{
					$data['content'] = 'pemesanan_produk';
					$this->cart->update($this->input->post());
					$data['info'] = array_merge($this->_cek_stok(),$this->_cek_stok_order());
					if(empty($data['info']) && $this->cart->total_items() > 0)
					{
						$data['title'] = $this->lang->line('lbl_pemesanan');
						$data['info'][0] = $this->lang->line('lbl_update_keranjang');
					}
					else {
						$data['title'] = $this->lang->line('lbl_pemesanan_gagal');
					}

					//if(current_lang(false) === 'id')
					$this->session->set_userdata('nama_halaman', '/pemesanan');
				}
				elseif ($this->uri->segment(2) == 'cek_pesan' || $this->uri->segment(3) == 'cek_pesan')
				{
					$data['content'] = 'data_pemesanan';
					if(empty($data['info']) || $this->cart->total_items() > 0)
					{
						$data['title'] = $this->lang->line('lbl_proses_pemesanan');
						// set input text
						if ( $this->session->userdata('kunci_id') === FALSE  )
						{
							$data['nama'] = '';$data['tlp'] = '';$data['email'] = '';
							$data['negara'] = '';$data['prov'] = '';$data['wil'] = '';$data['pos'] = '';
							$data['alamat'] = config_item('format_alamat');
						}
						else {
							$custdata = $this->orderan->getPembeliById(bs_kode($this->session->userdata('kunci_id'), TRUE));
							$data['nama'] = dekodeString($custdata->nama_lengkap);
							$data['tlp'] = dekodeString($custdata->no_telpon);
							$data['email'] = dekodeString($custdata->email_cust);
							$data['negara'] = $custdata->id_negara;
							$data['prov'] = $custdata->id_provinsi;
							$data['wil'] = $custdata->id_wilayah;
							$data['pos'] = dekodeString($custdata->kode_pos);
							$almt = dekodeString($custdata->alamat_rumah);
							$data['alamat'] = str_ireplace("<br />", "\n", $almt);
						}
					}
					else {
						$data['title'] = $this->lang->line('lbl_proses_pemesanan_gagal');
					}
					
					if($this->_cek_formPesan() === FALSE)
					{
						$data['isi'] = '/partial/order/form_pesan';
					}
					else {
						if( ! $this->orderan->tambahPembeli())
						{
							$data['isi'] = '/partial/order/pembayaran_gagal';
						}
						else {
							//cari nilai diskon
							$this->_cekDiskonTransaksi($this->input->post('provinsi'));
							$data['login'] = $this->_cekLogin();

							$data['diskon_transaksi'] = bs_kode($this->session->userdata('diskon_transaksi'), TRUE);
							$data['total_transaksi'] = bs_kode($this->session->userdata('total_transaksi'), TRUE);
							$data['alamat_kirim'] = $this->_buildAlamatKirimEmail(bs_kode($this->session->userdata('alamat_id'), TRUE));

							//metode bayar
							$data['wilayah'] = $this->input->post('wilayah');
							$data['metode'] = $this->orderan->getMetodeBayarByWilayah($this->input->post('wilayah'));
							$data['vendor'] = $this->orderan->getVendorBayarByWilayah($this->input->post('wilayah'));
							
							$data['isi'] = '/partial/order/pembayaran';
						}
					}
					//if(current_lang(false) === 'id')
					$this->session->set_userdata('nama_halaman', '/pemesanan/cek_pesan');
				}
				elseif ($this->uri->segment(2) == 'pembayaran' || $this->uri->segment(3) == 'pembayaran')
				{
					$data['content'] = 'pembayaran_produk';
					$data['login'] = $this->_cekLogin();

					if (empty($data['info']) || $this->cart->total_items() > 0)
					{
						$data['title'] = $this->lang->line('lbl_metode_pembayaran');
					}
					else {
						$data['title'] = $this->lang->line('lbl_proses_pemesanan_gagal');
					}
					
					if ($this->_cek_formBayar() === FALSE)
					{
						$data['diskon_transaksi'] = bs_kode($this->session->userdata('diskon_transaksi'), TRUE);
						$data['total_transaksi'] = bs_kode($this->session->userdata('total_transaksi'), TRUE);
						$data['alamat_kirim'] = $this->_buildAlamatKirimEmail(bs_kode($this->session->userdata('alamat_id'), TRUE));
						//metode bayar
						$data['wilayah'] = $this->input->post('wilayah');
						$data['metode'] = $this->orderan->getMetodeBayarByWilayah($this->input->post('wilayah'));
						$data['vendor'] = $this->orderan->getVendorBayarByWilayah($this->input->post('wilayah'));
						
						$data['isi'] = '/partial/order/pembayaran';
					}
					else {

						if ( ! $this->orderan->tambahOrderById())
						{
							$data['isi'] = '/partial/order/pembayaran_gagal';
						}
						else {
							$metode = $this->input->post('metode',TRUE);
							//kirim email detail pesan ke pembeli + uname n pass klo blom terdaftar
							$trx = $this->session->userdata('trx_order');
							$detail = $this->orderan->getFrontAllOrderanByTrx($trx, true);
							$nama = $this->orderan->getNamaPembeliById($detail[0]['id_cust']);
							$status = ($metode == 'online')?'pembayaran lunas':$detail[0]['status_transaksi'];
							$via = $this->orderan->getDetailVendorBayarByNama($detail[0]['bayar_melalui']);
							$sub_total = $this->cart->total();
							$diskon = bs_kode($this->session->userdata('diskon_transaksi'), TRUE);
							$total = bs_kode($this->session->userdata('total_transaksi'), TRUE);
							$alamat = $this->_buildAlamatKirimEmail($detail[0]['id_alamat']);

							$isi = format_isi_email_transaksi(current_lang(false),$status,$metode,$via,$nama,$trx,$detail,$sub_total,$diskon,$total,$alamat);
							$judul = 'Detail Transaksi '.$this->config->item('site_name');

							kirim_email_user(array(), bs_kode($this->session->userdata('email_beli'), TRUE),$judul,$isi);

							$data['detail_order'] = $isi;
							$data['isi'] = '/partial/order/pembayaran_sukses';

							//hapus data cart dan data transaksi
							$this->cart->destroy();
							$belanja = array('trx_order'=>'','total_transaksi'=>'','diskon_transaksi'=>'');
							$this->session->unset_userdata($belanja);
						}
					}
					//if(current_lang(false) === 'id')
					$this->session->set_userdata('nama_halaman', '/pemesanan/pembayaran');
				}
				else {
					$data['content'] = 'pemesanan_produk';
					if (empty($data['info']) && $this->cart->total_items() > 0)
					{
						$data['title'] = $this->lang->line('lbl_pemesanan');
					}
					else {
						$data['title'] = $this->lang->line('lbl_pemesanan_gagal');
					}
					//if(current_lang(false) === 'id')
					$this->session->set_userdata('nama_halaman', 'pemesanan');
				}
			}
			else {
				$data['tipe'] = 'halaman';
				$data['title'] = $this->lang->line('lbl_halaman_kosong');
				$data['content'] = $this->lang->line('lbl_data_kosong');
			}
		}
		else {
			#$data['kat_konten'] = $this->cms->getKategoriKonten($halaman->tipe);
			$extra_seo = '';
			// update hits halaman
			if($up_hits)
			$this->cms->upHalamanHits($halaman->id_halaman);

			// tipe-halaman
			$data['tipe'] = $halaman->tipe;
			
			if(current_lang(false) === 'id')
			{
				$data['title'] = ucwords(humanize($halaman->label_id));
				$this->session->set_userdata('nama_halaman', $halaman->label_en);
			}
			else {
				$data['title'] = ucwords(humanize($halaman->label_en));
				$this->session->set_userdata('nama_halaman', $halaman->label_id);
			}

			if ($halaman->tipe == 'module')
			{
				$data['content'] = $halaman->id_module;
				$data['assets_module'] =  $this->cms->getModule();
				if (current_lang(false) === 'id')
				$this->session->set_userdata('fbasis',current_lang().'halaman/'.$id.'/'.$halaman->label_id);
				else
				$this->session->set_userdata('fbasis',current_lang().'pages/'.$id.'/'.$halaman->label_en);
			}
			elseif ($halaman->tipe == 'produk')
			{
				$this->load->library('pagination');
				if (current_lang(false) === 'id')
				{
					$this->session->set_userdata('pbasis',current_lang().'produk/'.$id);
					$this->session->set_userdata('pgbasis','/product/');
				}
				else{
					$this->session->set_userdata('pbasis',current_lang().'product/'.$id);
					$this->session->set_userdata('pgbasis','/produk/');
				}

				$config['base_url'] = base_url().$this->session->userdata('pbasis');
				$config['per_page'] = $this->per_halaman; 
				$config['uri_segment'] = 4;

				if ( ! empty($halaman->id_produk))
				{
					$config['total_rows'] = $this->mproduk->getCountFrontProdukById($halaman->id_produk);
					$config = array_merge($config, $this->_frontPagination());
					if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
					{
						$this->pagination->initialize($config);
						$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'],'page0'));
						$offset = $this->getOffsetPage($segment, $config['per_page']);
					}
					else {
						unset($config['prefix']);
						unset($config['suffix']);
						$this->pagination->initialize($config);
						$offset = $this->uri->segment($config['uri_segment'],0);
					}

					$data['produk_page'] =  $this->pagination->create_links();
					$data['per_page'] = $config['per_page'];
					$data['content'] = $this->mproduk->getAllFrontProdukById($halaman->id_produk,$config['per_page'],$offset);

					if (current_lang(false) === 'id')
					$extra_seo = $data['content'][0]->deskripsi;
					else
					$extra_seo = $data['content'][0]->deskripsi_en;

					$data['produk_js'] = array('zoom/multizoom.js');
					$data['produk_css'] = array('zoom/multizoom.css');
					$data['tipe_produk'] = 'induk';
				}
				else {
					$config['total_rows'] = $this->mproduk->getCountFrontProdukByKategori($halaman->kategori);
					$config = array_merge($config, $this->_frontPagination());
					if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
					{
						$this->pagination->initialize($config);
						$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
						$offset = $this->getOffsetPage($segment, $config['per_page']);
					}
					else {
						unset($config['prefix']);
						unset($config['suffix']);
						$this->pagination->initialize($config);
						$offset = $this->uri->segment($config['uri_segment'],0);
					}

					$data['produk_page'] =  $this->pagination->create_links();
					$data['per_page'] = $config['per_page'];
					$data['content'] = $this->mproduk->getFrontProdukByKategori($halaman->kategori,$config['per_page'],$offset);
					$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
					$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');
					$data['tipe_produk'] = 'kategori';

					if (current_lang(false) === 'id')
					$extra_seo = $data['content'][0]['deskripsi'];
					else
					$extra_seo = $data['content'][0]['deskripsi_en'];
				}
			}
			elseif ($halaman->tipe == 'blog') 
			{
				$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
				$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

				// by blog
				$this->load->library('pagination');
				$this->session->set_userdata('blogbasis',current_lang().'blog/'.$id);
				$this->session->set_userdata('pgbasis','/blog/');

				$config['base_url'] = base_url().$this->session->userdata('blogbasis');
				$config['per_page'] = $this->per_halaman; 
				$config['uri_segment'] = 4;

				$config['total_rows'] = $this->cms->getCountAllBlogFrontByKategori($halaman->kategori);
				$config = array_merge($config, $this->_frontPagination());
				if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
				{
					$this->pagination->initialize($config);
					$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
					$offset = $this->getOffsetPage($segment, $config['per_page']);
				}
				else {
					unset($config['prefix']);
					unset($config['suffix']);
					$this->pagination->initialize($config);
					$offset = $this->uri->segment($config['uri_segment'],0);
				}

				$data['blog_page'] =  $this->pagination->create_links();
				$data['per_page'] = $config['per_page'];
				$data['konten_blog'] = $this->cms->getAllBlogFrontByKategori($halaman->kategori,$config['per_page'],$offset);

				$data['content'] = '';

				if(current_lang(false) === 'id')
				$extra_seo = $data['konten_blog'][0]->isi_id;
				else
				$extra_seo = $data['konten_blog'][0]->isi_en;
			}
			elseif ($halaman->tipe == 'album') 
			{
				$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
				$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

				// by album
				if( ! empty($halaman->id_album))
				{
					$data['gambar_album'] = $this->cms->getAllAlbumFrontById($halaman->id_album);
					$data['per_page'] = 1;
					$data['content'] = '';

					if(current_lang(false) === 'id')
					$extra_seo = $data['gambar_album'][0]->ket_id;
					else
					$extra_seo = $data['gambar_album'][0]->ket_en;
				}
				else {
					$this->load->library('pagination');

					if(current_lang(false) === 'id')
					{
						$this->session->set_userdata('albbasis',current_lang().'album/'.$id);
						$this->session->set_userdata('pgbasis','/albums/');
					}else{
						$this->session->set_userdata('albbasis',current_lang().'albums/'.$id);
						$this->session->set_userdata('pgbasis','/album/');
					}

					$config['base_url'] = base_url().$this->session->userdata('albbasis');
					$config['per_page'] = $this->per_halaman; 
					$config['uri_segment'] = 4;

					$config['total_rows'] = $this->cms->getCountAllAlbumFrontByKategori($halaman->kategori);
					$config = array_merge($config, $this->_frontPagination());
					if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
					{
						$this->pagination->initialize($config);
						$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
						$offset = $this->getOffsetPage($segment, $config['per_page']);
					}
					else {
						unset($config['prefix']);
						unset($config['suffix']);
						$this->pagination->initialize($config);
						$offset = $this->uri->segment($config['uri_segment'],0);
					}

					$data['album_page'] =  $this->pagination->create_links();
					$data['per_page'] = $config['per_page'];
					$data['gambar_album'] = $this->cms->getAllAlbumFrontByKategori($halaman->kategori,$config['per_page'],$offset);

					if (current_lang(false) === 'id')
					{
						$data['content'] = html_entity_decode($halaman->isi_id);
						$extra_seo = $halaman->isi_id;
					}
					else {
						$data['content'] = html_entity_decode($halaman->isi_en);
						$extra_seo = $halaman->isi_en;
					}
				}
			}
			elseif ($halaman->tipe == 'video') 
			{
				$this->load->spark('video_helper/1.0.6');
				$data['fancybox_js'] = array('jquery.fancybox-1.3.4.pack.js');
				$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

				// by video
				if( ! empty($halaman->id_video))
				{
					$data['per_page'] = 1;
					$data['video'] = $this->cms->getAllVideoFrontById($halaman->id_video);
					$data['content'] = '';

					if(current_lang(false) === 'id')
					$extra_seo = $data['video'][0]->ket_id;
					else
					$extra_seo = $data['video'][0]->ket_en;
				}
				else {
					$this->load->library('pagination');

					if (current_lang(false) === 'id')
					{
						$this->session->set_userdata('vidbasis',current_lang().'video/'.$id);
						$this->session->set_userdata('pgbasis','/videos/');
						$data['content'] = html_entity_decode($halaman->isi_id);
						$extra_seo = $halaman->isi_id;
					}
					else {
						$this->session->set_userdata('vidbasis',current_lang().'videos/'.$id);
						$this->session->set_userdata('pgbasis','/video/');
						$data['content'] = html_entity_decode($halaman->isi_en);
						$extra_seo = $halaman->isi_en;
					}

					$config['base_url'] = base_url().$this->session->userdata('vidbasis');
					$config['per_page'] = $this->per_halaman; 
					$config['uri_segment'] = 4;

					$config['total_rows'] = $this->cms->getCountAllVideoFrontByKategori($halaman->kategori);
					$config = array_merge($config, $this->_frontPagination());
					if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
					{
						$this->pagination->initialize($config);
						$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
						$offset = $this->getOffsetPage($segment, $config['per_page']);
					}
					else {
						unset($config['prefix']);
						unset($config['suffix']);
						$this->pagination->initialize($config);
						$offset = $this->uri->segment($config['uri_segment'],0);
					}

					$data['video_page'] =  $this->pagination->create_links();
					$data['per_page'] = $config['per_page'];
					$data['video'] = $this->cms->getAllVideoFrontByKategori($halaman->kategori,$config['per_page'],$offset);
				}
			}
			else {
				if(current_lang(false) === 'id')
				{
					$data['content'] = html_entity_decode($halaman->isi_id);
					$extra_seo = $halaman->isi_id;
				}
				else {
					$data['content'] = html_entity_decode($halaman->isi_en);
					$extra_seo = $halaman->isi_en;
				}
			}

			if(current_lang(false) === 'id')
			{
				$description = $halaman->seo_description_id;
				$keyword = $halaman->seo_keyword_id;
			}
			else {
				$description = $halaman->seo_description_en;
				$keyword = $halaman->seo_keyword_en;
			}

			//QrCode
			if(! isset($data['qrcode']))
			$data['qrcode'] = $this->_buatImgQrCode(current_url());

			$data = array_merge($data, $this->_getSEO($keyword,$description,$extra_seo));
		}

		if( ! isset($data['kat_konten']))
		$data['kat_konten'] = $this->cms->getKategoriKonten();

		return $data;
	}

}

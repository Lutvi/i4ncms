<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

/* Form ATAS */
$lang['cari']				= "Cari";
$lang['masuk']				= "Masuk";
$lang['daftar']				= "Daftar";
$lang['lbl_form_cari']		= "Pencarian";
$lang['lbl_form_masuk']		= "Form Masuk";
$lang['lbl_form_daftar']	= "Form Daftar";

$lang['lbl_kata_pencarian']		= "Kata pencarian";

$lang['lbl_nama']				= "Nama";
$lang['lbl_id']					= "ID Akun";
$lang['lbl_kata_sandi']			= "Kata Sandi";
$lang['lbl_ulangi_kata_sandi']	= "Ulangi Kata Sandi";
$lang['lbl_email']				= "Email";

$lang['lbl_btn_masuk']		= "Masuk";
$lang['lbl_btn_keluar']		= "Keluar";
$lang['lbl_btn_daftar']		= "Mendaftar";

// validasi JS
$lang['lbl_cari_kosong']		= "<strong>Masukkan kata pencarian.</strong>";
$lang['lbl_nama_sandi_kosong']	= "<strong>Mohon isi ID Akun dan sandi Anda.</strong>";
$lang['lbl_nama_kosong']		= "<strong>Mohon isi ID Akun Anda.</strong>";
$lang['lbl_sandi_kosong']		= "<strong>Mohon isi sandi Anda.</strong>";
$lang['lbl_form_kosong']		= "<strong>Isi dataAnda, Form wajib diisi semua.!</strong>";

$lang['lbl_blm_terdaftar']				= "<strong>Akun tidak ada, Anda belum terdaftar.</strong>";
$lang['lbl_pendaftaran_sukses']			= "<span>Pendaftaran sukses, Anda bisa masuk sekarang.<br />Terima kasih telah menggunakan layanan kami.</span>";
$lang['lbl_pendaftaran_gagal_data']		= "<strong>Pendaftaran gagal, Data Anda tidak valid.!</strong>";
$lang['lbl_pendaftaran_gagal_lewati']	= "<strong>Pendaftaran gagal, Data Anda tidak dapat disetujui.!</strong>";

//Js Form Validtion
$lang['isi_nama'] 		= "Mohon isi nama";
$lang['minlen_nama'] 	= "Nama harus lebih dari 2 karakter";
$lang['maxlen_nama'] 	= "Nama tidak boleh lebih dari 20 karakter";

$lang['isi_namaid'] 		= "Mohon isi ID Akun";
$lang['minlen_namaid'] 		= "ID Akun harus lebih dari 2 karakter";
$lang['maxlen_namaid'] 		= "ID Akun tidak boleh lebih dari 20 karakter";
$lang['sudah_ada_namaid'] 	= "<b>ID Akun ini sudah digunakan.</b>";

$lang['isi_email'] 			= "Mohon isi alamat email";
$lang['maxlen_email'] 		= "Email tidak boleh lebih dari 60 karakter";
$lang['email_salah'] 		= "Format Email Anda tidak benar";
$lang['sudah_ada_email'] 	= "<b>Alamat Email tidak valid/sudah digunakan.</b>";

$lang['isi_sandi'] 		= "Mohon isi kata sandi";
$lang['minlen_sandi'] 	= "Kata sandi Anda harus lebih dari 6 karakter";
$lang['maxlen_sandi'] 	= "Kata sandi tidak boleh lebih dari 20 karakter";

$lang['isi_ulangi_sandi'] 	= "Mohon ulangi kata sandi";
$lang['samakan_sandi']		 = "Mohon disamakan dengan kata sandi yang diatas.";

$lang['lbl_btn_tutup'] 	= "Tutup";
$lang['lbl_salam'] 		= "Halo ,";

//Pofil
$lang['lbl_ganti_kata_sandi']	= "Ganti Kata Sandi";
$lang['lbl_btn_profil'] 		= "Profil";

// Halaman
$lang['lbl_halaman_kosong'] 	= "Halaman Kosong";
$lang['lbl_data_kosong'] 		= "<p>Data tidak ditemukan.</p>";
$lang['lbl_hasil_cari'] 	= "Hasil Pencarian";

//Toko
$lang['lbl_diskon'] 			= "Diskon";
$lang['lbl_harga'] 				= "Harga";
$lang['lbl_item'] 				= "Item";
$lang['keranjang'] 				= "Keranjang Belanja";
$lang['masuk_keranjang'] 		= "Masukkan Keranjang";
$lang['detail_produk'] 			= "Detail Produk";
$lang['ajukan_permintaan'] 		= "Ajukan Permintaan";
$lang['stok'] 					= "Stok";

$lang['btn_klik_foto_prod'] 	= "Klik untuk perbesar";

$lang['produk_tidak_ada'] 		= "Produk tidak ditemukan";
$lang['info_produk_tidak_ada'] 	= "Data produk tidak ditemukan";


$lang['btn_update_keranjang'] 	= "Update Keranjang";
$lang['btn_batal_keranjang'] 	= "Kosongkan";
$lang['lbl_pesan_keranjang'] 	= "Pesan Sekarang";

//Proses Order
$lang['btn_proses_pemesanan'] 	= "Proses Pemesanan";

$lang['lbl_pemesanan'] 					= "Pemesanan Produk";
$lang['lbl_update_keranjang'] 			= "Keranjang belanja Anda berhasil diupdate.";
$lang['lbl_pemesanan_gagal'] 			= "Proses Pemesanan Gagal";

$lang['lbl_proses_pemesanan'] 			= "Proses Pemesanan Produk";
$lang['lbl_proses_pemesanan_gagal'] 	= "Proses Pemesanan Produk Gagal";

$lang['lbl_metode_pembayaran'] 			= "Metode Pembayaran";

//form alamat kirim
$lang['lbl_data_pengiriman'] 		= "Data Pengiriman";
$lang['lbl_nama_penerima'] 			= "Nama";
$lang['lbl_info_nama_penerima'] 	= "Nama lengkap Penerima";
$lang['lbl_no_tlp_penerima'] 		= "No.Telpon";
$lang['lbl_info_no_tlp_penerima'] 	= "Nomor Telpon/Hp Penerima";
$lang['lbl_email_penerima'] 		= "Email";
$lang['lbl_info_email_penerima'] 	= "Alamat email Penerima";
$lang['lbl_negara_tujuan'] 			= "Negara";
$lang['lbl_info_negara_tujuan'] 	= "Negara untuk pengiriman";
$lang['lbl_prov_tujuan'] 			= "Provinsi";
$lang['lbl_info_prov_tujuan'] 		= "Provinsi untuk pengiriman";
$lang['lbl_wil_tujuan'] 			= "Wilayah";
$lang['lbl_info_wil_tujuan'] 		= "Wilayah untuk pengiriman";
$lang['lbl_kdpos_tujuan'] 			= "KodePos";
$lang['lbl_info_kdpos_tujuan'] 		= "KodePos pengiriman";
$lang['lbl_alamat_tujuan'] 			= "Alamat Lengkap";
$lang['lbl_info_alamat_tujuan'] 	= "Alamat lengkap pengiriman. sertakan pula RT/RW dan No.Rumah";
$lang['lbl_cek_alamat_tujuan'] 		= "Alamat harus di isi dengan benar.";

$lang['btn_proses_pembayaran'] 		= "Proses Pembayaran";
$lang['lbl_detail_pesanan'] 		= "Detail Pesanan";

$lang['lbl_pilihan_pembayaran'] 	= "Pilihan Pembayaran";
$lang['lbl_notes_pesanan'] 			= "Catatan";

$lang['lbl_pilih_pembayaran'] 		= "Pilih Metode Pembayaran Anda.";
$lang['lbl_catatan_pemesan'] 		= "Berikan catatan untuk kami tentang pesanan Anda.";
$lang['btn_pembayaran'] 			= "Selesai";
$lang['lbl_total_harga'] 			= "Total harga";
$lang['lbl_diskon_trans'] 			= "Diskon Transaksi";
$lang['lbl_total_bayar'] 			= "Total yang harus Anda bayar";
$lang['lbl_rp'] 					= "Rp.";

$lang['lbl_alamt_pengiriman'] 		= "Alamat Pengiriman";

//info proses pesan
$lang['lbl_pesanan_kosong'] 		= "Data Belanja Anda kosong, Proses Pemesanan tidak bisa dilanjutkan.";
$lang['lbl_pesanan_gagal_data'] 	= "Mohon Maaf telah terjadi kesalahan Server data-data Anda gagal diproses, Proses Pemesanan tidak bisa dilanjutkan.<br /> Silahkan coba beberapa saat lagi.";
$lang['lbl_pesanan_sukses_data'] 	= "Proses telah berhasil, kami akan segera menghubungi Anda melalui Email dan SMS/Telpon untuk mengkonfirmasi pesanan Anda.";

$lang['lbl_proses_pesan_beshasil'] 	= "Proses Pemesanan Berhasil";
$lang['lbl_mohon_cek_email'] 		= "Mohon cek Email Anda";
$lang['lbl_detail_pesanan_final'] 	= "Detail Pesanan Anda :";

//Konten
$lang['path_halaman'] 	= "id/halaman";
$lang['lbl_kategori'] 	= "Kategori Konten";
$lang['lbl_iklan'] 		= "Iklan";
$lang['lbl_tag'] 		= "Tag";
$lang['lbl_selengkapnya'] 	= "Selengkapnya...";

$lang['lbl_produk'] 	= "Produk";
$lang['lbl_blog'] 	= "Blog";
$lang['lbl_album'] 	= "Album";
$lang['lbl_video'] 	= "Video";
$lang['met_datang'] = "Selamat datang di";

$lang['lbl_bahasa']		= "Bahasa";
$lang['lbl_berdaya']	= "Diberdayakan oleh";
$lang['lbl_tentang']	= "Tentang website";

$lang['lbl_rating']		= "Peringkat";
$lang['lbl_nrating']	= "dari";
$lang['lbl_jvote']	= "suara.";
$lang['lbl_beriPeringkat']	= "Berikan peringkat ";
$lang['lbl_upPeringkat']	= "Peringkatnya sekarang ";

//Blog
$lang['lbl_ttg_penulis']	= "Tentang Penulis ";
$lang['jdl_posting_kosong']	= "Artikel tidak ada lagi.";
$lang['lbl_posting_kosong']	= "Judul artikel tidak ditemukan.";

//Album
$lang['jdl_album_kosong']	= "Album tidak ada lagi.";
$lang['lbl_album_kosong']	= "Judul album tidak ditemukan.";

//Video
$lang['jdl_video_kosong']	= "Video tidak ada lagi.";
$lang['lbl_video_kosong']	= "Judul video tidak ditemukan.";


/* End of file frontpage_lang.php */
/* Location: ./cms/language/indonesia/frontpage_lang.php */

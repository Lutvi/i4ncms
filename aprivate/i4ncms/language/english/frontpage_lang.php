<?php if ( ! defined('BASEPATH')) exit ('No dirrect script access allowed');

/* Form ATAS */
$lang['cari']				= "Search";
$lang['masuk']				= "Login";
$lang['daftar']				= "Sign Up";
$lang['lbl_form_cari']		= "Search Form";
$lang['lbl_form_masuk']		= "Login Form";
$lang['lbl_form_daftar']	= "SignUp form";

$lang['lbl_kata_pencarian']		= "Keywords";

$lang['lbl_nama']				= "Name";
$lang['lbl_id']					= "Account ID";
$lang['lbl_kata_sandi']			= "Password";
$lang['lbl_ulangi_kata_sandi']	= "Retype Password";
$lang['lbl_email']				= "Email";

$lang['lbl_btn_masuk']		= "Login";
$lang['lbl_btn_keluar']		= "Logout";
$lang['lbl_btn_daftar']		= "Register";

// validasi JS
$lang['lbl_cari_kosong']		= "<strong>Please type some words.</strong>";
$lang['lbl_nama_sandi_kosong']	= "<strong>Please type Account ID and passwords.</strong>";
$lang['lbl_nama_kosong']		= "<strong>Please type your Account ID.</strong>";
$lang['lbl_sandi_kosong']		= "<strong>Please type your password.</strong>";
$lang['lbl_form_kosong']		= "<strong>All fields is required.!</strong>";

$lang['lbl_blm_terdaftar']				= "<strong>Ivalid user, Please SignUp.</strong>";
$lang['lbl_pendaftaran_sukses']			= "<span>Your registration success, you can login now. <br /> Welcome to our services.</span>";
$lang['lbl_pendaftaran_gagal_data']		= "<strong>Registration Failed, Invalid data.!</strong>";
$lang['lbl_pendaftaran_gagal_lewati']	= "<strong>Registration Failed, Your data is ilegal.!</strong>";

//Js Form Validtion
$lang['isi_nama'] 		= "Please type your name";
$lang['minlen_nama'] 	= "Name must be at least 2 characters in length";
$lang['maxlen_nama'] 	= "Name can not exceed 20 characters in length";

$lang['isi_namaid'] 		= "Please type your Account ID";
$lang['minlen_namaid'] 		= "Account ID must be at least 2 characters in length";
$lang['maxlen_namaid'] 		= "Account ID can not exceed 20 characters in length";
$lang['sudah_ada_namaid'] 	= "<b>This Account ID is already used.</b>";


$lang['isi_email'] 			= "Please type your email";
$lang['maxlen_email'] 		= "Email can not exceed 60 characters in length";
$lang['email_salah'] 		= "Email must contain a valid email address.";
$lang['sudah_ada_email'] 	= "<b>Invalid Email address/This is already used.</b>";

$lang['isi_sandi'] 		= "Please type your password";
$lang['minlen_sandi'] 	= "Password must be at least 6 characters in length";
$lang['maxlen_sandi'] 	= "Password can not exceed 20 characters in length";

$lang['isi_ulangi_sandi'] 	= "Please retype your password";
$lang['samakan_sandi'] 		= "Password does not match.!";

$lang['lbl_btn_tutup'] 	= "Close";
$lang['lbl_salam'] 		= "Hello ,";

//Pofil
$lang['lbl_ganti_kata_sandi']	= "Change Password";
$lang['lbl_btn_profil'] 		= "Profile";

// Halaman
$lang['lbl_halaman_kosong'] 	= "Page Empty";
$lang['lbl_data_kosong'] 		= "<p>Data not found.</p>";
$lang['lbl_hasil_cari'] 	= "Search Results";

//Toko
$lang['lbl_diskon'] 			= "Save";
$lang['lbl_harga'] 				= "Price";
$lang['lbl_item'] 				= "Items";
$lang['keranjang'] 				= "Shopping Cart";
$lang['masuk_keranjang'] 		= "Add to Cart";
$lang['detail_produk'] 			= "Product details";
$lang['ajukan_permintaan'] 		= "Send Request";
$lang['stok'] 					= "Stock";

$lang['btn_klik_foto_prod'] 	= "Click for large image";

$lang['produk_tidak_ada'] 		= "Product not found";
$lang['info_produk_tidak_ada'] 	= "This product not in catalouges";

$lang['btn_update_keranjang'] 	= "Update Cart";
$lang['btn_batal_keranjang'] 	= "Empty Cart";
$lang['lbl_pesan_keranjang'] 	= "Order Now";

//Proses Order
$lang['btn_proses_pemesanan'] 	= "Get Order";

$lang['lbl_pemesanan'] 					= "Products Order";
$lang['lbl_update_keranjang'] 			= "Successfully update your shopping cart.";
$lang['lbl_pemesanan_gagal'] 			= "Order Process Failed";

$lang['lbl_proses_pemesanan'] 			= "Product Order Process";
$lang['lbl_proses_pemesanan_gagal'] 	= "Product Order Process Failed";

$lang['lbl_metode_pembayaran'] 			= "Payment Method";

//form alamat kirim
$lang['lbl_data_pengiriman'] 		= "Shipping address";
$lang['lbl_nama_penerima'] 			= "Name";
$lang['lbl_info_nama_penerima'] 	= "Full Name of Recipient";
$lang['lbl_no_tlp_penerima'] 		= "Telphone";
$lang['lbl_info_no_tlp_penerima'] 	= "Telphone number/cellphones";
$lang['lbl_email_penerima'] 		= "Email";
$lang['lbl_info_email_penerima'] 	= "Email address Recipient";
$lang['lbl_negara_tujuan'] 			= "Country";
$lang['lbl_info_negara_tujuan'] 	= "Country for Shipping";
$lang['lbl_prov_tujuan'] 			= "Province";
$lang['lbl_info_prov_tujuan'] 		= "Province for Shipping";
$lang['lbl_wil_tujuan'] 			= "Region";
$lang['lbl_info_wil_tujuan'] 		= "Region for Shipping";
$lang['lbl_kdpos_tujuan'] 			= "PostalCode";
$lang['lbl_info_kdpos_tujuan'] 		= "PostalCode for Shipping";
$lang['lbl_alamat_tujuan'] 			= "Full Address";
$lang['lbl_info_alamat_tujuan'] 	= "Complete delivery address. also include RT / RW and No.Rumah";
$lang['lbl_cek_alamat_tujuan'] 		= "Plase Insert your Complete address.";

$lang['btn_proses_pembayaran'] 		= "Goto Payment";
$lang['lbl_detail_pesanan'] 		= "Order Details";

$lang['lbl_pilihan_pembayaran'] 	= "Payment Vendor";
$lang['lbl_notes_pesanan'] 			= "Notes";

$lang['lbl_pilih_pembayaran'] 		= "Choose your payment method.";
$lang['lbl_catatan_pemesan'] 		= "Give a note to us about your order.";
$lang['btn_pembayaran'] 			= "Finish";
$lang['lbl_total_harga'] 			= "Total price";
$lang['lbl_diskon_trans'] 			= "Discounts";
$lang['lbl_total_bayar'] 			= "The total price you have to pay";
$lang['lbl_rp'] 					= "IDR ";

$lang['lbl_alamt_pengiriman'] 		= "Shipping address";

//info proses pesan
$lang['lbl_pesanan_kosong'] 		= "Your Shopping Cart is empty, process can not be continued.";
$lang['lbl_pesanan_gagal_data'] 	= "Sorry an error has occurred server fails processing your data, process can not be continued. <br /> Please try again.";
$lang['lbl_pesanan_sukses_data'] 	= "The process has been successful, we will immediately contact you by email and SMS or Calling to confirm your order.";

$lang['lbl_proses_pesan_beshasil'] 	= "Order Process Successfully";
$lang['lbl_mohon_cek_email'] 		= "Please check your email";
$lang['lbl_detail_pesanan_final'] 	= "Your Order Details :";

//Konten
$lang['path_halaman'] 	= "en/pages";
$lang['lbl_kategori'] 	= "Site Categories";
$lang['lbl_iklan'] 		= "Advertise";
$lang['lbl_tag'] 		= "Tag";
$lang['lbl_selengkapnya'] 		= "Read more...";

$lang['lbl_produk'] 	= "Products";
$lang['lbl_blog'] 	= "Blog";
$lang['lbl_album'] 	= "Albums";
$lang['lbl_video'] 	= "Videos";
$lang['met_datang'] = "Welcome to";

$lang['lbl_bahasa']		= "Language";
$lang['lbl_berdaya']	= "Powered By";
$lang['lbl_tentang']	= "About";

$lang['lbl_rating']		= "Average rating";
$lang['lbl_nrating']	= "based on";
$lang['lbl_jvote']	= "votes.";
$lang['lbl_beriPeringkat']	= "Give a rate ";
$lang['lbl_upPeringkat']	= "Now current rating is ";

//Blog
$lang['lbl_ttg_penulis']	= "About Author ";
$lang['jdl_posting_kosong']	= "The Article was not found.";
$lang['lbl_posting_kosong']	= "The title of the article was not found.";

//Album
$lang['jdl_album_kosong']	= "The Album was not found.";
$lang['lbl_album_kosong']	= "The title of this album was not found.";

//Video
$lang['jdl_video_kosong']	= "The Video was not found.";
$lang['lbl_video_kosong']	= "The title of this video was not found.";

/* End of file frontpage_lang.php */
/* Location: ./cms/language/english/frontpage_lang.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Hbanner extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman','batman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');
		$pp = $this->adm_per_halaman;

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Banner Header';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;

		$config['base_url'] = base_url().$this->config->item('admpath').'/hbanner';
		$config['total_rows'] = $this->web->getCountHbanner();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = (empty($sts))?3:4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		//banner
		$data['page'] =  $this->pagination->create_links();
        $data['hbanner'] = $this->web->getAllHbanner($config['per_page'],$offset);
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_hbanner',$data);
		}
		else {
			return $data;
		}
	}

	public function update_tabel()
	{
		$data= $this->index('ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_hbanner',$data);
        }
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function update_hbanner()
    {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		if( $this->input->post('hbanner_id'))
		{
			$this->form_validation->set_rules('hbanner_id', 'ID Banner Header', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Banner Header', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('hbid'))
		{
			$this->form_validation->set_rules('hbid', 'ID Banner Header', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('hbanner_id') && $this->_isSupAdmin() ) 
			{
				if( ! $this->web->upHbannerStat() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Status Banner Header..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->_isSupAdmin() ) {
				if( ! $this->web->hapusHbanner() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Hapus Banner Header..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('hbid') && $this->_isSupAdmin() ) {
				if( ! $this->web->upHbannerPos() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Posisi Banner Header..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }

	public function tambah_hbanner($sts='')
    {
		$this->hak = array('superman');
		
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css');
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMinimal($id='ket_id'),$this->editorMinimal($id='ket_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_hbanner',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_hbanner()
    {
		$this->hak = array('superman');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'required');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'required');
        $this->form_validation->set_rules('ket_id', 'Info singkat ID', 'required');
        $this->form_validation->set_rules('ket_en', 'Info singkat EN', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        /* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Gambar Header', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah_hbanner($sts);
        }
        else {
            if ( ! $this->_do_upload_hbanner() )
            {
                $error = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
                $this->session->set_flashdata('error', $error);
				$this->tambah_hbanner('error');
            }
            else {
                if( ! $this->web->addHbanner() )
                {
                    $error = "Gagal menyimpan data, Terjadi kesalahan pada server database";
	                $this->session->set_flashdata('error', $error);
					$this->tambah_hbanner('error');
                    hapus_file('./_media/banner-header/'.$this->upload->file_name);
                }
                else {
                    $info = "Sukses menambahkan gambar Header baru";
                    $this->session->set_flashdata('info', $info);
					redirect($this->config->item('admpath').'/hbanner', 'refresh');
                }
            }
        }
    }

    public function detail_hbanner($sts='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_banner'));
        
        $data['banner'] = $this->web->getHbannerById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMinimal($id='ket_id'),$this->editorMinimal($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_hbanner',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_form_hbanner()
    {
		$this->hak = array('superman');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'required');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'required');
        $this->form_validation->set_rules('ket_id', 'Info singkat ID', 'required');
        $this->form_validation->set_rules('ket_en', 'Info singkat EN', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        //Hidden
        $this->form_validation->set_rules('id_banner', 'ID', 'required');
        $this->form_validation->set_rules('nama_ori', 'Nama Ori', 'required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'required');
        $this->form_validation->set_rules('img_src', 'Img Src', 'required');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->detail_hbanner($sts);
        }
        else {
			if($this->input->post('status_ganti') == 'yes')
            {
				if ( ! $this->_do_upload_hbanner() )
				{
					$error = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->session->set_flashdata('error', $error);
					$this->detail_hbanner('error');
				}
				else {
					if( ! $this->web->upFormBanner() )
                    {
                        $error = "Gagal Update Banner, terjadi kesalahan pada server database";
						$this->session->set_flashdata('error', $error);
		                $this->detail_kurir('error');
                    }
                    else {
						//hapus logo lama
						hapus_file("./_media/banner-header/". $this->input->post('img_src'));
						hapus_file("./_media/banner-header/thumb_". $this->input->post('img_src'));

						$info = "Sukses memperbaharui data <strong>Banner ".$this->input->post('nama_ori')."</strong>";
						$this->session->set_flashdata('info', $info);
						redirect($this->config->item('admpath').'/hbanner','refresh');
                    }
				}
            }
            else {
				if ( ! $this->web->upFormBanner() )
                {
                    $error = "Gagal Update Banner, terjadi kesalahan pada server database";
					$this->session->set_flashdata('error', $error);
                }
                else {
                    $info = "Sukses memperbaharui data <strong>Kurir ".$this->input->post('nama_ori')."</strong>";
                    $this->session->set_flashdata('info', $info);
                    redirect($this->config->item('admpath').'/hbanner','refresh');
                }
			}
		}
    }

    function _do_upload_hbanner($path='./_media/banner-header/')
	{
		$this->load->helper('file');

		if ( ! is_dir($path) )
			mkdir($path, DIR_CMS_MODE);

		$config['file_name']  = time().'_'.$this->input->post('nama_id');
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';
        $config['max_width']  = '800';
        $config['max_height']  = '600';
        $config['min_width']  = '500';
        $config['min_height']  = '300';
        $config['upload_path'] = $path;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			print_r($this->upload->display_errors());
            hapus_file($path.$this->upload->file_name);
            return FALSE;
		}
		else {

			$config = array();
            // resize
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.$this->upload->file_name;
            $config['quality'] = '80';
            //$config['maintain_ratio'] = FALSE;
            //$config['master_dim'] = 'height';
            $config['width'] = 500;
            $config['height'] = 300;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();

			$config = array();
            // thumbs
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'thumb_'.$this->upload->file_name;
            $config['width'] = 63;
            $config['height'] = 32;
            //$this->load->library('image_lib', $config);
            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                hapus_file($path.$this->upload->file_name);
                return FALSE;
            }
			return TRUE;
		}
	}

}

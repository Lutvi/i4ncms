<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Atur_widget extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');
		
		$data = $this->_getAdminAssets(FALSE,TRUE);
		$data['title'] = 'Admin Widget';
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/atur_widget';
		$config['total_rows'] = $this->cms->getCountWidget();
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['widget'] = $this->cms->getAllWidget($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$data['partial_content'] = 'v_atur_widget';

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$this->load->library('pagination');
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/atur_widget/update_tabel';
		$config['total_rows'] = $this->cms->getCountMenu();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['widget'] = $this->cms->getAllWidget($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = '';

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_widget',$data);
		}
		else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	function admin_widget()
	{
		$this->load->spark('friendly-template/1.1.2');

		$nama = $this->uri->segment(4);
		$js = array('jqui/jquery-1.8.3-min.js');
		$data['admin'] = $this->template->widget($nama, array('wid_title'=> $nama, 'wid_content' => 'admin', 'js' => $js));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->load->view('widgets/general_admin',$data);
		}
		else {
			$konten['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$konten);
        }
	}

	function install_widget()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo form_open_multipart($this->config->item('admpath').'/atur_widget/upload_widget');
			echo '<input type="file" name="userfile" />';
			echo '<p>Aktifkan widget : <input type="checkbox" name="aktif" value="1" /><br /><small>Ceklist ini untuk aktifkan widget otomatis.</small></p>';
			//if ( $this->super_admin === TRUE )
			echo '<p>Fresh Install : <input type="checkbox" name="fresh" value="1" /><br /><small>Ceklist ini untuk hapus data yang lama.</small></p>';
			echo form_submit('mysubmit', 'Submit');
			echo form_close();
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function add_widget()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('widget_name', 'Nama Widget', 'trim|required|alpha_dash|callback__widget_cek');
		$this->form_validation->set_rules('judul_widget', 'Judul Widget Indonesia', 'trim_judul|required');
		$this->form_validation->set_rules('judul_widget_en', 'Judul Widget English', 'trim_judul|required');
		$this->form_validation->set_rules('widget_css', 'CSS Widget', 'trim');
		$this->form_validation->set_rules('widget_js', 'JS Widget', 'trim');
		$this->form_validation->set_rules('widget_html', 'HTML Widget', 'trim');
		$this->form_validation->set_rules('widget_status', 'Status Widget', 'trim|required|less_than[2]');
		$this->form_validation->set_rules('widget_path', 'Path Widget', 'trim');
		$this->form_validation->set_rules('widget_desc', 'Deskripsi Widget', 'trim');

		if ( $this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$sts = 'cek';
			$this->index($sts);
		}
		else {
			
			if(!$this->cms->addWidget())
			{
				$sts = 'error';
				$this->index($sts);
			}
			else {
				$this->index($sts='');
			}
		}
	}

	function update_widget()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('widget_id'))
		{
			$this->form_validation->set_rules('widget_id', 'ID Widget', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|integer|less_than[2]|xss_clean');
			$this->form_validation->set_rules('judul_widget', 'Judul ID', 'trim_judul|required|max_length[50]|xss_clean');
			$this->form_validation->set_rules('judul_widget_en', 'Judul EN', 'trim_judul|required|max_length[50]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Widget', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('wid'))
		{
			$this->form_validation->set_rules('wid', 'ID Widget', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('widget_id') && $this->_isSupAdmin() )
			{
				if( ! $this->cms->upWidgetStat() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Widget..!!';
				}
				else {
					echo 'sukses';
				}
			} 
			elseif ( $this->input->post('id_hps') && $this->_isSupAdmin() ) {
				if( ! $this->cms->hapusWidget() || ! $this->_isSupAdmin() )
				{
					if ($this->input->post('id_hps') === '3')
					echo 'Gagal Hapus, Widget ini statis tidak bisa dihapus..!';
					else
					echo 'Gagal Hapus Widget..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('wid') && $this->_isSupAdmin() ) {
				if( ! $this->cms->upWidgetPos()  || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Posisi Widget..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
	}

	function upload_widget()
	{
		if ( $this->do_upload() )
		{
			$fol = $this->upload->data();
			$fol = $fol['raw_name'];
			$info = '';

			$this->load->library('unzip');
			$this->unzip->extract('./_tmp/bak/'.$this->upload->file_name, './_tmp/install/');

			if ( file_exists("./_tmp/install/". $fol ."/install.xml") ) {

				$fol_install = "./_tmp/install/" . $fol;
				$xml = simplexml_load_file($fol_install ."/install.xml");

				$new = array();
				$buat = array();
				$cek = $xml->getName();
				if ( $cek == 'widget' )
				{
					foreach ( $xml->children() as $child )
					{
					   $new[$child->getName()] = htmlspecialchars_decode($child);
					}

					if ( (count($new) == 8 || count($new) == 9) && ! empty($new['nama_widget']) )
					{
						$new['nama_widget'] = underscore($new['nama_widget']);

						if ( ! empty($new['konfigurasi']) )
						{
							foreach( $xml->konfigurasi->children() as $item )
							{
								$buat[$item->getName()] = htmlspecialchars_decode($item);
							}

							if( ! empty($buat['tabel']) )
							{
								$nm_tabel = $buat['nama_tabel'];
								$i = 0;
								foreach ( $xml->konfigurasi->tabel->children() as $keys => $isi )
								{
									$kolom[$i] = $isi;
									$i++;
								}
									$fields = array();
									for( $x = 0; $x < count($kolom); $x++ )
									{
										$field[$x] = array( (string)$kolom[$x]->nama => (array)$kolom[$x]->setup );
										$fields = array_merge((array)$fields,(array)$field[$x]);
									}

									if ( $this->cms->adaWidget($new['nama_widget']) )
									{
										$this->load->dbforge();
										$this->dbforge->add_field('id');
										$this->dbforge->add_field($fields);

										if ( $this->input->post('fresh') == 1 )
											$this->dbforge->drop_table($nm_tabel);
										if ( $this->dbforge->create_table($nm_tabel, TRUE) )
										{
											$info = "<p><strong>Sukses membuat tabel untuk widget.</strong> -> " . $nm_tabel . "..!</p>";
										}
										else {
											$info = "<p><strong>Gagal membuat tabel untuk widget.</strong> -> " . $nm_tabel . ".!</p>";
										}
									}

									$ubah = array_pop($new);
									$new = array_merge($new, array('tabel_data' => $nm_tabel));
							}
						}

						$this->_simpan_install($fol_install, $new, $info);
					}
					else {
						echo "Perintah installer widget tidak valid.!";
						delete_files('./_tmp/install/', TRUE);
						$this->install_widget();
					}
				}
				else {
					echo "Paket(.zip) installer widget tidak valid.!";
					delete_files('./_tmp/install/', TRUE);
					$this->install_widget();
				}
			}
			else {
				echo "Paket(.zip) installer corrupt/rusak.!";
				$this->install_widget();
			}
		}
		else {
			echo "<strong>Gagal Upload.!</strong> <br />".$this->upload->display_errors();
			$this->install_widget();
		}
	}

	function do_upload()
	{
		$this->load->helper('file');
		$pins = './_tmp/bak/';
		if ( $this->_cekbesar($pins) > $this->config->item('max_tmp_install') )
		{
			delete_files($pins, TRUE);
		}
		delete_files('./_tmp/install/', TRUE);
		
		$config['upload_path'] = './_tmp/bak/';
		$config['allowed_types'] = 'zip';
		$config['max_size']	= '1024';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _simpan_install($fol_install, $new, $info = '')
	{
		$pos = $this->cms->maxPosWidget()+1;
		$new = array_merge($new, array('posisi' => $pos, 'status' => $this->input->post('aktif')));
		$old = umask(002);
		if ( ! $this->cms->uploadAddWidget($new) )
		{
			echo "<p>Widget <strong>" . $new['nama_widget'] . "</strong> sudah terpasang.<p/>";
			$this->install_widget();
		}
		else {
			if ( count($new) == 11 || count($new) == 10 )
			{
				if( empty($new['html']) )
				{
					$info .= $this->_widgets_file($fol_install, $new);
					$info .= $this->_assets_file($fol_install, $new);
					$info .= "Sukses, widget script <strong>" . $new['nama_widget'] . "</strong> telah terpasang";
				}
				else {
					$info .= $this->_assets_file($fol_install, $new);
					$info .= "Sukses, widget html <strong>" . $new['nama_widget'] . "</strong> telah terpasang";
				}
				$this->tutup_dialog($info,5000);
			}
			else {
				$this->cms->hapusWidgetByNama($new['nama_widget'],$new['tabel_data']);
                echo "<p>GAGAL-Perintah installer tidak bisa diproses.!</p>";
				$this->install_widget();
			}
		}

		delete_files('./_tmp/install/', TRUE);
		umask($old);

		if ($old != umask()) {
			die('An error occurred while changing back the umask.');
		}
	}

	function _widgets_file($fol_install, $new)
	{
		$info = '';
		if ( file_exists( $fol_install ."/widgets/". $new['nama_widget'] . EXT ) )
		{
			$file = $fol_install ."/widgets/". $new['nama_widget'] . EXT;
			$pindah = APPPATH ."widgets/". $new['nama_widget'] . EXT;
			$info .= "<strong>Widget-Class :</strong><br />";
			if( ! $this->_pindah_file($file, $pindah))
			{
				$info .= "Class - [" . symbolic_permissions(fileperms($pindah)) . "] " . $new['nama_widget'] . " tidak bisa terpasang...<br />";
			}
			else {
				$info .= "Sukses memasang Class - [". symbolic_permissions(fileperms($pindah)) . "] " . $new['nama_widget'] . "...<br />";
			}
		}
		else {
			$info .= "<p>GAGAL-pasang-widgets-Class...<br />File Tidak ditemukan.</p>";
		}

		if ( file_exists( $fol_install ."/views/". $new['nama_widget'] . "_view" . EXT ) )
		{
			$file = $fol_install ."/views/". $new['nama_widget'] . "_view" . EXT;
			$pindah = CMSPATH ."widgets/". $new['nama_widget'] . "_view" . EXT;
			$info .= "<strong>Widget-Views :</strong><br />";

			if( ! $this->_pindah_file($file, $pindah))
			{
				$info .= "View - [" . symbolic_permissions(fileperms($pindah)) ."] ". $new['nama_widget'] . "_view sudah terpasang sebelumnya...<br />";
			}
			else {
				$info .= "Sukses memasang View - [" . symbolic_permissions(fileperms($pindah)) . "] " . $new['nama_widget'] . "_view ...<br />";
			}
		}
		else {
			$info .= "<p>GAGAL-pasang-widgets-views...<br />File Tidak ditemukan.</p>";
		}

		if ( file_exists( $fol_install ."/models/model_". $new['nama_widget'] . EXT ) )
		{
			$file = $fol_install ."/models/model_". $new['nama_widget'] . EXT;
			$pindah = APPPATH ."models/widgets/model_". $new['nama_widget'] . EXT;
			$info .= "<strong>Widget-Models :</strong><br />";

			if( ! $this->_pindah_file($file, $pindah))
			{
				$info .= "Models - ["  . symbolic_permissions(fileperms($pindah)) . "] model_" . $new['nama_widget']. " tidak bisa terpasang...<br />";
			}
			else {
				$info .= "Sukses memasang Models - ["  . symbolic_permissions(fileperms($pindah)) . "] model_" . $new['nama_widget'] . "...<br />";
			}
		}
		else {
			if ( ! empty($new['konfigurasi']['tabel']) )
			$info .= "<p>GAGAL-pasang-widgets-Models...<br />File Tidak ditemukan.</p>";
		}

		return $info;
	}

	function _assets_file($fol_install, $new)
	{
		$info = '';
		if ( ! empty($new['js']) )
		{
			$js = explode(',', $new['js']);
			$jml = count($js);
			$info .= "<strong>JS :</strong><br />";
			for($i = 0; $i < $jml; $i++)
			{
				if ( file_exists( $fol_install ."/js/". $js[$i] ) )
				{
					$file = $fol_install ."/js/". $js[$i];
					$pindah = "./assets/js/widget/". $new['nama_widget']."/". $js[$i];
					
					if ( ! is_dir("./assets/js/widget/". $new['nama_widget']) )
						mkdir("./assets/js/widget/". $new['nama_widget'], DIR_CMS_MODE);
					
					if ( ! $this->_pindah_file($file, $pindah) )
						$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
				}
				else {
					$info .= "File <strong>" . $js[$i] . "</strong> tidak ditemukan di folder install!...<br />";
				}
			}
		}

		if ( ! empty($new['css']) )
		{
			$css = explode(',', $new['css']);
			$jml = count($css);
			$info .= "<strong>CSS :</strong><br />";
			for($i = 0; $i < $jml; $i++)
			{
				if ( file_exists( $fol_install ."/css/". $css[$i] ) )
				{
					$file = $fol_install . "/css/"  . $css[$i];
					$pindah = "./assets/css/widget/". $new['nama_widget'] . "/" . $css[$i];
				
					if ( ! is_dir("./assets/css/widget/". $new['nama_widget']) )
						mkdir("./assets/css/widget/". $new['nama_widget'], DIR_CMS_MODE);
					
					if ( ! $this->_pindah_file($file, $pindah) )
						$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah sukses terpasang.<br />";
				}
				else {
					$info .= "File <strong>" . $css[$i] . "</strong> tidak ditemukan di folder install!...<br />";
				}
			}
		}

		if ( is_dir($fol_install ."/img") )
		{
			$ftmp = $fol_install ."/img";
			$img = opendir($ftmp);
			$pindah = "./assets/css/widget/". $new['nama_widget'] . "/img"; 
			$info .= "<strong>CSS-img :</strong><br />";

			if ( ! is_dir($pindah) )
				mkdir($pindah, DIR_CMS_MODE);
			
			while($files=readdir($img)){
	            if($files!="." && $files!="..")
	            {
					if ( ! $this->_pindah_file($ftmp . "/" . $files, $pindah . "/" . $files) )
						$info .= "[" . symbolic_permissions(fileperms($pindah . "/" . $files))  . "] " . "$pindah/$files tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah . "/" . $files))  . "] " . "$pindah/$files sukses terpasang.<br />";
	            }
	        }
	        closedir($img);
		}

		return $info;
	}

	function _widget_cek($str)
	{
		if ( $this->cms->adaWidget($str) == FALSE )
		{
			$this->form_validation->set_message('_widget_cek', 'Widget dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

}

/* End of file atur_widget.php */
/* Location: ./application/controllers/admin/atur_widget.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends AdminController {

    public function __construct()
    {
        parent::__construct();

        $this->_cekStsLoginAdm();
        $this->hak = array('superman','batman','ironman');
        $this->_hpsQryCache('blog');
    }

    public function index($sts='')
    {
		$this->load->library('pagination');
        
        /*/- cek data jika kosong hapus semua foto -/*/
        $this->_kosong_folder();
        
        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Blog';
        
        $config['base_url'] = base_url().$this->config->item('admpath').'/blog';
        $config['total_rows'] = $this->cms->getCountBlog();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 3;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['blog_web'] = $this->cms->getAllBlog($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data['partial_content'] = 'v_blog';

        $this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
    }

    public function update_tabel()
    {
        $this->load->library('pagination');
        
        $config['base_url'] = site_url($this->config->item('admpath').'/album/update_tabel');
        $config['total_rows'] = $this->cms->getCountBlog();
        $config['per_page'] = $this->adm_per_halaman; 
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['blog_web'] = $this->cms->getAllBlog($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_blog',$data);
        }
        else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function tambah($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css','elem/magicsuggest-1.3.1.css');
        $data['parent_album'] = $this->cms->getAlbumSelect();
        $data['parent_video'] = $this->cms->getVideoSelect();
        $data['kat_blog'] = $this->cms->getKategori('blog');
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorBlogBagus($id='isi_id'),$this->editorBlogBagus($id='isi_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_blog',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail()
    {
        $id = $this->uri->segment(4,$this->input->post('id_blog'));
        
        $data['blog'] = $this->cms->getAllBlogAdminById($id);
        $data['tags'] = $this->cms->getTagsIdJsonByObjekId($id,'blog');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['parent_album'] = $this->cms->getAlbumSelect();
        $data['parent_video'] = $this->cms->getVideoSelect();
        $data['kat_blog'] = $this->cms->getKategori('blog');
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data = array_merge($data,$this->editorBlogBagus($id='isi_id'),$this->editorBlogBagus($id='isi_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_blog',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_blog()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('judul_id', 'Judul ID', 'trim_judul|required|max_length[100]|callback__blog_cek');
        $this->form_validation->set_rules('judul_en', 'Judul EN', 'trim_judul|required|max_length[100]|callback__blog_cek');
        $this->form_validation->set_rules('kategori_blog', 'Kategori', 'trim|required|integer');
        $this->form_validation->set_rules('isi_id', 'Konten Indonesia', 'trim|prep_for_form|required');
        $this->form_validation->set_rules('isi_en', 'Konten English', 'trim|prep_for_form|required');
        
        $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[200]|xss_clean');

        $this->form_validation->set_rules('snippet', 'Komentar Video', 'trim|max_length[1]');
        $this->form_validation->set_rules('headline', 'Headline', 'required');
        $this->form_validation->set_rules('diskusi', 'Komentar Video', 'required');
        $this->form_validation->set_rules('status', 'Status Blog', 'required');

        /*hidden*/
        $this->form_validation->set_rules('id_penulis', 'ID Penulis', 'trim|required');

        /* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Gambar Cover', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah($sts);
        }
        else {
            if ( ! $this->_do_upload() )
            {
                $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai.";
                $this->tutup_dialog_gagal($info);
            }
            else {
                if( ! $this->cms->addBlog() )
                {
                    $sts = 'error';
                    $this->tambah($sts);
                }
                else {
                    $info = "Sukses menambahkan konten Blog baru";
                    $this->tutup_dialog($info);
                }
                delete_files('./_media/blog/big/');
            }
        }
    }

    public function update_formblog()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('judul_id', 'Judul ID', 'trim_judul|required|max_length[100]|callback__update_blog_cek_id');
        $this->form_validation->set_rules('judul_en', 'Judul EN', 'trim_judul|required|max_length[100]|callback__update_blog_cek_en');
        $this->form_validation->set_rules('kategori_blog', 'Kategori', 'trim|required|integer');
        $this->form_validation->set_rules('isi_id', 'Konten Indonesia', 'trim|prep_for_form|required');
        $this->form_validation->set_rules('isi_en', 'Konten English', 'trim|prep_for_form|required');

        $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
		$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[200]|xss_clean');

        $this->form_validation->set_rules('snippet', 'Komentar Video', 'trim|max_length[1]');
        $this->form_validation->set_rules('headline', 'Headline', 'required');
        $this->form_validation->set_rules('diskusi', 'Komentar Video', 'required');
        //$this->form_validation->set_rules('status', 'Status Blog', 'required');

        // hidden
        $this->form_validation->set_rules('id_penulis', 'ID Penulis', 'trim|required');
        $this->form_validation->set_rules('nama_ori_id', 'Ori ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_ori_en', 'Ori EN', 'trim_judul|required');
        $this->form_validation->set_rules('id_blog', 'ID', 'trim|required');
        $this->form_validation->set_rules('tag_ori', 'TAG', 'trim|required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail();
        }
        else {
            if($this->input->post('status_ganti') == 'yes')
            {
                if( ! $this->_do_upload() )
                {
                    $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->tutup_dialog_gagal($info);
                }
                else {
                    if( ! $this->cms->upBlogForm() )
                    {
                        $this->detail();
                    }
                    else {
                        $file = trim($this->input->post('gambar'));
                        $this->_hapus_gambar($file);

                        $info = "Sukses memperbaharui data <strong>Posting ".$this->input->post('judul_id')."</strong>";
                        $this->tutup_dialog($info);
                    }
                    delete_files('./_media/blog/big/');
                }
            }
            else {
                if ( ! $this->cms->upBlogForm() )
                {
                    $this->detail();
                }
                else {
                    $info = "Sukses memperbaharui data <strong>Posting ".$this->input->post('judul_id')."</strong>";
                    $this->tutup_dialog($info);
                }
            }
        }
    }

    public function update_blog()
    {
    	$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		if( $this->input->post('blog_id'))
		{
			$this->form_validation->set_rules('blog_id', 'ID blog', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('diskusi', 'Komentar', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('headline', 'Utama', 'required|max_length[1]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Blog', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			$info = array(	'aksi' => 'gagal',
							'info' => '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors()
							);
			echo json_encode($info);
        }
        else {
			if ( $this->input->post('blog_id') && $this->super_admin ) 
			{
				if( ! $this->cms->upBlogStat() || ! $this->super_admin )
				{
					$info = array(	'aksi' => 'gagal',
									'info' => 'Gagal Ubah Status Blog..!!'
							);
					echo json_encode($info);
				}
				else {
					$data = array(
							'aksi' => 'sukses',
							'headline' => $this->input->post('headline'),
							'diskusi' => $this->input->post('diskusi'),
							'status' => $this->input->post('status')
							);
					echo json_encode($data);
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->cms->hapusBlog() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Blog..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				if ( $this->input->post('blog_id'))
				{
					$info = array(	'aksi' => 'gagal',
									'info' => 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.'
							);
					echo json_encode($info);
				}
				else {
					echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
				}
			}
        }
    }

    public function list_kategori()
	{
		$kat = $this->cms->getKategori('blog');
		$result = $this->_getKategoriList($kat);

		echo json_encode($result);
	}

    public function tambah_kategori()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nama_kat_id', 'Nama kategori ID', 'trim_judul|required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('nama_kat_en', 'Nama kategori EN', 'trim_judul|required|min_length[3]|max_length[50]');
		
		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$data = array('status'=>'Gagal, Data yang dikirim tidak valid.');
		}
		else {
			if( ! $this->cms->cekKategori($this->input->post('nama_kat_id'),'id','blog'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_id')).'" sudah ada.');
			}
			elseif( ! $this->cms->cekKategori($this->input->post('nama_kat_en'),'en','blog'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_en')).'" sudah ada.');
			}
			else {
				if( ! $this->cms->addKategori('blog') )
				{
					$data = array('status'=>'Gagal menambahkan data ke database.');
				}
				else {
					$data = array('status'=>'sukses', 'tipe'=>'kategori');
				}
			}
		}
		echo json_encode($data);
	}

	public function list_select_kategori()
	{
		$kat = $this->cms->getKategori('blog');
		echo '<option value="">-- Tidak ada --</option>';
		foreach($kat as $items)
		{
			echo '<option value="'.$items->id_kategori.'">'.$items->label_id.' | '.$items->label_en.'</option>';
		}
	}

    function _do_upload($path='./_media/blog/')
    {
        $this->load->helper('file');

        if ( ! is_dir($path.'big') )
			mkdir($path.'big', DIR_CMS_MODE);

		if ( ! is_dir($path.'medium') )
			mkdir($path.'medium', DIR_CMS_MODE);

		if ( ! is_dir($path.'small') )
			mkdir($path.'small', DIR_CMS_MODE);

		if ( ! is_dir($path.'thumb') )
			mkdir($path.'thumb', DIR_CMS_MODE);
        
        $config['file_name']  = random_string('alnum', 5).'_'.$this->input->post('judul_id');
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '1024';
        $config['max_width']  = '1800';
        $config['max_height']  = '1600';
        $config['remove_spaces']  = TRUE;
        $config['upload_path'] = $path.'big/';
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            print_r($this->upload->display_errors());
            delete_files($path.'big/');
            return FALSE;
        }
        else {
            // sukses upload => buat ukuran yang lainnya
            
            $config = array();
            // medium
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'medium/medium_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 800;
            $config['height'] = 600;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            
            $config = array();
            // small
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'small/small_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 320;
            $config['height'] = 240;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            
            $config = array();
            // thumb
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'thumb/thumb_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 144;
            $config['height'] = 96;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            
            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                if (is_dir($path.'big'))
                delete_files($path.'big/');
                return FALSE;
            }
            
            return TRUE;
        }
    }

    function _blog_cek($str)
    {
        if ($this->cms->adaBlog($str) == FALSE)
        {
            $this->form_validation->set_message('_blog_cek', 'Posting dengan judul "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_blog_cek_id($str)
    {
        if ($this->cms->adaUpdateBlog($str,$this->input->post('nama_ori_id'),'id') == FALSE)
        {
            $this->form_validation->set_message('_update_blog_cek_id', 'Posting dengan judul "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_blog_cek_en($str)
    {
        if ($this->cms->adaUpdateBlog($str,$this->input->post('nama_ori_en'),'en') == FALSE)
        {
            $this->form_validation->set_message('_update_blog_cek_en', 'Posting dengan judul "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _hapus_gambar($file='',$path_blog = './_media/blog/')
    {
        // hapus gambar yang lama
        if (file_exists($path_blog.'medium/medium_'.$file) && is_writable($path_blog.'medium/medium_'.$file))
            unlink($path_blog.'medium/medium_'.$file);
        if (file_exists($path_blog.'small/small_'.$file) && is_writable($path_blog.'small/small_'.$file))
            unlink($path_blog.'small/small_'.$file);
        if (file_exists($path_blog.'thumb/thumb_'.$file) && is_writable($path_blog.'thumb/thumb_'.$file))
            unlink($path_blog.'thumb/thumb_'.$file);
    }

    function _kosong_folder()
    {
        // @return TRUE => Sukses , FALSE => Gagal
        $path = './_media/blog/';
        
        // kosongkan folder jika data tabel kosong
        if( $this->cms->getCountBlog() === NULL || $this->cms->getCountBlog() <= 0)
        {
            $this->load->helper('file');

            if (is_dir($path.'big'))
            delete_files($path.'big/');
            if (is_dir($path.'medium'))
            delete_files($path.'medium/');
            if (is_dir($path.'small'))
            delete_files($path.'small/');
            if (is_dir($path.'thumb'))
            delete_files($path.'thumb/');
        }
    }

}

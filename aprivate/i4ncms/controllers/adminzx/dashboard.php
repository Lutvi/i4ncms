<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends AdminController {
	
	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
	}
	
	public function index()
	{
		$data = $this->_getAssets();
		$data['title'] = 'Admin Dashboard';
		
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['partial_content'] = 'v_dashboard';

		//sortcut menu
		$data['sortcut'] = $this->_getSortcutMenu();
		$data['info'] = $this->_getSortcutMenu(FALSE);

		$data['orderan'] = array(
						array('label' => 'Pembayaran','total' =>  $this->orderan->getCountOrderProses('pembayaran'),'ikon' => 'pembayaran.png'),
						array('label' => 'Pending','total' =>  $this->orderan->getCountOrderProses('pending'), 'ikon' => 'pending.png'),
						array('label' => 'Pemaketan','total' =>  $this->orderan->getCountOrderProses('pemaketan'), 'ikon' => 'pemaketan.png'),
						array('label' => 'Pengiriman','total' =>  $this->orderan->getCountOrderProses('pengiriman'), 'ikon' => 'pengiriman.png'),
						array('label' => 'Batal','total' =>  $this->orderan->getCountOrderProses('batal'), 'ikon' => 'batal.png')
					);

		$data = array_merge($data, $this->_getDataStatistik());

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	function _getSortcutMenu($pintas=TRUE)
	{
		$base_admin = base_url().$this->config->item('admpath');
		if (bs_kode($this->session->userdata('level'), TRUE) == 'superman')
		{
			if($pintas)
			{
				$data = array(
								array('label' => 'Menu', 'url' => $base_admin.'/atur_menu', 'ikon' => 'menu.png'),
								array('label' => 'Halaman', 'url' => $base_admin.'/halaman', 'ikon' => 'halaman.png'),
								array('label' => 'Album', 'url' => $base_admin.'/album', 'ikon' => 'album1.png'),
								array('label' => 'Video', 'url' => $base_admin.'/video', 'ikon' => 'video.png'),
								array('label' => 'Blog', 'url' => $base_admin.'/blog', 'ikon' => 'blog.png'),
								array('label' => 'Produk', 'url' => $base_admin.'/produk', 'ikon' => 'produk.png'),
								array('label' => 'Order', 'url' => $base_admin.'/order', 'ikon' => 'order.png'),
								array('label' => 'Pelanggan', 'url' => $base_admin.'/pelanggan', 'ikon' => 'pelanggan.png'),
								array('label' => 'Newsletter', 'url' => $base_admin.'/newsletter', 'ikon' => 'newsletter.png'),
								array('label' => 'Banner', 'url' => $base_admin.'/banner', 'ikon' => 'banner.png'),
								array('label' => 'Widget', 'url' => $base_admin.'/atur_widget', 'ikon' => 'widget.png'),
								array('label' => 'Module', 'url' => $base_admin.'/atur_module', 'ikon' => 'modul.png'),
								array('label' => 'Pengaturan', 'url' => $base_admin.'/pengaturan', 'ikon' => 'pengaturan.png'),
								array('label' => 'Admin CMS', 'url' => $base_admin.'/admincms', 'ikon' => 'admin.png'),
								array('label' => 'Log', 'url' => $base_admin.'/log_user', 'ikon' => 'log.png'),
								array('label' => 'Bantuan', 'url' => '#', 'ikon' => 'bantuan.png')
							);
			}
			else {
				$data['dtstat'] = 'ya';
				$data['dtorder'] = 'ya';
			}
		}

		if (bs_kode($this->session->userdata('level'), TRUE) == 'batman')
		{
			if($pintas)
			{
				$data = array(
								array('label' => 'Halaman', 'url' => $base_admin.'/halaman', 'ikon' => 'halaman.png'),
								array('label' => 'Album', 'url' => $base_admin.'/album', 'ikon' => 'album1.png'),
								array('label' => 'Video', 'url' => $base_admin.'/video', 'ikon' => 'video.png'),
								array('label' => 'Blog', 'url' => $base_admin.'/blog', 'ikon' => 'blog.png'),
								array('label' => 'Produk', 'url' => $base_admin.'/produk', 'ikon' => 'produk.png'),
								array('label' => 'Order', 'url' => $base_admin.'/order', 'ikon' => 'order.png'),
								array('label' => 'Pelanggan', 'url' => $base_admin.'/pelanggan', 'ikon' => 'pelanggan.png'),
								array('label' => 'Newsletter', 'url' => $base_admin.'/newsletter', 'ikon' => 'newsletter.png'),
								array('label' => 'Banner', 'url' => $base_admin.'/banner', 'ikon' => 'banner.png'),
								array('label' => 'Admin CMS', 'url' => $base_admin.'/admincms', 'ikon' => 'admin.png'),
								array('label' => 'Bantuan', 'url' => '#', 'ikon' => 'bantuan.png')
							);
			}
			else {
				$data['dtstat'] = 'ya';
				$data['dtorder'] = 'ya';
			}
		}

		if (bs_kode($this->session->userdata('level'), TRUE) == 'ironman')
		{
			if($pintas)
			{
				$data = array(
								array('label' => 'Album', 'url' => $base_admin.'/album', 'ikon' => 'album1.png'),
								array('label' => 'Video', 'url' => $base_admin.'/video', 'ikon' => 'video.png'),
								array('label' => 'Blog', 'url' => $base_admin.'/blog', 'ikon' => 'blog.png'),
								array('label' => 'Bantuan', 'url' => '#', 'ikon' => 'bantuan.png')
							);
			}
			else {
				$data['dtstat'] = 'no';
				$data['dtorder'] = 'no';
			}
		}

		if (bs_kode($this->session->userdata('level'), TRUE) == 'spiderman')
		{
			if($pintas)
			{
				$data = array(
								array('label' => 'Produk', 'url' => $base_admin.'/produk', 'ikon' => 'produk.png'),
								array('label' => 'Order', 'url' => $base_admin.'/order', 'ikon' => 'order.png'),
								array('label' => 'Pelanggan', 'url' => $base_admin.'/pelanggan', 'ikon' => 'pelanggan.png'),
								array('label' => 'Newsletter', 'url' => $base_admin.'/newsletter', 'ikon' => 'newsletter.png'),
								array('label' => 'Bantuan', 'url' => '#', 'ikon' => 'bantuan.png')
							);
			}
			else {
				$data['dtstat'] = 'no';
				$data['dtorder'] = 'ya';
			}
		}

		return $data;
	}


	function _getDataStatistik()
	{
		// Lokasi
		foreach ($this->web->getLokasiStatistik() as $key => $row)
			$data['dtLokasi']['data'][$key] = (int)$row['hits'];
		foreach ($this->web->getLokasiStatistik(FALSE) as $key => $row)
			$data['dtLokasi']['label'][$key] = $row['lokasi'];
		
		// Browser
		foreach ($this->web->getBrowserStatistik() as $key => $row)
			$data['dtBrowser']['data'][$key] = (int)$row['hits'];
		foreach ($this->web->getBrowserStatistik(FALSE) as $key => $row)
			$data['dtBrowser']['label'][$key] = $row['browser'];
		
		// Halaman
		foreach ($this->web->getHalamanStatistik() as $key => $row)
			$data['dtHalaman']['data'][$key] = (int)$row['hits'];
		foreach ($this->web->getHalamanStatistik(FALSE) as $key => $row)
			$data['dtHalaman']['label'][$key] = humanize($row['tipe']);

		return $data;
	}

	function _getAssets()
	{
		$data['meta'] = array(
			array('name' => 'robots', 'content' => 'noindex nofollow'),
            array('name' => 'author', 'content' => $this->config->item('author_name')),
			array('name' => 'application-name', 'content' => $this->config->item('site_name')),
			array('name' => 'distribution', 'content' => $this->config->item('site_distribution')),
			array('name' => 'generator', 'content' => $this->config->item('generator_site'))
    	);
		$data['css_ext'] = array();
		$data['css'] = array('admin-min.css','themes/'.$this->jq_style.'/jquery-ui.css','elem/table-style.css');
		$data['css_ie'] = array('IEmain.css');
		$data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js',
							'plugins/ChartJS/awesomechart.js',
							'plugins/flot/jquery.flot.js','plugins/flot/jquery.flot.selection.js');
		$data['js_ie'] = array('PIE-1.0.0/PIE.js','plugins/flot/excanvas.min.js');
		$data['assets_widget'] =  array();
		$data['assets_module'] =  array();
		
		return $data;
	}
}

/* End of file adminweb.php */
/* Location: ./application/controllers/adminweb.php */

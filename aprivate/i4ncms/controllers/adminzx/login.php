<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->input->post(NULL, TRUE);
	}

	function index($sts='')
	{
		$data = $this->_getAdminAssets();
		$data['title'] = 'Login';

		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['partial_content'] = 'v_login';
		$data['sts_web'] = $this->config->item('sts_web');
		$data['sts'] = $sts;

		$this->load->view($this->config->item('admin_theme_id').'/main/v_login',$data);
	}

	function status()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('uid', 'ID', 'trim|required|min_length[5]|xss_clean');
        $this->form_validation->set_rules('knci', 'Kunci', 'trim|required|min_length[5]|xss_clean');

        if ($this->form_validation->run($this) == FALSE)
        {
			$sts = 'error';
			$this->index($sts);
        }
        else {
			// cek loginnya disini
			// set sesi dr model aja ya..?
			if( ! $this->adminlogin->cekAdmLogin() || $this->adminlogin->cekAdmLogin() === 'banned')
			{
				if($this->adminlogin->cekAdmLogin() === 'banned')
				$sts = 'banned';
				else
				$sts = 'ilegal';

				$this->index($sts);
			}
			else {
				redirect($this->config->item('admpath'),'refresh');
				#zheader("Location: https://".$_SERVER['HTTP_HOST']."/".$this->config->item('admpath'));
			}
        }
	}

	function keluar()
	{
		$admin = array('level'=>'','nama_admin'=>'','kunci_adm'=>'','idmu'=>'','idses'=>'','admkunci'=>'',
						'admemail'=>'','ademinaja'=>'','admusrid'=>'',
						'superman'=>'','batman'=>'','ironman'=>'','admperpage'=>'');
		$this->session->unset_userdata($admin);
		//clean cache db
		if(config_item('cache_front') === TRUE)
		$this->db->cache_delete_all();
		$this->index('');
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fituradmin extends AdminController {

    public function __construct()
    {
        parent::__construct();

        $this->_cekStsLoginAdm();
        $this->hak = array();
        $this->load->library('pagination');
    }

    public function index() { }

/* Menu */
    public function carimenu($sts='')
    {
		$this->_cek('atur_menu');
		$this->hak = array('superman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Menu';

		$config['base_url'] = base_url().$this->config->item('admpath').'/carimenu/'.$cari;
		$config['total_rows'] = $this->admfitur->getCountCariMenu($cari);
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['menu'] = $this->admfitur->getCariMenu($cari,$config['per_page'],$offset);
		$data['nama_menu'] = $this->cms->getTitleMenu();
		$data['halaman'] = $this->cms->getSelectHalaman();
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = '';
		$data['txtcari'] = $cari;
		$data['partial_content'] = 'v_atur_menu';

		if(empty($sts))
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		else
		$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_menu',$data);
    }

    public function tblcarimenu()
    {
		$this->carimenu('update_tabel');
    }

/* Halaman */
    public function carihalaman($sts='')
    {
		$this->_cek('halaman');
		$this->hak = array('superman','batman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);

        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Halaman';

        $config['base_url'] = base_url().$this->config->item('admpath').'/carihalaman/'.$cari;
        $config['total_rows'] = $this->admfitur->getCountCariHalaman($cari);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['halaman_web'] = $this->admfitur->getCariHalaman($cari,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
        $data['hak'] = $this->hak;
        $data['sts'] = '';
        $data['txtcari'] = $cari;
        $data['partial_content'] = 'v_halaman';

		if(empty($sts))
        $this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
        else
        $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_halaman',$data);
    }

    public function tblcarihalaman()
    {
        $this->carihalaman('update_tabel');
    }
/* End Halaman */


/* Album */
	public function carialbum($sts='')
    {
		$this->_cek('album');
		$this->hak = array('superman','batman','ironman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);

        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Album';
        
        $config['base_url'] = base_url().$this->config->item('admpath').'/carialbum/'.$cari;
        $config['total_rows'] = $this->admfitur->getCountCariAlbum($cari);
        $config['per_page'] = $this->adm_per_halaman; 
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->admfitur->getCariAlbum($cari,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data['txtcari'] = $cari;
        $data['partial_content'] = 'v_album';

        $data['jml_gallery'] = $this->cms->getCountGallery();
        if($data['jml_gallery'] > 0)
        $data['parent_album'] = $this->cms->getAlbumSelect(TRUE);
        else
        $data['parent_album'] = $this->cms->getAlbumSelect();

		if(empty($sts))
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
        else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
	        $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_album',$data);
	        else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
        
    }

    public function tblcarialbum()
    {
        $this->carialbum('update_tabel');
    }

//Gallery=======================|
    public function carigallery($sts='')
    {
		$this->hak = array('superman','batman','ironman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		$id = $this->uri->segment(4,$this->input->post('parent_album'));
		$this->_cek('album/tabel_gallery/'.$id);

        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/carigallery/'.$cari.'/'.$id));
        $config['total_rows'] = $this->admfitur->getCountCariGallery($cari,$id);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 5;
        $config = array_merge($config,$this->_adminPagination());

        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->admfitur->getCariGallery($cari,$id,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
        $data['sts'] = '';
        $data['title'] = 'Album Gallery';
        $data['id_album'] = $id;
        $data['nama_album'] = $this->cms->getNamaGalleryAlbumById($id);
        $data = array_merge($data, $this->_getAdminAssets());

		if(empty($sts))
		{
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_gallery',$data);
			else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
        else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
	        $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_gallery',$data);
	        else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
    }

    public function tblcarigallery()
    {
        $this->carigallery('update_tabel');
    }
/* End Album */


/* Video */
	public function carivideo($sts='')
    {
		$this->_cek('video');
		$this->hak = array('superman','batman','ironman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);

        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Video';

        $config['base_url'] = base_url().$this->config->item('admpath').'/carivideo/'.$cari;
        $config['total_rows'] = $this->admfitur->getCountCariVideo($cari);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['video_web'] = $this->admfitur->getCariVideo($cari,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
        $data['sts'] = '';
        $data['partial_content'] = 'v_video';
        $data['jml_playlist'] = $this->cms->getCountPlaylist();

        if($data['jml_playlist'] > 0)
        $data['parent_video'] = $this->cms->getVideoSelect(TRUE);
        else
        $data['parent_video'] = $this->cms->getVideoSelect();

		if(empty($sts))
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
        else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
	        $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_video',$data);
	        else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
    }

    public function tblcarivideo()
    {
        $this->carivideo('update_tabel');
    }

//Playlist=======================|
    public function cariplaylist($sts='')
    {
		$this->hak = array('superman','batman','ironman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		$this->load->library('pagination');
        $id = $this->uri->segment(4,$this->input->post('parent_video'));
        $this->_cek('video/tabel_playlist/'.$id);
        
        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/cariplaylist/'.$cari.'/'.$id));
        $config['total_rows'] = $this->admfitur->getCountCariPlaylist($cari,$id);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 5;
        $config = array_merge($config,$this->_adminPagination());
        
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['video_web'] = $this->admfitur->getCariPlaylist($cari,$id,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
        $data['sts'] = '';
        $data['title'] = 'Video Playlist';
        $data['id_video'] = $id;
        $data['nama_video'] = $this->cms->getNamaVideoPlaylistById($id);
        $data = array_merge($data, $this->_getAdminAssets());

		if(empty($sts))
		{
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
	        $this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_playlist',$data);
	        else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
        else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_playlist',$data);
			else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
    }

    public function tblcariplaylist()
    {
        $this->cariplaylist('update_tabel');
    }
/* End Video */


/* Blog */
	public function cariblog($sts='')
    {
		$this->_cek('blog');
		$this->hak = array('superman','batman','ironman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
        $cari = $this->_txtCari($cari);
        
        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Blog';
        
        $config['base_url'] = base_url().$this->config->item('admpath').'/cariblog/'.$cari;
        $config['total_rows'] = $this->admfitur->getCountCariBlog($cari);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['blog_web'] = $this->admfitur->getCariBlog($cari,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
        $data['sts'] = '';
        $data['partial_content'] = 'v_blog';

		if(empty($sts))
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_blog',$data);
			else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
    }

    public function tblcariblog()
    {
        $this->cariblog('update_tabel');
    }
/* End Blog */


/* Produk */
	public function cariproduk($sts='')
    {
		$this->_cek('produk');
		$this->hak = array('superman','batman','spiderman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));

		$cari = $this->_txtCari($cari);
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Produk';
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/cariproduk/'.$cari;
		$config['total_rows'] = $this->admfitur->getCountCariProduk($cari);
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
		
		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['produk'] = $this->admfitur->getCariProdukInduk($cari,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = '';
		$data['partial_content'] = 'v_produk';

		if(empty($sts))
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_produk',$data);
			else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
    }

    public function tblcariproduk()
    {
		$this->cariproduk('update_tabel');
    }

	public function cariprod_anak($sts='')
    {
		$this->hak = array('superman','batman','spiderman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		$this->load->library('pagination');

		$id_induk = $this->uri->segment(4,$this->input->post('parent_id_prod', TRUE));
		$this->_cek('produk/detail_anak/'.$id_induk);
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Produk Anak';
		$config['base_url'] = base_url().$this->config->item('admpath').'/cariprod_anak/'.$cari.'/'.$id_induk;
		$config['total_rows'] = $this->admfitur->getCountCariProduk($cari,$id_induk);
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 5;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		}
		else{
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['produk'] = $this->admfitur->getCariProdukAnak($cari,$id_induk,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['id_induk'] = $id_induk;
		$data['txtcari'] = $cari;
		$data['sts'] = '';
		
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			if(empty($sts))
			{
				$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_produk_anak',$data);
			}
			else {
				$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_produk_anak',$data);
			}
		}
		else {
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
    }

    public function tblcariprod_anak()
    {
		$this->cariprod_anak('update_tabel');
    }
/* End Produk */


/* Orderan */
	public function cariorder($sts='')
	{
		$this->_cek('order');
		$this->hak = array('superman','batman','spiderman');
		if( empty($sts) )
		$cari = underscore($this->input->post('caritxt', TRUE));
		else
		$cari = underscore($this->uri->segment(4,$this->input->post('caritxt', TRUE)));

		$cari = $this->_txtCari($cari);
		$pp = $this->adm_per_halaman;
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Orderan';
		$config = $this->_adminPagination();

		/*proses transaksi bayar */
		$config_proses_bayar['base_url'] = base_url().$this->config->item('admpath').'/cariorder/proses_bayar/'.$cari;
		$config_proses_bayar['total_rows'] = $this->admfitur->getCountCariOrderProses($cari);
		$config_proses_bayar['per_page'] = $pp; 
		$config_proses_bayar['uri_segment'] = ($this->uri->segment(3) === 'proses_bayar')?5:6;
		$config_proses_bayar = array_merge($config_proses_bayar, $config);

		/*proses transaksi paket*/
		$config_proses_paket['base_url'] = base_url().$this->config->item('admpath').'/cariorder/proses_pemaketan/'.$cari;
		$config_proses_paket['total_rows'] = $this->admfitur->getCountCariOrderProses($cari,$proses='pemaketan');
		$config_proses_paket['per_page'] = $pp; 
		$config_proses_paket['uri_segment'] = ($this->uri->segment(3) === 'proses_pemaketan')?5:6;
		$config_proses_paket = array_merge($config_proses_paket, $config);

		/*proses transaksi kirim*/
		$config_proses_kirim['base_url'] = base_url().$this->config->item('admpath').'/cariorder/proses_pengiriman/'.$cari;
		$config_proses_kirim['total_rows'] = $this->admfitur->getCountCariOrderProses($cari,$proses='pengiriman');
		$config_proses_kirim['per_page'] = $pp; 
		$config_proses_kirim['uri_segment'] = ($this->uri->segment(3) === 'proses_pengiriman')?5:6;
		$config_proses_kirim = array_merge($config_proses_kirim, $config);

		/*proses transaksi pending*/
		$config_proses_pending['base_url'] = base_url().$this->config->item('admpath').'/cariorder/proses_pending/'.$cari;
		$config_proses_pending['total_rows'] = $this->admfitur->getCountCariOrderProses($cari,$proses='pending');
		$config_proses_pending['per_page'] = $pp; 
		$config_proses_pending['uri_segment'] = ($this->uri->segment(3) === 'proses_pending')?5:6;
		$config_proses_pending = array_merge($config_proses_pending, $config);

		/*proses transaksi batal*/
		$config_proses_batal['base_url'] = base_url().$this->config->item('admpath').'/cariorder/proses_batal/'.$cari;
		$config_proses_batal['total_rows'] = $this->admfitur->getCountCariOrderProses($cari,$proses='batal');
		$config_proses_batal['per_page'] = $pp; 
		$config_proses_batal['uri_segment'] = ($this->uri->segment(3) === 'proses_batal')?5:6;
		$config_proses_batal = array_merge($config_proses_batal, $config);

		/*status order sukses*/
		$config_sukses['base_url'] = base_url().$this->config->item('admpath').'/cariorder/sukses/'.$cari;
		$config_sukses['total_rows'] = $this->admfitur->getCountCariOrderSukses($cari);
		$config_sukses['per_page'] = $pp; 
		$config_sukses['uri_segment'] = ($this->uri->segment(3) === 'sukses')?5:6;
		$config_sukses = array_merge($config_sukses, $config);

		/*status order batal*/
		$config_batal['base_url'] = base_url().$this->config->item('admpath').'/cariorder/batal/'.$cari;
		$config_batal['total_rows'] = $this->admfitur->getCountCariOrderBatal($cari);
		$config_batal['per_page'] = $pp; 
		$config_batal['uri_segment'] = ($this->uri->segment(3) === 'batal')?5:6;
		$config_batal = array_merge($config_batal, $config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
		{
			/*proses transaksi*/
			$offset_proses_bayar = $this->getOffsetPage($this->uri->segment($config_proses_bayar['uri_segment']), $config_proses_bayar['per_page']);
			$offset_proses_paket = $this->getOffsetPage($this->uri->segment($config_proses_paket['uri_segment']), $config_proses_paket['per_page']);
			$offset_proses_kirim = $this->getOffsetPage($this->uri->segment($config_proses_kirim['uri_segment']), $config_proses_kirim['per_page']);
			$offset_proses_pending = $this->getOffsetPage($this->uri->segment($config_proses_pending['uri_segment']), $config_proses_pending['per_page']);
			$offset_proses_batal = $this->getOffsetPage($this->uri->segment($config_proses_batal['uri_segment']), $config_proses_batal['per_page']);
			
			/*status order*/
			$offset_sukses = $this->getOffsetPage($this->uri->segment($config_sukses['uri_segment']), $config_sukses['per_page']);
			$offset_batal = $this->getOffsetPage($this->uri->segment($config_batal['uri_segment']), $config_batal['per_page']);
		}
		else {
			/*proses transaksi*/
			$offset_proses_bayar = $this->uri->segment($config_proses_bayar['uri_segment'],0);
			$offset_proses_paket = $this->uri->segment($config_proses_paket['uri_segment'],0);
			$offset_proses_kirim = $this->uri->segment($config_proses_kirim['uri_segment'],0);
			$offset_proses_pending = $this->uri->segment($config_proses_pending['uri_segment'],0);
			$offset_proses_batal = $this->uri->segment($config_proses_batal['uri_segment'],0);
			/*status order*/
			$offset_sukses = $this->uri->segment($config_sukses['uri_segment'],0);
			$offset_batal = $this->uri->segment($config_batal['uri_segment'],0);
			
		}

		/*proses transaksi*/
		$data['proses_bayar'] = $this->admfitur->getCariOrderanProses($cari,$config_proses_bayar['per_page'], $offset_proses_bayar);
		$data['proses_paket'] = $this->admfitur->getCariOrderanProses($cari,$config_proses_paket['per_page'], $offset_proses_paket,'pemaketan');
		$data['proses_kirim'] = $this->admfitur->getCariOrderanProses($cari,$config_proses_kirim['per_page'], $offset_proses_kirim,'pengiriman');
		$data['proses_pending'] = $this->admfitur->getCariOrderanProses($cari,$config_proses_pending['per_page'], $offset_proses_pending,'pending');
		$data['proses_batal'] = $this->admfitur->getCariOrderanProses($cari,$config_proses_batal['per_page'],$offset_proses_batal,'batal');

		/*status order*/
		$data['order_sukses'] = $this->admfitur->getCariOrderanSukses($cari,$config_sukses['per_page'],$offset_sukses);
		$data['order_batal'] = $this->admfitur->getCariOrderanBatal($cari,$config_batal['per_page'],$offset_batal);

		/*proses transaksi*/
		$this->pagination->initialize($config_proses_bayar);
		$data['page_proses_bayar'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_paket);
		$data['page_proses_paket'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_kirim);
		$data['page_proses_kirim'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_pending);
		$data['page_proses_pending'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_batal);
		$data['page_proses_batal'] =  $this->pagination->create_links();

		/*status order*/
		$this->pagination->initialize($config_sukses);
		$data['page_sukses'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_batal);
		$data['page_batal'] =  $this->pagination->create_links();

		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['nosidebar'] = TRUE;
		$data['partial_content'] = 'v_order';

		/* Get Views */
		if( empty($sts) )
		{
			if($this->input->post('caritxt', TRUE))
			$this->session->unset_userdata('aktif_tab');
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			return $data;
		}
	}

	public function cari_proses_pending()
	{
		$data = $this->cariorder($sts='proses_pending');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_proses_bayar()
	{
		$data = $this->cariorder($sts='proses_bayar');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_proses_paket()
	{
		$data = $this->cariorder($sts='proses_paket');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_proses_kirim()
	{
		$data = $this->cariorder($sts='proses_kirim');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_proses_batal()
	{
		$data = $this->cariorder($sts='proses_batal');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_sukses()
	{
		$data = $this->cariorder($sts='sukses');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_batal()
	{
		$data = $this->cariorder($sts='batal');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function tblcariorder()
	{
		$data = $this->cariorder($sts='ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_order',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

/* End Orderan */


/* Pelanggan */
	public function caripelanggan($sts='')
	{
		$this->_cek('pelanggan');
		$this->hak = array('superman','batman','spiderman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Produk';

		$config['base_url'] = base_url().$this->config->item('admpath').'/caripelanggan/'.$cari;
		$config['total_rows'] = $this->admfitur->getCountCariPelanggan($cari);
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['pelanggan'] = $this->admfitur->getCariPelanggan($cari,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['partial_content'] = 'v_pelanggan';

		if(empty($sts))
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_pelanggan',$data);
			else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
	}

	public function tblcaripelanggan()
	{
		$this->caripelanggan('update_tabel');
	}
/* End Pelanggan */


/* Iklan */
	public function caribanner($sts='')
	{
		$this->_cek('banner');
		$pp = $this->adm_per_halaman;
		$this->hak = array('superman','batman');
		if( empty($sts) )
		$cari = underscore($this->input->post('caritxt', TRUE));
		else
		$cari = underscore($this->uri->segment(4,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Banner Iklan';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = '';
		$config = $this->_adminPagination();
		
		$iklan_sidebar['base_url'] = base_url().$this->config->item('admpath').'/caribanner/iklan_sidebar/'.$cari;
		$iklan_sidebar['total_rows'] = $this->admfitur->getCountCariIklan($cari,'sidebar');
		$iklan_sidebar['per_page'] = $pp;
		$iklan_sidebar['uri_segment'] = ($this->uri->segment(3) === 'iklan_sidebar')?5:6;
		$iklan_sidebar = array_merge($iklan_sidebar, $config);

		$iklan_utama['base_url'] = base_url().$this->config->item('admpath').'/caribanner/iklan_utama/'.$cari;
		$iklan_utama['total_rows'] = $this->admfitur->getCountCariIklan($cari,'atas');
		$iklan_utama['per_page'] = $pp;
		$iklan_utama['uri_segment'] = ($this->uri->segment(3) === 'iklan_utama')?5:6;
		$iklan_utama = array_merge($iklan_utama, $config);

		$iklan_atas['base_url'] = base_url().$this->config->item('admpath').'/caribanner/iklan_atas/'.$cari;
		$iklan_atas['total_rows'] = $this->admfitur->getCountCariIklan($cari,'konten_atas');
		$iklan_atas['per_page'] = $pp;
		$iklan_atas['uri_segment'] = ($this->uri->segment(3) === 'iklan_atas')?5:6;
        $iklan_atas = array_merge($iklan_atas, $config);

        $iklan_bawah['base_url'] = base_url().$this->config->item('admpath').'/caribanner/iklan_bawah/'.$cari;
		$iklan_bawah['total_rows'] = $this->admfitur->getCountCariIklan($cari,'konten_bawah');
		$iklan_bawah['per_page'] = $pp;
		$iklan_bawah['uri_segment'] = ($this->uri->segment(3) === 'iklan_bawah')?5:6;
        $iklan_bawah = array_merge($iklan_bawah, $config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset_sidebar = $this->getOffsetPage($this->uri->segment($iklan_sidebar['uri_segment']), $iklan_sidebar['per_page']);
            $offset_utama = $this->getOffsetPage($this->uri->segment($iklan_utama['uri_segment']), $iklan_utama['per_page']);
            $offset_atas = $this->getOffsetPage($this->uri->segment($iklan_atas['uri_segment']), $iklan_atas['per_page']);
            $offset_bawah = $this->getOffsetPage($this->uri->segment($iklan_bawah['uri_segment']), $iklan_bawah['per_page']);
        }
        else {
            $offset_sidebar = $this->uri->segment($iklan_sidebar['uri_segment'],0);
            $offset_utama = $this->uri->segment($iklan_utama['uri_segment'],0);
            $offset_atas = $this->uri->segment($iklan_atas['uri_segment'],0);
            $offset_bawah = $this->uri->segment($iklan_bawah['uri_segment'],0);
        }
        
		$data['iklan_sidebar'] = $this->admfitur->getCariIklan($cari,'sidebar',$iklan_sidebar['per_page'],$offset_sidebar);
		$data['iklan_utama'] = $this->admfitur->getCariIklan($cari,'atas',$iklan_utama['per_page'],$offset_utama);
		$data['iklan_atas'] = $this->admfitur->getCariIklan($cari,'konten_atas',$iklan_atas['per_page'],$offset_atas);
		$data['iklan_bawah'] = $this->admfitur->getCariIklan($cari,'konten_bawah',$iklan_bawah['per_page'],$offset_bawah);
		
		$this->pagination->initialize($iklan_sidebar);
		$data['page_sidebar'] =  $this->pagination->create_links();

		$this->pagination->initialize($iklan_utama);
		$data['page_utama'] =  $this->pagination->create_links();

		$this->pagination->initialize($iklan_atas);
		$data['page_atas'] =  $this->pagination->create_links();

		$this->pagination->initialize($iklan_bawah);
		$data['page_bawah'] =  $this->pagination->create_links();

		$data['partial_content'] = 'v_banner';
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			return $data;
		}
	}

	public function cari_iklan_sidebar()
	{
		$data = $this->caribanner('iklan_sidebar');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_iklan_utama()
	{
		$data = $this->caribanner('iklan_utama');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_iklan_atas()
	{
		$data = $this->caribanner('iklan_atas');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_iklan_bawah()
	{
		$data = $this->caribanner('iklan_bawah');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function tblcaribanner()
	{
		$data = $this->caribanner('ajax');
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_iklan',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}
/* End Iklan */


/* Widget */
	public function cariwidget($sts='')
	{
		$this->_cek('atur_widget');
		$this->hak = array('superman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		
		$data = $this->_getAdminAssets(FALSE,TRUE);
		$data['title'] = 'Admin Widget';
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/cariwidget/'.$cari;
		$config['total_rows'] = $this->admfitur->getCountCariWidget($cari);
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['widget'] = $this->admfitur->getCariWidget($cari,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = '';
		$data['partial_content'] = 'v_atur_widget';

		if(empty($sts))
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_widget',$data);
			else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
	}

	public function tblcariwidget()
	{
		$this->cariwidget('update_tabel');
	}
/* End Widget */

/* Module */
	public function carimodule($sts='')
	{
		$this->_cek('atur_module');
		$this->hak = array('superman');
		$cari = underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		
		$data = $this->_getAdminAssets(TRUE);
		$data['title'] = 'Admin Module';
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/carimodule/'.$cari;
		$config['total_rows'] = $this->admfitur->getCountCariModule($cari);
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['modul'] = $this->admfitur->getCariModule($cari,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = '';
		$data['partial_content'] = 'v_atur_module';

		if(empty($sts))
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_module',$data);
			else
	        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
	}

	public function tblcarimodule()
	{
		$this->carimodule('update_tabel');
	}
/* End Module */


/* AdminCMS */
	public function cariadmincms($sts='')
	{
		$this->_cek('admincms');
		$this->hak = array('superman','batman');
		$cari = $this->_txtCari(underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE))));

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin CMS';

		$config['base_url'] = base_url().$this->config->item('admpath').'/cariadmincms/'.$cari;
		$config['total_rows'] = $this->adminlogin->getCountCariAdmin($cari);
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['admin'] = $this->adminlogin->getCariAdmin($cari,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['partial_content'] = 'v_admincms';

		if(empty($sts))
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_admin',$data);
			else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
	}

	public function tblcariadmincms()
	{
		$this->cariadmincms('update_tabel');
	}
/* End AdminCMS */


/* LogUser */
	public function carilog_user($sts='')
	{
		$this->_cek('log_user');
		$this->hak = array('superman');
		if( empty($sts) )
		$cari = underscore($this->input->post('caritxt', TRUE));
		else
		$cari = underscore($this->uri->segment(4,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);
		
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Log';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = '';
		$config = $this->_adminPagination();
		
		$config_err['base_url'] = base_url().$this->config->item('admpath').'/carilog_user/log_err/'.$cari;
		$config_err['total_rows'] = $this->admfitur->getCountCariLog($cari);
		$config_err['per_page'] = $this->adm_per_halaman;
		$config_err['uri_segment'] = ($this->uri->segment(3) === 'log_err')?5:6;
		$config_err = array_merge($config_err, $config);

		$config_usr['base_url'] = base_url().$this->config->item('admpath').'/carilog_user/log_usr/'.$cari;
		$config_usr['total_rows'] = $this->admfitur->getCountCariLogUser($cari);
		$config_usr['per_page'] = $this->adm_per_halaman;
		$config_usr['uri_segment'] = ($this->uri->segment(3) === 'log_usr')?5:6;
        $config_usr = array_merge($config_usr, $config);

		$config_robot['base_url'] = base_url().$this->config->item('admpath').'/carilog_user/log_bot/'.$cari;
		$config_robot['total_rows'] = $this->admfitur->getCountCariLogRobot($cari);
		$config_robot['per_page'] = $this->adm_per_halaman;
		$config_robot['uri_segment'] = ($this->uri->segment(3) === 'log_bot')?5:6;
        $config_robot = array_merge($config_robot, $config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset_err = $this->getOffsetPage($this->uri->segment($config_err['uri_segment']), $config_err['per_page']);
            $offset_usr = $this->getOffsetPage($this->uri->segment($config_usr['uri_segment']), $config_usr['per_page']);
            $offset_robot = $this->getOffsetPage($this->uri->segment($config_robot['uri_segment']), $config_robot['per_page']);
        }
        else {
            $offset_err = $this->uri->segment($config_err['uri_segment'],0);
            $offset_usr = $this->uri->segment($config_usr['uri_segment'],0);
            $offset_robot = $this->uri->segment($config_robot['uri_segment'],0);
        }
        
		$data['logging'] = $this->admfitur->lihatCariLog($cari,$config_err['per_page'],$offset_err);
		$data['log_user'] = $this->admfitur->lihatCariLogUser($cari,$config_usr['per_page'],$offset_usr);
		$data['log_robot'] = $this->admfitur->lihatCariLogRobot($cari,$config_robot['per_page'],$offset_robot);
		
		$this->pagination->initialize($config_err);
		$data['page'] =  $this->pagination->create_links();

		$this->pagination->initialize($config_usr);
		$data['user'] =  $this->pagination->create_links();

		$this->pagination->initialize($config_robot);
		$data['robot'] =  $this->pagination->create_links();

		$data['partial_content'] = 'v_log_user';
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			return $data;
		}
	}

	public function tblcarilog_user()
	{
		$data = $this->carilog_user($sts='ajax');
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_log_user',$data);
		else
		$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function cari_log_err()
	{
		$data = $this->carilog_user($sts='log_err');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_log_usr()
	{
		$data = $this->carilog_user($sts='log_usr');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cari_log_bot()
	{
		$data = $this->carilog_user($sts='log_bot');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}
/* End LogUser */

/* Metode Bayar */
	public function cari_metodebayar($sts='')
	{
		$this->_cek('atur_metode_bayar');
		$this->hak = array('superman');
		if( empty($sts) )
		$cari = underscore($this->input->post('caritxt', TRUE));
		else
		$cari = underscore($this->uri->segment(4,$this->input->post('caritxt', TRUE)));
		$cari = $this->_txtCari($cari);

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Metode Pembayaran';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = $sts;
		$config = $this->_adminPagination();
		
		$bayar_transfer['base_url'] = base_url().$this->config->item('admpath').'/cari_metodebayar/bayar_transfer/'.$cari;
		$bayar_transfer['total_rows'] = $this->admfitur->getCountCariMetodeBayarTransfer($cari);
		$bayar_transfer['per_page'] = $this->adm_per_halaman;
		$bayar_transfer['uri_segment'] = ($this->uri->segment(3) === 'bayar_transfer')?5:6;
		$bayar_transfer = array_merge($bayar_transfer, $config);

		$bayar_online['base_url'] = base_url().$this->config->item('admpath').'/cari_metodebayar/bayar_online/'.$cari;
		$bayar_online['total_rows'] = $this->admfitur->getCountCariMetodeBayarOnline($cari);
		$bayar_online['per_page'] = $this->adm_per_halaman;
		$bayar_online['uri_segment'] = ($this->uri->segment(3) === 'bayar_online')?5:6;
		$bayar_online = array_merge($bayar_online, $config);

		$bayar_giro['base_url'] = base_url().$this->config->item('admpath').'/cari_metodebayar/bayar_giro/'.$cari;
		$bayar_giro['total_rows'] = $this->admfitur->getCountCariMetodeBayarGiro($cari);
		$bayar_giro['per_page'] = $this->adm_per_halaman;
		$bayar_giro['uri_segment'] = ($this->uri->segment(3) === 'bayar_giro')?5:6;
        $bayar_giro = array_merge($bayar_giro, $config);

        $bayar_cod['base_url'] = base_url().$this->config->item('admpath').'/cari_metodebayar/bayar_cod/'.$cari;
		$bayar_cod['total_rows'] = $this->admfitur->getCountCariMetodeBayarCod($cari);
		$bayar_cod['per_page'] = $this->adm_per_halaman;
		$bayar_cod['uri_segment'] = ($this->uri->segment(3) === 'bayar_cod')?5:6;
        $bayar_cod = array_merge($bayar_cod, $config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset_transfer = $this->getOffsetPage($this->uri->segment($bayar_transfer['uri_segment']), $bayar_transfer['per_page']);
            $offset_online = $this->getOffsetPage($this->uri->segment($bayar_online['uri_segment']), $bayar_online['per_page']);
            $offset_giro = $this->getOffsetPage($this->uri->segment($bayar_giro['uri_segment']), $bayar_giro['per_page']);
            $offset_cod = $this->getOffsetPage($this->uri->segment($bayar_cod['uri_segment']), $bayar_cod['per_page']);
        }
        else {
            $offset_transfer = $this->uri->segment($bayar_transfer['uri_segment'],0);
            $offset_online = $this->uri->segment($bayar_online['uri_segment'],0);
            $offset_giro = $this->uri->segment($bayar_giro['uri_segment'],0);
            $offset_cod = $this->uri->segment($bayar_cod['uri_segment'],0);
        }
        
		$data['bayar_transfer'] = $this->admfitur->lihatCariMetodeBayarTransfer($cari,$bayar_transfer['per_page'],$offset_transfer);
		$data['bayar_online'] = $this->admfitur->lihatCariMetodeBayarOnline($cari,$bayar_online['per_page'],$offset_online);
		$data['bayar_giro'] = $this->admfitur->lihatCariMetodeBayarGiro($cari,$bayar_giro['per_page'],$offset_giro);
		$data['bayar_cod'] = $this->admfitur->lihatCariMetodeBayarCod($cari,$bayar_cod['per_page'],$offset_cod);

		$this->pagination->initialize($bayar_transfer);
		$data['page_transfer'] =  $this->pagination->create_links();

		$this->pagination->initialize($bayar_online);
		$data['page_online'] =  $this->pagination->create_links();

		$this->pagination->initialize($bayar_giro);
		$data['page_giro'] =  $this->pagination->create_links();

		$this->pagination->initialize($bayar_cod);
		$data['page_cod'] =  $this->pagination->create_links();
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->session->unset_userdata('aktif_tab_metode_bayar');
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
		}
		else {
			return $data;
		}
	}

	public function tblcari_metodebayar()
	{
		$data = $this->cari_metodebayar('ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_metode_bayar',$data);
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function cari_bayar_transfer()
	{
		$data = $this->cari_metodebayar('bayar_transfer');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function cari_bayar_online()
	{
		$data = $this->cari_metodebayar('bayar_online');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function cari_bayar_giro()
	{
		$data = $this->cari_metodebayar('bayar_giro');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function cari_bayar_cod()
	{
		$data = $this->cari_metodebayar('bayar_cod');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}
/* End Metode Bayar */

/* Kurir */
	public function carikurir($sts='')
	{
		$this->_cek('atur_kurir_pengiriman');
		$this->hak = array('superman');
		$cari = $this->_txtCari(underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE))));

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Kurir Pengiriman';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = $sts;

		$config['base_url'] = base_url().$this->config->item('admpath').'/carikurir/'.$cari;
		$config['total_rows'] = $this->admfitur->getCountCariKurir($cari);
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
        }
        else{
            $offset = $this->uri->segment($bayar_transfer['uri_segment'],0);
        }
        
		$data['kurir'] = $this->admfitur->getCariKurir($cari,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_kurir_pengiriman',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
				$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_kurir_pengiriman',$data);
			else {
				$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
				$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	        }
		}
	}

	public function tblcarikurir()
	{
		$this->carikurir('ajax');
	}
/* End Kurir */

/* Kategori */
	public function carikategori($sts='')
	{
		$this->_cek('atur_kategori');
		$this->hak = array('superman');
		$cari = $this->_txtCari(underscore($this->uri->segment(3,$this->input->post('caritxt', TRUE))));

		$data = $this->_getAdminAssets();
		$data['title'] = 'Kategori Konten';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;

        $config['base_url'] = base_url().$this->config->item('admpath').'/carikategori/'.$cari;
        $config['total_rows'] = $this->admfitur->getCountCariKategori($cari);
        $config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['kategori'] = $this->admfitur->getCariKategori($cari,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['hak'] = $this->hak;
		$data['txtcari'] = $cari;
		$data['sts'] = $sts;

		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_kategori',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			{
				$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_kategori',$data);
			}
			else {
				$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
				$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	        }
	    }
	}

	public function tblcarikategori()
	{
		$this->carikategori('update_tabel');
	}
/* End Kategori */

/* Helper */
	function _txtCari($txt='')
	{
		if (is_numeric($txt))
		return '';
		else
		return $txt;
	}

	// Redirect jika nilai numeric
    function _cek($main=''){
        if (is_numeric($this->input->post('caritxt', TRUE))) {
			$this->session->set_flashdata('error', 'Kata pencarian tidak boleh hanya memuat angka.');
			redirect(base_url().$this->config->item('admpath').'/'.$main.config_item('url_suffix'),'refresh');
		}
    }
/* End Helper */


}

/* End of file fituradmin.php */
/* Location: ./cms/controllers/admin/fituradmin.php */

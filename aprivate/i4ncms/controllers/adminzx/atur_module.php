<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Atur_module extends AdminController {
	
	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');
	}
	
	public function index($sts='')
	{
		$this->load->library('pagination');
		
		$data = $this->_getAdminAssets(TRUE);
		$data['title'] = 'Admin Module';
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/atur_module';
		$config['total_rows'] = $this->cms->getCountModule();
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['modul'] = $this->cms->getAllModule($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$data['partial_content'] = 'v_atur_module';
		
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$this->load->library('pagination');
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/atur_module/update_tabel';
		$config['total_rows'] = $this->cms->getCountMenu();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['modul'] = $this->cms->getAllModule($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = '';

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_module',$data);
		}
		else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function add_module()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$this->form_validation->set_rules('module_name', 'Nama Module', 'trim_judul|required|alpha_dash|callback__module_cek');
		$this->form_validation->set_rules('module_css', 'CSS Module', 'trim');
		$this->form_validation->set_rules('module_js', 'JS Module', 'trim');
		$this->form_validation->set_rules('module_html', 'HTML Module', 'trim');
		$this->form_validation->set_rules('module_status', 'Status Module', 'trim|required|less_than[2]');
		$this->form_validation->set_rules('module_path', 'Path Module', 'trim|required');
		$this->form_validation->set_rules('module_desc', 'Deskripsi Module', 'trim');
		
		if ( $this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$sts = 'cek';
			$this->index($sts);
		}
		else {
			
			if ( ! $this->cms->addModule() )
			{
				$sts = 'error';
				$this->index($sts);
			}
			else {
				$this->index($sts='');
			}
		}
	}

	function update_module()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('module_id'))
		{
			$this->form_validation->set_rules('module_id', 'ID Modul', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|integer|less_than[2]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Modul', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('modid'))
		{
			$this->form_validation->set_rules('modid', 'ID Modul', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ($this->input->post('module_id') && $this->_isSupAdmin()) 
			{
				if( ! $this->cms->upModuleStat() || ! $this->_isSupAdmin())
				{
					echo 'Gagal Ubah Module..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ($this->input->post('id_hps') && $this->_isSupAdmin()) {
				if( ! $this->cms->hapusModule() || ! $this->_isSupAdmin())
				{
					echo 'Gagal Hapus Module..!!';
				}
				else{
					echo 'sukses';
				}
			}
			elseif ($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('modid') && $this->_isSupAdmin()) {
				if( ! $this->cms->upModPos() || ! $this->_isSupAdmin())
				{
					echo 'Gagal Ubah Posisi Module..!!';
				}
				else{
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
	}

	function install_module()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo form_open_multipart($this->config->item('admpath').'/atur_module/upload_module');
			echo '<input type="file" name="userfile" />';
			echo '<p>Aktifkan Module : <input type="checkbox" name="aktif" value="1" /><br /><small>Ceklist ini untuk aktifkan module otomatis.</small></p>';
			//if ( $this->super_admin === TRUE )
			echo '<p>Fresh Install : <input type="checkbox" name="fresh" value="1" /><br /><small>Ceklist ini untuk hapus data yang lama.</small></p>';
			echo form_submit('mysubmit', 'Submit');
			echo form_close();
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}
	
	function upload_module()
	{
		if ( $this->do_upload() )
		{
			$fol = $this->upload->data();
			$fol = $fol['raw_name'];
			$info = '';
			
			$this->load->library('unzip');
			$this->unzip->extract('./_tmp/bak/'.$this->upload->file_name, './_tmp/install/');
			if ( file_exists("./_tmp/install/". $fol ."/install.xml") ) {
				
				$fol_install = "./_tmp/install/" . $fol;
				$xml = simplexml_load_file($fol_install ."/install.xml");
				
				$new = array();
				$buat = array();
				$cek = $xml->getName();
				
				if ( $cek == 'module' )
				{
					foreach ( $xml->children() as $child )
					{
					   $new[$child->getName()] = htmlspecialchars_decode($child);
					}
					
					if ( (count($new) == 7 || count($new) == 8) && ! empty($new['nama_module']) && ! empty($new['konfigurasi']) )
					{
						$new['nama_module'] = underscore($new['nama_module']);
						foreach( $xml->konfigurasi->children() as $item )
						{
							$buat[$item->getName()] = htmlspecialchars_decode($item);
						}
							
						if ( ! empty($buat['versi']) && $buat['versi'] == $this->config->item('cms_version') )
						{
							if( ! empty($buat['database']) && $this->cms->adaModule($new['nama_module']) )
							{
								$this->load->dbforge();
								
								$i = 0;
								foreach($xml->konfigurasi->database->children() as $keys => $isi)
								{
									$kolom[$i] = $isi;
									$i++;
								}
								
								$nmtbl = array();
								$field = array();
								for($x=0; $x < count($kolom); $x++)
								{
									$nmtbl[$x] = $kolom[$x]['nama'];

									$this->dbforge->add_field('id');
									
									foreach($xml->konfigurasi->database->tabel[$x]->children() as $isi)
									{
										$field[$x] = array(
													(string)$isi->nama => (array)$isi->setup
												 );
										$this->dbforge->add_field($field[$x]);
									}
									
									if ( $this->input->post('fresh') == 1 )
									{
										$this->dbforge->drop_table($nmtbl[$x]);
									}
									if( $this->dbforge->create_table($nmtbl[$x], TRUE) )
									{
										$info .= "<p><strong>Sukses membuat tabel untuk module.</strong> -> " . $nmtbl[$x] . "..!</p>";
									}
									else {
										$info = "<p><strong>Gagal membuat tabel untuk module.</strong> -> " . $nmtbl[$x] . ".!</p>";
									}
								}
								
								$tbldb = implode(",", $nmtbl);
								$ubah = array_pop($new);
								$new = array_merge($new, array('tabel_data' => $tbldb));
							}
							else {
								$ubah = array_pop($new);
							}
							
							$this->_simpan_install($fol_install, $new, $info);
						}
						elseif ( ! empty($buat['versi']) && $buat['versi'] != $this->config->item('cms_version') ) {
							echo "Perintah installer gagal diproses.!<br />Versi Module CMS tidak sesuai.";
							delete_files('./_tmp/install/', TRUE);
							$this->install_module();
						}
						else {
							echo "Perintah installer gagal diproses.!<br />Konfigurasi install Module tidak valid.";
							delete_files('./_tmp/install/', TRUE);
							$this->install_module();
						}
					}
					else {
						echo "Format File(.xml) installer module tidak valid.!";
						delete_files('./_tmp/install/', TRUE);
						$this->install_module();
					}
				}
				else {
					echo "Paket(.zip) installer module tidak valid.!";
					delete_files('./_tmp/install/', TRUE);
					$this->install_module();
				}
			}
			else {
				echo "Paket(.zip) installer corrupt/rusak.!";
				$this->install_module();
			}
		}
		else {
			echo "<strong>Gagal Upload.!</strong> <br />".$this->upload->display_errors();
			$this->install_module();
		}
	}
	
	function do_upload()
	{
		$this->load->helper('file');
		$pins = './_tmp/bak/';
		if ( $this->_cekbesar($pins) > $this->config->item('max_tmp_install') )
		{
			delete_files($pins, TRUE);
		}

		delete_files('./_tmp/install/', TRUE);

		$config['upload_path'] = './_tmp/bak/';
		$config['allowed_types'] = 'zip';
		$config['max_size']	= '1024';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload() )
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function _simpan_install($fol_install, $new, $info ='')
	{
		$pos = $this->cms->maxPosModule()+1;
		$new = array_merge($new, array('posisi' => $pos, 'status' => $this->input->post('aktif')));
		$old = umask(002);
		
		if ( count($new) <> 10 || $this->cms->adaModule($new['nama_module']) === FALSE )
		{
			if( $this->cms->adaModule($new['nama_module']) === FALSE )
			{
				echo "<p>Module <strong>" . $new['nama_module'] . "</strong> sudah terpasang.<p/>";
				$this->install_module();
			}
			else {
				echo "<p>GAGAL-Isi File(.xml) installer tidak bisa diproses.!</p>";
				$this->install_module();
			}
		}
		else {
			if ( $this->cms->uploadAddModule($new) )
			{
				$info .= $this->_module_file($fol_install, $new);
				$info .= $this->_assets_file($fol_install, $new);
				$info .= "Sukses, Module <strong>" . $new['nama_module'] . "</strong> telah terpasang";
				$this->tutup_dialog($info,5000);
			}
			else {
				$info = "Gagal, Terjadi kesalahan pada database.<br /> Cobalah beberapa saat lagi";
				$this->tutup_dialog_gagal($info);
			}
		}
		
		umask($old);
		if ($old != umask()) {
			die('An error occurred while changing back the umask.');
		}
	}
	
	function _module_file($fol_install, $new)
	{
		$this->load->helper('directory');

		$info = '';
		$map = directory_map($fol_install, FALSE, TRUE);
		$mfiles = $map['modules'];
		
		$info .= "<strong>Module Files :</strong><br />";
		foreach ( $mfiles as $key => $val )
		{
			foreach ( $val as $item )
			{
				$file = $fol_install ."/modules/". $key . "/" . $item;
				$pindah = "./" . APPPATH . "modules/mod_". $new['nama_module']."/". $key . "/" . $item;
				
				if ( ! is_dir("./" . APPPATH . "modules/mod_".  $new['nama_module']) )
					mkdir("./" . APPPATH . "modules/mod_".  $new['nama_module'], DIR_CMS_MODE);

				if ( ! is_dir("./" . APPPATH . "modules/mod_".  $new['nama_module']."/". $key) )
					mkdir("./" . APPPATH . "modules/mod_".  $new['nama_module']."/". $key, DIR_CMS_MODE);

				if ( ! $this->_pindah_file($file, $pindah) )
					$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$key/$item tidak bisa terpasang.<br />";
				else
					$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$key/$item sukses terpasang.<br />";
			}
		}

		return $info;
	}
	
	function _assets_file($fol_install, $new)
	{
		if ( ! empty($new['js']) )
		{

			$info = '';
			$js = explode(',', $new['js']);
			$jml = count($js);
			$info .= "<strong>JS :</strong><br />";
			for($i = 0; $i < $jml; $i++)
			{
				if ( file_exists( $fol_install ."/js/". $js[$i] ) )
				{
					$file = $fol_install ."/js/". $js[$i];
					$pindah = "./assets/js/modul/". $new['nama_module']."/". $js[$i];

					if ( ! is_dir("./assets/js/modul/". $new['nama_module']) )
						mkdir("./assets/js/modul/". $new['nama_module'], DIR_CMS_MODE);

					if ( ! $this->_pindah_file($file, $pindah) )
						$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
				}
				else {
					$info .= "File <strong>" . $js[$i] . "</strong> tidak ditemukan di folder install!...<br />";
				}
			}
		}
		
		if ( ! empty($new['css']) )
		{
			$css = explode(',', $new['css']);
			$jml = count($css);
			$info .= "<strong>CSS :</strong><br />";
			for($i = 0; $i < $jml; $i++)
			{
				if ( file_exists( $fol_install ."/css/". $css[$i] ) )
				{
					$file = $fol_install . "/css/"  . $css[$i];
					$pindah = "./assets/css/modul/". $new['nama_module'] . "/" . $css[$i];

					if ( ! is_dir("./assets/css/modul/". $new['nama_module']) )
						mkdir("./assets/css/modul/". $new['nama_module'], DIR_CMS_MODE);

					if ( ! $this->_pindah_file($file, $pindah) )
						$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah tidak bisa terpasang.<br />";
					else
						$info .=  "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah sukses terpasang.<br />";
				}
				else {
					$info .= "File <strong>" . $css[$i] . "</strong> tidak ditemukan di folder install!...<br />";
				}
			}
		}
		
		if ( is_dir($fol_install ."/img") )
		{
			$ftmp = $fol_install ."/img";
			$img = opendir($ftmp);
			$pindah = "./assets/css/modul/". $new['nama_module'] . "/img"; 
			$info .= "<strong>CSS-img :</strong><br />";

			if ( ! is_dir($pindah) )
				mkdir($pindah, DIR_CMS_MODE);

			while($files=readdir($img)){
	            if($files!="." && $files!="..")
	            {
					if ( ! $this->_pindah_file($ftmp . "/" . $files, $pindah . "/" . $files) )
						$info .= "[" . symbolic_permissions(fileperms($pindah . "/" . $files))  . "] " . "$pindah/$files tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah . "/" . $files))  . "] " . "$pindah/$files sukses terpasang.<br />";
	            }
	        }
	        closedir($img);
		}

		return $info;
	}
	
	function _module_cek($str)
	{
		if ($this->cms->adaModule($str) == FALSE)
		{
			$this->form_validation->set_message('_module_cek', 'Module dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

}

/* End of file atur_module.php */
/* Location: ./application/controllers/admin/atur_module.php */

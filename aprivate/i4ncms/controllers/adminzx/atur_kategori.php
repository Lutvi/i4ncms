<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Atur_kategori extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');

		$this->load->library('form_validation');
		$this->input->post(NULL, TRUE);
	}

	public function index($sts='')
	{
        $this->load->library('pagination');

        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/atur_kategori'));
        $config['total_rows'] = $this->web->getCountKategoriKonten();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = empty($sts)?3:4;
        $config = array_merge($config,$this->_adminPagination());

        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }

        $data = $this->_getAdminAssets();
        $data['kategori'] = $this->web->getAllKategoriKonten($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data['title'] = 'Kategori Konten';

        if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_kategori',$data);
		}
		else {
			if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			{
				$this->load->view('dynamic_js',$data);
				$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_kategori',$data);
			}
			else {
				$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
				$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	        }
	    }
	}

	function update_tabel()
	{
		$this->index('update_tabel');
	}

	function update_nama()
	{
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('katid', 'ID', 'required|integer|xss_clean');
		$this->form_validation->set_rules('nama_id', 'Nama ID', 'required|xss_clean|callback__cek_upkat_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'required|xss_clean|callback__cek_upkat_en');

        //hidden
        $this->form_validation->set_rules('tipe', 'Tipe', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nama_ori_id', 'Ori Indonesia', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nama_ori_en', 'Ori English', 'trim|required|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( $this->input->post('katid') && $this->_isSupAdmin() )
			{
				if( ! $this->web->upLabelKategori() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Label Kategori..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
	}

	function _cek_upkat_id($str)
	{
		if ($this->web->cekUpKategori($str,$this->input->post('nama_ori_id'),$this->input->post('tipe'),'id') == FALSE)
        {
            $this->form_validation->set_message('_cek_upkat_id', 'Kategori '.$this->input->post('tipe').' Indonesia dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

	function _cek_upkat_en($str)
	{
		if ($this->web->cekUpKategori($str,$this->input->post('nama_ori_en'),$this->input->post('tipe'),'en') == FALSE)
        {
            $this->form_validation->set_message('_cek_upkat_en', 'Kategori '.$this->input->post('tipe').' English dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

}

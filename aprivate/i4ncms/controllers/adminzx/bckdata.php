<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bckdata extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');

		$this->load->library('form_validation');
		$this->load->helper('file');
		$this->load->library('zip');
		$this->input->post(NULL, TRUE);
		ini_set('memory_limit', '1026M'); //1GB
		ini_set('zlib.output_compression', 0);
		set_time_limit(0);
	}

	public function index()
	{
		$data = $this->_getAdminAssets();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		$data['hist_backup'] = $this->bckp->geAlltHistoryBackup();
        $data['jml_backup'] = $this->bckp->getCountHistoryBackupByTgl();
        $data['jml_all_backup'] = $this->bckp->getCountAllBackup();
        $data['sts_backup'] = $this->bckp->getStsBackupByTgl();

		$data['file_tmp'] = $this->_cekbesar('./_tmp/');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/tab_backup',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

// Download file backup
    function download_backup()
    {
		$tgl = bs_kode(trim($this->uri->segment(4,0)), true);

		if ( ! is_dir('_xbck/pack/'.$tgl) || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak) )
		{
			redirect(config_item('admpath').'/pengaturan','refresh');
		}
		else {
			// Up status
			$nama_file = underscore(config_item('site_name')).'-'.$tgl;
			#$this->bckp->upStsBackupByTgl($tgl,'download');

			$this->zip->read_dir('_xbck/pack/media/', FALSE);
			$this->zip->read_dir('_xbck/pack/'.$tgl.'/', FALSE);
			$this->zip->download('backup-'.$nama_file.'.zip');
		}
    }

/* Aksi Backup */
	function full_backup($partial=FALSE)
    {
		if($this->backup_web(TRUE) && $this->backup_db(TRUE) && $this->backup_media(TRUE)) {
			if($partial) {
				$this->bckp->addHistoryBackup('full');
				return TRUE;
			}
			else {
				echo "sukses";
				$this->bckp->addHistoryBackup('full');
			}
		}
		else {
			echo "Mohon Maaf, Telah terjadi kesalahan.<br>sepertinya aktifitas website sedang sibuk.<br>cobalah untuk mengulangi setelah beberapa menit lagi.";
		}
    }
    
	function backup_web($partial=FALSE)
    {
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('token', 'Token Aksi', 'required|xss_clean|callback__cektoken');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$tgl = date("dmY");
			$nama_file = $tgl.'-'.underscore(config_item('site_name'));
			$path = './_xbck/';

			// Buat folder khusus
			$this->_dir_bck($tgl);
			delete_files($path.'pack/'.$tgl, TRUE);

			// CORE
			$this->zip->get_files_from_folder(APPPATH.'/models/widgets/', '/models/widgets/');
			$this->zip->read_dir(APPPATH.'/modules/', FALSE);
			$this->zip->read_dir(APPPATH.'/widgets/', FALSE);
			$this->zip->archive($path.'pack/'.$tgl.'/'.'cms-'.$nama_file.'.zip');
			$this->zip->clear_data();

			// APP
			$this->zip->read_dir('app/widgets/');
			$this->zip->read_dir('app/'.config_item('theme_id').'/');
			$this->zip->read_dir('app/mobile_'.config_item('theme_id').'/');
			// Assets APP
			$this->zip->read_dir('assets/js/modul/');
			$this->zip->read_dir('assets/js/widget/');
			$this->zip->read_dir('assets/css/modul/');
			$this->zip->read_dir('assets/css/widget/');
			$this->zip->read_dir('assets/'.config_item('theme_id').'/');
			$this->zip->read_dir('assets/scr-tema/');
			if(file_exists('m-assets/main/'))
			$this->zip->read_dir('m-assets/main/');
			if(file_exists('m-assets/'.config_item('theme_id').'/'))
			$this->zip->read_dir('m-assets/'.config_item('theme_id').'/');
			$this->zip->archive($path.'pack/'.$tgl.'/'.'app-'.$nama_file.'.zip');
			$this->zip->clear_data();

			if($partial)
			return TRUE;
			else {
				echo "sukses";
				$this->bckp->addHistoryBackup('web');
			}
		}
    }

    function backup_db($partial=FALSE)
    {
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('token', 'Token Aksi', 'required|xss_clean|callback__cektoken');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$this->bckp->stopWebsite(TRUE);
			// Load the utility class
			$this->bckp->addHistoryBackup('db');
			$this->load->dbutil();

			$tgl = date("dmY");
			$nama_file = $tgl.'-'.underscore(config_item('site_name'));
			$path = './_xbck/';

			// Buat folder khusus
			$this->_dir_bck($tgl);
			if( ! $partial)
			delete_files($path.'pack/'.$tgl, TRUE);

			// DB CMS
			$tbl_sys = array($this->web->tbl_web_config, $this->web->tbl_tema, $this->web->tbl_widget, $this->web->tbl_module, $this->web->tbl_menu, $this->web->tbl_halaman, $this->web->tbl_tag);
			$tbl_konten = array($this->web->tbl_album, $this->web->tbl_gallery, $this->web->tbl_video, $this->web->tbl_playlist, $this->web->tbl_blog);
			$tbl_media = array($this->web->tbl_banner_header,$this->web->tbl_img_produk, $this->web->tbl_img_produk_lama);

			// DB Module & Widget
			$tbl_mod = $this->bckp->getDependModTbl();
			$tbl_wid = $this->bckp->getDependWidTbl();

			$bck_tbl = array_merge($tbl_sys,$tbl_konten,$tbl_media,$tbl_mod,$tbl_wid);
			$prefs = array('tables' => $bck_tbl,'format'  => 'zip', 'filename' => 'dbbackup.sql');
			$this->dbutil->backup($prefs);
			$this->zip->archive($path.'pack/'.$tgl.'/'.'db-'.$nama_file.'.zip');
			$this->zip->clear_data();

			//FULL DB
			#$this->_backup_dbfull();
			$this->bckp->startWebsite();

			if($partial)
				return TRUE;
			else {
				$this->bckp->addHistoryBackup('db');
				echo "sukses";
			}
		}
    }

	// FULL DB
    function _backup_dbfull()
    {
		// Load the utility class
		$this->load->dbutil();

		$tgl = date("dmY");
		$nama_file = $tgl.'-'.underscore(config_item('site_name'));
		$path = './_xbck/';

		// DB FULL
		$ignore = array($this->web->tbl_log,$this->web->tbl_ref_user,$this->web->tbl_ref_user);
		$this->dbutil->backup(array('ignore' => $ignore, 'format'  => 'zip', 'filename' => 'dbfull-backup.sql'));
		$this->zip->archive($path.'pack/'.$tgl.'/'.'db-full-'.$nama_file.'.zip');
		$this->zip->clear_data();
    }

    function backup_media($partial=FALSE)
    {
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('token', 'Token Aksi', 'required|xss_clean|callback__cektoken');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {

			$path = './_xbck/';
			// Buat folder khusus
			$this->_dir_bck();

			// Media
			if ( ! is_dir($path.'pack/media') )
				mkdir($path.'pack/media', DIR_CMS_MODE);
				delete_files($path.'pack/media', TRUE);
			
			$this->zip->read_dir('_media/');
			#$this->zip->read_dir('_produk/');
			$this->zip->read_dir('_tmp/');
			$this->zip->archive($path.'pack/media/media-'.underscore(config_item('site_name')).'.zip');
			$this->zip->clear_data();

			if($partial)
			return TRUE;
			else {
				$this->bckp->addHistoryBackup('media');
				echo "sukses";
			}
		}
	}

    function jalankan_restore()
    {
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('token', 'Token Aksi', 'required|xss_clean|callback__cektoken');
        $this->form_validation->set_rules('tgl_backup', 'Tgl Backup', 'required|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			// Cek klo blm ada file backup tgl sekarang dibuatin dulu
			if ($this->bckp->getCountHistoryBackupByTgl() == 0)
			$status = $this->full_backup(TRUE);
			else
			$this->bckp->stopWebsite(TRUE);

			$status = FALSE;
			$dtpost = explode("|",$this->input->post('tgl_backup'));
			$tgl = $dtpost[0]; // sesuai tgl
			$spath = './_xbck/pack/';

			$status = $this->_restore_cms('db',$tgl);
			$status = $this->_restore_cms('cms',$tgl);
			$status = $this->_restore_cms('app',$tgl);
			$status = $this->_restore_cms('media');

			if($status === TRUE) {
				$this->session->set_flashdata('info', 'Aksi Restore Sukses, konfigurasi website <b>'.$dtpost[1].'</b> telah berhasil  dipasang.');
				$this->bckp->upStsBackupByTgl($tgl,'restore');
				$status = "sukses";
			}
			else {
				$status = "gagal";
				$this->session->set_flashdata('error', $status);
			}

			$this->bckp->startWebsite();
			if($this->input->is_ajax_request() === FALSE)
				redirect(config_item('admpath').'/pengaturan','refresh');
			else
				echo $status;
		}
    }

	function cleanup()
	{
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('token', 'Token Aksi', 'required|xss_clean|callback__cektoken');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$this->bckp->stopWebsite(TRUE);

			// clean file cache
			delete_files('./_tmp/web/', TRUE);
			// clean file install & Log
			delete_files('./_tmp/bak/', TRUE);
			delete_files('./_tmp/catat/', TRUE);
			// clean cache db
			$this->db->cache_delete_all();
			// clean alamat
			$status = $this->orderan->cleanAlamat();
			// clean file backup <= 1tahun
			$this->bckp->cleanFileBackup();

			if($status === TRUE) {
				$this->session->set_flashdata('info', 'Aksi CleanUp Sukses, cleanup sistem selesai diproses.');
				echo "sukses";
			}
			else {
				if($this->input->is_ajax_request() === FALSE)
				$this->session->set_flashdata('error', 'Aksi Gagal, cleanup sistem gagal diproses.<br>cobalah beberapa saat lagi.');
				else
				echo "Aksi Gagal, Cleanup sistem gagal diproses.<br>cobalah beberapa saat lagi.";
			}

			$this->bckp->startWebsite();
			if($this->input->is_ajax_request() === FALSE)
			redirect(config_item('admpath').'/pengaturan','refresh');
		}
	}

	// opsi : db,cms,app,media
	// tgl file backup
    function _restore_cms($tipe='', $tgl='')
    {
		//Set Bck File
		$spath = './_xbck/pack/';
		if($tipe == 'db')
		$src = $spath.$tgl.'/db-'.$tgl.'-'.underscore(config_item('site_name')).'.zip';
		if($tipe == 'cms')
		$src = $spath.$tgl.'/cms-'.$tgl.'-'.underscore(config_item('site_name')).'.zip';
		if($tipe == 'app')
		$src = $spath.$tgl.'/app-'.$tgl.'-'.underscore(config_item('site_name')).'.zip';
		if($tipe == 'media')
		$src = $spath.'media/media-'.underscore(config_item('site_name')).'.zip';

		// Buat folder khusus
		if ( ! is_dir('./_xbck/unpack') )
			mkdir('./_xbck/unpack', DIR_CMS_MODE);

		if(file_exists($src)){
			$zip = new ZipArchive();
			if($zip->open($src) === TRUE)
			{
				$status = FALSE;
				$zip->extractTo('./_xbck/unpack/');
				$zip->close();

				//copy recursively
				if($tipe == 'db')
				$status = $this->bckp->restore_dbnya('./_xbck/unpack/dbbackup.sql');
				if($tipe == 'cms')
				$status = cpy('_xbck/unpack', APPPATH);
				if($tipe == 'app' || $tipe == 'media')
				$status = cpy('_xbck/unpack', FCPATH);

				//CleanUp Unpack folder
				delete_files('./_xbck/unpack/', TRUE);
				return $status;
			}
			else {
				return "Mohon Maaf, Telah terjadi kesalahan saat menjalankan restore ".$tipe.".<br>sepertinya aktifitas website sedang sibuk.<br>cobalah untuk mengulangi setelah beberapa menit lagi.<br>";
			}
		}
		else {
			return TRUE;
		}
	}

	function _cektoken($str)
	{
		if (bs_kode($str, true) != 'letsflying')
		{
			$this->form_validation->set_message('_cektoken', 'Token tidak valid, aksi digagalkan."');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _dir_bck($tgl='')
	{
		$path = './_xbck/';

		// Buat folder khusus
		if ( ! is_dir($path) )
			mkdir($path, DIR_CMS_MODE);
		if ( ! is_dir($path.'pack') )
			mkdir($path.'pack', DIR_CMS_MODE);

		//Tambah htaccess & file index
		if( ! is_file($path.'.htaccess'))
		cpy('./conf/.htaccess',$path.'.htaccess',0640);
		if( ! is_file($path.'index.html'))
		cpy('./conf/index.html',$path.'index.html');

		if ( ! empty($tgl)) {
			if ( ! is_dir($path.'pack/'.$tgl) )
				mkdir($path.'pack/'.$tgl, DIR_CMS_MODE);
		}
	}
/* End Backup */

}

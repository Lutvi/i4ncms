<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Order extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman','batman','spiderman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');

		$pp = $this->adm_per_halaman;
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Orderan';
		$config = $this->_adminPagination();

		/*proses transaksi bayar */
		$config_proses_bayar['base_url'] = base_url().$this->config->item('admpath').'/order/proses_bayar';
		$config_proses_bayar['total_rows'] = $this->orderan->getCountOrderProses();
		$config_proses_bayar['per_page'] = $pp; 
		$config_proses_bayar['uri_segment'] = $this->_getSegment('proses_bayar');
		$config_proses_bayar = array_merge($config_proses_bayar, $config);

		/*proses transaksi paket*/
		$config_proses_paket['base_url'] = base_url().$this->config->item('admpath').'/order/proses_pemaketan';
		$config_proses_paket['total_rows'] = $this->orderan->getCountOrderProses($proses='pemaketan');
		$config_proses_paket['per_page'] = $pp; 
		$config_proses_paket['uri_segment'] = $this->_getSegment('proses_pemaketan');
		$config_proses_paket = array_merge($config_proses_paket, $config);

		/*proses transaksi kirim*/
		$config_proses_kirim['base_url'] = base_url().$this->config->item('admpath').'/order/proses_pengiriman';
		$config_proses_kirim['total_rows'] = $this->orderan->getCountOrderProses($proses='pengiriman');
		$config_proses_kirim['per_page'] = $pp; 
		$config_proses_kirim['uri_segment'] = $this->_getSegment('proses_pengiriman');
		$config_proses_kirim = array_merge($config_proses_kirim, $config);

		/*proses transaksi pending*/
		$config_proses_pending['base_url'] = base_url().$this->config->item('admpath').'/order/proses_pending';
		$config_proses_pending['total_rows'] = $this->orderan->getCountOrderProses($proses='pending');
		$config_proses_pending['per_page'] = $pp; 
		$config_proses_pending['uri_segment'] = $this->_getSegment('proses_pending');
		$config_proses_pending = array_merge($config_proses_pending, $config);

		/*proses transaksi batal*/
		$config_proses_batal['base_url'] = base_url().$this->config->item('admpath').'/order/proses_batal';
		$config_proses_batal['total_rows'] = $this->orderan->getCountOrderProses($proses='batal');
		$config_proses_batal['per_page'] = $pp; 
		$config_proses_batal['uri_segment'] = $this->_getSegment('proses_batal');
		$config_proses_batal = array_merge($config_proses_batal, $config);

		/*status order sukses*/
		$config_sukses['base_url'] = base_url().$this->config->item('admpath').'/order/sukses';
		$config_sukses['total_rows'] = $this->orderan->getCountOrderSukses();
		$config_sukses['per_page'] = $pp; 
		$config_sukses['uri_segment'] = $this->_getSegment('sukses');
		$config_sukses = array_merge($config_sukses, $config);

		/*status order batal*/
		$config_batal['base_url'] = base_url().$this->config->item('admpath').'/order/batal';
		$config_batal['total_rows'] = $this->orderan->getCountOrderBatal();
		$config_batal['per_page'] = $pp; 
		$config_batal['uri_segment'] = $this->_getSegment('batal');
		$config_batal = array_merge($config_batal, $config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
		{
			/*proses transaksi*/
			$offset_proses_bayar = $this->getOffsetPage($this->uri->segment($config_proses_bayar['uri_segment']), $config_proses_bayar['per_page']);
			$offset_proses_paket = $this->getOffsetPage($this->uri->segment($config_proses_paket['uri_segment']), $config_proses_paket['per_page']);
			$offset_proses_kirim = $this->getOffsetPage($this->uri->segment($config_proses_kirim['uri_segment']), $config_proses_kirim['per_page']);
			$offset_proses_pending = $this->getOffsetPage($this->uri->segment($config_proses_pending['uri_segment']), $config_proses_pending['per_page']);
			$offset_proses_batal = $this->getOffsetPage($this->uri->segment($config_proses_batal['uri_segment']), $config_proses_batal['per_page']);
			
			/*status order*/
			$offset_sukses = $this->getOffsetPage($this->uri->segment($config_sukses['uri_segment']), $config_sukses['per_page']);
			$offset_batal = $this->getOffsetPage($this->uri->segment($config_batal['uri_segment']), $config_batal['per_page']);
		}
		else {
			/*proses transaksi*/
			$offset_proses_bayar = $this->uri->segment($config_proses_bayar['uri_segment'],0);
			$offset_proses_paket = $this->uri->segment($config_proses_paket['uri_segment'],0);
			$offset_proses_kirim = $this->uri->segment($config_proses_kirim['uri_segment'],0);
			$offset_proses_pending = $this->uri->segment($config_proses_pending['uri_segment'],0);
			$offset_proses_batal = $this->uri->segment($config_proses_batal['uri_segment'],0);
			/*status order*/
			$offset_sukses = $this->uri->segment($config_sukses['uri_segment'],0);
			$offset_batal = $this->uri->segment($config_batal['uri_segment'],0);
			
		}

		/*proses transaksi*/
		$data['proses_bayar'] = $this->orderan->getAllOrderanProses($config_proses_bayar['per_page'], $offset_proses_bayar);
		$data['proses_paket'] = $this->orderan->getAllOrderanProses($config_proses_paket['per_page'], $offset_proses_paket, $proses='pemaketan');
		$data['proses_kirim'] = $this->orderan->getAllOrderanProses($config_proses_kirim['per_page'], $offset_proses_kirim, $proses='pengiriman');
		$data['proses_pending'] = $this->orderan->getAllOrderanProses($config_proses_pending['per_page'], $offset_proses_pending, $proses='pending');
		$data['proses_batal'] = $this->orderan->getAllOrderanProses($config_proses_batal['per_page'],$offset_proses_batal, $proses='batal');

		/*status order*/
		$data['order_sukses'] = $this->orderan->getAllOrderanSukses($config_sukses['per_page'],$offset_sukses);
		$data['order_batal'] = $this->orderan->getAllOrderanBatal($config_batal['per_page'],$offset_batal);

		/*proses transaksi*/
		$this->pagination->initialize($config_proses_bayar);
		$data['page_proses_bayar'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_paket);
		$data['page_proses_paket'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_kirim);
		$data['page_proses_kirim'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_pending);
		$data['page_proses_pending'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_proses_batal);
		$data['page_proses_batal'] =  $this->pagination->create_links();

		/*status order*/
		$this->pagination->initialize($config_sukses);
		$data['page_sukses'] =  $this->pagination->create_links();
		$this->pagination->initialize($config_batal);
		$data['page_batal'] =  $this->pagination->create_links();

		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['nosidebar'] = TRUE;
		$data['partial_content'] = 'v_order';

		/* Get Views */
		if( empty($sts) )
		{
			if($this->uri->segment(3) == FALSE)
			$this->session->unset_userdata('aktif_tab');
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			return $data;
		}
	}

	public function proses_pending()
	{
		$data = $this->index($sts='proses_pending');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function proses_bayar()
	{
		$data = $this->index($sts='proses_bayar');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function proses_paket()
	{
		$data = $this->index($sts='proses_paket');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function proses_kirim()
	{
		$data = $this->index($sts='proses_kirim');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function proses_batal()
	{
		$data = $this->index($sts='proses_batal');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function sukses()
	{
		$data = $this->index($sts='sukses');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function batal()
	{
		$data = $this->index($sts='batal');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$data = $this->index($sts='ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_order',$data);
		}
		else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function detail()
	{
		$data = $this->_getAdminAssets();
		$trx = $this->uri->segment(4,$this->input->post('id_trx'));
		$data['title'] = 'Admin Detail Order'.$trx;

		$data['order'] = $this->orderan->getAllOrderanByTrx($trx);
		$data['login'] = $this->login;
		$data['total'] = $this->orderan->getTotalOrderanByTrx($trx);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['alamat'] = $this->_buildAlamatKirim($data['order'][0]->id_alamat);
		$data['detail_alamat'] = $this->orderan->getAllAlamatById($data['order'][0]->id_alamat);
		$data['diskon_transaksi'] = $this->orderan->getDiskonTransaksiByTrx($trx);

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/detail_order_trx',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	// PopUp Dialog get-ajax
	public function detail_produk()
	{
		$pid = $this->uri->segment(4,0);
		$data['detail_produk'] = $this->mproduk->getAllDetailProdById($pid);
		$data['img_produk'] = $this->mproduk->getImgDetailProdById($pid);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/detail_produk_order',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function detail_sukses()
	{
		$data = $this->_getAdminAssets();
		$trx = $this->uri->segment(4,$this->input->post('id_trx'));
		$data['title'] = 'Admin Detail Order Sukses'.$trx;
		
		$order = $this->orderan->getAllOrderanSuksesByTrx($trx);
		$data['order'] = $order;
		$data['login'] = $this->login;
		$data['total'] = $this->orderan->getTotalOrderanSuksesByTrx($trx);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['alamat'] = $this->_buildAlamatKirim($order[0]->id_alamat);
		$data['diskon_transaksi'] = $this->orderan->getDiskonTransaksiByTrx($trx);

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/detail_order_trx',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function detail_batal()
	{
		$data = $this->_getAdminAssets();
		$trx = $this->uri->segment(4,$this->input->post('id_trx'));
		$data['title'] = 'Admin Detail Order Batal'.$trx;
		$order = $this->orderan->getAllOrderanBatalByTrx($trx);
		$j = count($order) - 1;
		$data['order'] = $order;
		$data['login'] = $this->login;
		$data['total'] = $this->orderan->getTotalOrderanBatalByTrx($trx);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['alamat'] = $this->_buildAlamatKirim($order[$j]->id_alamat);
		$data['diskon_transaksi'] = $this->orderan->getDiskonTransaksiByTrx($trx);

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/detail_order_trx',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	// Dialog Update alamat kirim ajax-post
	public function ubah_alamat_kirim()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="errors">', '</div>');

		$this->form_validation->set_rules('nama_lengkap', 'Nama', 'trim_judul|required|xss_clean');
		$this->form_validation->set_rules('no_telpon', 'Telpon', 'required|max_length[14]|xss_clean');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required|xss_clean');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('wilayah', 'Wilayah', 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('kode_pos', 'Kodepos', 'required|max_length[10]|xss_clean');
		$this->form_validation->set_rules('alamat_rumah', 'Alamat', 'trim|required|xss_clean');

		$this->form_validation->set_rules('email_cust', 'Email', 'trim|valid_email|required|xss_clean|callback__ini_valid_email');

		//hidden
		$this->form_validation->set_rules('id_alamat', 'Id Alamat', 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('id_cust', 'Id Custommer', 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('trx', 'No TRX', 'trim|required|max_length[50]|xss_clean');

		$this->form_validation->set_rules('status_transaksi', 'Status Trans', 'trim|required|xss_clean');
		$this->form_validation->set_rules('metode_bayar', 'Metode byar', 'trim|required|xss_clean');
		$this->form_validation->set_rules('via', 'bayar via', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sub_total', 'Sub total', 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('diskon', 'diskon', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total', 'total', 'trim|integer|required|xss_clean');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo 'Gagal Ubah data, data yang terkirim tidak valid.!';
		}
		else {
			if ( ! $this->orderan->updateAlamatOrderan())
			{
				echo 'Gagal Ubah data, data tidak bisa tersimpan.!';
			}
			else {
				/*//kirim email*/
				$id_alamat = $this->input->post('id_alamat', TRUE);
				$id_cust = $this->input->post('id_cust', TRUE);
				$email = $this->pelanggan->getEmailPelangganById($id_cust);

				$status = $this->input->post('status_transaksi', TRUE);
				$metode = $this->input->post('metode_bayar', TRUE);
				$via = $this->input->post('via', TRUE);
				$nama = $this->pelanggan->getNamaPelangganById($id_cust);
				$trx = $this->input->post('trx', TRUE);
				$detail = $this->orderan->getFrontAllOrderanByTrx($trx, true);
				$sub_total = $this->input->post('sub_total', TRUE);
				$diskon = $this->input->post('diskon', TRUE);
				$total = $this->input->post('total', TRUE);
				$alamat = $this->_buildAlamatKirimEmail($id_alamat);
				
				$isi = format_isi_email_transaksi(current_lang(false),$status,$metode,$via,$nama,$trx,$detail,$sub_total,$diskon,$total,$alamat);
				$judul = 'Perubahan Alamat Kirim [-'.$trx.'-]'.$this->config->item('site_name');
				
				if ( ! kirim_email_user(array(), $email,$judul,$isi,TRUE))
					echo 'Data telah berhasil dirubah, tapi email notifikasi perubahan belum terkirim.!';
				else
					echo 'sukses';
			}
		}
	}

	public function update_trx()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('id_trx', 'No Struk Transaksi', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status_transaksi', 'Status Transaksi', 'trim|required');
		$this->form_validation->set_rules('status_order', 'Status Order', 'trim|required');
		
		if($this->input->post('status_order') === 'proses' && ($this->input->post('status_transaksi') === 'pemaketan' OR $this->input->post('status_transaksi') === 'pengiriman' OR $this->input->post('status_transaksi') === 'sukses'))
			$this->form_validation->set_rules('no_resi', 'No Resi', 'trim|required');
		
		if($this->form_validation->run($this) === FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->detail();
		}
		else {
			if ($this->input->post('hapus') && $this->input->post('id_trx') && $this->input->post('id_cust') && $this->input->post('jml_trans_trx') && $this->input->post('total_trx'))
			{
				if( ! $this->orderan->hapusOrderBatal($this->input->post('id_trx'),$this->input->post('id_cust'),$this->input->post('jml_trans_trx'),$this->input->post('total_trx')))
				{
					$info = 'Gagal, Telah terjadi kesalahan database.';
					$this->session->set_flashdata('info', $info);
					redirect($this->config->item('admpath').'/order/detail/'.$this->input->post('id_trx'), 'refresh');
				}
				else {
					$info = "Sukses, Data Transaksi ".$this->input->post('id_trx')." berhasil dihapus.";
                    $this->tutup_dialog($info);
				}
			}
			else {
				if ( ! $this->orderan->upTrxStatusOrder($this->input->post('id_trx'), $this->input->post('status_transaksi'),$this->input->post('status_order'),$this->input->post('no_resi')))
				{
					$info = 'Gagal, Telah terjadi kesalahan database.';
				}
				else {
					$info = 'Sukses, Data Transaksi berhasil diupdate.';
				}
				$this->session->set_flashdata('info', $info);
				if($this->input->post('status_transaksi') === 'sukses')
				{
					redirect($this->config->item('admpath').'/order/detail_sukses/'.$this->input->post('id_trx'), 'refresh');
				}
				elseif($this->input->post('hapus')) {
					redirect($this->config->item('admpath').'/order/detail_batal/'.$this->input->post('id_trx'), 'refresh');
				}
				else {
					redirect($this->config->item('admpath').'/order/detail/'.$this->input->post('id_trx'), 'refresh');
				}
			}
		}
	}

	public function batalkan_transaksi()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		$this->form_validation->set_rules('id_trx', 'No Transaksi', 'trim|required|max_length[50]|xss_clean');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
		}
		else {
			if( ! $this->orderan->upTrxBatalTransaksi($this->input->post('id_trx')))
			{
				echo 'Gagal batalkan transaksi, terjadi kesalahan database';
			}
			else {
				echo 'sukses';
			}
		}
	}

	public function lanjut_proses_order()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('trx', 'TRX', 'trim|required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('status', 'Status Transaksi', 'trim|required|max_length[20]|xss_clean');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
		}
		else {
			if ( ! $this->orderan->upTrxStatusTrans($this->input->post('trx'),$this->input->post('status')))
			{
				echo "Gagal update transaksi ".$this->input->post('trx')." silahkan coba lagi.! <br> atau silahkan update secara manual.";
			}
			else {
				echo "sukses";
			}
		}
	}

	public function hapus_order()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('id_trx', 'No Transaksi', 'trim|required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('id_cust', 'Id Pembeli', 'trim|required|integer|max_length[11]|xss_clean');
		$this->form_validation->set_rules('jml_trans_trx', 'Jumlah Transaksi', 'trim|required|integer|max_length[11]|xss_clean');
		$this->form_validation->set_rules('total_trx', 'Total Transaksi', 'trim|required|integer|max_length[11]|xss_clean');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
		}
		else {
			if( ! $this->orderan->hapusOrderBatal($this->input->post('id_trx'),$this->input->post('id_cust'),$this->input->post('jml_trans_trx'),$this->input->post('total_trx')))
			{
				echo 'Gagal batalkan transaksi, terjadi kesalahan database';
			}
			else {
				echo 'sukses';
			}
		}
	}

	public function cek_tab()
	{
		$aktif = $this->input->get('aktif_tab', TRUE);
		$this->session->set_userdata('aktif_tab', $aktif);
	}

}

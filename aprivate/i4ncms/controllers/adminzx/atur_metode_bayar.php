<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Atur_metode_bayar extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');
		#$pp = $this->adm_per_halaman;

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Metode Pembayaran';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$config = $this->_adminPagination();
		
		$bayar_transfer['base_url'] = base_url().$this->config->item('admpath').'/atur_metode_bayar/bayar_transfer';
		$bayar_transfer['total_rows'] = $this->orderan->getCountMetodeBayarByTipe('transfer');
		$bayar_transfer['per_page'] = $this->adm_per_halaman;
		$bayar_transfer['uri_segment'] = $this->_getSegment('bayar_transfer');
		$bayar_transfer = array_merge($bayar_transfer, $config);

		$bayar_online['base_url'] = base_url().$this->config->item('admpath').'/atur_metode_bayar/bayar_online';
		$bayar_online['total_rows'] = $this->orderan->getCountMetodeBayarByTipe('online');
		$bayar_online['per_page'] = $this->adm_per_halaman;
		$bayar_online['uri_segment'] = $this->_getSegment('bayar_online');
		$bayar_online = array_merge($bayar_online, $config);

		$bayar_giro['base_url'] = base_url().$this->config->item('admpath').'/atur_metode_bayar/bayar_giro';
		$bayar_giro['total_rows'] = $this->orderan->getCountMetodeBayarByTipe('giro');
		$bayar_giro['per_page'] = $this->adm_per_halaman;
		$bayar_giro['uri_segment'] = $this->_getSegment('bayar_giro');
        $bayar_giro = array_merge($bayar_giro, $config);

        $bayar_cod['base_url'] = base_url().$this->config->item('admpath').'/atur_metode_bayar/bayar_cod';
		$bayar_cod['total_rows'] = $this->orderan->getCountMetodeBayarByTipe('cod');
		$bayar_cod['per_page'] = $this->adm_per_halaman;
		$bayar_cod['uri_segment'] = $this->_getSegment('bayar_cod');
        $bayar_cod = array_merge($bayar_cod, $config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset_transfer = $this->getOffsetPage($this->uri->segment($bayar_transfer['uri_segment']), $bayar_transfer['per_page']);
            $offset_online = $this->getOffsetPage($this->uri->segment($bayar_online['uri_segment']), $bayar_online['per_page']);
            $offset_giro = $this->getOffsetPage($this->uri->segment($bayar_giro['uri_segment']), $bayar_giro['per_page']);
            $offset_cod = $this->getOffsetPage($this->uri->segment($bayar_cod['uri_segment']), $bayar_cod['per_page']);
        }
        else{
            $offset_transfer = $this->uri->segment($bayar_transfer['uri_segment'],0);
            $offset_online = $this->uri->segment($bayar_online['uri_segment'],0);
            $offset_giro = $this->uri->segment($bayar_giro['uri_segment'],0);
            $offset_cod = $this->uri->segment($bayar_cod['uri_segment'],0);
        }
        
		$data['bayar_transfer'] = $this->orderan->getAllMetodeBayarByTipe('transfer',$bayar_transfer['per_page'],$offset_transfer);
		$data['bayar_online'] = $this->orderan->getAllMetodeBayarByTipe('online',$bayar_online['per_page'],$offset_online);
		$data['bayar_giro'] = $this->orderan->getAllMetodeBayarByTipe('giro',$bayar_giro['per_page'],$offset_giro);
		$data['bayar_cod'] = $this->orderan->getAllMetodeBayarByTipe('cod',$bayar_cod['per_page'],$offset_cod);
		
		$this->pagination->initialize($bayar_transfer);
		$data['page_transfer'] =  $this->pagination->create_links();

		$this->pagination->initialize($bayar_online);
		$data['page_online'] =  $this->pagination->create_links();

		$this->pagination->initialize($bayar_giro);
		$data['page_giro'] =  $this->pagination->create_links();

		$this->pagination->initialize($bayar_cod);
		$data['page_cod'] =  $this->pagination->create_links();
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->session->unset_userdata('aktif_tab_metode_bayar');
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
		}
		else {
			return $data;
		}
	}

	public function cek_tab()
	{
		$aktif = $this->input->get('aktif_tab_metode_bayar');
		$this->session->set_userdata('aktif_tab_metode_bayar', $aktif);
		echo $aktif;
	}

	public function bayar_transfer()
	{
		$data = $this->index('bayar_transfer');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function bayar_online()
	{
		$data = $this->index('bayar_online');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function bayar_giro()
	{
		$data = $this->index('bayar_giro');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function bayar_cod()
	{
		$data = $this->index('bayar_cod');
		$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_metode_bayar',$data);
	}

	public function update_tabel()
	{
		$data = $this->index('ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_metode_bayar',$data);
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function tambah_metode($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['sts'] = $sts;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_metode_bayar',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_metode()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'trim_judul|max_length[50]|required|callback__cek_vendor');
		$this->form_validation->set_rules('tipe', 'Tipe Metode', 'trim|required|strtolower');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('detail', 'Detail Metode', 'trim|required|max_length[250]');

        /* Cek logo */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Logo Vendor', 'trim|required');
        }

		//cek online
		if($this->input->post('tipe') == 'online')
		{
			$this->form_validation->set_rules('id_merchant', 'Id Merchant', 'trim|required');
			$this->form_validation->set_rules('hash_key', 'Hash Key', 'trim|required');
			$this->form_validation->set_rules('redirect_url', 'URL Redirect', 'trim|required|prep_url');
			$this->form_validation->set_rules('sukses_url', 'URL Sukses', 'trim|required|prep_url');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->tambah_metode('error');
        }
        else {
            if ( ! $this->_do_upload_logo() )
            {
                $error = "Gagal Upload Logo, pastikan ukuran dan jenis file yang diupload sudah sesuai";
                $this->session->set_flashdata('error', $error);
                $this->tambah_metode('error');
            }
            else {
                if( ! $this->orderan->tambahMetodeBayar() )
                {
					$error = "Data tidak dapat tersimpan ke dalam database.";
					$this->session->set_flashdata('error', $error);
                    $this->tambah_metode('error');
                }
                else {
                    $info = "Sukses, menambahkan Metode Pembayaran.";
                    $this->session->set_flashdata('info', $info);
                    redirect($this->config->item('admpath').'/atur_metode_bayar','refresh');
                }
				hapus_file('./_media/logo-bayar/'.$this->upload->orig_name);
            }
        }
    }

    public function detail_metode($sts='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_metode'));
        
        $data['metode'] = $this->orderan->getMetodeBayarById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['sts'] = '';

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_metode_bayar',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_metode()
    {
    	$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		if( $this->input->post('metode_id'))
		{
			$this->form_validation->set_rules('metode_id', 'ID', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID', 'required|integer|max_length[11]|xss_clean');
		}

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('metode_id') && $this->_isSupAdmin() ) 
			{
				if( ! $this->orderan->upMetodeBayarStat() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Status Metode Bayar..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->_isSupAdmin() ) {
				if( ! $this->orderan->hapusMetodeBayar() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Hapus Metode Bayar..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }

    public function update_form_metode()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'trim_judul|max_length[50]|required|callback__cek_upvendor');
		$this->form_validation->set_rules('tipe', 'Tipe Metode', 'trim|required|strtolower');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('detail', 'Detail Metode', 'trim|required|max_length[250]');

		//cek online
		if($this->input->post('tipe') == 'online')
		{
			$this->form_validation->set_rules('id_merchant', 'Id Merchant', 'trim|required');
			$this->form_validation->set_rules('hash_key', 'Hash Key', 'trim|required');
			$this->form_validation->set_rules('redirect_url', 'URL Redirect', 'trim|required|prep_url');
			$this->form_validation->set_rules('sukses_url', 'URL Sukses', 'trim|required|prep_url');
        }

        // hidden
        $this->form_validation->set_rules('id_metode', 'ID', 'trim|required');
        $this->form_validation->set_rules('nama_vendor_ori', 'vendor Ori', 'trim_judul|max_length[50]|required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'trim|required');
        $this->form_validation->set_rules('logo', 'Logo', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail_metode('error');
        }
        else {
            if($this->input->post('status_ganti') == 'yes')
            {
                if( ! $this->_do_upload_logo() )
                {
                    $error = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->session->set_flashdata('error', $error);
	                $this->detail_metode('error');
                }
                else {
                    if( ! $this->orderan->upMetodeBayar() )
                    {
                        $error = "Gagal Update Metode Bayar, terjadi kesalahan pada server database";
						$this->session->set_flashdata('error', $error);
		                $this->detail_metode('error');
                    }
                    else {
						//hapus logo lama
						hapus_file("./_media/logo-bayar/". $this->input->post('logo'));
						hapus_file("./_media/logo-bayar/thumb_". $this->input->post('logo'));

                        $info = "Sukses memperbaharui data <strong>Metode Bayar ".$this->input->post('nama_vendor')."</strong>";
                        $this->session->set_flashdata('info', $info);
	                    redirect($this->config->item('admpath').'/atur_metode_bayar','refresh');
                    }
                    hapus_file('./_media/logo-bayar/'.$this->upload->orig_name);
                }
            }
            else {
                if ( ! $this->orderan->upMetodeBayar() )
                {
                    $error = "Gagal Update Metode Bayar, terjadi kesalahan pada server database";
					$this->session->set_flashdata('error', $error);
                }
                else {
                    $info = "Sukses memperbaharui data <strong>Metode Bayar ".$this->input->post('nama_vendor')."</strong>";
                    $this->session->set_flashdata('info', $info);
                    redirect($this->config->item('admpath').'/atur_metode_bayar','refresh');
                }
            }
        }
    }

    function _cek_vendor($str)
    {
		if ($this->orderan->cekVendor($str) == FALSE)
        {
            $this->form_validation->set_message('_cek_vendor', 'Nama Vendor "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _cek_upvendor($str)
    {
		if ($this->orderan->cekVendor($str,$this->input->post('nama_vendor_ori')) == FALSE)
        {
            $this->form_validation->set_message('_cek_upvendor', 'Nama Vendor "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _do_upload_logo($path='./_media/logo-bayar/')
	{
		$this->load->helper('file');

		if ( ! is_dir($path) )
			mkdir($path, DIR_CMS_MODE);

		$config['file_name']  = time().'_'.$this->input->post('nama_vendor');
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '800';
        $config['max_width']  = '640';
        $config['max_height']  = '640';
        $config['remove_spaces']  = TRUE;
        $config['upload_path'] = $path;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			print_r($this->upload->display_errors());
            hapus_file($path.$this->upload->orig_name);
            return FALSE;
		}
		else {
            $config = array();
            // thumb
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'thumb_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 80;
            $config['height'] = 80;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                hapus_file($path.$this->upload->orig_name);
                return FALSE;
            }
			return TRUE;
		}
	}

}

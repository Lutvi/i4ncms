<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminCms extends AdminController {
	
	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman','batman');
	}

	public function index($sts='')
    {
        $this->load->library('pagination');

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin CMS';

		$config['base_url'] = base_url().$this->config->item('admpath').'/admincms';
		$config['total_rows'] = $this->adminlogin->getCountAllAdmin();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['admin'] = $this->adminlogin->getAllAdmin($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['partial_content'] = 'v_admincms';

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
    }

    public function update_tabel()
	{
		$this->load->library('pagination');
		
		$config['base_url'] = site_url($this->config->item('admpath').'/admincms/update_tabel');
		$config['total_rows'] = $this->adminlogin->getCountAllAdmin();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else{
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['admin'] = $this->adminlogin->getAllAdmin($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_admin',$data);
		}
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

    public function update_admin()
    {
    	$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		if( $this->input->post('id'))
		{
			$this->form_validation->set_rules('id', 'ID Akun Admin', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Akun Admin', 'required|integer|max_length[11]|xss_clean');
		}

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin)
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('id', TRUE) && $this->super_admin ) {
				if( ! $this->adminlogin->upAdminStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Status Admin..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->adminlogin->hapusAdminCms() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Admin..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }
    
	public function tambah($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css');
        $data['sts'] = $sts;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_admincms',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail()
    {
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$id = $this->uri->segment(4,$this->input->post('id_admin'));
			$id = bs_kode($id, TRUE);
		}
		else {
			$id = 0;
		}
		
        $data['admin'] = $this->adminlogin->getAdminById($id);
		$data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css');

	    $data['col_id'] = 'id';
	    $data['col_nama'] = 'nama_lengkap';
	    $data['col_uname'] = 'id_adm_uname';
	    $data['col_pass'] = 'adm_kunci_pas';
	    $data['col_email'] = 'adm_email';
	    $data['col_level'] = 'level';
	    $data['col_status'] = 'status';
	    $data['col_pembuat'] = 'pembuatnya';
	    $data['col_foto'] = 'foto';
	    $data['col_biografi'] = 'biografi';
	    $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_admincms',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail_profil()
    {
		$id = $this->session->userdata('idmu');
		$id = bs_kode($id, TRUE);

        $data['admin'] = $this->adminlogin->getAdminById($id);
		$data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css');

	    $data['col_id'] = 'id';
	    $data['col_nama'] = 'nama_lengkap';
	    $data['col_uname'] = 'id_adm_uname';
	    $data['col_pass'] = 'adm_kunci_pas';
	    $data['col_email'] = 'adm_email';
	    $data['col_level'] = 'level';
	    $data['col_status'] = 'status';
	    $data['col_pembuat'] = 'pembuatnya';
	    $data['col_foto'] = 'foto';
	    $data['col_biografi'] = 'biografi';
	    $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		if ($this->super_admin)
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_profil_admincms',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function updateform_admin()
    {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		// hidden
		$this->form_validation->set_rules('id_admin', 'ID', 'trim|required|cek__valid_idadmin');
		$this->form_validation->set_rules('nama_lengkap_ori', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('adm_email_ori', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('adm_id_ori', 'ID Login', 'trim|required');
		$this->form_validation->set_rules('adm_kunci_ori', 'Kunci Login', 'trim|required|min_length[6]');

		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('adm_email', 'Email', 'trim|required|valid_email|callback__ini_valid_email|callback__cek_up_admemail');
		$this->form_validation->set_rules('adm_id', 'ID Login', 'trim|required|alpha_dash|callback__cek_up_admid');
		$this->form_validation->set_rules('adm_kunci', 'Kunci Login', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('level', 'Level Admin', 'trim|required|callback__cek_valid_level');
		$this->form_validation->set_rules('biografi', 'Biografi singkat', 'trim|max_length[300]|required');

		if ($this->form_validation->run($this) == FALSE)
        {
            $this->detail();
        }
        else {
			if($this->input->post('status_ganti') == 'yes')
            {
				if( ! $this->_do_upload_profil() )
                {
                    $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->session->set_flashdata('error',$info);
					$this->detail('error');
                }
                else {
					if( ! $this->adminlogin->upAdminCmsForm())
					{
						$info = "Gagal memperbaharui data Admin, data tidak bisa tersimpan";
						$this->tutup_dialog_gagal($info);
					}
					else {
						if($this->input->post('profil'))
						{
							if($this->input->post('profil') != $this->input->post('adm_email'))
							$this->session->set_userdata('admemail', enkodeString($this->input->post('adm_email')));
						}
						$info = "Sukses memperbaharui data Admin ".$this->input->post('nama_lengkap');
						$this->tutup_dialog($info);
					}
				}
			}
			else {
				if( ! $this->adminlogin->upAdminCmsForm())
				{
					$info = "Gagal memperbaharui data Admin, data tidak bisa tersimpan";
					$this->tutup_dialog_gagal($info);
				}
				else {
					if($this->input->post('profil'))
					{
						if($this->input->post('profil') != $this->input->post('adm_email'))
						$this->session->set_userdata('admemail', enkodeString($this->input->post('adm_email')));
					}
					$info = "Sukses memperbaharui data Admin ".$this->input->post('nama_lengkap');
					$this->tutup_dialog($info);
				}
			}
        }
    }

    public function updateform_profil_admin()
    {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		// hidden
		$this->form_validation->set_rules('id_admin', 'ID', 'trim|required|_cek_up_admid');
		$this->form_validation->set_rules('nama_lengkap_ori', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('adm_email_ori', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('adm_id_ori', 'ID Login', 'trim|required');
		$this->form_validation->set_rules('adm_kunci_ori', 'Kunci Login', 'trim|required|min_length[6]');

		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('adm_email', 'Email', 'trim|required|valid_email|callback__ini_valid_email|callback__cek_up_admemail');
		$this->form_validation->set_rules('adm_id', 'ID Login', 'trim|required|alpha_dash|callback__cek_up_admid');
		$this->form_validation->set_rules('adm_kunci', 'Kunci Login', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('level', 'Level Admin', 'trim|required|callback__cek_valid_level');
		$this->form_validation->set_rules('biografi', 'Biografi singkat', 'trim|max_length[300]|required');

		if ($this->form_validation->run($this) == FALSE)
        {
            $this->detail_profil();
        }
        else {
			if($this->input->post('status_ganti') == 'yes')
            {
				if( ! $this->_do_upload_profil() )
                {
                    $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->session->set_flashdata('error',$info);
					$this->detail_profil();
                }
                else {
					if( ! $this->adminlogin->upProfilAdminForm())
					{
						$info = "Gagal memperbaharui data Profil, data tidak bisa tersimpan";
						$this->tutup_dialog_gagal($info);
					}
					else {
						if($this->input->post('profil'))
						{
							if($this->input->post('profil') != $this->input->post('adm_email'))
							$this->session->set_userdata('admemail', enkodeString($this->input->post('adm_email')));
						}
						$info = "Sukses memperbaharui data Profil ".$this->input->post('nama_lengkap');
						$this->tutup_dialog($info);
					}
				}
			}
			else {
				if( ! $this->adminlogin->upProfilAdminForm())
				{
					$info = "Gagal memperbaharui data Profil, data tidak bisa tersimpan";
					$this->tutup_dialog_gagal($info);
				}
				else {
					if($this->input->post('profil'))
					{
						if($this->input->post('profil') != $this->input->post('adm_email'))
						$this->session->set_userdata('admemail', enkodeString($this->input->post('adm_email')));
					}
					$info = "Sukses memperbaharui data Profil ".$this->input->post('nama_lengkap');
					$this->tutup_dialog($info);
				}
			}
        }
    }


	function add_admin()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		// hidden
		$this->form_validation->set_rules('id_pembuat', 'ID Pembuat', 'trim|required');

		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('adm_email', 'Email', 'trim|required|valid_email|callback__ini_valid_email|callback__cek_admemail');
		$this->form_validation->set_rules('adm_id', 'ID Login', 'trim|required|alpha_dash|callback__cek_admid');
		$this->form_validation->set_rules('adm_kunci', 'Kunci Login', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('level', 'Level Admin', 'trim|required|callback__cek_valid_level');
		$this->form_validation->set_rules('biografi', 'Biografi singkat', 'trim|max_length[300]|required');

		/* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Foto Profil', 'trim|required');
        }

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $info = "Akses ilegal, proses tidak bisa dilanjutkan.";
            $this->session->set_flashdata('error',$info);
            $this->tambah('error');
        }
        else {
			if( ! $this->_do_upload_profil() )
			{
				$info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
				$this->session->set_flashdata('error',$info);
				$this->tambah('error');
			}
			else {
				if( ! $this->adminlogin->addAdminCms())
				{
					$info = "Gagal menambahkan Admin baru";
					$this->tutup_dialog_gagal($info);
				}
				else {
					$info = "Sukses menambahkan Admin baru";
					$this->tutup_dialog($info);
				}
			}
        }
	}

	function _do_upload_profil($path='./_media/admincms/')
	{
		$this->load->helper('file');

		if ( ! is_dir($path) )
			mkdir($path, DIR_CMS_MODE);
		if ( ! is_dir($path.'medium') )
			mkdir($path.'medium', DIR_CMS_MODE);
		if ( ! is_dir($path.'thumb') )
			mkdir($path.'thumb', DIR_CMS_MODE);

		$config['file_name']  = time().'_pp';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';
        $config['max_width']  = '1280';
        $config['max_height']  = '1024';
        $config['remove_spaces']  = TRUE;
        $config['upload_path'] = $path;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			print_r($this->upload->display_errors());
            hapus_file($path.$this->upload->file_name);
            return FALSE;
		}
		else {

            $config = array();
            // medium
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'medium/medium_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 128;
            $config['height'] = 128;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();

            $config = array();
            // thumb
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'thumb/thumb_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 80;
            $config['height'] = 80;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                hapus_file($path.$this->upload->file_name);
                return FALSE;
            }
			hapus_file($path.$this->upload->file_name);
			return TRUE;
		}
	}

	function _valid_idadmin($str)
	{
		if ($this->super_admin && bs_kode($this->session->userdata('level'), TRUE) === 'ironman')
		{
			if ($str !== $this->session->userdata('idmu'))
			{
				$this->form_validation->set_message('_valid_idadmin', 'Gagal data tidak valid.');
				return FALSE;
			}
			else {
				return TRUE;
			}
        }
        else {
			return TRUE;
        }
	}

	function _cek_valid_level($str)
	{
		$level = bs_kode($str, TRUE);
		if ($level === 'superman' || $level === 'batman' || $level === 'ironman' || $level === 'spiderman')
		{
			return TRUE;
		}
		else {
			$this->form_validation->set_message('_cek_valid_level', 'Gagal data level tidak valid.');
			return FALSE;
		}
	}

	function _cek_up_admid($str)
	{
		if ($this->adminlogin->cekUpAdaUname($str,$this->input->post('adm_id_ori')) == FALSE)
        {
            $this->form_validation->set_message('_cek_up_admid', 'ID "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

	function _cek_up_admemail($str)
	{
		if ($this->adminlogin->cekUpAdaEmail($str,$this->input->post('adm_email_ori')) == FALSE)
        {
            $this->form_validation->set_message('_cek_up_admemail', 'Email "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

	function _cek_admid($str)
	{
		if ($this->adminlogin->cekAdaUname($str) == FALSE)
        {
            $this->form_validation->set_message('_cek_admid', 'ID "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

	function _cek_admemail($str)
	{
		if ($this->adminlogin->cekAdaEmail($str) == FALSE)
        {
            $this->form_validation->set_message('_cek_admemail', 'Email "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

}

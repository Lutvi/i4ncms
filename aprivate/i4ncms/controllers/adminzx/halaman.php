<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Halaman extends AdminController {

    public function __construct()
    {
        parent::__construct();

        $this->_cekStsLoginAdm();
        $this->hak = array('superman','batman');
    }
    
    public function index($sts='')
    {
        $this->load->library('pagination');
        
        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Halaman';
        
        $config['base_url'] = base_url().$this->config->item('admpath').'/halaman';
        $config['total_rows'] = $this->cms->getCountHalaman();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 3;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['halaman_web'] = $this->cms->getAllHalaman($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
        $data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data['partial_content'] = 'v_halaman';

        $this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
    }
    
    public function tambah($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['aktif_mod'] = $this->cms->getModule();
        $data['aktif_album'] = $this->cms->getAlbumSelect();
        $data['aktif_video'] = $this->cms->getVideoSelect();
        $data['aktif_prod'] = $this->mproduk->getListAktifProdukInduk();
        $data['nama_menu'] = $this->cms->getTitleMenu();
        $data['super_admin'] = $this->super_admin;
        $data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorBagus($id='isi_id'),$this->editorBagus($id='isi_en'));

        if ($this->super_admin && bs_kode($this->session->userdata('level'), TRUE) == 'superman')
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_halaman',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_tabel()
    {
        $this->load->library('pagination');
        
        $config['base_url'] = site_url($this->config->item('admpath').'/halaman/update_tabel');
        $config['total_rows'] = $this->cms->getCountHalaman();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['halaman_web'] = $this->cms->getAllHalaman($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
        $data['hak'] = $this->hak;
        $data['sts'] = '';

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_halaman',$data);
		}else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }
    
    public function add_halaman()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('nama_id', 'Label Indonesia', 'trim_judul|required|xss_clean|callback__halaman_cek_id');
        $this->form_validation->set_rules('nama_en', 'Label English', 'trim_judul|required|xss_clean|callback__halaman_cek_en');
        $this->form_validation->set_rules('tipe', 'Tipe Halaman', 'trim|required|xss_clean');
        $this->form_validation->set_rules('menu_parent', 'Parent Menu', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('status', 'Status Halaman', 'trim|required');

        if($this->input->post('tipe') == 'module')
        {
            $this->form_validation->set_rules('module_id', 'Module', 'trim|required|integer|max_length[11]|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'produk')
        {
            $this->form_validation->set_rules('tipe_prod', 'Tipe Produk', 'trim|required|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			
            if($this->input->post('tipe_prod') === 'by_kategori')
            $this->form_validation->set_rules('kategori', 'Kategori Produk', 'trim|required|xss_clean|callback__kat_cek');
            else
            $this->form_validation->set_rules('id_produk', 'Produk', 'trim|integer|max_length[11]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'blog')
        {
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
            $this->form_validation->set_rules('kategori_blog', 'Kategori Blog', 'trim|required|xss_clean|callback__kat_cek');
        }
        elseif($this->input->post('tipe') == 'album')
        {
            $this->form_validation->set_rules('tipe_album', 'Tipe Album', 'trim|required|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('isi_id', 'Isi Indonesia', 'prep_for_form|required');
			$this->form_validation->set_rules('isi_en', 'Isi English', 'prep_for_form|required');

            if($this->input->post('tipe_album') === 'by_kategori')
            $this->form_validation->set_rules('kategori_album', 'Kategori Album', 'trim|required|xss_clean|callback__kat_cek');
            else
            $this->form_validation->set_rules('id_album', 'Album', 'trim|integer|max_length[11]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'video')
        {
            $this->form_validation->set_rules('tipe_video', 'Tipe Video', 'trim|required|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('isi_id', 'Isi Indonesia', 'prep_for_form|required');
			$this->form_validation->set_rules('isi_en', 'Isi English', 'prep_for_form|required');

            if($this->input->post('tipe_video') === 'by_kategori')
            $this->form_validation->set_rules('kategori_video', 'Kategori Video', 'trim|required|xss_clean|callback__kat_cek');
            else
            $this->form_validation->set_rules('id_video', 'Video', 'trim|integer|max_length[11]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'halaman')
        {
			$this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('isi_id', 'Isi Indonesia', 'prep_for_form|trim|required');
			$this->form_validation->set_rules('isi_en', 'Isi English', 'prep_for_form|trim|required');
        }
        else {
            
        }
        
        if ($this->form_validation->run($this) == FALSE || bs_kode($this->session->userdata('level'), TRUE) != 'superman')
        {
            if($this->input->post('tipe') == 'module')
            $sts = 'error_module';
            elseif($this->input->post('tipe') == 'blog')
            $sts = 'error_blog';
            elseif($this->input->post('tipe') == 'album')
            $sts = 'error_album';
            elseif($this->input->post('tipe') == 'video')
	        $sts = 'error_video';
            elseif($this->input->post('tipe') == 'produk')
            $sts = 'error_produk';
            elseif($this->input->post('tipe') == 'halaman')
			$sts = 'error_halaman';
			else
			$sts = 'error_menu';

            $this->tambah($sts);
        }
        else {
            if( ! $this->cms->addHalaman())
            {
                $info = "Gagal Tambahkan Halaman baru " . $this->input->post('nama_id') . ", pastikan data yang Anda masukkan sudah sesuai/valid.";
				$this->tutup_dialog_gagal($info);
            }
            else {
                $info = "Sukses menambahkan Halaman baru " . $this->input->post('nama_id');
                $this->tutup_dialog($info);
            }
        }
    }
    
    public function detail($sts='',$tipe='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_halaman'));
        
        $data['halaman'] = $this->cms->getAllHalamanAdminById($id);
        
        //$data['tags'] = $this->cms->getTagsIdJsonByObjekId($id,'album');
        
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['aktif_mod'] = $this->cms->getModule();
        $data['sts'] = $sts;
        $data['super_admin'] = $this->super_admin;
        $data['hak'] = $this->hak;
        $data['tipe_halaman'] = (empty($tipe))?$data['halaman']->tipe:$tipe;
        $data['nama_menu'] = $this->cms->getTitleMenu();
        $data['aktif_album'] = $this->cms->getAlbumSelect();
        $data['aktif_video'] = $this->cms->getVideoSelect();
        $data['aktif_prod'] = $this->mproduk->getListAktifProdukInduk();
        $data = array_merge($data,$this->editorBagus($id='isi_id'),$this->editorBagus($id='isi_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_halaman',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }
    
    public function update_halaman()
    {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('halaman_id', 'ID Halaman', 'required|integer|xss_clean');
		$this->form_validation->set_rules('status', 'Status Halaman', 'required|max_length[3]|xss_clean');

		if ($this->form_validation->run($this) == FALSE || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('halaman_id') && $this->super_admin )
			{
				if( ! $this->cms->upHalamanStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Halaman..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
    }
    
    public function update_formhalaman()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('nama_id', 'Label Indonesia', 'trim_judul|max_length[100]|required|xss_clean|callback__update_halaman_id_cek');
        $this->form_validation->set_rules('nama_en', 'Label English', 'trim_judul|max_length[100]|required|xss_clean|callback__update_halaman_en_cek');
        $this->form_validation->set_rules('tipe', 'Tipe Halaman', 'trim|required|xss_clean');

        if($this->input->post('tipe') == 'module')
        {
            $this->form_validation->set_rules('module_id', 'Module', 'trim|integer|max_length[11]|xss_clean');
            
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'produk')
        {
            $this->form_validation->set_rules('tipe_prod', 'Tipe Produk', 'trim|required|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			
            if($this->input->post('tipe_prod') === 'by_kategori')
            $this->form_validation->set_rules('kategori', 'Kategori Produk', 'trim|required|xss_clean|callback__kat_cek');
            else
            $this->form_validation->set_rules('id_produk', 'Produk', 'trim|integer|max_length[11]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'blog')
        {
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');

            $this->form_validation->set_rules('kategori_blog', 'Kategori Blog', 'trim|required|xss_clean|callback__kat_cek');
        }
        elseif($this->input->post('tipe') == 'album')
        {
            $this->form_validation->set_rules('tipe_album', 'Tipe Album', 'trim|required|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('isi_id', 'Isi Indonesia', 'prep_for_form|required');
			$this->form_validation->set_rules('isi_en', 'Isi English', 'prep_for_form|required');
			
            if($this->input->post('tipe_album') === 'by_kategori')
            $this->form_validation->set_rules('kategori_album', 'Kategori Album', 'trim|required|xss_clean|callback__kat_cek');
            else
            $this->form_validation->set_rules('id_album', 'Album', 'trim|integer|max_length[11]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'video')
        {
            $this->form_validation->set_rules('tipe_video', 'Tipe Video', 'trim|required|xss_clean');
            $this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('isi_id', 'Isi Indonesia', 'prep_for_form|required');
			$this->form_validation->set_rules('isi_en', 'Isi English', 'prep_for_form|required');
			
            if($this->input->post('tipe_video') === 'by_kategori')
            $this->form_validation->set_rules('kategori_video', 'Kategori Video', 'trim|required|xss_clean|callback__kat_cek');
            else
            $this->form_validation->set_rules('id_video', 'Video', 'trim|integer|max_length[11]|xss_clean');
        }
        elseif($this->input->post('tipe') == 'halaman')
        {
			$this->form_validation->set_rules('keyword_id', 'SEO Keyword Indonesia', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_id', 'SEO Description Indonesia', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('keyword_en', 'SEO Keyword English', 'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('description_en', 'SEO Description English', 'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('isi_id', 'Isi Indonesia', 'prep_for_form|required');
			$this->form_validation->set_rules('isi_en', 'Isi English', 'prep_for_form|required');
        }
        else {
            
        }
        
        if ($this->form_validation->run($this) == FALSE || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            if($this->input->post('tipe') == 'module')
            $sts = 'error_module';
            elseif($this->input->post('tipe') == 'blog')
            $sts = 'error_blog';
            elseif($this->input->post('tipe') == 'album')
            $sts = 'error_album';
            elseif($this->input->post('tipe') == 'video')
            $sts = 'error_video';
            elseif($this->input->post('tipe') == 'produk')
            $sts = 'error_produk';
            elseif($this->input->post('tipe') == 'halaman')
			$sts = 'error_halaman';
			else
			$sts = 'error_menu';
			
            $this->detail($sts);
        }
        else {
            if( ! $this->cms->upHalamanForm())
            {
                $info = "Gagal update data halaman " . $this->input->post('nama_id') . ", pastikan data yang Anda masukkan sudah sesuai/valid.";
				$this->tutup_dialog_gagal($info);
            }
            else {
				if( ! $this->cms->upNamaMenu())
				{
					$info = "Gagal merubah nama menu secara auto, Anda bisa merubahnya secara manual melalui pengaturan menu";
					$this->tutup_dialog_gagal($info);
				}
				else {
					$info = "Sukses update data halaman " . $this->input->post('nama_id');
					$this->tutup_dialog($info);
                }
            }
        }
    }

    function _kat_cek($str)
    {
        if (count(json_decode($str), true) == 0)
        {
            $this->form_validation->set_message('_kat_cek', 'Kategori tidak boleh kosong.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _halaman_cek_id($str)
    {
        if ($this->cms->adaHalaman(underscore($str)) == FALSE)
        {
            $this->form_validation->set_message('_halaman_cek_id', 'Halaman Indonesia dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _halaman_cek_en($str)
    {
        if ($this->cms->adaHalaman(underscore($str),'en') == FALSE)
        {
            $this->form_validation->set_message('_halaman_cek_en', 'Halaman English dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_halaman_id_cek($str)
    {
        if ($this->cms->adaUpdateHalaman(underscore($str),$this->input->post('nama_ori_id'),'id') == FALSE)
        {
            $this->form_validation->set_message('_update_halaman_id_cek', 'Halaman ID dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function _update_halaman_en_cek($str)
    {
        if ($this->cms->adaUpdateHalaman(underscore($str),$this->input->post('nama_ori_en'),'en') == FALSE)
        {
            $this->form_validation->set_message('_update_halaman_en_cek', 'Halaman EN dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

}

/* End of file halaman.php */
/* Location: ./application/controllers/admin/halaman.php */

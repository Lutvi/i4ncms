<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_user extends AdminController {
	
	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');
		
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Log';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$config = $this->_adminPagination();
		
		$config_err['base_url'] = base_url().$this->config->item('admpath').'/log_user/log_err';
		$config_err['total_rows'] = $this->logs->getCountLog();
		$config_err['per_page'] = $this->adm_per_halaman;
		$config_err['uri_segment'] = $this->_getSegment('log_err');
		$config_err = array_merge($config_err, $config);

		$config_usr['base_url'] = base_url().$this->config->item('admpath').'/log_user/log_usr';
		$config_usr['total_rows'] = $this->logs->getCountLogUser();
		$config_usr['per_page'] = $this->adm_per_halaman;
		$config_usr['uri_segment'] = $this->_getSegment('log_usr');
        $config_usr = array_merge($config_usr, $config);

		$config_robot['base_url'] = base_url().$this->config->item('admpath').'/log_user/log_bot';
		$config_robot['total_rows'] = $this->logs->getCountLogRobot();
		$config_robot['per_page'] = $this->adm_per_halaman;
		$config_robot['uri_segment'] = $this->_getSegment('log_bot');
        $config_robot = array_merge($config_robot, $config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset_err = $this->getOffsetPage($this->uri->segment($config_err['uri_segment']), $config_err['per_page']);
            $offset_usr = $this->getOffsetPage($this->uri->segment($config_usr['uri_segment']), $config_usr['per_page']);
            $offset_robot = $this->getOffsetPage($this->uri->segment($config_robot['uri_segment']), $config_robot['per_page']);
        }
        else{
            $offset_err = $this->uri->segment($config_err['uri_segment'],0);
            $offset_usr = $this->uri->segment($config_usr['uri_segment'],0);
            $offset_robot = $this->uri->segment($config_robot['uri_segment'],0);
        }
        
		$data['logging'] = $this->logs->lihatLog($config_err['per_page'],$offset_err);
		$data['log_user'] = $this->logs->lihatLogUser($config_usr['per_page'],$offset_usr);
		$data['log_robot'] = $this->logs->lihatLogRobot($config_robot['per_page'],$offset_robot);
		
		$this->pagination->initialize($config_err);
		$data['page'] =  $this->pagination->create_links();

		$this->pagination->initialize($config_usr);
		$data['user'] =  $this->pagination->create_links();

		$this->pagination->initialize($config_robot);
		$data['robot'] =  $this->pagination->create_links();

		$data['partial_content'] = 'v_log_user';
        
		/* Get Views */
		if( empty($sts) )
		{
			//$this->session->unset_userdata('aktif_tab_log');
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			return $data;
		}
	}

	public function update_tabel()
	{
		$data = $this->index($sts='ajax');
		$this->load->view('dynamic_js',$data);
		$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_log_user',$data);
	}

	public function log_err()
	{
		$data = $this->index($sts='log_err');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function log_usr()
	{
		$data = $this->index($sts='log_usr');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function log_bot()
	{
		$data = $this->index($sts='log_bot');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function cek_tab()
	{
		$aktif = $this->input->get('aktif_tab_log');
		$this->session->set_userdata('aktif_tab_log', $aktif);

		echo $aktif;
	}

	function selektif_hapus_errlog()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('err_log[]', 'ID Log', 'required|integer|max_length[11]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
        	echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('err_log', TRUE) && $this->_isSupAdmin() )
			{
				foreach($_POST['err_log'] as $item)
				{
					if( ! $this->logs->hapusLog($item) || ! $this->_isSupAdmin() )
					{
						echo 'Gagal Hapus Log,<br>Data ini masih dibutuhkan oleh sistem..!!';
						break;
					}
				}
				echo 'sukses';
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
	}
	
	function detail()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$id = $this->uri->segment(4,0);
			$info = $this->logs->detailLog($id);
	
			print_r('TIPE : <span style="color:orange">'.$info->tipe_error.'</span>');
			print_r('<br>');
			print_r('IP : <span style="color:red">'.$info->ip_port.'</span>');
			print_r('<br>');
			print_r('Browser : <span style="color:#999999">'.$info->user_agent.'</span>');
			print_r('<br>');
			print_r('URL : <span style="color:blue">'.$info->url.'</span>');
			print_r('<br>');
			print('DATA : <small><pre>'.print_r(unserialize($info->data), TRUE).'</pre></small>');
			print_r('<hr>');
			print_r('<span style="color:brown">'.$info->ket.'</span>');
		}
		else {
			$konten['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$konten);
        }
	}

	function detail_user()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$id = $this->uri->segment(4,0);
			$info = $this->logs->detailLogUser($id);

			print_r('IP : <span style="color:#E41867">'.$info->ip. '</span> : <span style="color:#FF500B">' .$info->port. '</span> <span style="color:FFA500">('.$info->hits.')</span>');
			print_r('<br>');
			print_r('Host : <span style="color:#1E6CB7">'.$info->host.'</span>');
			print_r('<br>');
			print_r('Browser : <span style="color:#A52A2A">'.$info->browser.'</span>');
			print_r('<br>');
			print_r('Lokasi Akses : <span style="color:#1E90FF">'.$info->lokasi.'</span>');
			print_r('<br>');
			print_r('Halaman : <span style="color:#1E90FF">'.$info->tipe_halaman.'</span>');
			print_r('<br>');
			print_r('URL Asal : <span style="color:#31A131">'.$info->url_asal.'</span>');
			print_r('<br>');
			print_r('URL Target : <span style="color:#D33019">'.$info->url_target.'</span>');
			print_r('<br>');
			print_r('Detail Platform : <span style="color:#4D4D4D">'.$info->detail_platform.'</span>');
		}
		else {
			$konten['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$konten);
        }
	}

	function detail_robot()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$id = $this->uri->segment(4,0);
			$info = $this->logs->detailLogRobot($id);

			print_r('IP : <span style="color:E41867">'.$info->ip. '</span> : <span style="color:#FF500B">' .$info->port. '</span>');
			print_r('<br>');
			print_r('Halaman : <span style="color:#1E90FF">'.$info->tipe_halaman.'</span>');
			print_r('<br>');
			print_r('URL Asal : <span style="color:#31A131">'.$info->url_asal.'</span>');
			print_r('<br>');
			print_r('URL Target : <span style="color:#D33019">'.$info->url_target.'</span>');
			print_r('<br>');
			print_r('Detail Robot : <span style="color:#4D4D4D">'.$info->detail_robot.'</span>');
		}
		else {
			$konten['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$konten);
        }
	}

}

/* End of file log_user.php */
/* Location: ./application/controllers/admin/log_user.php */

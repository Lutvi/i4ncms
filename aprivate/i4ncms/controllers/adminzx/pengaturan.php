<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengaturan extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');

		$this->load->library('form_validation');
		$this->load->helper('file');
		$this->input->post(NULL, TRUE);
	}

	public function index()
	{
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Pengaturan Web';

		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['partial_content'] = 'v_pengaturan';
		$data['sts_web'] = $this->config->item('sts_web');
		$data['scure_web'] = ($this->config->item('pake_ssl') === FALSE)?0:1;
		$data['cache_web'] = ($this->config->item('cache_front') === FALSE)?0:1;
		$data['sa_web'] = ($this->config->item('super_aktif') === FALSE)?0:1;
		$data['iklan'] = ($this->config->item('iklan') === FALSE)?0:1;
		$data['file_cache'] = $this->_cekbesar('./_tmp/web/');
		$data['file_tmp'] = $this->_cekbesar('./_tmp/');

		$data['ststoko'] = ($this->config->item('toko_aktif') === FALSE)?0:1;
		$data['cekstok'] = ($this->config->item('cek_stok_front') === FALSE)?0:1;

		//banner
        $data['hbanner'] = $this->web->getAllHbanner();

        //backup
        $data['hist_backup'] = $this->bckp->geAlltHistoryBackup();
        $data['jml_backup'] = $this->bckp->getCountHistoryBackupByTgl();
        $data['jml_all_backup'] = $this->bckp->getCountAllBackup();
        $data['sts_backup'] = $this->bckp->getStsBackupByTgl();
        

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function ubah_status_web()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('stweb', 'Status', 'required|integer|less_than[4]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status website <br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'sts_web';
				$nilai = $this->input->post('stweb');
				$tipe ='angka';
				$ket = 'Tentukan status website opsi, 1=Offline, 2=Perbaikan, 3=Online';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status website <br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_status_scure_web()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('scureweb', 'Status SSL', 'required|integer|less_than[2]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status HTTPS website <br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'pake_ssl';
				$nilai = $this->input->post('scureweb');
				$tipe ='boolean';
				$ket = 'Tentukan status https opsi, TRUE=HTTPS, FALSE=HTTP';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status HTTPS website <br> Ada kesalahan pada data.!';
				}
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_status_cache_web()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('cacheweb', 'Status Cache', 'required|integer|less_than[2]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status CACHE website <br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'cache_front';
				$nilai = $this->input->post('cacheweb');
				$tipe ='boolean';
				$ket = 'Tentukan status cache opsi, TRUE=On, FALSE=Off';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status CACHE website <br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function bersihkan_file_cache()
	{
		if(delete_files('./_tmp/web/', TRUE) && delete_files('./_tmp/tbl-cache/', TRUE))
		echo 'sukses';
		else
		echo 'Gagal membersihkan file Cache, cobalah beberapa saat lagi.';
	}

	public function ubah_status_super_admin_web()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('saweb', 'Status Proteksi', 'required|integer|less_than[2]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status Proteksi Level Admin<br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'super_aktif';
				$nilai = $this->input->post('saweb');
				$tipe ='boolean';
				$ket = 'Tentukan status Proteksi Level Admin, TRUE=On, FALSE=Off';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status Proteksi Level Admin<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_status_iklan_web()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('iklan', 'Status Iklan', 'required|integer|less_than[2]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status Iklan website<br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'iklan';
				$nilai = $this->input->post('iklan');
				$tipe ='boolean';
				$ket = 'Tentukan status Iklan pada website, TRUE=On, FALSE=Off';
	
				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status Iklan website<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_identitas_web()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('w-namaweb', 'Nama website', 'required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('w-slogan', 'Slogan website', 'required|max_length[100]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$tipe ='string';

			$nama_web = 'site_name';
			$nilai_web = $this->input->post('w-namaweb');
			$ket_web = 'Nama untuk website ini.';
			
			$nama_des = 'sort_site_description';
			$nilai_des = $this->input->post('w-slogan');
			$ket_des = 'Slogan atau Deskripsi singkat untuk website ini.';
			
			if( ! $this->super_admin || empty($nilai_web) || empty($nilai_des) )
			{
				echo 'Gagal merubah status website <br> Anda tidak memiliki akses.!';
			}
			else {
				if ( ! $this->web->simpanConfig($nama_web,$nilai_web,$tipe,$ket_web) || ! $this->web->simpanConfig($nama_des,$nilai_des,$tipe,$ket_des) )
				{
					if ( ! $this->web->simpanConfig($nama_web,$nilai_web,$tipe,$ket_web) )
					echo 'Gagal merubah Nama Website website <br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_des,$nilai_des,$tipe,$ket_des) )
					echo 'Gagal merubah Slogan website <br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_robot()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('robot', 'robots.txt', 'required|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$this->load->helper('file');
			
			$data = $this->input->post('robot');
			
			if( ! $this->super_admin || empty($data) )
			{
				echo 'Gagal merubah isi file robots.txt <br> Anda tidak memiliki akses.!';
			}
			else {
				if ( ! write_file('./robots.txt', $data) )
				{
					 echo 'Gagal akses file, Gagal merubah isi file robot.txt';
				}
				else {
					 echo 'sukses';
				}
			}
		}
	}

	public function ubah_sosmed()
	{
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('sosmed[nm][]', 'Nama SosMed', 'xss_clean|underscore');
        $this->form_validation->set_rules('sosmed[url][]', 'URL SosMed', 'xss_clean|prep_url');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$sosmed = $this->input->post('sosmed');
			$c = array_combine($sosmed['nm'], $sosmed['url']);
			foreach($c as $key => $val){
			if(!empty($key) && !empty($val))
			$cm[$key] = $val;
			}

			$nama = 'sosmed_url';
			$nilai = serialize($cm);
			$tipe ='serialize';
			$ket = 'URL Akun Sosial Media untuk website ini.';

			if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
			{
				echo 'Gagal merubah URL Akun Sosial Media<br> Ada kesalahan pada data.!';
			} 
			else {
				echo 'sukses';
				$this->_up_pengaturan_web();
			}
        }
	}

	public function ubah_metav()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');
        $this->form_validation->set_rules('metav', 'metav website', 'trim|required|prep_for_form');

		if ($this->form_validation->run($this) == FALSE)
		{
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
		}
		else {
			$this->load->helper('file');
			$data = $this->input->post('metav');
			if( ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			{
				echo 'Gagal merubah isi file<br> Anda tidak memiliki akses.!';
			}
			else {
				if ( ! write_file('./conf/metaverify.txt', $data) )
				{
					 echo 'Gagal akses file, Gagal merubah isi file';
				}
				else {
					echo 'sukses';
				}
			}
		}
	}

	public function ubah_seo()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('w-keyword-id', 'keyword website Indonesia', 'required|max_length[150]|xss_clean');
        $this->form_validation->set_rules('w-description-id', 'Meta Description website Indonesia', 'required|max_length[300]|xss_clean');
        $this->form_validation->set_rules('w-keyword-en', 'keyword website English', 'required|max_length[150]|xss_clean');
        $this->form_validation->set_rules('w-description-en', 'Meta Description website English', 'required|max_length[300]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$tipe ='string';

			$nama_key_id = 'site_keyword_id';
			$nilai_key_id = $this->input->post('w-keyword-id');
			$ket_key_id = 'Meta Keyword Indonesia untuk website ini.';

			$nama_des_id = 'site_description_id';
			$nilai_des_id = $this->input->post('w-description-id');
			$ket_des_id = 'Meta Description Indonesia untuk website ini.';

			$nama_key_en = 'site_keyword_en';
			$nilai_key_en = $this->input->post('w-keyword-en');
			$ket_key_en = 'Meta Keyword English untuk website ini.';

			$nama_des_en = 'site_description_en';
			$nilai_des_en = $this->input->post('w-description-en');
			$ket_des_en = 'Meta Description English untuk website ini.';

			if( ! $this->super_admin || empty($nilai_key_id) || empty($nilai_des_id) || empty($nilai_key_en) || empty($nilai_des_en) )
			{
				echo 'Gagal merubah Meta website <br> Anda tidak memiliki akses.!';
			}
			else {
				if ( ! $this->web->simpanConfig($nama_key_id,$nilai_key_id,$tipe,$ket_key_id) || ! $this->web->simpanConfig($nama_des_id,$nilai_des_id,$tipe,$ket_des_id) || ! $this->web->simpanConfig($nama_key_en,$nilai_key_en,$tipe,$ket_key_en) || ! $this->web->simpanConfig($nama_des_en,$nilai_des_en,$tipe,$ket_des_en) )
				{
					if ( ! $this->web->simpanConfig($nama_key_id,$nilai_key_id,$tipe,$ket_key_id) )
					echo 'Gagal merubah Meta Keyword Indonesia website <br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_des_id,$nilai_des_id,$tipe,$ket_des_id) )
					echo 'Gagal merubah Description Indonesia website <br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_key_en,$nilai_key_en,$tipe,$ket_key_en) )
					echo 'Gagal merubah Meta Keyword English website <br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_des_en,$nilai_des_en,$tipe,$ket_des_en) )
					echo 'Gagal merubah Description English website <br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

/* Toko ===================================================================== */
	public function ubah_status_toko()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('toko', 'Status Toko Online', 'required|integer|less_than[2]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status Iklan website<br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'toko_aktif';
				$nilai = $this->input->post('toko');
				$tipe ='boolean';
				$ket = 'Tentukan status Toko Online pada website, TRUE=On, FALSE=Off';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status Toko Online<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_cek_stok()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('cekstok', 'Status Cek Stok', 'required|integer|less_than[2]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah status Iklan website<br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'cek_stok_front';
				$nilai = $this->input->post('cekstok');
				$tipe ='boolean';
				$ket = 'Tentukan status Cek Stok Produk pada frontend, TRUE=On, FALSE=Off';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah status Cek Stok Produk<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_ppn()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('ppn', 'PPN Transaksi', 'required|decimal|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( ! $this->super_admin )
			{
				echo 'Gagal merubah PPN Transaksi<br> Anda tidak memiliki akses.!';
			}
			else {
				$nama = 'ppn';
				$nilai = $this->input->post('ppn');
				$tipe = 'string';
				$ket = 'Tentukan PPN Transaksi';

				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah PPN Transaksi<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function satuan_jenis_produk()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('satjen', 'Satuan Jenis', 'required|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$nama = 'satuan_jenis_produk';
			$nilai = $this->input->post('satjen');
			$tipe ='string';
			$ket = 'Nama satuan untuk jenis produk';
			
			if( ! $this->super_admin || $this->config->item('satuan_jenis_produk') === $this->input->post('satjen') )
			{
				if ( ! $this->super_admin )
				echo 'gagal';
				if ( $this->config->item('satuan_jenis_produk') === $this->input->post('satjen') )
				echo 'Nilai yang Anda masukkan sama <br> Data tidak berubah.!';
			}
			else {
				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah Satuan Jenis Produk<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function satuan_produk()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('satprod', 'Satuan Produk', 'required|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$nama = 'satuan_produk';
			$nilai = $this->input->post('satprod');
			$tipe ='string';
			$ket = 'Nama satuan untuk produk';
			
			if( ! $this->super_admin || $this->config->item('satuan_produk') === $this->input->post('satprod') )
			{
				if ( ! $this->super_admin )
				echo 'gagal';
				if ( $this->config->item('satuan_produk') === $this->input->post('satprod') )
				echo 'Nilai yang Anda masukkan sama <br> Data tidak berubah.!';
			}
			else {
				if ( ! $this->web->simpanConfig($nama,$nilai,$tipe,$ket) )
				{
					echo 'Gagal merubah Satuan Produk<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}

	public function ubah_cs()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('namacs', 'Nama Devis CS', 'required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('emailcs', 'alamat Email CS', 'required|valid_email|xss_clean');
        $this->form_validation->set_rules('tlpcs', 'No.Telpon CS', 'required|xss_clean');
        $this->form_validation->set_rules('smscs', 'SMS CS', 'required|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$tipe ='string';

			$nama_namacs = 'cs_name';
			$nilai_namacs = $this->input->post('namacs');
			$ket_namacs = 'Nama Devisi untuk CS.';

			$nama_emailcs = 'cs_email';
			$nilai_emailcs = $this->input->post('emailcs');
			$ket_emailcs = 'Alamat Email untuk CS.';

			$nama_tlpcs = 'cs_telpon';
			$nilai_tlpcs = $this->input->post('tlpcs');
			$ket_tlpcs = 'Layanan No.Telpon untuk CS.';

			$nama_smscs = 'cs_sms';
			$nilai_smscs = $this->input->post('smscs');
			$ket_smscs = 'Layanan SMS untuk CS.';
			
			if( ! $this->super_admin || empty($nilai_namacs) || empty($nilai_emailcs) || empty($nilai_tlpcs) || empty($nilai_smscs))
			{
				echo 'Gagal merubah Detail CS<br> Anda tidak memiliki akses.!';
			}
			else {
				if ( ! $this->web->simpanConfig($nama_namacs,$nilai_namacs,$tipe,$ket_namacs) || ! $this->web->simpanConfig($nama_emailcs,$nilai_emailcs,$tipe,$ket_emailcs) || ! $this->web->simpanConfig($nama_tlpcs,$nilai_tlpcs,$tipe,$ket_tlpcs) || ! $this->web->simpanConfig($nama_smscs,$nilai_smscs,$tipe,$ket_smscs))
				{
					if ( ! $this->web->simpanConfig($nama_namacs,$nilai_namacs,$tipe,$ket_namacs))
					echo 'Gagal merubah Nama CS<br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_emailcs,$nilai_emailcs,$tipe,$ket_emailcs))
					echo 'Gagal merubah Email CS<br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_tlpcs,$nilai_tlpcs,$tipe,$ket_tlpcs))
					echo 'Gagal merubah No.Telpon CS<br> Ada kesalahan pada data.!';
					if ( ! $this->web->simpanConfig($nama_smscs,$nilai_smscs,$tipe,$ket_smscs))
					echo 'Gagal merubah SMS CS<br> Ada kesalahan pada data.!';
				} 
				else {
					echo 'sukses';
					$this->_up_pengaturan_web();
				}
			}
		}
	}
/* End Toko ================================================================================================ */


	public function ubah_tema()
	{
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        $this->form_validation->set_rules('id-tema', 'ID Tema', 'required|alpha_dash|max_length[50]|xss_clean');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			$tema_id = $this->input->post('id-tema');
			if( ! $this->super_admin || empty($tema_id) )
			{
				echo 'Gagal merubah tema website <br> Anda tidak memiliki akses.!';
			}
			else {
				if ( ! $this->web->upTemaWeb($this->input->post('id-tema')) )
				{
					echo 'Gagal merubah Tema website <br> Ada kesalahan pada server.!';
				} 
				else {
					echo 'sukses';
					$this->_up_temaweb();
				}
			}
		}
	}

	public function update_list_tema()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$data['login'] = $this->login;
			$data['super_admin'] = $this->super_admin;
			$data['hak'] = $this->hak;
			$data['partial_content'] = 'pengaturan';
			$data['sts_web'] = $this->config->item('sts_web');
			$data['scure_web'] = ($this->config->item('pake_ssl') === FALSE)?0:1;
			$data['cache_web'] = ($this->config->item('cache_front') === FALSE)?0:1;
			$data['sa_web'] = ($this->config->item('super_aktif') === FALSE)?0:1;
			$data['file_cache'] = $this->_cekbesar('./_tmp/web/');

			$this->load->view($this->config->item('admin_theme_id').'/ajax/list_tema',$data);
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function opsi_tema()
	{
		echo $this->config->item('opsi_conf_tema');
	}

	public function atur_opsi_tema()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo form_open($this->config->item('admpath').'/pengaturan/simpan_opsi_tema');
			echo form_fieldset('Opsi pengaturan tema');
			foreach( $this->config->item('opsi_conf_tema') as $key => $val )
			{
				echo form_label(ucwords($key), $key);
				echo br();
				$vals = array_combine($val,$val);
				echo form_dropdown($key, $vals, $this->_isi_opsi_tema($key)).$this->_cek_warna($this->_isi_opsi_tema($key));
				echo '<p style="padding:0; margin:8px auto;">';
				foreach($val as $item)
				{
					echo $this->_cek_warna($item);
				}
				echo '</p>';
			}

			echo br();
			echo form_submit('mysubmit', 'Submit');
			echo form_fieldset_close();
			echo form_close();
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}
	
	function _isi_opsi_tema($key)
	{
		foreach( $this->config->item('nilai_opsi_tema') as $nilai => $val  )
		{
			if( $nilai == $key )
			$isi = array_values($val);
		}
		return $isi;
	}
	
	function _cek_warna($isi)
	{
		if(is_array($isi))
		{
		foreach ($isi as $warna )
		//Check for a hex color string '#c1c2b4'
			if(preg_match('/^#[a-f0-9]{6}$/i', $warna)) //hex color is valid
			{
				$out = '<span style="line-height:10px; width:10px; margin-left:3px; background:'.$warna.'">&nbsp;&nbsp;</span>';
			}
			else{
				$out = '';
			} 
		}
		else {
			if(preg_match('/^#[a-f0-9]{6}$/i', $isi)) //hex color is valid
			{
				$out = '<span title="'.$isi.'" style="cursor:pointer; line-height:10px; width:10px; margin-left:3px; background:'.$isi.'">&nbsp;&nbsp;</span>';
			}
			else{
				$out = '';
			}
		} 
		
		//Check for a hex color string without hash 'c1c2b4'
		/*else if(preg_match('/^[a-f0-9]{6}$/i', $warna)) //hex color is valid
		{
		      $fix_color = '#' . $color;
		}*/
		
		return $out;
	}
	
	public function simpan_opsi_tema()
	{
		$buang = array_pop($_POST);
		$out = array();
		foreach( $_POST as $key => $val )
		{
			$set =  array($key => array($val));
			$out = array_merge($out,$set);
		}
		//print_r(json_encode($out));
		$nilai = json_encode($out);
		if( ! $this->web->upOpsiTema($nilai) )
		{
			$info = 'Gagal merubah opsi untuk tema.';
			$this->tutup_dialog_gagal($info);
		}
		else {
			$info = 'Sukses, Opsi tema berhasil diubah.<br><br>Silahkan tutup jendela ini.';
			$this->tutup_dialog($info,2000);
			$this->_up_temaweb();
		}
	}
	
	public function install_tema()
	{
		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo form_open_multipart($this->config->item('admpath').'/pengaturan/upload_tema');
			echo '<p><input type="file" name="userfile" />';
			echo '<br /><small>Pilih paket installer, lalu tekan submit.</small></p>';
			//echo '<p>Aktifkan tema : <input type="checkbox" name="aktif" value="1" /><br /><small>Ceklist ini untuk aktifkan tema otomatis.</small></p>';
			//if ( $this->super_admin === TRUE )
			//echo '<p>Fresh Install : <input type="checkbox" name="fresh" value="1" /><br /><small>Ceklist ini untuk update tema yang lama.</small></p>';
			echo form_submit('mysubmit', 'Submit');
			echo form_close();
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function upload_tema()
	{
		if ( $this->do_upload() )
		{
			$fol = $this->upload->data();
			$fol = $fol['raw_name'];

			$this->load->library('unzip');
			$this->unzip->extract('./_tmp/bak/'.$this->upload->file_name, './_tmp/install/');
			
			if ( file_exists("./_tmp/install/". $fol ."/install.xml") ) {
			
				$fol_install = "./_tmp/install/" . $fol;
				$xml = simplexml_load_file($fol_install ."/install.xml");
				
				$new = array();
				$buat = array();
				$cek = $xml->getName();
				if ( $cek == 'tema' )
				{
					foreach ( $xml->children() as $child )
					{
					   $new[$child->getName()] = htmlspecialchars_decode($child);
					}
					
					if ( (count($new) == 10 || count($new) == 12) && ! empty($new['nama_tema']) )
					{
						if( $new['versi'] === $this->config->item('cms_version') )
						{
							$new['nama_tema'] = underscore($new['nama_tema']);
							
							// convert json opsi_conf
							if ( isset($new['opsi_conf']) )
							{
								$opsi_conf = (array)$xml->opsi_conf->children();
								foreach ( $opsi_conf as $key => $opsi )
								{
									$opc[$key] = explode("," , $opsi);
								}
								$new['opsi_conf'] = json_encode($opc);
							}
							if ( isset($new['nilai_opsi']) )
							{
								$nilai_opsi = (array)$xml->nilai_opsi->children();
								foreach ( $nilai_opsi as $key => $nopsi )
								{
									$nopc[$key] = explode("," , $nopsi);
								}
								$new['nilai_opsi'] = json_encode($nopc);
							}

							$this->_simpan_tema($fol_install, $new); 
						}
						else {
							echo "Perintah installer gagal diproses.!<br />Versi CMS tidak sesuai.";
							delete_files('./_tmp/install/', TRUE);
							$this->install_tema();
						}
					}
					else {
						echo "Perintah installer tema tidak valid.!";
						delete_files('./_tmp/install/', TRUE);
						$this->install_tema();
					}
				}
				else {
					echo "Paket(.zip) installer tema tidak valid.!";
					delete_files('./_tmp/install/', TRUE);
					$this->install_tema();
				}
			}
			else {
				echo "Paket(.zip) installer corrupt/rusak.!";
				$this->install_tema();
			}
		}
		else {
			echo "<strong>Gagal Upload.!</strong> <br />".$this->upload->display_errors();
			$this->install_tema();
		}
	}
	
	function do_upload()
	{
		$this->load->helper('file');
		
		$pins = './_tmp/bak/';
		if ( $this->_cekbesar($pins) > $this->config->item('max_tmp_install') )
		{
			delete_files($pins, TRUE);
		}
		delete_files('./_tmp/install/', TRUE);
		
		$config['upload_path'] = './_tmp/bak/';
		$config['allowed_types'] = 'zip';
		$config['max_size']	= '2048';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function _simpan_tema($fol_install, $new)
	{
		$this->load->helper('directory');
		$old = umask(002);
		$info = '';

		if( ! $this->web->addTemaWeb($new) )
		{
			echo "<p>Tema <strong>" . $new['nama_tema'] . "</strong> sudah terpasang.<p/>";
			$this->install_tema();
		}
		else {
			$info .= $this->_tema_file($fol_install, $new);
			$info .= $this->_assets_file($fol_install, $new);
			$info .= $this->_massets_file($fol_install, $new);
			$info .= $this->_scrtema_file($fol_install, $new);
			$info .= "<hr />Sukses, Tema <strong>" . $new['nama_tema'] . "</strong> telah terpasang";

			$this->tutup_dialog($info,8000);
		}

		delete_files('./_tmp/install/', TRUE);
		umask($old);

		if ($old != umask()) {
			die('An error occurred while changing back the umask.');
		}
	}
	
	function _tema_file($fol_install, $new)
	{
		$map = directory_map($fol_install.'/views',5);
		$nm = $new['nama_tema'];
		$info = '';

		foreach ($map as $key => $val )
		{
			if( $key === $nm )
				$info .= "<hr /><strong>File View :</strong><br />";
			elseif( $key === 'mobile_'.$nm )
				$info .= "<strong>File Mobile-View :</strong><br />";
			else
				$info .= "<hr /><strong>File Extra-View :</strong><br />";

			if ( is_array($val) )
			{
				foreach ( $val as $sub => $isinya )
				{
					if ( is_array($isinya) )
					{
						foreach ( $isinya as $partial => $item )
						{
							if ( ! is_array($item) )
							{
								$file = $fol_install ."/views/". $key . "/" . $sub . "/" . $item;
								$pindah = CMSPATH . $key . "/" . $sub . "/" . $item;

								if ( ! is_dir(CMSPATH . $key) )
									mkdir(CMSPATH . $key, DIR_CMS_MODE);
								
								if ( ! is_dir(CMSPATH . $key . "/" . $sub) )
									mkdir(CMSPATH . $key . "/" . $sub, DIR_CMS_MODE);

								if ( ! $this->_pindah_file($file, $pindah) )
									$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
								else
									$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
							}
							else {
								foreach($item as $vitems)
								{
									$file = $fol_install ."/views/". $key . "/" . $sub . "/" . $partial . "/" . $vitems;
									$pindah = CMSPATH . $key . "/" . $sub . "/" . $partial . "/" . $vitems;

									if ( ! is_dir(CMSPATH . $key . "/" . $sub . "/" . $partial) )
										mkdir(CMSPATH . $key . "/" . $sub . "/" . $partial, DIR_CMS_MODE);

									if ( ! $this->_pindah_file($file, $pindah) )
										$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
									else
										$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
								}
							}
						}
					}
					else {
						$file = $fol_install ."/views/". $key . "/" . $isinya;
						$pindah = CMSPATH . $key . "/" . $isinya;

						if ( ! is_dir(CMSPATH . $key) )
							mkdir(CMSPATH . $key, DIR_CMS_MODE);

						if ( ! $this->_pindah_file($file, $pindah) )
							$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
						else
							$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
					}
				}
			}
			else {
				$file = $fol_install ."/views/". $val;
				$pindah = CMSPATH . "/" . $val;

				if ( ! $this->_pindah_file($file, $pindah) )
					$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
				else
					$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
			}
		}

		return $info;
	}
	
	function _assets_file($fol_install, $new)
	{
		if(is_dir($fol_install.'/assets'))
		{
			$map = directory_map($fol_install.'/assets', FALSE, TRUE);
			$nm = $new['nama_tema'];
			$info = '';

			$info .= "<hr /><strong>Web-Assets Files</strong><br />";
			foreach ($map as $key => $val )
			{
				if ( ! is_dir("./assets/" . $nm) )
					mkdir("./assets/" . $nm, DIR_APP_MODE);
				$info .= "<strong>" . ucwords($key) . " Files :</strong><br />";
				foreach ($val as $item )
				{
					$file = $fol_install ."/assets/". $key . "/" . $item;
					$pindah = "./assets/" . $nm . "/" . $key . "/" . $item;

					if ( ! is_dir("./assets/" . $nm . "/" . $key) )
						mkdir("./assets/" . $nm . "/" . $key, DIR_CMS_MODE);

					if ( ! $this->_pindah_file($file, $pindah) )
						$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
				}
			}
		}
		else {
			$info = "Tidak ada file assets untuk tema ini.";
		}

		return $info;
	}
	
	function _massets_file($fol_install, $new)
	{
		if(is_dir($fol_install.'/m-assets'))
		{
			$map = directory_map($fol_install.'/m-assets', FALSE, TRUE);
			$nm = $new['nama_tema'];
			$info = '';

			$info .= "<hr /><strong>Mobile-Assets Files</strong><br />";
			foreach ($map as $key => $val )
			{
				if ( ! is_dir("./m-assets/" . $nm) )
					mkdir("./m-assets/" . $nm, DIR_APP_MODE);
				$info .= "<strong>" . ucwords($key) . " Files :</strong><br />";
				foreach ($val as $item )
				{
					$file = $fol_install ."/m-assets/". $key . "/" . $item;
					$pindah = "./m-assets/" . $nm . "/" . $key . "/" . $item;

					if ( ! is_dir("./m-assets/" . $nm . "/" . $key) )
						mkdir("./m-assets/" . $nm . "/" . $key, DIR_CMS_MODE);
					
					if ( ! $this->_pindah_file($file, $pindah) )
						$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
					else
						$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
				}
			}
		}
		else {
			$info = "Tidak ada file m-assets untuk tema ini.";
		}

		return $info;
	}
	
	function _scrtema_file($fol_install, $new)
	{
		$map = directory_map($fol_install.'/scr-tema', FALSE, TRUE);
		$nm = $new['nama_tema'];
		$info = '';
		
		$info .= "<hr /><strong>Screenshot tema</strong><br />";
		foreach ($map as $key => $val )
		{
			if ( ! is_dir("./assets/scr-tema") )
				mkdir("./assets/scr-tema", DIR_CMS_MODE);
			
			$file = $fol_install ."/scr-tema/". $val;
			$pindah = "./assets/scr-tema/". $val;
			
			if ( ! $this->_pindah_file($file, $pindah) )
				$info .= "[" . symbolic_permissions(fileperms($pindah)) . "] " . "$pindah tidak bisa terpasang.<br />";
			else
				$info .= "[" . symbolic_permissions(fileperms($pindah))  . "] " . "$pindah sukses terpasang.<br />";
		}

		return $info;
	}


	function _up_pengaturan_web()
	{
		$this->load->helper('file');
		$conf = $this->mweb->initConfigWeb();
		$data = '';
		$data .= "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n\n";

		foreach ($conf as $item)
		{
			// set tipe datanya
			if( $item['tipe_data'] === 'string' )
			$data .= '$config[\''.$item['nama_config'].'\'] = \''.$item['nilai_config'].'\';'."\n";
			if( $item['tipe_data'] === 'angka' )
			$data .= '$config[\''.$item['nama_config'].'\'] = '.(int)$item['nilai_config'].';'."\n";
			if( $item['tipe_data'] === 'desimal' )
			$data .= '$config[\''.$item['nama_config'].'\'] = '.abs($item['nilai_config']).';'."\n";
			if( $item['tipe_data'] === 'array' )
			$data .= '$config[\''.$item['nama_config'].'\'] = '.var_export(implode(",",$item['nilai_config']),TRUE).';'."\n";
			if( $item['tipe_data'] === 'boolean' )
			$data .= '$config[\''.$item['nama_config'].'\'] = '.var_export((boolean)$item['nilai_config'],TRUE).';'."\n";
			if( $item['tipe_data'] === 'json' )
			$data .= '$config[\''.$item['nama_config'].'\'] = '.var_export(json_encode($item['nilai_config']),TRUE).';'."\n";
			if( $item['tipe_data'] === 'serialize' ){
			$unserialized = str_replace("\r","",$item['nilai_config']);
			$data .= '$config[\''.$item['nama_config'].'\'] = '.var_export(unserialize($unserialized),TRUE).';'."\n";
			}
		}
		write_file(APPPATH.'config/pengaturan_web.php', $data, 'w+');
	}

	function _up_temaweb()
	{
		$this->load->helper('file');
		$tema = $this->mweb->getTemaWeb();
		$dtema = '';
		$dtema .= "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n\n";
		foreach ($tema as $item)
		{
			$dtema .= '$config[\'theme_id\'] = \''.$item['nama_tema'].'\';'."\n";
			$dtema .= '$config[\'is_respon\'] = '.var_export((boolean)$item['is_responsive'],TRUE).';'."\n";
			$dtema .= '$config[\'slider_menu\'] = '.var_export((boolean)$item['slider_menu'],TRUE).';'."\n";
			$dtema .= '$config[\'opsi_conf_tema\'] = '.var_export(json_decode($item['opsi_conf'], true, 5),TRUE).';'."\n";
			$dtema .= '$config[\'nilai_opsi_tema\'] = '.var_export(json_decode($item['nilai_opsi'], true),TRUE).';'."\n";
		}
		write_file(APPPATH.'config/temaweb.php', $dtema, 'w+');
	}

}

/* End of file pengaturan.php */
/* Location: ./application/controllers/pengaturan.php */

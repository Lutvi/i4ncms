<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Pelanggan extends AdminController {

	//private $pp = $this->adm_per_halaman;

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman','batman','spiderman');
	}

	public function index()
	{
		$this->load->library('pagination');

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Produk';

		$config['base_url'] = base_url().$this->config->item('admpath').'/pelanggan';
		$config['total_rows'] = $this->pelanggan->getCountAllPelanggan();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['pelanggan'] = $this->pelanggan->getAllPelanggan($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['partial_content'] = 'v_pelanggan';

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$this->load->library('pagination');
		
		$config['base_url'] = site_url($this->config->item('admpath').'/pelanggan/update_tabel');
		$config['total_rows'] = $this->pelanggan->getCountAllPelanggan();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
		}
		else{
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['pelanggan'] = $this->pelanggan->getAllPelanggan($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_pelanggan',$data);
		}
		else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function detail($sts='')
	{
		$pid = $this->uri->segment(4,$this->input->post('pid'));
		$data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
		$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
		$data['fck_conf'] = array('name' => 'isi_id','id' => 'iss','toolbarset' => 'Semi','width' => '100%','height' => '400px');
		$data['detail_pelanggan'] = $this->pelanggan->getPelangganById($pid);
		$data['alamat'] = $this->_buildAlamat($data['detail_pelanggan']);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$data['list_provinsi'] = $this->maddons->listProvinsi();
		$data['list_wilayah'] = $this->maddons->listKotaById($data['detail_pelanggan']->id_provinsi);

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/detail_pelanggan',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function update_detail()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('cust_id'))
		{
			$this->form_validation->set_rules('cust_id', 'ID Pelanggan', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[6]|xss_clean');
			$this->form_validation->set_rules('rss', 'Newsletter', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('prioritas', 'Prioritas', 'required|max_length[8]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Pelanggan', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
		}
        else {
			if ($this->input->post('cust_id') && $this->_isSupAdmin())
			{
				$cid = $this->input->post('cust_id');
				if ( ! $this->pelanggan->upStatPelangganById($cid))
					echo 'Gagal merubah status pelanggan.';
				else
					echo 'sukses';
			}
			elseif ($this->input->post('id_hps')  && $this->_isSupAdmin()) {
				$cid = $this->input->post('id_hps');
				if ( ! $this->pelanggan->hapusPelanggan($cid))
					echo 'Gagal hapus data pelanggan. <br>karena pelanggan masih memiliki order transaksi.!';
				else
					echo 'sukses';
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
	}

	public function update_detail_pelanggan()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$ubah_alamat = $this->input->post('status-ubah');

		$this->form_validation->set_rules('status-ubah', 'Ubah', 'trim|required');
		$this->form_validation->set_rules('pid', 'PID', 'trim|required');
		$this->form_validation->set_rules('uname_id', 'ID Pelanggan', 'trim|required');
		
		$this->form_validation->set_rules('status_rss', 'RSS', 'trim');
		$this->form_validation->set_rules('status_cust', 'Status', 'trim');
		$this->form_validation->set_rules('prioritas_cust', 'Prioritas', 'trim|required');

		if ($ubah_alamat === '1')
		{
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('no_telpon', 'No Telpon', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|integer|required');
			$this->form_validation->set_rules('negara', 'Negara', 'trim|required');
			$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required|integer');
			$this->form_validation->set_rules('wilayah', 'Wilayah', 'trim|required|integer');
			$this->form_validation->set_rules('alamat_rumah', 'Alamat Rumah', 'trim|required');
		}
		else {
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim');
			$this->form_validation->set_rules('no_telpon', 'No Telpon', 'trim');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
			$this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|integer');
			$this->form_validation->set_rules('negara', 'Negara', 'trim');
			$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|integer');
			$this->form_validation->set_rules('wilayah', 'Wilayah', 'trim|integer');
			$this->form_validation->set_rules('alamat_rumah', 'Alamat Rumah', 'trim');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->detail();
		}
		else {
			$pid = $this->input->post('pid');
			//print_r($pid);
			
			if ( ! $this->pelanggan->upDetailPelangganById($pid))
			{
				$info = 'Gagal memperbaharui data Pelanggan, telah terjadi kesalahan saat menyimpan data';
				$this->tutup_dialog_gagal($info);
			}
			else {
				$info = 'Sukses Data pelanggan berhasil diperbaharui';
				$this->tutup_dialog($info);
			}
		}
		
	}

}

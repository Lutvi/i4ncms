<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends AdminController {

    public function __construct()
    {
        parent::__construct();

        $this->_cekStsLoginAdm();
        $this->hak = array('superman','batman','spiderman');

        $this->load->config('email');
    }

    public function index()
    {
		$this->load->library('pagination');
        
        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Newsletter';
        
        $config['base_url'] = base_url().$this->config->item('admpath').'/newsletter';
        $config['total_rows'] = $this->subcribe->getCountNewsletter();
        $config['per_page'] = $this->adm_per_halaman; 
        $config['uri_segment'] = 3;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->subcribe->getAllUserRss($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['partial_content'] = 'v_newsletter';
                
        $this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
    }

    public function test_email()
    {
        $this->load->library('email');

		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('fun9uy5@gmail.com');
		$this->email->cc('cysca_unix@yahoo.co.id');
		
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		
		$this->email->send();
		
		echo $this->email->print_debugger();
    }

}

/* End of file newsletter.php */
/* Location: ./cms/controllers/admin/newsletter.php */

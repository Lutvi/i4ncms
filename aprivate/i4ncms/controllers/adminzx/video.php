<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video extends AdminController {

    public function __construct()
    {
        parent::__construct();

        $this->_cekStsLoginAdm();
        $this->hak = array('superman','batman','ironman');
        $this->load->spark('video_helper/1.0.6');
        $this->_hpsQryCache('video');
    }

    public function index($sts='')
    {
        $this->load->library('pagination');

        /*/- cek data jika kosong hapus semua foto -/*/
        $this->_kosong_folder();

        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Video';

        $config['base_url'] = base_url().$this->config->item('admpath').'/video';
        $config['total_rows'] = $this->cms->getCountVideo();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 3;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['video_web'] = $this->cms->getAllVideo($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data['partial_content'] = 'v_video';
        $data['jml_playlist'] = $this->cms->getCountPlaylist();

        if($data['jml_playlist'] > 0)
        $data['parent_video'] = $this->cms->getVideoSelect(TRUE);
        else
        $data['parent_video'] = $this->cms->getVideoSelect();

        $this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
    }

    public function update_tabel()
    {
        $this->load->library('pagination');
        
        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/video/update_tabel'));
        $config['total_rows'] = $this->cms->getCountVideo();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['video_web'] = $this->cms->getAllVideo($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        
        $data['jml_playlist'] = $this->cms->getCountPlaylist();
        if($data['jml_playlist'] > 0)
        $data['parent_video'] = $this->cms->getVideoSelect(TRUE);
        else
        $data['parent_video'] = $this->cms->getVideoSelect();

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_video',$data);
        }
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }
    
    public function tambah($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css','elem/magicsuggest-1.3.1.css');
        $data['kat_video'] = $this->cms->getKategori('video');
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_video',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_video()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__video_cek_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__video_cek_en');
        $this->form_validation->set_rules('utama', 'Tipe Video', 'required');
        $this->form_validation->set_rules('diskusi', 'Komentar Video', 'required');
        $this->form_validation->set_rules('kategori_video', 'Kategori Video', 'required|integer');
        //$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');
        $this->form_validation->set_rules('src_video', 'URL Video', 'required|prep_url|callback__is_valid_url');
        $this->form_validation->set_rules('ket_id', 'Deskripsi Indonesia', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Deskripsi English', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah($sts);
        }
        else {
			$tipe_video = $this->_getTipeVideo($this->input->post('src_video'));

			if ($tipe_video === 'youtube')
			$url = youtube_thumbs($this->input->post('src_video'), 1);
			else
			$url = vimeo_thumbs($this->input->post('src_video'), 1);

			$nama_file = time().'-'.basename($url);
			if( ! $this->_download_thumbs($url, $nama_file) || ! $this->_buat_thumbs($nama_file) || empty($tipe_video) || $url === FALSE)
			{
				$sts = 'error';
				$this->tambah($sts);
			}
			else {
				if( ! $this->cms->addVideo($nama_file,$tipe_video))
				{
					$info = "Gagal menambahkan konten Video baru";
					$this->tutup_dialog_gagal($info);
				}
				else {
					$info = "Sukses menambahkan konten Video baru";
                    $this->tutup_dialog($info);
				}
			}
        }
    }

    public function tambah_playlist($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        if($this->cms->getCountPlaylist() > 0)
        $data['parent_video'] = $this->cms->getVideoSelect(TRUE);
        else
        $data['parent_video'] = $this->cms->getVideoSelect();

        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_video_playlist',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function tambah_playlist_video($sts='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_video'));
        
        $data['video'] = $this->cms->getAllVideoAdminById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        $data['parent_video'] = $id;
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_video_playlist',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_playlist()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__playlist_cek');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__playlist_cek');
        $this->form_validation->set_rules('parent_video', 'Parent Video', 'trim|required');
        $this->form_validation->set_rules('src_video', 'URL Video', 'required|prep_url|callback__is_valid_url');
        $this->form_validation->set_rules('ket_id', 'Deskripsi Indonesia', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Deskripsi English', 'trim|required');
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah_playlist($sts);
        }
        else {
			$tipe_video = $this->_getTipeVideo($this->input->post('src_video'));

			if ($tipe_video === 'youtube')
			$url = youtube_thumbs($this->input->post('src_video'), 1);
			else
			$url = vimeo_thumbs($this->input->post('src_video'), 1);

			$nama_file = time().'-'.basename($url);
			if( ! $this->_download_thumbs($url, $nama_file) || ! $this->_buat_thumbs($nama_file) || empty($tipe_video))
			{
				$sts = 'error';
				$this->tambah_playlist($sts);
			}
			else {
				if( ! $this->cms->addPlaylist($nama_file,$tipe_video))
				{
					$sts = 'error';
					$this->tambah_playlist($sts);
				}
				else {
					$sts = "Sukses, menambahkan playlist baru.";
					$this->session->set_flashdata('info', $sts);
					redirect($this->config->item('admpath').'/video/tabel_playlist/'.$this->input->post('parent_video'),'refresh');
				}
			}
        }
    }

    public function add_playlist_video()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__playlist_cek_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__playlist_cek_en');
        $this->form_validation->set_rules('parent_video', 'Parent Video', 'trim|required');
        $this->form_validation->set_rules('src_video', 'URL Video', 'required|prep_url|callback__is_valid_url');
        $this->form_validation->set_rules('ket_id', 'Deskripsi Indonesia', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Deskripsi English', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah_playlist_video($sts);
        }
        else {
			$tipe_video = $this->_getTipeVideo($this->input->post('src_video'));

			if ($tipe_video === 'youtube')
			$url = youtube_thumbs($this->input->post('src_video'), 1);
			else
			$url = vimeo_thumbs($this->input->post('src_video'), 1);

			$nama_file = time().'-'.basename($url);
			if( ! $this->_download_thumbs($url, $nama_file) || ! $this->_buat_thumbs($nama_file) || empty($tipe_video))
			{
				$sts = 'Gagal mengambil gambar cover video.!';
				$this->session->set_flashdata('error', $sts);
				$this->tambah_playlist_video($sts);
			}
			else {
				if( ! $this->cms->addPlaylist($nama_file,$tipe_video))
				{
					$sts = 'Gagal Simpan ke Database.!';
					$this->session->set_flashdata('error', $sts);
					$this->tambah_playlist_video($sts);
				}
				else {
					$sts = "Sukses, menambahkan playlist baru.";
					$this->session->set_flashdata('info', $sts);
					redirect($this->config->item('admpath').'/video/tabel_playlist/'.$this->input->post('parent_video'),'refresh');
				}
			}
        }
    }

    public function tabel_playlist()
    {
        $this->load->library('pagination');
        $id = $this->uri->segment(4,$this->input->post('parent_video'));
        
        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/video/tabel_playlist/'.$id));
        $config['total_rows'] = $this->cms->getCountVideoPlaylist($id);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 5;
        $config = array_merge($config,$this->_adminPagination());
        
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['video_web'] = $this->cms->getAllVideoPlaylist($id,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data['title'] = 'Video Playlist';
        $data['id_video'] = $id;
        $data['nama_video'] = $this->cms->getNamaVideoPlaylistById($id);
        $data = array_merge($data, $this->_getAdminAssets());

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_playlist',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_tabel_playlist()
    {
        $this->load->library('pagination');
        $id = $this->uri->segment(4,$this->input->post('parent_video'));
        
        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/video/update_tabel_playlist/'.$id));
        $config['total_rows'] = $this->cms->getCountVideoPlaylist($id);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 5;
        $config = array_merge($config,$this->_adminPagination());
        
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['video_web'] = $this->cms->getAllVideoPlaylist($id,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data['title'] = 'Playlist Video';
        $data['id_video'] = $id;
        $data['nama_video'] = $this->cms->getNamaVideoPlaylistById($id);

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_playlist',$data);
        }
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail()
    {
        $id = $this->uri->segment(4,$this->input->post('id_video'));
        
        $data['video'] = $this->cms->getAllVideoAdminById($id);
        $data['tags'] = $this->cms->getTagsIdJsonByObjekId($id,'video');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['kat_video'] = $this->cms->getKategori('video');
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_video',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail_playlist()
    {
        $id = $this->uri->segment(4,$this->input->post('id_playlist'));
        
        $data['video'] = $this->cms->getVideoPlaylistAdminById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        $data['kat_video'] = $this->cms->getKategori('video');
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_playlist',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }


    public function update_video()
    {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('video_id'))
		{
			$this->form_validation->set_rules('video_id', 'ID Video', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('komentar', 'Komentar', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('utama', 'Utama', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Video', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('vdoid'))
		{
			$this->form_validation->set_rules('vdoid', 'ID Video', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('video_id') && $this->super_admin ) 
			{
				if( ! $this->cms->upVideoStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Video Cover..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->cms->hapusVideo() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Video Cover..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('vdoid') && $this->super_admin ) {
				if( ! $this->cms->upVideoPos() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Posisi Video..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
    }

    public function update_playlist()
    {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('video_id'))
		{
			$this->form_validation->set_rules('video_id', 'ID Video', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Video', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('vdoid'))
		{
			$this->form_validation->set_rules('vdoid', 'ID Video', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('video_id') && $this->super_admin ) 
			{
				if( ! $this->cms->upVideoPlaylistStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Playlist..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->cms->hapusVideoPlaylist() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Playlist..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('vdoid') && $this->super_admin ) {
				if( ! $this->cms->upVideoPlaylistPos() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Posisi Playlist..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
    }

    public function update_formvideo()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__update_video_cek_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__update_video_cek_en');

        $this->form_validation->set_rules('src_video', 'URL Video', 'required|prep_url|callback__is_valid_url');
        $this->form_validation->set_rules('kategori_video', 'Kategori Video', 'trim|required|integer');
        //$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');
        $this->form_validation->set_rules('ket_id', 'Keterangan ID', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Keterangan EN', 'trim|required');

        // hidden
        $this->form_validation->set_rules('nama_ori_id', 'Ori ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_ori_en', 'Ori EN', 'trim_judul|required');
        $this->form_validation->set_rules('id_video', 'ID', 'trim|required');
        $this->form_validation->set_rules('tag_ori', 'TAG', 'trim|required');
        $this->form_validation->set_rules('src_video_ori', 'SRC Asli', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail();
        }
        else {
			if ($this->input->post('src_video') === $this->input->post('src_video_ori'))
			{
				if ( ! $this->cms->upVideoForm() )
				{
					$info = "Gagal memperbaharui data <strong>Video ".$this->input->post('nama_id')."</strong>";
					$this->tutup_dialog_gagal($info);
				}
				else {
					$info = "Sukses memperbaharui data <strong>Video ".$this->input->post('nama_id')."</strong>";
					$this->tutup_dialog($info);
				}
			}
			else {
				$tipe_video = $this->_getTipeVideo($this->input->post('src_video'));
				if ($tipe_video === 'youtube')
				$url = youtube_thumbs($this->input->post('src_video'), 1);
				else
				$url = vimeo_thumbs($this->input->post('src_video'), 1);

				$nama_file = time().'-'.basename($url);
				if( ! $this->_download_thumbs($url, $nama_file) || ! $this->_buat_thumbs($nama_file) || empty($tipe_video))
				{
					$this->detail();
				}
				else {
					
					if( ! $this->cms->upVideoForm(TRUE,$nama_file,$tipe_video))
					{
						$this->detail();
					}
					else {
						$info = "Sukses memperbaharui data konten Video";
						$this->tutup_dialog($info);
					}
				}
			}
        }
    }

    public function update_formplaylist()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('ket_id', 'Keterangan ID', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Keterangan EN', 'trim|required');
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__update_playlist_cek_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__update_playlist_cek_en');
        $this->form_validation->set_rules('src_video', 'URL Video', 'required|prep_url|callback__is_valid_url');
        
        // hidden
        $this->form_validation->set_rules('nama_ori_id', 'Ori ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_ori_en', 'Ori EN', 'trim_judul|required');
         $this->form_validation->set_rules('parent_video', 'Parent', 'trim|required');
        $this->form_validation->set_rules('id_playlist', 'ID', 'trim|required');
        $this->form_validation->set_rules('src_video_ori', 'SRC Asli', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail_playlist();
        }
        else {
			if ($this->input->post('src_video') === $this->input->post('src_video_ori'))
			{
				if ( ! $this->cms->upVideoPlaylistForm() )
				{
					$sts =  "Gagal Update data.";
					$this->session->set_flashdata('error', $sts);
					$this->detail_playlist();
				}
				else {
					$sts = "Sukses, Update playlist.";
					$this->session->set_flashdata('info', $sts);
					redirect($this->config->item('admpath').'/video/tabel_playlist/'.$this->input->post('parent_video'),'refresh');
				}
			}
			else {
				$tipe_video = $this->_getTipeVideo($this->input->post('src_video'));
				if ($tipe_video === 'youtube')
				$url = youtube_thumbs($this->input->post('src_video'), 1);
				else
				$url = vimeo_thumbs($this->input->post('src_video'), 1);

				$nama_file = time().'-'.basename($url);
				if( ! $this->_download_thumbs($url, $nama_file) || ! $this->_buat_thumbs($nama_file) || empty($tipe_video))
				{
					$sts =  "Gagal Update data.";
					$this->session->set_flashdata('error', $sts);
					$this->detail_playlist();
				}
				else {
					
					if( ! $this->cms->upVideoPlaylistForm(TRUE,$nama_file,$tipe_video))
					{
						$this->detail();
					}
					else {
						$sts = "Sukses, Update playlist.";
						$this->session->set_flashdata('info', $sts);
						redirect($this->config->item('admpath').'/video/tabel_playlist/'.$this->input->post('parent_video'),'refresh');
					}
				}
			}
        }
    }

	public function list_select_kategori()
	{
		$kat = $this->cms->getKategori('video');
		echo '<option value="">-- Tidak ada --</option>';
		foreach($kat as $items)
		{
			echo '<option value="'.$items->id_kategori.'">'.$items->label_id.' | '.$items->label_en.'</option>';
		}
	}

	public function tambah_kategori()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nama_kat_id', 'Nama kategori ID', 'trim_judul|required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('nama_kat_en', 'Nama kategori EN', 'trim_judul|required|min_length[3]|max_length[50]');
		
		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$data = array('status'=>'Gagal, Data yang dikirim tidak valid.');
		}
		else {
			if( ! $this->cms->cekKategori($this->input->post('nama_kat_id'),'id','video'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_id')).'" sudah ada.');
			}
			elseif( ! $this->cms->cekKategori($this->input->post('nama_kat_en'),'en','video'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_en')).'" sudah ada.');
			}
			else {
				if( ! $this->cms->addKategori('video') )
				{
					$data = array('status'=>'Gagal menambahkan data ke database.');
				}
				else {
					$data = array('status'=>'sukses', 'list'=>array(0,7));
				}
			}
		}
		echo json_encode($data);
	}

	function _kosong_folder()
    {
        // @return TRUE => Sukses , FALSE => Gagal
        $path_videos = './_media/videos/';
        
        // kosongkan folder jika data tabel kosong
        if( $this->cms->getCountVideo() === NULL || $this->cms->getCountVideo() <= 0)
        {
            $this->load->helper('file');
            
            if (is_dir($path_videos.'thumbs'))
            delete_files($path_videos.'thumbs/');
        }
    }

    function _video_cek_id($str)
    {
        if ($this->cms->adaVideo($str) == FALSE)
        {
            $this->form_validation->set_message('_video_cek_id', 'Video dengan nama Indonesia "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _video_cek_en($str)
    {
        if ($this->cms->adaVideo($str,'en') == FALSE)
        {
            $this->form_validation->set_message('_video_cek_en', 'Video dengan nama English "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_video_cek_id($str)
    {
        if ($this->cms->adaUpdateVideo($str,$this->input->post('nama_ori_id')) == FALSE)
        {
            $this->form_validation->set_message('_update_video_cek_id', 'Video dengan nama Indonesia "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_video_cek_en($str)
    {
        if ($this->cms->adaUpdateVideo($str,$this->input->post('nama_ori_en'),'en') == FALSE)
        {
            $this->form_validation->set_message('_update_video_cek_en', 'Video dengan nama English "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _playlist_cek_id($str)
    {
        if ($this->cms->adaPlaylist($str) == FALSE)
        {
            $this->form_validation->set_message('_playlist_cek_id', 'Playlist dengan nama Indonesia "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _playlist_cek_en($str)
    {
        if ($this->cms->adaPlaylist($str,'en') == FALSE)
        {
            $this->form_validation->set_message('_playlist_cek_en', 'Playlist dengan nama English "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_playlist_cek_id($str)
    {
        if ($this->cms->adaUpdatePlaylist($str,$this->input->post('nama_ori_id')) == FALSE)
        {
            $this->form_validation->set_message('_update_playlist_cek_id', 'Playlist dengan nama Indonesia "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_playlist_cek_en($str)
    {
        if ($this->cms->adaUpdatePlaylist($str,$this->input->post('nama_ori_en'),'en') == FALSE)
        {
            $this->form_validation->set_message('_update_playlist_cek_en', 'Playlist dengan nama English "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _is_valid_url($str)
    {
		if (filter_var($str, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED) === FALSE)
		{
			$this->form_validation->set_message('_is_valid_url', 'URL Video yang dimasukkan tidak valid.!');
            return FALSE;
		}
		else {
			$parts = parse_url($str);
			$boleh = array('vimeo.com','www.vimeo.com','youtube.com','www.youtube.com');
			if(in_array($parts['host'], $boleh))
			{
				return TRUE;
			}
			else {
	            $this->form_validation->set_message('_is_valid_url', 'Sistem Hanya memperbolehkan video dari "Youtube" atau "Vimeo" saja.!');
				return FALSE;
            }
        }
    }

    function _getTipeVideo($url)
	{
	    if(filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) === FALSE)
	    {
	        return null;
	    }

	    $parts = parse_url($url);
		if($parts['host'] === 'vimeo.com' || $parts['host'] === 'www.vimeo.com')
		$tipe = 'vimeo';
		else
		$tipe = 'youtube';

	    return $tipe;
	}

    function _download_thumbs($url='', $nama_file='')
    {
		$destination_folder = './_media/videos/medium/';
		$newfname = $destination_folder . $nama_file;

		if ( ! is_dir('./_media/videos/medium') )
			mkdir('./_media/videos/medium', DIR_CMS_MODE);

		$file = fopen($url, "rb");
		if($file)
		{
				$newf = fopen($newfname, "w+");
				if ($newf)
				{
					while(!feof($file)) {
						fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
					}
					fclose($newf);
				}
				fclose($file);
			return TRUE;
		}
		else {
			print('Gagal download gambar dari '.$url);
			return FALSE;
		}
    }

    function _buat_thumbs($nama_file='')
    {
		$path = './_media/videos/';
		$tipe = $this->_getTipeVideo($this->input->post('src_video'));

		if ( ! is_dir($path.'small') )
			mkdir($path.'small', DIR_CMS_MODE);

		if ( ! is_dir($path.'thumb') )
			mkdir($path.'thumb', DIR_CMS_MODE);

		if($tipe == 'youtube')
		{
			#$config['image_library'] = 'GD2';
			$config['image_library'] = 'imagemagick';
			$config['library_path'] = '/usr/bin/';
			$config['source_image'] = $path.'medium/'.$nama_file;
			$config['maintain_ratio'] = TRUE;
			$config['x_axis'] = '0';
			$config['y_axis'] = '200';
			$this->load->library('image_lib',$config);
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();

			$config = array();
			$config['image_library'] = 'GD2';
			#$config['image_library'] = 'netpbm';
			#$config['library_path'] = '/usr/bin/';
			$config['source_image'] = $path.'medium/'.$nama_file;
			$config['rotation_angle'] = 'vrt';
			$this->image_lib->initialize($config);
			$this->image_lib->rotate();
			$this->image_lib->clear();

			$config = array();
			#$config['image_library'] = 'GD2';
			$config['image_library'] = 'imagemagick';
			$config['library_path'] = '/usr/bin/';
			$config['source_image'] = $path.'medium/'.$nama_file;
			$config['maintain_ratio'] = TRUE;
			$config['x_axis'] = '0';
			$config['y_axis'] = '200';
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();

			$config = array();
			$config['image_library'] = 'GD2';
			#$config['image_library'] = 'netpbm';
			#$config['library_path'] = '/usr/bin/';
			$config['source_image'] = $path.'medium/'.$nama_file;
			$config['rotation_angle'] = 'vrt';
			$this->image_lib->initialize($config);
			$this->image_lib->rotate();
			$this->image_lib->clear();
		}

		$config = array();
		// small
		//$config['image_library'] = 'GD2';
		$config['source_image'] = $path.'medium/'.$nama_file;
		$config['new_image'] = $path.'small/small_'.$nama_file;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 240;
		$config['height'] = 180;
		if($tipe == 'vimeo')
		$this->load->library('image_lib',$config);
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$config = array();
		// thumb
		//$config['image_library'] = 'GD2';
		$config['source_image'] = $path.'medium/'.$nama_file;
		$config['new_image'] = $path.'thumb/thumb_'.$nama_file;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 120;
		$config['height'] = 90;
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		if ( ! $this->image_lib->resize())
		{
			print_r($this->image_lib->display_errors());
			hapus_file($path.$nama_file);
			return FALSE;
		}
		else {
			return TRUE;
		}
    }

}

/* End of file video.php */
/* Location: ./cms/controllers/admin/video.php */

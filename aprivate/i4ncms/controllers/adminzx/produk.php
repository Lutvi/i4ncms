<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman','batman','spiderman');
		$this->_hpsQryCache('produk');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Produk';
		
		$config['base_url'] = base_url().$this->config->item('admpath').'/produk';
		$config['total_rows'] = $this->mproduk->getCountIndukProduk();
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
		
		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['produk'] = $this->mproduk->getProdukInduk($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$data['partial_content'] = 'v_produk';

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$this->load->library('pagination');

		$config['base_url'] = base_url().$this->config->item('admpath').'/produk/update_tabel';
		$config['total_rows'] = $this->mproduk->getCountIndukProduk();
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		}
		else {
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['produk'] = $this->mproduk->getProdukInduk($config['per_page'],$offset);
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = '';

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_produk',$data);
		}
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function tambah()
	{
		$data['title'] = 'Admin Tambah Produk';
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','plugins/jpicker-1.1.6/css/jPicker-1.1.6.min.css');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','plugins/jpicker-1.1.6/jpicker-1.1.6.min.js','plugins/formatAngka.js');
		$data['induk_prod'] = $this->mproduk->getIndukProduk();
		$data['kat_produk'] = $this->cms->getKategori('produk');
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		
		$data = array_merge($data,$this->editorNormal($id='deskripsi'),$this->editorNormal($id='deskripsi_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_produk',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	function tambah_anak()
	{
		$data['title'] = 'Admin Tambah Produk Anak';
		$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','plugins/jpicker-1.1.6/css/jPicker-1.1.6.min.css');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','plugins/jpicker-1.1.6/jpicker-1.1.6.min.js','plugins/formatAngka.js');
		$data['kat_produk'] = $this->cms->getKategori('produk');
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;

		$data = array_merge($data,$this->editorNormal($id='deskripsi'),$this->editorNormal($id='deskripsi_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_produk_anak',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

		
	function detail($id='')
	{
		$pid = ($id=='')?$this->uri->segment(4):$id;

		$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','plugins/jpicker-1.1.6/css/jPicker-1.1.6.min.css');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','plugins/jpicker-1.1.6/jpicker-1.1.6.min.js','plugins/formatAngka.js');
		$data['kat_produk'] = $this->cms->getKategori('produk');
		$data['produk'] = $this->mproduk->getAllPropertiIndukById($pid);
		$data['produk_img'] = $this->mproduk->getAllImgAdminById($data['produk']->id_prod_img);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		
		$data = array_merge($data,$this->editorNormal($id='deskripsi'),$this->editorNormal($id='deskripsi_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_produk_induk',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	function detail_update_anak($id='')
	{
		$pid = ($id=='')?$this->uri->segment(4):$id;

		$data['title'] = 'Admin Produk Anak';
		$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','plugins/jpicker-1.1.6/css/jPicker-1.1.6.min.css');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','plugins/jpicker-1.1.6/jpicker-1.1.6.min.js','plugins/formatAngka.js');
		$data['kat_produk'] = $this->cms->getKategori('produk');
		$data['produk'] = $this->mproduk->getAllPropertiIndukById($pid);
		$data['produk_img'] = $this->mproduk->getAllImgAdminById($data['produk']->id_prod_img);
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		
		$data = array_merge($data,$this->editorNormal($id='deskripsi'),$this->editorNormal($id='deskripsi_en'));

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		$this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_produk_anak',$data);
		else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	function detail_anak()
	{
		$this->load->library('pagination');

		$id_induk = $this->uri->segment(4,$this->input->post('parent_id_prod', TRUE));
		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Produk Anak';
		$config['base_url'] = base_url().$this->config->item('admpath').'/produk/detail_anak/'.$id_induk;
		$config['total_rows'] = $this->mproduk->getCountAnakProduk($id_induk);
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 5;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

		if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
		{
			$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		}
		else{
			$offset = $this->uri->segment($config['uri_segment'],0);
		}
		$data['produk'] = $this->mproduk->getProdukAnak($id_induk,$config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['id_induk'] = $id_induk;
		$data['sts'] = '';
		

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			if ($this->uri->segment(5) === 'tabel' || $this->uri->segment(6) === 'tabel')
			{
				$this->load->view('dynamic_js',$data);
				$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_produk_anak',$data);
			}
			else {
				$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_produk_anak',$data);
			}
		}
		else {
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
		}
	}

	public function add_produk()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$tipe = $this->input->post('radio');
		
		$this->form_validation->set_rules('radio', 'Tipe', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('promo', 'Promo', 'trim|required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'trim');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|integer');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|integer');
		$this->form_validation->set_rules('harga_spesial', 'Spesial', 'trim|integer');
		$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');

		if ( $tipe === 'induk' )
		{
			$this->form_validation->set_rules('nama_prod', 'Nama Produk Indonesia', 'trim_judul|required|max_length[100]|callback__produk_cek');
			$this->form_validation->set_rules('nama_prod_en', 'Nama Produk English', 'trim_judul|required|max_length[100]|callback__produk_cek_en');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi Indonesia', 'trim|required');
			$this->form_validation->set_rules('deskripsi_en', 'Deskripsi English', 'trim|required');
			$this->form_validation->set_rules('harga', 'Harga', 'trim|required|greater_than[0]');
			$this->form_validation->set_rules('kode-warnaInduk', 'Kode Warna', 'trim');
			$this->form_validation->set_rules('kode_prod', 'Kode Produk', 'trim|callback__kode_produk_cek');
			$nm = $this->input->post('nama_prod');
		}

		if ( $tipe === 'anak' )
		{
			/* cek harga anak */
			if($this->input->post('cek-harga-ikut-induk') === 'y' )
			{
				$this->form_validation->set_rules('cek-harga-ikut-induk', 'Cek Harga', 'trim');
			}
			else {
				$this->form_validation->set_rules('harga', 'Harga', 'trim|required|greater_than[0]');
			}
			/* cek deskripsi anak */
			if($this->input->post('cek-ikut-induk') === 'y' )
			{
				$this->form_validation->set_rules('cek-ikut-induk', 'Cek Deskripsi', 'trim');
			}
			else {
				$this->form_validation->set_rules('deskripsi', 'Deskripsi Indonesia', 'trim|required');
				$this->form_validation->set_rules('deskripsi_en', 'Deskripsi English', 'trim|required');
			}
			
			$jns = $this->input->post('anak_untuk');
			$this->form_validation->set_rules('nama_anak', 'Nama Indonesia '.$jns, 'trim_judul|required|max_length[100]|callback__anak_produk_cek');
			$this->form_validation->set_rules('nama_anak_en', 'Nama English '.$jns, 'trim_judul|max_length[100]|callback__anak_produk_cek');
			$this->form_validation->set_rules('parent_id_prod', 'Induk Produk', 'trim|required');
			$this->form_validation->set_rules('anak_untuk', 'Nama Jenis', 'trim|required');
			$this->form_validation->set_rules('kode-warnaAnak', 'Kode Warna', 'trim');
			$this->form_validation->set_rules('kode_prod_anak', 'Kode Produk', 'trim|callback__kode_produk_cek');
			
			$induk = $this->mproduk->getPropertiIndukById($this->input->post('parent_id_prod'));            
			$nm = $induk->nama_prod . '_' . $this->input->post('nama_anak');
		}
		
		if(empty($_FILES['userfile']['name'][0]))
		{
			$this->form_validation->set_rules('userfile', 'Foto Produk minimal 1 Foto', 'trim|required');
		}
		
		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->tambah();
		}
		else {
			if ( ! $this->_upload_gambar_produk('userfile',$nm) )
			{
				$info = 'Gagal Upload foto produk. pastikan ukuran dan jenis file yang diupload sudah sesuai';
				$this->tutup_dialog_gagal($info);
			}
			else {
				if( ! $this->mproduk->tambahProduk() )
				{
					$info = 'Gagal Memasukkan data Produk';
					$this->tutup_dialog_gagal($info);
				}
				else {
					$info = 'Sukses Produk baru berhasil ditambahkan';
					$this->tutup_dialog($info);
				}
				
				delete_files('./_produk/big/');
			}
		}
	}

	public function add_produk_anak()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('promo', 'Promo', 'trim|required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'trim');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|integer');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|integer');
		$this->form_validation->set_rules('harga_spesial', 'Spesial', 'trim|integer');
		$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');


		/* cek harga anak */
		if($this->input->post('cek-harga-ikut-induk') === 'y' )
		{
			$this->form_validation->set_rules('cek-harga-ikut-induk', 'Cek Harga', 'trim');
		}
		else {
			$this->form_validation->set_rules('harga', 'Harga', 'trim|required|greater_than[0]');
		}
		/* cek deskripsi anak */
		if($this->input->post('cek-ikut-induk') === 'y' )
		{
			$this->form_validation->set_rules('cek-ikut-induk', 'Cek Deskripsi', 'trim');
		}
		else {
			$this->form_validation->set_rules('deskripsi', 'Deskripsi Indonesia', 'trim|required');
			$this->form_validation->set_rules('deskripsi_en', 'Deskripsi English', 'trim|required');
		}

		$jns = $this->input->post('anak_untuk');
		$this->form_validation->set_rules('nama_anak', 'Nama Indonesia '.$jns, 'trim_judul|required|max_length[100]|callback__anak_produk_cek');
		$this->form_validation->set_rules('nama_anak_en', 'Nama English '.$jns, 'trim_judul|required|max_length[100]|callback__anak_produk_cek');
		$this->form_validation->set_rules('parent_id_prod', 'Induk Produk', 'trim|required');
		$this->form_validation->set_rules('anak_untuk', 'Nama Jenis', 'trim_judul|required');
		$this->form_validation->set_rules('kode-warnaAnak', 'Kode Warna', 'trim');
		$this->form_validation->set_rules('kode_prod_anak', 'Kode Produk', 'trim|callback__kode_produk_cek');

		$induk = $this->mproduk->getPropertiIndukById($this->input->post('parent_id_prod'));
		$nm = $induk->nama_prod . '_' . $this->input->post('nama_anak');


		if(empty($_FILES['userfile']['name'][0]))
		{
			$this->form_validation->set_rules('userfile', 'Foto Produk minimal 1 Foto', 'trim|required');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->tambah_anak();
		}
		else {
			if ( ! $this->_upload_gambar_produk('userfile',$nm) )
			{
				$info = 'Gagal Upload foto produk. pastikan ukuran dan jenis file yang diupload sudah sesuai';
				$info .= '<br>Silahkan coba lagi.!';
				$this->session->set_flashdata('info', $info);
				redirect($this->config->item('admpath').'/produk/detail_anak/'.$this->input->post('parent_id_prod'), 'refresh');
			}
			else {
				if( ! $this->mproduk->tambahProdukAnak() )
				{
					$info = 'Gagal, Tambahkan data ke database.<br>Silahkan coba lagi.!';
					$this->session->set_flashdata('info', $info);
					redirect($this->config->item('admpath').'/produk/detail_anak/'.$this->input->post('parent_id_prod'), 'refresh');
				}
				else {
					$info = 'Sukses, Data Produk berhasil ditambah.';
					$this->session->set_flashdata('info', $info);
					redirect($this->config->item('admpath').'/produk/detail_anak/'.$this->input->post('parent_id_prod'), 'refresh');
				}

				delete_files('./_produk/big/');
			}
		}
	}

	public function list_select_kategori()
	{
		$kat = $this->cms->getKategori('produk');
		echo '<option value="">-- Tidak ada --</option>';
		foreach($kat as $items)
		{
			echo '<option value="'.$items->id_kategori.'">'.$items->label_id.' | '.$items->label_en.'</option>';
		}
	}

	public function tambah_kategori()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		$this->form_validation->set_rules('nama_kat_id', 'Nama kategori ID', 'trim_judul|required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('nama_kat_en', 'Nama kategori EN', 'trim_judul|required|min_length[3]|max_length[50]');
		
		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$data = array('status'=>'Gagal, Data yang dikirim tidak valid.');
		}
		else {
			if( ! $this->cms->cekKategori($this->input->post('nama_kat_id'),'id','produk'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_id')).'" sudah ada.');
			}
			elseif( ! $this->cms->cekKategori($this->input->post('nama_kat_en'),'en','produk'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_en')).'" sudah ada.');
			}
			else {
				if( ! $this->cms->addKategori('produk') )
				{
					$data = array('status'=>'Gagal menambahkan data ke database.');
				}
				else {
					$data = array('status'=>'sukses', 'list'=>array(0,7));
				}
			}
		}
		echo json_encode($data);
	}

	public function update_produk()
	{
		$this->load->library('form_validation');
		
		if ( $this->input->post('prod_id') && $this->super_admin )
		{
			if($this->input->post('id_anak'))
			$this->form_validation->set_rules('id_anak', 'ID Produk Anak', 'trim|required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('prod_id', 'ID Produk', 'trim|required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('promo', 'Promo', 'trim|required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('stok', 'Stok', 'trim|required|integer|max_length[11]|xss_clean');

			if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			{
				echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
			}
			else {
				if( ! $this->mproduk->upProdukStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Status..!!';
				}
				else {
					echo 'sukses';
				}
			}
		}
		elseif ( $this->input->post('id_hps') && $this->super_admin ) {
			$this->form_validation->set_rules('id_hps', 'ID Produk', 'trim|required|integer|max_length[11]|xss_clean');
			
			if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
			{
				echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
			}
			else {
				if( ! $this->mproduk->hapusProduk($this->input->post('id_hps')) || ! $this->super_admin )
				{
					echo 'Terjadi kesalahan database, Gagal Hapus Produk..!!';
				}
				else {
					echo 'sukses';
				}
			}
		}
		else {
			echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
		}
	}

	function updateform_produk_induk()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$this->form_validation->set_rules('promo', 'Promo', 'trim|required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'trim');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|integer');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|integer');
		$this->form_validation->set_rules('harga_spesial', 'Spesial', 'trim|integer');

		$this->form_validation->set_rules('nama_prod', 'Nama Produk Indonesia', 'trim_judul|max_length[100]|required|callback__produk_cek_update');
		$this->form_validation->set_rules('nama_prod_en', 'Nama Produk English', 'trim_judul|max_length[100]|required|callback__produk_cek_update_en');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Indonesia', 'trim|required');
		$this->form_validation->set_rules('deskripsi_en', 'Deskripsi English', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('kode-warnaInduk', 'Kode Warna', 'trim');
		$this->form_validation->set_rules('kode_prod', 'Kode Produk', 'trim|callback__kode_produk_cek_update');
		$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');

		/*hidden input*/
		$this->form_validation->set_rules('id_prod', 'Id Produk', 'trim|required');
		$this->form_validation->set_rules('nama_ori', 'Nama Ori ID', 'trim_judul|required');
		$this->form_validation->set_rules('nama_ori_en', 'Nama Ori EN', 'trim_judul|required');
		$this->form_validation->set_rules('kode_ori', 'Kode Ori', 'trim');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->detail($this->input->post('id_prod'));
		}
		else {
			if ( ! $this->mproduk->updateProdukInduk() )
			{
				$info = 'Gagal Memperbaharui data Produk';
				$this->tutup_dialog_gagal($info);
			}
			else  {
				$info = 'Sukses Data Produk berhasil diperbahrui';
				$this->tutup_dialog($info);
			}
		}
	}

	function updateform_produk_anak()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$this->form_validation->set_rules('promo', 'Promo', 'trim|required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'trim');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|integer');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|integer');
		$this->form_validation->set_rules('harga_spesial', 'Harga Spesial', 'trim|integer');


		$jns = $this->input->post('anak_untuk');
		$this->form_validation->set_rules('nama_anak', 'Nama '.$jns. ' Indonesia', 'trim_judul|required|max_length[100]|callback__anak_produk_cek_update');
		$this->form_validation->set_rules('nama_anak_en', 'Nama '.$jns.' English', 'trim_judul|required|max_length[100]|callback__anak_produk_cek_update_en');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Indonesia', 'trim|required');
		$this->form_validation->set_rules('deskripsi_en', 'Deskripsi English', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('kode-warna', 'Kode Warna', 'trim');
		$this->form_validation->set_rules('kode_prod', 'Kode Produk', 'trim|callback__kode_produk_cek_update');
		$this->form_validation->set_rules('parent_id_prod', 'Induk Produk', 'trim|required');
		$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');

		/*hidden input*/
		$this->form_validation->set_rules('id_prod', 'Id Produk', 'trim|required');
		$this->form_validation->set_rules('nama_ori', 'Nama Ori Indonesia', 'trim_judul|required');
		$this->form_validation->set_rules('nama_ori_en', 'Nama Ori English', 'trim_judul|required');
		$this->form_validation->set_rules('kode_ori', 'Kode Ori', 'trim');
		$this->form_validation->set_rules('nama_prod', 'Nama Produk', 'trim|required');
		$this->form_validation->set_rules('anak_untuk', 'Tipe Anak', 'trim|required');
	
		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$this->detail_update_anak($this->input->post('parent_id_prod'));
		}
		else {
			if ( ! $this->mproduk->updateProdukAnak() )
			{
				echo '<p>Gagal, Update data ke database.<br>Silahkan coba lagi.</p>';
				echo anchor(site_url($this->config->item('admpath').'/produk/detail_anak/'.$this->input->post('parent_id_prod').'/show'),'Kembali ke List');
			}
			else  {
				$info = 'Sukses, Data Produk berhasil diupdate.';
				$this->session->set_flashdata('info', $info);
				redirect($this->config->item('admpath').'/produk/detail_anak/'.$this->input->post('parent_id_prod'), 'refresh');
			}
		}
	}

	function tambah_foto()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id_prod_img', 'Nama Img', 'trim|required');
		$this->form_validation->set_rules('radio', 'Tipe', 'trim|required');

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			echo 'Gagal Upload foto, data yang terkirim tidak valid.<br>Silahkan coba lagi.';
		}
		else {
			if ($this->input->post('radio') === 'induk')
			{
				$nm = trim(ltrim($this->input->post('id_prod_img'), 'induk'),'_');
			} else {
				$nm = trim(ltrim($this->input->post('id_prod_img'), 'anak'),'_');
			}

			if ( ! $this->_upload_gambar_produk('userfile', $nm))
			{
				echo 'Gagal, Upload foto produk.<br>Silahkan coba lagi.';
			}
			else {
				echo 'sukses';
				delete_files('./_produk/big/');
			}
		}
	}

	function hapus_foto()
	{
		if ( ! $this->mproduk->hapusByImgId($this->input->post('id_img')))
		{
			echo 'Gagal hapus foto, Silahkan coba lagi.';
		}
		else {
			echo 'sukses';
		}
	}

	function _produk_cek($str)
	{
		if ($this->mproduk->cekNamaProduk($str) == FALSE)
		{
			$this->form_validation->set_message('_produk_cek', 'Nama Produk Indonesia dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _produk_cek_en($str)
	{
		if ($this->mproduk->cekNamaProduk($str, 'en') == FALSE)
		{
			$this->form_validation->set_message('_produk_cek_en', 'Nama Produk English dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function _anak_produk_cek($str)
	{
		if ($this->mproduk->cekNamaProdukAnak($str) == FALSE)
		{
			$induk = $this->mproduk->getPropertiIndukById($this->input->post('parent_id_prod'));
			$this->form_validation->set_message('_anak_produk_cek', 'Produk dengan nama "' . $induk->nama_prod . '-' . $str . '" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function _kode_produk_cek($str)
	{
		if ($this->mproduk->cekKodeProduk($str) == FALSE)
		{
			$this->form_validation->set_message('_kode_produk_cek', 'Kode Produk "' . $str . '" sudah terdaftar.!');
			
			if (empty($str))
				return TRUE;
			else
				return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function _produk_cek_update($str)
	{
		if ($this->mproduk->cekNamaProdukUpdate($str,$this->input->post('nama_ori')) == FALSE)
		{
			$this->form_validation->set_message('_produk_cek_update', 'Nama Produk Indonesia dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _produk_cek_update_en($str)
	{
		if ($this->mproduk->cekNamaProdukUpdate($str,$this->input->post('nama_ori_en'),'en') == FALSE)
		{
			$this->form_validation->set_message('_produk_cek_update_en', 'Nama Produk English dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function _anak_produk_cek_update($str)
	{
		if ($this->mproduk->cekNamaProdukAnakUpdate($str,$this->input->post('nama_ori')) == FALSE)
		{
			$induk = $this->mproduk->getPropertiIndukById($this->input->post('parent_id_prod'));
			$this->form_validation->set_message('_anak_produk_cek_update', 'Nama jenis Produk Indonesia dengan nama "' . $induk->nama_prod . '-' . $str . '" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _anak_produk_cek_update_en($str)
	{
		if ($this->mproduk->cekNamaProdukAnakUpdate($str,$this->input->post('nama_ori_en'),'en') == FALSE)
		{
			$induk = $this->mproduk->getPropertiIndukById($this->input->post('parent_id_prod'));
			$this->form_validation->set_message('_anak_produk_cek_update_en', 'Nama Jenis Produk English dengan nama "' . $induk->nama_prod . '-' . $str . '" sudah terdaftar.!');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _kode_produk_cek_update($str)
	{
		if ($this->mproduk->cekKodeProdukUpdate($str,$this->input->post('kode_ori')) == FALSE)
		{
			$this->form_validation->set_message('_kode_produk_cek_update', 'Kode Produk "' . $str . '" sudah terdaftar.!');
			
			if (empty($str))
				return TRUE;
			else
				return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _upload_gambar_produk($inputfile,$nm)
	{
		$this->load->helper('file');

		if ( ! is_dir('./_produk/big') )
			mkdir('./_produk/big', DIR_CMS_MODE);

		if ( ! is_dir('./_produk/large') )
			mkdir('./_produk/large', DIR_CMS_MODE);

		if ( ! is_dir('./_produk/medium') )
			mkdir('./_produk/medium', DIR_CMS_MODE);

		if ( ! is_dir('./_produk/small') )
			mkdir('./_produk/small', DIR_CMS_MODE);

		if ( ! is_dir('./_produk/thumb') )
			mkdir('./_produk/thumb', DIR_CMS_MODE);
		
		/* Buat nama img array */
		if ($this->input->post('jml_img'))
		{
			$i = $this->input->post('jml_img');
		} else {
			$i=1;
		}

		foreach ($_FILES[$inputfile]['name'] as $items)
		{
			$nm_img[] = time() . '_'. humanize($nm) . '-' . $i;
			$i++;
		}

		$this->load->library('upload');
		/* Konfigurasi proses upload */
		$this->upload->initialize(array(
			"file_name"  => $nm_img,
			"upload_path"   => "./_produk/big/",
			"allowed_types" => "gif|jpg|png",
			"overwrite" => FALSE,
			"max_size" => "1024",
			"min_height" => "800",
			"min_width" => "600",
			"max_width" => "1600",
			"max_height" => "1200"
		));

		/* Proses Upload */
		if ($this->upload->do_multi_upload($inputfile))
		{
			/* Resize foto/gambar */
			$this->load->library('image_lib');
			foreach ($this->upload->get_multi_upload_data() as $key=>$val)
			{
				/* Ambil nama file untuk dimanipulasi */
				$fnm = $val['file_name'];

				/* Resize */
				$config = array();
				// large
				//$config['image_library'] = 'GD2';
				$config['source_image'] = './_produk/big/'.$fnm;
				$config['new_image'] = './_produk/large/large_'.$fnm;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 1024;
				$config['height'] = 768;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();
				 /* Water marking */
				$config = array();
				$config['source_image'] = './_produk/large/large_'.$fnm;
				$config['wm_type'] = 'overlay';
				$config['wm_vrt_alignment'] = 'bottom';
				$config['wm_hor_alignment'] = 'right';
				$config['wm_overlay_path'] = './assets/images/watermark-logo/large-logo.jpg';
				$config['wm_opacity'] = 15;
				$this->image_lib->initialize($config);
				$this->image_lib->watermark();
				$this->image_lib->clear();

				$config = array();
				// medium
				//$config['image_library'] = 'GD2';
				$config['source_image'] = './_produk/big/'.$fnm;
				$config['new_image'] = './_produk/medium/medium_'.$fnm;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 640;
				$config['height'] = 428;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();
				 /* Water marking */
				$config = array();
				$config['source_image'] = './_produk/medium/medium_'.$fnm;
				$config['wm_type'] = 'overlay';
				$config['wm_vrt_alignment'] = 'bottom';
				$config['wm_hor_alignment'] = 'right';
				$config['wm_overlay_path'] = './assets/images/watermark-logo/medium-logo.jpg';
				$config['wm_opacity'] = 12;
				$this->image_lib->initialize($config);
				$this->image_lib->watermark();
				$this->image_lib->clear();

				$config = array();
				// small
				//$config['image_library'] = 'GD2';
				$config['source_image'] = './_produk/big/'.$fnm;
				$config['new_image'] = './_produk/small/small_'.$fnm;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 320;
				$config['height'] = 240;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

				$config = array();
				// thumb
				//$config['image_library'] = 'GD2';
				$config['source_image'] = './_produk/big/'.$fnm;
				$config['new_image'] = './_produk/thumb/thumb_'.$fnm;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 160;
				$config['height'] = 108;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				//$this->image_lib->clear();

				$config = array();
				// small_thumb
				//$config['image_library'] = 'GD2';
				$config['source_image'] = './_produk/big/'.$fnm;
				$config['new_image'] = './_produk/thumb/small_thumb_'.$fnm;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 65;
				$config['height'] = 49;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

				/* Water marking small */
				$config = array();
				$config['source_image'] = './_produk/small/small_'.$fnm;
				$config['wm_type'] = 'overlay';
				$config['wm_vrt_alignment'] = 'bottom';
				$config['wm_hor_alignment'] = 'right';
				$config['wm_overlay_path'] = './assets/images/watermark-logo/small-logo.jpg';
				$config['wm_opacity'] = 12;
				$this->image_lib->initialize($config);
				$this->image_lib->watermark();

				if ( ! $this->image_lib->resize() || ! $this->image_lib->watermark())
				{
					print_r($this->image_lib->display_errors());
					delete_files('./_produk/big/');
					return FALSE;
				}
				else {
					if ( ! $this->mproduk->tambahImgProduk($fnm))
					{
						print_r('Gagal menyimpan ke database.');
						delete_files('./_produk/big/');
						return FALSE;
					}
				}
			} 
			
			return TRUE;
		}
		else {
			print_r($this->upload->display_errors());
			delete_files('./_produk/big/');
			return FALSE;
		}
	}

}

/* End of file produk.php */
/* Location: ./cms/controllers/admin/produk.php */

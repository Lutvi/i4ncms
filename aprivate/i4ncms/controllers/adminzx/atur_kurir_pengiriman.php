<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Atur_kurir_pengiriman extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');
		$pp = $this->adm_per_halaman;

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Kurir Pengiriman';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;

		$config['base_url'] = base_url().$this->config->item('admpath').'/atur_kurir_pengiriman';
		$config['total_rows'] = $this->orderan->getCountKurirPengiriman();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']), $config['per_page']);
        }
        else{
            $offset = $this->uri->segment($bayar_transfer['uri_segment'],0);
        }
        
		$data['kurir'] = $this->orderan->getAllKurirPengiriman($config['per_page'],$offset);
		$data['page'] =  $this->pagination->create_links();
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_kurir_pengiriman',$data);
		}
		else {
			return $data;
		}
	}

	public function update_tabel()
	{
		$data = $this->index('ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_kurir_pengiriman',$data);
		}
		else {
			$data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
        }
	}

	public function tambah_kurir($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['sts'] = $sts;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_kurir_pengiriman',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_kurir()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'trim_judul|max_length[50]|required|callback__cek_vendor');
		$this->form_validation->set_rules('nama_layanan', 'Nama Layanan', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('biaya_ongkir', 'Ongkos Kirim', 'required|integer|max_length[11]');
		$this->form_validation->set_rules('waktu_barang_sampai', 'Waktu Sampai', 'required|integer|max_length[2]');
		$this->form_validation->set_rules('url_trace', 'URL Trace', 'trim|prep_url');
		$this->form_validation->set_rules('detail', 'Detail Kurir', 'trim|rmax_length[100]');

        /* Cek logo */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Logo Vendor', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->tambah_kurir('error');
        }
        else {
            if ( ! $this->_do_upload_logo() )
            {
                $error = "Gagal Upload Logo, pastikan ukuran dan jenis file yang diupload sudah sesuai.";
                $this->session->set_flashdata('error', $error);
                $this->tambah_kurir('error');
            }
            else {
                if( ! $this->orderan->tambahKurir() )
                {
					$error = "Data tidak dapat tersimpan ke dalam database.";
					$this->session->set_flashdata('error', $error);
                    $this->tambah_kurir('error');
                }
                else {
                    $info = "Sukses, menambahkan Kurir Pengiriman.";
                    $this->session->set_flashdata('info', $info);
                    redirect($this->config->item('admpath').'/atur_kurir_pengiriman','refresh');
                }
				hapus_file('./_media/logo-kurir/'.$this->upload->orig_name);
            }
        }
    }

    public function detail_kurir($sts='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_kurir'));
        
        $data['kurir'] = $this->orderan->getKurirPengirimanById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['sts'] = '';

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_kurir_pengiriman',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_kurir()
    {
    	$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		if( $this->input->post('kurir_id'))
		{
			$this->form_validation->set_rules('kurir_id', 'ID', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID', 'required|integer|max_length[11]|xss_clean');
		}

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('kurir_id') && $this->_isSupAdmin() ) 
			{
				if( ! $this->orderan->upKurirPengirimanStat() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Status Kurir..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->_isSupAdmin() ) {
				if( ! $this->orderan->hapusKurirPengiriman() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Hapus Kurir..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }

    public function update_form_kurir()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'trim_judul|max_length[50]|required|callback__cek_upvendor');
		$this->form_validation->set_rules('nama_layanan', 'Nama Layanan', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('biaya_ongkir', 'Ongkos Kirim', 'required|integer|max_length[11]');
		$this->form_validation->set_rules('waktu_barang_sampai', 'Waktu Sampai', 'required|integer|max_length[2]');
		$this->form_validation->set_rules('url_trace', 'URL Trace', 'trim|prep_url');
		$this->form_validation->set_rules('detail', 'Detail Kurir', 'trim|rmax_length[100]');

        // hidden
        $this->form_validation->set_rules('id_kurir', 'ID', 'trim|required');
        $this->form_validation->set_rules('nama_vendor_ori', 'vendor Ori', 'trim_judul|max_length[50]|required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'trim|required');
        $this->form_validation->set_rules('logo', 'Logo', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail_kurir('error');
        }
        else {
            if($this->input->post('status_ganti') == 'yes')
            {
                if( ! $this->_do_upload_logo() )
                {
                    $error = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->session->set_flashdata('error', $error);
	                $this->detail_kurir('error');
                }
                else {
                    if( ! $this->orderan->upKurirPengiriman() )
                    {
                        $error = "Gagal Update Kurir, terjadi kesalahan pada server database";
						$this->session->set_flashdata('error', $error);
		                $this->detail_kurir('error');
                    }
                    else {
						//hapus logo lama
						hapus_file("./_media/logo-kurir/". $this->input->post('logo'));
						hapus_file("./_media/logo-kurir/thumb_". $this->input->post('logo'));

                        $info = "Sukses memperbaharui data <strong>Kurir ".$this->input->post('nama_vendor')."</strong>";
                        $this->session->set_flashdata('info', $info);
	                    redirect($this->config->item('admpath').'/atur_kurir_pengiriman','refresh');
                    }
                    hapus_file('./_media/logo-kurir/'.$this->upload->orig_name);
                }
            }
            else {
                if ( ! $this->orderan->upKurirPengiriman() )
                {
                    $error = "Gagal Update Kurir, terjadi kesalahan pada server database";
					$this->session->set_flashdata('error', $error);
                }
                else {
                    $info = "Sukses memperbaharui data <strong>Kurir ".$this->input->post('nama_vendor')."</strong>";
                    $this->session->set_flashdata('info', $info);
                    redirect($this->config->item('admpath').'/atur_kurir_pengiriman','refresh');
                }
            }
        }
    }

    function _cek_vendor($str)
    {
		if ($this->orderan->cekVendorKurir($str) == FALSE)
        {
            $this->form_validation->set_message('_cek_vendor', 'Nama Vendor "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _cek_upvendor($str)
    {
		if ($this->orderan->cekVendorKurir($str,$this->input->post('nama_vendor_ori')) == FALSE)
        {
            $this->form_validation->set_message('_cek_upvendor', 'Nama Vendor "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _do_upload_logo($path='./_media/logo-kurir/')
	{
		$this->load->helper('file');

		if ( ! is_dir($path) )
			mkdir($path, DIR_CMS_MODE);

		$config['file_name']  = time().'_'.$this->input->post('nama_vendor');
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '800';
        $config['max_width']  = '640';
        $config['max_height']  = '640';
        $config['remove_spaces']  = TRUE;
        $config['upload_path'] = $path;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			print_r($this->upload->display_errors());
            hapus_file($path.$this->upload->orig_name);
            return FALSE;
		}
		else {
            $config = array();
            // thumb
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'thumb_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 80;
            $config['height'] = 80;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                hapus_file($path.$this->upload->orig_name);
                return FALSE;
            }
			return TRUE;
		}
	}

}

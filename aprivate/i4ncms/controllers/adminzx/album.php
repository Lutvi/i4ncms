<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Album extends AdminController {

    public function __construct()
    {
        parent::__construct();

        $this->_cekStsLoginAdm();
        $this->hak = array('superman','batman','ironman');
        $this->_hpsQryCache('album');
    }

    public function index($sts='')
    {
        $this->load->library('pagination');
        
        /*/- cek data jika kosong hapus semua foto -/*/
        $this->_kosong_folder();
        
        $data = $this->_getAdminAssets();
        $data['title'] = 'Admin Album';
        
        $config['base_url'] = base_url().$this->config->item('admpath').'/album';
        $config['total_rows'] = $this->cms->getCountAlbum();
        $config['per_page'] = $this->adm_per_halaman; 
        $config['uri_segment'] = 3;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->cms->getAllAlbum($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data['partial_content'] = 'v_album';

        $data['jml_gallery'] = $this->cms->getCountGallery();
        if($data['jml_gallery'] > 0)
        $data['parent_album'] = $this->cms->getAlbumSelect(TRUE);
        else
        $data['parent_album'] = $this->cms->getAlbumSelect();

        $this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
    }

    public function update_tabel()
    {
        $this->load->library('pagination');
        
        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/album/update_tabel'));
        $config['total_rows'] = $this->cms->getCountAlbum();
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 4;
        $config = array_merge($config, $this->_adminPagination());
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->cms->getAllAlbum($config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';

        $data['jml_gallery'] = $this->cms->getCountGallery();
        if($data['jml_gallery'] > 0)
        $data['parent_album'] = $this->cms->getAlbumSelect(TRUE);
        else
        $data['parent_album'] = $this->cms->getAlbumSelect();

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_album',$data);
        }else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function tambah($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['kat_album'] = $this->cms->getKategori('album');
        $data['sts'] = $sts;

        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_album',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail()
    {
        $id = $this->uri->segment(4,$this->input->post('id_album'));
        
        $data['album'] = $this->cms->getAllAlbumAdminById($id);
        $data['tags'] = $this->cms->getTagsIdJsonByObjekId($id,'album');
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['kat_album'] = $this->cms->getKategori('album');

        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_album',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function detail_gallery()
    {
        $id = $this->uri->segment(4,$this->input->post('id_album'));
        
        $data['album'] = $this->cms->getAlbumGalleryAdminById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        $data['kat_album'] = $this->cms->getKategori('album');

        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_gallery',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function tabel_gallery()
    {
        $id = $this->uri->segment(4,$this->input->post('parent_album'));

        $this->load->library('pagination');

        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/album/tabel_gallery/'.$id));
        $config['total_rows'] = $this->cms->getCountGalleryAlbum($id);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 5;
        $config = array_merge($config,$this->_adminPagination());

        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->cms->getAllGalleryAlbum($id,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data['title'] = 'Album Gallery';
        $data['id_album'] = $id;
        $data['nama_album'] = $this->cms->getNamaGalleryAlbumById($id);
        $data = array_merge($data, $this->_getAdminAssets());

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/partial_frame/v_gallery',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_tabel_gallery()
    {
        $id = $this->uri->segment(4,$this->input->post('parent_album'));
        
        $this->load->library('pagination');
        
        $config['base_url'] = bersihkanUrlPaginasi(site_url($this->config->item('admpath').'/album/update_tabel_gallery/'.$id.'/'));
        $config['total_rows'] = $this->cms->getCountGalleryAlbum($id);
        $config['per_page'] = $this->adm_per_halaman;
        $config['uri_segment'] = 5;
        $config = array_merge($config,$this->_adminPagination());
        
        $this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else {
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
        $data['album_web'] = $this->cms->getAllGalleryAlbum($id,$config['per_page'],$offset);
        $data['page'] =  $this->pagination->create_links();
        $data['login'] = $this->login;
        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = '';
        $data['title'] = 'Album Gallery';
        $data['id_album'] = $id;
        $data['nama_album'] = $this->cms->getNamaGalleryAlbumById($id);

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
		$this->load->view('dynamic_js',$data);
        $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_gallery',$data);
        }
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function tambah_gallery($sts='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_album'));
        
        $data['album'] = $this->cms->getAllAlbumAdminById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        if($this->cms->getCountGallery() > 0)
        $data['parent_album'] = $this->cms->getAlbumSelect(TRUE);
        else
        $data['parent_album'] = $this->cms->getAlbumSelect();

        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_album_gallery',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function tambah_gallery_album($sts='')
    {
        $id = $this->uri->segment(4,$this->input->post('id_album'));
        
        $data['album'] = $this->cms->getAllAlbumAdminById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css');
        $data['parent_album'] = $id;

        $data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
        $data['sts'] = $sts;
        $data = array_merge($data,$this->editorMini($id='ket_id'),$this->editorMini($id='ket_en'));

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_album_gallery',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_album()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__album_cek');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__album_cek');
        $this->form_validation->set_rules('utama', 'Album Utama', 'required');
        $this->form_validation->set_rules('diskusi', 'Komentar Album', 'required');
        //$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');
        $this->form_validation->set_rules('ket_id', 'Deskripsi Indonesia', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Deskripsi English', 'trim|required');
        $this->form_validation->set_rules('kategori_album', 'Kategori', 'trim|integer|required');

        /* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Gambar Cover', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah($sts);
        }
        else {
            if ( ! $this->_do_upload() )
            {
                $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
                $this->tutup_dialog_gagal($info);
            }
            else {
                if( ! $this->cms->addAlbum() )
                {
                    $sts = 'error';
                    $this->tambah($sts);
                }
                else {
                    $info = "Sukses menambahkan konten Album baru";
                    $this->tutup_dialog($info);
                }
                delete_files('./_media/album/big/');
            }
        }
    }

    public function add_gallery()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__gallery_cek');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__gallery_cek');
        $this->form_validation->set_rules('parent_album', 'Parent Album', 'trim|required');
        $this->form_validation->set_rules('ket_id', 'Deskripsi Indonesia', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Deskripsi English', 'trim|required');
        
        /* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Gambar Cover', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah_gallery($sts);
        }
        else {
			$path='./_media/album-gallery/';
			
            if ( ! $this->_do_upload($path) )
            {
                echo "Gagal Upload.";
            }
            else {
                if( ! $this->cms->addGallery() )
                {
                    $sts = 'error';
                    $this->tambah_gallery($sts);
                }
                else {
                    $sts = "Sukses, menambahkan gallery baru.";
                    $this->session->set_flashdata('info', $sts);
                    redirect($this->config->item('admpath').'/album/tabel_gallery/'.$this->input->post('parent_album'),'refresh');
                }
                delete_files('./_media/album-gallery/big/');
            }
        }
    }

    public function add_gallery_album()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|required|callback__gallery_cek');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|required|callback__gallery_cek');
        $this->form_validation->set_rules('parent_album', 'Parent Album', 'trim|required');
        $this->form_validation->set_rules('ket_id', 'Deskripsi Indonesia', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Deskripsi English', 'trim|required');
        
        /* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Gambar Cover', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah_gallery_album($sts);
        }
        else {
			$path='./_media/album-gallery/';

            if ( ! $this->_do_upload($path) )
            {
                $sts =  "Gagal Upload.";
                $this->session->set_flashdata('error', $sts);
                $this->tambah_gallery_album();
            }
            else {
                if( ! $this->cms->addGallery() )
                {
                    $sts = 'error';
                    $this->session->set_flashdata('error', $sts);
	                $this->tambah_gallery_album();
                }
                else {
                    $sts = "Sukses, menambahkan gallery baru.";
                    $this->session->set_flashdata('info', $sts);
                    redirect($this->config->item('admpath').'/album/tabel_gallery/'.$this->input->post('parent_album'),'refresh');
                }
                delete_files('./_media/album-gallery/big/');
            }
        }
    }

    public function update_album()
    {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('album_id'))
		{
			$this->form_validation->set_rules('album_id', 'ID Album', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('komentar', 'Komentar', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
			$this->form_validation->set_rules('utama', 'Utama', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Album', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('albid'))
		{
			$this->form_validation->set_rules('albid', 'ID Album', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('album_id') && $this->super_admin ) 
			{
				if( ! $this->cms->upAlbumStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Status Album..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->cms->hapusAlbum() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Album..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('albid') && $this->super_admin ) {
				if( ! $this->cms->upAlbumPos() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Posisi Album..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }

    public function update_gallery()
    {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('album_id'))
		{
			$this->form_validation->set_rules('album_id', 'ID Gallery', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Gallery', 'required|integer|max_length[11]|xss_clean');
		}
		if( $this->input->post('albid'))
		{
			$this->form_validation->set_rules('albid', 'ID Gallery', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('album_id') && $this->super_admin ) 
			{
				if( ! $this->cms->upAlbumGalleryStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Gambar Gallery..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->cms->hapusAlbumGallery() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Gambar Gallery..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('albid') && $this->super_admin ) {
				if( ! $this->cms->upAlbumGalleryPos() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Posisi Gambar Gallery..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }

    public function update_formalbum()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__update_album_cek_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__update_album_cek_en');
        $this->form_validation->set_rules('kategori_album', 'Kategori Album', 'trim|required|integer');
        //$this->form_validation->set_rules('ratting', 'Ratting', 'required|decimal');
        $this->form_validation->set_rules('ket_id', 'Keterangan ID', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Keterangan EN', 'trim|required');

        // hidden
        $this->form_validation->set_rules('nama_ori_id', 'Ori ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_ori_en', 'Ori EN', 'trim_judul|required');
        $this->form_validation->set_rules('id_album', 'ID', 'trim|required');
        $this->form_validation->set_rules('tag_ori', 'TAG', 'trim|required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail();
        }
        else {
            if($this->input->post('status_ganti') == 'yes')
            {
                if( ! $this->_do_upload() )
                {
                    $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->tutup_dialog_gagal($info);
                }
                else {
                    if( ! $this->cms->upAlbumForm() )
                    {
                        $this->detail();
                    }
                    else {
                        $file = trim($this->input->post('gambar'));
                        $this->_hapus_gambar($file);

                        $info = "Sukses memperbaharui data <strong>Album ".$this->input->post('nama_id')."</strong>";
                        $this->tutup_dialog($info);
                    }
                    delete_files('./_media/album/big/');
                }
            }
            else {
                if ( ! $this->cms->upAlbumForm() )
                {
                    $this->detail();
                }
                else {
                    $info = "Sukses memperbaharui data <strong>Album ".$this->input->post('nama_id')."</strong>";
                    $this->tutup_dialog($info);
                }
            }
        }
    }

    public function update_formgallery()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('ket_id', 'Keterangan ID', 'trim|required');
        $this->form_validation->set_rules('ket_en', 'Keterangan EN', 'trim|required');

        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|max_length[100]|required|callback__update_gallery_cek_id');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|max_length[100]|required|callback__update_gallery_cek_en');

        // hidden
        $this->form_validation->set_rules('id_album', 'ID', 'trim|required');
        $this->form_validation->set_rules('parent_album', 'Parent', 'trim|required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');
        $this->form_validation->set_rules('nama_ori_id', 'Ori ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_ori_en', 'Ori EN', 'trim_judul|required');
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail_gallery();
        }
        else {
            if($this->input->post('status_ganti') == 'yes')
            {
				$path='./_media/album-gallery/';
				
                if( ! $this->_do_upload($path) )
                {
                    $sts =  "Gagal Upload.";
					$this->session->set_flashdata('error', $sts);
					$this->detail_gallery();
                }
                else {
                    if( ! $this->cms->upAlbumGalleryForm() )
                    {
                        $sts =  "Gagal Update data.";
						$this->session->set_flashdata('error', $sts);
						$this->detail_gallery();
                    }
                    else {
                        $file = trim($this->input->post('gambar'));
                        $this->_hapus_gambar($file, $path);
                        $sts = "Sukses, Update gallery.";
						$this->session->set_flashdata('info', $sts);
						redirect($this->config->item('admpath').'/album/tabel_gallery/'.$this->input->post('parent_album'),'refresh');
                    }
                    delete_files('./_media/album-gallery/big/');
                }
            }
            else {
                if ( ! $this->cms->upAlbumGalleryForm() )
                {
                    $sts =  "Gagal Update data.";
					$this->session->set_flashdata('error', $sts);
					$this->detail_gallery();
                }
                else {
                    $sts = "Sukses, Update gallery.";
					$this->session->set_flashdata('info', $sts);
					redirect($this->config->item('admpath').'/album/tabel_gallery/'.$this->input->post('parent_album'),'refresh');
                }
            }
        }
    }

	public function list_select_kategori()
	{
		$kat = $this->cms->getKategori('album');
		echo '<option value="">-- Tidak ada --</option>';
		foreach($kat as $items)
		{
			echo '<option value="'.$items->id_kategori.'">'.$items->label_id.' | '.$items->label_en.'</option>';
		}
	}

	public function tambah_kategori()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nama_kat_id', 'Nama kategori ID', 'trim_judul|required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('nama_kat_en', 'Nama kategori EN', 'trim_judul|required|min_length[3]|max_length[50]');
		
		if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
		{
			$data = array('status'=>'Gagal, Data yang dikirim tidak valid.');
		}
		else {
			if( ! $this->cms->cekKategori($this->input->post('nama_kat_id'),'id','album'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_id')).'" sudah ada.');
			}
			elseif( ! $this->cms->cekKategori($this->input->post('nama_kat_en'),'en','album'))
			{
				$data = array('status'=>'Gagal, Nama kategori "'.ucwords($this->input->post('nama_kat_en')).'" sudah ada.');
			}
			else {
				if( ! $this->cms->addKategori('album') )
				{
					$data = array('status'=>'Gagal menambahkan data ke database.');
				}
				else {
					$data = array('status'=>'sukses', 'list'=>array(0,7));
				}
			}
		}
		echo json_encode($data);
	}

    function _kosong_folder()
    {
        // @return TRUE => Sukses , FALSE => Gagal
        $path_album = './_media/album/';
        $path_gallery = './_media/album-gallery/';
        
        // kosongkan folder jika data tabel kosong
        if( $this->cms->getCountAlbum() === NULL || $this->cms->getCountAlbum() <= 0)
        {
            $this->load->helper('file');
            
            if (is_dir($path_album.'big'))
            delete_files($path_album.'big/');
            if (is_dir($path_album.'large'))
            delete_files($path_album.'large/');
            if (is_dir($path_album.'medium'))
            delete_files($path_album.'medium/');
            if (is_dir($path_album.'small'))
            delete_files($path_album.'small/');
            if (is_dir($path_album.'thumb'))
            delete_files($path_album.'thumb/');

            if (is_dir($path_gallery.'big'))
            delete_files($path_gallery.'big/');
            if (is_dir($path_gallery.'large'))
            delete_files($path_gallery.'large/');
            if (is_dir($path_gallery.'medium'))
            delete_files($path_gallery.'medium/');
            if (is_dir($path_gallery.'small'))
            delete_files($path_gallery.'small/');
            if (is_dir($path_gallery.'thumb'))
            delete_files($path_gallery.'thumb/');
        }
    }

    function _hapus_gambar($file='',$path_album = './_media/album/')
    {
        // hapus gambar yang lama
        if (file_exists($path_album.'large/large_'.$file) && is_writable($path_album.'large/large_'.$file))
            unlink($path_album.'large/large_'.$file);
        if (file_exists($path_album.'medium/medium_'.$file) && is_writable($path_album.'medium/medium_'.$file))
            unlink($path_album.'medium/medium_'.$file);
        if (file_exists($path_album.'small/small_'.$file) && is_writable($path_album.'small/small_'.$file))
            unlink($path_album.'small/small_'.$file);
        if (file_exists($path_album.'thumb/thumb_'.$file) && is_writable($path_album.'thumb/thumb_'.$file))
            unlink($path_album.'thumb/thumb_'.$file);
    }

    function _do_upload($path='./_media/album/')
    {
        $this->load->helper('file');

        if ( ! is_dir($path.'big') )
			mkdir($path.'big', DIR_CMS_MODE);

		if ( ! is_dir($path.'large') )
			mkdir($path.'large', DIR_CMS_MODE);

		if ( ! is_dir($path.'medium') )
			mkdir($path.'medium', DIR_CMS_MODE);

		if ( ! is_dir($path.'small') )
			mkdir($path.'small', DIR_CMS_MODE);

		if ( ! is_dir($path.'thumb') )
			mkdir($path.'thumb', DIR_CMS_MODE);
        
        $config['file_name']  = time().'_'.$this->input->post('nama_id');
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '1024';
        $config['max_width']  = '1800';
        $config['max_height']  = '1600';
        $config['remove_spaces']  = TRUE;
        $config['upload_path'] = $path.'big/';
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            print_r($this->upload->display_errors());
            delete_files($path.'big/');
            return FALSE;
        }
        else {
            // sukses upload => buat ukuran yang lainnya
            
            $config = array();
            // large
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'large/large_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1200;
            $config['height'] = 768;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            
            $config = array();
            // medium
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'medium/medium_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 800;
            $config['height'] = 600;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            
            $config = array();
            // small
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'small/small_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 320;
            $config['height'] = 240;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            
            $config = array();
            // thumb
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.'big/'.$this->upload->file_name;
            $config['new_image'] = $path.'thumb/thumb_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 144;
            $config['height'] = 96;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            
            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                if (is_dir($path.'big'))
                delete_files($path.'big/');
                return FALSE;
            }
            
            return TRUE;
        }
    }

    function _album_cek($str)
    {
        if ($this->cms->adaAlbum($str) == FALSE)
        {
            $this->form_validation->set_message('_album_cek', 'Album dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_album_cek_id($str)
    {
        if ($this->cms->adaUpdateAlbum($str,$this->input->post('nama_ori_id'),'id') == FALSE)
        {
            $this->form_validation->set_message('_update_album_cek_id', 'Gallery dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_album_cek_en($str)
    {
        if ($this->cms->adaUpdateAlbum($str,$this->input->post('nama_ori_en'),'en') == FALSE)
        {
            $this->form_validation->set_message('_update_album_cek_en', 'Gallery dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _gallery_cek($str)
    {
        if ($this->cms->adaGallery($str) == FALSE)
        {
            $this->form_validation->set_message('_gallery_cek', 'Gallery dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_gallery_cek_id($str)
    {
        if ($this->cms->adaUpdateGallery($str,$this->input->post('nama_ori_id'),'id') == FALSE)
        {
            $this->form_validation->set_message('_update_gallery_cek_id', 'Gallery dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    function _update_gallery_cek_en($str)
    {
        if ($this->cms->adaUpdateGallery($str,$this->input->post('nama_ori_en'),'en') == FALSE)
        {
            $this->form_validation->set_message('_update_gallery_cek_en', 'Gallery dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }


}

/* End of file album.php */
/* Location: ./application/controllers/admin/album.php */

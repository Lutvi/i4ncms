<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Atur_menu extends AdminController {
	
	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Menu';

		$config['base_url'] = base_url().$this->config->item('admpath').'/atur_menu';
		$config['total_rows'] = $this->cms->getCountMenu();
		$config['per_page'] = $this->adm_per_halaman;
		$config['uri_segment'] = 3;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['menu'] = $this->cms->getAllMenu($config['per_page'],$offset);
		$data['nama_menu'] = $this->cms->getTitleMenu();
		$data['halaman'] = $this->cms->getSelectHalaman();
		$data['page'] =  $this->pagination->create_links();
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$data['partial_content'] = 'v_atur_menu';

		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$this->load->library('pagination');

		$config['base_url'] = site_url($this->config->item('admpath').'/atur_menu/update_tabel');
		$config['total_rows'] = $this->cms->getCountMenu();
		$config['per_page'] = $this->adm_per_halaman; 
		$config['uri_segment'] = 4;
		$config = array_merge($config, $this->_adminPagination());
		$this->pagination->initialize($config);

        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
        }
        else{
            $offset = $this->uri->segment($config['uri_segment'],0);
        }
		$data['menu'] = $this->cms->getAllMenu($config['per_page'],$offset);
		$data['nama_menu'] = $this->cms->getTitleMenu();
		$data['halaman'] = $this->cms->getSelectHalaman();
		$data['page'] =  $this->pagination->create_links();
		$data['super_admin'] = $this->super_admin;
        $data['hak'] = $this->hak;
		$data['sts'] = '';

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_menu',$data);
		}else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function update_menu()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

        if( $this->input->post('menu_id'))
		{
			$this->form_validation->set_rules('status', 'Status Menu', 'required|integer|less_than[2]|xss_clean');
			$this->form_validation->set_rules('menu_id', 'ID Menu', 'required|integer|xss_clean');
			$this->form_validation->set_rules('nama_id', 'Label Indonesia', 'trim_judul|required|max_length[100]|xss_clean|callback__menu_upcek_id');
			$this->form_validation->set_rules('nama_en', 'Label Indonesia', 'trim_judul|required|max_length[100]|xss_clean|callback__menu_upcek_en');

			//hidden
			$this->form_validation->set_rules('halaman', 'Link Halaman', 'required|integer|max_length[20]|xss_clean');
			$this->form_validation->set_rules('parentid', 'Parent Menu', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('parentn', 'Parent Baru', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('nama_ori_id', 'Label Ori Indonesia', 'trim_judul|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nama_ori_en', 'Label Ori English', 'trim_judul|required|max_length[100]|xss_clean');
		}
		if ( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Menu', 'required|integer|max_length[11]|xss_clean');
		}
		if(($this->input->post('posisi') || $this->input->post('old_posisi') || $this->input->post('mnid')))
		{
			$this->form_validation->set_rules('mnid', 'ID Menu', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('posisi', 'Posisi', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('old_posisi', 'Old Posisi', 'required|integer|max_length[11]|xss_clean');
		}

        if ($this->form_validation->run($this) == FALSE || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if( $this->input->post('menu_id') && $this->input->post('nama_id') && $this->super_admin )
			{
				if( ! $this->cms->upMenuStat() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Menu, data tidak tersimpan..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->super_admin ) {
				if( ! $this->cms->hapusMenu() || ! $this->super_admin )
				{
					echo 'Gagal Hapus Menu..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif($this->input->post('posisi') && $this->input->post('old_posisi') && $this->input->post('mnid') && $this->super_admin ){
				if( ! $this->cms->upMenuPos() || ! $this->super_admin )
				{
					echo 'Gagal Ubah Posisi Menu..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
		}
	}

	function _menu_upcek_id($str)
    {
        if ($this->cms->adaMenuUpdate($str, $this->input->post('nama_ori_id'), 'id') == FALSE)
        {
            $this->form_validation->set_message('_menu_upcek_id', 'Menu Indonesia dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function _menu_upcek_en($str)
    {
        if ($this->cms->adaMenuUpdate($str, $this->input->post('nama_ori_en'), 'en') == FALSE)
        {
            $this->form_validation->set_message('_menu_upcek_en', 'Menu English dengan nama "'.$str.'" sudah terdaftar.!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

	function _menu_cek($str)
	{
		if ($this->cms->adaMenu($str) == FALSE)
		{
			$this->form_validation->set_message('_menu_cek', 'Menu dengan nama "'.$str.'" sudah terdaftar.!');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

}
/* End of file atur_menu.php */
/* Location: ./application/controllers/admin/atur_menu.php */

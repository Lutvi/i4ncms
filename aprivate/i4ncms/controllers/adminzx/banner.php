<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Banner extends AdminController {

	public function __construct()
	{
		parent::__construct();

		$this->_cekStsLoginAdm();
		$this->hak = array('superman','batman');
	}

	public function index($sts='')
	{
		$this->load->library('pagination');
		$pp = $this->adm_per_halaman;

		$data = $this->_getAdminAssets();
		$data['title'] = 'Admin Banner Iklan';
		$data['login'] = $this->login;
		$data['super_admin'] = $this->super_admin;
		$data['hak'] = $this->hak;
		$data['sts'] = $sts;
		$config = $this->_adminPagination();
		
		$iklan_sidebar['base_url'] = base_url().$this->config->item('admpath').'/banner/iklan_sidebar';
		$iklan_sidebar['total_rows'] = $this->web->getCountIklan('sidebar');
		$iklan_sidebar['per_page'] = $pp;
		$iklan_sidebar['uri_segment'] = $this->_getSegment('iklan_sidebar');
		$iklan_sidebar = array_merge($iklan_sidebar, $config);

		$iklan_utama['base_url'] = base_url().$this->config->item('admpath').'/banner/iklan_utama';
		$iklan_utama['total_rows'] = $this->web->getCountIklan('atas');
		$iklan_utama['per_page'] = $pp;
		$iklan_utama['uri_segment'] = $this->_getSegment('iklan_utama');
		$iklan_utama = array_merge($iklan_utama, $config);

		$iklan_atas['base_url'] = base_url().$this->config->item('admpath').'/banner/iklan_atas';
		$iklan_atas['total_rows'] = $this->web->getCountIklan('konten_atas');
		$iklan_atas['per_page'] = $pp;
		$iklan_atas['uri_segment'] = $this->_getSegment('iklan_atas');
        $iklan_atas = array_merge($iklan_atas, $config);

        $iklan_bawah['base_url'] = base_url().$this->config->item('admpath').'/banner/iklan_bawah';
		$iklan_bawah['total_rows'] = $this->web->getCountIklan('konten_bawah');
		$iklan_bawah['per_page'] = $pp;
		$iklan_bawah['uri_segment'] = $this->_getSegment('iklan_bawah');
        $iklan_bawah = array_merge($iklan_bawah, $config);
        
        if(isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE )
        {
            $offset_sidebar = $this->getOffsetPage($this->uri->segment($iklan_sidebar['uri_segment']), $iklan_sidebar['per_page']);
            $offset_utama = $this->getOffsetPage($this->uri->segment($iklan_utama['uri_segment']), $iklan_utama['per_page']);
            $offset_atas = $this->getOffsetPage($this->uri->segment($iklan_atas['uri_segment']), $iklan_atas['per_page']);
            $offset_bawah = $this->getOffsetPage($this->uri->segment($iklan_bawah['uri_segment']), $iklan_bawah['per_page']);
        }
        else{
            $offset_sidebar = $this->uri->segment($iklan_sidebar['uri_segment'],0);
            $offset_utama = $this->uri->segment($iklan_utama['uri_segment'],0);
            $offset_atas = $this->uri->segment($iklan_atas['uri_segment'],0);
            $offset_bawah = $this->uri->segment($iklan_bawah['uri_segment'],0);
        }
        
		$data['iklan_sidebar'] = $this->web->getAllIklan('sidebar',$iklan_sidebar['per_page'],$offset_sidebar);
		$data['iklan_utama'] = $this->web->getAllIklan('atas',$iklan_utama['per_page'],$offset_utama);
		$data['iklan_atas'] = $this->web->getAllIklan('konten_atas',$iklan_atas['per_page'],$offset_atas);
		$data['iklan_bawah'] = $this->web->getAllIklan('konten_bawah',$iklan_bawah['per_page'],$offset_bawah);
		
		$this->pagination->initialize($iklan_sidebar);
		$data['page_sidebar'] =  $this->pagination->create_links();

		$this->pagination->initialize($iklan_utama);
		$data['page_utama'] =  $this->pagination->create_links();

		$this->pagination->initialize($iklan_atas);
		$data['page_atas'] =  $this->pagination->create_links();

		$this->pagination->initialize($iklan_bawah);
		$data['page_bawah'] =  $this->pagination->create_links();

		$data['partial_content'] = 'v_banner';
        
		/* Get Views */
		if( empty($sts) )
		{
			$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
		}
		else {
			return $data;
		}
	}

	public function cek_tab()
	{
		$aktif = $this->input->get('aktif_tab_banner');
		$this->session->set_userdata('aktif_tab_banner', $aktif);
		echo $aktif;
	}

	public function iklan_sidebar()
	{
		$data = $this->index('iklan_sidebar');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function iklan_utama()
	{
		$data = $this->index('iklan_utama');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function iklan_atas()
	{
		$data = $this->index('iklan_atas');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function iklan_bawah()
	{
		$data = $this->index('iklan_bawah');
		$this->load->view($this->config->item('admin_theme_id').'/main/adminMain',$data);
	}

	public function update_tabel()
	{
		$data = $this->index('ajax');

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak)) {
			$this->load->view('dynamic_js',$data);
			$this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_iklan',$data);
		}
		else
			$this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
	}

	public function tambah_iklan($sts='')
    {
        $data['js'] = array('jqui/jquery-1.8.3-min.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/jquery.iframeDialog-min.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/table-style.css');
        $data['sts'] = $sts;

		if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/tambah_iklan',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function add_iklan()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('nama_pengiklan', 'Nama Pengiklan', 'trim_judul|required');
		$this->form_validation->set_rules('kredit', 'Tarif', 'required|numeric');
		$this->form_validation->set_rules('url_target', 'URL Target', 'required|prep_url');
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|required');
        $this->form_validation->set_rules('tgl_terbit', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tgl_berakhir', 'Tanggal berakhir', 'required');
        $this->form_validation->set_rules('posisi', 'Posisi Iklan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        /* Cek gambar */
        if(empty($_FILES['userfile']['name'][0]))
        {
            $this->form_validation->set_rules('userfile', 'Gambar Iklan', 'trim|required');
        }
        
        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $sts = 'error';
            $this->tambah_iklan($sts);
        }
        else {
            if ( ! $this->_do_upload_iklan() )
            {
                $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
                $this->tutup_dialog_gagal($info);
            }
            else {
                if( ! $this->web->addIklan() )
                {
                    $sts = 'error';
                    $this->tambah_iklan($sts);
                }
                else {
                    $info = "Sukses menambahkan Iklan baru";
                    $this->tutup_dialog($info);
                }
				hapus_file('./_media/banner-iklan/'.$this->upload->file_name);
            }
        }
    }

    public function update_iklan()
    {
    	$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error kiri">', '</div>');

		if( $this->input->post('iklan_id'))
		{
			$this->form_validation->set_rules('iklan_id', 'ID Iklan', 'required|integer|max_length[11]|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|max_length[3]|xss_clean');
		}
		if( $this->input->post('id_hps'))
		{
			$this->form_validation->set_rules('id_hps', 'ID Iklan', 'required|integer|max_length[11]|xss_clean');
		}

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
			echo '<h5 class="infoErr">Terjadi kesalahan data.!</h5>'.validation_errors();
        }
        else {
			if ( $this->input->post('iklan_id') && $this->_isSupAdmin() ) 
			{
				if( ! $this->web->upIklanStat() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Ubah Status Iklan..!!';
				}
				else {
					echo 'sukses';
				}
			}
			elseif ( $this->input->post('id_hps') && $this->_isSupAdmin() ) {
				if( ! $this->web->hapusIklan() || ! $this->_isSupAdmin() )
				{
					echo 'Gagal Hapus Iklan,<br>Iklan ini masih memiliki masa tayang..!!';
				}
				else {
					echo 'sukses';
				}
			}
			else {
				echo 'Proses Gagal, Anda tidak memiliki otoritas aksi ini.<br> Data yang terkirim tidak valid.';
			}
        }
    }

    public function detail_iklan()
    {
        $id = $this->uri->segment(4,$this->input->post('id_iklan'));
        
        $data['iklan'] = $this->web->getIklanById($id);
        $data['js'] = array('jqui/jquery-1.8.3.js','jqui/jquery-ui-1.9.2.custom.min.js','jqui/magicsuggest-1.3.1.js');
        $data['css'] = array($this->config->item('admin_theme_id').'-min.css','elem/table-style.css','themes/'.$this->frame_jq_style.'/jquery-ui.css','elem/magicsuggest-1.3.1.css');
        $data['sts'] = '';

        if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        $this->load->view($this->config->item('admin_theme_id').'/ajax/form/update_iklan',$data);
        else
        $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten',$data);
    }

    public function update_formiklan()
    {
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('nama_pengiklan', 'Nama Pengiklan', 'trim_judul|required');
		$this->form_validation->set_rules('kredit', 'Tarif', 'required|numeric');
		$this->form_validation->set_rules('url_target', 'URL Target', 'required|prep_url');
        $this->form_validation->set_rules('nama_id', 'Nama ID', 'trim_judul|required');
        $this->form_validation->set_rules('nama_en', 'Nama EN', 'trim_judul|required');
        $this->form_validation->set_rules('tgl_terbit', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tgl_berakhir', 'Tanggal berakhir', 'required');
        $this->form_validation->set_rules('posisi', 'Posisi Iklan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        // hidden
        $this->form_validation->set_rules('id_iklan', 'ID', 'trim|required');
        $this->form_validation->set_rules('status_ganti', 'Status Ganti', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');

        if ($this->form_validation->run($this) == FALSE || ! $this->super_admin || ! in_array(bs_kode($this->session->userdata('level'), TRUE) ,$this->hak))
        {
            $this->detail_iklan();
        }
        else {
            if($this->input->post('status_ganti') == 'yes')
            {
                if( ! $this->_do_upload_iklan() )
                {
                    $info = "Gagal Upload Gambar, pastikan ukuran dan jenis file yang diupload sudah sesuai";
					$this->tutup_dialog_gagal($info);
                }
                else {
                    if( ! $this->web->upIklanForm() )
                    {
                        $info = "Gagal Update Iklan, terjadi kesalahan pada server";
						$this->tutup_dialog_gagal($info);
                    }
                    else {
                        $info = "Sukses memperbaharui data <strong>Iklan ".$this->input->post('nama_id')."</strong>";
                        $this->tutup_dialog($info);
                    }
                    hapus_file('./_media/banner-iklan/'.$this->upload->file_name);
                }
            }
            else {
                if ( ! $this->web->upIklanForm() )
                {
                    $info = "Gagal Update Iklan, terjadi kesalahan pada server";
					$this->tutup_dialog_gagal($info);
                }
                else {
                    $info = "Sukses memperbaharui data <strong>Iklan ".$this->input->post('nama_id')."</strong>";
                    $this->tutup_dialog($info);
                }
            }
        }
    }

    function _do_upload_iklan($path='./_media/banner-iklan/')
	{
		$this->load->helper('file');

		if ( ! is_dir($path) )
			mkdir($path, DIR_CMS_MODE);
		if ( ! is_dir($path.'medium') )
			mkdir($path.'medium', DIR_CMS_MODE);
		if ( ! is_dir($path.'thumb') )
			mkdir($path.'thumb', DIR_CMS_MODE);

		$config['file_name']  = time().'_'.$this->input->post('nama_id');
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';
        $config['max_width']  = '1280';
        $config['max_height']  = '1024';
        $config['remove_spaces']  = TRUE;
        $config['upload_path'] = $path;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			print_r($this->upload->display_errors());
            hapus_file($path.$this->upload->file_name);
            return FALSE;
		}
		else {

            $config = array();
            // medium
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'medium/medium_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 690;
            $config['height'] = 340;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();

            $config = array();
            // thumb
            //$config['image_library'] = 'GD2';
            $config['source_image'] = $path.$this->upload->file_name;
            $config['new_image'] = $path.'thumb/thumb_'.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 80;
            $config['height'] = 66;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            if (! $this->image_lib->resize())
            {
                print_r($this->image_lib->display_errors());
                hapus_file($path.$this->upload->file_name);
                return FALSE;
            }
			return TRUE;
		}
	}

}

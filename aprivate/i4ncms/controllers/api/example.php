<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Example extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();

		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->response(array('status' => 0, 'error' => 'Maaf Layanan untuk sementara belum dapat diakses, kami sedang melakukan perawatan website.'), 400);
			exit();
		}
	}

	function user_get()
    {
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }

        // $user = $this->some_model->getSomething( $this->get('id') );
    	$users = array(
			1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
			2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!', array('hobbies' => array('fartings', 'bikes'))),
		);
		
    	$user = @$users[$this->get('id')];
    	
        if($user)
        {
            $this->response($user, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
    }
    
    function user_post()
    {
        //$this->some_model->updateUser( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function user_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function users_get()
    {
		#url : http://reddanio.localhost.com/api/example/user/id/1
        //$users = $this->some_model->getSomething( $this->get('limit') );
        $users = array(
			'contacts' => array(
				array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com','address' => 'xx-xx-xxxx,x - street, x - country', 'gender' => 'male', 'phone' => array('mobile' => '+91 0000000000','home' => '00 000000','office' => '00 000000')
				),
				array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com','address' => 'xx-xx-xxxx,x - street, x - country', 'gender' => 'male', 'phone' => array('mobile' => '+91 0000000000','home' => '00 000000','office' => '00 000000')
				),
				array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com','address' => 'xx-xx-xxxx,x - street, x - country', 'gender' => 'male', 'phone' => array('mobile' => '+91 0000000000','home' => '00 000000','office' => '00 000000')
				)
			)
		);

        if($users)
        {
            $this->response($users, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Couldn\'t find any users!'), 404);
        }
    }

    function posts_get()
    {
		#url : http://reddanio.localhost.com/api/example/post/item/1
		#url-encypt : http://reddanio.localhost.com/api/example/posts/item/bbcea6cd1c076892492932803e0de0f2
		$id = bs_kode($this->get('item'),true);
		$posts = $this->cms->getAllBlogFrontById($id);

		if($posts)
		{
			$this->response($posts, 200); // 200 being the HTTP response code
		}
		else
		{
			$this->response(array('error' => 'Couldn\'t find any post!'), 404);
		}
    }


	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}

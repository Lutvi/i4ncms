<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdetail extends FrontController {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$nama = humanize($this->uri->segment(3));
			$produk = $this->mproduk->getAllDetailFrontProdByNama($nama);

			$data['produk_js'] = array('zoom/multizoom.js');
			$data['produk_css'] = array('zoom/multizoom.css');
			$data['tipe_produk'] = 'induk';

			$data['tipe'] = 'produk';
			$data['produk_page'] =  FALSE;

			//unset
			$rel_data = array('pbasis' => '', 'albbasis' => '', 'vidbasis' => '', 'blogbasis' => '', 'fbasis' => '', 'pgbasis' => '', 'nama_halaman' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			//by prod id
			if( ! is_null($produk))
			{
				$data['content'] =  $this->mproduk->getAllFrontProdukById($produk->id_prod,1,0);
				$data['kat_konten'] = $this->cms->getKategoriKonten();
				$data['per_page'] = 1;

				//update hits
				$this->mproduk->upProdukHits($produk->id_prod,'produk');

				//session kategori
				$this->session->set_userdata('kat_konten',$this->cms->getNamaKategoriById($produk->kategori_id,current_lang(false)));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = $produk->nama_prod;
					$this->session->set_userdata('nama_halaman', underscore($produk->nama_prod_en));
					$description = $produk->nama_prod.' '.word_limiter($produk->deskripsi,30);
					$extra = $produk->nama_prod;
				}
				else {
					$data['title'] = $produk->nama_prod_en;
					$this->session->set_userdata('nama_halaman', underscore($produk->nama_prod));
					$description = $produk->nama_prod_en.' '.word_limiter($produk->deskripsi_en,30);
					$extra = $produk->nama_prod_en;
				}

				//QrCode
				$data['qrcode'] = $this->_buatImgQrCode(current_url());
			}
			else {
				$this->load->library('pagination');

				$config['base_url'] = base_url().current_lang().'pdetail';
				$config['per_page'] = $this->per_halaman; 
				$config['uri_segment'] = 3;
				$config['total_rows'] = $this->mproduk->getCountFrontProdukHandler($this->per_halaman);
				$config = array_merge($config, $this->_frontPagination());
				if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
				{
					$this->pagination->initialize($config);
					$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
					$offset = $this->getOffsetPage($segment, $config['per_page']);
				}
				else {
					unset($config['prefix']);
					unset($config['suffix']);
					$this->pagination->initialize($config);
					$offset = $this->uri->segment($config['uri_segment'],0);
				}

				$data['produk_page'] =  $this->pagination->create_links();
				$data['per_page'] = $config['per_page'];
				$data['content'] = $this->mproduk->getAllFrontProdukHandler($config['per_page'],$offset);
				$this->session->set_userdata('nama_halaman', $this->uri->segment(3,'pdetail'));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = 'Produk';
					$description = $data['content'][0]->nama_prod.' '.word_limiter($data['content'][0]->deskripsi,30);
					$extra = $data['content'][0]->nama_prod;
				}
				else {
					$data['title'] = 'Product';
					$description = $data['content'][0]->nama_prod_en.' '.word_limiter($data['content'][0]->deskripsi_en,30);
					$extra = $data['content'][0]->nama_prod_en;
				}
			}

			$data = array_merge($this->_getFrontAssets(), $data, $this->_getSEO('',$description,$extra));

			$cache = $this->_getCachenya('produk',$data);
			$cache['login'] = $this->_cekLogin();

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

}

/* End of file pdetail.php */
/* Location: ./cms/controllers/frontpage/pdetail.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends FrontController {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			//unset
			$rel_data = array('kpbasis' => '', 'kalbbasis' => '', 'talbbasis' => '', 'kvidbasis' => '', 'tvidbasis' => '', 'kblogbasis' => '', 'tblogbasis' => '', 'fbasis' => '', 'tipepg' => '', 'nmpg' => '','key_cari_front' => '','blogbasis' => '','vidbasis' => '','albbasis' => '','pbasis' => '','pgbasis' => '');
			$this->session->unset_userdata($rel_data);

			// pagination base_url untuk module/widget
			$this->session->set_userdata('fbasis',current_lang().'home');
			$this->session->set_userdata('nama_halaman', 'home');

			$data = $this->_getFrontAssets();
			// title
			$data['title'] = 'Homepage';
			// modules
			$data['modules'] = $this->cms->getModuleName();
			$data['assets_module'] =  $this->cms->getModule();

			$data['content'] = '';
			$data['tipe'] = 'home';

			$data['fancybox_js'] = array('jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			$jml = $this->jknt_home;
			$data['jml_page'] = $jml;

			// total jumlah @konten
			$data['jml_produk'] = $this->cms->cariHomeKonten(TRUE,'produk');
			$data['jml_blog'] = $this->cms->getCountBlogFrontHandler();
			$data['jml_album'] = $this->cms->getCountAlbumFrontHandler();
			$data['jml_video'] = $this->cms->getCountVideoFrontHandler();

			$data['produk'] = $this->cms->cariHomeKonten(FALSE,'produk',$jml);
			$data['blog'] = $this->cms->cariHomeKonten(FALSE,'blog',$jml);
			$data['album'] = $this->cms->cariHomeKonten(FALSE,'album',$jml);
			$data['video'] = $this->cms->cariHomeKonten(FALSE,'video',$jml);

			$cache = $this->_getCachenya('home',$data);

			if ( ($this->mobiledetection->isMobile() && config_item('is_respon') === FALSE) || ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

	public function cari()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$data = $this->_getFrontAssets();
			$data['noindex'] = TRUE;
			$data['fancybox_js'] = array('jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			if($this->input->post('keyword_ext'))
			$this->session->set_userdata('key_cari_front', $this->input->post('keyword_ext',TRUE));

			$key = $this->session->userdata('key_cari_front');
			$data['produk'] = $this->cms->cariKonten('produk', $key, 10, 0);
			$data['blog'] = $this->cms->cariKonten('blog', $key, 10, 0);
			$data['album'] = $this->cms->cariKonten('album', $key, 10, 0);
			$data['video'] = $this->cms->cariKonten('video', $key, 10, 0);

			$data['tipe'] = 'cari';
			$data['title'] = $this->lang->line('lbl_hasil_cari').' - '.humanize($key);

			if(current_lang(false) === 'en')
			$this->session->set_userdata('nama_halaman', 'cari');
			else
			$this->session->set_userdata('nama_halaman', 'search');

			//unset
			$rel_data = array('kpbasis' => '', 'kalbbasis' => '', 'talbbasis' => '', 'kvidbasis' => '', 'tvidbasis' => '', 'kblogbasis' => '', 'tblogbasis' => '', 'fbasis' => '', 'tipepg' => '', 'nmpg' => '');
			$this->session->unset_userdata($rel_data);

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $data, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $data, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $data, config_item('theme_id').'/main/');
			}
		}
	}

	public function sitemap()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$data = $this->_getFrontAssets();
			$data['tipe'] = 'sitemap';
			$data['title'] = 'Sitemap';

			$this->session->set_userdata('nama_halaman', 'sitemap');

			//unset
			$rel_data = array('kpbasis' => '', 'kalbbasis' => '', 'talbbasis' => '', 'kvidbasis' => '', 'tvidbasis' => '', 'kblogbasis' => '', 'tblogbasis' => '', 'fbasis' => '', 'tipepg' => '', 'nmpg' => '','key_cari_front' => '','blogbasis' => '','vidbasis' => '','albbasis' => '','pbasis' => '','pgbasis' => '');
			$this->session->unset_userdata($rel_data);

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $data, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $data, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				if($this->uri->segment(2) == 'sitemapxml')
				$this->load->view_theme('sitemapxml', $data, config_item('theme_id').'/main/');
				else
				$this->load->view_theme('main', $data, config_item('theme_id').'/main/');
			}
		}
	}

	public function halaman()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {

			//unset
			$rel_data = array('kpbasis' => '', 'kalbbasis' => '', 'talbbasis' => '', 'kvidbasis' => '', 'tvidbasis' => '', 'kblogbasis' => '', 'tblogbasis' => '', 'tipepg' => '', 'nmpg' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			$data = array_merge($this->_getFrontAssets(), $this->_getKonten());
			$cache = $this->_getCachenya($data['tipe'], $data);
			$cache['login'] = $this->_cekLogin();

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) || ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

	public function logout()
	{
		$this->keluar();
	}

	public function kategori()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$tipe = $this->uri->segment(3,'blog');

			if($tipe == 'product')
			$tipe = 'produk';

			//unset
			$rel_data = array('pbasis' => '', 'albbasis' => '', 'talbbasis' => '', 'vidbasis' => '', 'tvidbasis' => '', 'blogbasis' => '', 'tblogbasis' => '', 'fbasis' => '', 'pgbasis' => '', 'nama_halaman' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			//session kategori
			$this->session->set_userdata('kat_konten',humanize($this->uri->segment(4)));

			$data['content_data'] = array_merge($this->_getFrontAssets(), $this->_getKontenKategori($tipe));
			$cache = $this->_getCachenya($tipe,$data['content_data']);
			$cache['login'] = $this->_cekLogin();

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

	public function tag()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$tipe = $this->uri->segment(3,'blog');
			$tag = $this->uri->segment(4);

			$data['content_data'] = array_merge($this->_getFrontAssets(),$this->_getKontenTag($tipe,$tag));

			$cache = $this->_getCachenya($tipe,$data['content_data']);
			$cache['login'] = $this->_cekLogin();

			//session kategori
			$this->session->set_userdata('tag_konten',humanize($this->uri->segment(4)));

			//unset
			$rel_data = array('pbasis' => '', 'kpbasis' => '', 'albbasis' => '', 'kalbbasis' => '', 'vidbasis' => '', 'kvidbasis' => '', 'blogbasis' => '', 'kblogbasis' => '', 'fbasis' => '', 'pgbasis' => '', 'nama_halaman' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

	public function sistem_error()
	{
		$this->load->view('partial/sys_error');
	}

	function _cek_formBayar()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="errors">', '</div>');

		$this->form_validation->set_rules('metode', $this->lang->line('lbl_metode_pembayaran'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('pilihan', $this->lang->line('lbl_pilihan_pembayaran'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('notes', $this->lang->line('lbl_notes_pesanan'), 'trim|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _cek_formPesan()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="errors">', '</div>');

		$this->form_validation->set_rules('nama_cust', $this->lang->line('lbl_nama_penerima'), 'trim_judul|required|xss_clean');
		$this->form_validation->set_rules('no_telp', $this->lang->line('lbl_no_tlp_penerima'), 'trim|required|max_length[14]|xss_clean');
		$this->form_validation->set_rules('negara', $this->lang->line('lbl_negara_tujuan'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('provinsi', $this->lang->line('lbl_prov_tujuan'), 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('wilayah', $this->lang->line('lbl_wil_tujuan'), 'trim|integer|required|xss_clean');
		$this->form_validation->set_rules('kode_pos', $this->lang->line('lbl_kdpos_tujuan'), 'trim|required|max_length[10]|xss_clean');
		$this->form_validation->set_rules('alamat', $this->lang->line('lbl_alamat_tujuan'), 'trim|required|xss_clean|callback__cek_alamat');

		$this->form_validation->set_rules('email', $this->lang->line('lbl_email_penerima'), 'trim|valid_email|required|xss_clean|callback__ini_valid_email');

		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	function _cek_alamat($str)
	{
		if ($str === config_item('format_alamat'))
		{
			$this->form_validation->set_message('_cek_alamat', $this->lang->line('lbl_cek_alamat_tujuan'));
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

}

/* End of file home.php */
/* Location: ./cms/controllers/frontpage/home.php */

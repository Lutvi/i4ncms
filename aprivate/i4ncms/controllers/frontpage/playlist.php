<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Playlist extends FrontController {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$this->load->spark('video_helper/1.0.6');

			$nama = humanize($this->uri->segment(3));
			$video = $this->cms->getAllVideoFrontByNama($nama);

			$data['fancybox_js'] = array('jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');
			$data['tipe'] = 'video';

			//unset
			$rel_data = array('pbasis' => '', 'albbasis' => '', 'vidbasis' => '', 'blogbasis' => '', 'fbasis' => '', 'pgbasis' => '', 'nama_halaman' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			if( ! is_null($video))
			{
				$data['per_page'] = 1;
				$data['kat_konten'] = $this->cms->getKategoriKonten();

				// by album id
				$data['video'] = $this->cms->getAllVideoFrontById($video->id);
				// update hits
				$this->cms->upHitsKonten($video->id,'video');
				//tag konten
				$data['tag_konten'] = $this->cms->getTagsLabelByObjekId($video->id,'video');

				//simpan session kategori
				$this->session->set_userdata('kat_konten',$this->cms->getNamaKategoriById($video->kategori_id,current_lang(false)));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = $video->nama_id;
					$this->session->set_userdata('nama_halaman', underscore($video->nama_en));
					$description = $video->nama_id.' '.word_limiter($video->ket_id,30);
					$extra = $video->nama_id;
				}
				else {
					$data['title'] = $video->nama_en;
					$this->session->set_userdata('nama_halaman', underscore($video->nama_id));
					$description = $video->nama_en.' '.word_limiter($video->ket_en,30);
					$extra = $video->nama_en;
				}
			}
			else {
				$this->load->library('pagination');

				$config['base_url'] = base_url().current_lang().'playlist';
				$config['per_page'] = $this->per_halaman; 
				$config['uri_segment'] = 3;

				$config['total_rows'] = $this->cms->getCountVideoFrontHandler();
				$config = array_merge($config, $this->_frontPagination());
				if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
				{
					$this->pagination->initialize($config);
					$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
					$offset = $this->getOffsetPage($segment, $config['per_page']);
				}
				else {
					unset($config['prefix']);
					unset($config['suffix']);
					$this->pagination->initialize($config);
					$offset = $this->uri->segment($config['uri_segment'],0);
				}
	
				$data['video_page'] =  $this->pagination->create_links();
				$data['per_page'] = $config['per_page'];
				$data['video'] = $this->cms->getAllVideoFrontHandler($config['per_page'],$offset);
				$this->session->set_userdata('nama_halaman', $this->uri->segment(3,'playlist'));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = 'Video';
					$description = $data['video'][0]->nama_id.' '.word_limiter($data['video'][0]->ket_id,30);
					$extra = $data['video'][0]->nama_id;
				}
				else {
					$data['title'] = 'Videos';
					$description = $data['video'][0]->nama_en.' '.word_limiter($data['video'][0]->ket_en,30);
					$extra = $data['video'][0]->nama_en;
				}
			}

			$data['content'] = '';
			$data = array_merge($this->_getFrontAssets(), $data, $this->_getSEO('',$description,$extra));

			//QrCode
			$data['qrcode'] = $this->_buatImgQrCode(current_url());

			$cache = $this->_getCachenya('video',$data);
			$cache['login'] = $this->_cekLogin();

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

}

/* End of file playlist.php */
/* Location: ./cms/controllers/frontpage/playlist.php */

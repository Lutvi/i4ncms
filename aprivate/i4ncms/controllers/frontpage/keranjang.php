<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keranjang extends FrontController 
{
    function __construct()
    {
        parent::__construct();
        // without compressing content
        $this->config->set_item('compress_output', FALSE);

        if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
        redirect('/home','refresh');
    }
    
    public function tambah()
    {
        $pid = bs_kode($this->input->post('id'),TRUE);
        $harga = bs_kode($this->input->post('price'),TRUE);
        $cek = $this->mproduk->cekStokProduk((int)$pid);
        $hitung = ($cek->stok - $this->input->post('qty', TRUE));

		// cek data is valid
        if (is_numeric($pid) && is_numeric($harga))
        {
			//print_r($hitung);
			if ($hitung >= 0)
			{
				$produk = array(
							   'id'      => (int)$pid,
							   'qty'     => (int)$this->input->post('qty', TRUE),
							   'price'   => (int)$harga,
							   'name'    => $this->input->post('name', TRUE)
							);
				$this->cart->insert($produk);
				$data['infok'] = $this->_cek_stok_order();
				if (empty($data['infok']))
				{
					if (current_lang(false) === 'id')
					$data['infok'][0] = 'Produk '.$this->input->post('name').' berhasil ditambahkan.';
					else
					$data['infok'][0] = $this->input->post('name').' is added to your cart.';
				}
			}
			else {
				$data['infok'] = $this->_cek_stok();
				if (empty($data['infok']))
				{
					if (current_lang(false) === 'id')
					$data['infok'][0] = 'Mohon maaf, Produk '.$this->input->post('name').' belum tersedia.';
					else
					$data['infok'][0] = 'Sorry, '.$this->input->post('name').' is out of stock.';
				}
			}

			//update hits produk
			$this->mproduk->upProdukHits((int)$pid);
			$this->load->view_theme('keranjang', $data, $this->config->item('theme_id').'/ajax/');
		}
		else {
			echo 'error';
		}
    }
    
    public function update()
    {
        $this->cart->update($this->input->post());
        $data['infok'] = array_merge($this->_cek_stok(),$this->_cek_stok_order());
        if (empty($data['infok']))
        {
			$data['infok'][0] = $this->lang->line('lbl_update_keranjang');
        }
        $this->load->view_theme('keranjang', $data, $this->config->item('theme_id').'/ajax/');
    }
    
    public function batal()
    {
        $this->cart->destroy();
        $this->load->view_theme('keranjang', '', $this->config->item('theme_id').'/ajax/');
    }
    
    public function list_keranjang()
    {
        $data['info'] = array_merge($this->_cek_stok(),$this->_cek_stok_order());
        if (empty($data['info']))
        {
            $data['title'] = $this->lang->line('lbl_pemesanan');
        } else {
            $data['title'] = $this->lang->line('lbl_pemesanan_gagal');
        }
        $this->load->view_theme('list_keranjang', $data, $this->config->item('theme_id').'/partial/order/');
    }
    
    public function form_pesan()
    {
        $data['info'] = array_merge($this->_cek_stok(),$this->_cek_stok_order());
        if (empty($data['info']))
        {
            $data['title'] = $this->lang->line('lbl_proses_pemesanan');
			// set input text
			if ( $this->session->userdata('kunci_id') === FALSE  )
			{
				$data['nama'] = '';$data['tlp'] = '';$data['email'] = '';
				$data['negara'] = '';$data['prov'] = '';$data['wil'] = '';$data['pos'] = '';
				$data['alamat'] = $this->config->item('format_alamat');
			}
			else {
				$custdata = $this->orderan->getPembeliById(bs_kode($this->session->userdata('kunci_id'), TRUE));
				$data['nama'] = dekodeString($custdata->nama_lengkap);
				$data['tlp'] = dekodeString($custdata->no_telpon);
				$data['email'] = dekodeString($custdata->email_cust);
				$data['negara'] = $custdata->id_negara;
				$data['prov'] = $custdata->id_provinsi;
				$data['wil'] = $custdata->id_wilayah;
				$data['pos'] = dekodeString($custdata->kode_pos);
				$almt = dekodeString($custdata->alamat_rumah);
				$data['alamat'] = str_ireplace("<br />", "\n", $almt);
			}
        }
        else {
            $data['title'] = $this->lang->line('lbl_proses_pemesanan_gagal');
        }
        $this->load->view_theme('form_pesan', $data, $this->config->item('theme_id').'/partial/order/');
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posting extends FrontController {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$data['tipe'] = 'blog';
			$nama = humanize($this->uri->segment(3));
			$blog = $this->cms->getAllBlogFrontByNama($nama);

			$data['fancybox_js'] = array('jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			//unset
			$rel_data = array('pbasis' => '', 'albbasis' => '', 'vidbasis' => '', 'blogbasis' => '', 'fbasis' => '', 'pgbasis' => '', 'nama_halaman' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			//by id
			if( ! is_null($blog))
			{
				if($blog->embed_id_video > 0)
				$this->load->spark('video_helper/1.0.6');

				$data['per_page'] = 1;
				$data['kat_konten'] = $this->cms->getKategoriKonten();

				// blog konten
				$snippet['js'] = array('shCore.js','shAutoloader.js');
				$snippet['css'] = array('shCore.css','shThemeDefault.css');
				$data['snippet'] = $snippet;

				$data['konten_blog'] = $this->cms->getAllBlogFrontById($blog->id);
				// update hits
				$this->cms->upHitsKonten($blog->id,'blog');
				//tag konten
				$data['tag_konten'] = $this->cms->getTagsLabelByObjekId($blog->id,'blog');
				//penulis
				$data['penulis'] = $this->cms->getProfilPenulisById($blog->id_penulis);

				//simpan session kategori
				$this->session->set_userdata('kat_konten',$this->cms->getNamaKategoriById($blog->kategori_id,current_lang(false)));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = $blog->judul_id;
					$this->session->set_userdata('nama_halaman', underscore($blog->judul_en));
					$keyword = $blog->keyword_id;
					$description = $blog->deskripsi_id;
					$extra = word_limiter($blog->isi_id,30);
				}
				else {
					$data['title'] = $blog->judul_en;
					$this->session->set_userdata('nama_halaman', underscore($blog->judul_id));
					$keyword = $blog->keyword_en;
					$description = $blog->deskripsi_en;
					$extra = word_limiter($blog->isi_en,30);
				}
			}
			else {
				$this->load->library('pagination');

				$config['base_url'] = base_url().current_lang().'posting';
				$config['per_page'] = $this->per_halaman; 
				$config['uri_segment'] = 3;

				$config['total_rows'] = $this->cms->getCountBlogFrontHandler();
				$config = array_merge($config, $this->_frontPagination());
				if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
				{
					$this->pagination->initialize($config);
					$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
					$offset = $this->getOffsetPage($segment, $config['per_page']);
				}
				else {
					unset($config['prefix']);
					unset($config['suffix']);
					$this->pagination->initialize($config);
					$offset = $this->uri->segment($config['uri_segment'],0);
				}

				$data['blog_page'] =  $this->pagination->create_links();
				$data['per_page'] = $config['per_page'];
				$data['konten_blog'] = $this->cms->getAllBlogFrontHandler($config['per_page'],$offset);
				$this->session->set_userdata('nama_halaman', $this->uri->segment(3,'posting'));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = 'Blog';
					$keyword = $data['konten_blog'][0]->keyword_id;
					$description = $data['konten_blog'][0]->deskripsi_id;
					$extra = word_limiter($data['konten_blog'][0]->isi_id,30);
				}
				else {
					$data['title'] = 'Blogs';
					$keyword = $data['konten_blog'][0]->keyword_en;
					$description = $data['konten_blog'][0]->deskripsi_en;
					$extra = word_limiter($data['konten_blog'][0]->isi_en,30);
				}
			}

			$data['content'] = '';
			$data = array_merge($this->_getFrontAssets(), $data, $this->_getSEO($keyword,$description,$extra));

			//QrCode
			$data['qrcode'] = $this->_buatImgQrCode(current_url());

			$cache = $this->_getCachenya('blog',$data);
			$cache['login'] = $this->_cekLogin();

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}

}

/* End of file posting.php */
/* Location: ./cms/controllers/frontpage/posting.php */

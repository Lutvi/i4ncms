<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends FrontController {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// cek status offline
		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->load->view('partial/splash');
		}
		else {
			$data['tipe'] = 'album';
			$nama = humanize($this->uri->segment(3));
			$album = $this->cms->getAllAlbumFrontByNama($nama);

			$data['fancybox_js'] = array('jquery.mousewheel-min.js','jquery.fancybox-1.3.4.pack.js');
			$data['fancybox_css'] = array('jquery.fancybox-1.3.4.css');

			//unset
			$rel_data = array('pbasis' => '', 'albbasis' => '', 'vidbasis' => '', 'blogbasis' => '', 'fbasis' => '', 'pgbasis' => '', 'nama_halaman' => '','key_cari_front' => '');
			$this->session->unset_userdata($rel_data);

			//by id
			if( ! is_null($album))
			{
				$data['per_page'] = 1;
				$data['kat_konten'] = $this->cms->getKategoriKonten();

				// by album id
				$data['gambar_album'] = $this->cms->getAllAlbumFrontById($album->id);
				// update hits
				$this->cms->upHitsKonten($album->id,'album');
				//tag konten
				$data['tag_konten'] = $this->cms->getTagsLabelByObjekId($album->id,'album');

				//simpan session kategori
				$this->session->set_userdata('kat_konten',$this->cms->getNamaKategoriById($album->kategori_id,current_lang(false)));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = $album->nama_id;
					$this->session->set_userdata('nama_halaman', underscore($album->nama_en));
					$description = $album->nama_id.' '.word_limiter($album->ket_id,30);
					$extra = $album->nama_id;
				}
				else {
					$data['title'] = $album->nama_en;
					$this->session->set_userdata('nama_halaman', underscore($album->nama_id));
					$description = $album->nama_en.' '.word_limiter($album->ket_en,30);
					$extra = $album->nama_en;
				}
			}
			else {
				$this->load->library('pagination');

				$config['base_url'] = base_url().current_lang().'gallery';
				$config['per_page'] = $this->per_halaman; 
				$config['uri_segment'] = 3;

				$config['total_rows'] = $this->cms->getCountAlbumFrontHandler();
				$config = array_merge($config, $this->_frontPagination());
				if (isset($config['use_page_numbers']) && $config['use_page_numbers'] === TRUE)
				{
					$this->pagination->initialize($config);
					$segment = $this->_getNomorPage($this->uri->segment($config['uri_segment'], 'page0'));
					$offset = $this->getOffsetPage($segment, $config['per_page']);
				}
				else {
					unset($config['prefix']);
					unset($config['suffix']);
					$this->pagination->initialize($config);
					$offset = $this->uri->segment($config['uri_segment'],0);
				}
	
				$data['album_page'] =  $this->pagination->create_links();
				$data['per_page'] = $config['per_page'];
				$data['gambar_album'] = $this->cms->getAllAlbumFrontHandler($config['per_page'],$offset);
				$this->session->set_userdata('nama_halaman', $this->uri->segment(3,'gallery'));

				// tambah seo
				if(current_lang(false) === 'id')
				{
					$data['title'] = 'Album';
					$description = $data['gambar_album'][0]->nama_id.' '.word_limiter($data['gambar_album'][0]->ket_id,30);
					$extra = $data['gambar_album'][0]->nama_id;
				}
				else {
					$data['title'] = 'Albums';
					$description = $data['gambar_album'][0]->nama_en.' '.word_limiter($data['gambar_album'][0]->ket_en,30);
					$extra = $data['gambar_album'][0]->nama_en;
				}
			}

			$data['content'] = '';
			$data = array_merge($this->_getFrontAssets(), $data, $this->_getSEO('',$description,$extra));
			//QrCode
			$data['qrcode'] = $this->_buatImgQrCode(current_url());

			$cache = $this->_getCachenya('album',$data);
			$cache['login'] = $this->_cekLogin();

			// Cek-Devices
			if ( ($this->mobiledetection->isMobile()  && config_item('is_respon') === FALSE) OR ENVIRONMENT === 'm-testing' )
			{
				if (config_item('slider_menu') === TRUE)
				{
					$this->load->view_theme('main_default', $cache, 'mobile_'.config_item('theme_id').'/');
				}
				else {
					$this->load->view_theme('main', $cache, 'mobile_'.config_item('theme_id').'/');
				}
			}
			else {
				$this->load->view_theme('main', $cache, config_item('theme_id').'/main/');
			}
		}
	}
}

/* End of file gallery.php */
/* Location: ./cms/controllers/frontpage/gallery.php */

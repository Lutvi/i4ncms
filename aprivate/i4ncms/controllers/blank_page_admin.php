<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blank_page_admin extends AdminController {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->track();
		$this->load->view('partial/error');
	}

}

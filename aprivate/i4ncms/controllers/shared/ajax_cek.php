<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_cek extends FrontController {

	public function __construct()
	{
		parent::__construct();
		// To Enable AJAX POST without compressing content
		$this->config->set_item('compress_output', FALSE);

		if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
		{
			$this->_hideForm();
			redirect('/home','refresh');
        }

		$this->load->library('form_validation');
	}

	//iklan
	public function uphits_iklan()
	{
		$this->form_validation->set_rules('iklan_id', 'id', 'required|xss_clean');
		$this->form_validation->set_rules('hits', 'hits', 'required|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
		}
		else {
			if( ! $this->web->upIklanHits($this->input->post('iklan_id'),$this->input->post('hits')))
			echo 'false';
			else
			echo $this->input->post('hits')+1;
		}
	}
	
	//rating
	function upratting()
	{
		$this->form_validation->set_rules('rt', 'rt', 'required|xss_clean');
		$this->form_validation->set_rules('tipe', 'tipe', 'required|xss_clean');
		$this->form_validation->set_rules('rating', 'rating', 'required|integer|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
		}
		else {
			echo $this->cms->upRattingKonten($this->input->post('tipe'),$this->input->post('rt'),$this->input->post('rating'));
			//$this->input->post('nilai');
		}
	}

	public function cari()
	{
		$this->form_validation->set_rules('keyword', 'keyword', 'required|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
		}
		else {
			$this->session->unset_userdata('key_cari_front', '');
			$this->session->set_userdata('key_cari_front', $this->input->post('keyword'));
			echo 'true';
		}
	}

	public function lo_user()
	{
		$this->form_validation->set_rules('uname', 'Username', 'required|max_length[20]|xss_clean');
		$this->form_validation->set_rules('upass', 'Password', 'required|max_length[20]|xss_clean');

		$gagal = $this->_jum_gagal();

        if ($this->form_validation->run($this) == FALSE)
		{
			$valid = 'false';
            $t = ($_GET)?'method-GET':'data';
            $ket = 'Gagal ' . $gagal . ' kali, Data ditolak karena ' . $t . ' tidak valid.';
            $ket .= '<br>Gagal login User, Data ditolak karena melewati validasi form.';

            if( $gagal >= $this->config->item('max_pr') )
            {
                $this->track(TRUE,$ket,$gagal);
                //redirect('home','refresh');
                $this->_hideForm();
            }
            else {
                $sts = array('sts' => false, 'nmusr' => '');
            }
		}
		else {
			if($this->user->cekLogin() === TRUE)
            {
				$sts = array('sts' => true, 'nmusr' => humanize(bs_kode($this->session->userdata('nmusr'), TRUE)));
				$valid = json_encode($sts);
				$this->session->unset_userdata('ggl');
			}
            else {
				$ket = 'Gagal login ' . $gagal . ' kali, Data ditolak karena belum terdaftar.';
				if($gagal >= $this->config->item('max_pr')) 
                {
					$ip = $this->input->ip_address() . ' : ' . $_SERVER['REMOTE_PORT'];
                    $brow = $this->input->user_agent();
                    $ket .= '[IP:'.$ip.']::[AGENT : '.$brow.']';
					log_message('error', $ket);
					$this->track(FALSE,$ket);
					//redirect('home','refresh');
					$this->_hideForm();
				}

				$sts = array('sts' => false, 'nmusr' => '');
				$valid = json_encode($sts);
			}
		}

		echo $valid;
	}

	function _hideForm()
	{
		$this->session->unset_userdata('ggl');
		echo '<script>documentGetElementById("topForm").remove();</script>';
	}

	public function uname()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[2]|max_length[20]|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
			return FALSE;
		}
		else {
			if($this->user->cekAdaUname() === TRUE)
            {
				echo 'true';
				return TRUE;
			}
            else {
				echo 'false';
				return FALSE;
			}
		}
	}

	public function uemail()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[60]|xss_clean|callback__ini_valid_email');

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
			return FALSE;
		}
		else {
			if($this->user->cekAdaEmail() === TRUE)
            {
				echo 'true';
				return TRUE;
			}
            else {
				echo 'false';
				return FALSE;
			}
		}
	}

	public function add_user()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Com Password', 'trim|required|min_length[6]|max_length[20]|matches[password]|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[60]|xss_clean');

		$gagal = $this->_jum_gagal();

		if($this->form_validation->run($this) == FALSE)
		{
			if($gagal >= $this->config->item('max_pr'))
			{
				$ket = 'Melewati Validasi AJAX - Form Registrasi :<br />';
				$ket .= str_replace(array('<p>','</p>'),array('','<br />'),validation_errors());
				$this->track(FALSE,$ket);
				//redirect('home','refresh');
				$this->_hideForm();
			}
			echo 'error';
		}
        else {
			// save 2 db
			$this->session->unset_userdata('ggl');
			if( ! $this->user->tambahUser())
            {
				if($gagal >= $this->config->item('max_pr'))
				{
					$ket = 'Gagal menyimpan data registrasi User ' . $ggl . ' kali gagal, Terjadi kesalahan database.';
					$this->track(TRUE,$ket,$ggl);
					//redirect('home','refresh');
					$this->_hideForm();
				}
				echo 'failed';
			}
            else {
				echo 'success';
			}
		}
	}

/* UPDATE PROFIL */
	public function up_uname()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[2]|max_length[20]|xss_clean');

		if(is_null($this->_cekLogin()))
		$this->_hideForm();

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
		}
		else {
			if($this->user->cekAdaUname(TRUE) === TRUE)
            {
				echo 'true';
			}
            else {
				echo 'false';
			}
		}
	}

	public function up_uemail()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[60]|xss_clean|callback__ini_valid_email');

		if(is_null($this->_cekLogin()))
		$this->_hideForm();

		if ($this->form_validation->run($this) == FALSE)
		{
			echo 'false';
		}
		else {
			if($this->user->cekAdaEmail(TRUE) === TRUE)
            {
				echo 'true';
			}
            else {
				echo 'false';
			}
		}
	}

	public function up_profil()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[60]|xss_clean');

		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Com Password', 'trim|min_length[6]|max_length[20]|matches[password]|xss_clean');

		if($this->form_validation->run($this) == FALSE || is_null($this->_cekLogin()))
		{
			redirect($this->input->post('url'));
		}
		else {
			$ori_nama = $this->input->post('ori_name');
			$ori_id = $this->input->post('ori_username');
			$ori_email = $this->input->post('ori_email');

			if( $this->input->post('password') != '' || $ori_nama != $this->input->post('name') || $ori_id != $this->input->post('username') || $ori_email != $this->input->post('email'))
			{
				if( ! $this->user->updateUser())
				{
					redirect('/error', 'refresh');
				}
				else {
					redirect($this->input->post('url'));
				}
			}
			else {
				redirect($this->input->post('url'));
			}
		}
	}

}

/* End of file ajax_cek.php */
/* Location: ./application/controllers/shared/ajax_cek.php */

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'third_party/fpdf17/html2fpdf.php');

class Post_features extends FrontController 
{
    function __construct()
    {
        parent::__construct();
        // without compressing content
        $this->config->set_item('compress_output', FALSE);

        if ($this->config->item('sts_web') === 1 || $this->config->item('sts_web') === 2)
        redirect('/home','refresh');

        $this->input->post(NULL, TRUE); // returns all POST items with XSS filter 
        $this->input->get(NULL, TRUE); // returns all GET items with XSS filter 
        $this->load->model('admin/blogmodel','artikel');
        $this->load->helper('inflector');
    }
    
    public function post_item()
    {
        $id = $this->uri->segment(2,0);
        $query = $this->artikel->getSelectedPost((int)$id);
        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            
            $data['judul'] = $row->judul_id;
            $data['isi'] = $row->isi_id;
            $data['author'] = $row->author.' - '.$this->config->item('site_name');
            $data['keyword'] = $row->keyword_id;
            $data['tgl_buat'] = date('d-m-Y H:i:s', strtotime($row->tgl_buat));
            $data['tgl_cetak'] = date('d-m-Y H:i:s');
            $data['konten'] = TRUE;
            
            $this->load->view('partial/popup/features_popup',$data);
        }
        else{
            $data['konten'] = FALSE;
            $this->load->view('partial/popup/features_popup',$data);
        }
    }
    
    public function generate_pdf()
    {
        $id = $this->uri->segment(2,0);
        $query = $this->artikel->getSelectedPost((int)$id);
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            $html = '<h1><u>'.html_entity_decode($row->judul_id).'</u></h1><br>'.$row->isi_id.'<p><br>keyword : '.$row->keyword_id.'<br>author : '.$row->author.'<br></p>';
            $name = underscore(strtolower($row->judul_id));
            $html2pdf  = new HTML2FPDF();
            $html2pdf->AliasNbPages();
            $html2pdf->SetTitle($row->judul_id);
            $html2pdf->SetAuthor($row->author.' - '.$this->config->item('site_name'));
            $html2pdf->SetSubject($row->judul_id);
            $html2pdf->SetKeywords($row->keyword_id);
            // page
            $html2pdf->AddPage();
            $html2pdf->SetFontSize(14);
            $html2pdf->WriteHTML($html);
            $html2pdf->Output($name.'.pdf','D');
        }
        else {
            //echo "No Data.!";
            $this->session->set_flashdata('info', 'Data Not Found.!');
        }
    }
    
    public function form_send_post()
    {
        $data['post_id'] = $this->uri->segment(3,0);
        $data['tipe'] = 'kirim_email';
        $this->load->view('partial/popup/features_popup',$data);
    }
    
    public function send_post()
    {
        $this->load->helper('email');
        $this->load->library('form_validation');
        
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('post_id', 'ID', 'required|xss_clean');
        $this->form_validation->set_rules('e_nama_pengirim', 'Nama Pengirim', 'trim|required|xss_clean');
        $this->form_validation->set_rules('e_pengirim', 'Email Pengirim', 'required|valid_email|xss_clean');
        $this->form_validation->set_rules('e_penerima', 'Email Penerima', 'required|valid_emails|xss_clean');
        
        // check valid email addr
        if ($this->form_validation->run() == FALSE)
        {
            $data['post_id'] = $this->input->post('post_id');
            $data['tipe'] = 'kirim_email';
            $this->load->view('partial/popup/features_popup',$data);
        }
        else {

            $id = htmlspecialchars(stripcslashes($this->input->post('post_id')));
            $p = htmlspecialchars(stripcslashes($this->input->post('e_pengirim')));
            $np = htmlspecialchars(stripcslashes($this->input->post('e_nama_pengirim')));
            $t = htmlspecialchars(stripcslashes($this->input->post('e_penerima')));
            
            $query = $this->artikel->getSelectedPost((int)$id);
            if ($query->num_rows() > 0)
            {
                $this->load->library('email');
                $this->email->from($p, $np);
                $this->email->to($t);
                
                $row = $query->row();
                $this->email->subject('['.$this->config->item('site_name').'] - '.$row->judul_id);
                $this->email->message(html_entity_decode($row->isi_id));
                
                if($this->email->send())
                {
                    $data['tipe'] = 'sukses_kirim_email';
                    $data['info'] = 'Sukses Kirim Email.';
                    $this->load->view('partial/popup/features_popup',$data);
                }
                else {
                    $data['tipe'] = 'gagal_kirim_email';
                    $data['info'] = '<h5>Gagal Kirim Email</h5>Info : ' . $this->email->print_debugger();
                    $this->load->view('partial/popup/features_popup',$data);
                }
            }
            else {
                $data['tipe'] = 'gagal_kirim_email';
                $data['info'] = '<h5>Gagal Kirim Email</h5>Info : Data tidak ditemukan.';
                $this->load->view('partial/popup/features_popup',$data);
            }
        }
    }
    
    public function print_post()
    {
        $id = $this->uri->segment(3,0);
        $query = $this->artikel->getSelectedPost((int)$id);
        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            
            $data['judul'] = $row->judul_id;
            $data['isi'] = $row->isi_id;
            $data['author'] = $row->author.' - '.$this->config->item('site_name');
            $data['keyword'] = $row->keyword_id;
            $data['tgl_buat'] = date('d-m-Y H:i:s', strtotime($row->tgl_buat));
            $data['tgl_cetak'] = date('d-m-Y H:i:s');
            $data['konten'] = TRUE;
            
            $this->load->view('partial/popup/features_popup',$data);
        }
        else {
            $data['konten'] = FALSE;
            $this->load->view('partial/popup/features_popup',$data);
        }
    }

}

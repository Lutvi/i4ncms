<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Check whether the site is offline or not.
 *
 */
class site_offline_hook {

    public function __construct()
    {
        log_message('debug','Status website Offline dijalankan.');
    }
    
    public function is_offline()
    {
        if(file_exists(APPPATH.'config/site.php'))
        {
            include(APPPATH.'config/site.php');
            
            // Offline
            if(isset($config['sts_web']) && $config['sts_web']=== 1)
            {
                header("Location: ". FURL  ."splash.php");
                exit;
            }
            
            // Perbaikan
            if(isset($config['sts_web']) && $config['sts_web']=== 2)
            {
                header("Location: ". FURL  ."splash.php");
                exit;
            }
        }
        else {
            echo '<body style="background:#1E90FF; color:#E6E6FA;"><div align= "center" style="margin-top:15%;">
                    <h1>Websites Underconstructions</h1></div></body>';
            exit;
        }
    }

}

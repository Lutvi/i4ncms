<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script>
	var BaseUrl='<?php echo base_url(); ?>',
	bhs ='<?php echo current_lang(); ?>',
	vtr='<?php echo random_string('alnum', 38); ?>',
	bbt='<?php echo random_string('alnum', 8); ?>',
	mnj='<?php echo random_string('alnum', 4); ?>',

	txtKeluar = '<?php echo $this->lang->line('lbl_btn_keluar'); ?>',
	txtMasuk = '<?php echo $this->lang->line('lbl_btn_masuk'); ?>',
	cariKosong = '<?php echo $this->lang->line('lbl_cari_kosong'); ?>',
	namaSandiKosong = '<?php echo $this->lang->line('lbl_nama_sandi_kosong'); ?>',
	namaKosong = '<?php echo $this->lang->line('lbl_nama_kosong'); ?>',
	sandiKosong = '<?php echo $this->lang->line('lbl_sandi_kosong'); ?>',

	formKosong = '<?php echo $this->lang->line('lbl_form_kosong'); ?>',
	blmTerdaftar = '<?php echo $this->lang->line('lbl_blm_terdaftar'); ?>',
	daftarSukses = '<?php echo $this->lang->line('lbl_pendaftaran_sukses'); ?>',
	daftarGagalData= '<?php echo $this->lang->line('lbl_pendaftaran_gagal_data'); ?>',
	daftarIlegal= '<?php echo $this->lang->line('lbl_pendaftaran_gagal_lewati'); ?>',

	vnamaKosong = '<?php echo $this->lang->line('isi_nama'); ?>',
	vnamaMin = '<?php echo $this->lang->line('minlen_nama'); ?>',
	vnamaMax = '<?php echo $this->lang->line('maxlen_nama'); ?>',

	vnamaIdKosong = '<?php echo $this->lang->line('isi_namaid'); ?>',
	vnamaIdMin = '<?php echo $this->lang->line('minlen_namaid'); ?>',
	vnamaIdMax = '<?php echo $this->lang->line('maxlen_namaid'); ?>',
	vnamaIdAda = '<?php echo $this->lang->line('sudah_ada_namaid'); ?>',

	vemailKosong = '<?php echo $this->lang->line('isi_email'); ?>',
	vemailMax = '<?php echo $this->lang->line('maxlen_email'); ?>',
	vemailSalah = '<?php echo $this->lang->line('email_salah'); ?>',
	vemailAda = '<?php echo $this->lang->line('sudah_ada_email'); ?>',

	vsandilKosong = '<?php echo $this->lang->line('isi_sandi'); ?>',
	vsandiMin = '<?php echo $this->lang->line('minlen_sandi'); ?>',
	vsandiMax = '<?php echo $this->lang->line('maxlen_sandi'); ?>',
	vsandiUlang = '<?php echo $this->lang->line('isi_ulangi_sandi'); ?>',
	vsandiSamkan = '<?php echo $this->lang->line('samakan_sandi'); ?>',

	btnTutup = '<?php echo $this->lang->line('lbl_btn_tutup'); ?>';
</script>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarimodule/'.$txtcari:'atur_module/update_tabel';
?>
var postURL = '<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_module/update_module'); ?>';
var upTblURL = '<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_module script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){

	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", "Konfirmasi " + jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		return false;
	});
	$(".rup").click(function(){
		var ID = $(this).attr('id').split("_");
		var modid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])-1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&modid='+modid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		return false;
	});
	$(".rdown").click(function(){
		var ID = $(this).attr('id').split("_");
		var modid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])+1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&modid='+modid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&module_id='+ID[2]+'&status='+status;
		
		if(status == 0 || status == 1 || status != ''){
			$(".editbox").hide();
			$(".text").show();
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data === 'sukses') {
					$('#status_'+ID[2]).html((status == 1)?'On':'Off');
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
			aksiFormAJAX(postURL,dataString,successFunct);
		}else{
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open");
		}	
	});
	$(".editbox").mouseup(function(){
		return false;
	});

});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 w(6){R(6===\'10\'){}S{$("#T").6(\'U\',6).c("x")}$(\'#g\').11(12)}5 1e(3){4 V=3[1];4 8=9+\'=\'+$("y[z="+9+"]").s()+\'&V=\'+V;$(\'#g\').d(\'<b 7="A" B="C-D:\'+E+\'F;"><h i="\'+j+\'k/l/G-m.n" 7="o" /></b>\');H(I,8,w)}$(13).1f(5(){$(\'a.W\').J(5(e){e.1g();4 3=$(p).q(\'t\').K("L");4 14=$(p).q(\'15\');$("#c-W").c("1h","15","1i "+14);$("#c-W").6(\'3\',3).c("x");u v});$(".1j").J(5(){4 3=$(p).q(\'t\');$("#M"+3).X();$("#Y"+3).Z();$("#Y"+3).1k();u v});$(".1l").J(5(){4 3=$(p).q(\'t\').K("L");4 r=3[1];4 N=O(3[2]);4 P=O(3[2])-1;4 8=9+\'=\'+$("y[z="+9+"]").s()+\'&r=\'+r+\'&16=\'+P+\'&17=\'+N;$(\'#g\').d(\'<b 7="A" B="C-D:\'+E+\'F;"><h i="\'+j+\'k/l/G-m.n" 7="o" /></b>\');H(I,8,w);u v});$(".1m").J(5(){4 3=$(p).q(\'t\').K("L");4 r=3[1];4 N=O(3[2]);4 P=O(3[2])+1;4 8=9+\'=\'+$("y[z="+9+"]").s()+\'&r=\'+r+\'&16=\'+P+\'&17=\'+N;$(\'#g\').d(\'<b 7="A" B="C-D:\'+E+\'F;"><h i="\'+j+\'k/l/G-m.n" 7="o" /></b>\');H(I,8,w);u v});$(".Q").1n(5(){4 3=$(p).q(\'t\').K("L");4 f=$("#Y"+3[2]).s();4 8=9+\'=\'+$("y[z="+9+"]").s()+\'&1o=\'+3[2]+\'&f=\'+f;R(f==0||f==1||f!=\'\'){$(".Q").X();$(".18").Z();$("#M"+3[2]).d(\'<h i="\'+j+\'k/l/19-m-1p.n" 7="o" />\');5 1a(6){R(6===\'10\'){$(\'#M\'+3[2]).d((f==1)?\'1q\':\'1r\')}S{$(\'#g\').d(\'<b 7="A" B="C-D:\'+E+\'F;"><h i="\'+j+\'k/l/G-m.n" 7="o" /></b>\');$("#T").6(\'U\',6).c("x");$(\'#g\').11(12)}}H(I,8,1a)}S{$("#M"+3[2]).d(\'<h i="\'+j+\'k/l/1s-19-m.n" 7="o" />\');$("#T").6(\'U\',\'1t 1u 1v 1 1b 0.<1w>1 = 1c 1b 0 = 1x 1c.\').c("x")}});$(".Q").1d(5(){u v})});$(13).1d(5(){$(".Q").X();$(".18").Z()});',62,96,'|||ID|var|function|data|align|dataString|tkn||div|dialog|html||status|tabel|img|src|baseURL|assets|icons|loader|gif|absmiddle|this|attr|modid|val|id|return|false|successFunctDefault|open|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|split|_|status_|opos|parseInt|pos|editbox|if|else|gagal|INFO|id_hps|hapus|hide|status_input_|show|sukses|load|upTblURL|document|jdl|title|posisi|old_posisi|text|ajax|successFunct|dan|aktif|mouseup|hapusItem|ready|preventDefault|option|Konfirmasi|edit|focus|rup|rdown|change|module_id|small|On|Off|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcaribanner/'.$txtcari:'hbanner/update_tabel';
?>
var postURL = '<?php echo ($super_admin==FALSE)?'#':site_url($this->config->item('admpath').'/hbanner/update_hbanner'); ?>';
var upTblURL = '<?php echo ($super_admin==FALSE)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script>

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){

	//aksi hbanner
	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".rup").click(function(e){
		e.preventDefault();
	    var ID = $(this).attr('id').split("_");
	    var albid = ID[1];
	    var opos = parseInt(ID[2]);
	    var pos = parseInt(ID[2])-1;
	    var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&hbid='+albid+'&posisi='+pos+'&old_posisi='+opos;

	    $('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');

		aksiFormAJAX(postURL,dataString,successFunctDefault);

	    return false;
	});
	$(".rdown").click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var albid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])+1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&hbid='+albid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		
		return false;
	});
	$(".edit").click(function(e){
		e.preventDefault();
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();

		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&hbanner_id='+ID[2]+'&status='+status;
	
		if(status != '')
		{
			$(".editbox").hide();
			$(".text").show();
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data == 'sukses') {
					$('#status_'+ID[2]).html(capitalise(status));
				} else {
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
			aksiFormAJAX(postURL,dataString,successFunct);
		} else {
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara on dan off.<br>on = aktif dan off = tidak aktif.').dialog( "open" );
		}
	});
	$(".editbox").mouseup(function(){
		return false;
	});
	$(document).mouseup(function(){
		$(".editbox").hide();
		$(".text").show();
		return false;
	});
	// end Aksi hbanner
});

</script>

<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 u(5){R(5===\'10\'){}S{$("#T").5(\'U\',5).b("v")}$(\'#d\').11(12)}4 1h(0){3 V=0[1];3 7=8+\'=\'+$("w[x="+8+"]").r()+\'&V=\'+V;$(\'#d\').c(\'<9 6="y" z="A-B:\'+C+\'D;"><f g="\'+h+\'i/j/E-k.l" 6="m" /></9>\');F(G,7,u)}$(13).1i(4(){$(\'a.W\').H(4(e){e.I();3 0=$(n).o(\'s\').J("K");3 14=$(n).o(\'15\');$("#b-W").b("1j","15",14);$("#b-W").5(\'0\',0).b("v");p q});$(".1k").H(4(e){e.I();3 0=$(n).o(\'s\').J("K");3 L=0[1];3 M=N(0[2]);3 O=N(0[2])-1;3 7=8+\'=\'+$("w[x="+8+"]").r()+\'&16=\'+L+\'&17=\'+O+\'&18=\'+M;$(\'#d\').c(\'<9 6="y" z="A-B:\'+C+\'D;"><f g="\'+h+\'i/j/E-k.l" 6="m" /></9>\');F(G,7,u);p q});$(".1l").H(4(e){e.I();3 0=$(n).o(\'s\').J("K");3 L=0[1];3 M=N(0[2]);3 O=N(0[2])+1;3 7=8+\'=\'+$("w[x="+8+"]").r()+\'&16=\'+L+\'&17=\'+O+\'&18=\'+M;$(\'#d\').c(\'<9 6="y" z="A-B:\'+C+\'D;"><f g="\'+h+\'i/j/E-k.l" 6="m" /></9>\');F(G,7,u);p q});$(".1m").H(4(e){e.I();3 0=$(n).o(\'s\');$("#P"+0).X();$("#Y"+0).Z();$("#Y"+0).1n();p q});$(".Q").1o(4(){3 0=$(n).o(\'s\').J("K");3 t=$("#Y"+0[2]).r();3 7=8+\'=\'+$("w[x="+8+"]").r()+\'&1p=\'+0[2]+\'&t=\'+t;R(t!=\'\'){$(".Q").X();$(".19").Z();$("#P"+0[2]).c(\'<f g="\'+h+\'i/j/1a-k-1q.l" 6="m" />\');4 1b(5){R(5==\'10\'){$(\'#P\'+0[2]).c(1r(t))}S{$(\'#d\').c(\'<9 6="y" z="A-B:\'+C+\'D;"><f g="\'+h+\'i/j/E-k.l" 6="m" /></9>\');$("#T").5(\'U\',5).b("v");$(\'#d\').11(12)}}F(G,7,1b)}S{$("#P"+0[2]).c(\'<f g="\'+h+\'i/j/1s-1a-k.l" 6="m" />\');$("#T").5(\'U\',\'1t 1u 1v 1c 1d 1e.<1w>1c = 1f 1d 1e = 1x 1f.\').b("v")}});$(".Q").1g(4(){p q});$(13).1g(4(){$(".Q").X();$(".19").Z();p q})});',62,96,'ID|||var|function|data|align|dataString|tkn|div||dialog|html|tabel||img|src|baseURL|assets|icons|loader|gif|absmiddle|this|attr|return|false|val|id|status|successFunctDefault|open|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|preventDefault|split|_|albid|opos|parseInt|pos|status_|editbox|if|else|gagal|INFO|id_hps|hapus|hide|status_input_|show|sukses|load|upTblURL|document|jdl|title|hbid|posisi|old_posisi|text|ajax|successFunct|on|dan|off|aktif|mouseup|hapusItem|ready|option|rup|rdown|edit|focus|change|hbanner_id|small|capitalise|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

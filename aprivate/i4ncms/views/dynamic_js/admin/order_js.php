<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<?php
	if(is_numeric($this->uri->segment(4)))
	$offset = $this->uri->segment(4,0); 
	else
	$offset = $this->uri->segment(5,0);

$uptbl = (isset($txtcari))?'tblcariorder/'.$this->uri->segment(3,'ajax').'/'.$txtcari:'order/update_tabel/'.$this->uri->segment(3,'ajax');
?>
<script type="text/javascript">
var upTblURL = '<?php echo ( ! $this->super_admin)?'#':base_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>',
	cekTabURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/order/cek_tab'); ?>',
	postBatalURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/order/batalkan_transaksi'); ?>',
	postHapusURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/order/hapus_order'); ?>';
	postLanjutURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/order/lanjut_proses_order'); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function batalItem(ID)
{
	var id_batal = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_trx='+id_batal;

	$('#tabel').html('<div align="center" class="loadtbl" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postBatalURL,dataString,successFunctDefault);
}

function hapusItem(ID,cid,jml,total)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_trx='+id_hps+'&id_cust='+cid+'&jml_trans_trx='+jml+'&total_trx='+total;

	$('#tabel').html('<div align="center" class="loadtbl" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postHapusURL,dataString,successFunctDefault);
}

function lanjutProses(ID)
{
	var trx = ID[1];
	var status = ID[0];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&trx='+trx+'&status='+status;

	$('#tabel').html('<div align="center" class="loadtbl" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postLanjutURL,dataString,successFunctDefault);
}

$(function() {
	
var aktif = $( '#aktifTxt' ).val();

	$( "#tabs" ).tabs({ active: aktif, disabled:[5] });

	$( "#tabs" ).on( "tabsbeforeactivate", function( event, ui ) {
		var cek = ui.newTab.index();
		$( '#aktifTxt' ).val(cek);
		$.get(cekTabURL, { aktif_tab:cek });
	});

	$( "#dialog-batal" ).dialog({
	  autoOpen: false,
	  resizable: false,
	  minHeight:160,
	  modal: true,
	  create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
	  buttons: {
		"Ya": function(event, ui) {
			var ID = $(this).data('ID');
			batalItem(ID);
			$( this ).dialog( "close" );
		},
		"Tidak": function() {
		  $( this ).dialog( "close" );
		}
	  }
	});
	
	$( "#dialog-hapus-order" ).dialog({
	  autoOpen: false,
	  resizable: false,
	  minHeight:160,
	  modal: true,
	  create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
	  buttons: {
		"Ya": function(event, ui) {
			var ID = $(this).data('ID'),
				cid = $(this).data('cid'),
				jml = $(this).data('jml'),
				total = $(this).data('total');
			hapusItem(ID,cid,jml,total);
			//alert(ID +'-'+ cid +'-'+ jml +'-'+ total);
			$( this ).dialog( "close" );
		},
		"Tidak": function() {
		  $( this ).dialog( "close" );
		}
	  }
	});
	
});

$(document).ready(function(){

	$('a.lanjut').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		var INFO = $(this).data('info');

		$( "#dialog-lanjut" ).dialog("option", "title", jdl);
		$( "#dialog-lanjut" ).data('ID',ID).data('INFO',INFO).dialog( "open");
		return false;
	});

	$('a.batal').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-batal" ).dialog("option", "title", jdl);
		$( "#dialog-batal" ).data('ID',ID).dialog( "open");
		return false;
	});

	$('a.buang').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_"),
			jdl = $(this).attr('title'),
			cid = $(this).attr('data-cid'),
			jml = $(this).attr('data-jml'),
			total = $(this).attr('data-total');
		
		$( "#dialog-hapus-order" ).dialog("option", "title", jdl);
		$( "#dialog-hapus-order" ).data('cid',cid);
		$( "#dialog-hapus-order" ).data('jml',jml);
		$( "#dialog-hapus-order" ).data('total',total);
		$( "#dialog-hapus-order" ).data('ID',ID);
		$( "#dialog-hapus-order" ).dialog( "open");
		return false;
	});

});
</script>
<?php else: ?>
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('8 r(3){1q(3===\'1r\'){}1s{$("#1t").3(\'s\',3).2("t")}$(\'#u\').1u(1v)}8 14(4){7 15=4[1];7 j=k+\'=\'+$("x[y="+k+"]").q()+\'&16=\'+15;$(\'#u\').z(\'<l m="A" B="C" D="E-F:\'+G+\'H;"><I J="\'+K+\'L/M/N-O.P" m="Q" /></l>\');R(1w,j,r)}8 17(4,9,b,c){7 18=4[1];7 j=k+\'=\'+$("x[y="+k+"]").q()+\'&16=\'+18+\'&1x=\'+9+\'&1y=\'+b+\'&1z=\'+c;$(\'#u\').z(\'<l m="A" B="C" D="E-F:\'+G+\'H;"><I J="\'+K+\'L/M/N-O.P" m="Q" /></l>\');R(1A,j,r)}8 1B(4){7 S=4[1];7 T=4[0];7 j=k+\'=\'+$("x[y="+k+"]").q()+\'&S=\'+S+\'&T=\'+T;$(\'#u\').z(\'<l m="A" B="C" D="E-F:\'+G+\'H;"><I J="\'+K+\'L/M/N-O.P" m="Q" /></l>\');R(1C,j,r)}$(8(){7 19=$(\'#1a\').q();$("#U").U({1D:19,1E:[5]});$("#U").1F("1G",8(f,n){7 V=n.1H.1I();$(\'#1a\').q(V);$.1J(1K,{1L:V})});$("#2-v").2({1b:g,1c:g,1d:1e,1f:1g,1h:8(f,n,3){$(f.1i).1j().1k(\'1l\',\'1m\')},1n:{"1o":8(f,n){7 4=$(6).3(\'4\');14(4);$(6).2("w")},"1p":8(){$(6).2("w")}}});$("#2-h-i").2({1b:g,1c:g,1d:1e,1f:1g,1h:8(f,n,3){$(f.1i).1j().1k(\'1l\',\'1m\')},1n:{"1o":8(f,n){7 4=$(6).3(\'4\'),9=$(6).3(\'9\'),b=$(6).3(\'b\'),c=$(6).3(\'c\');17(4,9,b,c);$(6).2("w")},"1p":8(){$(6).2("w")}}})});$(1M).1N(8(){$(\'a.W\').X(8(e){e.Y();7 4=$(6).d(\'Z\').10("11");7 o=$(6).d(\'p\');7 s=$(6).3(\'1O\');$("#2-W").2("12","p",o);$("#2-W").3(\'4\',4).3(\'s\',s).2("t");13 g});$(\'a.v\').X(8(e){e.Y();7 4=$(6).d(\'Z\').10("11");7 o=$(6).d(\'p\');$("#2-v").2("12","p",o);$("#2-v").3(\'4\',4).2("t");13 g});$(\'a.1P\').X(8(e){e.Y();7 4=$(6).d(\'Z\').10("11"),o=$(6).d(\'p\'),9=$(6).d(\'3-9\'),b=$(6).d(\'3-b\'),c=$(6).d(\'3-c\');$("#2-h-i").2("12","p",o);$("#2-h-i").3(\'9\',9);$("#2-h-i").3(\'b\',b);$("#2-h-i").3(\'c\',c);$("#2-h-i").3(\'4\',4);$("#2-h-i").2("t");13 g})});',62,114,'||dialog|data|ID||this|var|function|cid||jml|total|attr||event|false|hapus|order|dataString|tkn|div|align|ui|jdl|title|val|successFunctDefault|INFO|open|tabel|batal|close|input|name|html|center|class|loadtbl|style|margin|top|cth|px|img|src|baseURL|assets|icons|bar|loader|gif|absmiddle|aksiFormAJAX|trx|status|tabs|cek|lanjut|click|preventDefault|id|split|_|option|return|batalItem|id_batal|id_trx|hapusItem|id_hps|aktif|aktifTxt|autoOpen|resizable|minHeight|160|modal|true|create|target|parent|css|position|fixed|buttons|Ya|Tidak|if|sukses|else|gagal|load|upTblURL|postBatalURL|id_cust|jml_trans_trx|total_trx|postHapusURL|lanjutProses|postLanjutURL|active|disabled|on|tabsbeforeactivate|newTab|index|get|cekTabURL|aktif_tab|document|ready|info|buang'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarivideo/'.$txtcari:'video/update_tabel';
?>
var postURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/video/update_video'); ?>';
var upTblURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
var jmlAnak = <?php echo ( ! $super_admin)?0:$jml_playlist; ?>;
var jmlParent = <?php echo ( ! $super_admin)?0:count($parent_video); ?>;
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){
	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".rup").click(function(){
	    var ID = $(this).attr('id').split("_");
	    var vdoid = ID[1];
	    var opos = parseInt(ID[2]);
	    var pos = parseInt(ID[2])-1;
	    var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&vdoid='+vdoid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	    aksiFormAJAX(postURL,dataString,successFunctDefault);
	    return false;
	});
	$(".rdown").click(function(){
		var ID = $(this).attr('id').split("_");
		var vdoid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])+1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&vdoid='+vdoid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		return false;
	});
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		$("#utama_"+ID).hide();
		$("#utama_input_"+ID).show();
		$("#utama_input_"+ID).focus();
		$("#komentar_"+ID).hide();
		$("#komentar_input_"+ID).show();
		$("#komentar_input_"+ID).focus();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var utama=$("#utama_input_"+ID[2]).val();
		var status=$("#status_input_"+ID[2]).val();
		var komentar=$("#komentar_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&video_id='+ID[2]+'&utama='+utama+'&status='+status+'&komentar='+komentar;
	
		if(utama != '' || status != '' || komentar != '')
		{
			$(".editbox").hide();
			$(".text").show();
			$("#utama_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#komentar_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data === 'sukses') {
					$('#utama_'+ID[2]).html((utama == 'on')?'On':'Off');
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
					$('#komentar_'+ID[2]).html((komentar == 'on')?'On':'Off');
				} else {
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
			aksiFormAJAX(postURL,dataString,successFunct);
		} else {
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara on dan off.<br>on = aktif dan off = tidak aktif.').dialog( "open" );
		}
	});
	$(".editbox").mouseup(function(){
		return false;
	});
});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 A(7){W(7===\'1b\'){}X{$("#Y").7(\'Z\',7).l("B")}$(\'#n\').1c(1d)}5 1p(0){3 10=0[1];3 8=9+\'=\'+$("C[D="+9+"]").m()+\'&10=\'+10;$(\'#n\').6(\'<b 4="E" F="G-H:\'+I+\'J;"><c d="\'+f+\'g/h/K-i.j" 4="k" /></b>\');L(M,8,A)}$(1e).1q(5(){$(\'a.11\').N(5(e){e.1r();3 0=$(o).p(\'r\').O("P");3 1f=$(o).p(\'1g\');$("#l-11").l("1s","1g",1f);$("#l-11").7(\'0\',0).l("B");s t});$(".1t").N(5(){3 0=$(o).p(\'r\').O("P");3 q=0[1];3 Q=R(0[2]);3 S=R(0[2])-1;3 8=9+\'=\'+$("C[D="+9+"]").m()+\'&q=\'+q+\'&1h=\'+S+\'&1i=\'+Q;$(\'#n\').6(\'<b 4="E" F="G-H:\'+I+\'J;"><c d="\'+f+\'g/h/K-i.j" 4="k" /></b>\');L(M,8,A);s t});$(".1u").N(5(){3 0=$(o).p(\'r\').O("P");3 q=0[1];3 Q=R(0[2]);3 S=R(0[2])+1;3 8=9+\'=\'+$("C[D="+9+"]").m()+\'&q=\'+q+\'&1h=\'+S+\'&1i=\'+Q;$(\'#n\').6(\'<b 4="E" F="G-H:\'+I+\'J;"><c d="\'+f+\'g/h/K-i.j" 4="k" /></b>\');L(M,8,A);s t});$(".1v").N(5(){3 0=$(o).p(\'r\');$("#T"+0).u();$("#12"+0).v();$("#12"+0).13();$("#14"+0).u();$("#15"+0).v();$("#15"+0).13();$("#16"+0).u();$("#17"+0).v();$("#17"+0).13();s t});$(".U").1w(5(){3 0=$(o).p(\'r\').O("P");3 w=$("#15"+0[2]).m();3 x=$("#12"+0[2]).m();3 y=$("#17"+0[2]).m();3 8=9+\'=\'+$("C[D="+9+"]").m()+\'&1x=\'+0[2]+\'&w=\'+w+\'&x=\'+x+\'&y=\'+y;W(w!=\'\'||x!=\'\'||y!=\'\'){$(".U").u();$(".1j").v();$("#14"+0[2]).6(\'<c d="\'+f+\'g/h/V-i-18.j" 4="k" />\');$("#T"+0[2]).6(\'<c d="\'+f+\'g/h/V-i-18.j" 4="k" />\');$("#16"+0[2]).6(\'<c d="\'+f+\'g/h/V-i-18.j" 4="k" />\');5 1k(7){W(7===\'1b\'){$(\'#14\'+0[2]).6((w==\'z\')?\'19\':\'1a\');$(\'#T\'+0[2]).6((x==\'z\')?\'19\':\'1a\');$(\'#16\'+0[2]).6((y==\'z\')?\'19\':\'1a\')}X{$(\'#n\').6(\'<b 4="E" F="G-H:\'+I+\'J;"><c d="\'+f+\'g/h/K-i.j" 4="k" /></b>\');$("#Y").7(\'Z\',7).l("B");$(\'#n\').1c(1d)}}L(M,8,1k)}X{$("#T"+0[2]).6(\'<c d="\'+f+\'g/h/1y-V-i.j" 4="k" />\');$("#Y").7(\'Z\',\'1z 1A 1B z 1l 1m.<1C>z = 1n 1l 1m = 1D 1n.\').l("B")}});$(".U").1o(5(){s t})});$(1e).1o(5(){$(".U").u();$(".1j").v()});',62,102,'ID|||var|align|function|html|data|dataString|tkn||div|img|src||baseURL|assets|icons|loader|gif|absmiddle|dialog|val|tabel|this|attr|vdoid|id|return|false|hide|show|utama|status|komentar|on|successFunctDefault|open|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|split|_|opos|parseInt|pos|status_|editbox|ajax|if|else|gagal|INFO|id_hps|hapus|status_input_|focus|utama_|utama_input_|komentar_|komentar_input_|small|On|Off|sukses|load|upTblURL|document|jdl|title|posisi|old_posisi|text|successFunct|dan|off|aktif|mouseup|hapusItem|ready|preventDefault|option|rup|rdown|edit|change|video_id|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

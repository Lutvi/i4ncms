<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
if(is_numeric($this->uri->segment(3)))
$offset = $this->uri->segment(3,0); 
else
$offset = $this->uri->segment(4,0);

	$uptbl = (isset($txtcari))?'tblcaripelanggan/'.$txtcari:'pelanggan/update_tabel';
?>

var postURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/pelanggan/update_detail'); ?>';
var upTblURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_menu script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;
	
	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}


$(document).ready(function(){

    $('a.hapus').click(function(e){
        e.preventDefault();
        var ID = $(this).attr('id').split("_");
        var jdl = $(this).attr('title');
        
        $( "#dialog-hapus" ).dialog("option", "title", jdl);
        $( "#dialog-hapus" ).data('ID',ID).dialog( "open");
        return false;
    });

    $(".edit").click(function(){
        var ID=$(this).attr('id');
        $("#status_trss_"+ID).hide();
        $("#status_rss_"+ID).show();
        $("#status_tcust_"+ID).hide();
        $("#status_cust_"+ID).show();
        $("#status_tprio_"+ID).hide();
        $("#status_prio_"+ID).show();

        $(this).focus();
        return false;
    });
    
    $(".editbox").change(function(){
        var ID=$(this).attr('id').split("_");
        var status=$("#status_cust_"+ID[2]).val();
        var rss=$("#status_rss_"+ID[2]).val();
        var prioritas=$("#status_prio_"+ID[2]).val();
        var dataJson = {Hydra:$("input[name="+tkn+"]").val() ,cust_id:ID[2], status:status, rss:rss, prioritas:prioritas };
        
        if(status !== '' || rss !== '' || prioritas !== ''){
			//$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
			$(".editbox").hide();
			$(".text").show();
			$("#status_tcust_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#status_trss_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#status_tprio_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data === 'sukses'){
					//alert(data);
					$('#status_tcust_'+ID[2]).html(capitalise(status));
					$('#status_trss_'+ID[2]).html(capitalise(rss));
					$('#status_tprio_'+ID[2]).html(capitalise(prioritas));
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
		
			aksiFormAJAX(postURL,dataJson,successFunct);
        }else{
            $("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
            $("#status_input_"+ID[2]).focus();
            alert('Isi tidak boleh kosong..!');
        }
    });
    
    $(".editbox").mouseup(function(){
        return false;
    });

});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 J(5){v(5===\'K\'){}w{$("#L").5(\'M\',5).8("x")}$(\'#q\').N(O)}4 1f(0){3 y=0[1];3 P=z+\'=\'+$("Q[R="+z+"]").j()+\'&y=\'+y;$(\'#q\').6(\'<r 7="S" T="U-V:\'+W+\'X;"><9 b="\'+c+\'d/f/Y-g.h" 7="i" /></r>\');Z(10,P,J)}$(11).1g(4(){$(\'a.A\').12(4(e){e.1h();3 0=$(k).s(\'B\').13("14");3 15=$(k).s(\'16\');$("#8-A").8("1i","16",15);$("#8-A").5(\'0\',0).8("x");C D});$(".1j").12(4(){3 0=$(k).s(\'B\');$("#E"+0).l();$("#17"+0).m();$("#F"+0).l();$("#18"+0).m();$("#G"+0).l();$("#19"+0).m();$(k).1a();C D});$(".t").1k(4(){3 0=$(k).s(\'B\').13("14");3 n=$("#18"+0[2]).j();3 o=$("#17"+0[2]).j();3 p=$("#19"+0[2]).j();3 1b={1l:$("Q[R="+z+"]").j(),1m:0[2],n:n,o:o,p:p};v(n!==\'\'||o!==\'\'||p!==\'\'){$(".t").l();$(".1c").m();$("#F"+0[2]).6(\'<9 b="\'+c+\'d/f/u-g-H.h" 7="i" />\');$("#E"+0[2]).6(\'<9 b="\'+c+\'d/f/u-g-H.h" 7="i" />\');$("#G"+0[2]).6(\'<9 b="\'+c+\'d/f/u-g-H.h" 7="i" />\');4 1d(5){v(5===\'K\'){$(\'#F\'+0[2]).6(I(n));$(\'#E\'+0[2]).6(I(o));$(\'#G\'+0[2]).6(I(p))}w{$(\'#q\').6(\'<r 7="S" T="U-V:\'+W+\'X;"><9 b="\'+c+\'d/f/Y-g.h" 7="i" /></r>\');$("#L").5(\'M\',5).8("x");$(\'#q\').N(O)}}Z(10,1b,1d)}w{$("#1n"+0[2]).6(\'<9 b="\'+c+\'d/f/1o-u-g.h" 7="i" />\');$("#1p"+0[2]).1a();1q(\'1r 1s 1t 1u..!\')}});$(".t").1e(4(){C D})});$(11).1e(4(){$(".t").l();$(".1c").m()});',62,93,'ID|||var|function|data|html|align|dialog|img||src|baseURL|assets||icons|loader|gif|absmiddle|val|this|hide|show|status|rss|prioritas|tabel|div|attr|editbox|ajax|if|else|open|id_hps|tkn|hapus|id|return|false|status_trss_|status_tcust_|status_tprio_|small|capitalise|successFunctDefault|sukses|gagal|INFO|load|upTblURL|dataString|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|document|click|split|_|jdl|title|status_rss_|status_cust_|status_prio_|focus|dataJson|text|successFunct|mouseup|hapusItem|ready|preventDefault|option|edit|change|Hydra|cust_id|status_|wrong|status_input_|alert|Isi|tidak|boleh|kosong'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

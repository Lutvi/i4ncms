<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0);
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarialbum/'.$txtcari:'album/update_tabel';
?>
var postURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/album/update_album'); ?>';
var upTblURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
var jmlAnak = <?php echo ( ! $super_admin)?0:$jml_gallery; ?>;
var jmlParent = <?php echo ( ! $super_admin)?0:count($parent_album); ?>;

</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');

	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){

	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".rup").click(function(){
	    var ID = $(this).attr('id').split("_");
	    var albid = ID[1];
	    var opos = parseInt(ID[2]);
	    var pos = parseInt(ID[2])-1;
	    var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&albid='+albid+'&posisi='+pos+'&old_posisi='+opos;

	    $('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');

		aksiFormAJAX(postURL,dataString,successFunctDefault);

	    return false;
	});
	$(".rdown").click(function(){
		var ID = $(this).attr('id').split("_");
		var albid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])+1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&albid='+albid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		
		return false;
	});
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#utama_"+ID).hide();
		$("#utama_input_"+ID).show();
		$("#utama_input_"+ID).focus();
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		$("#komentar_"+ID).hide();
		$("#komentar_input_"+ID).show();
		$("#komentar_input_"+ID).focus();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var utama=$("#utama_input_"+ID[2]).val();
		var status=$("#status_input_"+ID[2]).val();
		var komentar=$("#komentar_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&album_id='+ID[2]+'&utama='+utama+'&status='+status+'&komentar='+komentar;
	
		if(utama != '' || status != '' || komentar != '')
		{
			$(".editbox").hide();
			$(".text").show();
			$("#utama_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#komentar_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data == 'sukses') {
					$('#utama_'+ID[2]).html(capitalise(utama));
					$('#status_'+ID[2]).html(capitalise(status));
					$('#komentar_'+ID[2]).html(capitalise(komentar));
				} else {
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
		
			aksiFormAJAX(postURL,dataString,successFunct);
		} else {
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara on dan off.<br>on = aktif dan off = tidak aktif.').dialog( "open" );
		}
	});
	$(".editbox").mouseup(function(){
		return false;
	});
	$(document).mouseup(function(){
		$(".editbox").hide();
		$(".text").show();
		return false;
	});

});
</script>
<?php else: ?>
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 z(7){V(7===\'19\'){}W{$("#X").7(\'Y\',7).l("A")}$(\'#n\').1a(1b)}5 1o(0){3 Z=0[1];3 8=9+\'=\'+$("B[C="+9+"]").m()+\'&Z=\'+Z;$(\'#n\').6(\'<b 4="D" E="F-G:\'+H+\'I;"><c d="\'+f+\'g/h/J-i.j" 4="k" /></b>\');K(L,8,z)}$(1c).1p(5(){$(\'a.10\').M(5(e){e.1q();3 0=$(o).p(\'t\').N("O");3 1d=$(o).p(\'1e\');$("#l-10").l("1r","1e",1d);$("#l-10").7(\'0\',0).l("A");q r});$(".1s").M(5(){3 0=$(o).p(\'t\').N("O");3 s=0[1];3 P=Q(0[2]);3 R=Q(0[2])-1;3 8=9+\'=\'+$("B[C="+9+"]").m()+\'&s=\'+s+\'&1f=\'+R+\'&1g=\'+P;$(\'#n\').6(\'<b 4="D" E="F-G:\'+H+\'I;"><c d="\'+f+\'g/h/J-i.j" 4="k" /></b>\');K(L,8,z);q r});$(".1t").M(5(){3 0=$(o).p(\'t\').N("O");3 s=0[1];3 P=Q(0[2]);3 R=Q(0[2])+1;3 8=9+\'=\'+$("B[C="+9+"]").m()+\'&s=\'+s+\'&1f=\'+R+\'&1g=\'+P;$(\'#n\').6(\'<b 4="D" E="F-G:\'+H+\'I;"><c d="\'+f+\'g/h/J-i.j" 4="k" /></b>\');K(L,8,z);q r});$(".1u").M(5(){3 0=$(o).p(\'t\');$("#11"+0).u();$("#12"+0).v();$("#12"+0).13();$("#S"+0).u();$("#14"+0).v();$("#14"+0).13();$("#15"+0).u();$("#16"+0).v();$("#16"+0).13();q r});$(".T").1v(5(){3 0=$(o).p(\'t\').N("O");3 w=$("#12"+0[2]).m();3 x=$("#14"+0[2]).m();3 y=$("#16"+0[2]).m();3 8=9+\'=\'+$("B[C="+9+"]").m()+\'&1w=\'+0[2]+\'&w=\'+w+\'&x=\'+x+\'&y=\'+y;V(w!=\'\'||x!=\'\'||y!=\'\'){$(".T").u();$(".1h").v();$("#11"+0[2]).6(\'<c d="\'+f+\'g/h/U-i-17.j" 4="k" />\');$("#S"+0[2]).6(\'<c d="\'+f+\'g/h/U-i-17.j" 4="k" />\');$("#15"+0[2]).6(\'<c d="\'+f+\'g/h/U-i-17.j" 4="k" />\');5 1i(7){V(7==\'19\'){$(\'#11\'+0[2]).6(18(w));$(\'#S\'+0[2]).6(18(x));$(\'#15\'+0[2]).6(18(y))}W{$(\'#n\').6(\'<b 4="D" E="F-G:\'+H+\'I;"><c d="\'+f+\'g/h/J-i.j" 4="k" /></b>\');$("#X").7(\'Y\',7).l("A");$(\'#n\').1a(1b)}}K(L,8,1i)}W{$("#S"+0[2]).6(\'<c d="\'+f+\'g/h/1x-U-i.j" 4="k" />\');$("#X").7(\'Y\',\'1y 1z 1A 1j 1k 1l.<1B>1j = 1m 1k 1l = 1C 1m.\').l("A")}});$(".T").1n(5(){q r});$(1c).1n(5(){$(".T").u();$(".1h").v();q r})});',62,101,'ID|||var|align|function|html|data|dataString|tkn||div|img|src||baseURL|assets|icons|loader|gif|absmiddle|dialog|val|tabel|this|attr|return|false|albid|id|hide|show|utama|status|komentar|successFunctDefault|open|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|split|_|opos|parseInt|pos|status_|editbox|ajax|if|else|gagal|INFO|id_hps|hapus|utama_|utama_input_|focus|status_input_|komentar_|komentar_input_|small|capitalise|sukses|load|upTblURL|document|jdl|title|posisi|old_posisi|text|successFunct|on|dan|off|aktif|mouseup|hapusItem|ready|preventDefault|option|rup|rdown|edit|change|album_id|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

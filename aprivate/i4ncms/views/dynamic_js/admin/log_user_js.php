<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(4)))
	$offset = $this->uri->segment(4,0); 
	else
	$offset = $this->uri->segment(5,0);

$uptbl = (isset($txtcari))?'tblcarilog_user/'.$this->uri->segment(3,'ajax').'/'.$txtcari:'log_user/update_tabel/'.$this->uri->segment(3,'ajax');
?>
var postURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/log_user/hapus_log'); ?>';
var cekTabURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/log_user/cek_tab'); ?>';
var upTblURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

$(function() {
	var aktif = $( '#aktifTxt' ).val();

	$( "#logs-tab" ).tabs({ active: aktif });

	$( "#logs-tab" ).on( "tabsbeforeactivate", function( event, ui ) {
		var cek = ui.newTab.index();
		$( '#aktifTxt' ).val(cek);
		$.get(cekTabURL, { aktif_tab_log:cek });
	});

	$("button.hps").button({
		icons: {
			primary: "ui-icon-trash"
		},
		disabled : true
	});

	$( "#hpsErrLog" ).click(function(e){
		var isi = $("#frmLogErr").serialize();
		var postURL = $("#frmLogErr").attr('action');

		aksiFormAJAX(postURL,isi,successFunctDefault);
		return false;
	});

});

$(document).ready(function(){

	//cek err log
	$('#pil_err_log').change(function() {
		if($(this).is(':checked'))
		{
			$("input[name='err_log[]']:checkbox").attr("checked","checked");
			$("button.hps").button({ disabled : false });
		} else {
			$("input[name='err_log[]']:checkbox").removeAttr("checked","checked");
			$("button.hps").button({ disabled : true });
		}
	});

	$("input[name='err_log[]']:checkbox").change(function(e) {
		var iscek = $("input[name='err_log[]']:checked").size();
		if (iscek > 0)
			$("button.hps").button({ disabled : false });
		else
			$("button.hps").button({ disabled : true });
	});

	$('#logs-tab').tabs();
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('2 l(7){b(7===\'w\'){}c{$("#x").7(\'y\',7).z("A")}$(\'#B\').C(D)}$(2(){4 m=$(\'#n\').o();$("#d-f").p({E:m});$("#d-f").F("G",2(H,g){4 h=g.I.J();$(\'#n\').o(h);$.K(L,{M:h})});$("1.5").1({N:{O:"g-P-Q"},6:i});$("#R").S(2(e){4 q=$("#r").T();4 s=$("#r").t(\'U\');V(s,q,l);W j})});$(X).Y(2(){$(\'#Z\').u(2(){b($(10).11(\':3\')){$("8[9=\'a[]\']:k").t("3","3");$("1.5").1({6:j})}c{$("8[9=\'a[]\']:k").12("3","3");$("1.5").1({6:i})}});$("8[9=\'a[]\']:k").u(2(e){4 v=$("8[9=\'a[]\']:3").13();b(v>0)$("1.5").1({6:j});c $("1.5").1({6:i})});$(\'#d-f\').p()});',62,66,'|button|function|checked|var|hps|disabled|data|input|name|err_log|if|else|logs||tab|ui|cek|true|false|checkbox|successFunctDefault|aktif|aktifTxt|val|tabs|isi|frmLogErr|postURL|attr|change|iscek|sukses|gagal|INFO|dialog|open|tabel|load|upTblURL|active|on|tabsbeforeactivate|event|newTab|index|get|cekTabURL|aktif_tab_log|icons|primary|icon|trash|hpsErrLog|click|serialize|action|aksiFormAJAX|return|document|ready|pil_err_log|this|is|removeAttr|size'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

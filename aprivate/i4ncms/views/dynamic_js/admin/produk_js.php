<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcariproduk/'.$txtcari:'produk/update_tabel';
?>
var postURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/produk/update_produk'); ?>';
var upTblURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_widget script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;
	
	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}
    
$(document).ready(function(){

    $('a.hapus').click(function(e){
        e.preventDefault();
        var ID = $(this).attr('id').split("_");
        var jdl = $(this).attr('title');
        
        $( "#dialog-hapus" ).dialog("option", "title", "Konfirmasi " + jdl);
        $( "#dialog-hapus" ).data('ID',ID).dialog( "open");
        return false;
    });

    $(".edit").click(function(){
        var ID=$(this).attr('id');
        $("#stok_"+ID).hide();
        $("#stok_input_"+ID).show();
        $("#stok_input_"+ID).focus();
        $("#promo_"+ID).hide();
        $("#promo_input_"+ID).show();
        $("#promo_input_"+ID).focus();
        $("#status_"+ID).hide();
        $("#status_input_"+ID).show();
        $("#status_input_"+ID).focus();
        
        return false;
    });
    $(".editbox").change(function(){
        var ID=$(this).attr('id').split("_");
        var stok=$("#stok_input_"+ID[2]).val();
        var promo=$("#promo_input_"+ID[2]).val();
        var status=$("#status_input_"+ID[2]).val();
        var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&prod_id='+ID[2]+'&stok='+stok+'&promo='+promo+'&status='+status;
        
        if(status != '' || stok >= 0 || stok.constructor == Integer || promo != ''){
            $(".editbox").hide();
            $(".text").show();
            $("#stok_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
            $("#promo_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
            $("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
            $("#judul_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" /></div>');

			function successFunct(data)
			{
				if(data === 'sukses'){
					$('#stok_'+ID[2]).html(stok);
					$('#promo_'+ID[2]).html((promo == 'on')?'On':'Off');
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
			aksiFormAJAX(postURL,dataString,successFunct);
        }else{
            $("#status_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" /></div>');
			if(stok < 0 || stok.constructor != Integer)
            $( "#gagal" ).data('INFO','Isi dengan nomor dan lebih besar dari 0').dialog( "open");
            else
            $( "#gagal" ).data('INFO','Isi hanya antara on dan off.<br>on = aktif dan off = tidak aktif.').dialog( "open");
        }
    });
    $(".editbox").mouseup(function(){
        return false;
    });

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
});

</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('8 T(4){t(4===\'U\'){}u{$("#v").4(\'w\',4).9("m")}$(\'#x\').V(W)}8 1n(3){6 H=3[1];6 y=z+\'=\'+$("X[Y="+z+"]").n()+\'&H=\'+H;$(\'#x\').7(\'<b 5="A" Z="10-11:\'+12+\'13;"><d f="\'+g+\'h/i/14-j.k" 5="l" /></b>\');15(16,y,T)}$(17).1o(8(){$(\'a.I\').18(8(e){e.1p();6 3=$(B).C(\'J\').19("1a");6 1b=$(B).C(\'1c\');$("#9-I").9("1q","1c","1r "+1b);$("#9-I").4(\'3\',3).9("m");K L});$(".1s").18(8(){6 3=$(B).C(\'J\');$("#M"+3).o();$("#N"+3).p();$("#N"+3).O();$("#P"+3).o();$("#Q"+3).p();$("#Q"+3).O();$("#D"+3).o();$("#R"+3).p();$("#R"+3).O();K L});$(".E").1t(8(){6 3=$(B).C(\'J\').19("1a");6 c=$("#N"+3[2]).n();6 q=$("#Q"+3[2]).n();6 r=$("#R"+3[2]).n();6 y=z+\'=\'+$("X[Y="+z+"]").n()+\'&1u=\'+3[2]+\'&c=\'+c+\'&q=\'+q+\'&r=\'+r;t(r!=\'\'||c>=0||c.1d==1e||q!=\'\'){$(".E").o();$(".1f").p();$("#M"+3[2]).7(\'<d f="\'+g+\'h/i/s-j-F.k" 5="l" />\');$("#P"+3[2]).7(\'<d f="\'+g+\'h/i/s-j-F.k" 5="l" />\');$("#D"+3[2]).7(\'<d f="\'+g+\'h/i/s-j-F.k" 5="l" />\');$("#1v"+3[2]).7(\'<b 5="A"><d f="\'+g+\'h/i/s-j-F.k" 5="l" /></b>\');8 1g(4){t(4===\'U\'){$(\'#M\'+3[2]).7(c);$(\'#P\'+3[2]).7((q==\'G\')?\'1h\':\'1i\');$(\'#D\'+3[2]).7((r==\'G\')?\'1h\':\'1i\')}u{$(\'#x\').7(\'<b 5="A" Z="10-11:\'+12+\'13;"><d f="\'+g+\'h/i/14-j.k" 5="l" /></b>\');$("#v").4(\'w\',4).9("m");$(\'#x\').V(W)}}15(16,y,1g)}u{$("#D"+3[2]).7(\'<b 5="A"><d f="\'+g+\'h/i/1w-s-j.k" 5="l" /></b>\');t(c<0||c.1d!=1e)$("#v").4(\'w\',\'1j 1x 1y S 1z 1A 1B 0\').9("m");u $("#v").4(\'w\',\'1j 1C 1D G S 1k.<1E>G = 1l S 1k = 1F 1l.\').9("m")}});$(".E").1m(8(){K L});$(17).1m(8(){$(".E").o();$(".1f").p()})});',62,104,'|||ID|data|align|var|html|function|dialog||div|stok|img||src|baseURL|assets|icons|loader|gif|absmiddle|open|val|hide|show|promo|status|ajax|if|else|gagal|INFO|tabel|dataString|tkn|center|this|attr|status_|editbox|small|on|id_hps|hapus|id|return|false|stok_|stok_input_|focus|promo_|promo_input_|status_input_|dan|successFunctDefault|sukses|load|upTblURL|input|name|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|document|click|split|_|jdl|title|constructor|Integer|text|successFunct|On|Off|Isi|off|aktif|mouseup|hapusItem|ready|preventDefault|option|Konfirmasi|edit|change|prod_id|judul_|wrong|dengan|nomor|lebih|besar|dari|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(5)))
	$offset = $this->uri->segment(5,0); 
	else
	$offset = $this->uri->segment(6,0);

$uptbl = (isset($txtcari))?'tblcariplaylist/'.$txtcari.'/'.$id_video:'video/update_tabel_playlist/'.$id_video;
?>
var postURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/video/update_playlist'); ?>';
var upTblURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

$(function($) {
    if($('#info-gallery').is(':visible')){
		$( "#refresh,#refresh-anak" ).button({ disabled:true });
        $('#info-gallery').effect('pulsate',{},800).delay(15000).slideUp('slow', function(){ $( "#refresh,#refresh-anak" ).button({ disabled:false }); });
    }
});

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){

	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".rup").click(function(){
	    var ID = $(this).attr('id').split("_");
	    var vdoid = ID[1];
	    var opos = parseInt(ID[2]);
	    var pos = parseInt(ID[2])-1;
	    var parent_video = $(this).attr('data-parentvideo');
	    var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&vdoid='+vdoid+'&posisi='+pos+'&old_posisi='+opos+'&parent_video='+parent_video;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	    aksiFormAJAX(postURL,dataString,successFunctDefault);
	    return false;
	});
	$(".rdown").click(function(){
		var ID = $(this).attr('id').split("_");
		var vdoid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])+1;
		var parent_video = $(this).attr('data-parentvideo');
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&vdoid='+vdoid+'&posisi='+pos+'&old_posisi='+opos+'&parent_video='+parent_video;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		return false;
	});
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&video_id='+ID[2]+'&status='+status;
		
		if(status == 'on' || status == 'off' || status != ''){
			$(".editbox").hide();
			$(".text").show();
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
	
			function successFunct(data)
			{
				if(data === 'sukses') {
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
				} else {
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
			aksiFormAJAX(postURL,dataString,successFunct);

		} else {
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open" );
		}	
	});
	$(".editbox").mouseup(function(){
		return false;
	});

});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 x(6){y(6===\'12\'){}U{$("#V").6(\'W\',6).f("z")}$(\'#i\').13(14)}$(5($){y($(\'#15-16\').1n(\':1o\')){$("#A,#A-17").18({19:1p});$(\'#15-16\').1q(\'1r\',{},1s).1t(1u).1v(\'1w\',5(){$("#A,#A-17").18({19:j})})}});5 1x(3){4 X=3[1];4 8=9+\'=\'+$("B[C="+9+"]").u()+\'&X=\'+X;$(\'#i\').g(\'<b 7="D" E="F-G:\'+H+\'I;"><k l="\'+m+\'n/o/J-p.q" 7="r" /></b>\');K(L,8,x)}$(1a).1y(5(){$(\'a.Y\').M(5(e){e.1z();4 3=$(c).d(\'v\').N("O");4 1b=$(c).d(\'1c\');$("#f-Y").f("1A","1c",1b);$("#f-Y").6(\'3\',3).f("z");w j});$(".1B").M(5(){4 3=$(c).d(\'v\').N("O");4 s=3[1];4 P=Q(3[2]);4 R=Q(3[2])-1;4 t=$(c).d(\'6-1d\');4 8=9+\'=\'+$("B[C="+9+"]").u()+\'&s=\'+s+\'&1e=\'+R+\'&1f=\'+P+\'&t=\'+t;$(\'#i\').g(\'<b 7="D" E="F-G:\'+H+\'I;"><k l="\'+m+\'n/o/J-p.q" 7="r" /></b>\');K(L,8,x);w j});$(".1C").M(5(){4 3=$(c).d(\'v\').N("O");4 s=3[1];4 P=Q(3[2]);4 R=Q(3[2])+1;4 t=$(c).d(\'6-1d\');4 8=9+\'=\'+$("B[C="+9+"]").u()+\'&s=\'+s+\'&1e=\'+R+\'&1f=\'+P+\'&t=\'+t;$(\'#i\').g(\'<b 7="D" E="F-G:\'+H+\'I;"><k l="\'+m+\'n/o/J-p.q" 7="r" /></b>\');K(L,8,x);w j});$(".1D").M(5(){4 3=$(c).d(\'v\');$("#S"+3).Z();$("#10"+3).11();$("#10"+3).1E();w j});$(".T").1F(5(){4 3=$(c).d(\'v\').N("O");4 h=$("#10"+3[2]).u();4 8=9+\'=\'+$("B[C="+9+"]").u()+\'&1G=\'+3[2]+\'&h=\'+h;y(h==\'1g\'||h==\'1H\'||h!=\'\'){$(".T").Z();$(".1h").11();$("#S"+3[2]).g(\'<k l="\'+m+\'n/o/1i-p-1I.q" 7="r" />\');5 1j(6){y(6===\'12\'){$(\'#S\'+3[2]).g((h==\'1g\')?\'1J\':\'1K\')}U{$(\'#i\').g(\'<b 7="D" E="F-G:\'+H+\'I;"><k l="\'+m+\'n/o/J-p.q" 7="r" /></b>\');$("#V").6(\'W\',6).f("z");$(\'#i\').13(14)}}K(L,8,1j)}U{$("#S"+3[2]).g(\'<k l="\'+m+\'n/o/1L-1i-p.q" 7="r" />\');$("#V").6(\'W\',\'1M 1N 1O 1 1k 0.<1P>1 = 1l 1k 0 = 1Q 1l.\').f("z")}});$(".T").1m(5(){w j})});$(1a).1m(5(){$(".T").Z();$(".1h").11()});',62,115,'|||ID|var|function|data|align|dataString|tkn||div|this|attr||dialog|html|status|tabel|false|img|src|baseURL|assets|icons|loader|gif|absmiddle|vdoid|parent_video|val|id|return|successFunctDefault|if|open|refresh|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|split|_|opos|parseInt|pos|status_|editbox|else|gagal|INFO|id_hps|hapus|hide|status_input_|show|sukses|load|upTblURL|info|gallery|anak|button|disabled|document|jdl|title|parentvideo|posisi|old_posisi|on|text|ajax|successFunct|dan|aktif|mouseup|is|visible|true|effect|pulsate|800|delay|15000|slideUp|slow|hapusItem|ready|preventDefault|option|rup|rdown|edit|focus|change|video_id|off|small|On|Off|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

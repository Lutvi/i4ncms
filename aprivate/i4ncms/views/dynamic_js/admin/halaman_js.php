<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarihalaman/'.$txtcari:'halaman/update_tabel';
?>
var postURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/halaman/update_halaman'); ?>';
var upTblURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<!-- halaman script -->
<script type="text/javascript">

$(document).ready(function(){
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&halaman_id='+ID[2]+'&status='+status;
		
		if(status == 'on' || status == 'off' || status != ''){
			$(".editbox").hide();
			$(".text").show();
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data === 'sukses') {
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
		
			aksiFormAJAX(postURL,dataString,successFunct);
		}else{
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open" );
		}	
	});
	$(".editbox").mouseup(function(){
		return false;
	});
	$(document).mouseup(function(){
		$(".editbox").hide();
		$(".text").show();
	});
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(n).L(5(){$(".M").N(5(){7 3=$(o).p(\'q\');$("#8"+3).c();$("#d"+3).e();$("#d"+3).O();r s});$(".9").P(5(){7 3=$(o).p(\'q\').Q("R");7 4=$("#d"+3[2]).t();7 u=v+\'=\'+$("S[T="+v+"]").t()+\'&U=\'+3[2]+\'&4=\'+4;w(4==\'x\'||4==\'V\'||4!=\'\'){$(".9").c();$(".y").e();$("#8"+3[2]).a(\'<f g="\'+h+\'i/j/z-k-W.l" b="m" />\');5 A(6){w(6===\'X\'){$(\'#8\'+3[2]).a((4==\'x\')?\'Y\':\'Z\')}B{$(\'#C\').a(\'<D b="10" 11="12-13:\'+14+\'15;"><f g="\'+h+\'i/j/16-k.l" b="m" /></D>\');$("#E").6(\'F\',6).G("H");$(\'#C\').17(18)}}19(1a,u,A)}B{$("#8"+3[2]).a(\'<f g="\'+h+\'i/j/1b-z-k.l" b="m" />\');$("#E").6(\'F\',\'1c 1d 1e 1 I 0.<1f>1 = J I 0 = 1g J.\').G("H")}});$(".9").K(5(){r s});$(n).K(5(){$(".9").c();$(".y").e()})});',62,79,'|||ID|status|function|data|var|status_|editbox|html|align|hide|status_input_|show|img|src|baseURL|assets|icons|loader|gif|absmiddle|document|this|attr|id|return|false|val|dataString|tkn|if|on|text|ajax|successFunct|else|tabel|div|gagal|INFO|dialog|open|dan|aktif|mouseup|ready|edit|click|focus|change|split|_|input|name|halaman_id|off|small|sukses|On|Off|center|style|margin|top|cth|px|bar|load|upTblURL|aksiFormAJAX|postURL|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

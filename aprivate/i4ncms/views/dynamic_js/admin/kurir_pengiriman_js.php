<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarikurir/'.$txtcari:'atur_kurir_pengiriman/update_tabel';
?>

var postURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/atur_kurir_pengiriman/update_kurir'); ?>';
var upTblURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<!-- halaman script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){
	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&kurir_id='+ID[2]+'&status='+status;
		
		if(status == 'on' || status == 'off' || status != ''){
			$(".editbox").hide();
			$(".text").show();
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data === 'sukses') {
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
		
			aksiFormAJAX(postURL,dataString,successFunct);
		}else{
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open" );
		}	
	});
	$(".editbox").mouseup(function(){
		return false;
	});
	$(document).mouseup(function(){
		$(".editbox").hide();
		$(".text").show();
	});
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 H(4){u(4===\'I\'){}v{$("#w").4(\'x\',4).7("c")}$(\'#d\').J(K)}5 19(3){6 y=3[1];6 f=g+\'=\'+$("L[M="+g+"]").z()+\'&y=\'+y;$(\'#d\').b(\'<h 9="N" O="P-Q:\'+R+\'S;"><i j="\'+k+\'l/m/T-n.o" 9="p" /></h>\');U(V,f,H)}$(W).1a(5(){$(\'a.A\').X(5(e){e.1b();6 3=$(q).r(\'B\').Y("Z");6 10=$(q).r(\'11\');$("#7-A").7("1c","11",10);$("#7-A").4(\'3\',3).7("c");C D});$(".1d").X(5(){6 3=$(q).r(\'B\');$("#s"+3).E();$("#F"+3).G();$("#F"+3).1e();C D});$(".t").1f(5(){6 3=$(q).r(\'B\').Y("Z");6 8=$("#F"+3[2]).z();6 f=g+\'=\'+$("L[M="+g+"]").z()+\'&1g=\'+3[2]+\'&8=\'+8;u(8==\'12\'||8==\'1h\'||8!=\'\'){$(".t").E();$(".13").G();$("#s"+3[2]).b(\'<i j="\'+k+\'l/m/14-n-1i.o" 9="p" />\');5 15(4){u(4===\'I\'){$(\'#s\'+3[2]).b((8==\'12\')?\'1j\':\'1k\')}v{$(\'#d\').b(\'<h 9="N" O="P-Q:\'+R+\'S;"><i j="\'+k+\'l/m/T-n.o" 9="p" /></h>\');$("#w").4(\'x\',4).7("c");$(\'#d\').J(K)}}U(V,f,15)}v{$("#s"+3[2]).b(\'<i j="\'+k+\'l/m/1l-14-n.o" 9="p" />\');$("#w").4(\'x\',\'1m 1n 1o 1 16 0.<1p>1 = 17 16 0 = 1q 17.\').7("c")}});$(".t").18(5(){C D});$(W).18(5(){$(".t").E();$(".13").G()})});',62,89,'|||ID|data|function|var|dialog|status|align||html|open|tabel||dataString|tkn|div|img|src|baseURL|assets|icons|loader|gif|absmiddle|this|attr|status_|editbox|if|else|gagal|INFO|id_hps|val|hapus|id|return|false|hide|status_input_|show|successFunctDefault|sukses|load|upTblURL|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|document|click|split|_|jdl|title|on|text|ajax|successFunct|dan|aktif|mouseup|hapusItem|ready|preventDefault|option|edit|focus|change|kurir_id|off|small|On|Off|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

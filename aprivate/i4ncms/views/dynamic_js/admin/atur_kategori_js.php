<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarikategori/'.$txtcari:'atur_kategori/update_tabel';
?>
var postURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/atur_kategori/update_nama'); ?>';
var upTblURL = '<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_menu script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

$(document).ready(function(){

    $(".edit").click(function(){
        var ID=$(this).attr('id');
        $("#status_"+ID).hide();
        $("#status_input_"+ID).show();
        $("#nmid_"+ID).hide();
		$("#nmid_input_"+ID).show();
        $("#nmen_"+ID).hide();
        $("#nmen_input_"+ID).show();
        $("#parent_"+ID).hide();
        $("#parent_select_"+ID).show();

        $(this).focus();
                
        return false;
    });

    $(".editbox2").change(function(){
        var ID=$(this).attr('id').split("_"),
			katid = ID[2];
			nama_id=$("#nmid_input_"+ID[2]).val(),
			nama_ori_id=$("#nmoriid_input_"+ID[2]).val(),
			nama_en=$("#nmen_input_"+ID[2]).val(),
			nama_ori_en=$("#nmorien_input_"+ID[2]).val(),
			tipe=$("#tipe_input_"+ID[2]).val();

			dataJson = {Hydra:$("input[name="+tkn+"]").val(),katid:katid, tipe:tipe, nama_id:nama_id, nama_en:nama_en, nama_ori_id:nama_ori_id, nama_ori_en:nama_ori_en };

        if(nama_id == '' || nama_en == ''){
			if(nama_id == '') {
				$("#nmid_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
				$("#nmid_input_"+ID[2]).focus();
			}else{
				$("#nmen_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
				$("#nmen_input_"+ID[2]).focus();
			}
			alert('Isi tidak boleh kosong..!');
	   }else{
			$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
			aksiFormAJAX(postURL,dataJson,successFunctDefault);
		}
    });
    $(".editbox,.editbox2,.selectbox").mouseup(function(){
        return false;
    });
    $(document).mouseup(function(){
        $(".editbox").hide();
        $(".editbox2, .selectbox").hide();
        $(".text").show();
    });
    
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('1 u(7){a(7===\'L\'){}b{$("#M").7(\'N\',7).O("P")}$(\'#v\').Q(R)}$(w).S(1(){$(".T").U(1(){x 0=$(c).y(\'z\');$("#V"+0).3();$("#W"+0).5();$("#A"+0).3();$("#d"+0).5();$("#B"+0).3();$("#e"+0).5();$("#X"+0).3();$("#Y"+0).5();$(c).f();C D});$(".g").Z(1(){x 0=$(c).y(\'z\').10("11"),h=0[2];6=$("#d"+0[2]).4(),i=$("#12"+0[2]).4(),8=$("#e"+0[2]).4(),j=$("#13"+0[2]).4(),k=$("#14"+0[2]).4();E={15:$("16[17="+18+"]").4(),h:h,k:k,6:6,8:8,i:i,j:j};a(6==\'\'||8==\'\'){a(6==\'\'){$("#A"+0[2]).l(\'<m n="\'+o+\'p/q/F-G-r.s" 9="t" />\');$("#d"+0[2]).f()}b{$("#B"+0[2]).l(\'<m n="\'+o+\'p/q/F-G-r.s" 9="t" />\');$("#e"+0[2]).f()}19(\'1a 1b 1c 1d..!\')}b{$(\'#v\').l(\'<H 9="1e" 1f="1g-1h:\'+1i+\'1j;"><m n="\'+o+\'p/q/1k-r.s" 9="t" /></H>\');1l(1m,E,u)}});$(".I,.g,.J").K(1(){C D});$(w).K(1(){$(".I").3();$(".g, .J").3();$(".1n").5()})});',62,86,'ID|function||hide|val|show|nama_id|data|nama_en|align|if|else|this|nmid_input_|nmen_input_|focus|editbox2|katid|nama_ori_id|nama_ori_en|tipe|html|img|src|baseURL|assets|icons|loader|gif|absmiddle|successFunctDefault|tabel|document|var|attr|id|nmid_|nmen_|return|false|dataJson|wrong|ajax|div|editbox|selectbox|mouseup|sukses|gagal|INFO|dialog|open|load|upTblURL|ready|edit|click|status_|status_input_|parent_|parent_select_|change|split|_|nmoriid_input_|nmorien_input_|tipe_input_|Hydra|input|name|tkn|alert|Isi|tidak|boleh|kosong|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|text'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcariwidget/'.$txtcari:'atur_widget/update_tabel';
?>
var postURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_widget/update_widget'); ?>';
var upTblURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_widget script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;
	
	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}
	
$(document).ready(function(){

	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", "Konfirmasi " + jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".rup").click(function(){
		var ID = $(this).attr('id').split("_");
		var wid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])-1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&wid='+wid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		return false;
	});
	$(".rdown").click(function(){
		var ID = $(this).attr('id').split("_");
		var wid = ID[1];
		var opos = parseInt(ID[2]);
		var pos = parseInt(ID[2])+1;
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&wid='+wid+'&posisi='+pos+'&old_posisi='+opos;
		
		$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);
		return false;
	});

	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		$("#judul_"+ID).hide();
		$("#judul_input_"+ID).show();
		$("#judulen_"+ID).hide();
		$("#judulen_input_"+ID).show();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var judul=$("#judul_input_"+ID[2]).val();
		var judulen=$("#judulen_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&widget_id='+ID[2]+'&status='+status+'&judul_widget='+judul+'&judul_widget_en='+judulen;
		
		if(status == 0 || status == 1){
		   if(judul == ''){
		   		$("#judul_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" /></div>');
				//alert('Isi tidak boleh kosong..!');
				$( "#gagal" ).data('INFO','Isi tidak boleh kosong..!').dialog( "open");
			}else{
				$(".editbox").hide();
				$(".text").show();
				$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
				$("#judul_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" /></div>');
				$("#judulen_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" /></div>');

				function successFunct(data)
				{
					if(data === 'sukses'){
						$('#status_'+ID[2]).html((status == 1)?'On':'Off');
						$('#judul_'+ID[2]).html(judul);
						$('#judulen_'+ID[2]).html(judulen);
					}else{
						$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
						$( "#gagal" ).data('INFO',data).dialog( "open");
						$('#tabel').load(upTblURL);
					}
				}
				aksiFormAJAX(postURL,dataString,successFunct);
			}
		}else{
			$("#status_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" /></div>');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open");
		}
	});
	$(".editbox").mouseup(function(){
		return false;
	});

});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('8 B(9){C(9===\'16\'){}D{$("#E").9(\'F\',9).k("u")}$(\'#p\').17(18)}8 1o(3){4 10=3[1];4 l=m+\'=\'+$("G[H="+m+"]").o()+\'&10=\'+10;$(\'#p\').7(\'<6 5="n" I="J-K:\'+L+\'M;"><b c="\'+d+\'f/g/N-h.i" 5="j" /></6>\');O(P,l,B)}$(19).1p(8(){$(\'a.11\').Q(8(e){e.1q();4 3=$(q).r(\'v\').R("S");4 1a=$(q).r(\'1b\');$("#k-11").k("1r","1b","1s "+1a);$("#k-11").9(\'3\',3).k("u");w x});$(".1t").Q(8(){4 3=$(q).r(\'v\').R("S");4 s=3[1];4 T=U(3[2]);4 V=U(3[2])-1;4 l=m+\'=\'+$("G[H="+m+"]").o()+\'&s=\'+s+\'&1c=\'+V+\'&1d=\'+T;$(\'#p\').7(\'<6 5="n" I="J-K:\'+L+\'M;"><b c="\'+d+\'f/g/N-h.i" 5="j" /></6>\');O(P,l,B);w x});$(".1u").Q(8(){4 3=$(q).r(\'v\').R("S");4 s=3[1];4 T=U(3[2]);4 V=U(3[2])+1;4 l=m+\'=\'+$("G[H="+m+"]").o()+\'&s=\'+s+\'&1c=\'+V+\'&1d=\'+T;$(\'#p\').7(\'<6 5="n" I="J-K:\'+L+\'M;"><b c="\'+d+\'f/g/N-h.i" 5="j" /></6>\');O(P,l,B);w x});$(".1v").Q(8(){4 3=$(q).r(\'v\');$("#W"+3).y();$("#12"+3).z();$("#12"+3).1w();$("#X"+3).y();$("#1e"+3).z();$("#13"+3).y();$("#1f"+3).z();w x});$(".Y").1x(8(){4 3=$(q).r(\'v\').R("S");4 t=$("#12"+3[2]).o();4 Z=$("#1e"+3[2]).o();4 14=$("#1f"+3[2]).o();4 l=m+\'=\'+$("G[H="+m+"]").o()+\'&1y=\'+3[2]+\'&t=\'+t+\'&1z=\'+Z+\'&1A=\'+14;C(t==0||t==1){C(Z==\'\'){$("#X"+3[2]).7(\'<6 5="n"><b c="\'+d+\'f/g/1g-A-h.i" 5="j" /></6>\');$("#E").9(\'F\',\'1h 1i 1B 1C..!\').k("u")}D{$(".Y").y();$(".1j").z();$("#W"+3[2]).7(\'<b c="\'+d+\'f/g/A-h-15.i" 5="j" />\');$("#X"+3[2]).7(\'<6 5="n"><b c="\'+d+\'f/g/A-h-15.i" 5="j" /></6>\');$("#13"+3[2]).7(\'<6 5="n"><b c="\'+d+\'f/g/A-h-15.i" 5="j" /></6>\');8 1k(9){C(9===\'16\'){$(\'#W\'+3[2]).7((t==1)?\'1D\':\'1E\');$(\'#X\'+3[2]).7(Z);$(\'#13\'+3[2]).7(14)}D{$(\'#p\').7(\'<6 5="n" I="J-K:\'+L+\'M;"><b c="\'+d+\'f/g/N-h.i" 5="j" /></6>\');$("#E").9(\'F\',9).k("u");$(\'#p\').17(18)}}O(P,l,1k)}}D{$("#W"+3[2]).7(\'<6 5="n"><b c="\'+d+\'f/g/1g-A-h.i" 5="j" /></6>\');$("#E").9(\'F\',\'1h 1F 1G 1 1l 0.<1H>1 = 1m 1l 0 = 1i 1m.\').k("u")}});$(".Y").1n(8(){w x})});$(19).1n(8(){$(".Y").y();$(".1j").z()});',62,106,'|||ID|var|align|div|html|function|data||img|src|baseURL||assets|icons|loader|gif|absmiddle|dialog|dataString|tkn|center|val|tabel|this|attr|wid|status|open|id|return|false|hide|show|ajax|successFunctDefault|if|else|gagal|INFO|input|name|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|split|_|opos|parseInt|pos|status_|judul_|editbox|judul|id_hps|hapus|status_input_|judulen_|judulen|small|sukses|load|upTblURL|document|jdl|title|posisi|old_posisi|judul_input_|judulen_input_|wrong|Isi|tidak|text|successFunct|dan|aktif|mouseup|hapusItem|ready|preventDefault|option|Konfirmasi|rup|rdown|edit|focus|change|widget_id|judul_widget|judul_widget_en|boleh|kosong|On|Off|hanya|antara|br'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

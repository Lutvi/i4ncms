<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

	$uptbl = (isset($txtcari))?'tblcariblog/'.$txtcari:'blog/update_tabel';
?>
var postURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/blog/update_blog'); ?>';
var upTblURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(document).ready(function(){

	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#headline_"+ID).hide();
		$("#headline_input_"+ID).show();
		$("#diskusi_"+ID).hide();
		$("#diskusi_input_"+ID).show();
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		
		$(this).focus();
		return false;
	});
	
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var headline=$("#headline_input_"+ID[2]).val();
		var diskusi=$("#diskusi_input_"+ID[2]).val();
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&blog_id='+ID[2]+'&headline='+headline+'&diskusi='+diskusi+'&status='+status;
		
		if(status == 'on' || status == 'off' || status != ''){
			$(".editbox").hide();
			$(".text").show();
			$("#headline_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#diskusi_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
	
			function successFunct(data)
			{
				if(data.aksi === 'sukses') {
					$('#headline_'+ID[2]).html((data.headline == 'y')?'Ya':'Bukan');
					$('#diskusi_'+ID[2]).html((data.diskusi == 'on')?'On':'Off');
					$('#status_'+ID[2]).html((data.status == 'on')?'On':'Off');
				} else {
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data.info).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}

			aksiFormAJAXjson(postURL,dataString,successFunct);
		} else {
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			//alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open" );
		}	
	});
	$(".editbox").mouseup(function(){
		return false;
	});

});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 O(4){B(4===\'P\'){}C{$("#D").4(\'E\',4).9("p")}$(\'#q\').Q(R)}6 1i(3){5 F=3[1];5 r=s+\'=\'+$("S[T="+s+"]").l()+\'&F=\'+F;$(\'#q\').7(\'<t 8="U" V="W-X:\'+Y+\'Z;"><c d="\'+f+\'g/h/10-i.j" 8="k" /></t>\');1j(11,r,O)}$(12).1k(6(){$(\'a.G\').13(6(e){e.1l();5 3=$(m).u(\'H\').14("15");5 16=$(m).u(\'17\');$("#9-G").9("1m","17",16);$("#9-G").4(\'3\',3).9("p");I J});$(".1n").13(6(){5 3=$(m).u(\'H\');$("#K"+3).n();$("#18"+3).o();$("#L"+3).n();$("#19"+3).o();$("#v"+3).n();$("#1a"+3).o();$(m).1o();I J});$(".w").1p(6(){5 3=$(m).u(\'H\').14("15");5 x=$("#18"+3[2]).l();5 z=$("#19"+3[2]).l();5 b=$("#1a"+3[2]).l();5 r=s+\'=\'+$("S[T="+s+"]").l()+\'&1q=\'+3[2]+\'&x=\'+x+\'&z=\'+z+\'&b=\'+b;B(b==\'M\'||b==\'1r\'||b!=\'\'){$(".w").n();$(".1b").o();$("#K"+3[2]).7(\'<c d="\'+f+\'g/h/A-i-N.j" 8="k" />\');$("#L"+3[2]).7(\'<c d="\'+f+\'g/h/A-i-N.j" 8="k" />\');$("#v"+3[2]).7(\'<c d="\'+f+\'g/h/A-i-N.j" 8="k" />\');6 1c(4){B(4.1s===\'P\'){$(\'#K\'+3[2]).7((4.x==\'y\')?\'1t\':\'1u\');$(\'#L\'+3[2]).7((4.z==\'M\')?\'1d\':\'1e\');$(\'#v\'+3[2]).7((4.b==\'M\')?\'1d\':\'1e\')}C{$(\'#q\').7(\'<t 8="U" V="W-X:\'+Y+\'Z;"><c d="\'+f+\'g/h/10-i.j" 8="k" /></t>\');$("#D").4(\'E\',4.1v).9("p");$(\'#q\').Q(R)}}1w(11,r,1c)}C{$("#v"+3[2]).7(\'<c d="\'+f+\'g/h/1x-A-i.j" 8="k" />\');$("#D").4(\'E\',\'1y 1z 1A 1 1f 0.<1B>1 = 1g 1f 0 = 1C 1g.\').9("p")}});$(".w").1h(6(){I J})});$(12).1h(6(){$(".w").n();$(".1b").o()});',62,101,'|||ID|data|var|function|html|align|dialog||status|img|src||baseURL|assets|icons|loader|gif|absmiddle|val|this|hide|show|open|tabel|dataString|tkn|div|attr|status_|editbox|headline||diskusi|ajax|if|else|gagal|INFO|id_hps|hapus|id|return|false|headline_|diskusi_|on|small|successFunctDefault|sukses|load|upTblURL|input|name|center|style|margin|top|cth|px|bar|postURL|document|click|split|_|jdl|title|headline_input_|diskusi_input_|status_input_|text|successFunct|On|Off|dan|aktif|mouseup|hapusItem|aksiFormAJAX|ready|preventDefault|option|edit|focus|change|blog_id|off|aksi|Ya|Bukan|info|aksiFormAJAXjson|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

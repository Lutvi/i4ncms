<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(3)))
	$offset = $this->uri->segment(3,0); 
	else
	$offset = $this->uri->segment(4,0);

$uptbl = (isset($txtcari))?'tblcarimenu/'.$txtcari:'atur_menu/update_tabel';
?>
var postURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/atur_menu/update_menu'); ?>';
var upTblURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_menu script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;
	
	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');

	aksiFormAJAX(postURL,dataString,successFunctDefault);

}

$(document).ready(function(){

    $('a.hapus').click(function(e){
        e.preventDefault();
        var ID = $(this).attr('id').split("_");
        var jdl = $(this).attr('title');
        
        $( "#dialog-hapus" ).dialog("option", "title", jdl);
        $( "#dialog-hapus" ).data('ID',ID).dialog( "open");
        return false;
    });
    $(".edit").click(function(){
        var ID=$(this).attr('id');
        $("#status_"+ID).hide();
        $("#status_input_"+ID).show();
        $("#nmid_"+ID).hide();
		$("#nmid_input_"+ID).show();
        $("#nmen_"+ID).hide();
        $("#nmen_input_"+ID).show();
        $("#parent_"+ID).hide();
        $("#parent_select_"+ID).show();

        $(this).focus();
                
        return false;
    });
    $(".rup").click(function(){
        var ID = $(this).attr('id').split("_");
        var mnid = ID[1];
        var opos = parseInt(ID[2]);
        var pos = parseInt(ID[2])-1;
        var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&mnid='+mnid+'&posisi='+pos+'&old_posisi='+opos;
        
        $('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);

        return false;
    });
    $(".rdown").click(function(){
        var ID = $(this).attr('id').split("_");
        var mnid = ID[1];
        var opos = parseInt(ID[2]);
        var pos = parseInt(ID[2])+1;
        var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&mnid='+mnid+'&posisi='+pos+'&old_posisi='+opos;
        
        $('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		aksiFormAJAX(postURL,dataString,successFunctDefault);

        return false;
    });
    $(".editbox,.editbox2,.selectbox").change(function(){
        var ID=$(this).attr('id').split("_"),
			nama_id=$("#nmid_input_"+ID[2]).val(),
			nama_ori_id=$("#nmoriid_input_"+ID[2]).val(),
			nama_en=$("#nmen_input_"+ID[2]).val(),
			nama_ori_en=$("#nmorien_input_"+ID[2]).val(),
			status=$("#status_input_"+ID[2]).val(),
			parentid=$("#parent_select_"+ID[2]).val(),
			parentn=$("#parent_select_"+ID[2]+" option:selected").val(),
			halaman=$("#halaman_select_"+ID[2]).val(),
			dataJson = {Hydra:$("input[name="+tkn+"]").val() ,menu_id:ID[2], status:status, nama_id:nama_id, nama_en:nama_en, nama_ori_id:nama_ori_id, nama_ori_en:nama_ori_en, parentid:parentid, halaman:halaman, parentn:parentn };

        if(status == 0 || status == 1){	
           if(status == ''){
                $("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
                $("#status_input_"+ID[2]).focus();
                alert('Isi tidak boleh kosong..!\nIsi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
           }else if(nama_id == '' || nama_en == ''){
				if(nama_id == '') {
					$("#nmid_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
					$("#nmid_input_"+ID[2]).focus();
                }else{
					$("#nmen_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
					$("#nmen_input_"+ID[2]).focus();
                }
                alert('Isi tidak boleh kosong..!');
           }else{
                $('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
                aksiFormAJAX(postURL,dataJson,successFunctDefault);
            }
        }else{
            $("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
            $("#status_input_"+ID[2]).focus();
            alert('Isi hanya antara 1 dan 0.\n1 = aktif dan 0 = tidak aktif.');
        }
    });
    $(".editbox,.editbox2,.selectbox").mouseup(function(){
        return false;
    });
    $(document).mouseup(function(){
        $(".editbox").hide();
        $(".editbox2, .selectbox").hide();
        $(".text").show();
    });
    
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('7 r(s){t(s===\'1x\'){}u{$("#1y").s(\'1z\',s).v("1h")}$(\'#w\').1A(1B)}7 1C(3){4 11=3[1];4 m=k+\'=\'+$("D[E="+k+"]").5()+\'&11=\'+11;$(\'#w\').8(\'<9 6="F" G="H-I:\'+J+\'K;"><b c="\'+d+\'f/g/L-h.i" 6="j" /></9>\');M(N,m,r)}$(1i).1D(7(){$(\'a.12\').O(7(e){e.1E();4 3=$(l).n(\'x\').P("Q");4 1j=$(l).n(\'1k\');$("#v-12").v("1l","1k",1j);$("#v-12").s(\'3\',3).v("1h");y z});$(".1F").O(7(){4 3=$(l).n(\'x\');$("#13"+3).o();$("#R"+3).A();$("#1m"+3).o();$("#14"+3).A();$("#1n"+3).o();$("#15"+3).A();$("#1G"+3).o();$("#16"+3).A();$(l).B();y z});$(".1H").O(7(){4 3=$(l).n(\'x\').P("Q");4 p=3[1];4 S=T(3[2]);4 U=T(3[2])-1;4 m=k+\'=\'+$("D[E="+k+"]").5()+\'&p=\'+p+\'&1o=\'+U+\'&1p=\'+S;$(\'#w\').8(\'<9 6="F" G="H-I:\'+J+\'K;"><b c="\'+d+\'f/g/L-h.i" 6="j" /></9>\');M(N,m,r);y z});$(".1I").O(7(){4 3=$(l).n(\'x\').P("Q");4 p=3[1];4 S=T(3[2]);4 U=T(3[2])+1;4 m=k+\'=\'+$("D[E="+k+"]").5()+\'&p=\'+p+\'&1o=\'+U+\'&1p=\'+S;$(\'#w\').8(\'<9 6="F" G="H-I:\'+J+\'K;"><b c="\'+d+\'f/g/L-h.i" 6="j" /></9>\');M(N,m,r);y z});$(".17,.18,.19").1J(7(){4 3=$(l).n(\'x\').P("Q"),C=$("#14"+3[2]).5(),1a=$("#1K"+3[2]).5(),V=$("#15"+3[2]).5(),1b=$("#1L"+3[2]).5(),q=$("#R"+3[2]).5(),1c=$("#16"+3[2]).5(),1d=$("#16"+3[2]+" 1l:1M").5(),1e=$("#1N"+3[2]).5(),1q={1O:$("D[E="+k+"]").5(),1P:3[2],q:q,C:C,V:V,1a:1a,1b:1b,1c:1c,1e:1e,1d:1d};t(q==0||q==1){t(q==\'\'){$("#13"+3[2]).8(\'<b c="\'+d+\'f/g/W-X-h.i" 6="j" />\');$("#R"+3[2]).B();1f(\'1g Y 1r 1s..!\\1Q 1t 1u 1 Z 0.\\1v = 10 Z 0 = Y 10.\')}u t(C==\'\'||V==\'\'){t(C==\'\'){$("#1m"+3[2]).8(\'<b c="\'+d+\'f/g/W-X-h.i" 6="j" />\');$("#14"+3[2]).B()}u{$("#1n"+3[2]).8(\'<b c="\'+d+\'f/g/W-X-h.i" 6="j" />\');$("#15"+3[2]).B()}1f(\'1g Y 1r 1s..!\')}u{$(\'#w\').8(\'<9 6="F" G="H-I:\'+J+\'K;"><b c="\'+d+\'f/g/L-h.i" 6="j" /></9>\');M(N,1q,r)}}u{$("#13"+3[2]).8(\'<b c="\'+d+\'f/g/W-X-h.i" 6="j" />\');$("#R"+3[2]).B();1f(\'1g 1t 1u 1 Z 0.\\1v = 10 Z 0 = Y 10.\')}});$(".17,.18,.19").1w(7(){y z});$(1i).1w(7(){$(".17").o();$(".18, .19").o();$(".1R").A()})});',62,116,'|||ID|var|val|align|function|html|div||img|src|baseURL||assets|icons|loader|gif|absmiddle|tkn|this|dataString|attr|hide|mnid|status|successFunctDefault|data|if|else|dialog|tabel|id|return|false|show|focus|nama_id|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|click|split|_|status_input_|opos|parseInt|pos|nama_en|wrong|ajax|tidak|dan|aktif|id_hps|hapus|status_|nmid_input_|nmen_input_|parent_select_|editbox|editbox2|selectbox|nama_ori_id|nama_ori_en|parentid|parentn|halaman|alert|Isi|open|document|jdl|title|option|nmid_|nmen_|posisi|old_posisi|dataJson|boleh|kosong|hanya|antara|n1|mouseup|sukses|gagal|INFO|load|upTblURL|hapusItem|ready|preventDefault|edit|parent_|rup|rdown|change|nmoriid_input_|nmorien_input_|selected|halaman_select_|Hydra|menu_id|nIsi|text'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

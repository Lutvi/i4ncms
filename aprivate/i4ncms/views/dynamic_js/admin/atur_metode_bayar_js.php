<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(4)))
	$offset = $this->uri->segment(4,0); 
	else
	$offset = $this->uri->segment(5,0);

$uptbl = (isset($txtcari))?'tblcari_metodebayar/'.$this->uri->segment(3,'ajax').'/'.$txtcari:'atur_metode_bayar/update_tabel/'.$this->uri->segment(3,'ajax');
?>
var postURL = '<?php echo ($super_admin==FALSE)?'#':site_url($this->config->item('admpath').'/atur_metode_bayar/update_metode'); ?>';
var upTblURL = '<?php echo ($super_admin==FALSE)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset); ?>';
var cekTabURL = '<?php echo ($super_admin==FALSE)?'#':site_url($this->config->item('admpath').'/atur_metode_bayar/cek_tab'); ?>';
</script>

<?php if( ENVIRONMENT == 'development') : ?>
<script>

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(function() {
	var aktif = $( '#aktifTxt' ).val();

	$( "#metode-tab" ).tabs({ active: aktif });

	$( "#metode-tab" ).on( "tabsbeforeactivate", function( event, ui ) {
		var cek = ui.newTab.index();
		$( '#aktifTxt' ).val(cek);
		$.get(cekTabURL, { aktif_tab_banner:cek });
	});
});

$(document).ready(function(){
	$('#metode-tab').tabs();

	$('a.hapus').click(function(e){
		e.preventDefault();
		var ID = $(this).attr('id').split("_");
		var jdl = $(this).attr('title');
		
		$( "#dialog-hapus" ).dialog("option", "title", jdl);
		$( "#dialog-hapus" ).data('ID',ID).dialog( "open");
		return false;
	});
	$(".edit").click(function(){
		var ID=$(this).attr('id');
		$("#status_"+ID).hide();
		$("#status_input_"+ID).show();
		$("#status_input_"+ID).focus();
		return false;
	});
	$(".editbox").change(function(){
		var ID=$(this).attr('id').split("_");
		var status=$("#status_input_"+ID[2]).val();
		var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&metode_id='+ID[2]+'&status='+status;
		
		if(status == 'on' || status == 'off' || status != ''){
			$(".editbox").hide();
			$(".text").show();
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');

			function successFunct(data)
			{
				if(data === 'sukses') {
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
		
			aksiFormAJAX(postURL,dataString,successFunct);
		}else{
			$("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" />');
			$( "#gagal" ).data('INFO','Isi hanya antara 1 dan 0.<br>1 = aktif dan 0 = tidak aktif.').dialog( "open" );
		}	
	});
	$(".editbox").mouseup(function(){
		return false;
	});
	$(document).mouseup(function(){
		$(".editbox").hide();
		$(".text").show();
	});
});
</script>

<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 M(5){w(5===\'N\'){}x{$("#y").5(\'z\',5).7("d")}$(\'#f\').O(P)}4 1f(3){6 A=3[1];6 g=h+\'=\'+$("Q[R="+h+"]").b()+\'&A=\'+A;$(\'#f\').c(\'<i 9="S" T="U-V:\'+W+\'X;"><j k="\'+l+\'m/n/Y-o.p" 9="q" /></i>\');Z(10,g,M)}$(4(){6 r=$(\'#11\').b();$("#B-C").12({1g:r});$("#B-C").D("1h",4(1i,13){6 E=13.1j.1k();$(\'#11\').b(E);$.1l(1m,{1n:E})})});$(14).1o(4(){$(\'#B-C\').12();$(\'a.F\').15(4(e){e.1p();6 3=$(s).t(\'G\').16("17");6 18=$(s).t(\'19\');$("#7-F").7("1q","19",18);$("#7-F").5(\'3\',3).7("d");H I});$(".1r").15(4(){6 3=$(s).t(\'G\');$("#u"+3).J();$("#K"+3).L();$("#K"+3).1s();H I});$(".v").1t(4(){6 3=$(s).t(\'G\').16("17");6 8=$("#K"+3[2]).b();6 g=h+\'=\'+$("Q[R="+h+"]").b()+\'&1u=\'+3[2]+\'&8=\'+8;w(8==\'D\'||8==\'1v\'||8!=\'\'){$(".v").J();$(".1a").L();$("#u"+3[2]).c(\'<j k="\'+l+\'m/n/1b-o-1w.p" 9="q" />\');4 1c(5){w(5===\'N\'){$(\'#u\'+3[2]).c((8==\'D\')?\'1x\':\'1y\')}x{$(\'#f\').c(\'<i 9="S" T="U-V:\'+W+\'X;"><j k="\'+l+\'m/n/Y-o.p" 9="q" /></i>\');$("#y").5(\'z\',5).7("d");$(\'#f\').O(P)}}Z(10,g,1c)}x{$("#u"+3[2]).c(\'<j k="\'+l+\'m/n/1z-1b-o.p" 9="q" />\');$("#y").5(\'z\',\'1A 1B 1C 1 1d 0.<1D>1 = r 1d 0 = 1E r.\').7("d")}});$(".v").1e(4(){H I});$(14).1e(4(){$(".v").J();$(".1a").L()})});',62,103,'|||ID|function|data|var|dialog|status|align||val|html|open||tabel|dataString|tkn|div|img|src|baseURL|assets|icons|loader|gif|absmiddle|aktif|this|attr|status_|editbox|if|else|gagal|INFO|id_hps|metode|tab|on|cek|hapus|id|return|false|hide|status_input_|show|successFunctDefault|sukses|load|upTblURL|input|name|center|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|aktifTxt|tabs|ui|document|click|split|_|jdl|title|text|ajax|successFunct|dan|mouseup|hapusItem|active|tabsbeforeactivate|event|newTab|index|get|cekTabURL|aktif_tab_banner|ready|preventDefault|option|edit|focus|change|metode_id|off|small|On|Off|wrong|Isi|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

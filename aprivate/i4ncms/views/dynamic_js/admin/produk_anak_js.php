<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
<?php
	if(is_numeric($this->uri->segment(5)))
	$offset = $this->uri->segment(5,0);
	else
	$offset = $this->uri->segment(6,0);

$uptbl = (isset($txtcari))?'tblcariprod_anak/'.$txtcari.'/'.$id_induk:'produk/detail_anak/'.$id_induk;
?>
var postURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/produk/update_produk'); ?>';
var upTblURL = '<?php echo ( ! $this->super_admin)?'#':site_url($this->config->item('admpath').'/'.$uptbl.'/'.$offset.'/tabel'); ?>';
</script>
<?php if( ENVIRONMENT == 'development') : ?>
<!-- atur_widget script -->
<script type="text/javascript">

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
	aksiFormAJAX(postURL,dataString,successFunctDefault);
}

$(function($) {
    if($('#info-anak').is(':visible')){
		$( "#refresh,#refresh-anak" ).button({ disabled:true });
        $('#info-anak').effect('pulsate',{},800).delay(15000).slideUp('slow', function(){ $( "#refresh,#refresh-anak" ).button({ disabled:false }); });
    }
});
    
$(document).ready(function(){

    $('a.hapus').click(function(e){
        e.preventDefault();
        var ID = $(this).attr('id').split("_");
        var jdl = $(this).attr('title');
        
        $( "#dialog-hapus" ).dialog("option", "title", "Konfirmasi " + jdl);
        $( "#dialog-hapus" ).data('ID',ID).dialog( "open");
        return false;
    });
    $(".edit").click(function(){
        var ID=$(this).attr('id');
        $("#stok_"+ID).hide();
        $("#stok_input_"+ID).show();
        $("#stok_input_"+ID).focus();
        $("#promo_"+ID).hide();
        $("#promo_input_"+ID).show();
        $("#promo_input_"+ID).focus();
        $("#status_"+ID).hide();
        $("#status_input_"+ID).show();
        $("#status_input_"+ID).focus();
        return false;
    });
    $(".editbox").change(function(){
        var ID=$(this).attr('id').split("_");
        var stok=$("#stok_input_"+ID[2]).val();
        var promo=$("#promo_input_"+ID[2]).val();
        var status=$("#status_input_"+ID[2]).val();
        var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&prod_id='+ID[2]+'&stok='+stok+'&promo='+promo+'&status='+status+'&id_anak='+ID[2];
        
        if(status != '' || stok >= 0 || stok.constructor == Integer){
            $(".editbox").hide();
            $(".text").show();
            $("#stok_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
            $("#promo_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
            $("#status_"+ID[2]).html('<img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" />');
            $("#judul_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" /></div>');

			function successFunct(data)
			{
				if(data === 'sukses'){
					$('#stok_'+ID[2]).html(stok);
					$('#status_'+ID[2]).html((status == 'on')?'On':'Off');
					$('#promo_'+ID[2]).html((promo == 'on')?'On':'Off');
				}else{
					$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#tabel').load(upTblURL);
				}
			}
			aksiFormAJAX(postURL,dataString,successFunct);
        }else{
            $("#status_"+ID[2]).html('<div align="center"><img src="'+baseURL+'assets/icons/wrong-ajax-loader.gif" align="absmiddle" /></div>');
            if(stok < 0 || stok.constructor != Integer)
            $( "#gagal" ).data('INFO','Isi dengan nomor dan lebih besar dari 0').dialog( "open");
            else
            $( "#gagal" ).data('INFO','Isi hanya antara on dan off.<br>on = aktif dan off = tidak aktif.').dialog( "open");
        }
    });
    $(".editbox").mouseup(function(){
        return false;
    });

});

$(document).mouseup(function(){
	$(".editbox").hide();
	$(".text").show();
});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 V(5){m(5===\'W\'){}t{$("#u").5(\'v\',5).9("n")}$(\'#w\').X(Y)}4 1s(3){7 K=3[1];7 x=y+\'=\'+$("Z[10="+y+"]").o()+\'&K=\'+K;$(\'#w\').8(\'<b 6="z" 11="12-13:\'+14+\'15;"><d f="\'+g+\'h/i/16-j.k" 6="l" /></b>\');17(18,x,V)}$(4($){m($(\'#19-A\').1t(\':1u\')){$("#B,#B-A").1a({1b:1v});$(\'#19-A\').1w(\'1x\',{},1y).1z(1A).1B(\'1C\',4(){$("#B,#B-A").1a({1b:C})})}});$(1c).1D(4(){$(\'a.L\').1d(4(e){e.1E();7 3=$(D).E(\'M\').1e("1f");7 1g=$(D).E(\'1h\');$("#9-L").9("1F","1h","1G "+1g);$("#9-L").5(\'3\',3).9("n");N C});$(".1H").1d(4(){7 3=$(D).E(\'M\');$("#O"+3).p();$("#P"+3).q();$("#P"+3).Q();$("#R"+3).p();$("#S"+3).q();$("#S"+3).Q();$("#F"+3).p();$("#T"+3).q();$("#T"+3).Q();N C});$(".G").1I(4(){7 3=$(D).E(\'M\').1e("1f");7 c=$("#P"+3[2]).o();7 H=$("#S"+3[2]).o();7 r=$("#T"+3[2]).o();7 x=y+\'=\'+$("Z[10="+y+"]").o()+\'&1J=\'+3[2]+\'&c=\'+c+\'&H=\'+H+\'&r=\'+r+\'&1K=\'+3[2];m(r!=\'\'||c>=0||c.1i==1j){$(".G").p();$(".1k").q();$("#O"+3[2]).8(\'<d f="\'+g+\'h/i/s-j-I.k" 6="l" />\');$("#R"+3[2]).8(\'<d f="\'+g+\'h/i/s-j-I.k" 6="l" />\');$("#F"+3[2]).8(\'<d f="\'+g+\'h/i/s-j-I.k" 6="l" />\');$("#1L"+3[2]).8(\'<b 6="z"><d f="\'+g+\'h/i/s-j-I.k" 6="l" /></b>\');4 1l(5){m(5===\'W\'){$(\'#O\'+3[2]).8(c);$(\'#F\'+3[2]).8((r==\'J\')?\'1m\':\'1n\');$(\'#R\'+3[2]).8((H==\'J\')?\'1m\':\'1n\')}t{$(\'#w\').8(\'<b 6="z" 11="12-13:\'+14+\'15;"><d f="\'+g+\'h/i/16-j.k" 6="l" /></b>\');$("#u").5(\'v\',5).9("n");$(\'#w\').X(Y)}}17(18,x,1l)}t{$("#F"+3[2]).8(\'<b 6="z"><d f="\'+g+\'h/i/1M-s-j.k" 6="l" /></b>\');m(c<0||c.1i!=1j)$("#u").5(\'v\',\'1o 1N 1O U 1P 1Q 1R 0\').9("n");t $("#u").5(\'v\',\'1o 1S 1T J U 1p.<1U>J = 1q U 1p = 1V 1q.\').9("n")}});$(".G").1r(4(){N C})});$(1c).1r(4(){$(".G").p();$(".1k").q()});',62,120,'|||ID|function|data|align|var|html|dialog||div|stok|img||src|baseURL|assets|icons|loader|gif|absmiddle|if|open|val|hide|show|status|ajax|else|gagal|INFO|tabel|dataString|tkn|center|anak|refresh|false|this|attr|status_|editbox|promo|small|on|id_hps|hapus|id|return|stok_|stok_input_|focus|promo_|promo_input_|status_input_|dan|successFunctDefault|sukses|load|upTblURL|input|name|style|margin|top|cth|px|bar|aksiFormAJAX|postURL|info|button|disabled|document|click|split|_|jdl|title|constructor|Integer|text|successFunct|On|Off|Isi|off|aktif|mouseup|hapusItem|is|visible|true|effect|pulsate|800|delay|15000|slideUp|slow|ready|preventDefault|option|Konfirmasi|edit|change|prod_id|id_anak|judul_|wrong|dengan|nomor|lebih|besar|dari|hanya|antara|br|tidak'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

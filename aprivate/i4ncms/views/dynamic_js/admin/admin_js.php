<style>
input#caritxt {width:250px}
</style>
<script type="text/javascript">
	var sts = '<?php echo (isset($sts))?$sts:''; ?>';
	var tkn = '<?php echo $this->config->item('csrf_token_name'); ?>';
	var baseURL = '<?php echo base_url(); ?>';
	var admPath = '<?php echo $this->config->item('admpath'); ?>';
	var manual = <?php echo ($this->config->item('manual_install') === FALSE)?'false':'true'; ?>;
	var spm = <?php echo ($super_admin === TRUE)?'true':'false'; ?>;

	var dcth = 25;
	var th  = $('#tabel').innerHeight();
	var sdh = $('#sidebarWraper').innerHeight();
	var cth = (parseInt(sdh) > 350)?parseInt(th)/2:dcth;

$(function() {
	if($('.flash-info').is(':visible')){
		$( "#refresh,#refresh-anak,#profil" ).button({ disabled:true });
        $('.flash-info').effect('pulsate',{},800).delay(15000).slideUp('slow', function(){ $( "#refresh,#refresh-anak,#profil" ).button({ disabled:false }); });
    }

    if($('.flash-error').is(':visible')){
		$( "#refresh,#refresh-anak,#profil" ).button({ disabled:true });
        $('.flash-error').effect('pulsate',{},800).delay(15000).slideUp('slow', function(){ $( "#refresh,#refresh-anak,#profil" ).button({ disabled:false }); });
    }
});
</script>

<?php if( ENVIRONMENT == 'development' ) : ?>
<!-- admin script -->
<script type="text/javascript">

$(window).load(function(){
	if (window.PIE) {
		$('.rounded, .ui-corner-all').each(function() {
			PIE.attach(this);
		});
	}
});

function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function errPost()
{
    location.reload();
}

function aksiFormAJAX(UrlPost,isiPost,successFunct)
{
	$.ajax({
		type: "POST",
		url: UrlPost,
		data: isiPost,
		success: successFunct,
		error : errPost
	});
}

function aksiFormAJAXjson(UrlPost,isiPost,successFunct)
{
	$.ajax({
		type: "POST",
		url: UrlPost,
		data: isiPost,
		dataType: 'json',
		success: successFunct,
		error : errPost
	});
}

$(function() {

	// Abort AJAX - Long Request
	$.xhrPool = []; // array of uncompleted requests
    $.xhrPool.abortAll = function() { // our abort function
		$(this).each(function(idx, jqXHR) {
			jqXHR.abort();
		});
		$.xhrPool.length = 0
    };

    $.ajaxSetup({
		beforeSend: function(jqXHR) { // before jQuery send the request we will push it to our array
		$.xhrPool.push(jqXHR);
	    },
	    complete: function(jqXHR) { // when some of the requests completed it will splice from the array
			var index = $.xhrPool.indexOf(jqXHR);
			if (index > -1) {
				$.xhrPool.splice(index, 1);
		    }
	    }
    });

	// dashboard-tab
	$("#dashboard-tab").tabs();

	// Button
	$( "#btn-dashboard" ).button({
			text: "Dashboard",
			icons: {
				primary: "ui-icon-bookmark"
			}
	})
	.click(function() {
		window.location.assign(baseURL+admPath+'/dashboard');
	});

	$( "#btn-logout" ).button({
			text: "Dashboard",
			icons: {
				primary: "ui-icon-power"
			}
	})
	.click(function() {
		window.location.assign(baseURL+admPath+'/login/keluar');
	});

	$('#kembali,.kelist').button({icons:{primary: "ui-icon-arrowreturnthick-1-w"}});
	$( "#refresh,#refresh-anak" ).button({
			icons: {
				primary: "ui-icon-refresh"
			}
	});
	$( "#tambah" ).button({
			icons: {
				primary: "ui-icon-plus"
			}
	});
	$( "#install,.install" ).button({
			icons: {
				primary: "ui-icon-arrowthickstop-1-s"
			}
	});
	$( ".simpan" ).button({
			icons: {
				primary: "ui-icon-disk"
			}
	});
	$( "#profil" ).button({
			icons: {
				primary: "ui-icon-person"
			}
	});
	$( "#cari" ).button({
			icons: {
				primary: "ui-icon-search"
			},
			disabled : true
	});
	$( "#newsletter" ).button({
			icons: {
				primary: "ui-icon-mail-closed"
			}
	});

	// all-dialog
	$( "#salah_db" ).dialog({
		autoOpen: false,
		resizable:false,
		draggable: false,
		modal: true,
		create: function(event, ui) {
					$(event.target).parent().css('position', 'fixed');
				},
		buttons: {
					"Tutup": function() {
					  $( this ).dialog( "close" );
					}
			   },
		maxHeight:109
	});
	$( "#gagal" ).dialog({
		autoOpen: false,
		resizable:false,
		draggable: false,
		modal: true,
		create: function(event, ui) {
					$(event.target).parent().css('position', 'fixed');
				},
		open: function(event, ui, data) {
					var INFO = $(this).data('INFO');
					$(this).find('#info').html(INFO);
				},
		buttons: {
					"Tutup": function() {
					  $( this ).dialog( "close" );
					}
			   },
		maxHeight:109
	});
	$( "#info-sukses" ).dialog({
		autoOpen: false,
		resizable:false,
		draggable: false,
		modal: true,
		create: function(event, ui) {
					$(event.target).parent().css('position', 'fixed');
				},
		open: function(event, ui, data) {
					var INFO = $(this).data('INFO');
					$(this).find('#txtsukses').html(INFO);
				},
		buttons: {
					"Tutup": function() {
					  $( this ).dialog( "close" );
					}
			   },
		maxHeight:109
	});
	$( "#nohapus" ).dialog({
		autoOpen: false,
		resizable:false,
		draggable: false,
		modal: true,
		create: function(event, ui) {
					$(event.target).parent().css('position', 'fixed');
				},
		buttons: {
					"Tutup": function() {
					  $( this ).dialog( "close" );
					}
			   },
		maxHeight:109
	});
	$( "#noedit" ).dialog({
		autoOpen: false,
		resizable:false,
		draggable: false,
		modal: true,
		create: function(event, ui) {
					$(event.target).parent().css('position', 'fixed');
				},
		buttons: {
					"Tutup": function() {
					  $( this ).dialog( "close" );
					}
			   },
		maxHeight:109
	});
	$( "#noatur" ).dialog({
		autoOpen: false,
		resizable:false,
		draggable: false,
		modal: true,
		create: function(event, ui) {
					$(event.target).parent().css('position', 'fixed');
				},
		buttons: {
					"Tutup": function() {
					  $( this ).dialog( "close" );
					}
			   },
		maxHeight:109
	});

	$( "#dialog-hapus" ).dialog({
	  autoOpen: false,
	  resizable: false,
	  height:185,
	  modal: true,
	  create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
	  buttons: {
		"Hapus item": function(event, ui) {
			var ID = $(this).data('ID');
			hapusItem(ID);
			$( this ).dialog( "close" );
		},
		"Batal": function() {
		  $( this ).dialog( "close" );
		}
	  }
	});

	$( "#dialog-lanjut" ).dialog({
	  autoOpen: false,
	  resizable: false,
	  height:185,
	  modal: true,
	  create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
	open: function(event, ui, data) {
					var INFO = $(this).data('INFO');
					$(this).find('#info').html(INFO);
				},
	  buttons: {
		"Ya": function(event, ui) {
			var ID = $(this).data('ID');
			lanjutProses(ID);
			$( this ).dialog( "close" );
		},
		"Batal": function() {
		  $( this ).dialog( "close" );
		}
	  }
	});

});

if( typeof upTblURL !== 'undefined' )
{
	$(document).ajaxSend(function(event, jqxhr, settings) {
		if ( settings.url === upTblURL ) {
			$( "#refresh" ).button({ disabled: true });
			$('#tabel').html('<div align="center" class="loadtbl" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
		}
	});

	$(document).ajaxComplete(function(event, xhr, settings) {
		if ( settings.url === upTblURL ) {
			$( "#refresh" ).button({ disabled: false });
		    // cek button tambah anak
		    if(typeof jmlParent !== 'undefined' && typeof jmlAnak !== 'undefined')
		    {
				if(jmlParent > 0 || jmlAnak === 0)
				{
					$('#anak').show();
				}
				else {
					$('#anak').hide();
				}
			}
		}
	});
}


$(document).ready(function(){
	var mt  = $('#mainContent').position();

	// cek admin
	if( spm === false || spm.constructor!=Boolean ){

		$('a.hapus').removeClass('hapus').addClass('dis_hapus');
		$('a.atur').removeClass('atur').addClass('dis_atur');
		$('a.anak').removeClass('anak').addClass('dis_anak');
		$('a.edit').removeClass('edit').addClass('dis_edit');
		$('a.lanjut').removeClass('lanjut').addClass('dis_lanjut');
		$('#install,.install,.simpan,#profil,#refresh,#tambah').button({ disabled: true });
		$(".rup").removeClass('rup').addClass('dis_rup');
		$(".rdown").removeClass('rdown').addClass('dis_rdown');
		$('#tombol').hide();
		$('#stylized').hide();

		$('a.dis_edit,a.dis_hapus,a.dis_atur,a.dis_atur-anak,a.dis_lanjut,a.lanjut,a.atur,a.anak,a.rup,a.rdown,a.dis_rup,a.dis_rdown').click(function(){
			var info = 'Aksi ini di non aktifkan,<br> Anda tidak memiliki akses.!';
			$( "#gagal" ).dialog("option", "title", "Gagal Akses");
			$( "#gagal" ).data('INFO',info).dialog( "open");
			return false;
		});

		$(".rup,.rdown").click(function(){
			return false;
			location.reload();
			if( typeof aksiFormAJAX !== 'undefined' ) {
				
			}
		});
	}
	else {
		if(sts == 'cek'){
			$('#tombol').hide();
			$('#stylized').show();
			$(document).scrollTop(parseInt(mt.top));
		}else if(sts == 'error'){
			$('#tombol').hide();
			$( "#salah_db" ).dialog( "open" );
		}else{
			$('#stylized').hide();
		}
		
		if(manual === false || manual.constructor!=Boolean ){
			$( "#tambah" ).hide();
		}else{
			$('#tambah').click(function(){
				$('#tombol').hide();
				$('#stylized').show();
				$(document).scrollTop(parseInt(mt.top));
			});
		}

		$('#batal').click(function(){
			$('#tombol').show();
			$('#stylized').slideUp(200);
			$('.error').hide();
			$(document).scrollTop(0);
			if(sts == 'cek'){
			   sts = '';
			}
		});
		
		$('a.dis_hapus').click(function(){
			$( "#nohapus" ).dialog( "open" );
			return false;
		});
		$('a.dis_edit').click(function(){
			$( "#noedit" ).dialog( "open" );
			return false;
		});
		
		if (jQuery.isFunction(jQuery.fn.iframeDialog)) {

			function CloseFunction()
			{
				/*$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/ajax-loader-small.gif" align="absmiddle" /></div>');
				$.xhrPool.abortAll();
				$('#tabel').load(upTblURL);*/
				if(sts == 'cek'){
				   sts = '';
				}
				if( typeof upTblURL !== 'undefined' )
				{
					$('#tabel').html('<div align="center" class="loadtbl" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
					$.xhrPool.abortAll();
					$('#tabel').load(upTblURL);
				}
				if( typeof upListTema !== 'undefined' )
				{
					$('#temaWeb').addClass('blurIt');
					$('#temaWeb').load(upListTema);
					$('#temaWeb').removeClass('blurIt');
				}

				$('body').css({'overflow-y': 'auto'});
			}

			function OpenFunction()
			{
				$('body').css({'overflow-y': 'hidden'});
			}

			$('a.atur,a.anak').iframeDialog({
				id: 'aturAppDialog',
				url: $(this).attr("href"),
				title : $(this).attr("data-info"),
				scrolling: 'auto',
				draggable: false,
				modal: true,
				resizable: false,
				height: $(window).height(),
				//width: ($("#container").innerWidth() != null)?$("#container").innerWidth():900,
				width: $(window).innerWidth()+10,
				open : OpenFunction,
				close: CloseFunction
			});
		}
		
		$('a.dis_atur,a.dis_anak').click(function(){
			$( "#noatur" ).dialog( "open" );
			return false;
		});
		
		$('#refresh').click(function(e){
			e.preventDefault();
			$.xhrPool.abortAll();
			$('#tabel').html('<div align="center" class="loadtbl" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');
			$('#tabel').load(upTblURL);
		});

		$('#caritxt').bind('load keyup mousedown blur',function() {
			if($(this).val() != '')
			$( "#cari" ).button({ disabled : false });
			else
			$( "#cari" ).button({ disabled : true });
		});

		$('#cari').click(function(e){
			if($('#caritxt').val() == '')
			return false;
		});

	} //end cek admin

	//login
    $("#login-btn").button({
		icons: {
			primary: "ui-icon-key"
		}
	});

	$("#loginBatal-btn").button({
		icons: {
			primary: "ui-icon-close"
		}
	}).click(function(e){
		$('.error').hide();
		$('#infoLog').hide();
	});

	// tooltip
    $( "#toolbar" ).tooltip({
			track: true
    });
	
});

</script>

<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(I).W(2(){6(I.1I){$(\'.2t, .f-2u-2v\').1J(2(){1I.2w(5)})}});2 2x(a){F a.2y(0).2z()+a.2A(1).2B()}2 1j(){15.1K()}2 1L(a,b,c){$.1M({1N:"1O",X:a,m:b,1P:c,Y:1j})}2 2C(a,b,c){$.1M({1N:"1O",X:a,m:b,2D:\'2E\',1P:c,Y:1j})}$(2(){$.r=[];$.r.1k=2(){$(5).1J(2(a,b){b.2F()});$.r.2G=0};$.2H({2I:2(a){$.r.2J(a)},2K:2(a){t b=$.r.2L(a);6(b>-1){$.r.2M(b,1)}}});$("#1l-2N").2O();$("#16-1l").7({1Q:"1R",8:{g:"f-h-2P"}}).i(2(){I.15.1S(Z+1T+\'/1l\')});$("#16-2Q").7({1Q:"1R",8:{g:"f-h-2R"}}).i(2(){I.15.1S(Z+1T+\'/1U/2S\')});$(\'#2T,.2U\').7({8:{g:"f-h-2V-1-w"}});$("#G,#G-u").7({8:{g:"f-h-G"}});$("#17").7({8:{g:"f-h-2W"}});$("#18,.18").7({8:{g:"f-h-2X-1-s"}});$(".1V").7({8:{g:"f-h-2Y"}});$("#1W").7({8:{g:"f-h-2Z"}});$("#19").7({8:{g:"f-h-30"},J:9});$("#31").7({8:{g:"f-h-32-33"}});$("#1X").4({v:3,n:3,H:3,o:9,x:2(a,b){$(a.z).A().l(\'p\',\'B\')},C:{"K":2(){$(5).4("j")}},L:M});$("#1m").4({v:3,n:3,H:3,o:9,x:2(a,b){$(a.z).A().l(\'p\',\'B\')},q:2(a,b,c){t d=$(5).m(\'1a\');$(5).1n(\'#1b\').N(d)},C:{"K":2(){$(5).4("j")}},L:M});$("#1b-34").4({v:3,n:3,H:3,o:9,x:2(a,b){$(a.z).A().l(\'p\',\'B\')},q:2(a,b,c){t d=$(5).m(\'1a\');$(5).1n(\'#35\').N(d)},C:{"K":2(){$(5).4("j")}},L:M});$("#1Y").4({v:3,n:3,H:3,o:9,x:2(a,b){$(a.z).A().l(\'p\',\'B\')},C:{"K":2(){$(5).4("j")}},L:M});$("#1Z").4({v:3,n:3,H:3,o:9,x:2(a,b){$(a.z).A().l(\'p\',\'B\')},C:{"K":2(){$(5).4("j")}},L:M});$("#20").4({v:3,n:3,H:3,o:9,x:2(a,b){$(a.z).A().l(\'p\',\'B\')},C:{"K":2(){$(5).4("j")}},L:M});$("#4-1o").4({v:3,n:3,1c:21,o:9,x:2(a,b,c){$(a.z).A().l(\'p\',\'B\')},C:{"36 37":2(a,b){t c=$(5).m(\'22\');38(c);$(5).4("j")},"23":2(){$(5).4("j")}}});$("#4-1d").4({v:3,n:3,1c:21,o:9,x:2(a,b,c){$(a.z).A().l(\'p\',\'B\')},q:2(a,b,c){t d=$(5).m(\'1a\');$(5).1n(\'#1b\').N(d)},C:{"39":2(a,b){t c=$(5).m(\'22\');3a(c);$(5).4("j")},"23":2(){$(5).4("j")}}})});6(O P!==\'Q\'){$(R).3b(2(a,b,c){6(c.X===P){$("#G").7({J:9});$(\'#11\').N(\'<S T="1p" 1q="1r" 1s="1t-12:\'+1u+\'1v;"><1w 1x="\'+Z+\'1y/8/1z-1A.1B" T="1C" /></S>\')}});$(R).3c(2(a,b,c){6(c.X===P){$("#G").7({J:3});6(O 24!==\'Q\'&&O 25!==\'Q\'){6(24>0||25===0){$(\'#u\').1e()}U{$(\'#u\').k()}}}})}$(R).3d(2(){t b=$(\'#3e\').p();6(26===3||26.27!=28){$(\'a.1o\').D(\'1o\').E(\'1D\');$(\'a.1f\').D(\'1f\').E(\'1g\');$(\'a.u\').D(\'u\').E(\'29\');$(\'a.2a\').D(\'2a\').E(\'1E\');$(\'a.1d\').D(\'1d\').E(\'2b\');$(\'#18,.18,.1V,#1W,#G,#17\').7({J:9});$(".1h").D(\'1h\').E(\'2c\');$(".1i").D(\'1i\').E(\'2d\');$(\'#13\').k();$(\'#14\').k();$(\'a.1E,a.1D,a.1g,a.1g-u,a.2b,a.1d,a.1f,a.u,a.1h,a.1i,a.2c,a.2d\').i(2(){t a=\'3f 3g 3h 3i 3j,<3k> 3l 3m 3n 3o.!\';$("#1m").4("3p","2e","3q 3r");$("#1m").m(\'1a\',a).4("q");F 3});$(".1h,.1i").i(2(){F 3;15.1K();6(O 1L!==\'Q\'){}})}U{6(V==\'1F\'){$(\'#13\').k();$(\'#14\').1e();$(R).1G(2f(b.12))}U 6(V==\'Y\'){$(\'#13\').k();$("#1X").4("q")}U{$(\'#14\').k()}6(2g===3||2g.27!=28){$("#17").k()}U{$(\'#17\').i(2(){$(\'#13\').k();$(\'#14\').1e();$(R).1G(2f(b.12))})}$(\'#3s\').i(2(){$(\'#13\').1e();$(\'#14\').3t(3u);$(\'.Y\').k();$(R).1G(0);6(V==\'1F\'){V=\'\'}});$(\'a.1D\').i(2(){$("#1Y").4("q");F 3});$(\'a.1E\').i(2(){$("#1Z").4("q");F 3});6(2h.3v(2h.3w.2i)){2 2j(){6(V==\'1F\'){V=\'\'}6(O P!==\'Q\'){$(\'#11\').N(\'<S T="1p" 1q="1r" 1s="1t-12:\'+1u+\'1v;"><1w 1x="\'+Z+\'1y/8/1z-1A.1B" T="1C" /></S>\');$.r.1k();$(\'#11\').W(P)}6(O 2k!==\'Q\'){$(\'#1H\').E(\'2l\');$(\'#1H\').W(2k);$(\'#1H\').D(\'2l\')}$(\'2m\').l({\'2n-y\':\'2o\'})}2 2p(){$(\'2m\').l({\'2n-y\':\'3x\'})}$(\'a.1f,a.u\').2i({3y:\'3z\',X:$(5).2q("3A"),2e:$(5).2q("m-1b"),3B:\'2o\',H:3,o:9,n:3,1c:$(I).1c(),3C:$(I).3D()+10,q:2p,j:2j})}$(\'a.1g,a.29\').i(2(){$("#20").4("q");F 3});$(\'#G\').i(2(e){e.3E();$.r.1k();$(\'#11\').N(\'<S T="1p" 1q="1r" 1s="1t-12:\'+1u+\'1v;"><1w 1x="\'+Z+\'1y/8/1z-1A.1B" T="1C" /></S>\');$(\'#11\').W(P)});$(\'#2r\').3F(\'W 3G 3H 3I\',2(){6($(5).2s()!=\'\')$("#19").7({J:3});U $("#19").7({J:9})});$(\'#19\').i(2(e){6($(\'#2r\').2s()==\'\')F 3})}$("#1U-16").7({8:{g:"f-h-3J"}});$("#3K-16").7({8:{g:"f-h-j"}}).i(2(e){$(\'.Y\').k();$(\'#3L\').k()});$("#3M").3N({3O:9})});',62,237,'||function|false|dialog|this|if|button|icons|true||||||ui|primary|icon|click|close|hide|css|data|resizable|modal|position|open|xhrPool||var|anak|autoOpen||create||target|parent|fixed|buttons|removeClass|addClass|return|refresh|draggable|window|disabled|Tutup|maxHeight|109|html|typeof|upTblURL|undefined|document|div|align|else|sts|load|url|error|baseURL||tabel|top|tombol|stylized|location|btn|tambah|install|cari|INFO|info|height|lanjut|show|atur|dis_atur|rup|rdown|errPost|abortAll|dashboard|gagal|find|hapus|center|class|loadtbl|style|margin|cth|px|img|src|assets|bar|loader|gif|absmiddle|dis_hapus|dis_edit|cek|scrollTop|temaWeb|PIE|each|reload|aksiFormAJAX|ajax|type|POST|success|text|Dashboard|assign|admPath|login|simpan|profil|salah_db|nohapus|noedit|noatur|185|ID|Batal|jmlParent|jmlAnak|spm|constructor|Boolean|dis_anak|edit|dis_lanjut|dis_rup|dis_rdown|title|parseInt|manual|jQuery|iframeDialog|CloseFunction|upListTema|blurIt|body|overflow|auto|OpenFunction|attr|caritxt|val|rounded|corner|all|attach|capitalise|charAt|toUpperCase|slice|toLowerCase|aksiFormAJAXjson|dataType|json|abort|length|ajaxSetup|beforeSend|push|complete|indexOf|splice|tab|tabs|bookmark|logout|power|keluar|kembali|kelist|arrowreturnthick|plus|arrowthickstop|disk|person|search|newsletter|mail|closed|sukses|txtsukses|Hapus|item|hapusItem|Ya|lanjutProses|ajaxSend|ajaxComplete|ready|mainContent|Aksi|ini|di|non|aktifkan|br|Anda|tidak|memiliki|akses|option|Gagal|Akses|batal|slideUp|200|isFunction|fn|hidden|id|aturAppDialog|href|scrolling|width|innerWidth|preventDefault|bind|keyup|mousedown|blur|key|loginBatal|infoLog|toolbar|tooltip|track'.split('|'),0,{}))
</script>
<?php endif; ?>

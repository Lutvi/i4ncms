<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

<script type="text/javascript">
    var stsWeb = $("#stsWebInput").val();
    var scureWeb = $("#ScureWebInput").val();
    var cacheWeb = $("#CacheWebInput").val();
    var saWeb = $("#SuperAdminWebInput").val();
    var iklanWeb = $("#IklanWebInput").val();

    //toko
	var stsToko = $("#stsTokoInput").val();
	var cekStok = $("#cekStokInput").val();
	var ppn = '<?php echo (! $this->super_admin)?'#':$this->config->item('ppn'); ?>';
	var satjen = '<?php echo (! $this->super_admin)?'#':$this->config->item('satuan_jenis_produk'); ?>';
	var satprod = '<?php echo (! $this->super_admin)?'#':$this->config->item('satuan_produk'); ?>';
	var namacs = '<?php echo (! $this->super_admin)?'#':$this->config->item('cs_name'); ?>';
	var emailcs = '<?php echo (! $this->super_admin)?'#':$this->config->item('cs_email'); ?>';
	var tlpcs = '<?php echo (! $this->super_admin)?'#':$this->config->item('cs_telpon'); ?>';
	var smscs = '<?php echo (! $this->super_admin)?'#':$this->config->item('cs_sms'); ?>';

    var idTema = $("#id-tema").val();
    //var ga = '<?php echo (! $this->super_admin)?'#':$this->config->item('google_analytics_code'); ?>';
    //var mcky = '<?php echo (! $this->super_admin)?'#':$this->config->item('microsoft_key'); ?>';
    //var slurp = '<?php echo (! $this->super_admin)?'#':$this->config->item('slurp_key'); ?>';
    var upListTema = '<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/pengaturan/update_list_tema'); ?>';
    var opsiTema = '<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/pengaturan/opsi_tema'); ?>';
    var aturOpsiTema = '<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/pengaturan/atur_opsi_tema'); ?>';

    //backup
    var upBckURL = '<?php echo ( ! $super_admin)?'#':site_url($this->config->item('admpath').'/bckdata/index'); ?>';

	var htmlLoaderB = '<div align="center" style="width:100%; margin-top:20px"><img src="/assets/icons/bar-loader.gif" align="absmiddle" /></div>';
	var htmlLoaderK = '<div align="center" style="margin-top:10px;"><img src="/assets/icons/ajax-loader-small.gif" align="absmiddle" /></div>';
	
</script>

<?php if( ENVIRONMENT == 'development' ) : ?>
<script type="text/javascript">

function cekOpsiTema()
{
    $.get(opsiTema, function(data) {
      if(data == '')
      $("#atur-tema").button({ disabled:true });
      else
      $("#atur-tema").button({ disabled:false });
    });
}

function successFunctDefault(data)
{
	if(data === 'sukses') {
		//alert('OK');
	}else{
		$( "#gagal" ).data('INFO',data).dialog( "open");
	}
	$('#tabel').load(upTblURL);
}

function hapusItem(ID)
{
	var id_hps = ID[1];
	var dataString = tkn+'='+$("input[name="+tkn+"]").val()+'&id_hps='+id_hps;

	$('#tabel').html('<div align="center" style="margin-top:'+cth+'px;"><img src="'+baseURL+'assets/icons/bar-loader.gif" align="absmiddle" /></div>');

	aksiFormAJAX(postURL,dataString,successFunctDefault);
}


$(function() {

    // Dialog ubah status web
    $( "#dialog-wstatus" ).dialog({
      autoOpen: false,
      resizable: false,
      height:185,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#stWebForm").serialize();
            var UrlPost = $("form#stWebForm").attr('action');

            function successFunct(data)
            {
				if(data === 'sukses') {
					$("#stsWebInput").val(ubah);
					$( "#dialog-wstatus" ).dialog( "close" );
				}
				else {
					$("#stweb"+stsWeb).attr("checked","checked");
					$( "#statusWeb" ).buttonset();
					$( "#dialog-wstatus" ).dialog( "close" );
					
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {

            $("#stweb"+stsWeb).attr("checked","checked");
            $( "#statusWeb" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // Dialog ubah status https
    $( "#dialog-wstatusscure" ).dialog({
      autoOpen: false,
      resizable: false,
      height:185,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#ScureWebForm").serialize();
            var UrlPost = $("form#ScureWebForm").attr('action');
            var scureWeb = $("#ScureWebInput").val();

			function successFunct(data)
            {
				if(data === 'sukses') {
					$("#ScureWebInput").val(ubah);
					$("#scureweb"+ubah).attr("checked","checked");
					$( "#dialog-wstatusscure" ).dialog( "close" );
				}
				else {
					$("#scureweb"+scureWeb).attr("checked","checked");
					$( "#ScureWeb" ).buttonset();
					$( "#dialog-wstatusscure" ).dialog( "close" );

					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {
            $("#scureweb"+scureWeb).attr("checked","checked");
            $( "#ScureWeb" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // Dialog ubah status cache
    $( "#dialog-wstatuscache" ).dialog({
      autoOpen: false,
      resizable: false,
      //height:185,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#CacheWebForm").serialize();
            var UrlPost = $("form#CacheWebForm").attr('action');
            var cacheWeb = $("#CacheWebInput").val();

            function successFunct(data)
            {
				if(data === 'sukses') {
					$("#CacheWebInput").val(ubah);
					$("#cacheweb"+ubah).attr("checked","checked");
					$( "#dialog-wstatuscache" ).dialog( "close" );
				}
				else {
					$("#cacheweb"+cacheWeb).attr("checked","checked");
					$( "#CacheWeb" ).buttonset();
					$( "#dialog-wstatuscache" ).dialog( "close" );

					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {
            $("#cacheweb"+cacheWeb).attr("checked","checked");
            $( "#CacheWeb" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // Dialog ubah status proteksi admin
    $( "#dialog-saweb" ).dialog({
      autoOpen: false,
      resizable: false,
      height:210,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#SuperAdminWebForm").serialize();
            var UrlPost = $("form#SuperAdminWebForm").attr('action');
            var saWeb = $("#SuperAdminWebInput").val();

            function successFunct(data)
            {
				if(data === 'sukses') {
					$("#SuperAdminWebInput").val(ubah);
					$("#saweb"+ubah).attr("checked","checked");
					$( "#dialog-saweb" ).dialog( "close" );
				}
				else {
					$("#saweb"+saWeb).attr("checked","checked");
					$( "#SuperAdminWeb" ).buttonset();
					$( "#dialog-saweb" ).dialog( "close" );

					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {
            $("#saweb"+saWeb).attr("checked","checked");
            $( "#SuperAdminWeb" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // Dialog ubah status iklan
    $( "#dialog-iklan" ).dialog({
      autoOpen: false,
      resizable: false,
      //height:180,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#IklanForm").serialize();
            var UrlPost = $("form#IklanForm").attr('action');
            var iklanWeb = $("#IklanWebInput").val();

            function successFunct(data)
            {
				if(data === 'sukses') {
					$("#IklanWebInput").val(ubah);
					$("#iklan"+ubah).attr("checked","checked");
					$( "#dialog-iklan" ).dialog( "close" );
				}
				else {
					$("#iklan"+iklanWeb).attr("checked","checked");
					$( "#IklanWeb" ).buttonset();
					$( "#dialog-iklan" ).dialog( "close" );

					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {
            $("#iklan"+iklanWeb).attr("checked","checked");
            $( "#IklanWeb" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // Dialog ubah status toko
    $( "#dialog-toko" ).dialog({
      autoOpen: false,
      resizable: false,
      //height:180,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#TokoForm").serialize();
            var UrlPost = $("form#TokoForm").attr('action');
            var stsToko = $("#stsTokoInput").val();

            function successFunct(data)
            {
				if(data === 'sukses') {
					$("#stsTokoInput").val(ubah);
					$("#toko"+ubah).attr("checked","checked");
					$( "#dialog-toko" ).dialog( "close" );
				}
				else {
					$("#toko"+stsToko).attr("checked","checked");
					$( "#stsToko" ).buttonset();
					$( "#dialog-toko" ).dialog( "close" );

					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {
            $("#toko"+stsToko).attr("checked","checked");
            $( "#stsToko" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // Dialog ubah cek stok
    $( "#dialog-cekstok" ).dialog({
      autoOpen: false,
      resizable: false,
      //height:180,
      modal: true,
      create: function(event, ui, data) {
					$(event.target).parent().css('position', 'fixed');
				},
      buttons: {
        "Ubah status": function(event, ui) {
			var ubah = $(this).data('post');
            var isi = $("form#cekStokForm").serialize();
            var UrlPost = $("form#cekStokForm").attr('action');
            var cekStok = $("#cekStokInput").val();

            function successFunct(data)
            {
				if(data === 'sukses') {
					$("#stsTokoInput").val(ubah);
					$("#cekstok"+ubah).attr("checked","checked");
					$( "#dialog-cekstok" ).dialog( "close" );
				}
				else {
					$("#cekstok"+cekStok).attr("checked","checked");
					$( "#cekStok" ).buttonset();
					$( "#dialog-cekstok" ).dialog( "close" );

					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
            }

            aksiFormAJAX(UrlPost,isi,successFunct);
		},
        "Batal": function() {
            $("#cekstok"+cekStok).attr("checked","checked");
            $( "#cekStok" ).buttonset();
            $( this ).dialog( "close" );
        }
      }
    });

    // tooltip
    $( ".blok" ).tooltip({
		track: true
    });
    
    // status-web
    $("#stweb1").button({
        icons: {
            primary: "ui-icon-locked"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-wstatus" ).dialog("option", "title", "Status Website Offine");
        $( "#dialog-wstatus" ).data('post',lnk).dialog("open");
	});
    $("#stweb2").button({
        icons: {
            primary: "ui-icon-wrench"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-wstatus" ).dialog("option", "title", "Status Website Perbaikan");
        $( "#dialog-wstatus" ).data('post',lnk).dialog("open");
	});
    $("#stweb3").button({
        icons: {
            primary: "ui-icon-signal-diag"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-wstatus" ).dialog("option", "title", "Status Website Online");
        $( "#dialog-wstatus" ).data('post',lnk).dialog("open");
	});

	// status-https
    $("#scureweb0").button({
        icons: {
            primary: "ui-icon-unlocked"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-wstatusscure" ).dialog("option", "title", "Status Website HTTP");
        $( "#dialog-wstatusscure" ).data('post',lnk).dialog("open");
	});
    $("#scureweb1").button({
        icons: {
            primary: "ui-icon-locked"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-wstatusscure" ).dialog("option", "title", "Status Website Redirect HTTPS");
        $( "#dialog-wstatusscure" ).data('post',lnk).dialog("open");
	});

	// web-cache
    $("#cacheweb0").button({
        icons: {
            primary: "ui-icon-heart"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-wstatuscache" ).dialog("option", "title", "Status Cache Off");
        $( "#dialog-wstatuscache" ).data('post',lnk).dialog("open");
	});
    $("#cacheweb1").button({
        icons: {
            primary: "ui-icon-star"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-wstatuscache" ).dialog("option", "title", "Status Cache On");
        $( "#dialog-wstatuscache" ).data('post',lnk).dialog("open");
	});

	// btn bersihkan cache
	$("#bersih-cache").button({
        icons: {
            primary: "ui-icon-trash"
        }
    }).click(function(e) {
		e.preventDefault();
		var lnk = $("#BersihCacheWebForm").attr('action');
		var isi = $("form#BersihCacheWebForm").serialize();

		function successFunct(data)
		{
			if(data === 'sukses') {
				$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
				$( "#gagal" ).data('INFO','File Cahe telah berhasil dibersihkan.').dialog( "open");
				$("#bersih-cache").button({ disabled: true });
				$("#chcmeter").text('0 KB');
			}
			else {
				$( "#gagal" ).dialog("option", "title", "Gagal Akses");
				$( "#gagal" ).data('INFO',data).dialog( "open");
			}
		}

		aksiFormAJAX(lnk,isi,successFunct);
	});

	// super-admin(sa)
    $("#saweb0").button({
        icons: {
            primary: "ui-icon-lightbulb"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-saweb" ).dialog("option", "title", "Status Proteksi Admin Off");
        $( "#dialog-saweb" ).data('post',lnk).dialog("open");
	});
    $("#saweb1").button({
        icons: {
            primary: "ui-icon-key"
        }
    }).click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-saweb" ).dialog("option", "title", "Status Proteksi Admin On");
        $( "#dialog-saweb" ).data('post',lnk).dialog("open");
	});

//global btn icon On Off
	$(".btnon").button({
        icons: {
            primary: "ui-icon-check"
        }
    });
    $(".btnoff").button({
        icons: {
            primary: "ui-icon-close"
        }
    });
    $(".popatur").button({
        icons: {
            primary: "ui-icon-gear"
        }
    });
//end global icon on off

	// iklan web
    $("#iklan0").click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-iklan" ).dialog("option", "title", "Status Iklan Off");
        $( "#dialog-iklan" ).data('post',lnk).dialog("open");
	});
    $("#iklan1").click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-iklan" ).dialog("option", "title", "Status Iklan On");
        $( "#dialog-iklan" ).data('post',lnk).dialog("open");
	});

	// kategori
    $("#kategori").button({
        icons: {
            primary: "ui-icon-pencil"
        }
    });
    // hbanner
    $("#hbanner").button({
        icons: {
            primary: "ui-icon-image"
        }
    });
    
    if (jQuery.isFunction(jQuery.fn.iframeDialog)) {
		
		function CloseFunction()
		{
			$('body').css({'overflow-y': 'auto'});
		}

		function OpenFunction()
		{
			$('body').css({'overflow-y': 'hidden'});
		}

		$('#kategori,#hbanner,.popatur').iframeDialog({
			id: 'aturKategoriDialog',
			url: $(this).attr("href"),
			scrolling: 'auto',
			draggable: false,
			modal: true,
			resizable: false,
			height: $(window).height(),
			//width: ($("#container").innerWidth() != null)?$("#container").innerWidth():900,
			width: $(window).innerWidth()+10,
			open : OpenFunction,
			close: CloseFunction
		});
	}
    
    // select tema
    $( "#selectable" ).selectable({ 
        tolerance: false,
        selecting: function( event, ui ) {
                if($(this).children().hasClass('ui-selected'))
                $(this).children().removeClass('ui-selected');
        },
        create: function( event, ui ) {
				// tema
				var idTema = $("#id-tema").val();
				$(this).find("li#"+idTema).addClass("ui-selected");
		}
    });
    
    $("#preview-tema").button({
        icons: {
            primary: "ui-icon-extlink"
        }
    });
    
    $("#atur-tema").button({
        icons: {
            primary: "ui-icon-gear"
        }
    });

    // toko online
    $("#toko0").click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-toko" ).dialog("option", "title", "Status Toko Off");
        $( "#dialog-toko" ).data('post',lnk).dialog("open");
	});
    $("#toko1").click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-toko" ).dialog("option", "title", "Status Toko On");
        $( "#dialog-toko" ).data('post',lnk).dialog("open");
	});

	// cek stok
    $("#cekstok0").click(function() {
		var lnk = $(this).attr('data-post');

        $( "#dialog-cekstok" ).dialog("option", "title", "Cek Stok Produk Off");
        $( "#dialog-cekstok" ).data('post',lnk).dialog("open");
	});
    $("#cekstok1").click(function() {
		var lnk = $(this).attr('data-post');
        
        $( "#dialog-cekstok" ).dialog("option", "title", "Cek Stok Produk On");
        $( "#dialog-cekstok" ).data('post',lnk).dialog("open");
	});
    
    // Tabs Pengaturan & Backup
    $( "#pengaturan-tab, #tabs-backup, #tabs-meta" ).tabs();
    
    $(".download").button({
        icons: {
            primary: "ui-icon-arrowthickstop-1-s"
        }
    });
    
    $(".backups").button({
        icons: {
            primary: "ui-icon-transferthick-e-w"
        }
    });
    
    $(".restore").button({
        icons: {
            primary: "ui-icon-arrowreturnthick-1-w"
        }
    });

    $(".cleanup").button({
        icons: {
            primary: "ui-icon-trash"
        }
    });
    
    // dissable all buttons on starup
    $( "#simpan-idweb,#simpan-robot,#simpan-sosmed,#simpan-metav,#simpan-seo" ).button({ disabled: true });
    //toko
    $( "#simpan-ppn,#simpan-satjen,#simpan-satprod,#simpan-cs" ).button({ disabled: true });

});

$(document).ready(function(){

    // Btn set
    $( "#statusWeb,#ScureWeb,#CacheWeb,#SuperAdminWeb,#IklanWeb,#stsToko,#cekStok" ).buttonset();

	// identitas-web
    $( "#w-keyword-id,#w-description-id,#w-keyword-en,#w-description-en" ).bind("keyup change", function(e){
        if(spm==true )
        $( "button#simpan-seo" ).button({ disabled: false });
    });
    $( "#w-namaweb,#w-slogan" ).bind("keyup change", function(e){
        if(spm==true )
        $( "button#simpan-idweb" ).button({ disabled: false });
    });

    // Dev-Key
    $( "#robot,#metav" ).bind("keyup change", function(e){
        if(spm==true )
        $(e.currentTarget).parent().find("button").button({ disabled: false });
    });

    //SosMed
    $( "input.nmsosmed,input.urlsosmed" ).bind("keyup change", function(e){
        if(spm==true )
        $("#simpan-sosmed").button({ disabled: false });
    });

    // Toko
	$( "#ppn,#satjen,#satprod,#namacs,#emailcs,#tlpcs,#smscs" ).bind("keyup change", function(e){
        if(spm==true )
        $(e.currentTarget).parent().find("button").button({ disabled: false });
        //alert(e.currentTarget === this);
    });

    // cek admin
    if(spm==false || spm.constructor!=Boolean){
        $( "#statusWeb,#ScureWeb,#CacheWeb,#saweb,#bersih-cache" ).buttonset({ disabled: true });
        $("input[name='autoBck']").button({ disabled: true });
        $( ".backups,.restore,#preview-tema,#atur-tema,#install-tema" ).button({ disabled: true });
        $("label").attr('title','Pengaturan ini di nonaktifkan');
        $('#temaWeb').attr('title','Pengaturan tema di nonaktifkan');
        $( "#selectable" ).selectable( "disable" );
        $('input,textarea').attr('disabled','disabled');
    }
    else{
		// identitas-web
        $( "#simpan-idweb" ).click(function(e){
           var isi = $("form#idwebForm").serialize();
           var UrlPost = $("form#idwebForm").attr('action');
           
           $("form#idwebForm").hide();
           $('#boxLoadIdWeb').html(htmlLoaderB);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#boxLoadIdWeb').empty();
				$("form#idwebForm").show();
			}
	
			aksiFormAJAX(UrlPost,isi,successFunct);
            
           $( this ).button({ disabled: true });
           return false;
        });

        // robot
        $("#simpan-robot").click(function(){
			var isi = $("form#robotForm").serialize();
			var UrlPost = $("form#robotForm").attr('action');

			//alert(UrlPost);
			$("form#robotForm").hide();
			$('#boxLoadRobot').html(htmlLoaderB);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}
				else {
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				
				$('#boxLoadRobot').empty();
				$("form#robotForm").show();
			}
	
			aksiFormAJAX(UrlPost,isi,successFunct);

           $( this ).button({ disabled: true });
           return false;
        });

		//SosMed
        $( "#simpan-sosmed" ).click(function(){
			var isi = $("form#sosmedForm").serialize();
			var UrlPost = $("form#sosmedForm").attr('action');

			$("#inputSosmed").hide();
			$('#boxLoadSosmed').html(htmlLoaderB);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else if(data === 'gagal'){
					info = 'Gagal merubah Google analytics code,<br> Anda tidak memiliki akses.!';
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',info).dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
					//$('#ga-key').val(ga);
				}
				$('#boxLoadSosmed').empty();
				$("#inputSosmed").show();
			}
	
			aksiFormAJAX(UrlPost,isi,successFunct);
            
           $( this ).button({ disabled: true });
           return false;
        });

        // htaccess
        $("#simpan-metav").click(function(){
           var isi = $("form#metavForm").serialize();
           var UrlPost = $("form#metavForm").attr('action');
           
           //alert(UrlPost);
           $("form#metavForm").hide();
           $('#boxLoadmetav').html(htmlLoaderB);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else{
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				
				$('#boxLoadmetav').empty();
				$("form#metavForm").attr("readonly","readonly");
				$("form#metavForm").show();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);

            $( this ).button({ disabled: true });
            return false;
        });

        // seo
        $( "#simpan-seo" ).click(function(){
			var isi = $("form#seoForm").serialize();
			var UrlPost = $("form#seoForm").attr('action');

			$("form#seoForm").hide();
			$('#boxLoadSeo').html(htmlLoaderB);

			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert('OK');
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#boxLoadSeo').empty();
				$("form#seoForm").show();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });

    //TOKO
		//PPN
        $( "#simpan-ppn" ).click(function(){
			var isi = $("form#ppnForm").serialize();
			var UrlPost = $("form#ppnForm").attr('action');

			$("form#ppnForm").hide();
			$('#boxLoadPPN').html(htmlLoaderK);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else if(data === 'gagal'){
					info = 'Gagal merubah Satuan Jenis Produk,<br> Anda tidak memiliki akses.!';
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',info).dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#ppn').val(ppn);
				}
				$('#boxLoadPPN').empty();
				$("form#ppnForm").show();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });
		//Satuan Jenis
        $( "#simpan-satjen" ).click(function(){
			var isi = $("form#satJenForm").serialize();
			var UrlPost = $("form#satJenForm").attr('action');

			$("form#satJenForm").hide();
			$('#boxLoadSatjen').html(htmlLoaderK);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else if(data === 'gagal'){
					info = 'Gagal merubah Satuan Jenis Produk,<br> Anda tidak memiliki akses.!';
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',info).dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#satjen').val(satjen);
				}
				$('#boxLoadSatjen').empty();
				$("form#satJenForm").show();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });
        //Satuan Produk
        $( "#simpan-satprod" ).click(function(){
			var isi = $("form#satProdForm").serialize();
			var UrlPost = $("form#satProdForm").attr('action');

			$("form#satProdForm").hide();
			$('#boxLoadSatprod').html(htmlLoaderK);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else if(data === 'gagal'){
					info = 'Gagal merubah Satuan Produk,<br> Anda tidak memiliki akses.!';
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',info).dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#satprod').val(satprod);
				}
				$('#boxLoadSatprod').empty();
				$("form#satProdForm").show();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });
        //CS
        $( "#simpan-cs" ).click(function(){
			var isi = $("form#csForm").serialize();
			var UrlPost = $("form#csForm").attr('action');

			$("form#csForm").hide();
			$('#boxLoadCS').html(htmlLoaderB);

			function successFunct(data)
			{
				if(data === 'sukses') {
					$( "#gagal" ).dialog("option", "title", "Aksi Sukses");
					$( "#gagal" ).data('INFO','Data berhasil diperbaharui.').dialog( "open");
				}else if(data === 'gagal'){
					info = 'Gagal merubah Detail CS,<br> Anda tidak memiliki akses.!';
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',info).dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
					$('#namacs').val(namacs);
					$('#emailcs').val(emailcs);
					$('#tlpcs').val(tlpcs);
					$('#smscs').val(smscs);
				}
				$('#boxLoadCS').empty();
				$("form#csForm").show();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });
    // END TOKO
        
    // TEMA
        // ubah tema
        $( "#selectable" ).on( "selectableselected", function( event, ui, target ) {
            var id = $(ui.selected).attr('id');
            $("#id-tema").val(id);
            var idtm = $("#id-ctema").val();
            $('#temaWeb').addClass('blurIt');
            $('#loadTema').show();
            var isi = $("form#temaForm").serialize();
            var UrlPost = $("form#temaForm").attr('action');

            function successFunct(data)
			{
				$('#loadTema').hide();
				$('#temaWeb').removeClass('blurIt');
				
				if(data === 'sukses') {
					//alert('OK');
					$(ui.selected).effect( "bounce", { times: 4 }, "fast" );
					$("#id-ctema").val(id);
				}else{
					$("#selectable").children().removeClass('ui-selected');
					$("#selectable").find('li#'+idtm).addClass('ui-selected');

					$("#preview-tema").attr("disabled","disabled");
					$("#atur-tema").attr("disabled","disabled");
					$("#install-tema").attr("disabled","disabled");
					$( "#toolbar-tema" ).effect("clip");
					$(ui.selected).parent().selectable( "disable" );
					
					$('#temaWeb').hover(function(){
						$(this).attr('title','Pengaturan tema di nonaktifkan');
					});
					$( "#gagal" ).dialog("option", "title", "Gagal Akses");
					$( "#gagal" ).data('INFO',(data !=='')?data:'Data ditolak oleh server.').dialog( "open");
				}
				cekOpsiTema();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
        });

        // atur opsi tema
        if (jQuery.isFunction(jQuery.fn.iframeDialog)) {

			function OpenFunction()
			{
				$('body').css({'overflow-y': 'hidden'});
			}
			
            $('#atur-tema').iframeDialog({
                id: 'aturAppDialog',
                url: aturOpsiTema,
                title : 'Atur opsi tema',
                scrolling: 'auto',
                draggable: false,
                modal: true,
                resizable: false,
                height: $(window).height(),
				//width: ($("#container").innerWidth() != null)?$("#container").innerWidth():900,
				width: $(window).innerWidth()+10,
				open : OpenFunction,
                buttons: {
                    'Tutup': function() {
                        $(this).remove();
                        $('body').css({'overflow-y': 'auto'});
                    }
                }
            });
        }

    //BACKUP
		//full
		$( "#btnbckfull" ).click(function(e){
			var isi = $("form#bckFullForm").serialize();
			var UrlPost = $("form#bckFullForm").attr('action');
			
			$('#proses_data').show();

			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert('OK');
					$( "#wrapbackup" ).load(upBckURL);
					$( "#info-sukses" ).dialog("option", "title", "Aksi Backup Selesai");
					$( "#info-sukses" ).data('INFO','Sukses membuat file backup.').dialog( "open");
					$( ".manbck" ).remove();
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#proses_data').hide();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });

        //web
		$( "#btnwebbck" ).click(function(e){
			var isi = $("form#webBckForm").serialize();
			var UrlPost = $("form#webBckForm").attr('action');
			
			$('#proses_data').show();

			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert('OK');
					$( "#wrapbackup" ).load(upBckURL);
					$( "#info-sukses" ).dialog("option", "title", "Aksi Backup Selesai");
					$( "#info-sukses" ).data('INFO','Sukses membuat file backup untuk web.').dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#proses_data').hide();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });

        //db
		$( "#btndbbck" ).click(function(e){
			var isi = $("form#dbBckForm").serialize();
			var UrlPost = $("form#dbBckForm").attr('action');
			
			$('#proses_data').show();

			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert('OK');
					$( "#wrapbackup" ).load(upBckURL);
					$( "#info-sukses" ).dialog("option", "title", "Aksi Backup Selesai");
					$( "#info-sukses" ).data('INFO','Sukses membuat file backup database.').dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#proses_data').hide();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });

        //media
		$( "#btnmediabck" ).click(function(e){
			var isi = $("form#mediaBckForm").serialize();
			var UrlPost = $("form#mediaBckForm").attr('action');
			
			$('#proses_data').show();

			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert('OK');
					$( "#wrapbackup" ).load(upBckURL);
					$( "#info-sukses" ).dialog("option", "title", "Aksi Backup Selesai");
					$( "#info-sukses" ).data('INFO','Sukses membuat file backup untuk media.').dialog( "open");
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#proses_data').hide();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            
            $( this ).button({ disabled: true });
            return false;
        });

    // RESTORE
		$( "#btnrestorebck" ).click(function(e){
			var isi = $("form#restoreBckForm").serialize();
			var UrlPost = $("form#restoreBckForm").attr('action');

			$('#proses_data').show();
			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert(data);
					window.location.assign('/'+admPath+'/pengaturan');
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#proses_data').hide();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            $( this ).button({ disabled: true });
            return false;
        });

    // CLEAN-UP
		$( "#btncleanup" ).click(function(e){
			var isi = $("form#cleanupForm").serialize();
			var UrlPost = $("form#cleanupForm").attr('action');

			$('#proses_data').show();
			function successFunct(data)
			{
				if(data === 'sukses') {
					//alert(data);
					window.location.assign('/'+admPath+'/pengaturan');
				}else{
					$( "#gagal" ).data('INFO',data).dialog( "open");
				}
				$('#proses_data').hide();
			}

			aksiFormAJAX(UrlPost,isi,successFunct);
            $( this ).button({ disabled: true });
            return false;
        });

    } //endif cek admin

});
</script>
<?php else: ?>
<!-- Minified script -->
<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 2J(){$.48(49,5(3){m(3==\'\')$("#1q-L").b({q:o});t $("#1q-L").b({q:l})})}5 2K(3){m(3===\'r\'){}t{$("#7").3(\'k\',3).2("a")}$(\'#2L\').1v(4a)}5 4b(2M){6 2d=2M[1];6 2N=2O+\'=\'+$("1w[2P="+2O+"]").x()+\'&2d=\'+2d;$(\'#2L\').Y(\'<2Q 2R="4c" 4d="4e-4f:\'+4g+\'4h;"><4i 4j="\'+4k+\'4l/E/4m-4n.4o" 2R="4p" /></2Q>\');z(4q,2N,2K)}$(5(){$("#2-14").2({1g:l,15:l,1h:2S,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#2T").B();6 i=$("9#2T").8(\'C\');5 f(3){m(3===\'r\'){$("#4r").x(J);$("#2-14").2("D")}t{$("#2U"+2V).8("n","n");$("#1P").M();$("#2-14").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#2U"+2V).8("n","n");$("#1P").M();$(c).2("D")}}});$("#2-1n").2({1g:l,15:l,1h:2S,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#2W").B();6 i=$("9#2W").8(\'C\');6 2e=$("#2X").x();5 f(3){m(3===\'r\'){$("#2X").x(J);$("#2f"+J).8("n","n");$("#2-1n").2("D")}t{$("#2f"+2e).8("n","n");$("#1Q").M();$("#2-1n").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#2f"+2e).8("n","n");$("#1Q").M();$(c).2("D")}}});$("#2-1o").2({1g:l,15:l,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#2Y").B();6 i=$("9#2Y").8(\'C\');6 2g=$("#2Z").x();5 f(3){m(3===\'r\'){$("#2Z").x(J);$("#2h"+J).8("n","n");$("#2-1o").2("D")}t{$("#2h"+2g).8("n","n");$("#1R").M();$("#2-1o").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#2h"+2g).8("n","n");$("#1R").M();$(c).2("D")}}});$("#2-X").2({1g:l,15:l,1h:4s,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#30").B();6 i=$("9#30").8(\'C\');6 2i=$("#31").x();5 f(3){m(3===\'r\'){$("#31").x(J);$("#X"+J).8("n","n");$("#2-X").2("D")}t{$("#X"+2i).8("n","n");$("#2j").M();$("#2-X").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#X"+2i).8("n","n");$("#2j").M();$(c).2("D")}}});$("#2-11").2({1g:l,15:l,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#32").B();6 i=$("9#32").8(\'C\');6 2k=$("#33").x();5 f(3){m(3===\'r\'){$("#33").x(J);$("#11"+J).8("n","n");$("#2-11").2("D")}t{$("#11"+2k).8("n","n");$("#2l").M();$("#2-11").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#11"+2k).8("n","n");$("#2l").M();$(c).2("D")}}});$("#2-12").2({1g:l,15:l,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#34").B();6 i=$("9#34").8(\'C\');6 1r=$("#2m").x();5 f(3){m(3===\'r\'){$("#2m").x(J);$("#12"+J).8("n","n");$("#2-12").2("D")}t{$("#12"+1r).8("n","n");$("#1r").M();$("#2-12").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#12"+1r).8("n","n");$("#1r").M();$(c).2("D")}}});$("#2-13").2({1g:l,15:l,16:o,1a:5(A,h,3){$(A.1b).Z().W(\'1i\',\'1j\')},1c:{"1k 1l":5(A,h){6 J=$(c).3(\'p\');6 d=$("9#35").B();6 i=$("9#35").8(\'C\');6 1s=$("#4t").x();5 f(3){m(3===\'r\'){$("#2m").x(J);$("#13"+J).8("n","n");$("#2-13").2("D")}t{$("#13"+1s).8("n","n");$("#1s").M();$("#2-13").2("D");$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(i,d,f)},"1m":5(){$("#13"+1s).8("n","n");$("#1s").M();$(c).2("D")}}});$(".4u").4v({4w:o});$("#4x").b({E:{G:"h-H-36"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-14").2("j","g","R 1x 4y");$("#2-14").3(\'p\',u).2("a")});$("#4z").b({E:{G:"h-H-4A"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-14").2("j","g","R 1x 4B");$("#2-14").3(\'p\',u).2("a")});$("#4C").b({E:{G:"h-H-4D-4E"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-14").2("j","g","R 1x 4F");$("#2-14").3(\'p\',u).2("a")});$("#4G").b({E:{G:"h-H-4H"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-1n").2("j","g","R 1x 4I");$("#2-1n").3(\'p\',u).2("a")});$("#4J").b({E:{G:"h-H-36"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-1n").2("j","g","R 1x 4K 4L");$("#2-1n").3(\'p\',u).2("a")});$("#4M").b({E:{G:"h-H-4N"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-1o").2("j","g","R 37 1y");$("#2-1o").3(\'p\',u).2("a")});$("#4O").b({E:{G:"h-H-4P"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-1o").2("j","g","R 37 1z");$("#2-1o").3(\'p\',u).2("a")});$("#2n-2o").b({E:{G:"h-H-38"}}).v(5(e){e.4Q();6 u=$("#39").8(\'C\');6 d=$("9#39").B();5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'4R 4S 4T 17 4U.\').2("a");$("#2n-2o").b({q:o});$("#4V").4W(\'0 4X\')}t{$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}}z(u,d,f)});$("#4Y").b({E:{G:"h-H-4Z"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-X").2("j","g","R 3a 3b 1y");$("#2-X").3(\'p\',u).2("a")});$("#50").b({E:{G:"h-H-51"}}).v(5(){6 u=$(c).8(\'3-p\');$("#2-X").2("j","g","R 3a 3b 1z");$("#2-X").3(\'p\',u).2("a")});$(".52").b({E:{G:"h-H-53"}});$(".54").b({E:{G:"h-H-D"}});$(".3c").b({E:{G:"h-H-3d"}});$("#55").v(5(){6 u=$(c).8(\'3-p\');$("#2-11").2("j","g","R 3e 1y");$("#2-11").3(\'p\',u).2("a")});$("#56").v(5(){6 u=$(c).8(\'3-p\');$("#2-11").2("j","g","R 3e 1z");$("#2-11").3(\'p\',u).2("a")});$("#3f").b({E:{G:"h-H-57"}});$("#3g").b({E:{G:"h-H-58"}});m(1S.3h(1S.3i.1T)){5 3j(){$(\'1U\').W({\'1V-y\':\'1W\'})}5 1X(){$(\'1U\').W({\'1V-y\':\'3k\'})}$(\'#3f,#3g,.3c\').1T({U:\'59\',3l:$(c).8("5a"),3m:\'1W\',3n:l,16:o,15:l,1h:$(1t).1h(),3o:$(1t).3p()+10,a:1X,D:3j})}$("#1d").1d({5b:l,5c:5(A,h){m($(c).2p().5d(\'h-1e\'))$(c).2p().2q(\'h-1e\')},1a:5(A,h){6 3q=$("#U-L").x();$(c).1Y("3r#"+3q).2r("h-1e")}});$("#2s-L").b({E:{G:"h-H-5e"}});$("#1q-L").b({E:{G:"h-H-3d"}});$("#5f").v(5(){6 u=$(c).8(\'3-p\');$("#2-12").2("j","g","R 3s 1y");$("#2-12").3(\'p\',u).2("a")});$("#5g").v(5(){6 u=$(c).8(\'3-p\');$("#2-12").2("j","g","R 3s 1z");$("#2-12").3(\'p\',u).2("a")});$("#5h").v(5(){6 u=$(c).8(\'3-p\');$("#2-13").2("j","g","3t 3u 1A 1y");$("#2-13").3(\'p\',u).2("a")});$("#5i").v(5(){6 u=$(c).8(\'3-p\');$("#2-13").2("j","g","3t 3u 1A 1z");$("#2-13").3(\'p\',u).2("a")});$("#2t-5j, #2u-1B, #2u-5k").2u();$(".5l").b({E:{G:"h-H-5m-1-s"}});$(".3v").b({E:{G:"h-H-5n-e-w"}});$(".3w").b({E:{G:"h-H-5o-1-w"}});$(".5p").b({E:{G:"h-H-38"}});$("#I-2v,#I-2w,#I-2x,#I-2y,#I-2z").b({q:o});$("#I-1C,#I-1D,#I-1E,#I-3x").b({q:o})});$(5q).5r(5(){$("#1P,#1Q,#1R,#2j,#2l,#1r,#1s").M();$("#w-3y-U,#w-3z-U,#w-3y-3A,#w-3z-3A").1F("1G 1H",5(e){m(1p==o)$("b#I-2z").b({q:l})});$("#w-5s,#w-5t").1F("1G 1H",5(e){m(1p==o)$("b#I-2v").b({q:l})});$("#2w,#2y").1F("1G 1H",5(e){m(1p==o)$(e.3B).Z().1Y("b").b({q:l})});$("1w.5u,1w.5v").1F("1G 1H",5(e){m(1p==o)$("#I-2x").b({q:l})});$("#1C,#1D,#1E,#2A,#2B,#2C,#2D").1F("1G 1H",5(e){m(1p==o)$(e.3B).Z().1Y("b").b({q:l})});m(1p==l||1p.5w!=5x){$("#1P,#1Q,#1R,#X,#2n-2o").M({q:o});$("1w[2P=\'5y\']").b({q:o});$(".3v,.3w,#2s-L,#1q-L,#3C-L").b({q:o});$("5z").8(\'g\',\'2E 5A 2F 2G\');$(\'#1Z\').8(\'g\',\'2E L 2F 2G\');$("#1d").1d("3D");$(\'1w,5B\').8(\'q\',\'q\')}t{$("#I-2v").v(5(e){6 d=$("9#20").B();6 i=$("9#20").8(\'C\');$("9#20").O();$(\'#3E\').Y(1u);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t{$("#7").3(\'k\',3).2("a")}$(\'#3E\').19();$("9#20").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-2w").v(5(){6 d=$("9#21").B();6 i=$("9#21").8(\'C\');$("9#21").O();$(\'#3F\').Y(1u);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t{$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}$(\'#3F\').19();$("9#21").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-2x").v(5(){6 d=$("9#3G").B();6 i=$("9#3G").8(\'C\');$("#3H").O();$(\'#3I\').Y(1u);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t m(3===\'7\'){K=\'F 1I 5C 5D 5E,<1J> 1K 1L 1M 1N.!\';$("#7").2("j","g","F N");$("#7").3(\'k\',K).2("a")}t{$("#7").3(\'k\',3).2("a")}$(\'#3I\').19();$("#3H").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-2y").v(5(){6 d=$("9#1O").B();6 i=$("9#1O").8(\'C\');$("9#1O").O();$(\'#3J\').Y(1u);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t{$("#7").2("j","g","F N");$("#7").3(\'k\',3).2("a")}$(\'#3J\').19();$("9#1O").8("3K","3K");$("9#1O").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-2z").v(5(){6 d=$("9#22").B();6 i=$("9#22").8(\'C\');$("9#22").O();$(\'#3L\').Y(1u);5 f(3){m(3===\'r\'){}t{$("#7").3(\'k\',3).2("a")}$(\'#3L\').19();$("9#22").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-1C").v(5(){6 d=$("9#23").B();6 i=$("9#23").8(\'C\');$("9#23").O();$(\'#3M\').Y(2H);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t m(3===\'7\'){K=\'F 1I 2I 3N 1A,<1J> 1K 1L 1M 1N.!\';$("#7").2("j","g","F N");$("#7").3(\'k\',K).2("a")}t{$("#7").3(\'k\',3).2("a");$(\'#1C\').x(1C)}$(\'#3M\').19();$("9#23").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-1D").v(5(){6 d=$("9#24").B();6 i=$("9#24").8(\'C\');$("9#24").O();$(\'#3O\').Y(2H);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t m(3===\'7\'){K=\'F 1I 2I 3N 1A,<1J> 1K 1L 1M 1N.!\';$("#7").2("j","g","F N");$("#7").3(\'k\',K).2("a")}t{$("#7").3(\'k\',3).2("a");$(\'#1D\').x(1D)}$(\'#3O\').19();$("9#24").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-1E").v(5(){6 d=$("9#25").B();6 i=$("9#25").8(\'C\');$("9#25").O();$(\'#3P\').Y(2H);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t m(3===\'7\'){K=\'F 1I 2I 1A,<1J> 1K 1L 1M 1N.!\';$("#7").2("j","g","F N");$("#7").3(\'k\',K).2("a")}t{$("#7").3(\'k\',3).2("a");$(\'#1E\').x(1E)}$(\'#3P\').19();$("9#25").P()}z(i,d,f);$(c).b({q:o});Q l});$("#I-3x").v(5(){6 d=$("9#26").B();6 i=$("9#26").8(\'C\');$("9#26").O();$(\'#3Q\').Y(1u);5 f(3){m(3===\'r\'){$("#7").2("j","g","S T");$("#7").3(\'k\',\'18 17 1f.\').2("a")}t m(3===\'7\'){K=\'F 1I 5F 5G,<1J> 1K 1L 1M 1N.!\';$("#7").2("j","g","F N");$("#7").3(\'k\',K).2("a")}t{$("#7").3(\'k\',3).2("a");$(\'#2A\').x(2A);$(\'#2B\').x(2B);$(\'#2C\').x(2C);$(\'#2D\').x(2D)}$(\'#3Q\').19();$("9#26").P()}z(i,d,f);$(c).b({q:o});Q l});$("#1d").5H("5I",5(A,h,1b){6 U=$(h.1e).8(\'U\');$("#U-L").x(U);6 3R=$("#U-3S").x();$(\'#1Z\').2r(\'3T\');$(\'#3U\').P();6 d=$("9#3V").B();6 i=$("9#3V").8(\'C\');5 f(3){$(\'#3U\').O();$(\'#1Z\').2q(\'3T\');m(3===\'r\'){$(h.1e).3W("5J",{5K:4},"5L");$("#U-3S").x(U)}t{$("#1d").2p().2q(\'h-1e\');$("#1d").1Y(\'3r#\'+3R).2r(\'h-1e\');$("#2s-L").8("q","q");$("#1q-L").8("q","q");$("#3C-L").8("q","q");$("#5M-L").3W("5N");$(h.1e).Z().1d("3D");$(\'#1Z\').5O(5(){$(c).8(\'g\',\'2E L 2F 2G\')});$("#7").2("j","g","F N");$("#7").3(\'k\',(3!==\'\')?3:\'18 5P 5Q 5R.\').2("a")}2J()}z(i,d,f)});m(1S.3h(1S.3i.1T)){5 1X(){$(\'1U\').W({\'1V-y\':\'3k\'})}$(\'#1q-L\').1T({U:\'5S\',3l:5T,g:\'5U 5V L\',3m:\'1W\',3n:l,16:o,15:l,1h:$(1t).1h(),3o:$(1t).3p()+10,a:1X,1c:{\'5W\':5(){$(c).3X();$(\'1U\').W({\'1V-y\':\'1W\'})}}})}$("#5X").v(5(e){6 d=$("9#3Y").B();6 i=$("9#3Y").8(\'C\');$(\'#V\').P();5 f(3){m(3===\'r\'){$("#27").1v(28);$("#K-r").2("j","g","S 29 2a");$("#K-r").3(\'k\',\'T 2b 2c 1B.\').2("a");$(".5Y").3X()}t{$("#7").3(\'k\',3).2("a")}$(\'#V\').O()}z(i,d,f);$(c).b({q:o});Q l});$("#5Z").v(5(e){6 d=$("9#3Z").B();6 i=$("9#3Z").8(\'C\');$(\'#V\').P();5 f(3){m(3===\'r\'){$("#27").1v(28);$("#K-r").2("j","g","S 29 2a");$("#K-r").3(\'k\',\'T 2b 2c 1B 40 60.\').2("a")}t{$("#7").3(\'k\',3).2("a")}$(\'#V\').O()}z(i,d,f);$(c).b({q:o});Q l});$("#61").v(5(e){6 d=$("9#41").B();6 i=$("9#41").8(\'C\');$(\'#V\').P();5 f(3){m(3===\'r\'){$("#27").1v(28);$("#K-r").2("j","g","S 29 2a");$("#K-r").3(\'k\',\'T 2b 2c 1B 62.\').2("a")}t{$("#7").3(\'k\',3).2("a")}$(\'#V\').O()}z(i,d,f);$(c).b({q:o});Q l});$("#63").v(5(e){6 d=$("9#42").B();6 i=$("9#42").8(\'C\');$(\'#V\').P();5 f(3){m(3===\'r\'){$("#27").1v(28);$("#K-r").2("j","g","S 29 2a");$("#K-r").3(\'k\',\'T 2b 2c 1B 40 64.\').2("a")}t{$("#7").3(\'k\',3).2("a")}$(\'#V\').O()}z(i,d,f);$(c).b({q:o});Q l});$("#65").v(5(e){6 d=$("9#43").B();6 i=$("9#43").8(\'C\');$(\'#V\').P();5 f(3){m(3===\'r\'){1t.44.45(\'/\'+46+\'/2t\')}t{$("#7").3(\'k\',3).2("a")}$(\'#V\').O()}z(i,d,f);$(c).b({q:o});Q l});$("#66").v(5(e){6 d=$("9#47").B();6 i=$("9#47").8(\'C\');$(\'#V\').P();5 f(3){m(3===\'r\'){1t.44.45(\'/\'+46+\'/2t\')}t{$("#7").3(\'k\',3).2("a")}$(\'#V\').O()}z(i,d,f);$(c).b({q:o});Q l})}});',62,379,'||dialog|data||function|var|gagal|attr|form|open|button|this|isi||successFunct|title|ui|UrlPost|option|INFO|false|if|checked|true|post|disabled|sukses||else|lnk|click||val||aksiFormAJAX|event|serialize|action|close|icons|Gagal|primary|icon|simpan|ubah|info|tema|buttonset|Akses|hide|show|return|Status|Aksi|Sukses|id|proses_data|css|saweb|html|parent||iklan|toko|cekstok|wstatus|resizable|modal|berhasil|Data|empty|create|target|buttons|selectable|selected|diperbaharui|autoOpen|height|position|fixed|Ubah|status|Batal|wstatusscure|wstatuscache|spm|atur|stsToko|cekStok|window|htmlLoaderB|load|input|Website|Off|On|Produk|backup|ppn|satjen|satprod|bind|keyup|change|merubah|br|Anda|tidak|memiliki|akses|metavForm|statusWeb|ScureWeb|CacheWeb|jQuery|iframeDialog|body|overflow|auto|OpenFunction|find|temaWeb|idwebForm|robotForm|seoForm|ppnForm|satJenForm|satProdForm|csForm|wrapbackup|upBckURL|Backup|Selesai|membuat|file|id_hps|scureWeb|scureweb|cacheWeb|cacheweb|saWeb|SuperAdminWeb|iklanWeb|IklanWeb|stsTokoInput|bersih|cache|children|removeClass|addClass|preview|pengaturan|tabs|idweb|robot|sosmed|metav|seo|namacs|emailcs|tlpcs|smscs|Pengaturan|di|nonaktifkan|htmlLoaderK|Satuan|cekOpsiTema|successFunctDefault|tabel|ID|dataString|tkn|name|div|align|185|stWebForm|stweb|stsWeb|ScureWebForm|ScureWebInput|CacheWebForm|CacheWebInput|SuperAdminWebForm|SuperAdminWebInput|IklanForm|IklanWebInput|TokoForm|cekStokForm|locked|Cache|trash|BersihCacheWebForm|Proteksi|Admin|popatur|gear|Iklan|kategori|hbanner|isFunction|fn|CloseFunction|hidden|url|scrolling|draggable|width|innerWidth|idTema|li|Toko|Cek|Stok|backups|restore|cs|keyword|description|en|currentTarget|install|disable|boxLoadIdWeb|boxLoadRobot|sosmedForm|inputSosmed|boxLoadSosmed|boxLoadmetav|readonly|boxLoadSeo|boxLoadPPN|Jenis|boxLoadSatjen|boxLoadSatprod|boxLoadCS|idtm|ctema|blurIt|loadTema|temaForm|effect|remove|bckFullForm|webBckForm|untuk|dbBckForm|mediaBckForm|restoreBckForm|location|assign|admPath|cleanupForm|get|opsiTema|upTblURL|hapusItem|center|style|margin|top|cth|px|img|src|baseURL|assets|bar|loader|gif|absmiddle|postURL|stsWebInput|210|cekStokInput|blok|tooltip|track|stweb1|Offine|stweb2|wrench|Perbaikan|stweb3|signal|diag|Online|scureweb0|unlocked|HTTP|scureweb1|Redirect|HTTPS|cacheweb0|heart|cacheweb1|star|preventDefault|File|Cahe|telah|dibersihkan|chcmeter|text|KB|saweb0|lightbulb|saweb1|key|btnon|check|btnoff|iklan0|iklan1|pencil|image|aturKategoriDialog|href|tolerance|selecting|hasClass|extlink|toko0|toko1|cekstok0|cekstok1|tab|meta|download|arrowthickstop|transferthick|arrowreturnthick|cleanup|document|ready|namaweb|slogan|nmsosmed|urlsosmed|constructor|Boolean|autoBck|label|ini|textarea|Google|analytics|code|Detail|CS|on|selectableselected|bounce|times|fast|toolbar|clip|hover|ditolak|oleh|server|aturAppDialog|aturOpsiTema|Atur|opsi|Tutup|btnbckfull|manbck|btnwebbck|web|btndbbck|database|btnmediabck|media|btnrestorebck|btncleanup'.split('|'),0,{}))
</script>
<?php endif; ?>

<?php endif; ?>

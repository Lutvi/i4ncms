<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*if($this->uri->segment(1) == 'adminweb')
{
    $this->load->view('dynamic_js/admin/ext_grid_user');
}*/

if($this->uri->segment(1) === $this->config->item('admpath'))
{
    // global admin script
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/admin_js');

    // MENU dan HALAMAN
    if($this->uri->segment(2) == 'atur_menu' || $this->uri->segment(2) == 'carimenu' || $this->uri->segment(2) == 'tblcarimenu')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/atur_menu_js');

    if($this->uri->segment(2) == 'halaman' || $this->uri->segment(2) == 'carihalaman' || $this->uri->segment(2) == 'tblcarihalaman')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/halaman_js');

    // BLOG
    if($this->uri->segment(2) == 'blog' || $this->uri->segment(2) == 'cariblog' || $this->uri->segment(2) == 'tblcariblog')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/blog_js');

	// ALBUM
	if($this->uri->segment(2) == 'album')
	{
	    if($this->uri->segment(3) == 'tabel_gallery' || $this->uri->segment(3) == 'update_tabel_gallery')
	    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/album_gallery_js');
	    else
	    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/album_js');
    }
    if($this->uri->segment(2) == 'carialbum' || $this->uri->segment(2) == 'tblcarialbum')
	{
		$this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/album_js');
	}
	if($this->uri->segment(2) == 'carigallery' || $this->uri->segment(2) == 'tblcarigallery')
	{
		$this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/album_gallery_js');
	}

	// VIDEO
	if($this->uri->segment(2) == 'video')
	{
	    if($this->uri->segment(3) == 'tabel_playlist' || $this->uri->segment(3) == 'update_tabel_playlist')
	    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/video_playlist_js');
	    else
	    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/video_js');
    }
    if($this->uri->segment(2) == 'carivideo' || $this->uri->segment(2) == 'tblcarivideo')
	{
		$this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/video_js');
	}
	if($this->uri->segment(2) == 'cariplaylist' || $this->uri->segment(2) == 'tblcariplaylist')
	{
		$this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/video_playlist_js');
	}

	// PRODUK
    if(($this->uri->segment(2) == 'produk' || $this->uri->segment(2) == 'cariproduk' || $this->uri->segment(2) == 'tblcariproduk') && $this->uri->segment(3) != 'detail_anak')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/produk_js');
    if($this->uri->segment(2) == 'produk' && $this->uri->segment(3) == 'detail_anak')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/produk_anak_js');

	// ORDER
	$order_path = array('cariorder','tblcariorder');
    if($this->uri->segment(2) == 'order' || in_array($this->uri->segment(2), $order_path))
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/order_js');
    // Pelanggan
    if($this->uri->segment(2) == 'pelanggan' || $this->uri->segment(2) == 'caripelanggan' || $this->uri->segment(2) == 'tblcaripelanggan')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/pelanggan_js');

    // Banner CMS
    if($this->uri->segment(2) == 'banner' || $this->uri->segment(2) == 'caribanner' || $this->uri->segment(2) == 'tblcaribanner')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/banner_js');

	// WIDGET dan MODULE
    if($this->uri->segment(2) == 'atur_widget' || $this->uri->segment(2) == 'cariwidget' || $this->uri->segment(2) == 'tblcariwidget')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/atur_widget_js');
    if($this->uri->segment(2) == 'atur_module' || $this->uri->segment(2) == 'carimodule' || $this->uri->segment(2) == 'tblcarimodule')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/atur_module_js');

    //[:: INFO ::] PENGATURAN CMS - JS BUTUH diload dr pagenya langsung

	// ADMIN CMS
    if($this->uri->segment(2) == 'admincms' || $this->uri->segment(2) == 'cariadmincms' || $this->uri->segment(2) == 'tblcariadmincms')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/admincms_js');

    // LOG
    if($this->uri->segment(2) == 'log_user' || $this->uri->segment(2) == 'carilog_user' || $this->uri->segment(2) == 'tblcarilog_user')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/log_user_js');



  //========= IFRAME ===============//

	// Banner Header CMS
    if($this->uri->segment(2) == 'hbanner' || $this->uri->segment(2) == 'carihbanner' || $this->uri->segment(2) == 'tblcarihbanner')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/hbanner_js');

    // PENGATURAN Kategori
    if($this->uri->segment(2) == 'atur_kategori' || $this->uri->segment(2) == 'carikategori' || $this->uri->segment(2) == 'tblcarikategori')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/atur_kategori_js');

    // metode_bayar
    if($this->uri->segment(2) == 'atur_metode_bayar' || $this->uri->segment(2) == 'cari_metodebayar' || $this->uri->segment(2) == 'tblcari_metodebayar')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/atur_metode_bayar_js');

    // KURIR
    if($this->uri->segment(2) == 'atur_kurir_pengiriman' || $this->uri->segment(2) == 'carikurir' || $this->uri->segment(2) == 'tblcarikurir')
    $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/kurir_pengiriman_js');

  //========= END IFRAME ===============//

}
else {
	if (file_exists(APPPATH.'views/dynamic_js/'. $this->config->item('theme_id') . '/front_js'.EXT))
    $this->load->view('dynamic_js/'. $this->config->item('theme_id') . '/front_js');
    else
    $this->load->view('dynamic_js/front_js');
}

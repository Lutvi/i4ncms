<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
echo meta($meta);
?>
<title><?php echo $title. ' | ' . $this->config->item('site_name'); ?></title>
<?php $this->load->view('global_assets/admin_all_assets'); ?>
<?php $this->load->view('dynamic_js'); ?>
</head>

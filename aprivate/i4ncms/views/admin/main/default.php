<?php echo $pageHeader; ?>

<body class="expressCMS">
<div id="container">
  <div id="header">
    <h1>RedDanio-CMS</h1>
    Admin Panel
  </div><!-- end #header -->
  
  <div id="topContent">
     <div id="topForm" style="font: 10px normal Helvetica, Arial, sans-serif;">
        <input type="hidden" value="<?php echo (isset($login)?'login':'') ?>" id="isLogin" name="isLogin"  />
		<?php $this->load->view($this->config->item('admin_theme_id').'/form/form_container'); ?>
      </div><!-- end #topForm -->
      
      <div id="rightTop">
          <div id="topMenu" style="">
             <?php $this->load->view($this->config->item('admin_theme_id').'/menu/adminTop_menu'); ?>
          </div><!-- end #topMenu -->
          <!--<div id="topBanner">
          	<?php $this->load->view($this->config->item('admin_theme_id').'/banner/top_banner',$banner); ?>
          </div> end #topMenu -->
      </div><!-- end #rightTop -->
  <div class="clearfloat"></div>    
  </div><!-- end #topContent -->
  
  <div id="sidebarWraper" style="">
      <?php $this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar01',$sideContent1); ?>
      <?php $this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar02'); ?>
      <?php $this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar03'); ?>
      <?php $this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar04'); ?>
  </div><!-- end #sidebarWraper -->
  
  <div id="mainContent">
    <h2><?php echo $page; ?></h2>
    <div id="accordion" style="font: 12px normal Helvetica, Arial, sans-serif;">
     <?php foreach ($artikel as $row): ?> 
        <h3><a href="#"><?php echo $row->judul_id; ?></a></h3>
        <div>
            <?php echo $row->isi_id; ?>
        </div>
     <?php endforeach; ?>  
    </div><!-- end #accordion -->
    <?php echo $adm_product; ?>
    <div id="divgrid"></div>
    <?php echo form_fckeditor($fck_conf, $isi); ?>
  </div><!-- end #mainContent -->
  
   <br class="clearfloat" /><!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats -->
   <div id="footer">
    <p>Page rendered in <strong>{elapsed_time}</strong> seconds</p>
  </div><!-- end #footer -->
</div><!-- end #container -->
</body>
</html>

<?php $this->load->view('admin/main/head'); ?>

<body class="i4nCMS" style="width:350px;margin:10% auto">
<div id="container" class="rounded">
  <div id="header">
    <h1><?php echo $this->config->item('site_name'); ?></h1>
    Login Admin
  </div><!-- end #header -->

	<div style="font: 12px normal Helvetica, Arial, sans-serif;margin:0 auto;width:300px">
		<div style="padding:5px 0;color:#A52A2A;text-align:center">
			<span id="infoLog">
				<?php
					if($sts === 'banned')
					echo 'Akun admin Anda sedang di nonaktifkan,<br>Mohon hubungi Web Master.';
					if($sts === 'ilegal')
					echo 'Mohon hubungi Web Master,<br>untuk mendapatkan akses.!';
				?>
			</span>
		</div>
		<?php echo form_open($this->config->item('admpath').'/login/status'); ?>
		<fieldset style="padding:8px; font-size:12px;" class="text ui-widget-content ui-corner-all">
	        <label for="uid" style="padding:5px 0;float:left">ID</label>
	        <input type="text" name="uid" id="uid" autocomplete="off" maxlength="50" style="width:200px;padding:5px;float:right" class="text ui-widget-content ui-corner-all"/>
	        <div style="clear:both;height:10px"></div>
	        <?php echo form_error('uid'); ?>
	        <label for="knci" style="padding:5px 0;float:left">Kunci</label>
	        <input type="password" name="knci" id="knci" maxlength="50" style="width:200px;padding:5px;float:right" class="text ui-widget-content ui-corner-all"/>
	        <div style="clear:both;height:15px"></div>
	        <?php echo form_error('knci'); ?>
	        <button type="reset" id="loginBatal-btn" style="float:left">Batal</button>
	        <button type="submit" id="login-btn" style="float:right">Masuk</button>
	    </fieldset>
		<?php echo form_close(); ?>
	</div>

   <div id="footer">
    <p>Butuh Login, untuk mengakses Admin.</p>
    <h1>i4n<span style="color:#000000;font-size:14px">CMS</span></h1>
  </div>
</div>

</body>
</html>

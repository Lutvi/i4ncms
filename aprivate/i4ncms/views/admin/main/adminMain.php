<?php $this->load->view('admin/main/head'); ?>

<body class="i4nCMS">
<div id="container" class="rounded">
  <div id="header">
    <h1><?php echo $this->config->item('site_name'); ?></h1>
    Admin Panel
  </div><!-- end #header -->

  <div id="topContent">
     <div id="topForm" style="font: 10px normal Helvetica, Arial, sans-serif;">
        <input type="hidden" value="<?php echo (isset($this->login)?'login':'') ?>" id="isLogin" name="isLogin"  />
        <?php $this->load->view($this->config->item('admin_theme_id').'/form/form_container'); ?>
      </div><!-- end #topForm -->
      
      <div id="rightTop">
          <div id="topMenu">
             <?php $this->load->view($this->config->item('admin_theme_id').'/menu/adminTop_menu'); ?>
          </div><!-- end #topMenu -->
          <!--<div id="topBanner">
            <?php $this->load->view($this->config->item('admin_theme_id').'/banner/top_banner'); ?>
          </div> end #topBanner -->
      </div>
  <div class="clearfloat"></div>    
  </div>

  <?php if ( ! isset($nosidebar) ): ?>
  <div id="sidebarWraper">
      <?php  $this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar01'); ?>
      <?php //$this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar02'); ?>
      <?php //$this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar03'); ?>
      <?php //$this->load->view($this->config->item('admin_theme_id').'/sidebar/sidebar04'); ?>
  </div>
  <?php endif; ?>
  
  <div id="mainContent">
    <?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box'); ?>
    <?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
    <?php $this->load->view($this->config->item('admin_theme_id').'/partial/'.$partial_content); ?>
  </div>
  
   <br class="clearfloat" />
   <div id="footer">
    <p>Diberdayakan oleh</p>
    <h1>i4n<span style="color:#000000;font-size:14px">CMS</span></h1>
    Halaman diproses dalam <strong>{elapsed_time}</strong> detik
  </div>
</div>


<div id="proses_data" class="ui-widget-overlay" style="display:none">
	<img src="<?php echo base_url().'assets/icons/loaders/loader12.gif';?>" align="center" style="position:absolute;left:45%;margin-top:27%" />
</div>
</body>
</html>

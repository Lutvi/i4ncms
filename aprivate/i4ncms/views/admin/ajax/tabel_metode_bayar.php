		<div id="metode-tab" style="background:transparent;font-size:12px;margin-top:20px">
			<ul>
				<li><a href="#tabs-transfer">Metode Transfer</a></li>
				<li><a href="#tabs-online">Metode Online</a></li>
				<li><a href="#tabs-giro">Metode Giro</a></li>
				<li><a href="#tabs-cod">Metode COD</a></li>
			</ul>
		
		<div id="tabs-transfer" style="font: 12px normal Helvetica, Arial, sans-serif">
		
			<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
			  <div class="pagination"><?php echo (!$page_transfer)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_transfer; ?></div>
		
				  <table id="rounded-corner" summary="Menu">
				    <thead>
				      <tr align="center">
				        <th class="rounded-company" scope="col" width="110">Nama Vendor</th>
				        <th scope="col" width="110">Label</th>
				        <th scope="col" width="90">Logo</th>
				        <th scope="col" width="200">Detail</th>
				        <th scope="col" width="60">Status</th>
				        <th class="rounded-q4" scope="col" width="80">Opsi</th>
				      </tr>
				    </thead>
				    
				    <tbody>
				      <?php if(count($bayar_transfer) > 0): ?>
				      <?php $i=0; ?>
				      <?php foreach ($bayar_transfer as $row): ?>
				      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
				        <td><?php echo $row->nama_vendor; ?></td>
				        <td>
							<?php echo $row->label_id; ?>
							<hr>
							<?php echo $row->label_en; ?>
						</td>
				        <td align="center">
							<img src="<?php echo base_url(); ?>_media/logo-bayar/thumb_<?php echo $row->logo; ?>" width="80" align="absmiddle" />
				        </td>
				        <td align="center"><?php echo word_limiter(jadi_br($row->detail),15); ?></td>
				        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
						  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
							<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
							<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
						  </select>
						</td>
				        <td class="opsi" align="center">
							<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_vendor; ?>" href="#"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur-anak'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_metode_bayar/detail_metode/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_vendor; ?>"></a>
				        </td>
				      </tr>
				      <?php $i++; ?>
				      <?php endforeach; ?>
				      <?php else: ?>
				      <tr>
				        <td colspan="6"><em>Data Kosong</em></td>
				      </tr>
				      <?php endif; ?>
				    </tbody>
				    <tfoot>
				      <tr>
				        <td colspan="5" class="rounded-foot-left"><em>List Metode Bayar Transfer yang terdaftar</em></td>
				        <td class="rounded-foot-right">&nbsp;</td>
				      </tr>
				    </tfoot>
				  </table>
			</div>
		</div>
		
		<div id="tabs-online" style="font: 12px normal Helvetica, Arial, sans-serif">
		
			<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
			  <div class="pagination"><?php echo (!$page_online)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_online; ?></div>
		
				  <table id="rounded-corner" summary="Menu">
				    <thead>
				      <tr align="center">
				        <th class="rounded-company" scope="col" width="110">Nama Vendor</th>
				        <th scope="col" width="110">Label</th>
				        <th scope="col" width="90">Logo</th>
				        <th scope="col" width="200">Detail</th>
				        <th scope="col" width="60">Status</th>
				        <th class="rounded-q4" scope="col" width="80">Opsi</th>
				      </tr>
				    </thead>
				    
				    <tbody>
				      <?php if(count($bayar_online) > 0): ?>
				      <?php $i=0; ?>
				      <?php foreach ($bayar_online as $row): ?>
				      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
				        <td><?php echo $row->nama_vendor; ?></td>
				        <td>
							<?php echo $row->label_id; ?>
							<hr>
							<?php echo $row->label_en; ?>
						</td>
				        <td align="center">
							<img src="<?php echo base_url(); ?>_media/logo-bayar/thumb_<?php echo $row->logo; ?>" width="80" align="absmiddle" />
				        </td>
				        <td align="center"><?php echo word_limiter(jadi_br($row->detail),15); ?></td>
				        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
						  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
							<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
							<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
						  </select>
						</td>
				        <td class="opsi" align="center">
							<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_vendor; ?>" href="#"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur-anak'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_metode_bayar/detail_metode/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_vendor; ?>"></a>
				        </td>
				      </tr>
				      <?php $i++; ?>
				      <?php endforeach; ?>
				      <?php else: ?>
				      <tr>
				        <td colspan="6"><em>Data Kosong</em></td>
				      </tr>
				      <?php endif; ?>
				    </tbody>
				    <tfoot>
				      <tr>
				        <td colspan="5" class="rounded-foot-left"><em>List Metode Bayar Online yang terdaftar</em></td>
				        <td class="rounded-foot-right">&nbsp;</td>
				      </tr>
				    </tfoot>
				  </table>
			</div>
		</div>
		
		<div id="tabs-giro" style="font: 12px normal Helvetica, Arial, sans-serif;">
		
			<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
			  <div class="pagination"><?php echo (!$page_giro)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_giro; ?></div>
		
				  <table id="rounded-corner" summary="Menu">
				    <thead>
				      <tr align="center">
				        <th class="rounded-company" scope="col" width="110">Nama Vendor</th>
				        <th scope="col" width="110">Label</th>
				        <th scope="col" width="90">Logo</th>
				        <th scope="col" width="200">Detail</th>
				        <th scope="col" width="60">Status</th>
				        <th class="rounded-q4" scope="col" width="80">Opsi</th>
				      </tr>
				    </thead>
				    
				    <tbody>
				      <?php if(count($bayar_giro) > 0): ?>
				      <?php $i=0; ?>
				      <?php foreach ($bayar_giro as $row): ?>
				      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
				        <td><?php echo $row->nama_vendor; ?></td>
				        <td>
							<?php echo $row->label_id; ?>
							<hr>
							<?php echo $row->label_en; ?>
						</td>
				        <td align="center">
							<img src="<?php echo base_url(); ?>_media/logo-bayar/thumb_<?php echo $row->logo; ?>" width="80" align="absmiddle" />
				        </td>
				        <td align="center"><?php echo word_limiter(jadi_br($row->detail),15); ?></td>
				        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
						  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
							<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
							<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
						  </select>
						</td>
				        <td class="opsi" align="center">
							<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_vendor; ?>" href="#"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur-anak'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_metode_bayar/detail_metode/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_vendor; ?>"></a>
				        </td>
				      </tr>
				      <?php $i++; ?>
				      <?php endforeach; ?>
				      <?php else: ?>
				      <tr>
				        <td colspan="6"><em>Data Kosong</em></td>
				      </tr>
				      <?php endif; ?>
				    </tbody>
				    <tfoot>
				      <tr>
				        <td colspan="5" class="rounded-foot-left"><em>List Metode Bayar Giro yang terdaftar</em></td>
				        <td class="rounded-foot-right">&nbsp;</td>
				      </tr>
				    </tfoot>
				  </table>
			</div>
		</div>
		
		
		<div id="tabs-cod" style="font: 12px normal Helvetica, Arial, sans-serif">
		
			<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
			  <div class="pagination"><?php echo (!$page_cod)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_cod; ?></div>
		
				  <table id="rounded-corner" summary="Menu">
				    <thead>
				      <tr align="center">
				        <th class="rounded-company" scope="col" width="110">Nama Vendor</th>
				        <th scope="col" width="110">Label</th>
				        <th scope="col" width="90">Logo</th>
				        <th scope="col" width="200">Detail</th>
				        <th scope="col" width="60">Status</th>
				        <th class="rounded-q4" scope="col" width="80">Opsi</th>
				      </tr>
				    </thead>
				    
				    <tbody>
				      <?php if(count($bayar_cod) > 0): ?>
				      <?php $i=0; ?>
				      <?php foreach ($bayar_cod as $row): ?>
				      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
				        <td><?php echo $row->nama_vendor; ?></td>
				        <td>
							<?php echo $row->label_id; ?>
							<hr>
							<?php echo $row->label_en; ?>
						</td>
				        <td align="center">
							<img src="<?php echo base_url(); ?>_media/logo-bayar/thumb_<?php echo $row->logo; ?>" width="80" align="absmiddle" />
				        </td>
				        <td align="center"><?php echo word_limiter(jadi_br($row->detail),15); ?></td>
				        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
						  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
							<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
							<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
						  </select>
						</td>
				        <td class="opsi" align="center">
							<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_vendor; ?>" href="#"></a>&nbsp;
				            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur-anak'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_metode_bayar/detail_metode/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_vendor; ?>"></a>
				        </td>
				      </tr>
				      <?php $i++; ?>
				      <?php endforeach; ?>
				      <?php else: ?>
				      <tr>
				        <td colspan="6"><em>Data Kosong</em></td>
				      </tr>
				      <?php endif; ?>
				    </tbody>
				    <tfoot>
				      <tr>
				        <td colspan="5" class="rounded-foot-left"><em>List Metode Bayar COD yang terdaftar</em></td>
				        <td class="rounded-foot-right">&nbsp;</td>
				      </tr>
				    </tfoot>
				  </table>
			</div>
		</div>
		
		</div><!-- End #metode-tab -->

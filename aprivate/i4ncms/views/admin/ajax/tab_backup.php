
		<div id="tabs-backup">
			<ul>
				<?php if( $jml_all_backup != 0): ?>
				<li><a href="#tabDownloadBackup">Download Backup</a></li>
				<?php endif; ?>
				<?php if($jml_backup == 0 || (! empty($sts_backup->aksi) && $sts_backup->aksi !== 'full')): ?>
				<li><a href="#tabBuatBackup">Buat Backup</a></li>
				<?php endif; ?>
				<?php if( $jml_all_backup != 0 && (empty($sts_backup->status) || $sts_backup->status !== 'restore')): ?>
				<li><a href="#tabRestore">Restore</a></li>
				<?php endif; ?>
				<li><a href="#tabCleanUp">Clean Up</a></li>
			</ul>

			<?php if( $jml_all_backup != 0): ?>
			<div id="tabDownloadBackup">
				<div class="blok">
					<h3>Download File Backup</h3>
					<br />
					<p>Pilih tanggal file backup yang akan didownload.</p>
					<p>File paket backup ini bisa digunakan untuk restore website secara manual.</p>
					<br />
					<p>
						<?php foreach($hist_backup as $row): ?>
						<a class="download" id="btnbckdownload" href="<?php echo base_url().$this->config->item('admpath').'/bckdata/download_backup/'.bs_kode($row->tgl); ?>"><?php echo date('d-m-Y', $row->ts); ?></a>
						<?php endforeach; ?>
					</p>
				</div>
			</div>
			<?php endif; ?>

			<?php if($jml_backup == 0 || (! empty($sts_backup->aksi) && $sts_backup->aksi !== 'full')): ?>
			<div id="tabBuatBackup">
				<div class="blok">
					<h3>Full Backup</h3>
					<br />
					<?php $attr = array('id'=>'bckFullForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/full_backup'),$attr);
					?>
					<p>Direkomendasikan<br />
						<button class="backups" id="btnbckfull" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Jalankan Full Backup</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="blok fltlft manbck">
					<h3>Backup Web</h3>
					<br />
					<?php $attr = array('id'=>'webBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/backup_web'),$attr);
					?>
					<p>
						<button class="backups" id="btnwebbck" <?php echo (! $this->super_admin || (! empty($sts_backup->aksi) && $sts_backup->aksi == 'web'))?'disabled="disabled"':''; ?>>Jalankan Backup Web</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="blok fltlft manbck">
					<h3>Backup DB</h3>
					<br />
					<?php $attr = array('id'=>'dbBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/backup_db'),$attr);
					?>
					<p>
						<button class="backups" id="btndbbck" <?php echo (! $this->super_admin || (! empty($sts_backup->aksi) && $sts_backup->aksi == 'db'))?'disabled="disabled"':''; ?>>Jalankan Backup DB</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="blok fltlft manbck">
					<h3>Backup Media</h3>
					<br />
					<?php $attr = array('id'=>'mediaBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/backup_media'),$attr);
					?>
					<p>
						<button class="backups" id="btnmediabck" <?php echo (! $this->super_admin || (! empty($sts_backup->aksi) && $sts_backup->aksi == 'media'))?'disabled="disabled"':''; ?>>Jalankan Backup Media</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="clearfloat"></div>
			</div>
			<?php endif; ?>

			<?php if( $jml_all_backup != 0 && (empty($sts_backup->status) || $sts_backup->status !== 'restore')): ?>
			<div id="tabRestore">
				<div class="blok">
					<h3>Restore</h3>
					<br />
					<?php $attr = array('id'=>'restoreBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/jalankan_restore'),$attr);
					?>
					<p>Ini adalah fitur untuk mengembalikan konfigurasi website.</p>
					<p class="info-txt">Jika file backup hari ini belum ada, maka sistem secara otomatis akan membuatnya sebelum proses restore dijalankan.</p>
					<p>Pilihlah salah satu dari tanggal yang ada.</p>
					<br />
					<p>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
						<select class="bigSel" name="tgl_backup" id="tgl_backup">
						<?php foreach($hist_backup as $row): ?>
						<option value="<?php echo $row->tgl; ?>|<?php echo date('d-m-Y', $row->ts); ?>"><?php echo date('d-m-Y', $row->ts); ?></option>
						<?php endforeach; ?>
						</select>
						<button class="restore" id="btnrestorebck" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Jalankan Restore</button>
					</p>
					</form>
				</div>
			</div>
			<?php endif; ?>

			<div id="tabCleanUp">
				<div class="blok">
					<h3>Claen Up</h3>
					<br />
					<p>Ini adalah fitur untuk membersihkan file temporary dan clean up sistem, untuk menghemat kouta disk server.</p>
					<p>Total File Temporary : <?php echo $file_tmp; ?></p>
					<br />
					<p>
						<button class="cleanup">Jalankan CleanUp</button>
					</p>
				</div>
			</div>
		</div><!-- end #tabs-backups -->

	<?php $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/pengaturan_js'); ?>

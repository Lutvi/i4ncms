<?php $attr = array('id'=>'temaForm');
    echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_tema'),$attr);
?>
    <input type="hidden" id="id-tema" name="id-tema" value="<?php echo $this->config->item('theme_id'); ?>" />
	<input type="hidden" id="id-ctema" name="id-ctema" value="<?php echo $this->config->item('theme_id'); ?>" />
<?php echo form_close(); ?>
<ol id="selectable">
    <?php
    $tema = $this->web->getAllTemaWeb();
    foreach($tema as $item):
    ?>
    <li class="ui-state-default" id="<?php echo $item['nama_tema']?>"><img src="/assets/scr-tema/<?php echo $item['gambar']; ?>" width="200" height="317" align="absmidle" /></li>
    <?php endforeach; ?>        
</ol>

<?php $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/pengaturan_js'); ?>

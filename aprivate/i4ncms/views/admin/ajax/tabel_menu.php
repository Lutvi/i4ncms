  <table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col" width="150">Nama menu ID</th>
        <th scope="col" width="150">Nama menu EN</th>
        <th scope="col" width="60">Posisi</th>
        <th scope="col">Halaman</th>
        <th scope="col" width="80">Status</th>
        <th scope="col" width="150">Parent</th>
        <th class="rounded-q4" scope="col" width="60">Opsi</th>
      </tr>
    </thead>

    <tbody>
      <?php if(count($menu) > 0): ?>
      <?php $i=0; $jml = $this->cms->getCountMenu(); ?>
      <?php foreach ($menu as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><span id="nmid_<?php echo $row->id; ?>" class="text"><?php echo $row->title_id; ?></span> 
        <input type="text" value="<?php echo $row->title_id; ?>" class="editbox2" id="nmid_input_<?php echo $row->id; ?>" />
        <input type="hidden" value="<?php echo $row->title_id; ?>" class="editbox2" id="nmoriid_input_<?php echo $row->id; ?>" />
        </td>
        <td><span id="nmen_<?php echo $row->id; ?>" class="text"><?php echo $row->title_en; ?></span> 
        <input type="text" value="<?php echo $row->title_en; ?>" class="editbox2" id="nmen_input_<?php echo $row->id; ?>" />
        <input type="hidden" value="<?php echo $row->title_en; ?>" class="editbox2" id="nmorien_input_<?php echo $row->id; ?>" />
        </td>
        <td align="center">
        <?php if($row->posisi == 1): ?>
				<a href="#" title="Down <?php echo $row->posisi + 1; ?>" class="rdown" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	elseif($row->posisi == $jml): ?>
				<a href="#" title="Up <?php echo $row->posisi - 1; ?>" class="rup" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	else: ?>
				<a href="#" title="Down <?php echo $row->posisi + 1; ?>" class="rdown" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>&nbsp;<a href="#" title="Up <?php echo $row->posisi - 1; ?>" class="rup" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>        
        <?php endif; ?>
        </td>
        <td align="center">
        <?php echo ($this->cms->getLabelHalamanById($row->link) != NULL)?$this->cms->getLabelHalamanById($row->link):' - '; ?>
        <input type="hidden" value="<?php echo $row->link; ?>" name="halaman_select_<?php echo $row->id; ?>" id="halaman_select_<?php echo $row->id; ?>" />
        </td>
        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo ($row->status == 1)?'On':'Off'; ?></span>
        <select id="status_input_<?php echo $row->id; ?>" class="editbox">
            <option value="1" <?php echo ($row->status == 1)?'selected="selected"':''; ?>>On</option>
            <option value="0" <?php echo ($row->status == 0)?'selected="selected"':''; ?>>Off</option>
        </select>
        </td>
        <td><span id="parent_<?php echo $row->id; ?>" class="text"><?php echo ($this->cms->getParentName($row->parentid) == NULL)?'-- Tidak ada --':$this->cms->getParentName($row->parentid); ?></span>
        <select id="parent_select_<?php echo $row->id; ?>" name="parent_select_<?php echo $row->id; ?>" class="selectbox">
        <option value="0">-- Tidak ada --</option>
        <?php 
        $parent = $row->parentid;
        $id = $row->id;
        foreach ($nama_menu as $row2):
          if($id != $row2->id):
        ?>
        <option value="<?php echo $row2->id; ?>" <?php echo ($parent == $row2->id)?'selected="selected"':''; ?>>
        <?php
		$tab = "";
		if($row2->level > 0)
		{
			for ($x=1; $x<=$row2->level; $x++) {
				$tab .= "=";
			}
			$tab .=  "> ";
		}
		echo $tab.$row2->title;
		?>
        </option>
        <?php 
          endif;
        endforeach; ?>
        </select>
        </td>
        <td align="center">
            <a class="<?php echo ($super_admin==FALSE || $row->id==1)?'dis_edit':'edit'; ?>" id="<?php echo ($super_admin==FALSE || $row->id==1)?'xx':$row->id; ?>" href="javascript:void(0);" title="Ubah menu"></a>&nbsp;&nbsp;
            <a class="<?php echo ($super_admin==FALSE || $row->id==1)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo ($super_admin==FALSE || $row->id==1)?'xx':$row->id; ?>" href="javascript:void(0);" title="Hapus Menu <?php echo humanize($row->title_id); ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="6"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    
    <tfoot>
      <tr>
        <td colspan="6" class="rounded-foot-left"><em>List menu yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

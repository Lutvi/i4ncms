  <table id="rounded-corner" summary="2007 Major IT Companies' Profit">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col">Nama Widget</th>
        <th scope="col" width="150">Judul Widget ID</th>
        <th scope="col" width="150">Judul Widget EN</th>
        <th scope="col" width="50">Posisi</th>
        <th scope="col" width="60">Status</th>
        <th scope="col">Deskripsi</th>
        <th class="rounded-q4" scope="col" width="90">Opsi Widget</th>
      </tr>
    </thead>
    <tbody>
      <?php if(count($widget) > 0): ?>
      <?php $i=0; $jml = $this->cms->getCountWidget(); ?>
      <?php foreach ($widget as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><?php echo $row->nama_widget; ?> </td>
        <td><span id="judul_<?php echo $row->id_widget; ?>" class="text"><?php echo $row->judul_widget; ?> </span>
        <input type="text" maxlength="150" value="<?php echo $row->judul_widget; ?>" class="editbox" style="width:130px; text-align:left;" id="judul_input_<?php echo $row->id_widget; ?>" />
        </td>
        <td><span id="judulen_<?php echo $row->id_widget; ?>" class="text"><?php echo $row->judul_widget_en; ?> </span>
        <input type="text" maxlength="150" value="<?php echo $row->judul_widget_en; ?>" class="editbox" style="width:130px; text-align:left;" id="judulen_input_<?php echo $row->id_widget; ?>" />
        </td>
        <td align="center">
		<?php 
			$pos = $row->posisi;
				if($pos == 1): 
		?>
				<a href="#" title="Down" class="rdown" id="md_<?php echo $row->id_widget; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	elseif($pos == $jml): ?>
				<a href="#" title="Up" class="rup" id="mu_<?php echo $row->id_widget; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	else: ?>
				<a href="#" title="Down" class="rdown" id="md_<?php echo $row->id_widget; ?>_<?php echo $row->posisi; ?>"></a>&nbsp;<a href="#" title="Up" class="rup" id="mu_<?php echo $row->id_widget; ?>_<?php echo $row->posisi; ?>"></a>        
        <?php endif; ?>      
        </td>
        <td align="center"><span id="status_<?php echo $row->id_widget; ?>" class="text"><?php echo ($row->status == 1)?'On':'Off'; ?></span>
        <select id="status_input_<?php echo $row->id_widget; ?>" class="editbox">
	        <option value="1" <?php echo ($row->status == 1)?'selected="selected"':''; ?>>On</option>
	        <option value="0" <?php echo ($row->status == 0)?'selected="selected"':''; ?>>Off</option>
        </select>
        </td>
        <td><?php echo word_limiter($row->deskripsi,5); ?> </td>
        <td align="center">
        	<a class="edit" id="<?php echo ( ! $this->super_admin)?'xx':$row->id_widget; ?>" href="javascript:void(0)" title="Ubah status"></a>&nbsp;
            <a class="<?php echo ( ! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo ( ! $this->super_admin)?'xx':$row->id_widget; ?>" href="javascript:void(0)" title="Hapus <?php echo ( ! $this->super_admin)?'xx':humanize($row->nama_widget); ?>"></a>&nbsp;
            <a class="<?php echo ($row->path_admin_widget=='' || $super_admin==FALSE)?'dis_atur':'atur'; ?>" id="<?php echo 'atur_'.( ! $this->super_admin)?'xx':$row->id_widget; ?>" href="<?php echo ($row->path_admin_widget=='' || $super_admin==FALSE)?'javascript:void(0)':base_url().config_item('admpath').'/atur_widget/admin_widget'.$row->path_admin_widget; ?>" title="Atur <?php echo ( ! $this->super_admin)?'xx':humanize($row->nama_widget); ?>"></a>
      </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="7"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="6" class="rounded-foot-left"><em>List widget yang terpasang</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

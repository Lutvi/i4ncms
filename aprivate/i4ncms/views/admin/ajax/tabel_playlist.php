	  <table id="rounded-corner" summary="Menu">
		<thead>
		  <tr align="center">
			<th class="rounded-company" scope="col" width="80">Preview</th>
			<th scope="col">Nama ID</th>
			<th scope="col">Nama EN</th>
			<th scope="col" width="50">Posisi</th>
			<th scope="col">Video</th>
			<th scope="col" width="50">Status</th>
			<th class="rounded-q4" scope="col" width="110">Opsi</th>
		  </tr>
		</thead>
		<tbody>
		  <?php if(count($video_web) > 0): ?>
		  <?php $i=0; $jml = $this->cms->getCountVideoPlaylist($id_video); ?>
		  <?php foreach ($video_web as $row): ?>
		  <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td align="center">
			<a href="<?php echo $row->src_video; ?>" target="_blank" title="Preview Video">
			<img src="<?php echo base_url(); ?>_media/videos/thumb/thumb_<?php echo $row->gambar_video; ?>" align="absmiddle" width="60" height="45" />
			</a>
			</td>
			<td><?php echo $row->nama_id; ?></td>
			<td><?php echo $row->nama_en; ?></td>
			<td align="center">
			<?php 
				$pos = $row->posisi;
					if($pos == 1): 
			?>
					<a href="#" title="Down" class="rdown" data-parentvideo="<?php echo $row->id_video; ?>" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
			<?php	elseif($pos >= $jml): ?>
					<a href="#" title="Up" class="rup" data-parentvideo="<?php echo $row->id_video; ?>" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
			<?php	else: ?>
					<a href="#" title="Down" class="rdown" data-parentvideo="<?php echo $row->id_video; ?>" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>&nbsp;<a href="#" title="Up" class="rup" data-parentvideo="<?php echo $row->id_video; ?>" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>        
			<?php endif; ?>       
			</td>
			<td>
			<?php echo $nama_video->nama_id; ?>
			<div class="btsJudul"></div>
			<?php echo $nama_video->nama_en; ?>
			</td>
			
			<td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
			  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
				<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
				<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
			  </select>
			</td>
			<td align="center">
				<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
				<a class="<?php echo ( ! $super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" href="#" title="Hapus <?php echo humanize($row->nama_id); ?>"></a>&nbsp;
				<a class="atur-anak" href="<?php echo site_url($this->config->item('admpath').'/video/detail_playlist/'.$row->id.'/show'); ?>" title="Update <?php echo ucwords($row->nama_id); ?>"></a>
			</td>
		  </tr>
		  <?php $i++; ?>
		  <?php endforeach; ?>
		  <?php else: ?>
		  <tr>
			<td colspan="7"><em>Data Kosong</em></td>
		  </tr>
		  <?php endif; ?>
		</tbody>
		<tfoot>
		  <tr>
			<td colspan="6" class="rounded-foot-left"><em>List playlist yang terdaftar</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		  </tr>
		</tfoot>
	  </table>

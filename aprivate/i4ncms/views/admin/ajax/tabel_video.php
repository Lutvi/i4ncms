<table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col">Nama Video</th>
        <th scope="col" width="50">Posisi</th>
        <th scope="col" width="60">Hits</th>
        <th scope="col">Kategori</th>
        <th scope="col" width="80">Preview</th>
        <th scope="col" width="60">Utama</th>
        <th scope="col" width="60">Komentar</th>
        <th scope="col" width="60">Status</th>
        <th class="rounded-q4" scope="col" width="110">Opsi</th>
      </tr>
    </thead>

    <tbody>
      <?php if(count($video_web) > 0): ?>
      <?php $i=0; $jml = $this->cms->getCountVideo(); ?>
      <?php foreach ($video_web as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td>
        <?php echo $row->nama_id; ?>
        <div class="btsJudul"></div>
        <?php echo $row->nama_en; ?>
        </td>
        <td align="center">
		<?php 
		$pos = $row->posisi;
		if($pos == 1): 
		?>
				<a href="#" title="Down" class="rdown" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	elseif($pos == $jml): ?>
				<a href="#" title="Up" class="rup" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	else: ?>
				<a href="#" title="Down" class="rdown" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>&nbsp;<a href="#" title="Up" class="rup" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>        
        <?php endif; ?>       
        </td>
        <td align="right"><?php echo format_harga_indo($row->hits); ?></td>
        <td><?php echo $this->cms->getNamaKategoriById($row->kategori_id); ?></td>
        <td align="center">
        <a href="<?php echo $row->src_video; ?>" target="_blank" title="Preview Video">
        <img src="<?php echo base_url(); ?>_media/videos/thumb/thumb_<?php echo $row->gambar_video; ?>" align="absmiddle" width="60" height="45" />
		</a>
        </td>
        <td align="center"><span id="utama_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->utama); ?></span>
          <select id="utama_input_<?php echo $row->id; ?>" class="editbox">
            <option value="on" <?php echo ($row->utama == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->utama == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        <td align="center"><span id="komentar_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->diskusi); ?></span>
          <select id="komentar_input_<?php echo $row->id; ?>" class="editbox">
            <option value="on" <?php echo ($row->diskusi == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->diskusi == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
          <select id="status_input_<?php echo $row->id; ?>" class="editbox">
            <option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        
        <td align="center">
			<a class="<?php echo ( ! $super_admin)?'dis_edit':'edit'; ?>" id="<?php echo ( ! $super_admin)?'xx':$row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
			<a class="<?php echo ( ! $super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo ( ! $super_admin)?'xx':$row->id; ?>" href="#" title="Hapus <?php echo ( ! $super_admin)?'xx':humanize($row->nama_id); ?>"></a>&nbsp;
			<a class="<?php echo ( ! $super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/video/detail/'.$row->id.'/show'); ?>" title="Update <?php echo ( ! $super_admin)?'xx':ucwords($row->nama_id); ?>"></a>
			<?php if($this->cms->getCountVideoPlaylist($row->id) > 0 ): ?>
				<a class="anak <?php echo ( ! $super_admin)?'dis_atur-anak':'atur-anak'; ?>" href="<?php echo ( ! $super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/video/tabel_playlist/'.$row->id); ?>" title="Update Playlist <?php echo ( ! $super_admin)?'xx':humanize($row->nama_id); ?>"></a>
			<?php endif; ?>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="9"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="8" class="rounded-foot-left"><em>List video yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

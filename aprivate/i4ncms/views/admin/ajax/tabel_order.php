<div id="tabs" style="width:125%;background:transparent;font-size:12px;">
	<ul>
		<li><a href="#tabs-proses-bayar">Proses Pembayaran</a></li>
		<li><a href="#tabs-proses-pending">Proses Pending</a></li>
		<li><a href="#tabs-proses-paket">Proses Pemaketan</a></li>
		<li><a href="#tabs-proses-kirim">Proses Pengiriman</a></li>
		<li><a href="#tabs-proses-batal">Proses Batal</a></li>
		<li><a href="#">&nbsp;&nbsp;&nbsp;</a></li>
		<li><a href="#tabs-batal">Order Batal</a></li>
		<li><a href="#tabs-sukses">Order Sukses</a></li>
	</ul>

	<div id="tabs-proses-bayar" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_proses_bayar)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_proses_bayar; ?></div>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		  <tr>
			<th class="rounded-company" scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Harga Bayar</th>
			<th scope="col" width="40" style="font-size:11px">Status Transaksi</th>
			<th class="rounded-q4" scope="col" width="60">Opsi</th>
		</tr>
		</thead>
		<tbody>
		<?php
			if(count($proses_bayar) > 0):
			$i=0;
			foreach ($proses_bayar as $row):
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td title="Tgl Transaksi" align="center"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_transaksi); ?></td>
			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_hapus dis_batal':'hapus batal'; ?>" id="batal_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Batalkan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" href="javascript:void(0);"></a>&nbsp;
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0);':site_url($this->config->item('admpath').'/order/detail/'.$row->no_struk.'/show'); ?>" title="Update <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
				<a class="<?php echo ( ! $this->super_admin)?'dis_lanjut':'lanjut'; ?>" href="javascript:void(0);" data-info="Teruskan <strong><?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?></strong> ke proses Pending" id="pending_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Teruskan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?> ke proses Pending"></a>
			</td>

		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="10"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="9" class="rounded-foot-left"><em>List Pesanan yang masih dalam tahap pembayaran</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
		</tfoot>
	</table>
	</div>

	<div id="tabs-proses-pending" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_proses_pending)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_proses_pending; ?></div>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		<tr>
			<th class="rounded-company" scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="80" style="font-size:11px">No Resi</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Harga Bayar</th>
			<th scope="col" width="40" style="font-size:11px">Status Transaksi</th>
			<th class="rounded-q4" scope="col" width="50">Opsi</th>
		  </tr>
		</thead>
		<tbody>
		<?php 
			if(count($proses_pending) > 0):
			$i=0;
			foreach ($proses_pending as $row): 
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td align="left"><?php echo $row->no_resi_pengiriman; ?></td>
			<td title="Tgl Transaksi" align="center"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_transaksi); ?></td>
			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_hapus dis_batal':'hapus batal'; ?>" id="batal_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Batalkan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" href="javascript:void(0)"></a>&nbsp;
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/order/detail/'.$row->no_struk.'/show'); ?>" title="Update <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="11"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="10" class="rounded-foot-left"><em>List Pesanan yang dipending</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
		</tfoot>
	</table>
	</div>

	<div id="tabs-proses-paket" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_proses_paket)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_proses_paket; ?></div>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		<tr>
			<th class="rounded-company" scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="80" style="font-size:11px">No Resi</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Harga Bayar</th>
			<th scope="col" width="40" style="font-size:11px">Status Transaksi</th>
			<th class="rounded-q4" scope="col" width="60">Opsi</th>
		</tr>
		</thead>
		<tbody>
		<?php
			if(count($proses_paket) > 0):
			$i=0;
			foreach ($proses_paket as $row): 
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td align="left"><?php echo $row->no_resi_pengiriman; ?></td>
			<td title="Tgl Transaksi" align="center"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_transaksi); ?></td>
			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_hapus dis_batal':'hapus batal'; ?>" id="batal_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Batalkan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" href="javascript:void(0)"></a>&nbsp;
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/order/detail/'.$row->no_struk.'/show'); ?>" title="Update <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
				<a class="lanjut" href="javascript:void(0);" data-info="Teruskan <strong><?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?></strong> ke proses Pengiriman" id="pengiriman_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Teruskan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?> ke proses Pengiriman"></a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="11"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="10" class="rounded-foot-left"><em>List Pesanan yang dipaketkan</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
		</tfoot>
	</table>
	</div>

	<div id="tabs-proses-kirim" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_proses_kirim)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_proses_kirim; ?></div>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		<tr>
			<th class="rounded-company" scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="80" style="font-size:11px">No Resi</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Harga Bayar</th>
			<th scope="col" width="40" style="font-size:11px">Status Transaksi</th>
			<th class="rounded-q4" scope="col" width="60">Opsi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			if(count($proses_kirim) > 0):
			$i=0;
			
			foreach ($proses_kirim as $row): 
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td align="left"><?php echo $row->no_resi_pengiriman; ?></td>
			<td title="Tgl Transaksi" align="center"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_transaksi); ?></td>

			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_hapus dis_batal':'hapus batal'; ?>" id="batal_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Batalkan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" href="javascript:void(0)"></a>&nbsp;
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/order/detail/'.$row->no_struk.'/show'); ?>" title="Update <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
				<a class="lanjut" href="javascript:void(0);" data-info="Teruskan <strong><?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?></strong> ke Transaksi Sukses" id="trxsukses_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" title="Teruskan <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?> ke Transaksi Sukses"></a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="11"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="10" class="rounded-foot-left"><em>List Pesanan yang dikirim</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		  </tr>
		</tfoot>
	</table>
	</div>
	
	<div id="tabs-proses-batal" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_proses_batal)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_proses_batal; ?></div>
		<?php if(count($proses_batal) > 0): ?>
		<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: -10px; padding:8px;">
		<p style="color:#FF001A;font-weight:normal"><span class="ui-icon ui-icon-info" style="float: left; margin-right:7px; margin-top:-1px"></span>
		Jika Data <u>Proses BATAL dihapus</u> maka jumlah pesanan akan dikembalikan ke stok produk, <u>jika tidak dihapus</u> maka stok produk masih tertahan disini.</p>
		</div></div>
		<?php endif; ?>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		<tr>
			<th class="rounded-company" scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Harga Bayar</th>
			<th scope="col" width="40" style="font-size:11px">Status Transaksi</th>
			<th scope="col" width="40" style="font-size:11px">Status Order</th>
			<th class="rounded-q4" scope="col" width="50">Opsi</th>
		</tr>
		</thead>
		<tbody>
		<?php
			if(count($proses_batal) > 0):
			$i=0;
			foreach ($proses_batal as $row): 
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td title="Tgl Transaksi" align="center"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_transaksi); ?></td>
			<td align="center"><?php echo humanize($row->status_orderan); ?></td>
			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_hapus':'hapus'; ?> buang" id="hapus_<?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" data-cid="<?php echo ( ! $this->super_admin)?'xx':($row->id_cust); ?>" data-jml="<?php echo ( ! $this->super_admin)?'xx':$total->jml_pesan ?>" data-total="<?php echo ( ! $this->super_admin)?'xx':$total->total_harga; ?>" title="Hapus Data <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>" href="javascript:void(0)"></a>&nbsp;
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/order/detail/'.$row->no_struk.'/show'); ?>" title="Update <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="11"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="10" class="rounded-foot-left"><em>List Pesanan yang dibatalkan</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
		</tfoot>
	</table>
	</div>
	<!-- End Proses Transaksi -->
	
	<!-- Status Order -->
	<div id="tabs-batal" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_batal)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_batal; ?></div>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		<tr>
			<th class="rounded-company" scope="col" width="20" style="font-size:11px">Id</th>
			<th scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Batal</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Transaksi</th>
			<th scope="col" width="40" style="font-size:11px">Status Order</th>
			<th class="rounded-q4" scope="col" width="50">Opsi</th>
		</tr>
		</thead>
		<tbody>
		<?php
			if(count($order_batal) > 0):
			$i=0;
			foreach ($order_batal as $row): 
			//$total = $this->orderan->getTotalOrderanBatalByTrx($row->no_struk);
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanBatalByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="Id Pembeli"><?php echo ($row->id_cust); ?></td>
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td title="Tgl Batal" align="left"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_update)); ?></span></td>
			<td title="Tgl Transaksi" align="left"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_orderan); ?></td>
			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/order/detail_batal/'.$row->no_struk.'/show'); ?>" title="Detail Batal - <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="12"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="11" class="rounded-foot-left"><em>List Order yang batal</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
		</tfoot>
	</table>
	</div>

	<div id="tabs-sukses" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div class="pagination" style="width:98%;"><?php echo (!$page_sukses)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_sukses; ?></div>

	<table id="rounded-corner" summary="Menu"  style="font-size:10px">
		<thead>
		<tr>
			<th class="rounded-company" scope="col" width="20" style="font-size:11px">Id</th>
			<th scope="col" width="80" style="font-size:11px">No Struk</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Sukses</th>
			<th scope="col" width="100" style="font-size:11px">Tgl Transaksi</th>
			<th scope="col" width="80" style="font-size:11px">A/N Pembeli</th>
			<th scope="col" width="20" style="font-size:11px">Jml Jns Produk</th>
			<th scope="col" width="100" style="font-size:11px">Pembayaran</th>
			<th scope="col" width="20" style="font-size:11px">Ttl Jml Pesan</th>
			<th scope="col" width="60" style="font-size:11px">Diskon Transaksi</th>
			<th scope="col" width="60" style="font-size:11px">Total Transaksi</th>
			<th scope="col" width="40" style="font-size:11px">Status Order</th>
			<th class="rounded-q4"  scope="col" width="50">Opsi</th>
		</tr>
		</thead>
		<tbody>
		<?php
			if(count($order_sukses) > 0):
			$i=0;
			foreach ($order_sukses as $row): 
			//$total = $this->orderan->getTotalOrderanSuksesByTrx($row->no_struk);
			$diskon = $this->orderan->getDiskonTransaksiByTrx($row->no_struk);
			$total = $this->orderan->getTotalOrderanSuksesByTrx($row->no_struk);
			$bayar = $total->total_harga - $diskon;
		?>
		<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
			<td title="Id Pembeli"><?php echo ($row->id_cust); ?></td>
			<td title="No Struk"><?php echo ($row->no_struk); ?></td>
			<td align="left"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_update)); ?></td>
			<td title="Tgl Transaksi" align="center"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
			<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
			<td title="Detail Produk" align="center"><span style="font-size:10px"><?php echo $total->jml_produk.' '.$this->config->item('satuan_jenis_produk'); ?></span></td>
			<td title="Bayar Via" align="left"><span style="font-size:10px"><?php echo ucfirst($row->metode_bayar) . ' - ' . $row->bayar_melalui; ?></span></td>
			<td title="Jml Pesan" align="right"><?php echo $total->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
			<td title="Diskon Transaksi" align="right">
				<?php echo format_harga_indo($diskon); ?>
			</td>
			<td title="Total Harga" align="right">
				<?php echo format_harga_indo($bayar); ?>
			</td>
			<td align="center"><?php echo humanize($row->status_orderan); ?></td>
			<td align="center">
				<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/order/detail_sukses/'.$row->no_struk.'/show'); ?>" title="Detail Sukses - <?php echo ( ! $this->super_admin)?'xx':$row->no_struk; ?>"></a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php else: ?>
		<tr>
			<td colspan="12"><em>Data Kosong</em></td>
		</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="11" class="rounded-foot-left"><em>List Order yang sukses</em></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
		</tfoot>
	</table>
	</div>
</div> <!-- End id="tabs" -->

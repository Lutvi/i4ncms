
	<table id="rounded-corner" summary="Menu">
	    <thead>
	      <tr align="center">
	        <th class="rounded-company" scope="col" width="150">Nama ID</th>
	        <th scope="col" width="150">Nama EN</th>
	        <th scope="col" width="60">Posisi</th>
	        <th scope="col">Gambar Header</th>
	        <th scope="col" width="60">Status</th>
	        <th class="rounded-q4" scope="col" width="80">Opsi</th>
	      </tr>
	    </thead>
	    
	    <tbody>
	      <?php if(count($hbanner) > 0): ?>
	      <?php $i=0;  $jml = $this->web->getCountHbanner(); ?>
	      <?php foreach ($hbanner as $row): ?>
	      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
	        <td><?php echo $row->nama_id; ?></td>
	        <td><?php echo $row->nama_en; ?></td>
	        <td align="center">
			<?php 
				$pos = $row->posisi;
					if($pos == 1): 
			?>
					<a href="#" title="Down" class="rdown" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
			<?php	elseif($pos == $jml): ?>
					<a href="#" title="Up" class="rup" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
			<?php	else: ?>
					<a href="#" title="Down" class="rdown" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>&nbsp;<a href="#" title="Up" class="rup" id="mn_<?php echo $row->id; ?>_<?php echo $row->posisi; ?>"></a>
	        <?php endif; ?>       
	        </td>
	        <td>
				<img src="<?php echo base_url(); ?>_media/banner-header/thumb_<?php echo $row->img_src; ?>" align="absmiddle" />
				&nbsp;&nbsp;<i>
				<?php echo substr($row->img_src,0,25); ?></i>
	        </td>
	        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
	          <select id="status_input_<?php echo $row->id; ?>" class="editbox">
	            <option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
	            <option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
	          </select>
	        </td>
	        <td class="opsi" align="center">
				<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
	            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_id; ?>" href="#"></a>
	            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur-anak'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/hbanner/detail_hbanner/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_id; ?>"></a>
	        </td>
	      </tr>
	      <?php $i++; ?>
	      <?php endforeach; ?>
	      <?php else: ?>
	      <tr>
	        <td colspan="6"><em>Data Kosong</em></td>
	      </tr>
	      <?php endif; ?>
	    </tbody>
	    <tfoot>
	      <tr>
	        <td colspan="5" class="rounded-foot-left"><em>List Banner Header yang terdaftar</em></td>
	        <td class="rounded-foot-right">&nbsp;</td>
	      </tr>
	    </tfoot>
	  </table>

<?php  echo form_open(''); ?>
<?php echo form_close(); ?>
  <table id="rounded-corner" summary="Menu">
	<thead>
	  <tr>
		<th class="rounded-company" scope="col">Produk</th>
		<th scope="col" width="35">Diskon</th>
		<th scope="col" width="80">Harga</th>
		<th scope="col" width="60">Hits</th>
		<th scope="col" width="30">Anak</th>
		<th scope="col" width="60">Stok</th>
		<th scope="col" width="60">Promo</th>
		<th scope="col" width="60">Status</th>
		<th class="rounded-q4" scope="col" width="110">Opsi</th>
	  </tr>
	</thead>
	
	<tbody>
	  <?php if(count($produk) > 0): ?>
	  <?php $i=0; ?>
	  <?php foreach ($produk as $row): ?>
	  <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		<td title="Produk" valign="top">
			<img src="<?php echo base_url().'_produk/thumb/small_thumb_'.$this->mproduk->getImgById($row->id_prod_img); ?>" align="left" style="padding:0 5px;" />
			<div style="margin-left:75px;width:78%">
				<?php echo (!empty($row->kode_prod))?'<small>'.$row->kode_prod.'</small><br>':''; ?>
				<?php echo $row->nama_prod; ?>
				<div class="btsJudul"></div>
				<?php echo $row->nama_prod_en; ?>
				<br><small style="color:#3C5A96; font-weight:700">Rp.<?php echo format_harga_indo(($row->harga_spesial != 0)?$row->harga_spesial:$row->harga_prod); ?></small>
			</div>
		</td>
		<td title="Diskon" align="center"><?php echo ($row->diskon * 100); ?>%</td>
		<td title="Harga" align="right"><?php echo ($row->diskon == 0)?format_harga_indo($row->harga_prod):'<strike style="color:red">'.format_harga_indo($row->harga_prod).'</strike>'; ?></td>
		<td title="Hits" align="right"><?php echo $row->total_hits; ?></td>
		<td title="Jumlah Anak" align="right"><?php echo $this->mproduk->getCountAnakProduk($row->id_prod); ?></td>
		<td title="Stok" align="right">
		<span id="stok_<?php echo $row->id_prod; ?>" class="text"><?php echo $row->stok; ?></span>
		<input type="text" value="<?php echo $row->stok; ?>" class="editbox" id="stok_input_<?php echo $row->id_prod; ?>" />
		</td>
		<td align="center"><span id="promo_<?php echo $row->id_prod; ?>" class="text"><?php echo humanize($row->promo); ?></span>
		  <select id="promo_input_<?php echo $row->id_prod; ?>" class="editbox">
			<option value="on" <?php echo ($row->promo == 'on')?'selected="selected"':''; ?>>On</option>
			<option value="off" <?php echo ($row->promo == 'off')?'selected="selected"':''; ?>>Off</option>
		  </select>
		</td>
		<td align="center"><span id="status_<?php echo $row->id_prod; ?>" class="text"><?php echo humanize($row->status); ?></span>
		  <select id="status_input_<?php echo $row->id_prod; ?>" class="editbox">
			<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
			<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
		  </select>
		</td>
		<td align="center">
			<a class="edit" id="<?php echo $row->id_prod; ?>" href="#" title="Ubah status"></a>&nbsp;
			<a class="<?php echo ( ! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id_prod; ?>" title="Hapus <?php echo ( ! $this->super_admin)?'xx':$row->nama_prod; ?>" href="#"></a>&nbsp;
			<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/produk/detail/'.$row->id_prod.'/show'); ?>" title="Update Produk <?php echo ( ! $this->super_admin)?'xx':$row->nama_prod; ?>"></a>&nbsp;
			<?php if($this->mproduk->getCountAnakProduk($row->id_prod) > 0 ): ?>
			<a class="anak <?php echo ( ! $this->super_admin)?'dis_atur-anak':'atur-anak'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/produk/detail_anak/'.$row->id_prod.'/show'); ?>" title="Update Anak <?php echo ( ! $this->super_admin)?'xx':$row->nama_prod; ?>"></a>
			<?php endif; ?>
		</td>
	  </tr>
	  <?php $i++; ?>
	  <?php endforeach; ?>
	  <?php else: ?>
	  <tr>
		<td colspan="9"><em>Data Kosong</em></td>
	  </tr>
	  <?php endif; ?>
	</tbody>
	
	<tfoot>
	  <tr>
		<td colspan="8" class="rounded-foot-left"><em>List produk yang terdaftar</em></td>
		<td class="rounded-foot-right">&nbsp;</td>
	  </tr>
	</tfoot>
  </table>

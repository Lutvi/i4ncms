  <table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col" width="110">Nama Vendor</th>
        <th scope="col" width="90">Logo</th>
        <th scope="col" width="110">Nama Layanan</th>
        <th scope="col" width="90">Ongkos Kirim</th>
		<th scope="col" width="200">Waktu Sampai</th>
        <th scope="col" width="60">Status</th>
        <th class="rounded-q4" scope="col" width="80">Opsi</th>
      </tr>
    </thead>
    
    <tbody>
      <?php if(count($kurir) > 0): ?>
      <?php $i=0; ?>
      <?php foreach ($kurir as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><?php echo $row->nama_vendor; ?></td>
        <td align="center">
			<img src="<?php echo base_url(); ?>_media/logo-kurir/thumb_<?php echo $row->logo; ?>" width="80" align="absmiddle" />
        </td>
        <td><?php echo $row->nama_layanan; ?></td>
        <td align="center"><?php echo format_harga_indo($row->biaya_ongkir); ?></td>
        <td align="center"><?php echo $row->waktu_barang_sampai; ?> Hari</td>
        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
		  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
			<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
			<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
		  </select>
		</td>
        <td class="opsi" align="center">
			<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_vendor; ?>" href="#"></a>&nbsp;
            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur-anak'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/atur_kurir_pengiriman/detail_kurir/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_vendor; ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="7"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="6" class="rounded-foot-left"><em>List Kurir Pengiriman yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

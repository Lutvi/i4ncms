<div id="banner-tab" style="background:transparent;font-size:12px;margin-top:20px">

	<ul>
		<li><a href="#tabs-utama">Banner Iklan Atas</a></li>
		<li><a href="#tabs-sidebar">Banner Iklan Sidebar</a></li>
		<li><a href="#tabs-atas">Banner Iklan Koten Atas</a></li>
		<li><a href="#tabs-bawah">Banner Iklan Koten Bawah</a></li>
	</ul>

<div id="tabs-utama" style="font: 12px normal Helvetica, Arial, sans-serif">

	<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$page_utama)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_utama; ?></div>

		  <table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="110">Nama Pengiklan</th>
		        <th scope="col" width="110">Nama</th>
		        <th scope="col">Gambar/URL</th>
		        <th scope="col" width="80">Tgl terbit</th>
		        <th scope="col" width="80">Tgl berakhir</th>
		        <th scope="col" width="40">Tampil</th>
		        <th scope="col" width="40">Hits</th>
		        <th scope="col" width="40">Kredit</th>
		        <th scope="col" width="60">Status</th>
		        <th class="rounded-q4" scope="col" width="80">Opsi</th>
		      </tr>
		    </thead>
		    
		    <tbody>
		      <?php if(count($iklan_utama) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($iklan_utama as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		        <td><?php echo $row->nama_pengiklan; ?></td>
		        <td>
					<?php echo $row->nama_id; ?>
					<br>
					<?php echo $row->nama_en; ?>
				</td>
		        <td align="center">
					<img src="<?php echo base_url(); ?>_media/banner-iklan/thumb/thumb_<?php echo $row->img_src; ?>" align="absmiddle" />
					<br>
					<?php echo substr($row->url_target, 0, 10); ?>...
		        </td>
		        <td align="center"><?php echo format_tgl_indo_jq($row->tgl_terbit); ?></td>
		        <td align="center" <?php echo (time() > strtotime($row->tgl_berakhir))?'style="color:#FF0000"':'style="color:#0E9308"'; ?>><?php echo format_tgl_indo_jq($row->tgl_berakhir); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->tampil); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->hits); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->kredit); ?></td>
		        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
				  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
					<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
					<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
				  </select>
				</td>
		        <td class="opsi" align="center">
					<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin || strtotime($row->tgl_berakhir) >= time())?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_id; ?>" href="#"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/banner/detail_iklan/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_id; ?>"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="10"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="9" class="rounded-foot-left"><em>List Banner Iklan Konten Bawah yang terdaftar</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		  </table>
	</div>
</div>

<div id="tabs-sidebar" style="font: 12px normal Helvetica, Arial, sans-serif">

	<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$page_sidebar)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_sidebar; ?></div>

		  <table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="110">Nama Pengiklan</th>
		        <th scope="col" width="110">Nama</th>
		        <th scope="col">Gambar/URL</th>
		        <th scope="col" width="80">Tgl terbit</th>
		        <th scope="col" width="80">Tgl berakhir</th>
		        <th scope="col" width="40">Tampil</th>
		        <th scope="col" width="40">Hits</th>
		        <th scope="col" width="40">Kredit</th>
		        <th scope="col" width="60">Status</th>
		        <th class="rounded-q4" scope="col" width="80">Opsi</th>
		      </tr>
		    </thead>
		    
		    <tbody>
		      <?php if(count($iklan_sidebar) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($iklan_sidebar as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		        <td><?php echo $row->nama_pengiklan; ?></td>
		        <td>
					<?php echo $row->nama_id; ?>
					<br>
					<?php echo $row->nama_en; ?>
				</td>
		        <td align="center">
					<img src="<?php echo base_url(); ?>_media/banner-iklan/thumb/thumb_<?php echo $row->img_src; ?>" align="absmiddle" />
					<br>
					<?php echo substr($row->url_target, 0, 10); ?>...
		        </td>
		        <td align="center"><?php echo format_tgl_indo_jq($row->tgl_terbit); ?></td>
		        <td align="center" <?php echo (time() > strtotime($row->tgl_berakhir))?'style="color:#FF0000"':'style="color:#0E9308"'; ?>><?php echo format_tgl_indo_jq($row->tgl_berakhir); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->tampil); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->hits); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->kredit); ?></td>
		        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
				  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
					<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
					<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
				  </select>
				</td>
		        <td class="opsi" align="center">
					<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin || strtotime($row->tgl_berakhir) >= time())?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_id; ?>" href="#"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/banner/detail_iklan/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_id; ?>"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="10"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="9" class="rounded-foot-left"><em>List Banner Iklan Konten Bawah yang terdaftar</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		  </table>
	</div>
</div>

<div id="tabs-atas" style="font: 12px normal Helvetica, Arial, sans-serif;">

	<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$page_atas)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_atas; ?></div>

		  <table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="110">Nama Pengiklan</th>
		        <th scope="col" width="110">Nama</th>
		        <th scope="col">Gambar/URL</th>
		        <th scope="col" width="80">Tgl terbit</th>
		        <th scope="col" width="80">Tgl berakhir</th>
		        <th scope="col" width="40">Tampil</th>
		        <th scope="col" width="40">Hits</th>
		        <th scope="col" width="40">Kredit</th>
		        <th scope="col" width="60">Status</th>
		        <th class="rounded-q4" scope="col" width="80">Opsi</th>
		      </tr>
		    </thead>
		    
		    <tbody>
		      <?php if(count($iklan_atas) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($iklan_atas as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		        <td><?php echo $row->nama_pengiklan; ?></td>
		        <td>
					<?php echo $row->nama_id; ?>
					<br>
					<?php echo $row->nama_en; ?>
				</td>
		        <td align="center">
					<img src="<?php echo base_url(); ?>_media/banner-iklan/thumb/thumb_<?php echo $row->img_src; ?>" align="absmiddle" />
					<br>
					<?php echo substr($row->url_target, 0, 10); ?>...
		        </td>
		        <td align="center"><?php echo format_tgl_indo_jq($row->tgl_terbit); ?></td>
		        <td align="center" <?php echo (time() > strtotime($row->tgl_berakhir))?'style="color:#FF0000"':'style="color:#0E9308"'; ?>><?php echo format_tgl_indo_jq($row->tgl_berakhir); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->tampil); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->hits); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->kredit); ?></td>
		        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
				  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
					<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
					<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
				  </select>
				</td>
		        <td class="opsi" align="center">
					<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin || strtotime($row->tgl_berakhir) >= time())?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_id; ?>" href="#"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/banner/detail_iklan/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_id; ?>"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="10"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="9" class="rounded-foot-left"><em>List Banner Iklan Konten Bawah yang terdaftar</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		  </table>
	</div>
</div>


<div id="tabs-bawah" style="font: 12px normal Helvetica, Arial, sans-serif">

	<div style="font: 11px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$page_bawah)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page_bawah; ?></div>

		  <table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="110">Nama Pengiklan</th>
		        <th scope="col" width="110">Nama</th>
		        <th scope="col">Gambar/URL</th>
		        <th scope="col" width="80">Tgl terbit</th>
		        <th scope="col" width="80">Tgl berakhir</th>
		        <th scope="col" width="40">Tampil</th>
		        <th scope="col" width="40">Hits</th>
		        <th scope="col" width="40">Kredit</th>
		        <th scope="col" width="60">Status</th>
		        <th class="rounded-q4" scope="col" width="80">Opsi</th>
		      </tr>
		    </thead>
		    
		    <tbody>
		      <?php if(count($iklan_bawah) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($iklan_bawah as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		        <td><?php echo $row->nama_pengiklan; ?></td>
		        <td>
					<?php echo $row->nama_id; ?>
					<br>
					<?php echo $row->nama_en; ?>
				</td>
		        <td align="center">
					<img src="<?php echo base_url(); ?>_media/banner-iklan/thumb/thumb_<?php echo $row->img_src; ?>" align="absmiddle" />
					<br>
					<?php echo substr($row->url_target, 0, 10); ?>...
		        </td>
		        <td align="center"><?php echo format_tgl_indo_jq($row->tgl_terbit); ?></td>
		        <td align="center" <?php echo (time() > strtotime($row->tgl_berakhir))?'style="color:#FF0000"':'style="color:#0E9308"'; ?>><?php echo format_tgl_indo_jq($row->tgl_berakhir); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->tampil); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->hits); ?></td>
		        <td align="center"><?php echo format_harga_indo($row->kredit); ?></td>
		        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
				  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
					<option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
					<option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
				  </select>
				</td>
		        <td class="opsi" align="center">
					<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin || strtotime($row->tgl_berakhir) >= time())?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" title="Hapus <?php echo $row->nama_id; ?>" href="#"></a>&nbsp;
		            <a class="<?php echo (! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/banner/detail_iklan/'.$row->id.'/show'); ?>" title="Detail <?php echo $row->nama_id; ?>"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="10"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="9" class="rounded-foot-left"><em>List Banner Iklan Konten Atas yang terdaftar</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		  </table>
	</div>
</div>

</div><!-- End #banner-tab -->


<div id="detail-box">
	<img src="/_produk/small/small_<?php echo $img_produk; ?>" align="right"/>
	<div style="display:block">
		<?php if( ! empty($detail_produk->kode_warna)): ?>
		<div style="float:left;width:15px;height:15px;border:2px solid #F4970C;background:#<?php echo $detail_produk->kode_warna;?>"></div>
		<div style="clear:left"></div>
		<?php endif; ?>
		
		<?php if (empty($detail_produk->harga_spesial)): ?>
			<span style="float:left;font-weight:bold">Rp.<?php echo number_format($detail_produk->harga_prod, 0,'','.'); ?> / <?php echo $this->config->item('satuan_produk'); ?></span>
			<?php else: ?>
			<span style="float:left;text-decoration:line-through;color:#9C0909;">Rp.<?php echo number_format($detail_produk->harga_prod, 0,'','.'); ?> / <?php echo $this->config->item('satuan_produk'); ?></span>
			<div style="clear:left"></div>
			<span style="float:left;font-weight:bold">Rp.<?php echo number_format($detail_produk->harga_spesial, 0,'','.'); ?> / <?php echo $this->config->item('satuan_produk'); ?></span>
		<?php endif; ?>
		<div style="clear:left"></div>
		
		<?php if ( ! empty($detail_produk->diskon)): ?>
			<span style="float:left;margin-right:10px">Diskon : </span><span style="float:left"><?php echo ($detail_produk->diskon * 100).'%'; ?></span>
		<?php endif; ?>
		<div style="clear:left"></div>
		<span style="float:left;font-weight:bold"><?php echo $detail_produk->nama_prod;?></span>
		<div style="clear:left"></div>
		<span style="float:left;font-size:10px"><?php echo $detail_produk->kode_prod;?></span>
	</div>
</div>

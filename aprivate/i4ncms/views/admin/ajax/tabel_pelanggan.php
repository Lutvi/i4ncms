  <table id="rounded-corner" summary="Menu" style="font-size:10px">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col" width="150" style="font-size:11px">Nama Lengkap</th>
        <th scope="col" style="font-size:11px">Tgl Daftar</th>
        <th scope="col" style="font-size:11px">Tgl Update</th>
        <th scope="col" style="font-size:11px">Trans Sukses</th>
        <th scope="col" style="font-size:11px">Ttl Trans Sukses</th>
        <th scope="col" style="font-size:11px">Trans Gagal</th>
        <th scope="col" style="font-size:11px">Ttl Trans Gagal</th>
        <th scope="col" width="70" style="font-size:11px">Status</th>
        <th scope="col" width="70" style="font-size:11px">RSS</th>
        <th scope="col" width="70" style="font-size:11px">Prioritas</th>
        <th scope="col" width="40" style="font-size:11px">Point</th>
        <th class="rounded-q4" scope="col" width="80" style="font-size:11px">Opsi</th>
      </tr>
    </thead>
    
    <tbody>
      <?php if(count($pelanggan) > 0): ?>
      <?php $i=0; ?>
      <?php foreach ($pelanggan as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><?php echo humanize(dekodeString($row->nama_lengkap)); ?></td>
        <td><?php echo date('d/m/Y', strtotime($row->tgl_daftar)); ?></td>
        <td><?php echo date('d/m/Y', strtotime($row->tgl_update_data)); ?></td>
        <td align="right"><?php echo format_harga_indo($row->jml_transaksi); ?></td>
        <td align="right"><?php echo format_harga_indo($row->total_transaksi); ?></td>
        <td align="right"><?php echo format_harga_indo($row->jumlah_transaksi_gagal); ?></td>
        <td align="right"><?php echo format_harga_indo($row->total_transaksi_gagal); ?></td>
		<td align="center"><span id="status_tcust_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status_cust); ?></span>
          <select id="status_cust_<?php echo $row->id; ?>" class="editbox">
            <option value="aktif" <?php echo ($row->status_cust == 'aktif')?'selected="selected"':''; ?>>Aktif</option>
            <option value="banned" <?php echo ($row->status_cust == 'banned')?'selected="selected"':''; ?>>Banned</option>
            <option value="vakum" <?php echo ($row->status_cust == 'vakum')?'selected="selected"':''; ?>>Vakum</option>
          </select>
        </td>
        <td align="center"><span id="status_trss_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status_rss); ?></span>
          <select id="status_rss_<?php echo $row->id; ?>" class="editbox">
            <option value="on" <?php echo ($row->status_rss == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->status_rss == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        <td align="center"><span id="status_tprio_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->prioritas_cust); ?></span>
          <select id="status_prio_<?php echo $row->id; ?>" class="editbox">
            <option value="utama" <?php echo ($row->prioritas_cust == 'utama')?'selected="selected"':''; ?>>Utama</option>
            <option value="menengah" <?php echo ($row->prioritas_cust == 'menengah')?'selected="selected"':''; ?>>Menengah</option>
            <option value="normal" <?php echo ($row->prioritas_cust == 'normal')?'selected="selected"':''; ?>>Normal</option>
          </select>
        </td>
        <td align="center"><?php echo (empty($row->jml_poin))?0:format_harga_indo($row->jml_poin); ?></td>
        <td align="center">
          <a class="<?php echo ( ! $this->super_admin)?'dis_edit':'edit'; ?>" id="<?php echo ( ! $this->super_admin)?'xx':$row->id; ?>" href="javascript:void(0)" title="Ubah status"></a>&nbsp;
            <a class="<?php echo ( ! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo ( ! $this->super_admin)?'xx':$row->id; ?>" title="Hapus <?php echo ( ! $this->super_admin)?'xx':humanize(dekodeString($row->id_cust_uname)); ?>" href="javascript:void(0)"></a>&nbsp;
          <a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/pelanggan/detail/'.$row->id.'/show'); ?>" title="Detail <?php echo ( ! $this->super_admin)?'xx':humanize(dekodeString($row->nama_lengkap)); ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="12"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="11" class="rounded-foot-left"><em>List Pelanggan yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

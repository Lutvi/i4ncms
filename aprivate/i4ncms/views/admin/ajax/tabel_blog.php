  <table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col" width="370">Judul</th>
        <th scope="col">Kategori</th>
        <th scope="col" width="100">Embed</th>
        <th scope="col" width="85">Headline</th>
        <th scope="col" width="60">Diskusi</th>
        <th scope="col" width="60">Status</th>
        <th class="rounded-q4" scope="col" width="80">Opsi</th>
      </tr>
    </thead>

    <tbody>
      <?php if(count($blog_web) > 0): ?>
      <?php $i=0; $jml = $this->cms->getCountBlog(); ?>
      <?php foreach ($blog_web as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td>
			<img src="<?php echo base_url(); ?>_media/blog/thumb/thumb_<?php echo $row->gambar_cover; ?>" width="60" height="45" align="left" style="padding:0 5px 0 0;" />
			<div style="margin-left:75px;margin-bottom:10px;width:75%">
				<?php echo character_limiter($row->judul_id, 40); ?>
				<div class="btsJudul"></div>
				<?php echo character_limiter($row->judul_en, 40); ?>
			</div>
			Hits : <small style="color:#DA0404; font-weight:700"><?php echo format_harga_indo($row->hits); ?></small>
		</td>
		<td><?php echo $this->cms->getNamaKategoriById($row->kategori_id); ?></td>
        <td align="center">
	        <?php
	        if( ! empty($row->embed_id_album) && ! empty($row->embed_id_video))
	        echo 'Album &amp; Video';
	        elseif( ! empty($row->embed_id_album))
	        echo 'Album';
	        elseif(! empty($row->embed_id_video))
	        echo 'Video';
	        else
	        echo '-- Tidak Ada --';
	        ?>
        </td>
        <td align="center"><span id="headline_<?php echo $row->id; ?>" class="text"><?php echo ($row->headline === 'n')?'Bukan':'Ya'; ?></span>
          <select id="headline_input_<?php echo $row->id; ?>" class="editbox" style="width:80px;font-size:11px">
            <option value="y" <?php echo ($row->headline == 'y')?'selected="selected"':''; ?>>Ya</option>
            <option value="n" <?php echo ($row->headline == 'n')?'selected="selected"':''; ?>>Bukan</option>
          </select>
        </td>
        <td align="center"><span id="diskusi_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->diskusi); ?></span>
          <select id="diskusi_input_<?php echo $row->id; ?>" class="editbox" style="font-size:11px">
            <option value="on" <?php echo ($row->diskusi == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->diskusi == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        <td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize($row->status); ?></span>
          <select id="status_input_<?php echo $row->id; ?>" class="editbox" style="font-size:11px">
            <option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        <td align="center">
			<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
			<a class="<?php echo ( ! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo ( ! $this->super_admin)?'xx':$row->id; ?>" href="#" title="Hapus <?php echo ( ! $this->super_admin)?'xx':humanize($row->judul_id); ?>"></a>&nbsp;
			<a class="<?php echo ( ! $this->super_admin)?'dis_atur':'atur'; ?>" href="<?php echo ( ! $this->super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/blog/detail/'.$row->id.'/show'); ?>" title="Update <?php echo ( ! $this->super_admin)?'xx':ucwords($row->judul_id); ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="7"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="6" class="rounded-foot-left"><em>List posting yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

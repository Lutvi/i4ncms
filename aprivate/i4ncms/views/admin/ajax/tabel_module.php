<table id="rounded-corner" summary="2007 Major IT Companies' Profit">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col">Module</th>
        <th scope="col" width="50">Posisi</th>
        <th scope="col" width="70">Status</th>
        <th scope="col">Path</th>
        <th scope="col">Deskripsi</th>
        <th class="rounded-q4" scope="col" width="90">Opsi Module</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="5" class="rounded-foot-left"><em>List module yang terpasang</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
    <tbody>
      <?php if(count($modul) > 0): ?>
      <?php $i=0; $jml = $this->cms->getCountModule(); ?>
      <?php foreach ($modul as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><?php echo $row->nama_module; ?> </td>
        <td align="center">
		<?php 
			$pos = $row->posisi;
				if($pos == 1): 
		?>
				<a href="#" title="Down" class="rdown" id="md_<?php echo $row->id_module; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	elseif($pos == $jml): ?>
				<a href="#" title="Up" class="rup" id="mu_<?php echo $row->id_module; ?>_<?php echo $row->posisi; ?>"></a>
		<?php	else: ?>
				<a href="#" title="Down" class="rdown" id="md_<?php echo $row->id_module; ?>_<?php echo $row->posisi; ?>"></a>&nbsp;<a href="#" title="Up" class="rup" id="mu_<?php echo $row->id_module; ?>_<?php echo $row->posisi; ?>"></a>        
        <?php endif; ?>       
        </td>
        <td align="center">
        <span id="status_<?php echo $row->id_module; ?>" class="text"><?php echo ($row->status == 1)?'On':'Off'; ?></span>
        <select id="status_input_<?php echo $row->id_module; ?>" class="editbox">
	        <option value="1" <?php echo ($row->status == 1)?'selected="selected"':''; ?>>On</option>
	        <option value="0" <?php echo ($row->status == 0)?'selected="selected"':''; ?>>Off</option>
        </select>
        </td>
        <td><?php echo $row->path_front_module; ?> </td>
        <td><?php echo word_limiter($row->deskripsi,5); ?> </td>
        <td align="center">
        	<a class="edit" id="<?php echo (! $this->super_admin)?'xx':$row->id_module; ?>" href="javascript:void(0)" title="Ubah status"></a>&nbsp;
            <a class="<?php echo (! $this->super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo (! $this->super_admin)?'xx':$row->id_module; ?>" href="javascript:void(0)" title="Hapus Module <?php echo (! $this->super_admin)?'xx':humanize($row->nama_module); ?>"></a>&nbsp;
            <a class="<?php echo ($row->path_admin_module=='' || ! $this->super_admin)?'dis_atur':'atur'; ?>" id="<?php echo 'atur_'.$row->id_module; ?>" href="<?php echo ($row->path_admin_module=='' || ! $this->super_admin)?'javascript:void(0)':base_url().$row->path_admin_module; ?>" title="Atur Module <?php echo (! $this->super_admin)?'xx':humanize($row->nama_module); ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="6"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
</table>

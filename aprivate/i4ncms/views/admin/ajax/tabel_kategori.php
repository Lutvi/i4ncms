<table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col" width="90">Id</th>
        <th scope="col" width="110">Tipe</th>
        <th scope="col" width="">Label ID</th>
        <th scope="col" width="">Label EN</th>
        <th scope="col" width="60">Status</th>
        <th class="rounded-q4" scope="col" width="40">Opsi</th>
      </tr>
    </thead>
    <tbody>
      <?php if(count($kategori) > 0): ?>
      <?php $i=0; ?>
      <?php foreach ($kategori as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		<td align="center">
			<?php echo $row->id_kategori; ?>
		</td>
		<td><?php echo $row->tipe_kategori; ?></td>
        <td><span id="nmid_<?php echo $row->id_kategori; ?>" class="text"><?php echo $row->label_id; ?></span> 
        <input style="width:200px" maxlength="50" type="text" value="<?php echo $row->label_id; ?>" class="editbox2" id="nmid_input_<?php echo $row->id_kategori; ?>" />
        <input type="hidden" value="<?php echo $row->label_id; ?>" class="editbox2" id="nmoriid_input_<?php echo $row->id_kategori; ?>" />
        <input type="hidden" value="<?php echo $row->tipe_kategori; ?>" class="editbox2" id="tipe_input_<?php echo $row->id_kategori; ?>" />
        </td>
        <td><span id="nmen_<?php echo $row->id_kategori; ?>" class="text"><?php echo $row->label_en; ?></span> 
        <input style="width:200px" maxlength="50" type="text" value="<?php echo $row->label_en; ?>" class="editbox2" id="nmen_input_<?php echo $row->id_kategori; ?>" />
        <input type="hidden" value="<?php echo $row->label_en; ?>" class="editbox2" id="nmorien_input_<?php echo $row->id_kategori; ?>" />
        </td>
        <td align="center"><?php echo humanize($row->status); ?></td>
        <td align="center">
			<a class="edit" id="<?php echo $row->id_kategori; ?>" href="#" title="Ubah status"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="6"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="5" class="rounded-foot-left"><em>List kategori yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
</table>

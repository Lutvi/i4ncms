  <table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col">Label ID</th>
        <th scope="col">Label EN</th>
        <th scope="col">Tipe</th>
        <th scope="col" width="60">Status</th>
        <th scope="col">Module</th>
        <th scope="col" width="50">Hits</th>
        <th class="rounded-q4" scope="col" width="60">Opsi</th>
      </tr>
    </thead>

    <tbody>
      <?php if(count($halaman_web) > 0): ?>
      <?php $i=0; ?>
      <?php foreach ($halaman_web as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><?php echo $row->label_id; ?></td>
        <td><?php echo $row->label_en; ?></td>
        <td><?php echo (empty($row->kategori))?$row->tipe:$row->tipe.'-[kategori]'; ?></td>
        <td align="center"><span id="status_<?php echo $row->id_halaman; ?>" class="text"><?php echo humanize($row->status); ?></span>
          <select id="status_input_<?php echo $row->id_halaman; ?>" class="editbox">
            <option value="on" <?php echo ($row->status == 'on')?'selected="selected"':''; ?>>On</option>
            <option value="off" <?php echo ($row->status == 'off')?'selected="selected"':''; ?>>Off</option>
          </select>
        </td>
        <td>
            <?php 
            $id = $row->id_module;
            if($this->cms->getNamaModById($id))
                echo $this->cms->getNamaModById($id);
            else
                echo '<div style="text-align:center"> - </div>';
            ?>
        </td>
        <td align="right"><?php echo $row->hits; ?></td>
        <td align="center">
          <a class="<?php echo ($super_admin==FALSE)?'dis_edit':'edit'; ?>" id="<?php echo ($super_admin==FALSE)?'xx':$row->id_halaman; ?>" href="#" title="Ubah status"></a>&nbsp;
          <a class="<?php echo ($super_admin==FALSE)?'dis_atur':'atur'; ?>" href="<?php echo ($super_admin==FALSE)?'javascript:void(0)':site_url($this->config->item('admpath').'/halaman/detail/'.$row->id_halaman.'/show'); ?>" title="Update <?php echo ($super_admin==FALSE)?'xx':humanize($row->label_id); ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="7"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="6" class="rounded-foot-left"><em>List halaman yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
  </table>

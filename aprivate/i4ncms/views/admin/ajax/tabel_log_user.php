<div id="logs-tab" style="background:transparent;font-size:12px;margin-top:20px">

	<ul>
		<li><a href="#tabs-error">Error Log</a></li>
		<li><a href="#tabs-user">User Log</a></li>
		<li><a href="#tabs-robot">Robot Log</a></li>
	</ul>

<div id="tabs-error" style="font: 12px normal Helvetica, Arial, sans-serif;">

	<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

		<?php echo form_open($this->config->item('admpath').'/log_user/selektif_hapus_errlog', array('id' => 'frmLogErr')); ?>
		<span id="tombol" class="ui-widget-header ui-corner-all">
			<button class="hps" id="hpsErrLog" title="Hapus Terpilih" >Hapus</button>
		</span>

		<table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="40">
		        <?php if(count($logging) > 0): ?>
		        <input type="checkbox" id="pil_err_log" value="1" />
		        <?php endif; ?>
		        </th>
		        <th scope="col" width="110">Tipe Error</th>
		        <th scope="col" width="70">Tanggal</th>
		        <th scope="col">URL</th>
		        <th scope="col">Keterangan</th>
		        <th class="rounded-q4" scope="col" width="40">Detail</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php if(count($logging) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($logging as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
				<td align="center"><input type="checkbox" name="err_log[]" value="<?php echo $row->id; ?>" /></td>
		        <td><?php echo $row->tipe_error; ?><br><small style="color:#A52A2A; font-weight:700"><?php echo $row->ip_port; ?></small></td>
		        <td align="center"><?php echo date('d-m-Y H:i:s',strtotime($row->tgl)); ?></td>
		        <td ><?php echo substr($row->url, 0, 30); ?>...</td>
		        <td><?php echo word_limiter($row->ket,20); ?></td>
		        <td align="center">
		            <a class="<?php echo (! $super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/log_user/detail/'.$row->id.'/show'); ?>" title="Detail Log <?php echo $row->tgl; ?>"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="6"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="5" class="rounded-foot-left"><em>List log Error yang terekam</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		</table>
		<?php echo form_close(); ?>

	</div>
	
</div>

<div id="tabs-user" style="font: 12px normal Helvetica, Arial, sans-serif">

	<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$user)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$user; ?></div>
	
		<div id="tabel"> 
		  <table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="70">Tanggal</th>
		        <th scope="col" width="70">IP</th>
		        <th scope="col" width="40">Hits</th>
		        <th scope="col">Halaman</th>
		        <th scope="col">URL Asal</th>
		        <th scope="col">URL Tujuan</th>
		        <th scope="col">Keterangan</th>
		        <th class="rounded-q4" scope="col" width="40">Detail</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php if(count($log_user) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($log_user as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		        <td align="center"><?php echo date('d-m-Y H:i:s',strtotime($row->tgl_kujungan)); ?></td>
		        <td align="center"><?php echo $row->ip; ?><br><small style="color:#A52A2A; font-weight:700"><?php echo $row->port; ?></small></td>
		        <td align="right"><?php echo format_harga_indo($row->hits); ?></td>
		        <td><?php echo $row->tipe_halaman; ?></td>
		        <td><?php echo substr($row->url_asal,0,20); ?>...</td>
		        <td><?php echo substr($row->url_target,0,20); ?>...</td>
		        <td><?php echo substr($row->detail_platform,0,40); ?>..</td>
		        <td align="center">
		            <a class="<?php echo (! $super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/log_user/detail_user/'.$row->id.'/show'); ?>" title="Detail User"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="8"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="7" class="rounded-foot-left"><em>List log yang terekam</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		  </table>
		</div>
	</div>
	
</div>

<div id="tabs-robot" style="font: 12px normal Helvetica, Arial, sans-serif">

	<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination"><?php echo (!$robot)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$robot; ?></div>
	
		<table id="rounded-corner" summary="Menu">
		    <thead>
		      <tr align="center">
		        <th class="rounded-company" scope="col" width="70">Tanggal</th>
		        <th scope="col" width="70">IP</th>
		        <th scope="col">Halaman</th>
		        <th scope="col">URL Asal</th>
		        <th scope="col">URL Tujuan</th>
		        <th scope="col">Keterangan</th>
		        <th class="rounded-q4" scope="col" width="40">Detail</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php if(count($log_robot) > 0): ?>
		      <?php $i=0; ?>
		      <?php foreach ($log_robot as $row): ?>
		      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
		        <td align="center"><?php echo date('d-m-Y H:i:s',strtotime($row->tgl_kujungan)); ?></td>
		        <td align="center"><?php echo $row->ip; ?><br><small style="color:#A52A2A; font-weight:700"><?php echo $row->port; ?></small></td>
		        <td><?php echo $row->tipe_halaman; ?></td>
		        <td><?php echo substr($row->url_asal,0,20); ?>...</td>
		        <td><?php echo substr($row->url_target,0,20); ?>...</td>
		        <td><?php echo substr($row->detail_robot,0,40); ?>..</td>
		        <td align="center">
		            <a class="<?php echo (! $super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $super_admin)?'#':site_url($this->config->item('admpath').'/log_user/detail_robot/'.$row->id.'/show'); ?>" title="Detail Robot"></a>
		        </td>
		      </tr>
		      <?php $i++; ?>
		      <?php endforeach; ?>
		      <?php else: ?>
		      <tr>
		        <td colspan="7"><em>Data Kosong</em></td>
		      </tr>
		      <?php endif; ?>
		    </tbody>
		    <tfoot>
		      <tr>
		        <td colspan="6" class="rounded-foot-left"><em>List log yang terekam</em></td>
		        <td class="rounded-foot-right">&nbsp;</td>
		      </tr>
		    </tfoot>
		  </table>
		</div>
	</div>

</div><!-- End #logs-tab -->

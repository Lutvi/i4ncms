<?php $this->load->view($this->config->item('admin_theme_id').'/main/head'); ?>
<script type="text/javascript">
var stTrans = '<?php echo set_value('status_transaksi',$order[0]->status_transaksi); ?>',
	stOrder = '<?php echo set_value('status_order',$order[0]->status_orderan); ?>';
</script>

<style>
#stylized label { padding-top: 8px;}
</style>

<script type="text/javascript">
function cekResi()
{
	var selTrans = $('#status_transaksi').val();
	
	if(selTrans === 'pembayaran') {
		$('#box-noresi').hide();
	} else {
		$('#box-noresi').show();
	}
}

function cekOrder()
{
	var selOrder = $('#status_order').val();
	
	if(stOrder === 'batal')
	{
		if(selOrder === 'proses') {
			$('#box-status-transaksi').show();
			$('#hapus').hide();
		} else {
			$('#box-status-transaksi').hide();
			$('#status_transaksi').val('pending');
			$('#hapus').show();
		}
	} else {
		if(selOrder === 'proses') {
			$('#box-status-transaksi').show();
			$('#hapus').hide();
		} else {
			$('#box-status-transaksi').hide();
			$('#status_transaksi').val('pending');
		}
	}
}

function cekStatusOrder()
{
	if(stTrans === 'pembayaran') {
		$('#box-noresi').hide();
	} else {
		$('#box-noresi').show();
	}
	
	if(stOrder === 'batal') {
		$('#box-status-order').show();
		$('#box-status-transaksi').hide();
	}
}

function expPost()
{
    location.reload();
}

var getDialogButton = function(dialogSelector) {
					            return $('.ui-dialog-buttonpane button', $(dialogSelector).parent()[0]);
					        }

$(function() {

	$('#stylized').show();
	cekStatusOrder();
	
	if($('#info-update').is(':visible')) {
		$('#info-update').effect('pulsate',{},500)
		.delay(10000)
		.slideUp('slow');
	}
	
	$('#status_transaksi').change(function(){
		cekResi();
	});
	
	$('#status_order').change(function(){
		cekOrder();
	});

	$('.detail-produk').click(function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		showDialog(url);
	});

	$("#detail-prod").dialog({
		autoOpen: false,
		minHeight: 300,
		width: 550,
		modal: true
	});

	function showDialog(url){
		$("#detail-prod").empty().load(url);
		$("#detail-prod").dialog("open");
	}

	$( "#detail-alamat" ).dialog({
        autoOpen: false,
        //height: 250,
        width: 750,
        resizable: false,
        position: { my: "center top", at: "center top", of: window },
        modal: true,
        buttons: {
            "Update": function(e) {
				$('#proses_data').show();
				getDialogButton("#detail-alamat").addClass("ui-state-disabled").attr('disabled', 'disabled' );
				$.ajax({
					type: "POST",
					url: $(this).data('url'),
					data: $("#form-update-alamat").serialize(),
					success: function(data){
						if(data === 'sukses') {
							//$( "#detail-alamat" ).dialog( "close" );
						}
						else {
							alert(data);
						}
						$( "#detail-alamat" ).dialog( "close" );
						location.reload();
					},
					error : expPost
				});
            },
            "Batal": function() {
                $( this ).dialog( "close" );
                $( "#up-alamat" ).button({ disabled:true });
            }
        }
    });

    $( "#up-alamat" ).button({
        icons: {
            primary: "ui-icon-pencil"
        }
    })
    .click(function() {
		var url = $(this).data('url');
        $( "#detail-alamat" ).data('url',url).dialog( "open" );
    });

    //list-provinsi => kota
    $('select#provinsi').change(function(e){
        e.preventDefault();
        var postURL = $(this).attr('data-url');
        
        $.ajax({
            type: "POST",
            url: postURL,
            data: ({idp:$(this).val(),Hydra:$('input[name="Hydra"]').val()}),
            success: function(data){
                $('select#wilayah').empty().html(data);
                $('#kode_pos').css('border-color','red');
            },
            error : expPost
        });
    });
    
});
</script>
<?php
$bayar = $total->total_harga - $diskon_transaksi;
?>
<body class="i4nCMS">
	<div id="container" class="rounded" style="width:97%">
		<div id="mainContent" style="width:100%; margin: 0 auto;">
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box'); ?>
	<?php if($this->uri->segment(3) === 'detail' OR $this->uri->segment(3) === 'update_trx') : ?>
		<div id="stylized" class="myform ui-helper-clearfix" style="width:92%;">
		<?php  
		$attributes = array( 'id' => 'form');
		echo form_open($this->config->item('admpath').'/order/update_trx', $attributes);
		?>
			<input type="hidden" name="id_trx" id="id_trx" value="<?php echo set_value('id_trx',$order[0]->no_struk); ?>" />
			<input type="hidden" name="id_cust" id="id_cust" value="<?php echo set_value('id_cust',$order[0]->id_cust); ?>" />
			<input type="hidden" name="jml_trans_trx" id="jml_trans_trx" value="<?php echo set_value('jml_trans_trx',$total->jml_produk); ?>" />
			<input type="hidden" name="total_trx" id="total_trx" value="<?php echo set_value('total_trx',$bayar); ?>" />
			
			<div id="box-status-order">
			<label>Status Order<span class="small">Ubah status order </span> </label>
			<select id="status_order" name="status_order"  style="margin-right:30px;margin-left:-40px;">
				<option value="proses" <?php echo (set_value('status_order',$order[0]->status_orderan) == 'proses')?'selected="selected"':''; ?>>Proses</option>
				<option value="batal" <?php echo (set_value('status_order',$order[0]->status_orderan) == 'batal')?'selected="selected"':''; ?>>Batal</option>
			</select>
			</div>
			
			<div id="box-status-transaksi">
			<label>Status Transaksi<span class="small">Ubah status transaksi</span> </label>
			<select id="status_transaksi" name="status_transaksi"  style="margin-right:20px;margin-left:-5px;">
				<option value="pembayaran" <?php echo (set_value('status_transaksi',$order[0]->status_transaksi) == 'pembayaran')?'selected="selected"':''; ?>>Pembayaran</option>
				<option value="pending" <?php echo (set_value('status_transaksi',$order[0]->status_transaksi)  == 'pending')?'selected="selected"':''; ?>>Pending</option>
				<option value="pemaketan" <?php echo (set_value('status_transaksi',$order[0]->status_transaksi)  == 'pemaketan')?'selected="selected"':''; ?>>Pemaketan</option>
				<option value="pengiriman" <?php echo (set_value('status_transaksi',$order[0]->status_transaksi)  == 'pengiriman')?'selected="selected"':''; ?>>Pengiriman</option>
				<option value="sukses" <?php echo (set_value('status_transaksi',$order[0]->status_transaksi)  == 'sukses')?'selected="selected"':''; ?>>Sukses</option>
			</select>
			</div>

			<div id="box-noresi">
				<label>No Resi<span class="small">No Resi Pengiriman</span> </label>
				<input type="text" name="no_resi" id="no_resi" autocomplete="off" maxlength="30" <?php echo ($order[0]->status_transaksi  == 'pengiriman')?'readonly="readonly"':''; ?> value="<?php echo set_value('no_resi',$order[0]->no_resi_pengiriman); ?>" style="width:155px;margin-left:-40px;" />
				<div style="clear:left"></div>
				<?php echo form_error('no_resi'); ?>
			</div>
			
			<div style="clear:left"></div>
			<?php if ( set_value('status_order',$order[0]->status_orderan) === 'batal' ) : ?>
			<button id="hapus" name="hapus" value="hapus" type="submit" style="background:#A52A2A;margin-left:0">Hapus</button>
			<?php endif; ?>
			
			<button type="submit" style="float:right;">Update</button>

		<?php echo form_close(); ?>
		</div>

	<?php endif; ?>
	
		<?php echo  ($this->session->flashdata('info'))?'<div id="info-update" style="padding:5px;width:94%;margin:10px auto;border:1px solid #63EE00;background:#FFEF7F">' .$this->session->flashdata('info').'</div>':''; ?>

		<div id="tabel" style="min-width:890px; width:inherit; margin-left:10px"> 
		  <table id="rounded-corner" summary="Menu"  style="width:98%;font-size:10px">
			<thead>
			<tr>
				<th class="rounded-company" scope="col" width="80" style="font-size:11px">No Struk</th>
				<?php if($this->uri->segment(3) === 'detail_sukses'): ?>
				<th scope="col" width="105" style="font-size:11px">Tgl Sukses</th>
				<?php elseif($this->uri->segment(3) === 'detail_batal'): ?>
				<th scope="col" width="105" style="font-size:11px">Tgl Batal</th>
				<?php else: ?>
				<th scope="col" width="105" style="font-size:11px">Tgl Update</th>
				<?php endif; ?>
				<th scope="col" width="105" style="font-size:11px">Tgl Transaksi</th>
				<th scope="col" style="font-size:11px">A/N Pembeli</th>
				<th scope="col" width="110" style="font-size:11px">Produk</th>
				<th scope="col" width="20" style="font-size:11px">Metode</th>
				<th scope="col" width="80" style="font-size:11px">Bayar Via</th>
				<th scope="col" width="20" style="font-size:11px">Jml Pesan</th>
				<th scope="col" width="50" style="font-size:11px">Harga</th>
				<?php if($this->uri->segment(3) === 'detail'): ?>
				<th class="rounded-q4" scope="col" width="40" style="font-size:11px">Status Transaksi</th>
				<?php else: ?>
				<th class="rounded-q4"  scope="col" width="40" style="font-size:11px">Status Order</th>
				<?php endif; ?>
			</tr>
			</thead>

			<tbody>
			<?php
				if(count($order) > 0):
				$i=0;
				foreach ($order as $row): 
			?>
			<tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
				<td title="No Struk"><?php echo ($row->no_struk); ?></td>
				<td title="Tgl Update" align="left"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_update)); ?></td>
				<td title="Tgl Transaksi" align="left"><span style="font-size:10px"><?php echo date('d-m-Y H:i:s' ,strtotime($row->tgl_transaksi)); ?></span></td>
				<td title="Nama Pembeli" align="left"><?php echo ucfirst($this->orderan->getNamaPembeliByAlamatId($row->id_alamat)); ?></td>
				<td title="Detail Produk" align="left"><span style="font-size:10px">
				
				<div style="font-size:10px">
				<a class="detail-produk" href="<?php echo base_url($this->config->item('admpath').'/order/detail_produk/'.$row->id_prod); ?>">
				<?php echo $row->detail_produk; ?>
				</a>
				</div>
				</td>
				<td title="Metode Bayar" align="center"><?php echo ucfirst($row->metode_bayar); ?></td>
				<td title="Bayar Via" align="center"><span style="font-size:10px"><?php echo $row->bayar_melalui; ?></span></td>
				<td title="Jml Pesan" align="right"><?php echo $row->jml_pesan.' '.$this->config->item('satuan_produk'); ?></td>
				<td title="Harga Produk" align="right"><?php echo format_harga_indo($row->total_harga); ?></td>
				<?php if($this->uri->segment(3) === 'detail'): ?>
				<td align="center"><?php echo humanize($row->status_transaksi); ?></td>
				<?php else: ?>
				<td align="center"><?php echo humanize($row->status_orderan); ?></td>
				<?php endif; ?>
			  </tr>
			  <?php $i++; ?>
			  <?php endforeach; ?>

			  <?php if ($diskon_transaksi > 0): ?>
			  <tr style="font-weight:bold;background:#57BAD5">
				<td colspan="8">Sub Total</td>
				<td align="right">
					<strong><?php echo format_harga_indo($total->total_harga); ?></strong>
				</td>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <tr style="font-weight:bold;background:#B6F6B6">
				<td colspan="8">Diskon Transaksi</td>
				<td align="right">
					<strong><?php echo format_harga_indo($diskon_transaksi); ?></strong>
				</td>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <?php endif; ?>
			  
			  <tr style="font-weight:bold;background:#B1D3F4">
				<td colspan="8">Total <?php echo ($this->uri->segment(3) === 'detail')?'Pembayaran':'Transaksi'; ?></td>
				<td align="right">
					<strong><?php echo format_harga_indo($bayar); ?></strong>
				</td>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <?php else: ?>
			  <tr>
				<td colspan="10"><em>Data Kosong</em></td>
			</tr>
			<?php endif; ?>
			</tbody>

			<tfoot>
			<tr>
				<td colspan="9" class="rounded-foot-left"><em><?php echo ($this->uri->segment(3) === 'detail')?'List Pesanan '.$order[0]->no_struk:humanize($this->uri->segment(3)); ?></em></td>
				<td class="rounded-foot-right">&nbsp;</td>
			</tr>
			</tfoot>
		</table>
		</div>
		
		<div id="notes_cust" style="float:left;margin-left:10px; width:60%">
		<h6>Notes custommer :</h6>
		<?php echo word_wrap($order[$i-1]->notes_cust, 25); ?>
		</div>

		<div id="alamat-kirim" class="ui-helper-clearfix ui-widget ui-corner-all" style="margin-right:10px; margin-bottom:20px;font-size:12px;float:right;">
			<h2>Alamat Kirim : </h2>
			<?php if($order[0]->status_transaksi == 'pembayaran' || $order[0]->status_transaksi == 'pending' || $order[0]->status_transaksi == 'pemaketan') : ?>
			<button id="up-alamat" data-url="<?php echo site_url($this->config->item('admpath').'/order/ubah_alamat_kirim');?>">Ubah</button>
			<?php endif; ?>
			<?php foreach ($alamat as $isi) : ?>
			<p style="margin-top:2px;">
			<?php echo $isi; ?>
			</p>
			<?php endforeach; ?>
		</div>

		</div><!-- end #mainContent -->
	</div><!-- end #container -->

	<div id="detail-prod"></div>

<?php if($order[0]->status_transaksi == 'pembayaran' || $order[0]->status_transaksi == 'pending' || $order[0]->status_transaksi == 'pemaketan') : ?>
	<div id="detail-alamat" title="Ubah Alamat Kirim" style="display:none">
	<div id="stylized" class="myform" style="width:90%">
	<div id="proses_data" class="ui-widget-overlay" style="display:none">
		<img src="<?php echo base_url().'assets/icons/loaders/loader12.gif';?>" style="position:absolute;margin-top:25%" />
	</div>
	    <?php echo form_open('',array('id'=>'form-update-alamat')); ?>
	    <fieldset style="padding:8px; font-size:12px;" class="text ui-widget-content ui-corner-all">
			<input type="hidden" name="id_alamat" id="id_alamat" value="<?php echo $detail_alamat->id_alamat; ?>" />
			<input type="hidden" name="id_cust" id="id_cust" value="<?php echo $detail_alamat->id_cust; ?>" />
			<input type="hidden" name="trx" id="trx" value="<?php echo $order[0]->no_struk; ?>" />

			<input type="hidden" name="status_transaksi" value="<?php echo $order[0]->status_transaksi; ?>" />
			<input type="hidden" name="metode_bayar" value="<?php echo $order[0]->metode_bayar; ?>" />
			<input type="hidden" name="via" value="<?php echo $order[0]->bayar_melalui; ?>" />
			<input type="hidden" name="sub_total" value="<?php echo $total->total_harga; ?>" />
			<input type="hidden" name="diskon" value="<?php echo $diskon_transaksi; ?>" />
			<input type="hidden" name="total" value="<?php echo $bayar; ?>" />
			
	        <label for="nama">Nama Lengkap</label>
	        <input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="150" style="padding:5px;" class="text ui-widget-content ui-corner-all" value="<?php echo dekodeString($detail_alamat->nama_lengkap); ?>" />
	        <div style="clear:left"></div>
	        <label for="email_cust">Email</label>
	        <input type="text" name="email_cust" id="email_cust" maxlength="150" style="padding:5px;" class="text ui-widget-content ui-corner-all" value="<?php echo dekodeString($detail_alamat->email_cust); ?>" />
	        <div style="clear:left"></div>
	        <label for="nama">No Telpon</label>
	        <input type="text" name="no_telpon" id="no_telpon" maxlength="50" style="padding:5px;" class="text ui-widget-content ui-corner-all" value="<?php echo dekodeString($detail_alamat->no_telpon); ?>" />
	        <div style="clear:left"></div>
	        <label for="negara">Negara</label>
			<select id="negara" name="negara">
			    <option value="indonesia" <?php echo set_select('negara', 'indonesia', ($detail_alamat->id_negara == 'indonesia')?TRUE:FALSE); ?> >&nbsp;Indonesia&nbsp;</option>
			</select>

			<label style="margin-left:80px;margin-right:-67px" for="provinsi">Provinsi</label>
			<select id="provinsi" name="provinsi" data-url="<?php echo site_url('list_wilayah_admin'); ?>" style="width:200px">
				<?php
				$list_provinsi = $this->maddons->listProvinsi();
				foreach( $list_provinsi as $nama) {
				?>
				    <option value="<?php echo $nama->id_provinsi; ?>" <?php echo set_select('provinsi', $nama->id_provinsi, ($detail_alamat->id_provinsi == $nama->id_provinsi)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->provinsi); ?>&nbsp;</option>
				<?php
				}
				?>
			</select>
			<div style="clear:left"></div>
			<label for="wilayah">Wilayah</label>
			<select id="wilayah" name="wilayah" style="width:200px">
				<?php
				$list_wilayah = $this->maddons->listKotaById($detail_alamat->id_provinsi);
				foreach( $list_wilayah as $nama) {
				?>
					<option value="<?php echo $nama->id_kab_kota; ?>" <?php echo set_select('wilayah', $nama->id_kab_kota, ($detail_alamat->id_wilayah == $nama->id_kab_kota)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->kab_kota); ?>&nbsp;</option>
				<?php
				}
				?>
			</select>

	        <label style="margin-left:60px;margin-right:-70px" for="nama">Kodepos</label>
	        <input type="text" name="kode_pos" id="kode_pos" maxlength="50" style="padding:5px;width:100px" class="text ui-widget-content ui-corner-all" value="<?php echo dekodeString($detail_alamat->kode_pos); ?>" />
	        <div style="clear:left"></div>
	        <label for="nama">Alamat Lengkap</label>
	        <textarea name="alamat_rumah" id="alamat_rumah" style="padding:5px;height:80px" class="text ui-widget-content ui-corner-all"><?php echo str_ireplace("<br />", "\n", dekodeString($detail_alamat->alamat_rumah)); ?></textarea>
	        <br />
	    </fieldset>
	    </form>
	</div>
	</div>
<?php endif; ?>

</body>

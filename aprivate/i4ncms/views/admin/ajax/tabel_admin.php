<table id="rounded-corner" summary="Menu">
<thead>
  <tr align="center">
	<th class="rounded-company" scope="col">Nama Admin</th>
	<th scope="col">Email</th>
	<th scope="col" width="155">Login Terakhir</th>
	<th scope="col" width="80">Level</th>
	<th scope="col" width="60">Status</th>
	<th class="rounded-q4" scope="col" width="80">Opsi</th>
  </tr>
</thead>

<tbody>
  <?php if(count($admin) > 0): ?>
  <?php $i=0; ?>
  <?php foreach ($admin as $row): ?>
  <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
	<td><?php echo dekodeString($row->nama_lengkap); ?></td>
	<td><?php echo dekodeString($row->adm_email); ?></td>
	<td align="center"><?php echo $row->tgl_masuk > 0?date('d/m/Y h:i:s A', $row->tgl_masuk):'---'; ?></td>
	<td align="right">
		<?php
			if(bs_kode($row->level, TRUE) === 'superman')
			echo 'Super Admin';
			elseif(bs_kode($row->level, TRUE) === 'batman')
			echo 'Admin';
			else
			echo 'Entry Data';
		?>
	</td>
	<td align="center"><span id="status_<?php echo $row->id; ?>" class="text"><?php echo humanize(dekodeString($row->status)); ?></span>
	  <select id="status_input_<?php echo $row->id; ?>" class="editbox">
		<option value="on" <?php echo (dekodeString($row->status) == 'on')?'selected="selected"':''; ?>>On</option>
		<option value="off" <?php echo (dekodeString($row->status) == 'off')?'selected="selected"':''; ?>>Off</option>
	  </select>
	</td>
	<td align="center">
	  <a class="<?php echo (! $super_admin)?'dis_edit':'edit'; ?>" id="<?php echo (! $super_admin)?'xx':$row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
		<a class="<?php echo (! $super_admin)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo (! $super_admin)?'xx':$row->id; ?>" title="Hapus <?php echo (! $super_admin)?'xx':humanize(dekodeString($row->id_adm_uname)); ?>" href="#"></a>&nbsp;
	  <a class="<?php echo (! $super_admin)?'dis_atur':'atur'; ?>" href="<?php echo (! $super_admin)?'javascript:void(0)':site_url($this->config->item('admpath').'/admincms/detail/'.bs_kode($row->id).'/show'); ?>" title="Detail <?php echo (! $super_admin)?'xx':humanize(dekodeString($row->nama_lengkap)); ?>"></a>
	</td>
  </tr>
  <?php $i++; ?>
  <?php endforeach; ?>
  <?php else: ?>
  <tr>
	<td colspan="6"><em>Data Kosong</em></td>
  </tr>
  <?php endif; ?>
</tbody>
<tfoot>
  <tr>
	<td colspan="5" class="rounded-foot-left"><em>List Admin yang terdaftar</em></td>
	<td class="rounded-foot-right">&nbsp;</td>
  </tr>
</tfoot>
</table>

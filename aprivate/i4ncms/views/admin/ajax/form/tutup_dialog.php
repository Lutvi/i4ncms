<!-- Skrip JS buat tutup auto dialog form -->
<?php foreach($css as $item): ?>
<link href="<?php echo base_url().'assets/css/'.$item; ?>" rel="stylesheet" type="text/css" />
<?php endforeach; ?>
<?php foreach($js as $item): ?>
<script type="text/javascript" src="<?php echo base_url().'assets/js/'.$item; ?>"></script>
<?php endforeach; ?>

<script type="text/javascript">
i=parseInt(<?php echo $timeout/1000; ?>);
function hitungan()
{
	setInterval(function(){
	--i;
		if (i < 1) {
			$(function() {
				window.parent.$('#aturAppDialog').dialog('close'); //Auto Close Dialog Box.
				window.parent.$('#aturAppDialog').remove();
			});
		}
	document.getElementById("timer").innerHTML=i;
	},parseInt(<?php echo $timeout; ?>));
}

setTimeout(hitungan, 300);
</script>

<div class="ui-widget" style="width:99.5%">
	<div class="ui-state-highlight ui-corner-all" style="padding:0 .7em .7em .7em;">
		<h4><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Aksi sukses</h4>
		<?php
			// warning
			$info = highlight_phrase($info, "telah", '<span style="color:#1C66EF">', '</span>');
			$info = highlight_phrase($info, "sudah", '<span style="color:#FFD000">', '</span>');
			// error
			$info = highlight_phrase($info, "tidak bisa", '<span style="color:#D1181C">', '</span>');
			// ok
			$info = highlight_phrase($info, "Sukses", '<span style="color:#195ACB">', '</span>');
			$info = highlight_phrase($info, "menambahkan", '<span style="color:#195ACB">', '</span>');
			$info = highlight_phrase($info, "konten", '<span style="color:#195ACB">', '</span>');
			$info = highlight_phrase($info, "terpasang", '<span style="color:#195ACB">', '</span>');
			$info = highlight_phrase($info, "update", '<span style="color:#195ACB">', '</span>');
			$info = highlight_phrase($info, "memperbaharui", '<span style="color:#195ACB">', '</span>');
			$info = highlight_phrase($info, "data", '<span style="color:#195ACB">', '</span>');
		?>
		<?php echo $info; ?><span>, Silahkan tutup untuk melihat list.</span>
		<br />
		atau silahkan menunggu....
		<br />
		Dialog ini akan ditutup otomatis dalam <span id="timer"><?php echo $timeout/1000; ?></span> detik.
	</div>
</div>

<!DOCTYPE html>
<html>
	<head>
	<title>Update Halaman <?php echo humanize(set_value('nama_ori_id', $halaman->label_id)); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-autocomplete {
	    max-height: 170px;
	    overflow-y: auto;
	    /* prevent horizontal scrollbar */
	    overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
	    height: 170px;
	}
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	.tambah-keyword { padding:6px 4px; width:16px; float:left; margin-top:3px; margin-left:6px; }
	</style>

	<script>
	var sts = '<?php echo $sts; ?>';
	var tipe_halaman = '<?php echo $tipe_halaman; ?>';
	var keywordIdURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_id'; ?>';
	var keywordEnURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_en'; ?>';
	
	var katProduk = <?php echo (!empty($halaman->kategori) && $halaman->tipe == 'produk')?"[".$halaman->kategori."]":"''"; ?>;
	var katBlog = <?php echo (!empty($halaman->kategori) && $halaman->tipe == 'blog')?"[".$halaman->kategori."]":"''"; ?>;
	var katAlbum = <?php echo (!empty($halaman->kategori) && $halaman->tipe == 'album')?"[".$halaman->kategori."]":"''"; ?>;
	var katVideo = <?php echo (!empty($halaman->kategori) && $halaman->tipe == 'video')?"[".$halaman->kategori."]":"''"; ?>;
	
	var maxKategori = <?php echo $this->config->item('max_kategori'); ?>;
	var produkURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/produk'); ?>';
	var blogURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/blog'); ?>';
	var albumURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/album'); ?>';
	var videoURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/video'); ?>';
	</script>
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>

	function setTipeHalaman(tipe)
	{
		  var tipe_album = $('#tipe_album').val(),
			  tipe_video = $('#tipe_video').val(),
			  tipe_prod = $('#tipe_prod').val();
	      
	      if(tipe == 'module'){
	        $('#listModule, #seoHalaman').show();
	        $('.listProduk, .listBlog, .listAlbum, .listVideo, #htmlHalaman').hide();
	      }
	      else if(tipe == 'blog'){
	        $('.listBlog, #seoHalaman').show();
	        $('.listAlbum, .listProduk, .listVideo, #listModule, #htmlHalaman').hide();
	      }
	      else if(tipe == 'album'){
	        $('.listAlbum, #seoHalaman, #htmlHalaman').show();
	        $('.listProduk, .listBlog, .listVideo, #listModule').hide();
	
	        if( tipe_album == 'by_id' )
	        {
	            $('#box-induk-album').show();
	            $('#box-kategori-album').hide();
	        }
	        else{
	            $('#box-induk-album').hide();
	            $('#box-kategori-album').show();
	        }
	      }
	      else if(tipe == 'video'){
	        $('.listVideo, #seoHalaman, #htmlHalaman').show();
	        $('.listBlog, .listAlbum, .listProduk, #listModule').hide();
	        
	        if( tipe_video == 'by_id' )
	        {
	            $('#box-induk-video').show();
	            $('#box-kategori-video').hide();
	        }
	        else{
	            $('#box-induk-video').hide();
	            $('#box-kategori-video').show();
	        }
	      }
	      else if(tipe == 'produk'){
	        $('.listProduk, #seoHalaman').show();
	        $('.listBlog, .listAlbum, .listVideo, #listModule, #htmlHalaman').hide();
	
	        if( tipe_prod == 'by_id' )
	        {
	            $('#box-induk').show();
	            $('#box-kategori').hide();
	        }
	        else{
	            $('#box-induk').hide();
	            $('#box-kategori').show();
	        }
	      }
	      else if(tipe == 'halaman'){
			$('#seoHalaman, #htmlHalaman').show();
	        $('#listModule, .listBlog, .listAlbum, .listProduk, .listVideo').hide();
	      }
	      else{
	        $('#listModule, .listBlog, .listAlbum, .listProduk, .listVideo, #seoHalaman, #htmlHalaman').hide();
	      }
	}
	
	$(function(){
	
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function split( val ) {
				return val.split( /,\s*/ );
	    }
	    function extractLast( term ) {
	        return split( term ).pop();
	    }
	
	    // add keyword
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 270,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Keyword": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Keyword Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Keyword English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-keyword").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan keyword baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan keyword baru." );
	        }
	    });
	
	    $( ".tambah-keyword" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	    // end add keyword
	    
	    //keyword id halaman
	    $( "#keyword_id" )
	    .bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	                $( this ).data( "autocomplete" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        source: function( request, response ) {
	            $.getJSON( keywordIdURL, {
	                term: extractLast( request.term )
	            }, response );
	        },
	        search: function() {
	            var term = extractLast( this.value );
	            if ( term.length < 2 ) {
	                return false;
	            }
	        },
	        focus: function() {
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            terms.pop();
	            terms.push( ui.item.value );
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	
	    //keyword en halaman
	    $( "#keyword_en" )
	    .bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	                $( this ).data( "autocomplete" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        source: function( request, response ) {
	            $.getJSON( keywordEnURL, {
	                term: extractLast( request.term )
	            }, response );
	        },
	        search: function() {
	            var term = extractLast( this.value );
	            if ( term.length < 2 ) {
	                return false;
	            }
	        },
	        focus: function() {
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            terms.pop();
	            terms.push( ui.item.value );
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	
	
	
	    $( "#tabs-seo, #tabs-isi" ).tabs();
	});
	
	$(document).ready(function(){
	
		var tipe = $('#tipe').val();
	
		// set tipe
	    setTipeHalaman(tipe);
	
	    //-------------------------------
		// #kategori_produk
		//-------------------------------
		var kategori = $('#kategori').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: produkURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori'
		});

		if(katProduk !== '')
		{
			$(kategori).on('load', function(){
				if(this._dataSet === undefined){
					this._dataSet = true;
					kategori.setValue(katProduk);
				}
			});
	    }
	
	    //-------------------------------
		// #kategori_blog
		//-------------------------------
		var kategori_blog = $('#kategori_blog').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: blogURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori_blog'
		});
	
		if(katBlog !== '')
		{
			$(kategori_blog).on('load', function(){
				if(this._dataSet === undefined){
					this._dataSet = true;
					kategori_blog.setValue(katBlog);
				}
			});
	    }
	
	    //-------------------------------
		// #kategori_album
		//-------------------------------
		var kategori_album = $('#kategori_album').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: albumURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori_album'
		});
	
		if(katAlbum !== '')
		{
			$(kategori_album).on('load', function(){
				if(this._dataSet === undefined){
					this._dataSet = true;
					kategori_album.setValue(katAlbum);
				}
			});
	    }
	
		//-------------------------------
		// #kategori_video
		//-------------------------------
		var kategori_video = $('#kategori_video').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: videoURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori_video'
		});
	
		if(katVideo !== '')
		{
			$(kategori_video).on('load', function(){
				if(this._dataSet === undefined){
					this._dataSet = true;
					kategori_video.setValue(katVideo);
				}
			});
	    }
	
	
		if(sts == 'error_module'){
	      $('#listModule, #seoHalaman').show();
	    }
	    else if(sts == 'error_blog'){
	      $('.listBlog, #seoHalaman').show();
	    }
	    else if(sts == 'error_album'){
	      $('.listAlbum, #seoHalaman, #htmlHalaman').show();
	    }
	    else if(sts == 'error_video'){
	      $('.listVideo, #seoHalaman, #htmlHalaman').show();
	    }
	    else if(sts == 'error_produk'){
	      $('.listProduk, #seoHalaman').show();
	    }
	    else if(sts == 'error_halaman'){
	      $('#htmlHalaman, #seoHalaman').show();
	    }
	    else{
		  
	    }
	
	
		// select tipe halaman
		$("#tipe, #tipe_prod,#tipe_album,#tipe_video").change(function () {
		  var tipe = $('#tipe').val();
	      setTipeHalaman(tipe);
	      $('.error').hide();
	    });
	
	    //scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 150) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 1j(a){g b=$(\'#1v\').j(),T=$(\'#T\').j(),U=$(\'#U\').j();5(a==\'2b\'){$(\'#w, #l\').8();$(\'.x, .y, .B, .C, #r\').k()}6 5(a==\'2c\'){$(\'.y, #l\').8();$(\'.B, .x, .C, #w, #r\').k()}6 5(a==\'P\'){$(\'.B, #l, #r\').8();$(\'.x, .y, .C, #w\').k();5(b==\'1k\'){$(\'#q-M-P\').8();$(\'#q-D-P\').k()}6{$(\'#q-M-P\').k();$(\'#q-D-P\').8()}}6 5(a==\'Q\'){$(\'.C, #l, #r\').8();$(\'.y, .B, .x, #w\').k();5(T==\'1k\'){$(\'#q-M-Q\').8();$(\'#q-D-Q\').k()}6{$(\'#q-M-Q\').k();$(\'#q-D-Q\').8()}}6 5(a==\'2d\'){$(\'.x, #l\').8();$(\'.y, .B, .C, #w, #r\').k();5(U==\'1k\'){$(\'#q-M\').8();$(\'#q-D\').k()}6{$(\'#q-M\').k();$(\'#q-D\').8()}}6 5(a==\'2e\'){$(\'#l, #r\').8();$(\'#w, .y, .B, .x, .C\').k()}6{$(\'#w, .y, .B, .x, .C, #l, #r\').k()}}$(4(){g d=$(\'#1w\');g i=$(\'#1w p\').2f()+1;g e=$("#2g"),V=$("#2h"),W=$([]).1x(e).1x(V),1l=$(".2i");4 1y(){2j.2k()}4 N(t){1l.2l(t).1m("u-J-1z");2m(4(){1l.X("u-J-1z",2n)},2o)}4 1n(o,n,a,b){5(o.j().Y>b||o.j().Y<a){o.1m("u-J-K");N("2p "+n+" 2q 2r 2s "+a+" 1A 2t 2u "+b+" 1A.");m h}6{m E}}4 1o(o,a,n){5(!(a.2v(o.j()))){o.1m("u-J-K");N(n);m h}6{m E}}4 R(a){m a.R(/,\\s*/)}4 S(a){m R(a).1p()}$("#L-10").L({2w:h,2x:2y,2z:2A,2B:h,2C:{2D:"1B 1C",2E:"1B 1C",2F:1D},2G:E,2H:{"2I 1q":4(){g b=E;W.X("u-J-K");b=b&&1n(e,"1q 2J",3,1E);b=b&&1o(e,/^([0-9 a-z A-Z])+$/,"1F 1G 1H 1I 1J : a-z 0-9");b=b&&1n(V,"1q 2K",3,1E);b=b&&1o(V,/^([0-9 a-z A-Z])+$/,"1F 1G 1H 1I 1J : a-z 0-9");5(b){$.2L({2M:"2N",11:$(7).v(\'11\'),v:$("#10-1K-12").2O(),2P:4(a){5(a.1L===\'2Q\'){$("#L-10").L("1r")}6{N(a.1L)}},2R:\'2S\',K:1y})}},"2T":4(){$(7).L("1r");W.j("").X("u-J-K");N("1M 12 1N.")}},1r:4(){W.j("").X("u-J-K");N("1M 12 1N.")}});$(".1K-12").2U({2V:{2W:"u-2X-2Y-2Z"}}).1O(4(){g a=$(7).v(\'11\');$("#L-10").v(\'11\',a).L("30")});$("#31").1P("1Q",4(a){5(a.13===$.u.13.1R&&$(7).v("14").1S.1T){a.1U()}}).14({1V:4(a,b){$.1W(32,{15:S(a.15)},b)},1X:4(){g a=S(7.F);5(a.Y<2){m h}},1Y:4(){m h},1Z:4(a,b){g c=R(7.F);c.1p();c.16(b.20.F);c.16("");7.F=c.21(", ");m h}});$("#33").1P("1Q",4(a){5(a.13===$.u.13.1R&&$(7).v("14").1S.1T){a.1U()}}).14({1V:4(a,b){$.1W(34,{15:S(a.15)},b)},1X:4(){g a=S(7.F);5(a.Y<2){m h}},1Y:4(){m h},1Z:4(a,b){g c=R(7.F);c.1p();c.16(b.20.F);c.16("");7.F=c.21(", ");m h}});$("#1s-35, #1s-36").1s()});$(22).37(4(){g b=$(\'#1t\').j();1j(b);g c=$(\'#D\').17({18:19,1a:{G:$("1b[H=G]").j()},v:38,1c:h,1d:1e,H:\'D\'});5(23!==\'\'){$(c).1f(\'1g\',4(){5(7.I===1h){7.I=E;c.1i(23)}})}g d=$(\'#24\').17({18:19,1a:{G:$("1b[H=G]").j()},v:39,1c:h,1d:1e,H:\'24\'});5(25!==\'\'){$(d).1f(\'1g\',4(){5(7.I===1h){7.I=E;d.1i(25)}})}g e=$(\'#26\').17({18:19,1a:{G:$("1b[H=G]").j()},v:3a,1c:h,1d:1e,H:\'26\'});5(27!==\'\'){$(e).1f(\'1g\',4(){5(7.I===1h){7.I=E;e.1i(27)}})}g f=$(\'#28\').17({18:19,1a:{G:$("1b[H=G]").j()},v:3b,1c:h,1d:1e,H:\'28\'});5(29!==\'\'){$(f).1f(\'1g\',4(){5(7.I===1h){7.I=E;f.1i(29)}})}5(O==\'3c\'){$(\'#w, #l\').8()}6 5(O==\'3d\'){$(\'.y, #l\').8()}6 5(O==\'3e\'){$(\'.B, #l, #r\').8()}6 5(O==\'3f\'){$(\'.C, #l, #r\').8()}6 5(O==\'3g\'){$(\'.x, #l\').8()}6 5(O==\'3h\'){$(\'#r, #l\').8()}6{}$("#1t, #U,#1v,#T").3i(4(){g a=$(\'#1t\').j();1j(a);$(\'.K\').k()});$(1D).3j(4(){5($(22).2a()>3k){$(\'.1u\').3l()}6{$(\'.1u\').3m()}});$(\'.1u\').1O(4(){$("3n, 3o").3p({2a:0},3q);m h})});',62,213,'||||function|if|else|this|show||||||||var|false||val|hide|seoHalaman|return||||box|htmlHalaman|||ui|data|listModule|listProduk|listBlog|||listAlbum|listVideo|kategori|true|value|Hydra|name|_dataSet|state|error|dialog|induk|updateTips|sts|album|video|split|extractLast|tipe_video|tipe_prod|namaen|allFields|removeClass|length||form|url|keyword|keyCode|autocomplete|term|push|magicSuggest|maxSelection|maxKategori|dataUrlParams|input|allowFreeEntries|maxDropHeight|200|on|load|undefined|setValue|setTipeHalaman|by_id|tips|addClass|checkLength|checkRegexp|pop|Keyword|close|tabs|tipe|scrollup|tipe_album|addinput|add|expPost|highlight|karakter|center|top|window|50|Karakter|yang|di|perbolehkan|hanya|tambah|status|Masukkan|baru|click|bind|keydown|TAB|menu|active|preventDefault|source|getJSON|search|focus|select|item|join|document|katProduk|kategori_blog|katBlog|kategori_album|katAlbum|kategori_video|katVideo|scrollTop|module|blog|produk|halaman|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|270|width|350|resizable|position|my|at|of|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|dataType|json|Batal|button|icons|primary|icon|circle|plus|open|keyword_id|keywordIdURL|keyword_en|keywordEnURL|seo|isi|ready|produkURL|blogURL|albumURL|videoURL|error_module|error_blog|error_album|error_video|error_produk|error_halaman|change|scroll|150|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open($this->config->item('admpath').'/halaman/update_formhalaman', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Update</button>
	    <h1>Update halaman <?php echo humanize(set_value('nama_ori_id', $halaman->label_id)); ?></h1>
	    <input type="hidden" name="id_halaman" id="id_halaman" value="<?php echo set_value('id_halaman', $halaman->id_halaman); ?>" />
	    <input type="hidden" name="nama_ori_id" id="nama_ori_id" value="<?php echo set_value('nama_ori_id', $halaman->label_id); ?>" />
	    <input type="hidden" name="nama_ori_en" id="nama_ori_en" value="<?php echo set_value('nama_ori_en', $halaman->label_en); ?>" />
	    <p>Update data Halaman.</p>
	
	    <label>Label ID <span class="small">Nama halaman Indonesia</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id', humanize($halaman->label_id)); ?>" />
	
	    <label style="margin-right:-30px;">Label EN <span class="small">Nama halaman English</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en', humanize($halaman->label_en)); ?>" />
	    <div style="clear:both"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>

	    <label>Tipe Halaman<span class="small">Pilih tipe halaman.</span> </label>
	    <select id="tipe" name="tipe" style="margin-right:40px;">
	        <option value="halaman" <?php echo set_select('tipe', 'halaman',($halaman->tipe == 'halaman')?TRUE:FALSE); ?>>&nbsp;&nbsp;Halaman&nbsp;&nbsp;</option>
	        <option value="blog" <?php echo set_select('tipe', 'blog',($halaman->tipe == 'blog')?TRUE:FALSE); ?>>&nbsp;&nbsp;Blog&nbsp;&nbsp;</option>
	        <option value="album" <?php echo set_select('tipe', 'album',($halaman->tipe == 'album')?TRUE:FALSE); ?>>&nbsp;&nbsp;Album&nbsp;&nbsp;</option>
	        <option value="video" <?php echo set_select('tipe', 'video',($halaman->tipe == 'video')?TRUE:FALSE); ?>>&nbsp;&nbsp;Video&nbsp;&nbsp;</option>
	        <option value="produk" <?php echo set_select('tipe', 'produk',($halaman->tipe == 'produk')?TRUE:FALSE); ?>>&nbsp;&nbsp;Produk&nbsp;&nbsp;</option>
	        <option value="module" <?php echo set_select('tipe', 'module',($halaman->tipe == 'module')?TRUE:FALSE); ?>>&nbsp;&nbsp;Module&nbsp;&nbsp;</option>
	    </select>
	    
	    <div class="listProduk" style="display:none;">
	        <label>Tipe Produk<span class="small">Pilih tipe berdasarkan.</span> </label>
	        <select id="tipe_prod" name="tipe_prod" style="margin-left:-20px;">
	            <option value="by_id" <?php echo set_select('tipe_prod', 'by_id',(!empty($halaman->id_produk))?TRUE:FALSE); ?>>&nbsp;&nbsp;Produk Induk&nbsp;&nbsp;</option>
	            <option value="by_kategori" <?php echo set_select('tipe_prod', 'by_kategori',(!empty($halaman->kategori))?TRUE:FALSE); ?>>&nbsp;&nbsp;Kategori Produk&nbsp;&nbsp;</option>
	        </select>
	    </div>
	
	    <div class="listAlbum" style="display:none;">
	        <label style="margin-right:-25px;">Tipe Album<span class="small">Pilih Album berdasarkan.</span> </label>
	        <select id="tipe_album" name="tipe_album">
	            <option value="by_id" <?php echo set_select('tipe_album', 'by_id',(!empty($halaman->id_album))?TRUE:FALSE); ?>>&nbsp;&nbsp;Album Id&nbsp;&nbsp;</option>
	            <option value="by_kategori" <?php echo set_select('tipe_album', 'by_kategori',(!empty($halaman->kategori)  && $halaman->tipe == 'album')?TRUE:FALSE); ?>>&nbsp;&nbsp;Kategori Album&nbsp;&nbsp;</option>
	        </select>
	    </div>
	
	    <div class="listVideo" style="display:none;">
	        <label style="margin-right:-25px;">Tipe Video<span class="small">Pilih Video berdasarkan.</span> </label>
	        <select id="tipe_video" name="tipe_video">
	            <option value="by_id" <?php echo set_select('tipe_video', 'by_id',(!empty($halaman->id_video))?TRUE:FALSE); ?>>&nbsp;&nbsp;Video Id&nbsp;&nbsp;</option>
	            <option value="by_kategori" <?php echo set_select('tipe_video', 'by_kategori',(!empty($halaman->kategori)  && $halaman->tipe == 'video')?TRUE:FALSE); ?>>&nbsp;&nbsp;Kategori Video&nbsp;&nbsp;</option>
	        </select>
	    </div>
	    
	    <div style="clear:both"></div>
	    <?php echo form_error('tipe'); ?>
	    <?php echo form_error('tipe_prod'); ?>
	    <?php echo form_error('tipe_album'); ?>
	    <?php echo form_error('tipe_video'); ?>
	    
	    <div id="listModule" style="display:none;">
	    <label>Module <span class="small">Pilih module.</span> </label>
	    <select id="module_id" name="module_id">
			<?php foreach ($aktif_mod as $row): ?>
	        <option value="<?php echo $row['id_module']; ?>" <?php echo ($halaman->id_module == $row['id_module'])?'selected="selected"':''; ?>>&nbsp;&nbsp;<?php echo $row['nama_module']; ?>&nbsp;&nbsp;</option>
	        <?php endforeach; ?>>
	    </select>
	    <?php echo form_error('module_id'); ?>
	    <div style="clear:left"></div>
	    </div>
	
	    <div class="listBlog" style="display:none;">
			<br />
			<label>Kategori Blog<span class="small">Ketik min. 2 karakter.<br>(autocomplete)</span> </label>
			<input type="text" name="kategori_blog" id="kategori_blog" style="width:600px;margin-left:150px;min-height:50px" />
			<br />
	        <div style="clear:both"></div>
	        <?php echo form_error('kategori_blog'); ?>
	    </div>
	
	    <div class="listAlbum" style="display:none;">
	        <div id="box-induk-album">
	            <label>Album<span class="small">Pilih Album.</span> </label>
	            <select id="id_album" name="id_album">
	            <?php if(!empty($aktif_album)): ?>
	                <?php foreach ($aktif_album as $row): ?>
	                <option value="<?php echo $row['id']; ?>" <?php echo ($halaman->id_album === $row['id'])?'selected="selected"':''; ?>>&nbsp;&nbsp;<?php echo $row['nama_id']; ?>&nbsp;|<?php echo $row['nama_en']; ?>&nbsp;&nbsp;</option>
	                <?php endforeach; ?>
	            <?php endif; ?>
	            </select>
	        </div>
	        <div id="box-kategori-album" style="display:none;">
				<br />
	            <label>Kategori Album<span class="small">Ketik min. 2 karakter.<br>(autocomplete)</span> </label>
	            <input type="text" name="kategori_album" id="kategori_album" style="width:600px;margin-left:150px;min-height:50px" />
	            <br />
	        </div>
	        <div style="clear:both"></div>
	        <?php echo form_error('id_album'); ?>
	        <?php echo form_error('kategori_album'); ?>
	    </div>
	
	    <div class="listVideo" style="display:none;">
	        <div id="box-induk-video">
	            <label>Video<span class="small">Pilih Video.</span> </label>
	            <select id="id_video" name="id_video">
	            <?php if(!empty($aktif_video)): ?>
	                <?php foreach ($aktif_video as $row): ?>
	                <option value="<?php echo $row['id']; ?>" <?php echo ($halaman->id_video === $row['id'])?'selected="selected"':''; ?>>&nbsp;&nbsp;<?php echo $row['nama_id']; ?>&nbsp;|<?php echo $row['nama_en']; ?>&nbsp;&nbsp;</option>
	                <?php endforeach; ?>
	            <?php endif; ?>
	            </select>
	        </div>
	        <div id="box-kategori-video" style="display:none;">
				<br />
	            <label>Kategori Video<span class="small">Ketik min. 2 karakter.<br>(autocomplete)</span> </label>
	            <input type="text" name="kategori_video" id="kategori_video" style="width:600px;margin-left:150px;min-height:50px" />
	            <br />
	        </div>
	        <div style="clear:both"></div>
	        <?php echo form_error('id_video'); ?>
	        <?php echo form_error('kategori_video'); ?>
	    </div>
	
	    <div class="listProduk" style="display:none;">
	        <div id="box-induk">
	            <label>Produk<span class="small">Pilih produk induk.</span> </label>
	            <select id="id_produk" name="id_produk">
	            <?php if(!empty($aktif_prod)): ?>
	                <?php foreach ($aktif_prod as $row): ?>
	                <option value="<?php echo $row['id_prod']; ?>" <?php echo set_select('id_produk', $row['id_prod'],($halaman->id_produk === $row['id_prod'])?TRUE:FALSE); ?>>&nbsp;&nbsp;<?php echo $row['nama_prod']; ?>&nbsp;&nbsp;</option>
	                <?php endforeach; ?>
	            <?php endif; ?>
	            </select>
	        </div>
	        <div id="box-kategori" style="display:none;">
				<br />
	            <label>Kategori Produk<span class="small">Ketik min. 2 karakter.<br>(autocomplete)</span> </label>
	            <input type="text" name="kategori" id="kategori" style="width:600px;margin-left:150px;min-height:50px" />
	            <br />
	        </div>
	        <br /><br />
	        <?php echo form_error('id_produk'); ?>
	        <?php echo form_error('kategori'); ?>
	    </div>
	
	    <div style="clear:left"></div>
	    <div id="seoHalaman">
	    <label>SEO <span class="small">SEO Halaman</span> </label>
	    <div style="float:left;width:60%;">
			<div id="tabs-seo" style="height:200px">
	            <ul>
	                <li><a href="#seo-id">SEO Indonesia</a></li>
	                <li><a href="#seo-en">SEO English</a></li>
	            </ul>
	            <div id="seo-id">
	                <div class="">
						<label>Keyword ID <span class="small">Keyword Indonesia</span> </label>
	                    <input type="text" name="keyword_id" id="keyword_id" maxlength="200" value="<?php echo set_value('keyword_id',(!empty($halaman->seo_keyword_id))?$halaman->seo_keyword_id.' ,':''); ?>" />
	                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
	                    <label>Description ID <span class="small">Description Indonesia</span> </label>
	                    <textarea name="description_id" id="description_id" maxlength="250" style="height:80px;"><?php echo set_value('description_id',$halaman->seo_description_id); ?></textarea>
	                </div>
	            </div>
	            <div id="seo-en">     
	                 <div class="">
						<label>Keyword EN <span class="small">Keyword English</span> </label>
	                    <input type="text" name="keyword_en" id="keyword_en" maxlength="200" value="<?php echo set_value('keyword_en',(!empty($halaman->seo_keyword_en))?$halaman->seo_keyword_en.' ,':''); ?>" />
	                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
	                    <label>Description EN<span class="small">Description English</span> </label>
	                    <textarea name="description_en" id="description_en" maxlength="250" style="height:80px;"><?php echo set_value('description_en',$halaman->seo_description_en); ?></textarea>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div style="clear:both"></div>
	    <?php echo form_error('keyword_id'); ?>
	    <?php echo form_error('description_id'); ?>
	    <?php echo form_error('keyword_en'); ?>
	    <?php echo form_error('description_en'); ?>
	    </div>
	    <div style="clear:both"></div>
	    
	    <div id="htmlHalaman" style="display:none;">
				<div style="clear:left"></div>
				<label>Isi <span class="small">Isi Halaman</span></label>
				<div style="clear:left"></div>
				<?php echo form_error('isi_id'); ?>
				<?php echo form_error('isi_en'); ?>
			<div id="tabs-isi">
	            <ul>
	                <li><a href="#isi-id">Isi Indonesia</a></li>
	                <li><a href="#isi-en">Isi English</a></li>
	            </ul>
	            <div id="isi-id">
					<textarea name="isi_id" id="isi_id"><?php echo htmlspecialchars_decode(set_value('isi_id',$halaman->isi_id)); ?></textarea>
					<?php echo display_ckeditor($isi_id);?>
				</div>
				<div id="isi-en">
					<textarea name="isi_en" id="isi_en"><?php echo htmlspecialchars_decode(set_value('isi_en',$halaman->isi_en)); ?></textarea>
					<?php echo display_ckeditor($isi_en);?>
				</div>
			</div>
	    </div>
	    
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	</body>
</html>

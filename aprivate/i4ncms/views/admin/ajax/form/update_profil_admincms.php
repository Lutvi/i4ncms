<!DOCTYPE html>
<html>
	<head>
	<title>Update Profil</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		$('#status_ganti').val('no');
		$('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(1(){$(\'#2\').0(\'3\');$(\'#e\').4(1(){5 a=$(\'#6\').0();$(\'#7\').8();$(\'#9\').b();c(a!=""){$(\'#2\').0(\'d\')}});$(\'#6\').f(1(){5 a=$(g).0();c(a!=""){$(\'#2\').0(\'d\')}});$(\'#h\').4(1(){$(\'#7\').b();$(\'#9\').8();$(\'#2\').0(\'3\')})});',18,18,'val|function|status_ganti|no|click|var|userfile|c_gmbr|hide|up_gmbr||show|if|yes|ganti_gmbr|change|this|batal_ganti_gmbr'.split('|'),0,{}))
	</script>
	<?php endif; ?>
	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myform">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/admincms/updateform_profil_admin', $attributes);
	    ?>
	    <button type="submit" class="btn-aksi">Update</button>
	    <h1>Update Profil <?php echo set_value('nama_lengkap_ori',dekodeString($admin->$col_nama)); ?></h1>
	    <p>Ubah data Profil Anda</p>

	    <?php
			if (ENVIRONMENT == 'development')
			echo form_error('id_admin');
	    ?>

	    <input type="hidden" name="id_admin" id="id_admin" value="<?php echo set_value('id_admin',bs_kode($admin->$col_id)); ?>" />
	    <input type="hidden" name="nama_lengkap_ori" id="nama_lengkap_ori" value="<?php echo set_value('nama_lengkap_ori',dekodeString($admin->$col_nama)); ?>" />
	    <input type="hidden" name="adm_id_ori" id="adm_id_ori" value="<?php echo set_value('adm_id_ori',dekodeString($admin->$col_uname)); ?>" />
	    <input type="hidden" name="adm_kunci_ori" id="adm_kunci_ori" value="<?php echo set_value('adm_kunci_ori',dekodeString($admin->$col_pass)); ?>" />
	    <input type="hidden" name="adm_email_ori" id="adm_email_ori" value="<?php echo set_value('adm_email_ori',dekodeString($admin->$col_email)); ?>" />

	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="gambar" id="gambar" value="<?php echo set_value('gambar',$admin->$col_foto); ?>" />

		<input type="hidden" name="level" id="level" value="<?php echo set_value('level',$this->session->userdata('level')); ?>" />
		<input type="hidden" name="profil" id="profil" value="<?php echo set_value('profil',dekodeString($this->session->userdata('admemail'))); ?>" />
		<?php echo form_error('level'); ?>

		
		<div id="c_gmbr" style="height:80px;">
	    <button type="button" id="ganti_gmbr" class="ganti">Ganti Foto</button>
	    <label>Foto Profil<span class="small">Foto yang terpasang</span> </label>
	    <img src="<?php echo base_url(); ?>_media/admincms/thumb/thumb_<?php echo $admin->$col_foto; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
	    <div style="clear:left"></div>
	    </div>

	    <div id="up_gmbr" style="display:none;">
	    <button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Foto</button>
	    <label>Foto Profil<span class="small">ideal:256x256(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" id="userfile" style="width:100px" />
	    <br />
		<?php echo form_error('userfile'); ?>
	    <div style="clear:left"></div>
	    </div>

		<br />
	    <label>Nama Lengkap <span class="small">Nama lengkap admin</span> </label>
	    <input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_lengkap',dekodeString($admin->$col_nama)); ?>" />

	    <label>Email <span class="small">Email Admin</span> </label>
	    <input type="text" name="adm_email" id="adm_email" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('adm_email',dekodeString($admin->$col_email)); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_lengkap'); ?>
	    <?php echo form_error('adm_email'); ?>

	    <label>ID <span class="small">ID Login</span> </label>
	    <input type="text" name="adm_id" id="adm_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('adm_id',dekodeString($admin->$col_uname)); ?>" />

	    <label>Kunci<span class="small">Kunci Login</span> </label>
	    <input type="password" name="adm_kunci" id="adm_kunci" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('adm_kunci',dekodeString($admin->$col_pass)); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('adm_id'); ?>
	    <?php echo form_error('adm_kunci'); ?>

	    <label>Biografi<span class="small">Biografy singkat admin</span> </label>
		<textarea name="biografi" id="biografi" maxlength="300"><?php echo set_value('biografi',bs_kode($admin->$col_biografi, true)); ?></textarea>
		<div style="clear:left"></div>
		<?php echo form_error('biografi'); ?>

	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	</body>
</html>

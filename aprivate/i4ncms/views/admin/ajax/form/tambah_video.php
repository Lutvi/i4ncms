<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Video</title>

	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	
	<script>
	var kategoriVideoSelect = '<?php echo base_url($this->config->item('admpath').'/video/list_select_kategori'); ?>';
	var maxTags = <?php echo $this->config->item('max_tag'); ?>;
	var tagURL = '<?php echo base_url($this->config->item('admpath').'/album/list_tag/video'); ?>';
	</script>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 250,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Kategori": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori_video").empty().load(kategoriVideoSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	
	    $( ".tambah-kategori" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-isi" ).tabs();
	
	    //-------------------------------
		// Tags
		//-------------------------------
		var tag = $('#tag').magicSuggest({
			maxSelection: maxTags,
			dataUrlParams: {Hydra:$("input[name='Hydra']").val()},
			data: tagURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'tag'
		});
	
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 30) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
		
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(2(){7 c=$(\'#H\');7 i=$(\'#H p\').15()+1;7 d=$("#16"),k=$("#17"),l=$([]).I(d).I(k),x=$(".18");2 J(){19.1a()}2 8(t){x.1b(t).y("4-5-K");1c(2(){x.m("4-5-K",1d)},1e)}2 B(o,n,a,b){q(o.f().L>b||o.f().L<a){o.y("4-5-g");8("1f "+n+" 1g 1h 1i "+a+" M 1j 1k "+b+" M.");r h}C{r s}}2 D(o,a,n){q(!(a.1l(o.f()))){o.y("4-5-g");8(n);r h}C{r s}}$("#6-u").6({1m:h,1n:1o,1p:1q,1r:h,1s:{1t:"N O",1u:"N O",1v:1w},1x:s,1y:{"1z E":2(){7 b=s;l.m("4-5-g");b=b&&B(d,"P E 1A",3,Q);b=b&&D(d,/^([0-9 a-z A-Z])+$/,"R S T U V : a-z 0-9");b=b&&B(k,"P E 1B",3,Q);b=b&&D(k,/^([0-9 a-z A-Z])+$/,"R S T U V : a-z 0-9");q(b){$.1C({1D:"1E",v:$(F).j(\'v\'),j:$("#u-W-w").1F(),1G:2(a){q(a.X===\'1H\'){$("#6-u").6("G");$("#1I").1J().1K(1L)}C{8(a.X)}},1M:\'1N\',g:J})}},"1O":2(){$(F).6("G");l.f("").m("4-5-g");8("Y w 10.")}},G:2(){l.f("").m("4-5-g");8("Y w 10.")}});$(".W-w").1P({1Q:{1R:"4-1S-1T-1U"}}).1V(2(){7 a=$(F).j(\'v\');$("#6-u").j(\'v\',a).6("1W")});$("#11-1X").11();7 e=$(\'#12\').1Y({1Z:20,21:{13:$("22[14=\'13\']").f()},j:23,24:h,25:26,14:\'12\'})});',62,131,'||function||ui|state|dialog|var|updateTips|||||||val|error|false||data|namaen|allFields|removeClass||||if|return|true||form|url|kategori|tips|addClass|||checkLength|else|checkRegexp|Kategori|this|close|addinput|add|expPost|highlight|length|karakter|center|top|Nama|50|Karakter|yang|di|perbolehkan|hanya|tambah|status|Masukkan||baru|tabs|tag|Hydra|name|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|250|width|350|resizable|position|my|at|of|window|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|kategori_video|empty|load|kategoriVideoSelect|dataType|json|Batal|button|icons|primary|icon|circle|plus|click|open|isi|magicSuggest|maxSelection|maxTags|dataUrlParams|input|tagURL|allowFreeEntries|maxDropHeight|200'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/video/add_video', $attributes);
	    ?>
	
	    <button type="submit" class="btn-aksi">Simpan</button>
	    <h1>Tambah video baru</h1>
	    <p>Masukkan data Video yang baru.</p>
	    
	    <label>Nama ID <span class="small">Nama video ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama video EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	    <div style="clear:left"></div>
	
	    <label>URL Video <span class="small">Url video</span> </label>
	    <input type="text" name="src_video" id="src_video" maxlength="200" value="<?php echo set_value('src_video'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('src_video'); ?>
	
	    <br />
	    <label>Tag Video<span class="small">Tag untuk video Maks.(<?php echo $this->config->item('max_tag'); ?>)</span> </label>
		<input id="tag" style="width:600px;margin-left:150px;" type="text" name="tag"/>
	<br />
		<div style="clear:left;"></div>
	    
	    <label>Video Utama<span class="small">Tampilkan Video ini di Headline.</span> </label>
	    <select name="utama" id="utama" style="margin-right:20px">
			<option value="off">Bukan</option>
			<option value="on">Ya</option>
	    </select>
	
	    <label style="margin-right:-20px">Komentar<span class="small">Perbolehkan komentar.</span> </label>
	    <select name="diskusi" id="diskusi" style="margin-right:20px">
			<option value="off">Tidak</option>
			<option value="on">Ya</option>
	    </select>

	    <label style="margin-right:-20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
	
		<label style="margin-right:-20px">Kategori Video<span class="small">Pilih Kategori</span> </label>
	    <select name="kategori_video" id="kategori_video">
		<?php
		if(!empty($kat_video)){
		foreach($kat_video as $items)
		{
			echo '<option value="'.$items->id_kategori.'" '.set_select('kategori_video', $items->id_kategori).'>'.$items->label_id.' // '.$items->label_en.'</option>';
		}
		}
		?>
	    </select>
	    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/video/tambah_kategori');?>" class="tambah-kategori" title="Tambah Kategori/Tag"></a>
	    <div style="clear:left"></div>
	    <?php echo form_error('ratting'); ?>
	    <?php echo form_error('kategori_video'); ?>
	    <?php echo form_error('utama'); ?>
	    <?php echo form_error('diskusi'); ?>
	    
	    <div style="clear:left"></div>
	    <label>Deskripsi<span class="small">Deskripsi Video</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id',''); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en',''); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	<script>
	/* Perbolehkan Tab */
	var editor_id = CKEDITOR.instances.ket_id;
	var editor_en = CKEDITOR.instances.ket_en;

	editor_id.on('key', function(ev) {
	    if (ev.data.keyCode == 9) { // TAB
	        var tabHtml = '<span style="white-space:pre">&#09;</span>';
	        var tabElement = CKEDITOR.dom.element.createFromHtml(tabHtml, editor_id.document);
	        editor_id.insertElement(tabElement);
	        ev.cancel();
	    }
	});
	editor_en.on('key', function(ev) {
	    if (ev.data.keyCode == 9) { // TAB
	        var tabHtml = '<span style="white-space:pre">&#09;</span>';
	        var tabElement = CKEDITOR.dom.element.createFromHtml(tabHtml, editor_en.document);
	        editor_en.insertElement(tabElement);
	        ev.cancel();
	    }
	});
	</script>

	</body>
</html>

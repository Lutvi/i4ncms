<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Postingan</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	.tambah-keyword { padding:6px 4px; width:16px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	
	<script>
	var kategoriBlogSelect = '<?php echo base_url($this->config->item('admpath').'/blog/list_select_kategori'); ?>';
	var keywordIdURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_id'; ?>';
	var keywordEnURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_en'; ?>';
	var maxTags = <?php echo $this->config->item('max_tag'); ?>;
	var tagURL = '<?php echo base_url($this->config->item('admpath').'/blog/list_tag/blog'); ?>';
	</script>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function split( val ) {
				return val.split( /,\s*/ );
	    }
	    function extractLast( term ) {
	        return split( term ).pop();
	    }
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 270,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                if(data.tipe === 'kategori')
	                                $("#kategori_blog").empty().load(kategoriBlogSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan data baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan data baru." );
	        }
	    });
	
	    $( ".tambah-kategori, .tambah-keyword" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-seo, #tabs-isi" ).tabs();
	
	    //keyword id halaman
	    $( "#keyword_id" )
	    .bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	                $( this ).data( "autocomplete" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        source: function( request, response ) {
	            $.getJSON( keywordIdURL, {
	                term: extractLast( request.term )
	            }, response );
	        },
	        search: function() {
	            var term = extractLast( this.value );
	            if ( term.length < 2 ) {
	                return false;
	            }
	        },
	        focus: function() {
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            terms.pop();
	            terms.push( ui.item.value );
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	
	    //keyword en halaman
	    $( "#keyword_en" )
	    .bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	                $( this ).data( "autocomplete" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        source: function( request, response ) {
	            $.getJSON( keywordEnURL, {
	                term: extractLast( request.term )
	            }, response );
	        },
	        search: function() {
	            var term = extractLast( this.value );
	            if ( term.length < 2 ) {
	                return false;
	            }
	        },
	        focus: function() {
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            terms.pop();
	            terms.push( ui.item.value );
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	
		//-------------------------------
		// Tags
		//-------------------------------
		var tag = $('#tag').magicSuggest({
			maxSelection: maxTags,
			dataUrlParams: {Hydra:$("input[name='Hydra']").val()},
			data: tagURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'tag'
		});

		//scroll top
		$(window).scroll(function(){
            if ($(document).scrollTop() > 50) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(4(){8 d=$(\'#V\');8 i=$(\'#V p\').1w()+1;8 e=$("#1x"),x=$("#1y"),y=$([]).W(e).W(x),L=$(".1z");4 X(){1A.1B()}4 q(t){L.1C(t).M("g-l-Y");1D(4(){L.B("g-l-Y",1E)},1F)}4 N(o,n,a,b){h(o.r().C>b||o.r().C<a){o.M("g-l-u");q("1G "+n+" 1H 1I 1J "+a+" 10 1K 1L "+b+" 10.");5 6}D{5 E}}4 O(o,a,n){h(!(a.1M(o.r()))){o.M("g-l-u");q(n);5 6}D{5 E}}4 v(a){5 a.v(/,\\s*/)}4 w(a){5 v(a).P()}$("#m-F").m({1N:6,1O:1P,1Q:1R,1S:6,1T:{1U:"11 12",1V:"11 12",1W:13},1X:E,1Y:{"1Z":4(){8 b=E;y.B("g-l-u");b=b&&N(e,"14 20",3,15);b=b&&O(e,/^([0-9 a-z A-Z])+$/,"16 17 18 19 1a : a-z 0-9");b=b&&N(x,"14 21",3,15);b=b&&O(x,/^([0-9 a-z A-Z])+$/,"16 17 18 19 1a : a-z 0-9");h(b){$.22({23:"24",G:$(7).j(\'G\'),j:$("#F-Q-R").25(),26:4(a){h(a.1b===\'27\'){$("#m-F").m("S");h(a.28===\'R\')$("#29").2a().2b(2c)}D{q(a.1b)}},2d:\'2e\',u:X})}},"2f":4(){$(7).m("S");y.r("").B("g-l-u");q("1c j 1d.")}},S:4(){y.r("").B("g-l-u");q("1c j 1d.")}});$(".Q-R, .Q-2g").2h({2i:{2j:"g-2k-2l-2m"}}).1e(4(){8 a=$(7).j(\'G\');$("#m-F").j(\'G\',a).m("2n")});$("#T-2o, #T-2p").T();$("#2q").1f("1g",4(a){h(a.H===$.g.H.1h&&$(7).j("I").1i.1j){a.1k()}}).I({1l:4(a,b){$.1m(2r,{J:w(a.J)},b)},1n:4(){8 a=w(7.k);h(a.C<2){5 6}},1o:4(){5 6},1p:4(a,b){8 c=v(7.k);c.P();c.K(b.1q.k);c.K("");7.k=c.1r(", ");5 6}});$("#2s").1f("1g",4(a){h(a.H===$.g.H.1h&&$(7).j("I").1i.1j){a.1k()}}).I({1l:4(a,b){$.1m(2t,{J:w(a.J)},b)},1n:4(){8 a=w(7.k);h(a.C<2){5 6}},1o:4(){5 6},1p:4(a,b){8 c=v(7.k);c.P();c.K(b.1q.k);c.K("");7.k=c.1r(", ");5 6}});8 f=$(\'#1s\').2u({2v:2w,2x:{1t:$("2y[1u=\'1t\']").r()},j:2z,2A:6,2B:2C,1u:\'1s\'});$(13).2D(4(){h($(2E).1v()>2F){$(\'.U\').2G()}D{$(\'.U\').2H()}});$(\'.U\').1e(4(){$("2I, 2J").2K({1v:0},2L);5 6})});',62,172,'||||function|return|false|this|var||||||||ui|if||data|value|state|dialog||||updateTips|val|||error|split|extractLast|namaen|allFields|||removeClass|length|else|true|form|url|keyCode|autocomplete|term|push|tips|addClass|checkLength|checkRegexp|pop|tambah|kategori|close|tabs|scrollup|addinput|add|expPost|highlight||karakter|center|top|window|Nama|50|Karakter|yang|di|perbolehkan|hanya|status|Masukkan|baru|click|bind|keydown|TAB|menu|active|preventDefault|source|getJSON|search|focus|select|item|join|tag|Hydra|name|scrollTop|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|270|width|350|resizable|position|my|at|of|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|tipe|kategori_blog|empty|load|kategoriBlogSelect|dataType|json|Batal|keyword|button|icons|primary|icon|circle|plus|open|seo|isi|keyword_id|keywordIdURL|keyword_en|keywordEnURL|magicSuggest|maxSelection|maxTags|dataUrlParams|input|tagURL|allowFreeEntries|maxDropHeight|200|scroll|document|250|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>
	</head>
	<body>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>

		<button type="button" class="scrollup">Top</button>
		<div id="stylized" class="myformIframe">
		    <?php
		    $attributes = array( 'id' => 'form');
		    echo form_open_multipart($this->config->item('admpath').'/blog/add_blog', $attributes);
		    ?>
		    <button type="submit" class="btn-aksi">Simpan</button>
		    
		    <h1>Tambah posting baru</h1>
		    <p>Masukkan data Postingan yang baru.</p>
		
		    <input type="hidden" name="id_penulis" id="id_penulis" value="<?php echo set_value('id_penulis',1); ?>" />
		    
		    <label>Judul ID <span class="small">Judul post ID</span> </label>
		    <input type="text" name="judul_id" id="judul_id" maxlength="150" style="width:350px;margin-right:20px" value="<?php echo set_value('judul_id'); ?>" />
		
		    <label>Judul EN <span class="small">Judul post EN</span> </label>
		    <input type="text" name="judul_en" id="judul_en" maxlength="150" style="width:350px;" value="<?php echo set_value('judul_en'); ?>" />
		
		    <div style="clear:left"></div>
		    <?php echo form_error('judul_id'); ?>
		    <?php echo form_error('judul_en'); ?>
		
			<div style="clear:left"></div>
		    <label>Headline<span class="small">Apakah Post ini Headline.?</span> </label>
		    <select name="headline" id="headline" style="margin-right:20px">
				<option value="n">Bukan</option>
				<option value="y">Ya</option>
		    </select>
		
		    <label style="margin-right:-20px">Komentar<span class="small">Perbolehkan komentar.</span> </label>
		    <select name="diskusi" id="diskusi" style="margin-right:20px">
				<option value="off">Tidak</option>
				<option value="on">Ya</option>
		    </select>

		    <label style="margin-right:-20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		    <select name="ratting" id="ratting" style="margin-right:20px">
				<option value="1.0">1.0</option>
				<option value="2.0">2.0</option>
				<option value="2.3">2.3</option>
				<option value="2.5">2.5</option>
				<option value="3.0">3.0</option>
				<option value="3.3">3.3</option>
				<option value="3.6">3.6</option>
				<option value="4.1">4.1</option>
				<option value="4.6">4.6</option>
		    </select>
		
			<label style="margin-right:-20px">Kategori Post<span class="small">Pilih Kategori</span> </label>
		    <select name="kategori_blog" id="kategori_blog">
			<?php
			foreach($kat_blog as $items)
			{
				echo '<option value="'.$items->id_kategori.'" '.set_select('kategori_blog', $items->id_kategori).'>'.$items->label_id.' // '.$items->label_en.'</option>';
			}
			?>
		    </select>
		    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/blog/tambah_kategori');?>" class="tambah-kategori" title="Tambah Kategori/Tag"></a>
		    <div style="clear:left"></div>
		    <?php echo form_error('ratting'); ?>
		    <?php echo form_error('kategori_blog'); ?>
		    <?php echo form_error('utama'); ?>
		    <?php echo form_error('diskusi'); ?>
		    <div style="clear:left"></div>
		    
		    <label>Gambar Cover<span class="small">min:800x600, max:1366x1024<br>max-size : 1000KB / 1MB</span> </label>
		    <input type="file" name="userfile" />
		    <div style="clear:left"></div>
			<?php echo form_error('userfile'); ?>
		
		<br />
		    <label>Tag Post<span class="small">Tag untuk post Maks.(<?php echo $this->config->item('max_tag'); ?>)</span> </label>
			<input type="text" id="tag" name="tag" style="width:600px;margin-left:150px;"/>
		<br />
		
			<div style="clear:left"></div>
		    <label>SEO <span class="small">SEO Post</span> </label>
		    <div style="float:left;width:60%;margin-left:10px;">
				<div id="tabs-seo" style="height:200px">
		            <ul>
		                <li><a href="#seo-id">SEO Indonesia</a></li>
		                <li><a href="#seo-en">SEO English</a></li>
		            </ul>
		            <div id="seo-id">
		                <div class="">
							<label>Keyword ID <span class="small">Keyword Indonesia</span> </label>
		                    <input type="text" name="keyword_id" id="keyword_id" maxlength="200" value="<?php echo set_value('keyword_id'); ?>" />
		                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
		                    <label>Description ID <span class="small">Description Indonesia</span> </label>
		                    <textarea name="description_id" id="description_id" maxlength="200" style="height:80px;"><?php echo set_value('description_id'); ?></textarea>
		                </div>
		            </div>
		            <div id="seo-en">
		                 <div class="">
							<label>Keyword EN <span class="small">Keyword English</span> </label>
		                    <input type="text" name="keyword_en" id="keyword_en" maxlength="200" value="<?php echo set_value('keyword_en'); ?>" />
		                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
		                    <label>Description EN<span class="small">Description English</span> </label>
		                    <textarea name="description_en" id="description_en" maxlength="200" style="height:80px;"><?php echo set_value('description_en'); ?></textarea>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div style="clear:left"></div>
		    <?php echo form_error('keyword_id'); ?>
		    <?php echo form_error('description_id'); ?>
		    <?php echo form_error('keyword_en'); ?>
		    <?php echo form_error('description_en'); ?>
		
		    <br style="clear:left" />
			<label>Status<span class="small">Status postingan.</span> </label>
		    <select name="status" id="status">
				<option value="on">Publish</option>
				<option value="off">Offline</option>
				<option value="draft">Draft</option>
		    </select>
		
		    <label style="margin-left:20px;margin-right:-40px">Embed Album<span class="small">Pilih Album</span> </label>
		    <select name="embed_album" id="embed_album">
		    <option value="0">-- Tidak ada --</option>
			<?php
			if(!empty($parent_album)){
			foreach($parent_album as $items)
			{
				echo '<option value="'.$items['id'].'" '.set_select('embed_album', $items['id']).'>'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
			}
			}
			?>
		    </select>
		
		    <label style="margin-left:20px;margin-right:-40px">Embed Video<span class="small">Pilih Video</span> </label>
		    <select name="embed_video" id="embed_video">
		    <option value="0">-- Tidak ada --</option>
			<?php
			if(!empty($parent_video)){
			foreach($parent_video as $items)
			{
				echo '<option value="'.$items['id'].'" '.set_select('embed_video', $items['id']).'>'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
			}
			}
			?>
		    </select>
		    
		    <div style="clear:left"></div>
		    <?php echo form_error('status'); ?>
		    <?php echo form_error('embed_album'); ?>
		    <?php echo form_error('embed_video'); ?>
		    
		    <label>Konten<span class="small">Konten Post</span></label>
		    <div style="float:left;padding:3px;margin-left:10px;border:1px solid #ADD8E6">
		    <label style="">Kode Snippet <span class="small">Ceklist jika ada kode snippet</span></label> <input name="snippet" type="checkbox" value="y" <?php echo set_checkbox('snippet', 'y'); ?> />
		    </div>
		    <br style="clear:left" />
		    <?php echo form_error('isi_id'); ?>
		    <?php echo form_error('isi_en'); ?>
		    
		    <div id="tabs-isi">
			<ul>
				<li><a href="#isi-id">Konten Indonesia</a></li>
				<li><a href="#isi-en">Konten English</a></li>
			</ul>
			<div id="isi-id">
				<textarea name="isi_id" id="isi_id" ><?php echo set_value('isi_id',''); ?></textarea>
				<?php echo display_ckeditor($isi_id);?>
		    </div>
			<div id="isi-en">
				<textarea name="isi_en" id="isi_en" ><?php echo set_value('isi_en',''); ?></textarea>
				<?php echo display_ckeditor($isi_en);?>
			</div>
		
		    <div style="clear:both; height:10px;"></div>
		  </form>
		
		</div>

	</body>
</html>

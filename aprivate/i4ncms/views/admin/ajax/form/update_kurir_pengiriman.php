<!DOCTYPE html>
<html>
	<head>
	<title>Update Metode Pembayaran <?php echo set_value('nama_vendor_ori', $kurir->nama_vendor); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-autocomplete {
	    max-height: 170px;
	    overflow-y: auto;
	    /* prevent horizontal scrollbar */
	    overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
	    height: 170px;
	}
	</style>

	<script>
	var idWilayah = <?php echo (!empty($kurir->id_wilayah))?"[".$kurir->id_wilayah."]":"''"; ?>;
	var maxWilayah = <?php echo $this->config->item('max_wilayah'); ?>;
	var idWilayahURL = '<?php echo base_url($this->config->item('admpath').'/atur_kurir_pengiriman/list_idwilayah'); ?>';
	</script>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>

	function cekTipe(tipe)
	{
		if(tipe == "online"){
			$("#extra").show();
		}else{
			$("#extra").hide();
		}
	}
	
	$(document).ready(function(){
		var tipe = $('#tipe').val();
		//cek tipenya
		cekTipe(tipe);
		$("#tipe").change(function () {
			var tipe = $('#tipe').val();
	        cekTipe(tipe);
	        $('.error').hide();
	    });

		//-------------------------------
		// #id_wilayah
		//-------------------------------
		var wilayah = $('#wilayah').magicSuggest({
			maxSelection: maxWilayah,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: idWilayahURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'wilayah'
		});

		if(idWilayah !== '')
		{
			$(wilayah).on('load', function(){
				if(this._dataSet === undefined){
					this._dataSet = true;
					wilayah.setValue(idWilayah);
				}
			});
	    }

	    //Logo
	    $('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});

		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 50) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('1 6(a){3(a=="v"){$("#g").7()}h{$("#g").5()}}$(i).w(1(){4 b=$(\'#8\').2();6(b);$("#8").j(1(){4 a=$(\'#8\').2();6(a);$(\'.x\').5()});4 c=$(\'#k\').y({z:A,B:{l:$("C[m=l]").2()},D:E,F:n,G:H,m:\'k\'});3(o!==\'\'){$(c).I(\'J\',1(){3(9.p===K){9.p=L;c.M(o)}})}$(\'#N\').d(1(){4 a=$(\'#q\').2();$(\'#r\').5();$(\'#s\').7();3(a!=""){$(\'#e\').2(\'t\')}});$(\'#q\').j(1(){4 a=$(9).2();3(a!=""){$(\'#e\').2(\'t\')}});$(\'#O\').d(1(){$(\'#r\').7();$(\'#s\').5();$(\'#e\').2(\'P\')});$(Q).R(1(){3($(i).u()>S){$(\'.f\').T()}h{$(\'.f\').U()}});$(\'.f\').d(1(){$("V, W").X({u:0},Y);Z n})});',62,62,'|function|val|if|var|hide|cekTipe|show|tipe|this||||click|status_ganti|scrollup|extra|else|document|change|wilayah|Hydra|name|false|idWilayah|_dataSet|userfile|c_gmbr|up_gmbr|yes|scrollTop|online|ready|error|magicSuggest|maxSelection|maxWilayah|dataUrlParams|input|data|idWilayahURL|allowFreeEntries|maxDropHeight|200|on|load|undefined|true|setValue|ganti_gmbr|batal_ganti_gmbr|no|window|scroll|50|fadeIn|fadeOut|html|body|animate|600|return'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/atur_kurir_pengiriman'),'Kembali ke List', $attr); ?>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/atur_kurir_pengiriman/update_form_kurir', $attributes);
	    ?>

		<input type="hidden" name="id_kurir" id="id_metode" maxlength="11" value="<?php echo set_value('id_kurir', $kurir->id); ?>" />
	    <input type="hidden" name="nama_vendor_ori" id="nama_vendor_ori" maxlength="50" value="<?php echo set_value('nama_vendor_ori', $kurir->nama_vendor); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="logo" id="logo" value="<?php echo set_value('logo',$kurir->logo); ?>" />

	    <button type="submit" class="btn-aksi">Update</button>

		 <h1>Update Kurir Pengiriman <?php echo set_value('nama_vendor_ori', $kurir->nama_vendor); ?></h1>
	    <p>Masukkan data Kurir Pengiriman yang baru.</p>

	    <label>Nama Vendor<span class="small">Nama vendor atau instansi</span> </label>
	    <input type="text" name="nama_vendor" id="nama_vendor" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_vendor', $kurir->nama_vendor); ?>" />

	    <label>Nama Layanan<span class="small">Jenis Nama Layanan kurir.</span> </label>
	    <input type="text" name="nama_layanan" id="nama_layanan" maxlength="50" style="width:150px;margin-right:20px" value="<?php echo set_value('nama_layanan', $kurir->nama_layanan); ?>" />

		<label style="margin-left:25px;margin-right:-80px">Status<span class="small">Status iklan.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on',($kurir->status == 'on')?TRUE:FALSE); ?>>On</option>
			<option value="off" <?php echo set_select('status','off',($kurir->status == 'off')?TRUE:FALSE); ?>>Off</option>
	    </select>
		<div style="clear:left"></div>
		<?php echo form_error('nama_vendor'); ?>
		<?php echo form_error('nama_layanan'); ?>
		<?php echo form_error('status'); ?>

		<div style="clear:left"></div>
		<label>URL Trace<span class="small">URL Trace untuk cek pengiriman.</span> </label>
		<input type="text" name="url_trace" id="url_trace" maxlength="150" style="width:600px;" value="<?php echo set_value('url_trace', $kurir->url_trace); ?>" />
		<div style="clear:left"></div>
		<?php echo form_error('url_trace'); ?>


		<div id="c_gmbr" style="height:60px;">
			<button type="button" id="ganti_gmbr" class="ganti">Ganti Logo</button>
			<label>Logo Vendor<span class="small">Logo Vendor yang terpasang</span> </label>
			<img src="<?php echo base_url(); ?>_media/logo-kurir/thumb_<?php echo $kurir->logo; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
			<div style="clear:left"></div>
	    </div>

		<div id="up_gmbr" style="display:none;">
		<button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Logo</button>
	    <label>Ganti Logo Vendor<span class="small">min:120x120, max:640x640<br>max-size : 0.8MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" id="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>
		</div>

		<div style="clear:left"></div>
	    <br />
			<label>Wilayah Layanan<span class="small">Pilih Wilayah yang dapat menggunakan layanan<br>atau kosongkan saja jika semua wilayah bisa menggunakan layanan.</span> </label>
			<input type="text" name="wilayah" id="wilayah" value="<?php echo set_value('wilayah'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
		<br /><br />

		<div style="clear:left"></div>
		<label>Ongkos Kirim<span class="small">Ongkos kirim layanan kurir.</span> </label>
	    <input type="text" name="biaya_ongkir" id="biaya_ongkir" maxlength="11" style="width:300px;margin-right:20px" value="<?php echo set_value('biaya_ongkir', $kurir->biaya_ongkir); ?>" />
	    <label>Waktu Sampai<span class="small">Perkiraan lamanya hari barang akan sampai ke tujuan.</span> </label>
	    <input type="text" name="waktu_barang_sampai" id="waktu_barang_sampai" maxlength="2" style="width:300px;margin-right:20px" value="<?php echo set_value('waktu_barang_sampai', $kurir->waktu_barang_sampai); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('biaya_ongkir'); ?>
	    <?php echo form_error('waktu_barang_sampai'); ?>

		<div style="clear:left"></div>
		<label>Detail Metode<span class="small">Masukkan detail Identitas Metode pembayarannya</span> </label>
		<textarea name="detail" id="detail" maxlength="250" spellcheck="false" style="height:120px;width:600px"><?php echo set_value('detail', jadi_nl($kurir->detail)); ?></textarea>
		<div style="clear:both"></div>
		<?php echo form_error('detail'); ?>

		<div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	</body>
</html>

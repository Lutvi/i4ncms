<!-- Skrip JS buat tutup auto dialog form -->
<?php foreach($css as $item): ?>
<link href="<?php echo base_url().'assets/css/'.$item; ?>" rel="stylesheet" type="text/css" />
<?php endforeach; ?>
<?php foreach($js as $item): ?>
<script type="text/javascript" src="<?php echo base_url().'assets/js/'.$item; ?>"></script>
<?php endforeach; ?>

<script type="text/javascript">
i=parseInt(<?php echo $timeout/1000; ?>);
function hitungan()
{
	setInterval(function(){
	--i;
		if (i < 1) {
			$(function() {
				window.parent.$('#aturAppDialog').dialog('close');// //Auto Close Dialog Box.
				window.parent.$('#aturAppDialog').remove();
			});
		}
	document.getElementById("timer").innerHTML=i;
	}, parseInt(<?php echo $timeout; ?>));
}
setTimeout(hitungan, 300);
</script>

<div class="ui-widget" style="width:99.5%">
	<div class="ui-state-error ui-corner-all" style="padding:0 .7em .7em .7em;">
		<h4><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>Aksi gagal</h4>
		<?php
		$info = highlight_phrase($info, "gagal", '<span style="color:#F51822">', '</span>');
		$info = highlight_phrase($info, "tidak bisa menyimpan", '<span style="color:#F51828">', '</span>');
		?>
		<span><?php echo $info; ?>, Silahkan tutup dan mencobanya kembali.</span>
		<br />
		atau silahkan menunggu....
		<br />
		Dialog ini akan ditutup otomatis dalam <span id="timer"><?php echo $timeout/1000; ?></span> detik.
	</div>
</div>

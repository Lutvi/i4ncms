<!DOCTYPE html>
<html>
	<head>
	<title>Update video <?php echo humanize($video->nama_id); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	<script>
	var kategoriVideoSelect = '<?php echo base_url($this->config->item('admpath').'/video/list_select_kategori'); ?>';
	var maxTags = <?php echo $this->config->item('max_tag'); ?>;
	var tagURL = '<?php echo base_url($this->config->item('admpath').'/album/list_tag/video'); ?>';
	var nilaiTag = <?php echo $tags; ?>;
	</script>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 250,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Kategori": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori_video").empty().load(kategoriVideoSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	
	    $( ".tambah-kategori" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-isi" ).tabs();
	});
	
	$(document).ready(function(){
	
		//-------------------------------
		// Tags
		//-------------------------------
		var tag = $('#tag').magicSuggest({
			maxSelection: maxTags,
			dataUrlParams: {Hydra:$("input[name='Hydra']").val()},
			data: tagURL,
			allowFreeEntries: false,
			maxDropHeight: 200
		});
	
		$(tag).on('load', function(){
	        if(this._dataSet === undefined){
	            this._dataSet = true;
	            tag.setValue(nilaiTag);
	        }
	    });
	
	    //scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 30) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(2(){7 c=$(\'#G\');7 i=$(\'#G p\').14()+1;7 d=$("#15"),m=$("#16"),q=$([]).H(d).H(m),x=$(".17");2 I(){18.19()}2 8(t){x.1a(t).y("4-5-J");1b(2(){x.r("4-5-J",1c)},1d)}2 B(o,n,a,b){g(o.e().K>b||o.e().K<a){o.y("4-5-f");8("1e "+n+" 1f 1g 1h "+a+" L 1i 1j "+b+" L.");s h}C{s j}}2 D(o,a,n){g(!(a.1k(o.e()))){o.y("4-5-f");8(n);s h}C{s j}}$("#6-u").6({1l:h,1m:1n,1o:1p,1q:h,1r:{1s:"M N",1t:"M N",1u:1v},1w:j,1x:{"1y E":2(){7 b=j;q.r("4-5-f");b=b&&B(d,"O E 1z",3,P);b=b&&D(d,/^([0-9 a-z A-Z])+$/,"Q R S T U : a-z 0-9");b=b&&B(m,"O E 1A",3,P);b=b&&D(m,/^([0-9 a-z A-Z])+$/,"Q R S T U : a-z 0-9");g(b){$.1B({1C:"1D",v:$(k).l(\'v\'),l:$("#u-V-w").1E(),1F:2(a){g(a.W===\'1G\'){$("#6-u").6("F");$("#1H").1I().X(1J)}C{8(a.W)}},1K:\'1L\',f:I})}},"1M":2(){$(k).6("F");q.e("").r("4-5-f");8("Y w 10.")}},F:2(){q.e("").r("4-5-f");8("Y w 10.")}});$(".V-w").1N({1O:{1P:"4-1Q-1R-1S"}}).1T(2(){7 a=$(k).l(\'v\');$("#6-u").l(\'v\',a).6("1U")});$("#11-1V").11()});$(1W).1X(2(){7 a=$(\'#1Y\').1Z({20:21,22:{12:$("23[24=\'12\']").e()},l:25,26:h,27:28});$(a).29(\'X\',2(){g(k.13===2a){k.13=j;a.2b(2c)}})});',62,137,'||function||ui|state|dialog|var|updateTips||||||val|error|if|false||true|this|data|namaen||||allFields|removeClass|return||form|url|kategori|tips|addClass|||checkLength|else|checkRegexp|Kategori|close|addinput|add|expPost|highlight|length|karakter|center|top|Nama|50|Karakter|yang|di|perbolehkan|hanya|tambah|status|load|Masukkan||baru|tabs|Hydra|_dataSet|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|250|width|350|resizable|position|my|at|of|window|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|kategori_video|empty|kategoriVideoSelect|dataType|json|Batal|button|icons|primary|icon|circle|plus|click|open|isi|document|ready|tag|magicSuggest|maxSelection|maxTags|dataUrlParams|input|name|tagURL|allowFreeEntries|maxDropHeight|200|on|undefined|setValue|nilaiTag'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
		echo form_open($this->config->item('admpath').'/video/update_formvideo', $attributes);
	    ?>
	
	    <button type="submit" class="btn-aksi">Update</button>
	    <h1>Update video <?php echo humanize($video->nama_id); ?></h1>
	    <input type="hidden" name="id_video" id="id_video" value="<?php echo set_value('id_video',$video->id); ?>" />
	    <input type="hidden" name="tag_ori" id="tag_ori" value="<?php echo set_value('tag_ori',$tags); ?>" />
	    <input type="hidden" name="src_video_ori" id="src_video_ori" value="<?php echo set_value('src_video_ori',$video->src_video); ?>" />
	    <input type="hidden" name="gambar" id="gambar" value="<?php echo set_value('gambar',$video->gambar_video); ?>" />
	    <input type="hidden" name="nama_ori_id" id="nama_ori_id" value="<?php echo set_value('nama_ori_id',$video->nama_id); ?>" />
	    <input type="hidden" name="nama_ori_en" id="nama_ori_en" value="<?php echo set_value('nama_ori_en',$video->nama_en); ?>" />
	    <p>Masukkan data Video yang baru.</p>
	
		<label>Nama ID <span class="small">Nama video ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:280px;margin-right:20px" value="<?php echo set_value('nama_id',$video->nama_id); ?>" />
	
	    <label style="margin-right:-40px">Nama EN <span class="small">Nama video EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:280px;" value="<?php echo set_value('nama_en',$video->nama_en); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
	    <div style="clear:left"></div>
	    <label>URL Video <span class="small">Url video</span> </label>
	    <input type="text" name="src_video" id="src_video" maxlength="200" value="<?php echo set_value('src_video', $video->src_video); ?>" />
	
	    <a href="<?php echo $video->src_video; ?>" target="_blank" title="Preview Video">
	    <img src="<?php echo base_url(); ?>_media/videos/small/small_<?php echo $video->gambar_video; ?>" style="padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;float:left;margin-left:30px" height="60" align="absmiddle" />
	    </a>
	    <div style="clear:left"></div>
	    <?php echo form_error('src_video'); ?>
	
		<br />
		    <label>Tag Video<span class="small">Tag untuk video Maks.(<?php echo $this->config->item('max_tag'); ?>)</span> </label>
			<input id="tag" style="width:600px;margin-left:150px;" type="text" name="tag"/>
		<div style="clear:both;"></div>
		<br style="clear:left" />
	
	    <label>Kategori Video<span class="small">Pilih Kategori</span> </label>
		<select id="kategori_video" name="kategori_video">
			<?php foreach ($kat_video as $items): ?>
			<option value="<?php echo $items->id_kategori; ?>" <?php echo set_select('kategori_video', $items->id_kategori,($items->id_kategori == $video->kategori_id)?TRUE:FALSE); ?>>
			&nbsp;&nbsp;<?php echo $items->label_id; ?>&nbsp;//&nbsp;<?php echo $items->label_en; ?>&nbsp;&nbsp;
			</option>
			<?php endforeach; ?>>
		</select>
	    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/video/tambah_kategori');?>" class="tambah-kategori" title="Tambah Kategori/Tag"></a>

	    <label style="margin-left:20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="<?php echo $video->ratting; ?>"><?php echo $video->ratting; ?></option>
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
	    
	    <div style="clear:left"></div>
	    <?php echo form_error('ratting'); ?>
	    <?php echo form_error('kategori_video'); ?>
	    <?php echo form_error('utama'); ?>
	    <?php echo form_error('diskusi'); ?>
	
	    <div style="clear:left"></div>
	    <label>Deskripsi<span class="small">Deskripsi Video</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id',$video->ket_id); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en',$video->ket_en); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	</body>
</html>

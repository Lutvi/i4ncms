<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Kurir Pengiriman</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-autocomplete {
	    max-height: 170px;
	    overflow-y: auto;
	    /* prevent horizontal scrollbar */
	    overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
	    height: 170px;
	}
	</style>

	<script>
	var idWilayahURL = '<?php echo base_url($this->config->item('admpath').'/atur_kurir_pengiriman/list_idwilayah'); ?>';
	var maxWilayah = <?php echo $this->config->item('max_wilayah'); ?>;
	</script>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>

	function cekTipe(tipe)
	{
		if(tipe == "online"){
			$("#extra").show();
		}else{
			$("#extra").hide();
		}
	}
	
	$(document).ready(function(){
		var tipe = $('#tipe').val();
		//cek tipenya
		cekTipe(tipe);
		$("#tipe").change(function () {
			var tipe = $('#tipe').val();
	        cekTipe(tipe);
	        $('.error').hide();
	    });
	    
		//-------------------------------
		// #id_wilayah
		//-------------------------------
		var wilayah = $('#wilayah').magicSuggest({
			maxSelection: maxWilayah,
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: idWilayahURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'wilayah'
		});

		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 50) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});

		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('1 2(a){7(a=="k"){$("#8").l()}9{$("#8").d()}}$(e).m(1(){3 b=$(\'#4\').5();2(b);$("#4").n(1(){3 a=$(\'#4\').5();2(a);$(\'.o\').d()});3 c=$(\'#f\').p({q:r,s:{g:$("t[h=g]").5()},u:v,w:i,x:y,h:\'f\'});$(z).A(1(){7($(e).j()>B){$(\'.6\').C()}9{$(\'.6\').D()}});$(\'.6\').E(1(){$("F, G").H({j:0},I);J i})});',46,46,'|function|cekTipe|var|tipe|val|scrollup|if|extra|else||||hide|document|wilayah|Hydra|name|false|scrollTop|online|show|ready|change|error|magicSuggest|maxSelection|maxWilayah|dataUrlParams|input|data|idWilayahURL|allowFreeEntries|maxDropHeight|200|window|scroll|50|fadeIn|fadeOut|click|html|body|animate|600|return'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/atur_kurir_pengiriman'),'Kembali ke List', $attr); ?>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/atur_kurir_pengiriman/add_kurir', $attributes);
	    ?>

	    <button type="submit" class="btn-aksi">Simpan</button>

		 <h1>Tambah Kurir Pengiriman baru</h1>
	    <p>Masukkan data Kurir Pengiriman baru.</p>

	    <label>Nama Vendor<span class="small">Nama vendor atau instansi.</span> </label>
	    <input type="text" name="nama_vendor" id="nama_vendor" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_vendor'); ?>" />

	    <label>Nama Layanan<span class="small">Jenis Nama Layanan kurir.</span> </label>
	    <input type="text" name="nama_layanan" id="nama_layanan" maxlength="50" style="width:150px;margin-right:20px" value="<?php echo set_value('nama_layanan'); ?>" />

		<label style="margin-left:25px;margin-right:-80px">Status<span class="small">Status layanan kurir.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on'); ?>>On</option>
			<option value="off" <?php echo set_select('status','off'); ?>>Off</option>
	    </select>
		<div style="clear:left"></div>
		<?php echo form_error('nama_vendor'); ?>
		<?php echo form_error('nama_layanan'); ?>
		<?php echo form_error('status'); ?>

		<div style="clear:left"></div>
		<label>URL Trace<span class="small">URL Trace untuk cek pengiriman.</span> </label>
		<input type="text" name="url_trace" id="url_trace" maxlength="150" style="width:600px;" value="<?php echo set_value('url_trace'); ?>" />
		<div style="clear:left"></div>
		<?php echo form_error('url_trace'); ?>

	    <label>Logo Vendor<span class="small">min:120x120, max:640x640<br>max-size : 0.8MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>

	    <br />
			<label>Wilayah Layanan<span class="small">Pilih Wilayah yang dapat menggunakan layanan<br>atau kosongkan saja jika semua wilayah bisa menggunakan layanan.</span> </label>
			<input type="text" name="wilayah" id="wilayah" value="<?php echo set_value('wilayah'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
		<br /><br />

		<div style="clear:left"></div>
		<label>Ongkos Kirim<span class="small">Ongkos kirim layanan kurir.</span> </label>
	    <input type="text" name="biaya_ongkir" id="biaya_ongkir" maxlength="11" style="width:300px;margin-right:20px" value="<?php echo set_value('biaya_ongkir'); ?>" />
	    <label>Waktu Sampai<span class="small">Perkiraan lamanya hari barang akan sampai ke tujuan.</span> </label>
	    <input type="text" name="waktu_barang_sampai" id="waktu_barang_sampai" maxlength="2" style="width:300px;margin-right:20px" value="<?php echo set_value('waktu_barang_sampai'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('biaya_ongkir'); ?>
	    <?php echo form_error('waktu_barang_sampai'); ?>

		<div style="clear:left"></div>
		<label>Detail Kurir<span class="small">Masukkan detail singkat tentang kurir.</span> </label>
		<textarea name="detail" id="detail" maxlength="100" style="height:120px;width:600px"><?php echo set_value('detail'); ?></textarea>
		<div style="clear:both"></div>
		<?php echo form_error('detail'); ?>

		<div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	</body>
</html>

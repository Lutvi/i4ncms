<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Gallery Album baru</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<script>
	$(function(){
	    $( "#tabs-isi" ).tabs();
	});
	</script>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	
	<div id="stylized" class="myformIframe">
	
	<?php if(is_array($parent_album)): ?>
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/album/add_gallery', $attributes);
	    ?>

	    <button type="submit" class="btn-aksi">Simpan</button>
	    <h1>Tambah Gallery Album</h1>
	    <p>Masukkan data Gallery Album yang baru.</p>
	    
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
		<label>Album<span class="small">Pilih Album</span> </label>
	    <select name="parent_album" id="parent_album">
		<?php
		foreach($parent_album as $items)
		{
			echo '<option value="'.$items['id'].'">'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
		}
		?>
	    </select>
	    <?php echo form_error('parent_album'); ?>
	    <div style="clear:left"></div>
	<?php else: ?>

		<div class="kelist">
	        <?php 
	        $attr = array('class' => 'txtkelist');
	        echo anchor(site_url($this->config->item('admpath').'/album/tabel_gallery/'.set_value('parent_album', $parent_album)),'Kembali ke List', $attr); ?>
		</div>
		<?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/album/add_gallery_album', $attributes);
	    ?>
	    <h1>Tambah Gambar Gallery</h1>
	    <p>Masukkan data Gallery yang baru.</p>

	    <button type="submit" class="btn-aksi">Simpan</button>
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	    
		<input type="hidden" name="parent_album" id="parent_album" value="<?php echo set_value('parent_album', $parent_album); ?>" />
		<div style="clear:left"></div>
	<?php endif; ?>

	    <label>Gambar Gallery<span class="small">min:800x600, max:1366x1024<br>max-size : 1000KB / 1MB</span> </label>
	    <input type="file" name="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>
	
	    <label>Deskripsi<span class="small">Deskripsi Gambar</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id"><?php echo set_value('ket_id',''); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en"><?php echo set_value('ket_en',''); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	
	</body>
</html>

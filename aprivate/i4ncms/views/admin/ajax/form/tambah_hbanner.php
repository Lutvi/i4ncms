<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Banner Header</title>

	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 100) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});

		$( "#tabs-isi" ).tabs();
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/hbanner'),'Kembali ke List', $attr); ?>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/hbanner/add_hbanner', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Simpan</button>
	
	    <h1>Tambah Banner Header baru</h1>
	    <p>Masukkan data Banner Header yang baru.</p>
	    
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="50" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
	    <label>Gambar Banner<span class="small">Ukuran harus:<u>520x320</u><br>max-size : 1MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>

		<label>URL Target <span class="small">Link target URL ke konten</span> </label>
	    <input type="text" name="url_target" id="url_target" maxlength="200" style="width:65%;" value="<?php echo set_value('url_target'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('url_target'); ?>
	
	    <label>Status<span class="small">Status banner.</span> </label>
	    <select name="status" id="status">
			<option value="on">On</option>
			<option value="off">Off</option>
	    </select>
	    <div style="clear:left"></div>
		<?php echo form_error('status'); ?>

		<div style="clear:left;"></div>
	
	    <label>Info Singkat<span class="small">Info Singkat untuk Banner</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Indonesia</a></li>
			<li><a href="#isi-en">English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id',''); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en',''); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
		
	    <div style="clear:both; height:10px;"></div>
	  </form>

	</div>

	</body>
</html>

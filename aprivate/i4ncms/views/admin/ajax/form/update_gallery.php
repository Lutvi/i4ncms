<!DOCTYPE html>
<html>
	<head>
	<title>Update Gallery Album <?php echo humanize($album->nama_id); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
	    $( "#tabs-isi" ).tabs();
	});
	$(document).ready(function(){
		
		$('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});
		
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(0(){$("#3-e").3()});$(f).g(0(){$(\'#h\').4(0(){5 a=$(\'#6\').1();$(\'#7\').8();$(\'#9\').b();c(a!=""){$(\'#2\').1(\'d\')}});$(\'#6\').i(0(){5 a=$(j).1();c(a!=""){$(\'#2\').1(\'d\')}});$(\'#k\').4(0(){$(\'#7\').b();$(\'#9\').8();$(\'#2\').1(\'l\')})});',22,22,'function|val|status_ganti|tabs|click|var|userfile|c_gmbr|hide|up_gmbr||show|if|yes|isi|document|ready|ganti_gmbr|change|this|batal_ganti_gmbr|no'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/album/tabel_gallery/'.$album->id_album),'Kembali ke List', $attr);
		?>
	</div>
	
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
		echo form_open_multipart($this->config->item('admpath').'/album/update_formgallery', $attributes);
	    ?>
	    <h1>Update Gallery Album <?php echo humanize($album->nama_id); ?></h1>
	    <input type="hidden" name="id_album" id="id_album" value="<?php echo set_value('id_album',$album->id); ?>" />
	    <input type="hidden" name="parent_album" id="parent_album" value="<?php echo set_value('parent_album',$album->id_album); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="gambar" id="gambar" value="<?php echo set_value('gambar',$album->gambar_gallery); ?>" />
	    <input type="hidden" name="nama_ori_id" id="nama_ori_id" value="<?php echo $album->nama_id; ?>" />
	    <input type="hidden" name="nama_ori_en" id="nama_ori_en" value="<?php echo $album->nama_en; ?>" />
	    <p>Masukkan data Gambar yang baru.</p>
		
	    <button type="submit" class="btn-aksi">Update</button>
	
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:280px;margin-right:20px" value="<?php echo set_value('nama_id',$album->nama_id); ?>" />
	
	    <label style="margin-right:-40px">Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:280px;" value="<?php echo set_value('nama_en',$album->nama_en); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
	    <div style="clear:both"></div>
	    <div id="c_gmbr" style="height:60px;">
		    <button type="button" id="ganti_gmbr" class="ganti">Ganti Gambar</button>
		    <label>Gambar<span class="small">Gambar yang terpasang</span> </label>
		    <img src="<?php echo base_url(); ?>_media/album-gallery/thumb/thumb_<?php echo $album->gambar_gallery; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
		    <div style="clear:left"></div>
	    </div>
	
	    <div id="up_gmbr" style="display:none;">
		    <button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Gambar</button>
		    <label>Ganti Gambar <span class="small">min:800x600, max:1366x1024<br>max-size : 1000KB / 1MB</span> </label>
		    <input type="file" name="userfile" id="userfile" />
		    <br />
			<?php echo form_error('userfile'); ?>
		    <div style="clear:left"></div>
	    </div>
	    
	    <br style="clear:left" />
	    <label>Deskripsi<span class="small">Deskripsi 200char</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id',$album->ket_id); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en',$album->ket_en); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	
	</body>
</html>

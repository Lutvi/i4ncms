<!DOCTYPE html>
<html>
	<head>
	<title>Update posting <?php echo humanize($blog->judul_id); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	.tambah-keyword { padding:6px 4px; width:16px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	<script>
	var kategoriBlogSelect = '<?php echo base_url($this->config->item('admpath').'/blog/list_select_kategori'); ?>';
	var keywordIdURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_id'; ?>';
	var keywordEnURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_en'; ?>';
	var maxTags = <?php echo $this->config->item('max_tag'); ?>;
	var tagURL = '<?php echo base_url($this->config->item('admpath').'/blog/list_tag/blog'); ?>';
	var nilaiTag = <?php echo $tags; ?>;
	</script>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 270,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Kategori": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori_blog").empty().load(kategoriBlogSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	
	    $( ".tambah-kategori, .tambah-keyword" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-seo, #tabs-isi" ).tabs();
	
			//keyword id halaman
		    $( "#keyword_id" )
		    .bind( "keydown", function( event ) {
		        if ( event.keyCode === $.ui.keyCode.TAB &&
		                $( this ).data( "autocomplete" ).menu.active ) {
		            event.preventDefault();
		        }
		    })
		    .autocomplete({
		        source: function( request, response ) {
		            $.getJSON( keywordIdURL, {
		                term: extractLast( request.term )
		            }, response );
		        },
		        search: function() {
		            var term = extractLast( this.value );
		            if ( term.length < 2 ) {
		                return false;
		            }
		        },
		        focus: function() {
		            return false;
		        },
		        select: function( event, ui ) {
		            var terms = split( this.value );
		            terms.pop();
		            terms.push( ui.item.value );
		            terms.push( "" );
		            this.value = terms.join( ", " );
		            return false;
		        }
		    });
		
		    //keyword en halaman
		    $( "#keyword_en" )
		    .bind( "keydown", function( event ) {
		        if ( event.keyCode === $.ui.keyCode.TAB &&
		                $( this ).data( "autocomplete" ).menu.active ) {
		            event.preventDefault();
		        }
		    })
		    .autocomplete({
		        source: function( request, response ) {
		            $.getJSON( keywordEnURL, {
		                term: extractLast( request.term )
		            }, response );
		        },
		        search: function() {
		            var term = extractLast( this.value );
		            if ( term.length < 2 ) {
		                return false;
		            }
		        },
		        focus: function() {
		            return false;
		        },
		        select: function( event, ui ) {
		            var terms = split( this.value );
		            terms.pop();
		            terms.push( ui.item.value );
		            terms.push( "" );
		            this.value = terms.join( ", " );
		            return false;
		        }
		    });
	
	});
	
	$(document).ready(function(){
	
		//-------------------------------
		// Tags
		//-------------------------------
		var tag = $('#tag').magicSuggest({
			maxSelection: maxTags,
			dataUrlParams: {Hydra:$("input[name='Hydra']").val()},
			data: tagURL,
			allowFreeEntries: false,
			maxDropHeight: 200
		});

		$(tag).on('load', function(){
	        if(this._dataSet === undefined){
	            this._dataSet = true;
	            tag.setValue(nilaiTag);
	        }
	    });
	
	
		$('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});

		//scroll top
		$(window).scroll(function(){
            if ($(document).scrollTop() > 250) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
		
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(4(){7 d=$(\'#X\');7 i=$(\'#X p\').1E()+1;7 e=$("#1F"),x=$("#1G"),y=$([]).Y(e).Y(x),M=$(".1H");4 10(){1I.1J()}4 q(t){M.1K(t).N("h-k-11");1L(4(){M.B("h-k-11",1M)},1N)}4 O(o,n,a,b){8(o.g().C>b||o.g().C<a){o.N("h-k-r");q("1O "+n+" 1P 1Q 1R "+a+" 12 1S 1T "+b+" 12.");6 f}D{6 u}}4 P(o,a,n){8(!(a.1U(o.g()))){o.N("h-k-r");q(n);6 f}D{6 u}}4 v(a){6 a.v(/,\\s*/)}4 w(a){6 v(a).Q()}$("#l-E").l({1V:f,1W:1X,1Y:1Z,20:f,21:{22:"13 14",23:"13 14",24:15},25:u,26:{"27 R":4(){7 b=u;y.B("h-k-r");b=b&&O(e,"16 R 28",3,17);b=b&&P(e,/^([0-9 a-z A-Z])+$/,"18 19 1a 1b 1c : a-z 0-9");b=b&&O(x,"16 R 29",3,17);b=b&&P(x,/^([0-9 a-z A-Z])+$/,"18 19 1a 1b 1c : a-z 0-9");8(b){$.2a({2b:"2c",F:$(5).m(\'F\'),m:$("#E-S-G").2d(),2e:4(a){8(a.1d===\'2f\'){$("#l-E").l("T");$("#2g").2h().1e(2i)}D{q(a.1d)}},2j:\'2k\',r:10})}},"2l":4(){$(5).l("T");y.g("").B("h-k-r");q("1f G 1g.")}},T:4(){y.g("").B("h-k-r");q("1f G 1g.")}});$(".S-G, .S-2m").2n({2o:{2p:"h-2q-2r-2s"}}).H(4(){7 a=$(5).m(\'F\');$("#l-E").m(\'F\',a).l("2t")});$("#U-2u, #U-2v").U();$("#2w").1h("1i",4(a){8(a.I===$.h.I.1j&&$(5).m("J").1k.1l){a.1m()}}).J({1n:4(a,b){$.1o(2x,{K:w(a.K)},b)},1p:4(){7 a=w(5.j);8(a.C<2){6 f}},1q:4(){6 f},1r:4(a,b){7 c=v(5.j);c.Q();c.L(b.1s.j);c.L("");5.j=c.1t(", ");6 f}});$("#2y").1h("1i",4(a){8(a.I===$.h.I.1j&&$(5).m("J").1k.1l){a.1m()}}).J({1n:4(a,b){$.1o(2z,{K:w(a.K)},b)},1p:4(){7 a=w(5.j);8(a.C<2){6 f}},1q:4(){6 f},1r:4(a,b){7 c=v(5.j);c.Q();c.L(b.1s.j);c.L("");5.j=c.1t(", ");6 f}})});$(1u).2A(4(){7 b=$(\'#2B\').2C({2D:2E,2F:{1v:$("2G[2H=\'1v\']").g()},m:2I,2J:f,2K:2L});$(b).2M(\'1e\',4(){8(5.1w===2N){5.1w=u;b.2O(2P)}});$(\'#2Q\').H(4(){7 a=$(\'#1x\').g();$(\'#1y\').1z();$(\'#1A\').1B();8(a!=""){$(\'#V\').g(\'1C\')}});$(\'#1x\').2R(4(){7 a=$(5).g();8(a!=""){$(\'#V\').g(\'1C\')}});$(\'#2S\').H(4(){$(\'#1y\').1B();$(\'#1A\').1z();$(\'#V\').g(\'2T\')});$(15).2U(4(){8($(1u).1D()>2V){$(\'.W\').2W()}D{$(\'.W\').2X()}});$(\'.W\').H(4(){$("2Y, 2Z").30({1D:0},31);6 f})});',62,188,'||||function|this|return|var|if|||||||false|val|ui||value|state|dialog|data||||updateTips|error|||true|split|extractLast|namaen|allFields|||removeClass|length|else|form|url|kategori|click|keyCode|autocomplete|term|push|tips|addClass|checkLength|checkRegexp|pop|Kategori|tambah|close|tabs|status_ganti|scrollup|addinput|add||expPost|highlight|karakter|center|top|window|Nama|50|Karakter|yang|di|perbolehkan|hanya|status|load|Masukkan|baru|bind|keydown|TAB|menu|active|preventDefault|source|getJSON|search|focus|select|item|join|document|Hydra|_dataSet|userfile|c_gmbr|hide|up_gmbr|show|yes|scrollTop|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|270|width|350|resizable|position|my|at|of|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|kategori_blog|empty|kategoriBlogSelect|dataType|json|Batal|keyword|button|icons|primary|icon|circle|plus|open|seo|isi|keyword_id|keywordIdURL|keyword_en|keywordEnURL|ready|tag|magicSuggest|maxSelection|maxTags|dataUrlParams|input|name|tagURL|allowFreeEntries|maxDropHeight|200|on|undefined|setValue|nilaiTag|ganti_gmbr|change|batal_ganti_gmbr|no|scroll|250|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>
	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>
	
	<div id="showdata">
	</div>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
		echo form_open_multipart($this->config->item('admpath').'/blog/update_formblog', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Update</button>

	    <h1>Update posting <?php echo humanize($blog->judul_id); ?></h1>
	    <input type="hidden" name="id_penulis" id="id_penulis" value="<?php echo set_value('id_penulis',1); ?>" />
	    <input type="hidden" name="id_blog" id="id_blog" value="<?php echo set_value('id_blog',$blog->id); ?>" />
	    <input type="hidden" name="tag_ori" id="tag_ori" value="<?php echo set_value('tag_ori',$tags); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="gambar" id="gambar" value="<?php echo set_value('gambar',$blog->gambar_cover); ?>" />
	    <input type="hidden" name="nama_ori_id" id="nama_ori_id" value="<?php echo set_value('nama_ori_id',$blog->judul_id); ?>" />
	    <input type="hidden" name="nama_ori_en" id="nama_ori_en" value="<?php echo set_value('nama_ori_en',$blog->judul_en); ?>" />

	    <p>Masukkan data yang baru.</p>

		<label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="judul_id" id="judul_id" maxlength="150" style="width:350px;margin-right:20px" value="<?php echo set_value('judul_id',$blog->judul_id); ?>" />
	
	    <label style="margin-right:-40px">Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="judul_en" id="judul_en" maxlength="150" style="width:350px;" value="<?php echo set_value('judul_en',$blog->judul_en); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('judul_id'); ?>
	    <?php echo form_error('judul_en'); ?>
	
	    <div style="clear:left"></div>
	    <label>Headline<span class="small">Apakah Post ini Headline.?</span> </label>
	    <select name="headline" id="headline" style="margin-right:20px">
			<option value="n" <?php echo set_select('headline', 'n',($blog->headline == 'n')?TRUE:FALSE); ?>>Tidak</option>
			<option value="y" <?php echo set_select('headline', 'y',($blog->headline == 'y')?TRUE:FALSE); ?>>Ya</option>
	    </select>
	
	    <label style="margin-right:-20px">Komentar<span class="small">Perbolehkan komentar.</span> </label>
	    <select name="diskusi" id="diskusi" style="margin-right:20px">
			<option value="off" <?php echo set_select('diskusi', 'off',($blog->diskusi == 'off')?TRUE:FALSE); ?>>Tidak</option>
			<option value="on" <?php echo set_select('diskusi', 'on',($blog->diskusi == 'on')?TRUE:FALSE); ?>>Ya</option>
	    </select>

	    <label style="margin-right:-20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="<?php echo $blog->ratting; ?>"><?php echo $blog->ratting; ?></option>
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
	
	    <label>Kategori Post<span class="small">Pilih Kategori</span> </label>
		<select id="kategori_blog" name="kategori_blog">
			<?php foreach ($kat_blog as $items): ?>
			<option value="<?php echo $items->id_kategori; ?>" <?php echo set_select('kategori_blog', $items->id_kategori,($items->id_kategori == $blog->kategori_id)?TRUE:FALSE); ?>>
			&nbsp;&nbsp;<?php echo $items->label_id; ?>&nbsp;//&nbsp;<?php echo $items->label_en; ?>&nbsp;&nbsp;
			</option>
			<?php endforeach; ?>>
		</select>
	    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/blog/tambah_kategori');?>" class="tambah-kategori" title="Tambah Kategori/Tag"></a>
	    <div style="clear:left"></div>
	    <?php echo form_error('ratting'); ?>
	    <?php echo form_error('kategori_blog'); ?>
	    <?php echo form_error('utama'); ?>
	    <?php echo form_error('diskusi'); ?>
	
	    <div id="c_gmbr" style="height:60px;">
	    <button type="button" id="ganti_gmbr" class="ganti">Ganti Cover</button>
	    <label>Gambar Cover<span class="small">Gambar Cover yang terpasang</span> </label>
	    <img src="<?php echo base_url(); ?>_media/blog/thumb/thumb_<?php echo $blog->gambar_cover; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
	    <div style="clear:left"></div>
	    </div>
	
	    <div id="up_gmbr" style="display:none;">
	    <button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Cover</button>
	    <label>Ganti Cover <span class="small">min:800x600, max:1366x1024<br>max-size : 1000KB / 1MB</span> </label>
	    <input type="file" name="userfile" id="userfile" />
	    <br />
		<?php echo form_error('userfile'); ?>
	    <div style="clear:left"></div>
	    </div>
	
	    <div style="clear:left"></div>
		<br />
		    <label>Tag Post<span class="small">Tag untuk post Maks.(<?php echo $this->config->item('max_tag'); ?>)</span> </label>
			<input id="tag" style="width:600px;margin-left:150px;" type="text" name="tag"/>
		<br />
	
		<div style="clear:left"></div>
	    <label>SEO <span class="small">SEO Post</span> </label>
	    <div style="float:left;width:60%;margin-left:10px;">
			<div id="tabs-seo" style="height:200px">
	            <ul>
	                <li><a href="#seo-id">SEO Indonesia</a></li>
	                <li><a href="#seo-en">SEO English</a></li>
	            </ul>
	            <div id="seo-id">
	                <div class="">
						<label>Keyword ID <span class="small">Keyword Indonesia</span> </label>
	                    <input type="text" name="keyword_id" id="keyword_id" maxlength="200" value="<?php echo set_value('keyword_id', (!empty($blog->keyword_id))?$blog->keyword_id.' ,':''); ?>" />
	                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
	                    <label>Description ID <span class="small">Description Indonesia</span> </label>
	                    <textarea name="description_id" id="description_id" maxlength="200" style="height:80px;"><?php echo set_value('description_id',$blog->deskripsi_id); ?></textarea>
	                </div>
	            </div>
	            <div id="seo-en">
	                 <div class="">
						<label>Keyword EN <span class="small">Keyword English</span> </label>
	                    <input type="text" name="keyword_en" id="keyword_en" maxlength="200" value="<?php echo set_value('keyword_en',(!empty($blog->keyword_en))?$blog->keyword_en.' ,':''); ?>" />
	                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
	                    <label>Description EN<span class="small">Description English</span> </label>
	                    <textarea name="description_en" id="description_en" maxlength="200" style="height:80px;"><?php echo set_value('description_en',$blog->deskripsi_en); ?></textarea>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div style="clear:left"></div>
	    <?php echo form_error('keyword_id'); ?>
	    <?php echo form_error('description_id'); ?>
	    <?php echo form_error('keyword_en'); ?>
	    <?php echo form_error('description_en'); ?>
	
	    <br style="clear:left" />
		<label>Status<span class="small">Status postingan.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('headline', 'on',($blog->status == 'on')?TRUE:FALSE); ?>>Publish</option>
			<option value="off" <?php echo set_select('headline', 'off',($blog->status == 'off')?TRUE:FALSE); ?>>Offline</option>
			<option value="draft" <?php echo set_select('headline', 'draft',($blog->status == 'draft')?TRUE:FALSE); ?>>Draft</option>
	    </select>
	
	    <label style="margin-left:20px;margin-right:-40px">Embed Album<span class="small">Pilih Album</span> </label>
	    <select name="embed_album" id="embed_album">
	    <option value="0">-- Tidak ada --</option>
		<?php
		if(!empty($parent_album)){
		foreach($parent_album as $items)
		{
			if($blog->embed_id_album === $items['id'])
			echo '<option value="'.$items['id'].'" selected="selected"'.'>'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
			else
			echo '<option value="'.$items['id'].'" >'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
		}
		}
		?>
	    </select>
	
	    <label style="margin-left:20px;margin-right:-40px">Embed Video<span class="small">Pilih Video</span> </label>
	    <select name="embed_video" id="embed_video">
	    <option value="0">-- Tidak ada --</option>
		<?php
		if(!empty($parent_video)){
		foreach($parent_video as $items)
		{
			if($blog->embed_id_video === $items['id'])
			echo '<option value="'.$items['id'].'" selected="selected"'.'>'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
			else
			echo '<option value="'.$items['id'].'" >'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
		}
		}
		?>
	    </select>
	    
	    <div style="clear:left"></div>
	    <?php echo form_error('status'); ?>
	    <?php echo form_error('embed_album'); ?>
	    <?php echo form_error('embed_video'); ?>
	    
	    <label>Konten<span class="small">Konten Post</span></label>
	    <div style="float:left;padding:3px;margin-left:10px;border:1px solid #ADD8E6">
	    <label style="">Kode Snippet <span class="small">Ceklist jika ada kode snippet</span></label> <input name="snippet" type="checkbox" value="y" <?php echo set_checkbox('snippet', 'y', ($blog->snippet==='y')?TRUE:FALSE); ?> />
	    </div>
	    <br style="clear:left" />
	    <?php echo form_error('isi_id'); ?>
	    <?php echo form_error('isi_en'); ?>

	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="isi_id" id="isi_id" ><?php echo set_value('isi_id',$blog->isi_id); ?></textarea>
			<?php echo display_ckeditor($isi_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="isi_en" id="isi_en" ><?php echo set_value('isi_en',$blog->isi_en); ?></textarea>
			<?php echo display_ckeditor($isi_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	</body>
</html>

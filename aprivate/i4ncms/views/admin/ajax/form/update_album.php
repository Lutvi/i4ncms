<!DOCTYPE html>
<html>
	<head>
	<title>Update Album <?php echo set_value('nama_ori_id',$album->nama_id); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	<script>
	var kategoriAlbumSelect = '<?php echo base_url($this->config->item('admpath').'/album/list_select_kategori'); ?>';
	var maxTags = <?php echo $this->config->item('max_tag'); ?>;
	var tagURL = '<?php echo base_url($this->config->item('admpath').'/album/list_tag/album'); ?>';
	var nilaiTag = <?php echo $tags; ?>;
	</script>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 250,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Kategori": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori_album").empty().load(kategoriAlbumSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	
	    $( ".tambah-kategori" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-isi" ).tabs();
	
	});
	
	$(document).ready(function(){
	
		//-------------------------------
		// Tags
		//-------------------------------
		var tag = $('#tag').magicSuggest({
			maxSelection: maxTags,
			dataUrlParams: {Hydra:$("input[name='Hydra']").val()},
			data: tagURL,
			allowFreeEntries: false,
			maxDropHeight: 200
		});
	
		$(tag).on('load', function(){
	        if(this._dataSet === undefined){
	            this._dataSet = true;
	            tag.setValue(nilaiTag);
	        }
	    });

	    $('#status_ganti').val('no');
	
	
		$('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});
	
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 100) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
		
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(2(){5 c=$(\'#J\');5 i=$(\'#J p\').1h()+1;5 d=$("#1i"),q=$("#1j"),r=$([]).K(d).K(q),C=$(".1k");2 L(){1l.1m()}2 f(t){C.1n(t).D("6-8-M");1o(2(){C.s("6-8-M",1p)},1q)}2 E(o,n,a,b){7(o.4().N>b||o.4().N<a){o.D("6-8-g");f("1r "+n+" 1s 1t 1u "+a+" O 1v 1w "+b+" O.");k h}u{k l}}2 F(o,a,n){7(!(a.1x(o.4()))){o.D("6-8-g");f(n);k h}u{k l}}$("#e-v").e({1y:h,1z:1A,1B:1C,1D:h,1E:{1F:"P Q",1G:"P Q",1H:R},1I:l,1J:{"1K G":2(){5 b=l;r.s("6-8-g");b=b&&E(d,"S G 1L",3,T);b=b&&F(d,/^([0-9 a-z A-Z])+$/,"U V W X Y : a-z 0-9");b=b&&E(q,"S G 1M",3,T);b=b&&F(q,/^([0-9 a-z A-Z])+$/,"U V W X Y : a-z 0-9");7(b){$.1N({1O:"1P",w:$(j).m(\'w\'),m:$("#v-10-x").1Q(),1R:2(a){7(a.11===\'1S\'){$("#e-v").e("H");$("#1T").1U().12(1V)}u{f(a.11)}},1W:\'1X\',g:L})}},"1Y":2(){$(j).e("H");r.4("").s("6-8-g");f("13 x 14.")}},H:2(){r.4("").s("6-8-g");f("13 x 14.")}});$(".10-x").1Z({20:{21:"6-22-23-24"}}).y(2(){5 a=$(j).m(\'w\');$("#e-v").m(\'w\',a).e("25")});$("#15-26").15()});$(16).27(2(){5 b=$(\'#28\').29({2a:2b,2c:{17:$("2d[2e=\'17\']").4()},m:2f,2g:h,2h:2i});$(b).2j(\'12\',2(){7(j.18===2k){j.18=l;b.2l(2m)}});$(\'#B\').4(\'19\');$(\'#2n\').y(2(){5 a=$(\'#1a\').4();$(\'#1b\').1c();$(\'#1d\').1e();7(a!=""){$(\'#B\').4(\'1f\')}});$(\'#1a\').2o(2(){5 a=$(j).4();7(a!=""){$(\'#B\').4(\'1f\')}});$(\'#2p\').y(2(){$(\'#1b\').1e();$(\'#1d\').1c();$(\'#B\').4(\'19\')});$(R).2q(2(){7($(16).1g()>2r){$(\'.I\').2s()}u{$(\'.I\').2t()}});$(\'.I\').y(2(){$("2u, 2v").2w({1g:0},2x);k h})});',62,158,'||function||val|var|ui|if|state||||||dialog|updateTips|error|false||this|return|true|data||||namaen|allFields|removeClass||else|form|url|kategori|click|||status_ganti|tips|addClass|checkLength|checkRegexp|Kategori|close|scrollup|addinput|add|expPost|highlight|length|karakter|center|top|window|Nama|50|Karakter|yang|di|perbolehkan|hanya||tambah|status|load|Masukkan|baru|tabs|document|Hydra|_dataSet|no|userfile|c_gmbr|hide|up_gmbr|show|yes|scrollTop|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|250|width|350|resizable|position|my|at|of|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|kategori_album|empty|kategoriAlbumSelect|dataType|json|Batal|button|icons|primary|icon|circle|plus|open|isi|ready|tag|magicSuggest|maxSelection|maxTags|dataUrlParams|input|name|tagURL|allowFreeEntries|maxDropHeight|200|on|undefined|setValue|nilaiTag|ganti_gmbr|change|batal_ganti_gmbr|scroll|100|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>
	
	<div id="showdata">
	</div>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
		echo form_open_multipart($this->config->item('admpath').'/album/update_formalbum', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Update</button>
	
	    <h1>Update Album <?php echo set_value('nama_ori_id',$album->nama_id); ?></h1>
	    <input type="hidden" name="id_album" id="id_album" value="<?php echo set_value('id_album',$album->id); ?>" />
	    <input type="hidden" name="tag_ori" id="tag_ori" value="<?php echo set_value('tag_ori',$tags); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="gambar" id="gambar" value="<?php echo set_value('gambar',$album->gambar_cover); ?>" />
	    <input type="hidden" name="nama_ori_id" id="nama_ori_id" value="<?php echo set_value('nama_ori_id',$album->nama_id); ?>" />
	    <input type="hidden" name="nama_ori_en" id="nama_ori_en" value="<?php echo set_value('nama_ori_en',$album->nama_en); ?>" />
	    <p>Masukkan data Album yang baru.</p>
	
		<label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:280px;margin-right:20px" value="<?php echo set_value('nama_id',$album->nama_id); ?>" />
	
	    <label style="margin-right:-40px">Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:280px;" value="<?php echo set_value('nama_en',$album->nama_en); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	    <div style="clear:left"></div>
	    
	    <label>Kategori Album<span class="small">Pilih Kategori</span> </label>
		<select id="kategori_album" name="kategori_album">
			<?php foreach ($kat_album as $items): ?>
			<option value="<?php echo $items->id_kategori; ?>" <?php echo set_select('kategori_album', $items->id_kategori,($items->id_kategori == $album->kategori_id)?TRUE:FALSE); ?>>
			&nbsp;&nbsp;<?php echo $items->label_id; ?>&nbsp;//&nbsp;<?php echo $items->label_en; ?>&nbsp;&nbsp;
			</option>
			<?php endforeach; ?>
		</select>
	    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/album/tambah_kategori');?>" class="tambah-kategori" title="Tambah Kategori/Tag"></a>
	    
	    <label style="margin-left:20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="<?php echo $album->ratting; ?>"><?php echo $album->ratting; ?></option>
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
		<div style="clear:left"></div>
		<?php echo form_error('ratting'); ?>
		<?php echo form_error('kategori_album'); ?>
	
	    <div id="c_gmbr" style="height:60px;">
	    <button type="button" id="ganti_gmbr" class="ganti">Ganti Cover</button>
	    <label>Gambar Cover<span class="small">Gambar Cover yang terpasang</span> </label>
	    <img src="<?php echo base_url(); ?>_media/album/thumb/thumb_<?php echo $album->gambar_cover; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
	    <div style="clear:left"></div>
	    </div>
	
	    <div id="up_gmbr" style="display:none;">
	    <button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Cover</button>
	    <label>Ganti Cover <span class="small">min:800x600, max:1366x1024<br>max-size : 1000KB / 1MB</span> </label>
	    <input type="file" name="userfile" id="userfile" />
	    <br />
		<?php echo form_error('userfile'); ?>
	    <div style="clear:left"></div>
	    </div>
	
	    <div style="clear:left"></div>
	
	<br />
	    <label>Tag Album<span class="small">Tag untuk album Maks.(<?php echo $this->config->item('max_tag'); ?>)</span> </label>
		<input id="tag" style="width:600px;margin-left:150px;" type="text" name="tag"/>
	<div style="clear:both;"></div>
	
	<br style="clear:left" />
	    <label>Deskripsi<span class="small">Deskripsi Album</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id',$album->ket_id); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en',$album->ket_en); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Banner Header</title>

	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		//Gambar
	    $('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});
		
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 100) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});

		$( "#tabs-isi" ).tabs();
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(1(){$(\'#h\').4(1(){8 3=$(\'#9\').2();$(\'#a\').b();$(\'#c\').d();5(3!=""){$(\'#6\').2(\'e\')}});$(\'#9\').i(1(){8 3=$(j).2();5(3!=""){$(\'#6\').2(\'e\')}});$(\'#k\').4(1(){$(\'#a\').d();$(\'#c\').b();$(\'#6\').2(\'l\')});$(m).n(1(){5($(o).f()>p){$(\'.7\').q()}r{$(\'.7\').s()}});$(\'.7\').4(1(){$("t, u").v({f:0},w);x y});$("#g-z").g()});',36,36,'|function|val|file|click|if|status_ganti|scrollup|var|userfile|c_gmbr|hide|up_gmbr|show|yes|scrollTop|tabs|ganti_gmbr|change|this|batal_ganti_gmbr|no|window|scroll|document|100|fadeIn|else|fadeOut|html|body|animate|600|return|false|isi'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/hbanner'),'Kembali ke List', $attr); ?>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/hbanner/update_form_hbanner', $attributes);
	    ?>

	    <input type="hidden" name="id_banner" id="id_banner" maxlength="11" value="<?php echo set_value('id_banner', $banner->id); ?>" />
	    <input type="hidden" name="nama_ori" id="nama_ori" maxlength="50" value="<?php echo set_value('nama_ori', $banner->nama_id); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="img_src" id="img_src" value="<?php echo set_value('img_src',$banner->img_src); ?>" />

	<button type="submit" class="btn-aksi">Simpan</button>
	
	    <h1>Update Banner Header <?php echo humanize($banner->nama_id); ?></h1>
	    <p>Masukkan data Banner Header yang baru.</p>
	    
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id', $banner->nama_id); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="50" style="width:300px;" value="<?php echo set_value('nama_en', $banner->nama_en); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>

		<div id="c_gmbr" style="height:60px;">
			<button type="button" id="ganti_gmbr" class="ganti">Ganti Gambar</button>
			<label>Gambar Banner<span class="small">Gambar Banner yang terpasang</span> </label>
			<img src="<?php echo base_url(); ?>_media/banner-header/thumb_<?php echo $banner->img_src; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
			<div style="clear:left"></div>
	    </div>

		<div id="up_gmbr" style="display:none;">
		<button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Gambar</button>
	    <label>Ganti Gambar Banner<span class="small">Ukuran harus:<u>520x320</u><br>max-size : 1MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" id="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>
		</div>

		<br />

		<label>URL Target <span class="small">Link target URL ke konten</span> </label>
	    <input type="text" name="url_target" id="url_target" maxlength="200" style="width:65%;" value="<?php echo set_value('url_target', $banner->url_target); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('url_target'); ?>

	    <div style="clear:left;"></div>
	
	    <label>Status<span class="small">Status banner.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on',($banner->status == 'on')?TRUE:FALSE); ?>>On</option>
			<option value="off" <?php echo set_select('status','off',($banner->status == 'off')?TRUE:FALSE); ?>>Off</option>
	    </select>
	    <div style="clear:left"></div>
		<?php echo form_error('status'); ?>
	
	    <label>Info Singkat<span class="small">Info Singkat untuk Banner</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Indonesia</a></li>
			<li><a href="#isi-en">English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id', $banner->ket_id); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en', $banner->ket_en); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
		
	    <div style="clear:both; height:10px;"></div>
	  </form>

	</div>

	</body>
</html>

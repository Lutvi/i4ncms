<!DOCTYPE html>
<html>
	<head>
	<title>detail Pelanggan</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	
	<script>
	var sts = '<?php echo $sts; ?>';
	
		function expForm()
		{
			window.location.reload();
		}
	
	$(function() {
		$( "#ubah-alamat" ).buttonset();
	
		$("#status-ubah1").button({
	        icons: {
	            primary: "ui-icon-clipboard"
	        }
	    })
	    .click(function() {
	        $('#formAlamat').hide();
			$('#txtAlamat').slideDown();
	    });
	
		$("#status-ubah2").button({
	        icons: {
	            primary: "ui-icon-wrench"
	        }
	    })
	    .click(function() {
	        $('#txtAlamat').hide();
			$('#formAlamat').slideDown();
	    });
	
	    //list-provinsi => kota
	    $('select#provinsi').change(function(e){
	        e.preventDefault();
	        var postURL = $(this).attr('data-url');
	        
	        $.ajax({
	            type: "POST",
	            url: postURL,
	            data: ({idp:$(this).val(),Hydra:$('input[name="Hydra"]').val()}),
	            success: function(data){
	                $('select#wilayah').empty().html(data);
	            },
	            error : expForm
	        });
	    });
	    
	});
	
	$(document).ready(function(){
	
	    if($("#status-ubah2").is(':checked')) {
			$('#txtAlamat').hide();
			$('#formAlamat').slideDown();
	    } else {
			$('#formAlamat').hide();
			$('#txtAlamat').slideDown();
	    }
	
	});
	</script>
	
	</head>
	<body>
	
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open($this->config->item('admpath').'/pelanggan/update_detail_pelanggan', $attributes);
	    ?>
	    <h1>Update Data Pelanggan</h1>
	    <p>Masukkan data pelanggan yang baru.</p>
	
	    <input type="hidden" name="pid" id="pid" maxlength="11" value="<?php echo set_value('pid', $detail_pelanggan->id); ?>" />
	
	    <button type="submit" class="btn-aksi">Update</button>
	    
	    <label>ID Pelanggan<span class="small">User Id untuk pelanggan</span> </label>
	    <input type="text" name="uname_id" id="uname_id" readonly="readonly" maxlength="150" style="width:400px" value="<?php echo set_value('uname_id', dekodeString($detail_pelanggan->id_cust_uname)); ?>" />
	    <br />
		<?php echo form_error('nama_id'); ?>
	    <div style="clear:left"></div>
	    
	    <label>Status RSS <span class="small">Status RSS/Newsletter.</span> </label>
	    <select id="status_rss" name="status_rss" style="margin-right:30px">
	        <option value="off" <?php echo set_select('status_rss', $detail_pelanggan->status_rss, (set_value('status_rss') == 'off' || $detail_pelanggan->status_rss == 'off')?TRUE:FALSE); ?> >&nbsp;Off&nbsp;</option>
	        <option value="on" <?php echo set_select('status_rss', $detail_pelanggan->status_rss, (set_value('status_rss') == 'on' || $detail_pelanggan->status_rss == 'on')?TRUE:FALSE); ?> >&nbsp;On&nbsp;</option>
	    </select>
	
	    <label>Status Pelanggan <span class="small">Status keanggotaan.</span> </label>
	    <select id="status_cust" name="status_cust" style="margin-right:30px">
	        <option value="aktif" <?php echo set_select('status_cust', $detail_pelanggan->status_cust, (set_value('status_cust') == 'aktif' || $detail_pelanggan->status_cust == 'aktif')?TRUE:FALSE); ?> >&nbsp;Aktif&nbsp;</option>
	        <option value="banned" <?php echo set_select('status_cust', $detail_pelanggan->status_cust, (set_value('status_cust') == 'banned' || $detail_pelanggan->status_cust == 'banned')?TRUE:FALSE); ?> >&nbsp;Banned&nbsp;</option>
	        <option value="vakum" <?php echo set_select('status_cust', $detail_pelanggan->status_cust, (set_value('status_cust') == 'vakum' || $detail_pelanggan->status_cust == 'vakum')?TRUE:FALSE); ?> >&nbsp;Vakum&nbsp;</option>
	    </select>
	
	    <label>Prioritas <span class="small">Prioritas untuk pelanggan.</span> </label>
	    <select id="prioritas_cust" name="prioritas_cust">
	        <option value="utama" <?php echo set_select('prioritas_cust', $detail_pelanggan->prioritas_cust, (set_value('prioritas_cust') == 'utama' || $detail_pelanggan->prioritas_cust == 'utama')?TRUE:FALSE); ?> >&nbsp;Utama&nbsp;</option>
	        <option value="menengah" <?php echo set_select('prioritas_cust', $detail_pelanggan->prioritas_cust, (set_value('prioritas_cust') == 'menengah' || $detail_pelanggan->prioritas_cust == 'menengah' )?TRUE:FALSE); ?> >&nbsp;Menengah&nbsp;</option>
	        <option value="normal" <?php echo set_select('prioritas_cust', $detail_pelanggan->prioritas_cust, (set_value('prioritas_cust') == 'normal' || $detail_pelanggan->prioritas_cust == 'normal')?TRUE:FALSE); ?> >&nbsp;Normal&nbsp;</option>
	        <option value="biasa" <?php echo set_select('prioritas_cust', $detail_pelanggan->prioritas_cust, (set_value('prioritas_cust') == 'biasa'  || $detail_pelanggan->prioritas_cust == 'biasa')?TRUE:FALSE); ?> >&nbsp;Biasa&nbsp;</option>
	    </select>
	    
	    <br />
	    <?php echo form_error('status_rss'); ?>
	    <?php echo form_error('status_cust'); ?>
	    <?php echo form_error('prioritas_cust'); ?>
		<div style="clear:left"></div>
		
	    <label style="margin-right:10px">Detail Alamat <span class="small">Detail Alamat pelanggan.</span> </label>
	    <div id="ubah-alamat" style="font-size:12px">
			<input type="radio" id="status-ubah1" name="status-ubah" value="0" <?php echo (set_value('status-ubah')=='0' || set_value('status-ubah')== NULL)?'checked="checked"':''; ?> /><label for="status-ubah1">Detail</label>
			<input type="radio" id="status-ubah2" name="status-ubah" value="1" <?php echo (set_value('status-ubah')=='1')?'checked="checked"':''; ?> /><label for="status-ubah2">Ubah</label>
		</div>
	    <div style="clear:left"></div>
	    <div id="txtAlamat" style="background:#FFFFFF;border:1px solid #90C0EF;padding:10px;margin:10px auto;">
			<?php foreach ($alamat as $isi) : ?>
			<div style="margin-top:3px;text-align:left">
			<?php echo $isi; ?>
			</div>
			<?php endforeach; ?>
		</div>
	
		<div id="formAlamat" style="display:none;margin:10px auto;">
			<label>Nama Lengkap<span class="small">Nama lengkap pelanggan.</span> </label>
			<input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="150" style="width:400px" value="<?php echo set_value('nama_lengkap', humanize(dekodeString($detail_pelanggan->nama_lengkap))); ?>" />
			<br />
			<?php echo form_error('nama_lengkap'); ?>
			<div style="clear:left"></div>
			
			<label>No Telpon <span class="small">No telpon pelanggan.</span> </label>
			<input type="text" name="no_telpon" id="no_telpon"  maxlength="14" style="width:120px" value="<?php echo set_value('no_telpon', $alamat['fnotlp']); ?>" />
			
			<label style="margin-left:20px;width:80px;">Email <span class="small">Email pelanggan.</span> </label>
			<input type="text" name="email" id="email" maxlength="150" style="width:200px" value="<?php echo set_value('email', dekodeString($detail_pelanggan->email_cust)); ?>" />
			
			<label style="margin-left:20px;width:100px;">Kode Pos <span class="small">Kode Pos pelanggan.</span> </label>
			<input type="text" name="kode_pos" id="kode_pos"  maxlength="10" style="width:60px" value="<?php echo set_value('kode_pos', $alamat['fkdpos']); ?>" />
	
			<div style="clear:left"></div>
			<?php echo form_error('no_telpon'); ?>
			<?php echo form_error('email'); ?>
			<?php echo form_error('kode_pos'); ?>
			<div style="clear:left"></div>
	
			<label for="negara">Negara <br><span class="small">Negara pelanggan</span> </label>
			<select id="negara" name="negara">
				<option value="indonesia" <?php echo set_select('negara', 'indonesia', (set_value('negara') == 'indonesia')?TRUE:FALSE); ?> >&nbsp;Indonesia&nbsp;</option>
			</select>
	
			<label style="margin-left:20px;width:90px;">Provinsi <span class="small">Provinsi pelanggan.</span> </label>
			<select style="width:150px" id="provinsi" name="provinsi" data-url="<?php echo site_url('list_wilayah_admin'); ?>">
				<option value="" <?php echo set_select('provinsi', '', (set_value('provinsi') == '')?TRUE:FALSE); ?> >&nbsp;----&nbsp;</option>
				<?php
				foreach ($list_provinsi as $nama) {
				?>
				    <option value="<?php echo $nama->id_provinsi; ?>" <?php echo set_select('provinsi', $nama->id_provinsi, (set_value('provinsi', $detail_pelanggan->id_provinsi) == $nama->id_provinsi)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->provinsi); ?>&nbsp;</option>
				<?php
				}
				?>
			</select>
		
			<label style="margin-left:20px;width:90px;">Wilayah <span class="small">Wilayah pelanggan.</span> </label>
			<select style="width:170px" id="wilayah" name="wilayah">
				<option value="" <?php echo set_select('wilayah', '', (set_value('wilayah') == '')?TRUE:FALSE); ?> >&nbsp;----&nbsp;</option>
				<?php
				foreach ($list_wilayah as $nama) {
				?>
					<option value="<?php echo $nama->id_kab_kota; ?>" <?php echo set_select('wilayah', $nama->id_kab_kota, (set_value('wilayah',$detail_pelanggan->id_wilayah) == $nama->id_kab_kota)?TRUE:FALSE); ?> >&nbsp;<?php echo humanize($nama->kab_kota); ?>&nbsp;</option>
				<?php
				}
				?>
			</select>
	
			<br />
			<?php echo form_error('negara'); ?>
			<?php echo form_error('provinsi'); ?>
			<?php echo form_error('wilayah'); ?>
			<div style="clear:left"></div>
			
			<label for="alamat">Alamat Rumah <br><span class="small">Alamat Rumah Pelanggan</span> </label>
			<textarea spellcheck="false" name="alamat_rumah" id="alamat_rumah" style="height:90px;padding:5px"><?php echo set_value('alamat_rumah',str_replace("<br />", "\n",$alamat['frumah'])); ?></textarea>
			<div style="clear:left"></div>
			<?php echo form_error('alamat_rumah'); ?>
		</div>
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	
	</body>
</html>

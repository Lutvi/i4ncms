<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Album</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	</style>

	<script>
	var kategoriAlbumSelect = '<?php echo base_url($this->config->item('admpath').'/album/list_select_kategori'); ?>';
	var maxTags = <?php echo $this->config->item('max_tag'); ?>;
	var tagURL = '<?php echo base_url($this->config->item('admpath').'/album/list_tag/album'); ?>';
	</script>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 250,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Kategori": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori_album").empty().load(kategoriAlbumSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	
	    $( ".tambah-kategori" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-isi" ).tabs();
	
		//-------------------------------
		// Tags
		//-------------------------------
		var tag = $('#tag').magicSuggest({
			maxSelection: maxTags,
			dataUrlParams: {Hydra:$("input[name='Hydra']").val()},
			data: tagURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'tag'
		});
	
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 100) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(2(){7 c=$(\'#I\');7 i=$(\'#I p\').19()+1;7 d=$("#1a"),m=$("#1b"),q=$([]).J(d).J(m),y=$(".1c");2 K(){1d.1e()}2 8(t){y.1f(t).B("4-5-L");1g(2(){y.r("4-5-L",1h)},1i)}2 C(o,n,a,b){j(o.f().M>b||o.f().M<a){o.B("4-5-g");8("1j "+n+" 1k 1l 1m "+a+" N 1n 1o "+b+" N.");k h}s{k u}}2 D(o,a,n){j(!(a.1p(o.f()))){o.B("4-5-g");8(n);k h}s{k u}}$("#6-v").6({1q:h,1r:1s,1t:1u,1v:h,1w:{1x:"O P",1y:"O P",1z:Q},1A:u,1B:{"1C E":2(){7 b=u;q.r("4-5-g");b=b&&C(d,"R E 1D",3,S);b=b&&D(d,/^([0-9 a-z A-Z])+$/,"T U V W X : a-z 0-9");b=b&&C(m,"R E 1E",3,S);b=b&&D(m,/^([0-9 a-z A-Z])+$/,"T U V W X : a-z 0-9");j(b){$.1F({1G:"1H",w:$(F).l(\'w\'),l:$("#v-Y-x").1I(),1J:2(a){j(a.10===\'1K\'){$("#6-v").6("G");$("#1L").1M().1N(1O)}s{8(a.10)}},1P:\'1Q\',g:K})}},"1R":2(){$(F).6("G");q.f("").r("4-5-g");8("11 x 12.")}},G:2(){q.f("").r("4-5-g");8("11 x 12.")}});$(".Y-x").1S({1T:{1U:"4-1V-1W-1X"}}).13(2(){7 a=$(F).l(\'w\');$("#6-v").l(\'w\',a).6("1Y")});$("#14-1Z").14();7 e=$(\'#15\').20({21:22,23:{16:$("24[17=\'16\']").f()},l:25,26:h,27:28,17:\'15\'});$(Q).29(2(){j($(2a).18()>2b){$(\'.H\').2c()}s{$(\'.H\').2d()}});$(\'.H\').13(2(){$("2e, 2f").2g({18:0},2h);k h})});',62,142,'||function||ui|state|dialog|var|updateTips|||||||val|error|false||if|return|data|namaen||||allFields|removeClass|else||true|form|url|kategori|tips|||addClass|checkLength|checkRegexp|Kategori|this|close|scrollup|addinput|add|expPost|highlight|length|karakter|center|top|window|Nama|50|Karakter|yang|di|perbolehkan|hanya|tambah||status|Masukkan|baru|click|tabs|tag|Hydra|name|scrollTop|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|250|width|350|resizable|position|my|at|of|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|kategori_album|empty|load|kategoriAlbumSelect|dataType|json|Batal|button|icons|primary|icon|circle|plus|open|isi|magicSuggest|maxSelection|maxTags|dataUrlParams|input|tagURL|allowFreeEntries|maxDropHeight|200|scroll|document|100|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/dialog_kategori_box'); ?>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/album/add_album', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Simpan</button>
	
	    <h1>Tambah Album baru</h1>
	    <p>Masukkan data Album yang baru.</p>
	    
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
	    <div style="clear:left"></div>
	
	
		<div style="clear:left"></div>
	    <label>Album Utama<span class="small">Tampilkan Album ini di Headline.</span> </label>
	    <select name="utama" id="utama" style="margin-right:20px">
			<option value="off">Bukan</option>
			<option value="on">Ya</option>
	    </select>
	
	    <label style="margin-right:-20px">Komentar<span class="small">Perbolehkan komentar.</span> </label>
	    <select name="diskusi" id="diskusi" style="margin-right:20px">
			<option value="off">Tidak</option>
			<option value="on">Ya</option>
	    </select>
	    <div style="clear:left"></div>
	    <?php echo form_error('utama'); ?>
	    <?php echo form_error('diskusi'); ?>
	
		<label>Kategori Album<span class="small">Pilih Kategori</span> </label>
	    <select name="kategori_album" id="kategori_album">
		<?php
		foreach($kat_album as $items)
		{
			echo '<option value="'.$items->id_kategori.'" '.set_select('kategori_album', $items->id_kategori).'>'.$items->label_id.' // '.$items->label_en.'</option>';
		}
		?>
	    </select>
	    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/album/tambah_kategori');?>" class="tambah-kategori" title="Tambah Kategori/Tag"></a>
        
        <label style="margin-left:20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
		<div style="clear:left"></div>
		<?php echo form_error('ratting'); ?>
	    <?php echo form_error('kategori_album'); ?>
	
	
	    <label>Gambar Cover<span class="small">min:800x600, max:1366x1024<br>max-size : 1000KB / 1MB</span> </label>
	    <input type="file" name="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>
	
	<br />
	    <label>Tag Album<span class="small">Tag untuk album Maks.(<?php echo $this->config->item('max_tag'); ?>)</span> </label>
		<input id="tag" style="width:600px;margin-left:150px;" type="text" name="tag"/>
	<br />
		<div style="clear:left;"></div>
	
	    <label>Deskripsi<span class="small">Deskripsi Album</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id" ><?php echo set_value('ket_id',''); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en" ><?php echo set_value('ket_en',''); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
		
	    <div style="clear:both; height:10px;"></div>
	  </form>
	
	</div>

	</body>
</html>

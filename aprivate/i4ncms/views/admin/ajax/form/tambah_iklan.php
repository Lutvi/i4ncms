<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Banner Iklan</title>

	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 100) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});

		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});

		$( "#tgl_terbit,#tgl_berakhir" ).datepicker({
			showAnim: 'slideDown',
			changeMonth: true,
			changeYear: true
		});
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	
	</script>
	<?php endif; ?>

	</head>
	<body>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/banner/add_iklan', $attributes);
	    ?>
		<button type="submit" class="btn-aksi">Simpan</button>
	
	    <h1>Tambah Banner Iklan baru</h1>
	    <p>Masukkan data Banner Iklan yang baru.</p>

	    <label>Nama Pengiklan<span class="small">Nama pemasang iklan</span> </label>
	    <input type="text" name="nama_pengiklan" id="nama_pengiklan" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_pengiklan'); ?>" />

	    <label style="margin-right:-40px">Tarif <span class="small">Tarif pemasangan iklan</span> </label>
	    <input type="text" name="kredit" id="kredit" maxlength="11" style="width:300px;" value="<?php echo set_value('kredit'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_pengiklan'); ?>
	    <?php echo form_error('kredit'); ?>

	    <label>URL Iklan <span class="small">Link target URL iklan</span> </label>
	    <input type="text" name="url_target" id="url_target" maxlength="200" style="width:65%;" value="<?php echo set_value('url_target'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('url_target'); ?>

	    <label>Gambar Iklan<span class="small">ideal:960x640, max:1366x768<br>max-size : 1MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" />

	    <label style="margin-left:25px;">Posisi Iklan<span class="small">Posisi banner iklan di website.</span> </label>
	    <select name="posisi" id="posisi">
			<option value="">--- Pilih Posisi ---</option>
			<option value="atas" <?php echo set_select('posisi','atas'); ?>>Atas</option>
			<option value="sidebar" <?php echo set_select('posisi','sidebar'); ?>>Sidebar</option>
			<option value="konten_atas" <?php echo set_select('posisi','konten_atas'); ?>>Konten Atas</option>
			<option value="konten_bawah" <?php echo set_select('posisi','konten_bawah'); ?>>Konten Bawah</option>
	    </select>
	    <div style="clear:left"></div>
	    <?php echo form_error('userfile'); ?>
	    <?php echo form_error('posisi'); ?>
	    
	    <label>Nama ID <span class="small">Nama iklan ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label style="margin-right:-40px">Nama EN <span class="small">Nama iklan EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="50" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>

	    <label>Tgl Mulai <span class="small">Tanggal mulai iklan</span> </label>
	    <input type="text" name="tgl_terbit" id="tgl_terbit" maxlength="50" style="width:80px;margin-right:20px" value="<?php echo set_value('tgl_terbit'); ?>" />
	
	    <label style="margin-right:-40px">Tgl Berakhir <span class="small">Tanggal berakhir iklan</span> </label>
	    <input type="text" name="tgl_berakhir" id="tgl_berakhir" maxlength="50" style="width:80px;" value="<?php echo set_value('tgl_berakhir'); ?>" />

	    <label style="margin-left:25px;margin-right:-80px">Status<span class="small">Status iklan.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on'); ?>>On</option>
			<option value="off" <?php echo set_select('status','off'); ?>>Off</option>
	    </select>

	    <div style="clear:left"></div>
	    <?php echo form_error('tgl_terbit'); ?>
	    <?php echo form_error('tgl_berakhir'); ?>
	    <?php echo form_error('status'); ?>
		
	    <div style="clear:both; height:10px;"></div>
	  </form>

	</div>

	</body>
</html>

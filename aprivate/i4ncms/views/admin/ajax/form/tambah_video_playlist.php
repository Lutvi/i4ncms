<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Playlist</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<script>
	$(function(){
	    $( "#tabs-isi" ).tabs();
	});
	</script>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	
	<div id="stylized" class="myformIframe">
	
	<?php if(is_array($parent_video)): ?>
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/video/add_playlist', $attributes);
	    ?>
	
	    <button type="submit" class="btn-aksi">Simpan</button>
	    <h1>Tambah playlist baru</h1>
	    <p>Masukkan data playlist yang baru.</p>
	    
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
		<label>Video<span class="small">Pilih Video</span> </label>
	    <select name="parent_video" id="parent_video">
		<?php
		foreach($parent_video as $items)
		{
			echo '<option value="'.$items['id'].'">'.$items['nama_id'].' | '.$items['nama_en'].'</option>';
		}
		?>
	    </select>
	    <?php echo form_error('parent_album'); ?>
	    <div style="clear:left"></div>
	<?php else: ?>

		<div class="kelist">
	        <?php 
	        $attr = array('class' => 'txtkelist');
	        echo anchor(site_url($this->config->item('admpath').'/video/tabel_playlist/'.set_value('parent_video', $parent_video)),'Kembali ke List', $attr); ?>
		</div>
		<?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/video/add_playlist_video', $attributes);
	    ?>
	    <h1>Tambah playlist</h1>
	    <p>Masukkan data playlist yang baru.</p>

	    <button type="submit" class="btn-aksi">Simpan</button>
	    
	    <label>Nama ID <span class="small">Nama gambar ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label>Nama EN <span class="small">Nama gambar EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	    
		<input type="hidden" name="parent_video" id="parent_video" value="<?php echo set_value('parent_video', $parent_video); ?>" />
		<div style="clear:left"></div>

		
	<?php endif; ?>
	    
	    <div style="clear:left"></div>
	    <label>URL Video <span class="small">Url video</span> </label>
	    <input type="text" name="src_video" id="src_video" maxlength="200" value="<?php echo set_value('src_video'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('src_video'); ?>
	
	    <label>Deskripsi<span class="small">Deskripsi Gambar</span></label>
	    <div style="clear:left"></div>
	    <?php echo form_error('ket_id'); ?>
	    <?php echo form_error('ket_en'); ?>
	    <div id="tabs-isi">
		<ul>
			<li><a href="#isi-id">Deskripsi Indonesia</a></li>
			<li><a href="#isi-en">Deskripsi English</a></li>
		</ul>
		<div id="isi-id">
			<textarea name="ket_id" id="ket_id"><?php echo set_value('ket_id',''); ?></textarea>
			<?php echo display_ckeditor($ket_id);?>
	    </div>
		<div id="isi-en">
			<textarea name="ket_en" id="ket_en"><?php echo set_value('ket_en',''); ?></textarea>
			<?php echo display_ckeditor($ket_en);?>
		</div>
	
	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Produk Anak</title>
	<script>
	var baseURL = '<?php echo base_url(); ?>';
	</script>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	
	<script type="text/javascript">
	    var kategoriSelect = '<?php echo base_url($this->config->item('admpath').'/produk/list_select_kategori'); ?>';
	</script>
	
	<style>
	.ui-autocomplete{max-height:170px;overflow-y:auto;overflow-x:hidden}
	* html .ui-autocomplete{height:170px}
	.ui-tooltip{font-size:10px;background:#362C2C;color:#E6E6FA}
	#radio{float:right;width:142px;font-size:11px}
	#box_spesial{float:left;width:260px;margin-left:20px}
	#txt_spesial{float:left;width:120px;margin-top:3px;font-weight:bold;text-align:left;color:#7B97B3;background:#FFFCFC;margin-left:-10px;padding:4px;border:solid 1px #aacfe4}
	#tambah-kategori{padding:6px 4px;width:20px;float:left;margin-top:3px;margin-left:6px}
	#box-kode-warnaAnak,#box-kode-warnaInduk{float:left;width:35px;margin-top:1px;margin-left:2px}
	input[type="text"]#kode-warna,input[type="text"]#kode-warnaInduk{width:50px;padding:3px;float:left}
	.box-opsi{float:left;font-size:10px;width:150px;padding:4px;margin:2px 0 0 10px;color:#7B97B3;border:solid 1px #aacfe4}
	.ui-dialog .ui-state-error{background:#B51C37;color:#F8F3F3}
	.validateTips,.tipeFile{border:1px solid transparent;font-size:12px;padding:3px}
	.Icon{float:left;margin-top:2px}
	.info-nama{float:left;width:100%;margin-top:-15px}
	input[type="text"]#nama_prod,input[type="text"]#nama_anak{width:310px}
	input[type="text"]#kode_prod,input[type="text"]#kode_prod_anak{width:110px;margin-left:-77px}
	.scrollup{width:38px;height:38px;cursor:pointer;position:fixed;bottom:2px;right:2px;display:none;border:1px solid #E5E5E5;border-radius:2px;background:#FFA500;color:#FFFFFF;font-weight:bold}
	.scrollup:hover{background:#FFBB3F;color:#F4F4F4}
	</style>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	
	<script type="text/javascript">
	function expPost()
	{
	    location.reload();
	}
	
	function cekUploadExt(el) {
	  var ar_ext = ['png', 'gif', 'jpe', 'jpg'];
	  var name = el.value;
	  var ar_name = name.split('.');
	  var ar_nm = ar_name[0].split('\\');
	  
	  for(var i=0; i<ar_nm.length; i++) var nm = ar_nm[i];
	
	  var re = 0;
	  for(var i=0; i<ar_ext.length; i++) {
	    if(ar_ext[i] == ar_name[1]) {
	      re = 1;
	      break;
	    }
	  }
	
	  if(re==1) {
	    return 'ok';
	  }
	  else {
	    el.value = '';
	    return ar_name[1];
	  }
	}
	
	
	function hargaSpesial()
	{
	    var diskon = $('#diskon').val(),harga = $('#harga').val(),
	        pot = harga * diskon,total = parseInt(harga) - parseInt(pot);
	    
	    if(diskon == '' || harga == '' || harga == 0 || total == 0)
	    {
	        $('#txt_spesial').text(harga);
	        $('#harga_spesial').val('0');
	        if( diskon == '' && harga > 0 )
	        {
	            $('#txt_spesial').text(total).formatNumber('.', 3, 0);
	        }
	    }
	    else {
	        $('#txt_spesial').text(total).formatNumber('.', 3, 0);
	        $('#harga_spesial').val(total);
	    }
	}
	
	function cekAttributAnak()
	{
	    if($("#cek-harga-ikut-induk:checked").length > 0)
	    {
	        $('.box-harga').hide();
	        $('#diskon,#harga,#harga_spesial').val('0');
	        $('#box_spesial').hide();
	    }else{
	        $('.box-harga').show();
	    }
	
	    if($("#cek-ikut-induk:checked").length > 0)
	    {
	        $('#box-deskripsi').hide();
	    }else{
	        $('#box-deskripsi').show();
	    }
	}
	
	function cekJenis()
	{
	    var ket = $('#anak_untuk').val(); 
	    
	    $('#txt-jenis, #txt-jenis-en').empty().text('Ketik nama ' + ket + 'nya.?');
	    $('#nm-jenis, #nm-jenis-en').empty().text(ket);
	}
	
	function cekTipeFile()
	{
	    $("input:file").change(function (e){ 
	      var ket = cekUploadExt(this);
	      
	      if(ket !== 'ok')
	      $( "#dialog-message" ).dialog( "open" );
	      e.preventDefault;
	    }); 
	}
	
	$(function() {
	    
	    var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    $('#addNew').live('click', function(e) { 
	        e.preventDefault();
	        if ( i < 11 )
	        {
	            $('<p><input type="file" name="userfile[]"  style="border:none" /><a href="#" title="Buang Foto '+i+'" class="remNew"></a> </p>').appendTo(addDiv);
	            i++;
	        }
	        cekTipeFile();
	    });
	
	    $('.remNew').live('click', function(e) {
	        e.preventDefault();
	        if( i > 2 ) {
	            $(this).parents('p').remove();
	            i--;
	        }
	    });
	    
	    // tooltip
	    $( ".blok" ).tooltip({
	            track: true
	    });
	    
	    $( "#dialog-message" ).dialog({
	        autoOpen: false,
	        modal: true,
	        height: 150,
	        width: 320,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        buttons: {
	            Ok: function() {
	                $( this ).dialog( "close" );
	            }
	        }
	    });
	
		$( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 250,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori").empty().load(kategoriSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	
	    $( "#tambah-kategori" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
	        var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	
	    $( "#tabs-isi" ).tabs();
	    
	});
	
	$(document).ready(function(){
	
	    hargaSpesial();
	    cekAttributAnak();
	    cekJenis();
	    
	    $('#diskon,#harga').change(function(e){    
	        hargaSpesial();
	    });
	
	    $("#cek-ikut-induk,#cek-harga-ikut-induk").click(function(){
	        cekAttributAnak();
	    });
	    
	    $('#anak_untuk').change(function(){    
	        cekJenis();
	    });
	
	    $('#kode-warnaAnak,#kode-warnaInduk').jPicker();
	    $('span.Image').attr('title','Warna untuk produk ini.<br>boleh dikosongkan.');
	    
	    cekTipeFile();
	
	    //scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 150) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 1a(){23.24()}4 1b(a){5 b=[\'25\',\'26\',\'27\',\'28\'];5 c=a.1c;5 d=c.1d(\'.\');5 e=d[0].1d(\'\\\\\');1e(5 i=0;i<e.r;i++)5 f=e[i];5 g=0;1e(5 i=0;i<b.r;i++){6(b[i]==d[1]){g=1;29}}6(g==1){l\'1f\'}j{a.1c=\'\';l d[1]}}4 R(){5 a=$(\'#S\').h(),7=$(\'#7\').h(),1g=7*a,y=1h(7)-1h(1g);6(a==\'\'||7==\'\'||7==0||y==0){$(\'#T\').s(7);$(\'#U\').h(\'0\');6(a==\'\'&&7>0){$(\'#T\').s(y).1i(\'.\',3,0)}}j{$(\'#T\').s(y).1i(\'.\',3,0);$(\'#U\').h(y)}}4 V(){6($("#D-7-E-F:1j").r>0){$(\'.G-7\').W();$(\'#S,#7,#U\').h(\'0\');$(\'#2a\').W()}j{$(\'.G-7\').1k()}6($("#D-E-F:1j").r>0){$(\'#G-1l\').W()}j{$(\'#G-1l\').1k()}}4 X(){5 a=$(\'#1m\').h();$(\'#1n-H, #1n-H-1o\').Y().s(\'2b 2c \'+a+\'2d.?\');$(\'#1p-H, #1p-H-1o\').Y().s(a)}4 10(){$("1q:1r").12(4(e){5 a=1b(u);6(a!==\'1f\')$("#8-1s").8("1t");e.13})}$(4(){5 c=$(\'#1u\');5 i=$(\'#1u p\').2e()+1;5 d=$("#2f"),I=$("#2g"),J=$([]).1v(d).1v(I),14=$(".2h");4 v(t){14.s(t).15("k-m-1w");2i(4(){14.K("k-m-1w",2j)},2k)}4 16(o,n,a,b){6(o.h().r>b||o.h().r<a){o.15("k-m-w");v("2l "+n+" 2m 2n 2o "+a+" 1x 2p 2q "+b+" 1x.");l q}j{l x}}4 17(o,a,n){6(!(a.2r(o.h()))){o.15("k-m-w");v(n);l q}j{l x}}$(\'#2s\').1y(\'B\',4(e){e.13();6(i<11){$(\'<p><1q 1z="1r" 2t="2u[]"  2v="2w:2x" /><a 2y="#" 1A="2z 2A \'+i+\'" 2B="1B"></a> </p>\').2C(c);i++}10()});$(\'.1B\').1y(\'B\',4(e){e.13();6(i>2){$(u).2D(\'p\').2E();i--}});$(".2F").2G({2H:x});$("#8-1s").8({1C:q,1D:x,1E:1F,1G:2I,1H:q,1I:{1J:"L M",1K:"L M",1L:18},1M:{2J:4(){$(u).8("N")}}});$("#8-O").8({1C:q,1E:2K,1G:2L,1H:q,1I:{1J:"L M",1K:"L M",1L:18},1D:x,1M:{"2M":4(){5 b=x;J.K("k-m-w");b=b&&16(d,"1N 1O 2N",3,1P);b=b&&17(d,/^([0-9 a-z A-Z])+$/,"1Q 1R 1S 1T 1U : a-z 0-9");b=b&&16(I,"1N 1O 2O",3,1P);b=b&&17(I,/^([0-9 a-z A-Z])+$/,"1Q 1R 1S 1T 1U : a-z 0-9");6(b){$.2P({1z:"2Q",P:$(u).Q(\'P\'),Q:$("#O-1V-C").2R(),2S:4(a){6(a.1W===\'2T\'){$("#8-O").8("N");$("#C").Y().2U(2V)}j{v(a.1W)}},2W:\'2X\',w:1a})}},"2Y":4(){$(u).8("N");J.h("").K("k-m-w");v("1X C 1Y.")}},N:4(){J.h("").K("k-m-w");v("1X C 1Y.")}});$("#1V-C").2Z({30:{31:"k-32-33-34"}}).B(4(){5 a=$(u).Q(\'P\');$("#8-O").Q(\'P\',a).8("1t")});$("#1Z-35").1Z()});$(20).36(4(){R();V();X();$(\'#S,#7\').12(4(e){R()});$("#D-E-F,#D-7-E-F").B(4(){V()});$(\'#1m\').12(4(){X()});$(\'#21-37,#21-38\').39();$(\'3a.3b\').3c(\'1A\',\'3d 3e 3f 3g.<3h>3i 3j.\');10();$(18).3k(4(){6($(20).22()>1F){$(\'.19\').3l()}j{$(\'.19\').3m()}});$(\'.19\').B(4(){$("3n, 3o").3p({22:0},3q);l q})});',62,213,'||||function|var|if|harga|dialog|||||||||val||else|ui|return|state||||false|length|text||this|updateTips|error|true|total|||click|kategori|cek|ikut|induk|box|jenis|namaen|allFields|removeClass|center|top|close|form|url|data|hargaSpesial|diskon|txt_spesial|harga_spesial|cekAttributAnak|hide|cekJenis|empty||cekTipeFile||change|preventDefault|tips|addClass|checkLength|checkRegexp|window|scrollup|expPost|cekUploadExt|value|split|for|ok|pot|parseInt|formatNumber|checked|show|deskripsi|anak_untuk|txt|en|nm|input|file|message|open|addinput|add|highlight|karakter|live|type|title|remNew|autoOpen|modal|height|150|width|resizable|position|my|at|of|buttons|Nama|Kategori|50|Karakter|yang|di|perbolehkan|hanya|tambah|status|Masukkan|baru|tabs|document|kode|scrollTop|location|reload|png|gif|jpe|jpg|break|box_spesial|Ketik|nama|nya|size|nama_kat_id|nama_kat_en|validateTips|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|addNew|name|userfile|style|border|none|href|Buang|Foto|class|appendTo|parents|remove|blok|tooltip|track|320|Ok|250|350|Tambah|Indonesia|English|ajax|POST|serialize|success|sukses|load|kategoriSelect|dataType|json|Batal|button|icons|primary|icon|circle|plus|isi|ready|warnaAnak|warnaInduk|jPicker|span|Image|attr|Warna|untuk|produk|ini|br|boleh|dikosongkan|scroll|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>
	
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box_produk'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/produk/detail_anak/'.$this->uri->segment(4,set_value('parent_id_prod')).'/show'),'Kembali ke List', $attr); ?>
	</div>
	
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myform" style="width:93%">
	    <?php
	        $attributes = array( 'id' => 'form');
	        echo form_open_multipart($this->config->item('admpath').'/produk/add_produk_anak', $attributes);
	    ?>
	    <h1>Tambah Produk baru</h1>
	    <p>Masukkan data Produk yang baru.</p>
	
		<input type="hidden" name="radio" id="radio" value="anak" />
	    <input type="hidden" name="parent_id_prod" id="parent_id_prod" value="<?php echo set_value('parent_id_prod',$this->uri->segment(4))?>" />
	
		<button type="submit" class="btn-aksi">Simpan</button>
	
	    <label>Kategori<span class="small">Pilih Kategori produk</span> </label>
		<select name="kategori" id="kategori" style="margin-right:8px;">
		<option value="">-- Tidak ada --</option>
		<?php
		foreach($kat_produk as $items)
		{
			echo '<option value="'.$items->id_kategori.'" '.set_select('kategori', $items->id_kategori).'>'.$items->label_id.' // '.$items->label_en.'</option>';
		}
		?>
		</select>
		<a href="#" id="tambah-kategori" data-url="<?php echo site_url($this->config->item('admpath').'/produk/tambah_kategori');?>" class="blok" title="Tambah Kategori"></a>
	
		<label style="margin-left:10px">Anak untuk<span class="small">Menurut jenis apa dibedakan.?</span> </label>
		<select id="anak_untuk" name="anak_untuk">
			<?php
			$jenis = $this->config->item('list_jenis_prod');
			foreach( $jenis as $key => $val ):
			?>
			<option value="<?php echo $key; ?>" <?php echo set_select('anak_untuk', $key); ?> >&nbsp;<?php echo $val; ?>&nbsp;</option>
			<?php endforeach; ?>
		</select>
	
		<label style="margin-left:20px">SKU <span class="small">Kode produk</span> </label>
		<input type="text" name="kode_prod_anak" id="kode_prod_anak" maxlength="50" value="<?php echo set_value('kode_prod_anak'); ?>" />
		
		<div id="box-kode-warnaAnak" class="blok">
			<input id="kode-warnaAnak" name="kode-warnaAnak" type="hidden" value="<?php echo set_value('kode-warnaAnak'); ?>" />
		</div>
		
		<div style="clear:left"></div>
		<label>Nama <span id="nm-jenis">model</span><span id="txt-jenis" class="small">Apakah nama modelnya.?</span> </label>
		<input type="text" name="nama_anak" id="nama_anak" maxlength="100" value="<?php echo set_value('nama_anak'); ?>" />
		
		<label style="margin-left:10px">NamaEN <span id="nm-jenis-en">model</span><span id="txt-jenis-en" class="small">Apakah nama modelnya.?</span> </label>
		<input type="text" name="nama_anak_en" id="nama_anak_en" style="width:310px;margin-left:-10px" maxlength="100" value="<?php echo set_value('nama_anak_en'); ?>" />
		
		<div class="info-nama">
			<?php echo form_error('nama_anak'); ?>
			<?php echo form_error('nama_anak_en'); ?>
			<?php echo form_error('kode_prod_anak'); ?>
		</div>
	
	    <div style="clear:left"></div>
	    <label>Foto Produk<span class="small">@min-ukuran : 800x600<br>@max-ukuran : 1366x1024<br>@max-file size : 1MB</span> </label>
	    <div id="addinput" class="blok ui-helper-clearfix" style="padding-top:1.5%">
	        <p>
	            <input type="file" name="userfile[]" style="border:none" /><a href="#" id="addNew" title="Tambah Foto"></a>
	        </p>
	    </div>
	    <br style="clear:left;"/>
	    <?php echo form_error('userfile'); ?>
	    <div style="clear:left; height:10px;"></div>
	    
	    <label>Status <span class="small">Off = Offline, On = Online</span> </label>
	    <select id="status" name="status" style="margin-right:20px;">
	        <option value="on" <?php echo set_select('status', 'on'); ?> >&nbsp;On&nbsp;</option>
	        <option value="off" <?php echo set_select('status', 'off'); ?> >&nbsp;Off&nbsp;</option>
	    </select>
	
	    <label>Produk Promo<span class="small">Apakah ini produk promo.?</span> </label>
	    <select id="promo" name="promo" style="margin-right:20px; margin-left:-10px;">
	        <option value="off" <?php echo set_select('promo', 'off'); ?> >&nbsp;Bukan&nbsp;</option>
	        <option value="on" <?php echo set_select('promo', 'on'); ?> >&nbsp;Ya&nbsp;</option>
	    </select>

	    <label style="margin-right:-20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
	    
	    <div class="box-harga">
	        <label>Diskon Produk<span class="small">Diskon untuk produk ini.?</span> </label>
	        <select id="diskon" name="diskon" style="margin-left:-10px;">
	            <option value="" <?php echo set_select('diskon', ''); ?> >&nbsp;Tidak ada&nbsp;</option>
	            <?php
	            $diskon = $this->config->item('list_diskon_prod');
	            foreach( $diskon as $key => $val ):
	            ?>
	            <option value="<?php echo $key; ?>" <?php echo set_select('diskon', $key); ?> >&nbsp;<?php echo $val; ?>&nbsp;</option>
	            <?php endforeach; ?>
	        </select>
	    </div>
	    
	    <div class="box-anak box-opsi">
	        <input type="checkbox" id="cek-harga-ikut-induk" name="cek-harga-ikut-induk" value="y" style="float:left;" <?php echo set_checkbox('cek-harga-ikut-induk', 'y'); ?> /><span>Harga samakan dgn induk</span>
	    </div>
	    
	    <div style="clear:left"></div>
	    
	    <label>Stok <span class="small">Jumlah stok produk</span> </label>
	    <input type="text" name="stok" id="stok" maxlength="6" style="width:120px; margin-right:20px;" value="<?php echo set_value('stok','0'); ?>" />
	    
	    <div class="box-harga">
	        <label>Harga <span class="small">Harga produk</span> </label>
	        <input type="text" name="harga" id="harga" maxlength="11" style="width:120px; margin-left:-60px;" value="<?php echo set_value('harga','0'); ?>" />
	        
	        <div id="box_spesial">
	            <label style="width:120px">Harga Jual<span class="small">Harga Jual Produk</span> </label>
	            <div id="txt_spesial">0</div>
	            <input type="hidden" name="harga_spesial" id="harga_spesial" value="<?php echo set_value('harga_spesial','0'); ?>" />
	        </div>
	    </div>
	    
	    <br />
	    <?php echo form_error('stok'); ?>
	    <div class="box-harga">
	        <?php echo form_error('harga'); ?>
	    </div>
	    
	    <div style="clear:left"></div>
	    <label>Deskripsi <span class="small">Deskripsi untuk produk ini.</span></label>
	    <div class="box-anak box-opsi">
	        <input type="checkbox" id="cek-ikut-induk" name="cek-ikut-induk" value="y" style="float:left;" <?php echo set_checkbox('cek-ikut-induk', 'y'); ?> /><span>Samakan seperti induk</span>
	    </div>
	    <div style="clear:left; height:5px;"></div>
	    <div id="box-deskripsi">
	        <?php echo form_error('deskripsi'); ?>
	        <?php echo form_error('deskripsi_en'); ?>
	        <div id="tabs-isi">
				<ul>
					<li><a href="#isi-id">Deskripsi Indonesia</a></li>
					<li><a href="#isi-en">Deskripsi English</a></li>
				</ul>
				<div id="isi-id">
		        <textarea name="deskripsi" id="deskripsi" ><?php echo set_value('deskripsi',''); ?></textarea>
				<?php echo display_ckeditor($deskripsi);?>
				</div>
				<div id="isi-en">
					<textarea name="deskripsi_en" id="deskripsi_en" ><?php echo set_value('deskripsi_en',''); ?></textarea>
					<?php echo display_ckeditor($deskripsi_en);?>
				</div>
			</div> <!-- End #tabs-isi -->
	    </div>
	    <div style="clear:both; height:10px;"></div>
	    
	  </form>
	</div>

	</body>
</html>

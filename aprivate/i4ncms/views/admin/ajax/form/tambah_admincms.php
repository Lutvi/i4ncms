<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Admin CMS</title>

	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	
	</script>
	<?php endif; ?>
	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/admincms/add_admin', $attributes);
	    ?>
	
	    <button type="submit" class="btn-aksi">Simpan</button>
	    <h1>Tambah admin baru</h1>
	    <p>Masukkan data Admin yang baru.</p>

	    <input type="hidden" name="id_pembuat" id="id_pembuat" value="<?php echo set_value('id_pembuat',$this->session->userdata('idmu')); ?>" />

		<?php if (bs_kode($this->session->userdata('level'), TRUE) == 'superman'): ?>
	    <label>Level<span class="small">Level Admin</span> </label>
	    <select name="level" id="level" style="margin-right:20px">
			<option value="<?php echo bs_kode('superman'); ?>" <?php echo set_select('level', bs_kode('superman')); ?>>Super Admin</option>
			<option value="<?php echo bs_kode('batman'); ?>" <?php echo set_select('level', bs_kode('batman')); ?>>Admin</option>
			<option value="<?php echo bs_kode('ironman'); ?>" <?php echo set_select('level', bs_kode('ironman')); ?>>Entry Data</option>
			<option value="<?php echo bs_kode('spiderman'); ?>" <?php echo set_select('level', bs_kode('spiderman')); ?>>Marketing</option>
	    </select>
		<?php else: ?>
		<label>Level<span class="small">Level Entry</span> </label>
	    <select name="level" id="level" style="margin-right:20px">
			<option value="<?php echo bs_kode('ironman'); ?>" <?php echo set_select('level', bs_kode('ironman')); ?>>Entry Data</option>
			<option value="<?php echo bs_kode('spiderman'); ?>" <?php echo set_select('level', bs_kode('spiderman')); ?>>Marketing</option>
	    </select>
		<?php endif; ?>
		<div style="clear:left"></div>
		<?php echo form_error('level'); ?>

	    <label>Foto Profil<span class="small">ideal:256x256(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" />
	    <div style="clear:left"></div>
	    <?php echo form_error('userfile'); ?>

	    <label>Nama Lengkap <span class="small">Nama lengkap admin</span> </label>
	    <input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_lengkap'); ?>" />

	    <label>Email <span class="small">Email Admin</span> </label>
	    <input type="text" name="adm_email" id="adm_email" maxlength="150" style="width:300px;margin-left:-40px" value="<?php echo set_value('adm_email'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_lengkap'); ?>
	    <?php echo form_error('adm_email'); ?>

	    <label>ID <span class="small">ID Login</span> </label>
	    <input type="text" name="adm_id" id="adm_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('adm_id'); ?>" />

	    <label>Kunci<span class="small">Kunci Login</span> </label>
	    <input type="text" name="adm_kunci" id="adm_kunci" maxlength="150" style="width:300px;margin-left:-40px" value="<?php echo set_value('adm_kunci'); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('adm_id'); ?>
	    <?php echo form_error('adm_kunci'); ?>

		<label>Biografi<span class="small">Biografy singkat admin</span> </label>
		<textarea name="biografi" id="biografi" maxlength="300"><?php echo set_value('biografi'); ?></textarea>
		<div style="clear:left"></div>
		<?php echo form_error('biografi'); ?>

	    <div style="clear:both; height:10px;"></div>
	  </form>
	</div>

	</body>
</html>

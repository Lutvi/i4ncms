<!DOCTYPE html>
<html>
	<head>
	<title>Update Banner Iklan</title>

	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	$(function(){

		$('#status_ganti').val('no');

		$('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});
		
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 100) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});

		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});

		$( "#tgl_terbit" ).datepicker({
			showAnim: 'slideDown',
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			defaultDate: $("#tgl_terbit").val()
		});

		$( "#tgl_berakhir" ).datepicker({
			showAnim: 'slideDown',
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			defaultDate: $("#tgl_berakhir").val()
		});
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(2(){$(\'#3\').1(\'8\');$(\'#u\').5(2(){9 a=$(\'#b\').1();$(\'#c\').d();$(\'#e\').f();6(a!=""){$(\'#3\').1(\'g\')}});$(\'#b\').v(2(){9 a=$(w).1();6(a!=""){$(\'#3\').1(\'g\')}});$(\'#x\').5(2(){$(\'#c\').f();$(\'#e\').d();$(\'#3\').1(\'8\')});$(y).z(2(){6($(A).h()>B){$(\'.7\').C()}D{$(\'.7\').E()}});$(\'.7\').5(2(){$("F, G").H({h:0},I);J K});$("#i").j({k:\'l\',m:4,n:4,o:\'p-q-r\',s:$("#i").1()});$("#t").j({k:\'l\',m:4,n:4,o:\'p-q-r\',s:$("#t").1()})});',47,47,'|val|function|status_ganti|true|click|if|scrollup|no|var||userfile|c_gmbr|hide|up_gmbr|show|yes|scrollTop|tgl_terbit|datepicker|showAnim|slideDown|changeMonth|changeYear|dateFormat|yy|mm|dd|defaultDate|tgl_berakhir|ganti_gmbr|change|this|batal_ganti_gmbr|window|scroll|document|100|fadeIn|else|fadeOut|html|body|animate|600|return|false'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/banner/update_formiklan', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Update</button>
	
	    <h1>Update Banner Iklan</h1>
	    <p>Masukkan data Banner Iklan yang baru.</p>

	    <input type="hidden" name="id_iklan" id="id_iklan" value="<?php echo set_value('id_iklan',$iklan->id); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="gambar" id="gambar" value="<?php echo set_value('gambar',$iklan->img_src); ?>" />

	    <label>Nama Pengiklan<span class="small">Nama pemasang iklan</span> </label>
	    <input type="text" name="nama_pengiklan" id="nama_pengiklan" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_pengiklan', $iklan->nama_pengiklan); ?>" />

	    <label style="margin-right:-40px">Tarif <span class="small">Tarif pemasangan iklan</span> </label>
	    <input type="text" name="kredit" id="kredit" maxlength="11" style="width:300px;" value="<?php echo set_value('kredit', $iklan->kredit); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_pengiklan'); ?>
	    <?php echo form_error('kredit'); ?>

	    <label>URL Iklan <span class="small">Link target URL iklan</span> </label>
	    <input type="text" name="url_target" id="url_target" maxlength="200" style="width:65%;" value="<?php echo set_value('url_target', $iklan->url_target); ?>" />
	    <div style="clear:left"></div>
	    <?php echo form_error('url_target'); ?>

		<div id="c_gmbr" style="height:80px;">
	    <button type="button" id="ganti_gmbr" class="ganti">Ganti Gambar</button>
	    <label>Gambar Iklan<span class="small">Gambar yang terpasang</span> </label>
	    <img src="<?php echo base_url(); ?>_media/banner-iklan/thumb/thumb_<?php echo $iklan->img_src; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
	    <div style="clear:left"></div>
	    </div>

	    <div id="up_gmbr" style="display:none;">
	    <button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Gambar</button>
	    <label>Gambar Iklan<span class="small">ideal:960x640, max:1366x768<br>max-size : 1MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" id="userfile" />
	    <br />
		<?php echo form_error('userfile'); ?>
	    <div style="clear:left"></div>
	    </div>

		<div style="clear:left"></div>
	    <label>Posisi Iklan<span class="small">Posisi banner iklan di website.</span> </label>
	    <select name="posisi" id="posisi">
			<option value="">--- Pilih Posisi ---</option>
			<option value="atas" <?php echo set_select('posisi','atas', ($iklan->posisi == 'atas')?TRUE:FALSE); ?>>Atas</option>
			<option value="sidebar" <?php echo set_select('posisi','sidebar', ($iklan->posisi == 'sidebar')?TRUE:FALSE); ?>>Sidebar</option>
			<option value="konten_atas" <?php echo set_select('posisi','konten_atas', ($iklan->posisi == 'konten_atas')?TRUE:FALSE); ?>>Konten Atas</option>
			<option value="konten_bawah" <?php echo set_select('posisi','konten_bawah', ($iklan->posisi == 'konten_bawah')?TRUE:FALSE); ?>>Konten Bawah</option>
	    </select>
	    <div style="clear:left"></div>
	    <?php echo form_error('posisi'); ?>
	    
	    <label>Nama ID <span class="small">Nama iklan ID</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id', $iklan->nama_pengiklan); ?>" />
	
	    <label style="margin-right:-40px">Nama EN <span class="small">Nama iklan EN</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="50" style="width:300px;" value="<?php echo set_value('nama_en', $iklan->nama_pengiklan); ?>" />
	
	    <div style="clear:left"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>

	    <label>Tgl Mulai <span class="small">Tanggal mulai iklan</span> </label>
	    <input type="text" name="tgl_terbit" id="tgl_terbit" maxlength="50" style="width:120px;margin-right:20px" value="<?php echo set_value('tgl_terbit', format_tgl_jq($iklan->tgl_terbit)); ?>" />
	
	    <label style="margin-right:-40px">Tgl Berakhir <span class="small">Tanggal berakhir iklan</span> </label>
	    <input type="text" name="tgl_berakhir" id="tgl_berakhir" maxlength="50" style="width:120px;" value="<?php echo set_value('tgl_berakhir', format_tgl_jq($iklan->tgl_berakhir)); ?>" />

	    <label style="margin-left:25px;margin-right:-80px">Status<span class="small">Status iklan.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on', ($iklan->status == 'on')?TRUE:FALSE); ?>>On</option>
			<option value="off" <?php echo set_select('status','off', ($iklan->status == 'off')?TRUE:FALSE); ?>>Off</option>
	    </select>

	    <div style="clear:left"></div>
	    <?php echo form_error('tgl_terbit'); ?>
	    <?php echo form_error('tgl_berakhir'); ?>
	    <?php echo form_error('status'); ?>
		
	    <div style="clear:both; height:10px;"></div>
	  </form>

	</div>

	</body>
</html>

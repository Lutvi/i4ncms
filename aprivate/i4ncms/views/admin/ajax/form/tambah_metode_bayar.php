<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Metode Pembayaran</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-autocomplete {
	    max-height: 170px;
	    overflow-y: auto;
	    /* prevent horizontal scrollbar */
	    overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
	    height: 170px;
	}
	</style>

	<script>
	var idWilayahURL = '<?php echo base_url($this->config->item('admpath').'/atur_metode_bayar/list_idwilayah'); ?>';
	var maxWilayah = <?php echo $this->config->item('max_wilayah'); ?>;
	</script>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>

	function cekTipe(tipe)
	{
		if(tipe == "online"){
			$("#extra").show();
		}else{
			$("#extra").hide();
		}
	}
	
	$(document).ready(function(){
		var tipe = $('#tipe').val();
		//cek tipenya
		cekTipe(tipe);
		$("#tipe").change(function () {
			var tipe = $('#tipe').val();
	        cekTipe(tipe);
	        $('.error').hide();
	    });
	    
		//-------------------------------
		// #id_wilayah
		//-------------------------------
		var wilayah = $('#wilayah').magicSuggest({
			maxSelection: maxWilayah,
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: idWilayahURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'wilayah'
		});

		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 50) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});

		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('1 2(a){7(a=="k"){$("#8").l()}9{$("#8").d()}}$(e).m(1(){3 b=$(\'#4\').5();2(b);$("#4").n(1(){3 a=$(\'#4\').5();2(a);$(\'.o\').d()});3 c=$(\'#f\').p({q:r,s:{g:$("t[h=g]").5()},u:v,w:i,x:y,h:\'f\'});$(z).A(1(){7($(e).j()>B){$(\'.6\').C()}9{$(\'.6\').D()}});$(\'.6\').E(1(){$("F, G").H({j:0},I);J i})});',46,46,'|function|cekTipe|var|tipe|val|scrollup|if|extra|else||||hide|document|wilayah|Hydra|name|false|scrollTop|online|show|ready|change|error|magicSuggest|maxSelection|maxWilayah|dataUrlParams|input|data|idWilayahURL|allowFreeEntries|maxDropHeight|200|window|scroll|50|fadeIn|fadeOut|click|html|body|animate|600|return'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/atur_metode_bayar'),'Kembali ke List', $attr); ?>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/atur_metode_bayar/add_metode', $attributes);
	    ?>

	    <button type="submit" class="btn-aksi">Simpan</button>

		 <h1>Tambah Metode Pembayaran baru</h1>
	    <p>Masukkan data Metode Pembayaran baru.</p>

	    <label>Nama Vendor<span class="small">Nama vendor atau instansi</span> </label>
	    <input type="text" name="nama_vendor" id="nama_vendor" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_vendor'); ?>" />

	    <label>Tipe Metode<span class="small">Pilih tipe metode pembayaran.</span> </label>
	    <select id="tipe" name="tipe" style="margin-right:20px;">
			<option value="transfer" <?php echo set_select('tipe', 'transfer'); ?>>&nbsp;&nbsp;Transfer&nbsp;&nbsp;</option>
	        <option value="online" <?php echo set_select('tipe', 'online'); ?>>&nbsp;&nbsp;Online&nbsp;&nbsp;</option>
	        <option value="giro" <?php echo set_select('tipe', 'giro'); ?>>&nbsp;&nbsp;Giro&nbsp;&nbsp;</option>
	        <option value="cod" <?php echo set_select('tipe', 'cod'); ?>>&nbsp;&nbsp;COD&nbsp;&nbsp;</option>
	    </select>

		<label style="margin-left:25px;margin-right:-80px">Status<span class="small">Status Metode Bayar.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on'); ?>>On</option>
			<option value="off" <?php echo set_select('status','off'); ?>>Off</option>
	    </select>
		<div style="clear:left"></div>
		<?php echo form_error('nama_vendor'); ?>
		<?php echo form_error('tipe'); ?>
		<?php echo form_error('status'); ?>

	    <label>Logo Vendor<span class="small">min:120x120, max:640x640<br>max-size : 0.8MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>

	    <br />
			<label>Wilayah Layanan<span class="small">Pilih Wilayah yang dapat menggunakan layanan<br>atau kosongkan saja jika semua wilayah bisa menggunakan layanan.</span> </label>
			<input type="text" name="wilayah" id="wilayah" value="<?php echo set_value('wilayah'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
		<br /><br />

		<div style="clear:left"></div>
		<label>Detail Metode<span class="small">Masukkan detail Identitas Metode pembayarannya</span> </label>
		<textarea name="detail" id="detail" maxlength="100" style="height:120px;width:600px"><?php echo set_value('detail'); ?></textarea>
		<div style="clear:both"></div>
		<?php echo form_error('detail'); ?>

		<div style="clear:both"></div>
		<div id="extra" style="display:none">
			<label>Id Merchant<span class="small">Id Merchant Payment Gateway</span> </label>
			<input type="text" name="id_merchant" id="id_merchant" maxlength="100" style="width:600px;" value="<?php echo set_value('id_merchant'); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('id_merchant'); ?>

			<label>Hash / Key<span class="small">Hash / Key Payment Gateway</span> </label>
			<input type="text" name="hash_key" id="hash_key" maxlength="200" style="width:600px;" value="<?php echo set_value('hash_key'); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('hash_key'); ?>

			<label>URL Redirect<span class="small">URL Redirect Payment Gateway</span> </label>
			<input type="text" name="redirect_url" id="redirect_url" maxlength="150" style="width:600px;" value="<?php echo set_value('redirect_url'); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('redirect_url'); ?>

			<label>URL Sukses<span class="small">URL Sukses Payment Gateway</span> </label>
			<input type="text" name="sukses_url" id="sukses_url" maxlength="150" style="width:600px;" value="<?php echo set_value('sukses_url'); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('sukses_url'); ?>
		</div>

		<div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	</body>
</html>

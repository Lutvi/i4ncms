<!DOCTYPE html>
<html>
	<head>
	<title>Tambah Halaman</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<style>
	.ui-autocomplete {
	    max-height: 170px;
	    overflow-y: auto;
	    /* prevent horizontal scrollbar */
	    overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
	    height: 170px;
	}
	.ui-dialog .ui-state-error { background:#B51C37; color:#F8F3F3; }
	.validateTips,.tipeFile { border: 1px solid transparent; font-size:12px; padding:3px; }
	.tambah-kategori { padding:6px 4px; width:20px; float:left; margin-top:3px; margin-left:6px; }
	.tambah-keyword { padding:6px 4px; width:16px; float:left; margin-top:3px; margin-left:6px; }
	</style>
	
	<script>
	var sts = '<?php echo $sts; ?>';
	var keywordIdURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_id'; ?>';
	var keywordEnURL = '<?php echo base_url().$this->config->item('admpath').'/halaman/list_keyword_en'; ?>';
	
	var maxKategori = <?php echo $this->config->item('max_kategori'); ?>;
	var produkURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/produk'); ?>';
	var blogURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/blog'); ?>';
	var albumURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/album'); ?>';
	var videoURL = '<?php echo base_url($this->config->item('admpath').'/halaman/list_kategori/video'); ?>';
	</script>
	<?php if( ENVIRONMENT == 'development') : ?>
	<script>

	function setTipeHalaman(tipe)
	{
		var	tipe_album = $('#tipe_album').val(),
			tipe_video = $('#tipe_video').val(),
			tipe_prod = $('#tipe_prod').val();
	      
	      if(tipe == 'module'){
	        $('#listModule, #seoHalaman').show();
	        $('.listProduk, .listBlog, .listAlbum, .listVideo, #htmlHalaman').hide();
	      }
	      else if(tipe == 'blog'){
	        $('.listBlog, #seoHalaman').show();
	        $('.listAlbum, .listProduk, .listVideo, #listModule, #htmlHalaman').hide();
	      }
	      else if(tipe == 'album'){
	        $('.listAlbum, #seoHalaman, #htmlHalaman').show();
	        $('.listBlog,.listProduk, .listVideo, #listModule').hide();
	
	        if( tipe_album == 'by_id' )
	        {
	            $('#box-induk-album').show();
	            $('#box-kategori-album').hide();
	        }
	        else{
	            $('#box-induk-album').hide();
	            $('#box-kategori-album').show();
	        }
	      }
	      else if(tipe == 'video'){
	        $('.listVideo, #seoHalaman, #htmlHalaman').show();
	        $('.listBlog,.listAlbum, .listProduk, #listModule').hide();
	        
	        if( tipe_video == 'by_id' )
	        {
	            $('#box-induk-video').show();
	            $('#box-kategori-video').hide();
	        }
	        else{
	            $('#box-induk-video').hide();
	            $('#box-kategori-video').show();
	        }
	      }
	      else if(tipe == 'produk'){
	        $('.listProduk, #seoHalaman').show();
	        $('.listBlog,.listAlbum, .listVideo, #listModule, #htmlHalaman').hide();
	
	        if( tipe_prod == 'by_id' )
	        {
	            $('#box-induk').show();
	            $('#box-kategori').hide();
	        }
	        else{
	            $('#box-induk').hide();
	            $('#box-kategori').show();
	        }
	      }
	      else if(tipe == 'halaman'){
			$('#seoHalaman, #htmlHalaman').show();
	        $('#listModule, .listBlog, .listAlbum, .listProduk, .listVideo').hide();
	      }
	      else{
	        $('#listModule, .listBlog, .listAlbum, .listProduk, .listVideo, #seoHalaman, #htmlHalaman').hide();
	      }
	}
	
	$(function(){
	
		var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	    
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function expPost()
		{
		    location.reload();
		}
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	    
	    function split( val ) {
				return val.split( /,\s*/ );
	    }
	    function extractLast( term ) {
	        return split( term ).pop();
	    }
	
		// add keyword
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 270,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Keyword": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Keyword Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Keyword English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-keyword").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan keyword baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan keyword baru." );
	        }
	    });
	
	    $( ".tambah-keyword" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
			var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	    // end add keyword
	    
	    //keyword id halaman
	    $( "#keyword_id" )
	    .bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	                $( this ).data( "autocomplete" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        source: function( request, response ) {
	            $.getJSON( keywordIdURL, {
	                term: extractLast( request.term )
	            }, response );
	        },
	        search: function() {
	            var term = extractLast( this.value );
	            if ( term.length < 2 ) {
	                return false;
	            }
	        },
	        focus: function() {
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            terms.pop();
	            terms.push( ui.item.value );
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	
	    //keyword en halaman
	    $( "#keyword_en" )
	    .bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	                $( this ).data( "autocomplete" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        source: function( request, response ) {
	            $.getJSON( keywordEnURL, {
	                term: extractLast( request.term )
	            }, response );
	        },
	        search: function() {
	            var term = extractLast( this.value );
	            if ( term.length < 2 ) {
	                return false;
	            }
	        },
	        focus: function() {
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            terms.pop();
	            terms.push( ui.item.value );
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	
	    //-------------------------------
		// #kategori_produk
		//-------------------------------
		$('#kategori').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: produkURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori'
		});
	
	    //-------------------------------
		// #kategori_blog
		//-------------------------------
		$('#kategori_blog').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: blogURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori_blog'
		});
	
	    //-------------------------------
		// #kategori_album
		//-------------------------------
		$('#kategori_album').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: albumURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori_album'
		});
	
		//-------------------------------
		// #kategori_video
		//-------------------------------
		$('#kategori_video').magicSuggest({
			maxSelection: maxKategori,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: videoURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'kategori_video'
		});
	
	    $( "#tabs-seo, #tabs-isi" ).tabs();
	});
	
	
	$(document).ready(function(){

		var tipe = $('#tipe').val();
		// set tipe
	    setTipeHalaman(tipe);
	    
	    if(sts == 'error_module'){
	      $('#listModule, #seoHalaman').show();
	    }
	    else if(sts == 'error_blog'){
	      $('.listBlog, #seoHalaman').show();
	    }
	    else if(sts == 'error_album'){
	      $('.listAlbum, #seoHalaman, #htmlHalaman').show();
	    }
	    else if(sts == 'error_video'){
	      $('.listVideo, #seoHalaman, #htmlHalaman').show();
	    }
	    else if(sts == 'error_produk'){
	      $('.listProduk, #seoHalaman').show();
	    }
	    else if(sts == 'error_halaman'){
	      $('#htmlHalaman, #seoHalaman').show();
	    }
	    else{
		  $('#listModule, .listBlog, .listAlbum, .listProduk, .listVideo, #seoHalaman, #htmlHalaman').hide();
	    }
	
	    $("#tipe,#tipe_prod,#tipe_album,#tipe_video").change(function () {
			var tipe = $('#tipe').val();
	        setTipeHalaman(tipe);
	        $('.error').hide();
	    });
	
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 150) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 1d(a){k b=$(\'#1p\').g(),Q=$(\'#Q\').g(),R=$(\'#R\').g();5(a==\'21\'){$(\'#v, #h\').7();$(\'.w, .x, .y, .B, #m\').8()}6 5(a==\'22\'){$(\'.x, #h\').7();$(\'.y, .w, .B, #v, #m\').8()}6 5(a==\'M\'){$(\'.y, #h, #m\').7();$(\'.x,.w, .B, #v\').8();5(b==\'1e\'){$(\'#l-J-M\').7();$(\'#l-C-M\').8()}6{$(\'#l-J-M\').8();$(\'#l-C-M\').7()}}6 5(a==\'N\'){$(\'.B, #h, #m\').7();$(\'.x,.y, .w, #v\').8();5(Q==\'1e\'){$(\'#l-J-N\').7();$(\'#l-C-N\').8()}6{$(\'#l-J-N\').8();$(\'#l-C-N\').7()}}6 5(a==\'23\'){$(\'.w, #h\').7();$(\'.x,.y, .B, #v, #m\').8();5(R==\'1e\'){$(\'#l-J\').7();$(\'#l-C\').8()}6{$(\'#l-J\').8();$(\'#l-C\').7()}}6 5(a==\'24\'){$(\'#h, #m\').7();$(\'#v, .x, .y, .w, .B\').8()}6{$(\'#v, .x, .y, .w, .B, #h, #m\').8()}}$(4(){k d=$(\'#1q\');k i=$(\'#1q p\').25()+1;k e=$("#26"),S=$("#27"),T=$([]).1r(e).1r(S),1f=$(".28");4 1s(){29.2a()}4 K(t){1f.2b(t).1g("r-G-1t");2c(4(){1f.U("r-G-1t",2d)},2e)}4 1h(o,n,a,b){5(o.g().V>b||o.g().V<a){o.1g("r-G-H");K("2f "+n+" 2g 2h 2i "+a+" 1u 2j 2k "+b+" 1u.");j f}6{j W}}4 1i(o,a,n){5(!(a.2l(o.g()))){o.1g("r-G-H");K(n);j f}6{j W}}4 O(a){j a.O(/,\\s*/)}4 P(a){j O(a).1j()}$("#I-X").I({2m:f,2n:2o,2p:2q,2r:f,2s:{2t:"1v 1w",2u:"1v 1w",2v:1x},2w:W,2x:{"2y 1k":4(){k b=W;T.U("r-G-H");b=b&&1h(e,"1k 2z",3,1y);b=b&&1i(e,/^([0-9 a-z A-Z])+$/,"1z 1A 1B 1C 1D : a-z 0-9");b=b&&1h(S,"1k 2A",3,1y);b=b&&1i(S,/^([0-9 a-z A-Z])+$/,"1z 1A 1B 1C 1D : a-z 0-9");5(b){$.2B({2C:"2D",Y:$(q).u(\'Y\'),u:$("#X-1E-10").2E(),2F:4(a){5(a.1F===\'2G\'){$("#I-X").I("1l")}6{K(a.1F)}},2H:\'2I\',H:1s})}},"2J":4(){$(q).I("1l");T.g("").U("r-G-H");K("1G 10 1H.")}},1l:4(){T.g("").U("r-G-H");K("1G 10 1H.")}});$(".1E-10").2K({2L:{2M:"r-2N-2O-2P"}}).1I(4(){k a=$(q).u(\'Y\');$("#I-X").u(\'Y\',a).I("2Q")});$("#2R").1J("1K",4(a){5(a.11===$.r.11.1L&&$(q).u("12").1M.1N){a.1O()}}).12({1P:4(a,b){$.1Q(2S,{13:P(a.13)},b)},1R:4(){k a=P(q.D);5(a.V<2){j f}},1S:4(){j f},1T:4(a,b){k c=O(q.D);c.1j();c.14(b.1U.D);c.14("");q.D=c.1V(", ");j f}});$("#2T").1J("1K",4(a){5(a.11===$.r.11.1L&&$(q).u("12").1M.1N){a.1O()}}).12({1P:4(a,b){$.1Q(2U,{13:P(a.13)},b)},1R:4(){k a=P(q.D);5(a.V<2){j f}},1S:4(){j f},1T:4(a,b){k c=O(q.D);c.1j();c.14(b.1U.D);c.14("");q.D=c.1V(", ");j f}});$(\'#C\').15({16:17,18:{E:$("19[F=E]").g()},u:2V,1a:f,1b:1c,F:\'C\'});$(\'#1W\').15({16:17,18:{E:$("19[F=E]").g()},u:2W,1a:f,1b:1c,F:\'1W\'});$(\'#1X\').15({16:17,18:{E:$("19[F=E]").g()},u:2X,1a:f,1b:1c,F:\'1X\'});$(\'#1Y\').15({16:17,18:{E:$("19[F=E]").g()},u:2Y,1a:f,1b:1c,F:\'1Y\'});$("#1m-2Z, #1m-30").1m()});$(1Z).31(4(){k b=$(\'#1n\').g();1d(b);5(L==\'32\'){$(\'#v, #h\').7()}6 5(L==\'33\'){$(\'.x, #h\').7()}6 5(L==\'34\'){$(\'.y, #h, #m\').7()}6 5(L==\'35\'){$(\'.B, #h, #m\').7()}6 5(L==\'36\'){$(\'.w, #h\').7()}6 5(L==\'37\'){$(\'#m, #h\').7()}6{$(\'#v, .x, .y, .w, .B, #h, #m\').8()}$("#1n,#R,#1p,#Q").38(4(){k a=$(\'#1n\').g();1d(a);$(\'.H\').8()});$(1x).39(4(){5($(1Z).20()>3a){$(\'.1o\').3b()}6{$(\'.1o\').3c()}});$(\'.1o\').1I(4(){$("3d, 3e").3f({20:0},3g);j f})});',62,203,'||||function|if|else|show|hide|||||||false|val|seoHalaman||return|var|box|htmlHalaman||||this|ui|||data|listModule|listProduk|listBlog|listAlbum|||listVideo|kategori|value|Hydra|name|state|error|dialog|induk|updateTips|sts|album|video|split|extractLast|tipe_video|tipe_prod|namaen|allFields|removeClass|length|true|form|url||keyword|keyCode|autocomplete|term|push|magicSuggest|maxSelection|maxKategori|dataUrlParams|input|allowFreeEntries|maxDropHeight|200|setTipeHalaman|by_id|tips|addClass|checkLength|checkRegexp|pop|Keyword|close|tabs|tipe|scrollup|tipe_album|addinput|add|expPost|highlight|karakter|center|top|window|50|Karakter|yang|di|perbolehkan|hanya|tambah|status|Masukkan|baru|click|bind|keydown|TAB|menu|active|preventDefault|source|getJSON|search|focus|select|item|join|kategori_blog|kategori_album|kategori_video|document|scrollTop|module|blog|produk|halaman|size|nama_kat_id|nama_kat_en|validateTips|location|reload|text|setTimeout|1500|500|Bagian|harus|memuat|minimal|dan|maksimal|test|autoOpen|height|270|width|350|resizable|position|my|at|of|modal|buttons|Tambah|Indonesia|English|ajax|type|POST|serialize|success|sukses|dataType|json|Batal|button|icons|primary|icon|circle|plus|open|keyword_id|keywordIdURL|keyword_en|keywordEnURL|produkURL|blogURL|albumURL|videoURL|seo|isi|ready|error_module|error_blog|error_album|error_video|error_produk|error_halaman|change|scroll|150|fadeIn|fadeOut|html|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	<div id="dialog-form" title="Tambah Keyword" style="display:none">
	    <p class="validateTips">Masukkan keyword baru.</p>
	    <br />
	    <?php echo form_open('',array('id'=>'form-tambah-keyword')); ?>
	    <fieldset style="padding:8px; font-size:12px;" class="text ui-widget-content ui-corner-all">
	        <label for="nama">Keyword ID</label>
	        <input type="text" name="nama_kat_id" id="nama_kat_id" maxlength="50" style="padding:5px;" class="text ui-widget-content ui-corner-all"/>
	        <br /><br />
	        <label for="nama">Keyword EN</label>
	        <input type="text" name="nama_kat_en" id="nama_kat_en" maxlength="50" style="padding:5px;" class="text ui-widget-content ui-corner-all"/>
	        <br />
	    </fieldset>
	    </form>
	</div>
	
	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open($this->config->item('admpath').'/halaman/add_halaman', $attributes);
	    ?>
	<button type="submit" class="btn-aksi">Simpan</button>
	
	    <h1>Tambah halaman baru</h1>
	    <p>Masukkan data Halaman yang baru.</p>
	    
	    <label>Label ID <span class="small">Nama halaman Indonesia</span> </label>
	    <input type="text" name="nama_id" id="nama_id" maxlength="150" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_id'); ?>" />
	
	    <label style="margin-right:-30px;">Label EN <span class="small">Nama halaman English</span> </label>
	    <input type="text" name="nama_en" id="nama_en" maxlength="150" style="width:300px;" value="<?php echo set_value('nama_en'); ?>" />
	    <div style="clear:both"></div>
	    <?php echo form_error('nama_id'); ?>
	    <?php echo form_error('nama_en'); ?>
	
	    <label>Tipe Halaman<span class="small">Pilih tipe halaman.</span> </label>
	    <select id="tipe" name="tipe" style="margin-right:20px;">
			<option value="top" <?php echo set_select('tipe', 'top'); ?>>&nbsp;&nbsp;Top Menu&nbsp;&nbsp;</option>
	        <option value="halaman" <?php echo set_select('tipe', 'halaman'); ?>>&nbsp;&nbsp;Halaman&nbsp;&nbsp;</option>
	        <option value="blog" <?php echo set_select('tipe', 'blog'); ?>>&nbsp;&nbsp;Blog&nbsp;&nbsp;</option>
	        <option value="album" <?php echo set_select('tipe', 'album'); ?>>&nbsp;&nbsp;Album&nbsp;&nbsp;</option>
	        <option value="video" <?php echo set_select('tipe', 'video'); ?>>&nbsp;&nbsp;Video&nbsp;&nbsp;</option>
	        <option value="produk" <?php echo set_select('tipe', 'produk'); ?>>&nbsp;&nbsp;Produk&nbsp;&nbsp;</option>
	        <option value="module" <?php echo set_select('tipe', 'module'); ?>>&nbsp;&nbsp;Module&nbsp;&nbsp;</option>
	    </select>
	
	    <label style="margin-right:-35px;">Parent Menu<span class="small">Pilih parent menu.</span> </label>
	    <select id="menu_parent" name="menu_parent" style="margin-left:-5px;margin-right:10px">
	    <option value="0">-- Tidak ada --</option>
	    <?php foreach ($nama_menu as $rowm): ?>
	    <option value="<?php echo $rowm->id; ?>" <?php echo set_select('menu_parent', $rowm->id); ?>>
	    <?php
		$tab = "";
		if($rowm->level > 0)
		{
			for ($x=1; $x<=$rowm->level; $x++) {
				$tab .= "=";
			}
			$tab .=  "> ";
		}
	    echo $tab.$rowm->title;
	    ?>
	    </option>
	    <?php endforeach; ?>
	    </select>
	
	    <div class="listAlbum" style="display:none;">
	        <label style="margin-right:-25px;">Tipe Album<span class="small">Pilih Album berdasarkan.</span> </label>
	        <select id="tipe_album" name="tipe_album">
	            <option value="by_id" <?php echo set_select('tipe_album', 'by_id'); ?>>&nbsp;&nbsp;Album Id&nbsp;&nbsp;</option>
	            <option value="by_kategori" <?php echo set_select('tipe_album', 'by_kategori'); ?>>&nbsp;&nbsp;Kategori Album&nbsp;&nbsp;</option>
	        </select>
	    </div>
	
	    <div class="listVideo" style="display:none;">
	        <label style="margin-right:-25px;">Tipe Video<span class="small">Pilih Video berdasarkan.</span> </label>
	        <select id="tipe_video" name="tipe_video">
	            <option value="by_id" <?php echo set_select('tipe_video', 'by_id'); ?>>&nbsp;&nbsp;Video Id&nbsp;&nbsp;</option>
	            <option value="by_kategori" <?php echo set_select('tipe_video', 'by_kategori'); ?>>&nbsp;&nbsp;Kategori Video&nbsp;&nbsp;</option>
	        </select>
	    </div>
	    
	    <div class="listProduk" style="display:none;">
	        <label style="margin-right:-25px;">Tipe Produk<span class="small">Pilih tipe berdasarkan.</span> </label>
	        <select id="tipe_prod" name="tipe_prod">
	            <option value="by_id" <?php echo set_select('tipe_prod', 'by_id'); ?>>&nbsp;&nbsp;Produk Induk&nbsp;&nbsp;</option>
	            <option value="by_kategori" <?php echo set_select('tipe_prod', 'by_kategori'); ?>>&nbsp;&nbsp;Kategori Produk&nbsp;&nbsp;</option>
	        </select>
	    </div>
	    
	    <div style="clear:both"></div>
	    <?php echo form_error('tipe'); ?>
	    <?php echo form_error('menu_parent'); ?>
	    <?php echo form_error('status'); ?>
	    <?php echo form_error('tipe_album'); ?>
	    <?php echo form_error('tipe_video'); ?>
	    <?php echo form_error('tipe_prod'); ?>
	    
	    <div id="listModule" style="display:none;">
	    <label>Module <span class="small">Pilih module.</span> </label>
	    <select id="module_id" name="module_id">
	    <?php if(!empty($aktif_mod)): ?>
	        <?php foreach ($aktif_mod as $row): ?>
	        <option value="<?php echo $row['id_module']; ?>" <?php echo set_select('module_id', $row['id_module']); ?>>&nbsp;&nbsp;<?php echo humanize($row['nama_module']); ?>&nbsp;&nbsp;</option>
	        <?php endforeach; ?>>
	    <?php endif; ?>
	    </select>
	    <?php echo form_error('module_id'); ?>
	    <div style="clear:left"></div>
	    </div>
	
	    <div class="listBlog" style="display:none;">
			<br />
			<label>Kategori Blog<span class="small">Pilih kategori blog</span> </label>
			<input type="text" name="kategori_blog" id="kategori_blog" value="<?php echo set_value('kategori_blog'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
			 <br />
	        <div style="clear:both"></div>
	        <?php echo form_error('kategori_blog'); ?>
	    </div>
	
	    <div class="listAlbum" style="display:none;">
	        <div id="box-induk-album">
	            <label>Album<span class="small">Pilih Album.</span> </label>
	            <select id="id_album" name="id_album">
	            <?php if(!empty($aktif_album)): ?>
	                <?php foreach ($aktif_album as $row): ?>
	                <option value="<?php echo $row['id']; ?>" <?php echo set_select('id_album', $row['id']); ?>>&nbsp;&nbsp;<?php echo $row['nama_id']; ?>&nbsp;|<?php echo $row['nama_en']; ?>&nbsp;&nbsp;</option>
	                <?php endforeach; ?>>
	            <?php endif; ?>
	            </select>
	        </div>
	        <div id="box-kategori-album" style="display:none;">
				<br />
	            <label>Kategori Album<span class="small">Pilih kategori album</span> </label>
	            <input type="text" name="kategori_album" id="kategori_album" value="<?php echo set_value('kategori_album'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
	            <br />
	        </div>
	        <div style="clear:both"></div>
	        <?php echo form_error('id_album'); ?>
	        <?php echo form_error('kategori_album'); ?>
	    </div>
	
	    <div class="listVideo" style="display:none;">
	        <div id="box-induk-video">
	            <label>Video<span class="small">Pilih Video.</span> </label>
	            <select id="id_video" name="id_video">
	            <?php if(!empty($aktif_video)): ?>
	                <?php foreach ($aktif_video as $row): ?>
	                <option value="<?php echo $row['id']; ?>" <?php echo set_select('id_video', $row['id']); ?>>&nbsp;&nbsp;<?php echo $row['nama_id']; ?>&nbsp;|<?php echo $row['nama_en']; ?>&nbsp;&nbsp;</option>
	                <?php endforeach; ?>
	            <?php endif; ?>
	            </select>
	        </div>
	        <div id="box-kategori-video" style="display:none;">
				<br />
	            <label>Kategori Video<span class="small">Pilih kategori video</span> </label>
	            <input type="text" name="kategori_video" id="kategori_video" value="<?php echo set_value('kategori_video'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
	            <br />
	        </div>
	        <div style="clear:both"></div>
	        <?php echo form_error('id_video'); ?>
	        <?php echo form_error('kategori_video'); ?>
	    </div>
	    
	    <div class="listProduk" style="display:none;">
	        <div id="box-induk">
	            <label>Produk<span class="small">Pilih produk induk.</span> </label>
	            <select id="id_produk" name="id_produk">
	            <?php if(!empty($aktif_prod)): ?>
	                <?php foreach ($aktif_prod as $row): ?>
	                <option value="<?php echo $row['id_prod']; ?>" <?php echo set_select('id_produk', $row['id_prod']); ?>>&nbsp;&nbsp;<?php echo $row['nama_prod']; ?>&nbsp;&nbsp;</option>
	                <?php endforeach; ?>
	            <?php endif; ?>
	            </select>
	        </div>
	        <div id="box-kategori" style="display:none;">
				<br />
	            <label>Kategori Produk<span class="small">Pilih kategori produk</span> </label>
	            <input type="text" name="kategori" id="kategori" value="<?php echo set_value('kategori'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
	            <br />
	        </div>
	        <div style="clear:both"></div>
	        <?php echo form_error('id_produk'); ?>
	        <?php echo form_error('kategori'); ?>
	    </div>
	    
	    <div style="clear:left"></div>
	    
	    <div id="seoHalaman" style="display:none;">
	    <label>SEO <span class="small">SEO Halaman</span> </label>
	    <div style="float:left;width:60%;">
			<div id="tabs-seo" style="height:200px">
	            <ul>
	                <li><a href="#seo-id">SEO Indonesia</a></li>
	                <li><a href="#seo-en">SEO English</a></li>
	            </ul>
	            <div id="seo-id">
	                <div class="">
						<label>Keyword ID <span class="small">Keyword Indonesia</span> </label>
	                    <input type="text" name="keyword_id" id="keyword_id" maxlength="200" value="<?php echo set_value('keyword_id'); ?>" />
	                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
	                    <label>Description ID <span class="small">Description Indonesia</span> </label>
	                    <textarea name="description_id" id="description_id" maxlength="250" style="height:80px;"><?php echo set_value('description_id'); ?></textarea>
	                </div>
	            </div>
	            <div id="seo-en">
	                 <div class="">
						<label>Keyword EN <span class="small">Keyword English</span> </label>
	                    <input type="text" name="keyword_en" id="keyword_en" maxlength="200" value="<?php echo set_value('keyword_en'); ?>" />
	                    <a href="#" data-url="<?php echo site_url($this->config->item('admpath').'/halaman/tambah_keyword');?>" class="tambah-keyword" title="Tambah Keyword"></a>
	                    <label>Description EN<span class="small">Description English</span> </label>
	                    <textarea name="description_en" id="description_en" maxlength="250" style="height:80px;"><?php echo set_value('description_en'); ?></textarea>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div style="clear:both"></div>
	    <?php echo form_error('keyword_id'); ?>
	    <?php echo form_error('description_id'); ?>
	    <?php echo form_error('keyword_en'); ?>
	    <?php echo form_error('description_en'); ?>
	    </div>
	    <div style="clear:left"></div>
	    
	    <div id="htmlHalaman" style="display:none;">
				<label>Isi <span class="small">Isi Halaman</span></label>
				<div style="clear:left"></div>
				<?php echo form_error('isi_id'); ?>
				<?php echo form_error('isi_en'); ?>
			<div id="tabs-isi">
	            <ul>
	                <li><a href="#isi-id">Isi Indonesia</a></li>
	                <li><a href="#isi-en">Isi English</a></li>
	            </ul>
	            <div id="isi-id">
					<textarea name="isi_id" id="isi_id"><?php echo htmlspecialchars_decode(set_value('isi_id','')); ?></textarea>
					<?php echo display_ckeditor($isi_id);?>
				</div>
				<div id="isi-en">
					<textarea name="isi_en" id="isi_en"><?php echo htmlspecialchars_decode(set_value('isi_en','')); ?></textarea>
					<?php echo display_ckeditor($isi_en);?>
				</div>
			</div>
	    </div>
	  </form>
	  
	  <div style="clear:both; height:10px;"></div>
	</div>

	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
	<title>Update Metode Pembayaran <?php echo set_value('nama_vendor_ori', $metode->nama_vendor); ?></title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<script>
	var idWilayah = <?php echo (!empty($metode->id_wilayah))?"[".$metode->id_wilayah."]":"''"; ?>;
	var maxWilayah = <?php echo $this->config->item('max_wilayah'); ?>;
	var idWilayahURL = '<?php echo base_url($this->config->item('admpath').'/atur_metode_bayar/list_idwilayah'); ?>';
	</script>

	<?php if( ENVIRONMENT == 'development') : ?>
	<script>
	
	$(document).ready(function(){
		//-------------------------------
		// #id_wilayah
		//-------------------------------
		var wilayah = $('#wilayah').magicSuggest({
			maxSelection: maxWilayah,
			//valueField : 'name',
			dataUrlParams: {Hydra:$("input[name=Hydra]").val()},
			data: idWilayahURL,
			allowFreeEntries: false,
			maxDropHeight: 200,
			name: 'wilayah'
		});

		if(idWilayah !== '')
		{
			$(wilayah).on('load', function(){
				if(this._dataSet === undefined){
					this._dataSet = true;
					wilayah.setValue(idWilayah);
				}
			});
	    }

	    //Logo
	    $('#ganti_gmbr').click(function(){
			var file = $('#userfile').val();
			
			$('#c_gmbr').hide();
			$('#up_gmbr').show();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#userfile').change(function(){
			var file = $(this).val();
			if(file != ""){
				$('#status_ganti').val('yes');
			}
		});
		
		$('#batal_ganti_gmbr').click(function(){
			$('#c_gmbr').show();
			$('#up_gmbr').hide();
			$('#status_ganti').val('no');
		});

		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 50) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(9).p(1(){4 b=$(\'#c\').q({r:s,t:{d:$("u[e=d]").2()},v:w,x:f,y:z,e:\'c\'});3(g!==\'\'){$(b).A(\'B\',1(){3(5.h===C){5.h=D;b.E(g)}})}$(\'#F\').6(1(){4 a=$(\'#i\').2();$(\'#j\').k();$(\'#l\').m();3(a!=""){$(\'#7\').2(\'n\')}});$(\'#i\').G(1(){4 a=$(5).2();3(a!=""){$(\'#7\').2(\'n\')}});$(\'#H\').6(1(){$(\'#j\').m();$(\'#l\').k();$(\'#7\').2(\'I\')});$(J).K(1(){3($(9).o()>L){$(\'.8\').M()}N{$(\'.8\').O()}});$(\'.8\').6(1(){$("P, Q").R({o:0},S);T f})});',56,56,'|function|val|if|var|this|click|status_ganti|scrollup|document|||wilayah|Hydra|name|false|idWilayah|_dataSet|userfile|c_gmbr|hide|up_gmbr|show|yes|scrollTop|ready|magicSuggest|maxSelection|maxWilayah|dataUrlParams|input|data|idWilayahURL|allowFreeEntries|maxDropHeight|200|on|load|undefined|true|setValue|ganti_gmbr|change|batal_ganti_gmbr|no|window|scroll|50|fadeIn|else|fadeOut|html|body|animate|600|return'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

	<div class="kelist">
        <?php 
        $attr = array('class' => 'txtkelist');
        echo anchor(site_url($this->config->item('admpath').'/atur_metode_bayar'),'Kembali ke List', $attr); ?>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	    $attributes = array( 'id' => 'form');
	    echo form_open_multipart($this->config->item('admpath').'/atur_metode_bayar/update_form_metode', $attributes);
	    ?>

		<input type="hidden" name="id_metode" id="id_metode" maxlength="11" value="<?php echo set_value('id_metode', $metode->id); ?>" />
	    <input type="hidden" name="nama_vendor_ori" id="nama_vendor_ori" maxlength="50" value="<?php echo set_value('nama_vendor_ori', $metode->nama_vendor); ?>" />
	    <input type="hidden" name="status_ganti" id="status_ganti" maxlength="10" value="no" />
	    <input type="hidden" name="logo" id="logo" value="<?php echo set_value('logo',$metode->logo); ?>" />

	    <button type="submit" class="btn-aksi">Update</button>

		 <h1>Update Metode Pembayaran <?php echo set_value('nama_vendor_ori', $metode->nama_vendor); ?></h1>
	    <p>Masukkan data Metode Pembayaran yang baru.</p>

	    <label>Nama Vendor<span class="small">Nama vendor atau instansi</span> </label>
	    <input type="text" name="nama_vendor" id="nama_vendor" maxlength="50" style="width:300px;margin-right:20px" value="<?php echo set_value('nama_vendor', $metode->nama_vendor); ?>" />

	    <label>Tipe Metode<span class="small">Pilih tipe metode pembayaran.</span> </label>
	    <select id="tipe" name="tipe" style="margin-right:20px;">
			<option value="transfer" <?php echo set_select('tipe', 'transfer',($metode->tipe_metode == 'transfer')?TRUE:FALSE); ?>>&nbsp;&nbsp;Transfer&nbsp;&nbsp;</option>
	        <option value="online" <?php echo set_select('tipe', 'online',($metode->tipe_metode == 'online')?TRUE:FALSE); ?>>&nbsp;&nbsp;Online&nbsp;&nbsp;</option>
	        <option value="giro" <?php echo set_select('tipe', 'giro',($metode->tipe_metode == 'giro')?TRUE:FALSE); ?>>&nbsp;&nbsp;Giro&nbsp;&nbsp;</option>
	        <option value="cod" <?php echo set_select('tipe', 'cod',($metode->tipe_metode == 'cod')?TRUE:FALSE); ?>>&nbsp;&nbsp;COD&nbsp;&nbsp;</option>
	    </select>

		<label style="margin-left:25px;margin-right:-80px">Status<span class="small">Status iklan.</span> </label>
	    <select name="status" id="status">
			<option value="on" <?php echo set_select('status','on',($metode->status == 'on')?TRUE:FALSE); ?>>On</option>
			<option value="off" <?php echo set_select('status','off',($metode->status == 'off')?TRUE:FALSE); ?>>Off</option>
	    </select>
		<div style="clear:left"></div>
		<?php echo form_error('nama_vendor'); ?>
		<?php echo form_error('tipe'); ?>
		<?php echo form_error('status'); ?>


		<div id="c_gmbr" style="height:60px;">
			<button type="button" id="ganti_gmbr" class="ganti">Ganti Logo</button>
			<label>Logo Vendor<span class="small">Logo Vendor yang terpasang</span> </label>
			<img src="<?php echo base_url(); ?>_media/logo-bayar/thumb_<?php echo $metode->logo; ?>" style="float:left; margin-left:10px; padding:4px; background:#E6E6FA; border:solid 1px #aacfe4;" align="absmiddle" />
			<div style="clear:left"></div>
	    </div>

		<div id="up_gmbr" style="display:none;">
		<button type="button" id="batal_ganti_gmbr" class="batalGanti">Batal Ganti Logo</button>
	    <label>Ganti Logo Vendor<span class="small">min:120x120, max:640x640<br>max-size : 0.8MB(jpg,png,gif)</span> </label>
	    <input type="file" name="userfile" id="userfile" />
	    <div style="clear:left"></div>
		<?php echo form_error('userfile'); ?>
		</div>

		<div style="clear:left"></div>
	    <br />
			<label>Wilayah Layanan<span class="small">Pilih Wilayah yang dapat menggunakan layanan<br>atau kosongkan saja jika semua wilayah bisa menggunakan layanan.</span> </label>
			<input type="text" name="wilayah" id="wilayah" value="<?php echo set_value('wilayah'); ?>" style="width:600px;margin-left:150px;min-height:50px" />
		<br /><br />

		<div style="clear:left"></div>
		<label>Detail Metode<span class="small">Masukkan detail Identitas Metode pembayarannya</span> </label>
		<textarea name="detail" id="detail" maxlength="250" spellcheck="false" style="height:120px;width:600px"><?php echo set_value('detail', jadi_nl($metode->detail)); ?></textarea>
		<div style="clear:both"></div>
		<?php echo form_error('detail'); ?>

		<div style="clear:both"></div>
		<div id="extra" style="display:none">
			<label>Id Merchant<span class="small">Id Merchant Payment Gateway</span> </label>
			<input type="text" name="id_merchant" id="id_merchant" maxlength="100" style="width:600px;" value="<?php echo set_value('id_merchant', $metode->id_merchant); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('id_merchant'); ?>

			<label>Hash / Key<span class="small">Hash / Key Payment Gateway</span> </label>
			<input type="text" name="hash_key" id="hash_key" maxlength="200" style="width:600px;" value="<?php echo set_value('hash_key', $metode->hash_key); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('hash_key'); ?>

			<label>URL Redirect<span class="small">URL Redirect Payment Gateway</span> </label>
			<input type="text" name="redirect_url" id="redirect_url" maxlength="150" style="width:600px;" value="<?php echo set_value('redirect_url', $metode->redirect_url); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('redirect_url'); ?>

			<label>URL Sukses<span class="small">URL Sukses Payment Gateway</span> </label>
			<input type="text" name="sukses_url" id="sukses_url" maxlength="150" style="width:600px;" value="<?php echo set_value('sukses_url', $metode->sukses_url); ?>" />
			<div style="clear:both"></div>
			<?php echo form_error('sukses_url'); ?>
		</div>

		<div style="clear:both; height:10px;"></div>
	  </form>
	</div>
	</body>
</html>

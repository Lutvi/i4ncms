<!DOCTYPE html>
<html>
	<head>
	<title>Update Produk <?php echo $produk->nama_prod; ?></title>
	<script>
	var baseURL = '<?php echo base_url(); ?>';
	</script>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>

	<link href="<?php echo base_url(); ?>assets/css/admin-produk.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
	    var kategoriSelect = '<?php echo base_url($this->config->item('admpath').'/produk/list_select_kategori'); ?>',
	        upFotoProduk = '<?php echo site_url($this->config->item('admpath').'/produk/tambah_foto'); ?>',
	        hpsFotoURL = '<?php echo site_url($this->config->item('admpath').'/produk/hapus_foto'); ?>';
	</script>
	
	<?php if( ENVIRONMENT == 'development') : ?>
	
	<script type="text/javascript">
	
	function expPost()
	{
	    location.reload();
	}
	
	function cekUploadExt(el) {
	  var ar_ext = ['png', 'gif', 'jpe', 'jpg'];
	  var name = el.value;
	  var ar_name = name.split('.');
	  var ar_nm = ar_name[0].split('\\');
	  
	  for(var i=0; i<ar_nm.length; i++) var nm = ar_nm[i];
	
	  var re = 0;
	  for(var i=0; i<ar_ext.length; i++) {
	    if(ar_ext[i] == ar_name[1]) {
	      re = 1;
	      break;
	    }
	  }
	
	  if(re==1) {
	    return 'ok';
	  }
	  else {
	    el.value = '';
	    return ar_name[1];
	  }
	}
	
	function hargaSpesial()
	{
	    var diskon = $('#diskon').val(),harga = $('#harga').val(),
	        pot = harga * diskon,total = parseInt(harga) - parseInt(pot);
	    
	    if(diskon == '' || harga == '' || harga == 0 || total == 0)
	    {
	        $('#txt_spesial').text(harga);
	        $('#harga_spesial').val('0');
	        if( diskon == '' && harga > 0 )
	        {
	            $('#txt_spesial').text(total).formatNumber('.', 3, 0);
	        }
	    }
	    else {
	        $('#txt_spesial').text(total).formatNumber('.', 3, 0);
	        $('#harga_spesial').val(total);
	    }
	    
	}
	
	function cekTipeFile()
	{
	    $("input:file").change(function (e){ 
	      var ket = cekUploadExt(this);
	      
	      if(ket !== 'ok')
	      {
	        var info = 'File yang Anda pilih bukan file foto/gambar.!';
	        $( "#dialog-message" ).dialog("option", "title", "Tipe File Upload");
	        $( "#dialog-message" ).data('INFO',info).dialog( "open");
	      }
	      e.preventDefault;
	    }); 
	}
	
	$(function() {
	    
	    var addDiv = $('#addinput');
	    var i = $('#addinput p').size() + 1;
	
	    var namaid = $( "#nama_kat_id" ),
			namaen = $( "#nama_kat_en" ),
	        allFields = $( [] ).add( namaid ).add( namaen ),
	        tips = $( ".validateTips" );
	
	    function updateTips( t ) {
	        tips
	            .text( t )
	            .addClass( "ui-state-highlight" );
	        setTimeout(function() {
	            tips.removeClass( "ui-state-highlight", 1500 );
	        }, 500 );
	    }
	
	    function checkLength( o, n, min, max ) {
	        if ( o.val().length > max || o.val().length < min ) {
	            o.addClass( "ui-state-error" );
	            updateTips( "Bagian " + n + " harus memuat minimal " +
	                min + " karakter dan maksimal " + max + " karakter." );
	            return false;
	        } else {
	            return true;
	        }
	    }
	
	    function checkRegexp( o, regexp, n ) {
	        if ( !( regexp.test( o.val() ) ) ) {
	            o.addClass( "ui-state-error" );
	            updateTips( n );
	            return false;
	        } else {
	            return true;
	        }
	    }
	    
	    function progressHandlingFunction(e){
	        if(e.lengthComputable){
	            $('progress').attr({value:e.loaded,max:e.total});
	        }
	    }
	    
	    $('#addNew').live('click', function(e) { 
	        e.preventDefault();
	        if(i < 11)
	        {
	            $('<p class="tambahan ui-widget-content"><input type="file" name="userfile[]" /><a href="#" title="Buang Foto '+i+'" class="remNew"></a> </p>').appendTo(addDiv);
	            i++;
	        }
	        cekTipeFile();
	    });
	
	    $('.remNew').live('click', function(e) {
	        e.preventDefault();
	        if( i > 2 ) {
	            $(this).parents('p').remove();
	            i--;
	        }
	    });
	
	    // tooltip
	    $( ".blok" ).tooltip({
	            track: true
	    });
	    
	    $( "#dialog-message" ).dialog({
	        autoOpen: false,
	        modal: true,
	        minHeight: 150,
	        width: 320,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        open: function(event, ui, data) {
	                    var INFO = $(this).data('INFO');
	                    $(this).find('#infoFile').html(INFO);
	            },
	        buttons: {
	            Ok: function() {
	                $( this ).dialog( "close" );
	            }
	        }
	    });
	    
	    $( "#dialog-gagal" ).dialog({
	        autoOpen: false,
	        modal: true,
	        minHeight: 100,
	        width: 320,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        open: function(event, ui, data) {
	                    var INFO = $(this).data('INFO');
	                    $(this).find('#infoGagal').html(INFO);
	            },
	        buttons: {
	            Ok: function() {
	                $( this ).dialog( "close" );
	            }
	        }
	    });
	
	    $( "#dialog-form" ).dialog({
	        autoOpen: false,
	        height: 250,
	        width: 350,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah": function() {
	                var bValid = true;
	                allFields.removeClass( "ui-state-error" );
	
	                bValid = bValid && checkLength( namaid, "Nama Kategori Indonesia", 3, 50 );
	                bValid = bValid && checkRegexp( namaid, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	                bValid = bValid && checkLength( namaen, "Nama Kategori English", 3, 50 );
	                bValid = bValid && checkRegexp( namaen, /^([0-9 a-z A-Z])+$/, "Karakter yang di perbolehkan hanya : a-z 0-9" );
	
	                if ( bValid ) {
	                    
	                    $.ajax({
	                        type: "POST",
	                        url: $(this).data('url'),
	                        data: $("#form-tambah-kategori").serialize(),
	                        success: function(data){
	                            if(data.status === 'sukses') {
	                                $( "#dialog-form" ).dialog( "close" );
	                                $("#kategori").empty().load(kategoriSelect);
	                            } else {
	                                updateTips( data.status );
	                            }
	                        },
	                        dataType: 'json',
	                        error : expPost
	                    });
	                    
	                }
	            },
	            "Batal": function() {
	                $( this ).dialog( "close" );
	                allFields.val( "" ).removeClass( "ui-state-error" );
	                updateTips( "Masukkan kategori baru." );
	            }
	        },
	        close: function() {
	            allFields.val( "" ).removeClass( "ui-state-error" );
	            updateTips( "Masukkan kategori baru." );
	        }
	    });
	    
	    $( "#progressbar" ).progressbar({ value: false });
	    $( "#dialog-form-tambahFoto" ).dialog({
	        autoOpen: false,
	        minHeight: 200,
	        width: 550,
	        resizable: false,
	        position: { my: "center top", at: "center top", of: window },
	        modal: true,
	        buttons: {
	            "Tambah Foto": function() {
	                
	                var $this = $("form#formFoto");
	                var $file = $this.find('input:file');
	                
	                $file.each(function(){
	                    var isi = $(this).val();
	                    if( isi == '' )
	                    $(this).addClass( "ui-state-error" );
	                    else
	                    $(this).removeClass( "ui-state-error" );
	                });
	                
	                if($file.hasClass("ui-state-error")) {
	                    var info = 'File upload tidak boleh kosong.!';
	                    $( "#dialog-message" ).dialog("option", "title", "File Upload");
	                    $( "#dialog-message" ).data('INFO',info).dialog( "open");
	                }
	                else {
	                        var formData = new FormData($('#formFoto')[0]);
	                        $.ajax({
	                            xhr: function() {  // custom xhr
	                                var xhr = new window.XMLHttpRequest();
	                                
	                                xhr.upload.addEventListener("progress", function(evt){
	                                 if (evt.lengthComputable) {
										var percentComplete = Math.round(evt.loaded / evt.total ) * 100;
										$( ".progress-label" ).text(percentComplete + '%');
										$( "#progressbar" ).progressbar({
											value: Math.floor(evt.loaded),
											max: Math.floor(evt.total)
										});
										}
	                                }, false);
	                                
	                                return xhr;
	                            },
	                            url: upFotoProduk,  //server script to process data
	                            type: 'POST',
	                            //Ajax events
	                            beforeSend: function(){},
	                            success: function(data){
	                                if(data === 'sukses') {
	                                    $(":file").each(function(){
	                                        $(this).val('');
	                                        $(this).removeClass( "ui-state-error" );
	                                    });
	                                    $('.tambahan').hide();
	                                    $( "#dialog-form-tambahFoto" ).dialog( "close" );
	                                    expPost();
	                                }else{
	                                    $('#results').html( data );
	                                }
	                                $('progress').val('0');
	                            },
	                            error: expPost,
	                            // Form data
	                            data: formData,
	                            //Options to tell JQuery not to process data or worry about content-type
	                            cache: false,
	                            contentType: false,
	                            processData: false
	                        });
	                }
	            },
	            "Batal": function() {
	                $(":file").each(function(){
	                    $(this).val('');
	                    $(this).removeClass( "ui-state-error" );
	                });
	                $('.tambahan').hide();
	                $( this ).dialog( "close" );
	            }
	        },
	        close: function() {
	            $(":file").each(function(){
	                $(this).val('');
	                $(this).removeClass( "ui-state-error" );
	            });
	            $('.tambahan').hide();
	        }
	    });
	    
	    $( "#dialog-hapus" ).dialog({
	      autoOpen: false,
	      resizable: false,
	      height:185,
	      position: { my: "center top", at: "center top", of: window },
	      modal: true,
	      create: function(event, ui, data) {
	                    $(event.target).parent().css('position', 'fixed');
	                },
	      open: function(event, ui) {
	                    var INFO = $(this).data('INFO');
	                    $(this).find('#infoHapus').html(INFO);
	            },
	      buttons: {
	        "Hapus foto": function(event, ui) {
	            var ID = $(this).data('ID');
	            
	            $.ajax({
	                type: "POST",
	                url: hpsFotoURL,
	                data: ({id_img : ID,Hydra:$('input[name="Hydra"]').val()}),
	                success: function(data){
	                    if(data === 'sukses') {
	                        $('#box-img-'+ID).remove();
	                        $( "#dialog-hapus" ).dialog( "close" );
	                    }else{
	                        $( "#dialog-gagal" ).data('INFO',data).dialog( "open");
	                    }
	                },
	                error : expPost
	            });
	        },
	        "Batal": function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	
	    $( "#tambah-kategori" ).button({
	        icons: {
	            primary: "ui-icon-circle-plus"
	        }
	    })
	    .click(function() {
	        var url = $(this).data('url');
	        $( "#dialog-form" ).data('url',url).dialog( "open" );
	    });
	    
	    $( "#tambah_foto" ).click(function() {
	        $( "#dialog-form-tambahFoto" ).dialog( "open" );
	    });
	    
	    $( ".hps-foto" ).click(function(e) {
	        var fotoId = $(this).attr('id'),
	            namaId = $(this).data('nama'),
	            jmlFoto = $('.hps-foto').size();
	            
	        if( jmlFoto > 2 )
	        {
	            $( "#dialog-hapus" ).data('INFO',namaId).data('ID',fotoId).dialog( "open");
	        }
	        else {
	            $( "#dialog-gagal" ).dialog("option", "title", "Gagal Hapus");
	            $( "#dialog-gagal" ).data('INFO','Batas minimal foto untuk produk 1 atau 2 buah.').dialog( "open");
	        }
	        
	        // set jumlah img
	        $('#jml_img').val(jmlFoto+1);
	        
	        e.preventDefault;
	    });
	
	    $( "#tabs-isi" ).tabs();
	    
	});
	
	$(document).ready(function(){
	
	    // set nilai elm
	    $('#jml_img').val($('.hps-foto').size()+1);
	    hargaSpesial();
	    cekTipeFile();
	    $('#kode-warnaInduk').jPicker();
	    $('span.Image').attr('title','Warna untuk produk ini.<br>boleh dikosongkan.');
	    
	    $('#diskon,#harga').change(function(){    
	        hargaSpesial();
	    });
	
	    $("#sembunyikan_foto").click(function (e) {
	        var txt = $("#list_img").is(':visible')?'Tampilkan Foto':'Sembunyikan Foto';
	        
	        $(this).text(txt);
	        $("#list_img").toggle();
	        
	        e.preventDefault();
	    });
	    
		//scroll top
		$(window).scroll(function(){
			if ($(document).scrollTop() > 150) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
	
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	    
	});
	</script>
	<?php else: ?>
	<!-- Minified script -->
	<script type="text/javascript">
	eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('4 Q(){2D.2E()}4 1J(a){6 b=[\'2F\',\'2G\',\'2H\',\'2I\'];6 c=a.R;6 d=c.1K(\'.\');6 e=d[0].1K(\'\\\\\');1L(6 i=0;i<e.16;i++)6 f=e[i];6 g=0;1L(6 i=0;i<b.16;i++){8(b[i]==d[1]){g=1;2J}}8(g==1){D\'1M\'}u{a.R=\'\';D d[1]}}4 1l(){6 a=$(\'#1N\').l(),B=$(\'#B\').l(),1O=B*a,E=1P(B)-1P(1O);8(a==\'\'||B==\'\'||B==0||E==0){$(\'#1m\').L(B);$(\'#1Q\').l(\'0\');8(a==\'\'&&B>0){$(\'#1m\').L(E).1R(\'.\',3,0)}}u{$(\'#1m\').L(E).1R(\'.\',3,0);$(\'#1Q\').l(E)}}4 1n(){$("17:I").1S(4(e){6 a=1J(7);8(a!==\'1M\'){6 b=\'18 1o 2K 2L 2M I M/2N.!\';$("#5-S").5("1p","T","2O 18 1T");$("#5-S").m(\'F\',b).5("v")}e.U})}$(4(){6 h=$(\'#1U\');6 i=$(\'#1U p\').1q()+1;6 j=$("#2P"),19=$("#2Q"),1a=$([]).1V(j).1V(19),1r=$(".2R");4 N(t){1r.L(t).1b("q-s-1W");2S(4(){1r.G("q-s-1W",2T)},2U)}4 1s(o,n,a,b){8(o.l().16>b||o.l().16<a){o.1b("q-s-r");N("2V "+n+" 2W 2X 1X "+a+" 1Y 2Y 2Z "+b+" 1Y.");D k}u{D C}}4 1t(o,a,n){8(!(a.30(o.l()))){o.1b("q-s-r");N(n);D k}u{D C}}4 31(e){8(e.1Z){$(\'1c\').1u({R:e.1v,20:e.E})}}$(\'#32\').21(\'J\',4(e){e.U();8(i<11){$(\'<p 22="1d q-33-34"><17 1e="I" 23="35[]" /><a 36="#" T="37 1f \'+i+\'" 22="24"></a> </p>\').38(h);i++}1n()});$(\'.24\').21(\'J\',4(e){e.U();8(i>2){$(7).39(\'p\').25();i--}});$(".3a").3b({3c:C});$("#5-S").5({V:k,W:C,1w:26,1g:27,X:k,O:{Y:"w x",10:"w x",12:K},v:4(a,b,c){6 d=$(7).m(\'F\');$(7).1h(\'#3d\').13(d)},14:{28:4(){$(7).5("y")}}});$("#5-1i").5({V:k,W:C,1w:29,1g:27,X:k,O:{Y:"w x",10:"w x",12:K},v:4(a,b,c){6 d=$(7).m(\'F\');$(7).1h(\'#3e\').13(d)},14:{28:4(){$(7).5("y")}}});$("#5-H").5({V:k,2a:3f,1g:3g,X:k,O:{Y:"w x",10:"w x",12:K},W:C,14:{"2b":4(){6 b=C;1a.G("q-s-r");b=b&&1s(j,"2c 2d 3h",3,2e);b=b&&1t(j,/^([0-9 a-z A-Z])+$/,"2f 1o 2g 2h 2i : a-z 0-9");b=b&&1s(19,"2c 2d 3i",3,2e);b=b&&1t(19,/^([0-9 a-z A-Z])+$/,"2f 1o 2g 2h 2i : a-z 0-9");8(b){$.1x({1e:"1y",P:$(7).m(\'P\'),m:$("#H-2j-15").3j(),1z:4(a){8(a.2k===\'1A\'){$("#5-H").5("y");$("#15").3k().3l(3m)}u{N(a.2k)}},3n:\'3o\',r:Q})}},"1B":4(){$(7).5("y");1a.l("").G("q-s-r");N("2l 15 2m.")}},y:4(){1a.l("").G("q-s-r");N("2l 15 2m.")}});$("#1j").1j({R:k});$("#5-H-1C").5({V:k,1w:3p,1g:3q,X:k,O:{Y:"w x",10:"w x",12:K},W:C,14:{"2b 1f":4(){6 d=$("H#2n");6 e=d.1h(\'17:I\');e.1k(4(){6 a=$(7).l();8(a==\'\')$(7).1b("q-s-r");u $(7).G("q-s-r")});8(e.3r("q-s-r")){6 f=\'18 2o 3s 2p 3t.!\';$("#5-S").5("1p","T","18 1T");$("#5-S").m(\'F\',f).5("v")}u{6 g=2q 3u($(\'#2n\')[0]);$.1x({3v:4(){6 c=2q K.3w();c.2o.3x("1c",4(a){8(a.1Z){6 b=1D.3y(a.1v/a.E)*29;$(".1c-3z").L(b+\'%\');$("#1j").1j({R:1D.2r(a.1v),20:1D.2r(a.E)})}},k);D c},P:3A,1e:\'1y\',3B:4(){},1z:4(a){8(a===\'1A\'){$(":I").1k(4(){$(7).l(\'\');$(7).G("q-s-r")});$(\'.1d\').1E();$("#5-H-1C").5("y");Q()}u{$(\'#3C\').13(a)}$(\'1c\').l(\'0\')},r:Q,m:g,3D:k,3E:k,3F:k})}},"1B":4(){$(":I").1k(4(){$(7).l(\'\');$(7).G("q-s-r")});$(\'.1d\').1E();$(7).5("y")}},y:4(){$(":I").1k(4(){$(7).l(\'\');$(7).G("q-s-r")});$(\'.1d\').1E()}});$("#5-1F").5({V:k,X:k,2a:3G,O:{Y:"w x",10:"w x",12:K},W:C,3H:4(a,b,c){$(a.3I).3J().3K(\'O\',\'3L\')},v:4(a,b){6 c=$(7).m(\'F\');$(7).1h(\'#3M\').13(c)},14:{"2s M":4(b,c){6 d=$(7).m(\'2t\');$.1x({1e:"1y",P:3N,m:({3O:d,2u:$(\'17[23="2u"]\').l()}),1z:4(a){8(a===\'1A\'){$(\'#3P-3Q-\'+d).25();$("#5-1F").5("y")}u{$("#5-1i").m(\'F\',a).5("v")}},r:Q})},"1B":4(){$(7).5("y")}}});$("#2j-15").3R({3S:{3T:"q-3U-3V-3W"}}).J(4(){6 a=$(7).m(\'P\');$("#5-H").m(\'P\',a).5("v")});$("#3X").J(4(){$("#5-H-1C").5("v")});$(".1G-M").J(4(e){6 a=$(7).1u(\'3Y\'),2v=$(7).m(\'3Z\'),1H=$(\'.1G-M\').1q();8(1H>2){$("#5-1F").m(\'F\',2v).m(\'2t\',a).5("v")}u{$("#5-1i").5("1p","T","40 2s");$("#5-1i").m(\'F\',\'41 1X M 2w 2x 1 42 2 43.\').5("v")}$(\'#2y\').l(1H+1);e.U});$("#2z-44").2z()});$(2A).45(4(){$(\'#2y\').l($(\'.1G-M\').1q()+1);1l();1n();$(\'#46-47\').48();$(\'49.4a\').1u(\'T\',\'4b 2w 2x 4c.<4d>2p 4e.\');$(\'#1N,#B\').1S(4(){1l()});$("#4f").J(4(e){6 a=$("#2B").4g(\':4h\')?\'4i 1f\':\'4j 1f\';$(7).L(a);$("#2B").4k();e.U()});$(K).4l(4(){8($(2A).2C()>26){$(\'.1I\').4m()}u{$(\'.1I\').4n()}});$(\'.1I\').J(4(){$("13, 4o").4p({2C:0},4q);D k})});',62,275,'||||function|dialog|var|this|if||||||||||||false|val|data||||ui|error|state||else|open|center|top|close|||harga|true|return|total|INFO|removeClass|form|file|click|window|text|foto|updateTips|position|url|expPost|value|message|title|preventDefault|autoOpen|modal|resizable|my||at||of|html|buttons|kategori|length|input|File|namaen|allFields|addClass|progress|tambahan|type|Foto|width|find|gagal|progressbar|each|hargaSpesial|txt_spesial|cekTipeFile|yang|option|size|tips|checkLength|checkRegexp|attr|loaded|minHeight|ajax|POST|success|sukses|Batal|tambahFoto|Math|hide|hapus|hps|jmlFoto|scrollup|cekUploadExt|split|for|ok|diskon|pot|parseInt|harga_spesial|formatNumber|change|Upload|addinput|add|highlight|minimal|karakter|lengthComputable|max|live|class|name|remNew|remove|150|320|Ok|100|height|Tambah|Nama|Kategori|50|Karakter|di|perbolehkan|hanya|tambah|status|Masukkan|baru|formFoto|upload|boleh|new|floor|Hapus|ID|Hydra|namaId|untuk|produk|jml_img|tabs|document|list_img|scrollTop|location|reload|png|gif|jpe|jpg|break|Anda|pilih|bukan|gambar|Tipe|nama_kat_id|nama_kat_en|validateTips|setTimeout|1500|500|Bagian|harus|memuat|dan|maksimal|test|progressHandlingFunction|addNew|widget|content|userfile|href|Buang|appendTo|parents|blok|tooltip|track|infoFile|infoGagal|250|350|Indonesia|English|serialize|empty|load|kategoriSelect|dataType|json|200|550|hasClass|tidak|kosong|FormData|xhr|XMLHttpRequest|addEventListener|round|label|upFotoProduk|beforeSend|results|cache|contentType|processData|185|create|target|parent|css|fixed|infoHapus|hpsFotoURL|id_img|box|img|button|icons|primary|icon|circle|plus|tambah_foto|id|nama|Gagal|Batas|atau|buah|isi|ready|kode|warnaInduk|jPicker|span|Image|Warna|ini|br|dikosongkan|sembunyikan_foto|is|visible|Tampilkan|Sembunyikan|toggle|scroll|fadeIn|fadeOut|body|animate|600'.split('|'),0,{}))
	</script>
	<?php endif; ?>

	</head>
	<body>

	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box_produk'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	
	<div id="dialog-form-tambahFoto" title="Tambah Foto" style="display:none">
	    <?php 
	        $attributes = array( 'id' => 'formFoto');
	        echo form_open_multipart('',$attributes); ?>
	        <input type="hidden" name="id_prod_img" id="id_prod_img" value="<?php echo $produk->id_prod_img; ?>" />
	        <input type="hidden" name="jml_img" id="jml_img" value="" />
	        <input type="hidden" name="radio" id="radio" value="induk" />
	        
	        <fieldset style="padding:8px; font-size:12px;" class="text ui-widget-content ui-corner-all">
	            <label>Foto Produk<br><span style="font-size:10px">@min:800x600, @max:1366x1024,@max-size : 1000KB / 1MB</span> </label>
	            <div id="addinput" class="blok ui-helper-clearfix" style="margin-top:5px;">
	                <p class="ui-widget-content">
	                    <input type="file" name="userfile[]" /><a href="#" id="addNew" title="Tambah Foto"></a>
	                </p>
	            </div>
	        </fieldset>
	    </form>
	    <div id="results"></div>
	    <div id="progressbar"><div class="progress-label"></div></div>
	</div>

	<button type="button" class="scrollup">Top</button>
	<div id="stylized" class="myformIframe">
	    <?php
	        $attributes = array( 'id' => 'form');
	        echo form_open($this->config->item('admpath').'/produk/updateform_produk_induk', $attributes);
	    ?>
	    <button type="submit" class="btn-aksi">Update</button>
	    <h1>Update Produk <?php echo $produk->nama_prod; ?></h1>
	    <p>Masukkan data Produk yang baru.</p>
	    
	    <input type="hidden" name="radio" id="radio" value="induk" />
	    <input type="hidden" name="id_prod" id="id_prod" value="<?php echo set_value('id_prod',$produk->id_prod); ?>" />
	    <input type="hidden" name="nama_ori" id="nama_ori" value="<?php echo set_value('nama_ori',$produk->nama_prod); ?>" />
	    <input type="hidden" name="nama_ori_en" id="nama_ori_en" value="<?php echo set_value('nama_ori_en',$produk->nama_prod_en); ?>" />
	    <input type="hidden" name="kode_ori" id="kode_ori" value="<?php echo set_value('kode_ori',$produk->kode_prod); ?>" />
	    
	    <div class="box-induk">
	        <label>Nama ID <span class="small">Nama produk Indonesia</span> </label>
	        <input type="text" name="nama_prod" id="nama_prod" maxlength="100" value="<?php echo set_value('nama_prod',$produk->nama_prod); ?>" />
	        <label style="margin-left:15px">Nama EN <span class="small">Nama produk English</span> </label>
	        <input type="text" name="nama_prod_en" id="nama_prod_en" style="margin-left:-30px" maxlength="100" value="<?php echo set_value('nama_prod_en',$produk->nama_prod_en); ?>" />
	
	        <div class="info-nama">
	            <?php echo form_error('nama_prod'); ?>
	            <?php echo form_error('nama_prod_en'); ?>
	        </div>
	    </div><br />
	    <div style="clear:left"></div>
	
	    <label>Kategori<span class="small">Pilih Kategori produk</span> </label>
	    <select id="kategori" name="kategori" style="margin-right:8px;">
			<?php foreach ($kat_produk as $items): ?>
			<option value="<?php echo $items->id_kategori; ?>" <?php echo set_select('kategori', $items->id_kategori,($items->id_kategori === $produk->kategori_id)?TRUE:FALSE); ?>>
			&nbsp;&nbsp;<?php echo $items->label_id; ?>&nbsp;//&nbsp;<?php echo $items->label_en; ?>&nbsp;&nbsp;
			</option>
			<?php endforeach; ?>
		</select>
	    <a href="#" id="tambah-kategori" data-url="<?php echo site_url($this->config->item('admpath').'/produk/tambah_kategori');?>" class="blok" title="Tambah Kategori"></a>
		<label style="margin-left:20px">SKU <span class="small">Kode produk</span> </label>
		<input type="text" name="kode_prod" id="kode_prod" style="margin-left:-70px" maxlength="50" value="<?php echo set_value('kode_prod',$produk->kode_prod); ?>" />
		<div id="box-kode-warnaInduk" class="blok">
			<input id="kode-warnaInduk" name="kode-warnaInduk" type="hidden" value="<?php echo set_value('kode-warnaInduk',$produk->kode_warna); ?>" />
		</div>
	    <br />
	    <?php echo form_error('kategori'); ?>
	    <?php echo form_error('kode_prod'); ?>
	    
	    <button type="button" id="sembunyikan_foto" class="ptampilFoto">Tampilkan Foto</button>
	    <div style="clear:left"></div>
	    
	    <div id="list_img" class="ui-helper-clearfix">
	        <button type="button" id="tambah_foto" class="ptambahFoto">Tambah Foto</button>
	        <label style="margin-top:-40px;">Foto Produk<span class="small">Foto produk yang terpasang</span> </label>
	        <div class="blok ui-helper-clearfix imgProdbox" style="">
	            <?php foreach($produk_img as $foto): ?>
	            <div class="box-img" id="box-img-<?php echo $foto->id_img; ?>">
	                <img src="<?php echo base_url(); ?>_produk/thumb/thumb_<?php echo $foto->img_src; ?>" align="absmiddle" />
	                <span class="hps-foto" id="<?php echo $foto->id_img; ?>" data-nama="<?php echo $foto->alt_text_img; ?>" title="Buang Foto <?php echo $foto->alt_text_img; ?>">X</span>
	            </div>
	            <?php endforeach; ?>
	        </div>
	        <div style="clear:left; height:10px;"></div>
	    </div>
	
	    <label>Produk Promo<span class="small">Apakah ini produk promo.?</span> </label>
	    <select id="promo" name="promo" style="margin-right:20px;">
	        <option value="off" <?php echo set_select('promo', 'off', ($produk->promo == 'off')?TRUE:FALSE); ?> >&nbsp;Bukan&nbsp;</option>
	        <option value="on" <?php echo set_select('promo', 'on', ($produk->promo == 'on')?TRUE:FALSE); ?> >&nbsp;Ya&nbsp;</option>
	    </select>

	    <label style="margin-right:-20px">Nilai Ratting<span class="small">Ratting format desimal.</span> </label>
		<select name="ratting" id="ratting" style="margin-right:20px">
			<option value="<?php echo $produk->ratting; ?>"><?php echo $produk->ratting; ?></option>
			<option value="1.0">1.0</option>
			<option value="2.0">2.0</option>
			<option value="2.3">2.3</option>
			<option value="2.5">2.5</option>
			<option value="3.0">3.0</option>
			<option value="3.3">3.3</option>
			<option value="3.6">3.6</option>
			<option value="4.1">4.1</option>
			<option value="4.6">4.6</option>
		</select>
	    
	    <div class="box-harga">
	        <label>Diskon Produk<span class="small">Diskon untuk produk ini.?</span> </label>
	        <select id="diskon" name="diskon" style="margin-left:-10px;">
	            <option value="" <?php echo set_select('diskon', ''); ?> >&nbsp;Tidak ada&nbsp;</option>
	            <?php
	            $diskon = $this->config->item('list_diskon_prod');
	            foreach( $diskon as $key => $val ):
	            ?>
	            <option value="<?php echo $key; ?>" <?php echo set_select('diskon', $key, ($produk->diskon == $key)?TRUE:FALSE); ?> >&nbsp;<?php echo $val; ?>&nbsp;</option>
	            <?php endforeach; ?>
	        </select>
	    </div>
	
	    <div style="clear:left"></div>
	    <label>Stok <span class="small">Jumlah stok produk</span> </label>
	    <input type="text" name="stok" id="stok" maxlength="6" style="width:120px; margin-right:20px;" value="<?php echo set_value('stok',$produk->stok); ?>" />
	    
	    <div class="box-harga">
	        <label>Harga <span class="small">Harga produk</span> </label>
	        <input type="text" name="harga" id="harga" maxlength="11" style="width:120px; margin-left:-60px;" value="<?php echo set_value('harga',$produk->harga_prod); ?>" />
	        
	        <div id="box_spesial">
	            <label style="width:120px">Harga Jual<span class="small">Harga Jual Produk</span> </label>
	            <div id="txt_spesial">0</div>
	            <input type="hidden" name="harga_spesial" id="harga_spesial" value="<?php echo set_value('harga_spesial',$produk->harga_spesial); ?>" />
	        </div>
	    </div>    
	    <br /><br />
	    <?php echo form_error('stok'); ?>
	    <div class="box-harga">
	        <?php echo form_error('harga'); ?>
	    </div>
	    
	    <div style="clear:left"></div>
	    <label>Deskripsi <span class="small">Deskripsi untuk produk ini.</span></label>
	
	    <div style="clear:left; height:5px;"></div>
	    <div id="box-deskripsi">
	        <?php echo form_error('deskripsi'); ?>
	        <?php echo form_error('deskripsi_en'); ?>
	         <div id="tabs-isi">
				<ul>
					<li><a href="#isi-id">Deskripsi Indonesia</a></li>
					<li><a href="#isi-en">Deskripsi English</a></li>
				</ul>
				<div id="isi-id">
		        <textarea name="deskripsi" id="deskripsi" ><?php echo set_value('deskripsi',$produk->deskripsi); ?></textarea>
				<?php echo display_ckeditor($deskripsi);?>
				</div>
				<div id="isi-en">
					<textarea name="deskripsi_en" id="deskripsi_en" ><?php echo set_value('deskripsi_en',$produk->deskripsi_en); ?></textarea>
					<?php echo display_ckeditor($deskripsi_en);?>
				</div>
			</div> <!-- End #tabs-isi -->
	    </div>
	    <div style="clear:both; height:10px;"></div>
	    
	  </form>
	</div>
	
	</body>
</html>

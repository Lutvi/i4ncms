<ul id="menu">

<?php if (bs_kode($this->session->userdata('level'), TRUE) == 'superman'): ?>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/atur_menu'); ?>" class="<?php echo ($this->uri->segment(2) == 'atur_menu')?'current':''; ?>"><span>Menu</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/halaman'); ?>" class="<?php echo ($this->uri->segment(2) == 'halaman')?'current':''; ?>"><span>Halaman</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/album'); ?>" class="<?php echo ($this->uri->segment(2) == 'album')?'current':''; ?>"><span>Album</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/video'); ?>" class="<?php echo ($this->uri->segment(2) == 'video')?'current':''; ?>"><span>Video</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/blog'); ?>" class="<?php echo ($this->uri->segment(2) == 'blog')?'current':''; ?>"><span>Blog</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/produk'); ?>" class="<?php echo ($this->uri->segment(2) == 'produk')?'current':''; ?>"><span>Produk</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/order'); ?>" class="<?php echo ($this->uri->segment(2) == 'order')?'current':''; ?>"><span>Order</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/pelanggan'); ?>" class="<?php echo ($this->uri->segment(2) == 'pelanggan')?'current':''; ?>"><span>Pelanggan</span></a></li>
	<li><a style="color:#FF0000;" href="<?php echo site_url($this->config->item('admpath').'/newsletter'); ?>"  class="<?php echo ($this->uri->segment(2) == 'newsletter')?'current':''; ?>"><span><strike>Newsletter</strike></span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/banner'); ?>" class="<?php echo ($this->uri->segment(2) == 'banner')?'current':''; ?>"><span>Banner</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/atur_widget'); ?>" class="<?php echo ($this->uri->segment(2) == 'atur_widget')?'current':''; ?>"><span>Widget</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/atur_module'); ?>" class="<?php echo ($this->uri->segment(2) == 'atur_module')?'current':''; ?>"><span>Module</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/pengaturan'); ?>" class="<?php echo ($this->uri->segment(2) == 'pengaturan')?'current':''; ?>"><span>Pengaturan</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/admincms'); ?>" class="<?php echo ($this->uri->segment(2) == 'admincms')?'current':''; ?>"><span>Admin CMS</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/log_user'); ?>" class="<?php echo ($this->uri->segment(2) == 'log_user')?'current':''; ?>"><span>Log User</span></a></li>
	<li><a style="color:#0012FF;" href="#"><span><strike>Bantuan</strike></span></a></li>
<?php endif; ?>

<?php if (bs_kode($this->session->userdata('level'), TRUE) == 'batman'): ?>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/halaman'); ?>" class="<?php echo ($this->uri->segment(2) == 'halaman')?'current':''; ?>"><span>Halaman</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/album'); ?>" class="<?php echo ($this->uri->segment(2) == 'album')?'current':''; ?>"><span>Album</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/video'); ?>" class="<?php echo ($this->uri->segment(2) == 'video')?'current':''; ?>"><span>Video</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/blog'); ?>" class="<?php echo ($this->uri->segment(2) == 'blog')?'current':''; ?>"><span>Blog</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/produk'); ?>" class="<?php echo ($this->uri->segment(2) == 'produk')?'current':''; ?>"><span>Produk</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/order'); ?>" class="<?php echo ($this->uri->segment(2) == 'order')?'current':''; ?>"><span>Order</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/pelanggan'); ?>" class="<?php echo ($this->uri->segment(2) == 'pelanggan')?'current':''; ?>"><span>Pelanggan</span></a></li>
	<li><a style="color:#FF0000;" href="<?php echo site_url($this->config->item('admpath').'/newsletter'); ?>"  class="<?php echo ($this->uri->segment(2) == 'newsletter')?'current':''; ?>"><span><strike>Newsletter</strike></span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/banner'); ?>" class="<?php echo ($this->uri->segment(2) == 'banner')?'current':''; ?>"><span>Banner</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/admincms'); ?>" class="<?php echo ($this->uri->segment(2) == 'admincms')?'current':''; ?>"><span>Admin CMS</span></a></li>
	<li><a style="color:#0012FF;" href="#"><span><strike>Bantuan</strike></span></a></li>
<?php endif; ?>

<?php if (bs_kode($this->session->userdata('level'), TRUE) == 'ironman'): ?>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/album'); ?>" class="<?php echo ($this->uri->segment(2) == 'album')?'current':''; ?>"><span>Album</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/video'); ?>" class="<?php echo ($this->uri->segment(2) == 'video')?'current':''; ?>"><span>Video</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/blog'); ?>" class="<?php echo ($this->uri->segment(2) == 'blog')?'current':''; ?>"><span>Blog</span></a></li>
	<li><a style="color:#0012FF;" href="#"><span><strike>Bantuan</strike></span></a></li>
<?php endif; ?>

<?php if (bs_kode($this->session->userdata('level'), TRUE) == 'spiderman'): ?>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/produk'); ?>" class="<?php echo ($this->uri->segment(2) == 'produk')?'current':''; ?>"><span>Produk</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/order'); ?>" class="<?php echo ($this->uri->segment(2) == 'order')?'current':''; ?>"><span>Order</span></a></li>
	<li><a href="<?php echo site_url($this->config->item('admpath').'/pelanggan'); ?>" class="<?php echo ($this->uri->segment(2) == 'pelanggan')?'current':''; ?>"><span>Pelanggan</span></a></li>
	<li><a style="color:#FF0000;" href="<?php echo site_url($this->config->item('admpath').'/newsletter'); ?>"  class="<?php echo ($this->uri->segment(2) == 'newsletter')?'current':''; ?>"><span><strike>Newsletter</strike></span></a></li>
	<li><a style="color:#0012FF;" href="#"><span><strike>Bantuan</strike></span></a></li>
<?php endif; ?>

</ul>

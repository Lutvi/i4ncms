
<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

<?php if ($super_admin): ?>
<?php  echo form_open($this->config->item('admpath').'/carivideo'); ?>
<span id="tombol" class="ui-widget-header ui-corner-all">
    <a class="atur" id="install" href="<?php echo site_url($this->config->item('admpath').'/video/tambah'); ?>" title="Tambah Video" >Video</a>
	<?php if(count($video_web) > 0 || count($parent_video) > 0 && $jml_playlist == 0): ?>
    <a class="atur install" id="anak" href="<?php echo site_url($this->config->item('admpath').'/video/tambah_playlist'); ?>" title="Tambah Playlist" >Playlist</a>
    <?php endif; ?>
    <?php if(isset($txtcari)): ?>
	<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/video'); ?>" title="Kembali ke List" >Kembali ke List</a>
	<?php else: ?>
	<button type="button" id="refresh" title="Refresh list" >Refresh</button>
	<?php endif; ?>
    <button type="submit" id="cari" title="Cari Video" >Cari Video</button>
	<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
</span>
<?php echo form_close(); ?>
<?php endif; ?>

<div id="tabel">
  <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_video'); ?>
</div>

<?php else: ?>
	<h3><?php echo $title; ?></h3>
	<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>


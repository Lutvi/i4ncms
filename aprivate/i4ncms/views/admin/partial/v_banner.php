<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">

<?php if ($this->super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
	<?php  echo form_open($this->config->item('admpath').'/caribanner'); ?>
		<span id="tombol" class="ui-widget-header ui-corner-all">
			<?php if(isset($txtcari)): ?>
			<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/banner'); ?>" title="Kembali ke List" >Kembali ke List</a>
			<?php else: ?>
			<a class="atur install" id="istall" href="<?php echo site_url($this->config->item('admpath').'/banner/tambah_iklan'); ?>">Tambah</a>
			<button type="button" id="refresh" title="Refresh list" >Refresh</button>
			<?php endif; ?>
		    <button type="submit" id="cari" title="Cari Iklan">Cari Iklan</button>
			<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama ID" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
		</span>
	<?php echo form_close(); ?>
	<?php echo form_open(); ?>
	<input type="hidden" id="aktifTxt" value="<?php echo ($this->uri->segment(3) === FALSE)?0:$this->session->userdata('aktif_tab_banner'); ?>" />
	<?php echo form_close(); ?>
	
	<?php if(! $this->config->item('iklan')): ?>
		<div class="ui-widget">
			<div class="ui-state-highlight ui-corner-all" style="margin-top: 10px;margin-bottom:-10px; padding:8px;">
			<p style="color:#FF5672;font-weight:normal;font-size:12px"><span class="ui-icon ui-icon-info" style="float: left; margin-right:7px; margin-top:-1px"></span>
			Perhatian..! <u>Fitur iklan sedang tidak aktif</u> iklan tidak akan ditampilkan pada website, <u>Mohon hubungi Webmaster</u> untuk mengaktifkan fitur iklan.</p>
			</div>
		</div>
	<?php endif; ?>
	
	<div id="tabel">
	<?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_iklan'); ?>
	</div><!-- End #table -->

<?php else: ?>
	<h3><?php echo $title; ?></h3>
	<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>

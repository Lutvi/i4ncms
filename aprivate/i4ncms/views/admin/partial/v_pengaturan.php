<style>
.blok {
    margin-bottom:10px;
}
input.txt-big {
    height:20px; padding:2px; font-size:14px; font-weight:bold;
}
input.nmsosmed{
	width:200px;
}
input.urlsosmed{
	width:350px;
}
input.txt-big-panjang {
    height:20px; width:50%; padding:2px; font-size:14px; font-weight:bold;
}
input.txt-normal {
    height:20px; width:80%; padding:2px; font-weight:bold;
}
textarea.ta-tinggi {
    width:37%; height:120px; padding:2px; overflow-x:hidden; float:left;
}
textarea.ta-panjang {
    width:81%; height:90px; padding:2px; overflow-x:hidden;
}
label.lblkiri {float:left; width:120px; font-weight:bold;}

.btn-box { margin-top:10px; margin-bottom:15px; }
#loadTema { position:absolute; top:45%; left:50%; display:none; }
.blurIt { opacity:0.2; }
#temaWeb {  overflow:hidden; }
#selectable .ui-selecting { background: #1BFEE5; padding:3px; }
#selectable .ui-selected { background: #1E90FF; padding:3px; color: white; opacity:1; }
#selectable { list-style-type: none; margin: 0; padding: 0; width: 680px; }
#selectable li { margin: 3px; padding: 3px; float: left; width: 200px; height: 317px; font-size: 4em; text-align: center; cursor:pointer; opacity:0.5; }
#selectable li:hover { opacity:1; }
select.bigSel {padding:5px;font-size:14px}
.info-txt {color:#1166CA;font-weight:bold}

#wrapSosmed{width:600px;height:125px;overflow-y:scroll;border:1px solid #E5E5E5;margin-bottom:2px;padding:2px}
#boxLoadSosmed{width:600px;height:125px;display:none}
</style>

<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
<h3><?php echo $title; ?></h3>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
<br />
<div id="pengaturan-tab" style="font: 12px normal Helvetica, Arial, sans-serif;">
	<ul>
		<li><a href="#tabs-web">Opsi Website</a></li>
		<li><a href="#tabs-tema">Tema Website</a></li>
		<li><a href="#tabs-toko">Toko Online</a></li>
		<li><a href="#tabs-bck">Backup, Restore &amp; CleanUp</a></li>
	</ul>
	
	<div id="tabs-web" style="font: 12px normal Helvetica, Arial, sans-serif">
		<div>
			<div class="blok fltlft" style="width:300px;">
				<h3>Status web</h3>
				<?php $attr = array('id'=>'stWebForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_status_web'),$attr);
				?>
					<input type="hidden" id="stsWebInput" value="<?php echo $sts_web; ?>" />
					<div class="ui-helper-clearfix" id="statusWeb">
						<input type="radio" id="stweb1" name="stweb" <?php echo ($sts_web == 1)?'checked':''; ?> data-post="1" value="1" /><label for="stweb1" title="Pilih ini bila website tidak ingin diakses oleh publik.">Offline</label>
						<input type="radio" id="stweb2" name="stweb" <?php echo ($sts_web == 2)?'checked':''; ?> data-post="2" value="2" /><label for="stweb2" title="Pilih ini jika website sedang dalam masa perbaikan sistem.">Perbaikan</label>
						<input type="radio" id="stweb3" name="stweb" <?php echo ($sts_web == 3)?'checked':''; ?> data-post="3" value="3" /><label for="stweb3" title="Pilih ini bila website ingin dapat diakses oleh publik.">Online</label>
					</div>
				</form>
			</div>

			<div class="blok fltlft" style="width:120px;">
				<h3>Pakai SSL | HTTPS</h3>
				<?php $attr = array('id'=>'ScureWebForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_status_scure_web'),$attr);
				?>
					<input type="hidden" id="ScureWebInput" value="<?php echo $scure_web; ?>" />
					<div class="ui-helper-clearfix" id="ScureWeb">
						<input type="radio" id="scureweb0" name="scureweb" <?php echo ($scure_web == 0)?'checked':''; ?> data-post="0" value="0" /><label for="scureweb0" title="Pilih ini jika frontend ingin HTTP saja.">Off</label>
						<input type="radio" id="scureweb1" name="scureweb" <?php echo ($scure_web == 1)?'checked':''; ?> data-post="1" value="1" /><label for="scureweb1" title="Pilih ini jika frontend ingin di redirect ke HTTPS.">On</label>
					</div>
				</form>
			</div>

			<div class="blok fltlft" style="width:120px;">
				<h3>Status Cache</h3>
				<?php $attr = array('id'=>'CacheWebForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_status_cache_web'),$attr);
				?>
					<input type="hidden" id="CacheWebInput" value="<?php echo $cache_web; ?>" />
					<div class="ui-helper-clearfix" id="CacheWeb">
						<input type="radio" id="cacheweb0" name="cacheweb" <?php echo ($cache_web == 0)?'checked':''; ?> data-post="0" value="0" /><label for="cacheweb0" title="Pilih ini untuk nonaktifkan cache halaman.">Off</label>
						<input type="radio" id="cacheweb1" name="cacheweb" <?php echo ($cache_web == 1)?'checked':''; ?> data-post="1" value="1" /><label for="cacheweb1" title="Pilih ini untuk aktifkan cache (performa memuat lebih cepat).">On</label>
					</div>
				</form>
			</div>

			<div class="blok fltlft" style="width:105px;">
				<h3>Bersihkan Cache</h3>
				<?php $attr = array('id'=>'BersihCacheWebForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/bersihkan_file_cache'),$attr);
				?>
					<input type="hidden" id="BersihCacheWebInput" value="<?php echo $cache_web; ?>" />
					<button id="bersih-cache" <?php echo (! $this->super_admin || $file_cache <= 0)?'disabled="disabled"':''; ?>>Bersihkan</button>
					<div id="chcmeter" style="text-align:center;color:#4D4D4D;font-size:10px"><?php echo $file_cache; ?></div>
				</form>
			</div>

			<div class="blok fltlft" style="width:120px;">
				<h3>Proteksi Admin</h3>
				<?php $attr = array('id'=>'SuperAdminWebForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_status_super_admin_web'),$attr);
				?>
					<input type="hidden" id="SuperAdminWebInput" value="<?php echo $sa_web; ?>" />
					<div class="ui-helper-clearfix" id="SuperAdminWeb">
						<input type="radio" id="saweb0" name="saweb" <?php echo ($sa_web == 0)?'checked':''; ?> data-post="0" value="0" /><label for="saweb0" title="Pilih ini untuk nonaktifkan proteksi admin.">Off</label>
						<input type="radio" id="saweb1" name="saweb" <?php echo ($sa_web == 1)?'checked':''; ?> data-post="1" value="1" /><label for="saweb1" title="Pilih ini untuk mengaktifkan (hanya Super Admin yang bisa menghapus dan membuat konten).">On</label>
					</div>
				</form>
			</div>

			<div class="blok fltlft" style="width:120px;">
				<h3>Tampilkan Iklan</h3>
				<?php $attr = array('id'=>'IklanForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_status_iklan_web'),$attr);
				?>
					<input type="hidden" id="IklanWebInput" value="<?php echo $iklan; ?>" />
					<div class="ui-helper-clearfix" id="IklanWeb">
						<input type="radio" id="iklan0" name="iklan" <?php echo ($iklan == 0)?'checked':''; ?> class="btnoff" data-post="0" value="0" /><label for="iklan0" title="Pilih ini untuk nonaktifkan Iklan pada website.">Off</label>
						<input type="radio" id="iklan1" name="iklan" <?php echo ($iklan == 1)?'checked':''; ?> class="btnon" data-post="1" value="1" /><label for="iklan1" title="Pilih ini untuk mengaktifkan iklan.">On</label>
					</div>
				</form>
			</div>
			<div class="clearfloat"></div>
			
			<div class="blok fltlft" style="width:200px;">
				<h3>Pengaturan Kategori</h3>
					<a id="kategori" href="<?php echo site_url($this->config->item('admpath').'/atur_kategori'); ?>" data-judul="Edit Kategori Konten" title="Edit kategori Konten">Edit Kategori Konten</a> 
			</div>

			<div class="blok fltlft" style="width:200px;">
				<h3>Pengaturan Banner Header</h3>
					<a id="hbanner" href="<?php echo site_url($this->config->item('admpath').'/hbanner'); ?>" data-judul="Atur Banner Header" title="Atur Banner Header">Atur Banner Header</a> 
			</div>

			<div class="clearfloat"></div>
			<br />

			<div class="blok">
				<h3>Identitas Website (Nama &amp; Slogan)</h3>
				<div id="boxLoadIdWeb"></div>
				<?php $attr = array('id'=>'idwebForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_identitas_web'),$attr);
				?>
					<label for="w-namaweb" class="lblkiri">Nama Website</label>
					<input type="text" autocomplete="off" name="w-namaweb" id="w-namaweb" class="txt-normal" value="<?php echo $this->config->item('site_name'); ?>" />
					
					<div class="clearfloat"></div>
					<label for="w-slogan" class="lblkiri">Slogan Web</label>
					<input type="text" autocomplete="off" name="w-slogan" id="w-slogan" class="txt-normal" value="<?php echo $this->config->item('sort_site_description'); ?>" />
					
					<div class="clearfloat"></div>
					<button class="simpan" id="simpan-idweb" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>

			<div class="clearfloat"></div>
			<br />
			<div class="blok fltlft">
				<h3>Robots.txt</h3>
				<div id="boxLoadRobot" style="width:300px"></div>
				<?php $attr = array('id'=>'robotForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_robot'),$attr);
					$robot =  read_file('./robots.txt');
				?>
					<textarea name="robot" id="robot" class="ta-tinggi" style="<?php echo ($this->config->item('metav_edit') === TRUE)?'width:300px':'width:560px'; ?>" spellcheck="false"><?php echo $robot; ?></textarea>
					<div class="clearfloat"></div>
					<button class="simpan" id="simpan-robot" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>

			<div class="blok fltlft" style="width:600px;height:130px;">
				<h3>Sosial Media</h3>
				<?php $attr = array('id'=>'sosmedForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_sosmed'),$attr);
				?>
				<div class="rounded" id="wrapSosmed">
					<br />
					<div id="boxLoadSosmed"></div>
						<div class="blok fltlft" style="width:210px">
						<h3>&nbsp;Nama Sosial Media</h3>
						</div>
						<div class="blok fltlft" style="width:300px">
						<h3>URL Akun Sosial Media</h3>
						</div>
						<div class="clearfloat"></div>
					<div id="inputSosmed">
					<?php foreach($this->config->item('sosmed_url') as $nm => $url): ?>
						<input type="text" autocomplete="off" class="txt-big nmsosmed" name="sosmed[nm][]" value="<?php echo $nm; ?>" />
						<input type="text" autocomplete="off" class="txt-big urlsosmed" name="sosmed[url][]" value="<?php echo $url; ?>" />
					<?php endforeach; ?>
						<input type="text" autocomplete="off" class="txt-big nmsosmed" name="sosmed[nm][]" value="" />
						<input type="text" autocomplete="off" class="txt-big urlsosmed" name="sosmed[url][]" value="" />
					</div>
				</div>
				<button class="simpan" id="simpan-sosmed" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button>
				</form>
			</div>

		<div class="clearfloat"></div>
		<br>
		<?php if ($this->config->item('metav_edit') === TRUE && $super_admin==TRUE): ?>
			<div class="blok">
				<h3>Site Meta Verify dan Analytics</h3>
				<div id="boxLoadmetav" style="width:99%"></div>
				<?php
					$attr = array('id'=>'metavForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_metav'),$attr);
					$metaverify =  read_file('./conf/metaverify.txt');
				?>
					<textarea name="metav" id="metav" style="width:99%;height:120px" spellcheck="false"><?php echo $metaverify; ?></textarea>
					<div class="clearfloat"></div>
					<button class="simpan" id="simpan-metav" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>
		<?php endif; ?>


		<div class="clearfloat"></div>
		<br />
			<div class="blok">
				<h3>META (Keyword &amp; Description)</h3>
				<div id="boxLoadSeo"></div>
				<?php $attr = array('id'=>'seoForm');
					if($super_admin==TRUE)
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_seo'),$attr);
				?>
				<div id="tabs-meta">
					<ul>
						<li><a href="#tabMetaId">Indonesia</a></li>
						<li><a href="#tabMetaEn">English</a></li>
					</ul>
					<div id="tabMetaId">
						<label class="lblkiri" for="w-keyword-id">Keyword ID</label>
						<input type="text" autocomplete="off" name="w-keyword-id" id="w-keyword-id" class="txt-normal" value="<?php echo $this->config->item('site_keyword_id'); ?>" />
						
						<div class="clearfloat"></div>
						<label class="lblkiri" for="w-description-id">Description ID</label>
						<textarea name="w-description-id" id="w-description-id" class="ta-panjang"><?php echo $this->config->item('site_description_id'); ?></textarea>
					</div>
					<div id="tabMetaEn">
						<div class="clearfloat"></div>
						<label class="lblkiri" for="w-keyword-en">Keyword EN</label>
						<input type="text" autocomplete="off" name="w-keyword-en" id="w-keyword-en" class="txt-normal" value="<?php echo $this->config->item('site_keyword_en'); ?>" />
						
						<div class="clearfloat"></div>
						<label class="lblkiri" for="w-description-en">Description EN</label>
						<textarea name="w-description-en" id="w-description-en" class="ta-panjang"><?php echo $this->config->item('site_description_en'); ?></textarea>
					</div>
				</div>
					<div class="clearfloat"></div>
					<br />
					<button class="simpan" id="simpan-seo" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>
		</div>
	</div>

    <div id="tabs-tema" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div>
			<div class="btn-box">
				<span id="toolbar-tema" class="ui-widget-header ui-corner-all">
					<a title="Install Tema Baru" data-judul="Install Tema Baru" class="atur install" href="<?php echo (! $this->super_admin)?'#':site_url($this->config->item('admpath').'/pengaturan/install_tema'); ?>" id="install-tema">Install Tema Baru</a>
					<a id="preview-tema" href="<?php echo base_url(); ?>" target="_blank">Preview Tema</a>
					<button <?php echo ($this->config->item('opsi_conf_tema') == '')?'disabled="disabled"':''; ?> id="atur-tema" title="Atur opsi tema" data-judul="Atur opsi tema">Atur Tema</button>
				</span>
			</div>
				
			<div class="blok" style="position:relative;">    
				<h3>Tema yang terdaftar</h3>
				
				<div id="loadTema">
					<img src="/assets/icons/ajax-loader.gif" />
				</div>

				<div id="temaWeb">

					<?php $attr = array('id'=>'temaForm');
						if($super_admin==TRUE)
						echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_tema'),$attr);
					?>
						<input type="hidden" id="id-tema" name="id-tema" value="<?php echo underscore($this->config->item('theme_id')); ?>" />
						<input type="hidden" id="id-ctema" name="id-ctema" value="<?php echo underscore($this->config->item('theme_id')); ?>" />
					<?php echo form_close(); ?>
					<ol id="selectable" title="Klik untuk memilih tema yang akan diaktifkan">
						<?php
						$tema = $this->web->getAllTemaWeb();
						foreach($tema as $item):
						?>
						<li class="ui-state-default <?php echo  ($this->config->item('theme_id') == $item['nama_tema'])?'ui-selected':''; ?>" id="<?php echo underscore($item['nama_tema'])?>"><img src="<?php echo base_url(); ?>assets/scr-tema/<?php echo $item['gambar']; ?>" width="200" height="317" align="absmidle" /></li>
						<?php endforeach; ?>        
					</ol>
				</div>
			</div>
		</div>
	</div>

    <div id="tabs-toko" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div>
			<div class="blok fltlft" style="width:120px;">
				<h3>Status Toko</h3>
				<?php $attr = array('id'=>'TokoForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_status_toko'),$attr);
				?>
					<input type="hidden" id="stsTokoInput" value="<?php print $ststoko; ?>" />
					<div class="ui-helper-clearfix" id="stsToko">
						<input type="radio" id="toko0" name="toko" <?php echo ($ststoko == 0)?'checked':''; ?> class="btnoff" data-post="0" value="0" /><label for="toko0" title="Pilih ini menonaktifkan Toko Online.">Off</label>
						<input type="radio" id="toko1" name="toko" <?php echo ($ststoko == 1)?'checked':''; ?> class="btnon" data-post="1" value="1" /><label for="toko1" title="Pilih ini untuk mengaktifkan Toko Online.">On</label>
					</div>
				</form>
			</div>

			<div class="blok fltlft" style="width:120px;">
				<h3>Cek Stok Produk</h3>
				<?php $attr = array('id'=>'cekStokForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_cek_stok'),$attr);
				?>
					<input type="hidden" id="cekStokInput" value="<?php print $cekstok; ?>" />
					<div class="ui-helper-clearfix" id="cekStok">
						<input type="radio" id="cekstok0" name="cekstok" <?php echo ($cekstok == 0)?'checked':''; ?> class="btnoff" data-post="0" value="0" /><label for="cekstok0" title="Pilih Off maka produk tetap ditampilkan meskipun stok = 0">Off</label>
						<input type="radio" id="cekstok1" name="cekstok" <?php echo ($cekstok == 1)?'checked':''; ?> class="btnon" data-post="1" value="1" /><label for="cekstok1" title="Pilih On maka produk tidak ditampilan jika stok = 0">On</label>
					</div>
				</form>
			</div>

			<div class="blok fltlft" style="width:200px;">
				<h3>PPN Transaksi</h3>
				<div id="boxLoadPPN"></div>
				<?php $attr = array('id'=>'ppnForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_ppn'),$attr);
				?>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="ppn" id="ppn" value="<?php echo $this->config->item('ppn'); ?>" />
					<button class="simpan" id="simpan-ppn" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>

			<div class="blok fltlft" style="width:200px;">
				<h3>Satuan Jenis</h3>
				<div id="boxLoadSatjen"></div>
				<?php $attr = array('id'=>'satJenForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/satuan_jenis_produk'),$attr);
				?>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="satjen" id="satjen" value="<?php echo $this->config->item('satuan_jenis_produk'); ?>" />
					<button class="simpan" id="simpan-satjen" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>

			<div class="blok fltlft" style="width:200px;">
				<h3>Satuan Produk</h3>
				<div id="boxLoadSatprod"></div>
				<?php $attr = array('id'=>'satProdForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/satuan_produk'),$attr);
				?>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="satprod" id="satprod" value="<?php echo $this->config->item('satuan_produk'); ?>" />
					<button class="simpan" id="simpan-satprod" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>

		<!-- BATAS CS -->
			<div class="clearfloat"></div>
			<div class="blok">
				<div id="boxLoadCS"></div>
				<?php $attr = array('id'=>'csForm');
					echo form_open(site_url($this->config->item('admpath').'/pengaturan/ubah_cs'),$attr);
				?>
				<h3>Detail CS (Nama &amp; Kontak)</h3>
				<label class="lblkiri" for="w-description">Nama Devisi CS</label>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="namacs" id="namacs" value="<?php echo $this->config->item('cs_name'); ?>" />
				<br />
				<label class="lblkiri" for="w-description">Alamat Email CS</label>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="emailcs" id="emailcs" value="<?php echo $this->config->item('cs_email'); ?>" />
				<br />
				<label class="lblkiri" for="w-description">No.Telpon CS</label>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="tlpcs" id="tlpcs" value="<?php echo $this->config->item('cs_telpon'); ?>" />
				<br />
				<label class="lblkiri" for="w-description">SMS CS</label>
					<input type="text" autocomplete="off" class="txt-big-panjang" name="smscs" id="smscs" value="<?php echo $this->config->item('cs_sms'); ?>" />
				<br />
					<button class="simpan" id="simpan-cs" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Simpan</button> 
				</form>
			</div>

			<div class="blok fltlft" style="width:250px;">
				<h3>Pengaturan Metode Pembayaran</h3>
					<a class="popatur" id="aturbayar" href="<?php echo site_url($this->config->item('admpath').'/atur_metode_bayar'); ?>" data-judul="Atur Metode Pembayaran" title="Atur Metode Pembayaran">Atur Metode Pembayaran</a> 
			</div>

			<div class="blok fltlft" style="width:250px;">
				<h3>Pengaturan Kurir Pengiriman</h3>
					<a class="popatur" id="aturongkir" href="<?php echo site_url($this->config->item('admpath').'/atur_kurir_pengiriman'); ?>" data-judul="Atur Kurir Pengiriman" title="Atur Kurir Pengiriman">Atur Kurir Pengiriman</a> 
			</div>
		</div>
	</div><!-- end #tabs-toko -->

    <div id="tabs-bck" style="font: 12px normal Helvetica, Arial, sans-serif;">
		<div id="wrapbackup">
		<div id="tabs-backup">
			<ul>
				<?php if( $jml_all_backup != 0): ?>
				<li><a href="#tabDownloadBackup">Download Backup</a></li>
				<?php endif; ?>
				<?php if($jml_backup == 0 || (! empty($sts_backup->aksi) && $sts_backup->aksi !== 'full')): ?>
				<li><a href="#tabBuatBackup">Buat Backup</a></li>
				<?php endif; ?>
				<?php if( $jml_all_backup != 0 && (empty($sts_backup->status) || $sts_backup->status !== 'restore')): ?>
				<li><a href="#tabRestore">Restore</a></li>
				<?php endif; ?>
				<li><a href="#tabCleanUp">Clean Up</a></li>
			</ul>

			<?php if( $jml_all_backup != 0): ?>
			<div id="tabDownloadBackup">
				<div class="blok">
					<h3>Download File Backup</h3>
					<br />
					<p>Pilih tanggal file backup yang akan didownload.</p>
					<p>File paket backup ini bisa digunakan untuk restore website secara manual.</p>
					<br />
					<p>
						<?php foreach($hist_backup as $row): ?>
						<a style="margin-bottom:5px" class="download" id="btnbckdownload" href="<?php echo base_url().$this->config->item('admpath').'/bckdata/download_backup/'.bs_kode($row->tgl); ?>"><?php echo date('d-m-Y', $row->ts); ?></a>
						<?php endforeach; ?>
					</p>
				</div>
			</div>
			<?php endif; ?>

			<?php if($jml_backup == 0 || (! empty($sts_backup->aksi) && $sts_backup->aksi !== 'full')): ?>
			<div id="tabBuatBackup">
				<div class="blok">
					<h3>Full Backup</h3>
					<br />
					<?php $attr = array('id'=>'bckFullForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/full_backup'),$attr);
					?>
					<p>Direkomendasikan<br />
						<button class="backups" id="btnbckfull" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Jalankan Full Backup</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="blok fltlft manbck">
					<h3>Backup Web</h3>
					<br />
					<?php $attr = array('id'=>'webBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/backup_web'),$attr);
					?>
					<p>
						<button class="backups" id="btnwebbck" <?php echo (! $this->super_admin || (! empty($sts_backup->aksi) && $sts_backup->aksi == 'web'))?'disabled="disabled"':''; ?>>Jalankan Backup Web</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="blok fltlft manbck">
					<h3>Backup DB</h3>
					<br />
					<?php $attr = array('id'=>'dbBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/backup_db'),$attr);
					?>
					<p>
						<button class="backups" id="btndbbck" <?php echo (! $this->super_admin || (! empty($sts_backup->aksi) && $sts_backup->aksi == 'db'))?'disabled="disabled"':''; ?>>Jalankan Backup DB</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="blok fltlft manbck">
					<h3>Backup Media</h3>
					<br />
					<?php $attr = array('id'=>'mediaBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/backup_media'),$attr);
					?>
					<p>
						<button class="backups" id="btnmediabck" <?php echo (! $this->super_admin || (! empty($sts_backup->aksi) && $sts_backup->aksi == 'media'))?'disabled="disabled"':''; ?>>Jalankan Backup Media</button>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
					</p>
					</form>
				</div>
				
				<div class="clearfloat"></div>
			</div>
			<?php endif; ?>

			<?php if( $jml_all_backup != 0 && (empty($sts_backup->status) || $sts_backup->status !== 'restore')): ?>
			<div id="tabRestore">
				<div class="blok">
					<h3>Restore</h3>
					<br />
					<?php $attr = array('id'=>'restoreBckForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/jalankan_restore'),$attr);
					?>
					<p>Ini adalah fitur untuk mengembalikan konfigurasi website.</p>
					<p class="info-txt">Jika file backup hari ini belum ada, maka sistem secara otomatis akan membuatnya sebelum proses restore dijalankan.</p>
					<p>Pilihlah salah satu dari tanggal yang ada.</p>
					<br />
					<p>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
						<select class="bigSel" name="tgl_backup" id="tgl_backup">
						<?php foreach($hist_backup as $row): ?>
						<option value="<?php echo $row->tgl; ?>|<?php echo date('d-m-Y', $row->ts); ?>"><?php echo date('d-m-Y', $row->ts); ?></option>
						<?php endforeach; ?>
						</select>
						<button class="restore" id="btnrestorebck" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Jalankan Restore</button>
					</p>
					</form>
				</div>
			</div>
			<?php endif; ?>

			<div id="tabCleanUp">
				<div class="blok">
					<h3>CleanUp Sistem</h3>
					<br />
					<?php $attr = array('id'=>'cleanupForm');
					echo form_open(site_url($this->config->item('admpath').'/bckdata/cleanup'),$attr);
					?>
					<p>Ini adalah fitur untuk membersihkan file temporary dan clean up sistem, untuk menghemat kouta disk server.</p>
					<p>Total File Temporary : <?php echo $file_tmp; ?></p>
					<br />
					<p>
						<input type="hidden" name="token" id="token" value="<?php echo $this->session->userdata('superman'); ?>" />
						<button class="cleanup" id="btncleanup" <?php echo (! $this->super_admin)?'disabled="disabled"':''; ?>>Jalankan CleanUp</button>
					</p>
					</form>
				</div>
			</div>
		</div><!-- end #tabs-backups -->
		</div><!-- end #wrapbackups -->
	</div><!-- end #tabs-bck -->

</div><!-- end #pengaturan-tab -->

<?php $this->load->view('dynamic_js/'. $this->config->item('admin_theme_id') . '/pengaturan_js'); ?>

<?php else: ?>
	<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>


	<div id="dialog-form" title="Tambah Kategori" style="display:none">
	    <p class="validateTips">Masukkan kategori baru.</p>
	    <br />
	    <?php echo form_open('',array('id'=>'form-tambah-kategori')); ?>
	    <fieldset style="padding:8px; font-size:12px;" class="text ui-widget-content ui-corner-all">
	        <label for="nama">Nama Kategori ID</label>
	        <input type="text" name="nama_kat_id" id="nama_kat_id" maxlength="50" style="padding:5px;" class="text ui-widget-content ui-corner-all"/>
	        <br /><br />
	        <label for="nama">Nama Kategori EN</label>
	        <input type="text" name="nama_kat_en" id="nama_kat_en" maxlength="50" style="padding:5px;" class="text ui-widget-content ui-corner-all"/>
	        <br />
	    </fieldset>
	    </form>
	</div>
	
	<div id="dialog-message" title="Tipe File Ditolak" style="display:none">
	    <p class="tipeFile">
	        <span class="ui-icon ui-icon-circle-close" style="float:left; margin:0 7px 10px 0;"></span>
	        <span id="infoTXT">
	            <span id="infoFile">File yang Anda pilih bukan file foto/gambar.</span>
	            <br />
	            Tipe file harus <b>*.jpg, *.png, *.gif</b>
	        </span>
	    </p>
	</div>
	
	<div id="dialog-gagal" title="Terjadi Kesalahan" style="display:none">
	    <p class="tipeFile">
	        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 10px 0;"></span>
	        <span id="infoGagal">
	        </span>
	    </p>
	</div>
	
	<div id="dialog-hapus" title="Konfirrmasi Hapus" style="display:none; font-size:11px;">
	    <p align="left">
	        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
	        Foto <span style="font-weight:bold;" id="infoHapus"></span> ini akan dihapus,
	        Apakah setuju?
	    </p>
	</div>

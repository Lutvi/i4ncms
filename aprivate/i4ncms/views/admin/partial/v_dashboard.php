<br />
<h3>Dashboard</h3>
<br />
<div id="dashboard-tab" style="font: 12px normal Helvetica, Arial, sans-serif;">

	<ul>
		<li><a href="#aksi-pintas">Aksi Pintas</a></li>
		<?php if (isset($info['dtstat']) && $info['dtstat'] == 'ya'): ?>
		<li><a href="#akses-web">Akses Web</a></li>
		<li><a href="#pengunjung-web"><strike>Trafik Pengunjung</strike></a></li>
		<?php endif; ?>
		<?php if (isset($info['dtorder']) && $info['dtorder'] == 'ya'): ?>
		<li><a href="#order-produk">Orderan Pelanggan</a></li>
		<?php endif; ?>
	</ul>

	<div id="aksi-pintas">
		Aksi pintas menu Admin.
		<div style="position:relative">
		<?php for($i=0;$i<count($sortcut);$i++): ?>
			<a class="lnk_pintas" href="<?php echo $sortcut[$i]['url'];?>.html">
				<div class="ui-widget ui-widget-content ui-corner-all pintasan" style="background:#CEE3F9;text-align:center; height:50px;width:80px;border-color:#86B0CC">
				<img style="margin-top:5px;margin-bottom:10px" src="<?php echo base_url().'assets/icons/admin/'.$sortcut[$i]['ikon']; ?>" />
				<br />
				<span style="font-weight:bold"><?php echo $sortcut[$i]['label'];?></span>
	            </div>
            </a>
	    <?php endfor; ?>
		</div>
	</div>
<?php if (isset($info['dtstat']) && $info['dtstat'] == 'ya'): ?>
	<div id="akses-web">
	    <div>
	        <div id="tabs-web">
	            <ul>
	                <li><a href="#tabs-1">Lokasi</a></li>
	                <li><a href="#tabs-2">Browser</a></li>
	                <li><a href="#tabs-3">Halaman</a></li>
	            </ul>
	             
	            <div id="tabs-1">
	                <div class="chart_container_centered">
	                    <canvas id="lokasi" width="800" height="300">
	                        Your web-browser does not support the HTML 5 canvas element.
	                    </canvas>
	                </div>
	            </div>
	            <div id="tabs-2">
	                 <div class="chart_container_centered">
	                    <canvas id="browser" width="800" height="300">
	                        Your web-browser does not support the HTML 5 canvas element.
	                    </canvas>
	                </div>
	            </div>
	            <div id="tabs-3">
	                <div class="chart_container_centered">
	                    <canvas id="halaman" width="800" height="300">
	                        Your web-browser does not support the HTML 5 canvas element.
	                    </canvas>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>

    <div id="pengunjung-web">
	    <div>
	        <div class="chart_container_centered" style="height:auto; width:100%;">
	            <div id="placeholder" style="margin:0 auto; width:600px;height:320px; display:none;"></div>
	
	            <div id="overview" style="margin:20px auto; width:600px;height:70px;"></div>
	            <p>Drag dari (kiri ke kanan) pada kurva diatas untuk tampilkan detailnya.</p>
	        </div>
	    </div>
    </div>
<?php endif; ?>
<?php if (isset($info['dtorder']) && $info['dtorder'] == 'ya'): ?>
    <div id="order-produk">
		<div style="position:relative">
			<h3>Data Proses Order</h3>
			<?php for($i=0;$i<count($orderan);$i++): ?>
			<div class="ui-widget ui-widget-content ui-corner-all pintasan" style="background:#CEE3F9;text-align:center; height:100px;width:120px;border-color:#86B0CC">
			<img height="60" style="margin-top:5px;margin-bottom:10px" src="<?php echo base_url().'assets/icons/admin/orderan/'.$orderan[$i]['ikon']; ?>" />
			<br />
			<span><?php echo $orderan[$i]['label'];?></span>
			<br />
			<span style="font-weight:bold"><?php echo $orderan[$i]['total'];?></span>
            </div>
            <?php endfor; ?>

			<a class="lnk_pintas" href="<?php echo base_url().$this->config->item('admpath'); ?>/order.html">
				<div class="ui-widget ui-widget-content ui-corner-all pintasan" style="background:#E6EFF9;text-align:center; height:90px;width:120px;border-color:#86B0CC;margin-top:10px">
				<img height="60" style="margin-top:5px;margin-bottom:10px" src="<?php echo base_url().'assets/icons/admin/orderan/detail.png'; ?>" />
				<br />
				<span style="font-weight:bold">Lihat Orderan</span>
				</div>
            </a>
		</div>
    </div>
<?php endif; ?>
</div><!-- end #dashboard-tab -->
        
<?php
/* ===============================
 * Hardcode Contoh Array Data PHP
 * ===============================
 */
 
// Lokasi
//$dtLokasi['data'] = array(72,32,10,56,17,54);
//$dtLokasi['label'] = array('Jakarta','Medan','Surabaya','Pontianak','Bandung','Kota Lainnya');
// Browser
//$dtBrowser['data'] = array(51.62,31.3,10.06,4.27,1.96,0.78);
//$dtBrowser['label'] = array('IE','Firefox','Chrome','Safari','Opera','Lainnya');
// Halaman
//$dtHalaman['data'] = array(22,57,66,32,45,56);
//$dtHalaman['label'] = array('Artikel','Produk','Album','Profil','Kontak','Lainnya');

// Pengunjung
$dtPengunjung = array ( 
                    array ( 1196463600000,0 ), array ( 1196550000000,0 ),
                    array ( 1196636400000,0 ), array ( 1196722800000,77 ),
                    array ( 1196809200000,3636 ), array ( 1196895600000,3575 ),
                    array ( 1196982000000,2736 ), array ( 1197068400000,1086 ),
                    array ( 1197154800000,676 ), array ( 1197241200000,1205 ),
                    array ( 1197327600000,906 ), array ( 1197414000000,710 ),
                    array ( 1197500400000,639 ), array ( 1197586800000,540 ),
                    array ( 1197673200000,435 ), array ( 1197759600000,301 ),
                    array ( 1197846000000,575 ), array ( 1197932400000,481 ),
                    array ( 1198018800000,591 ), array ( 1198105200000,608 ),
                    array ( 1198191600000,459 ), array ( 1198278000000,234 ),
                    array ( 1198364400000,1352 ), array ( 1198450800000,686 ),
                    array ( 1198537200000,279 ), array ( 1198623600000,449 ),
                    array ( 1198710000000,468 ), array ( 1198796400000,392 ),
                    array ( 1198882800000,282 ), array ( 1198969200000,208 ),
                    array ( 1199055600000,229 ), array ( 1199142000000,177 ),
                    array ( 1199228400000,374 ), array ( 1199314800000,436 ),
                    array ( 1199401200000,404 ), array ( 1199487600000,253 ),
                    array ( 1199574000000,218 ), array ( 1199660400000,476 ),
                    array ( 1199746800000,462 ), array ( 1199833200000,448 ),
                    array ( 1199919600000,442 ), array ( 1200006000000,403 ),
                    array ( 1200092400000,204 ), array ( 1200178800000,194 ),
                    array ( 1200265200000,327 ), array ( 1200351600000,374 ),
                    array ( 1200438000000,507 ), array ( 1200524400000,546 ),
                    array ( 1200610800000,482 ), array ( 1200697200000,283 ),
                    array ( 1200783600000,221 ), array ( 1200870000000,483 ),
                    array ( 1200956400000,523 ), array ( 1201042800000,528 ),
                    array ( 1201129200000,483 ), array ( 1201215600000,452 ),
                    array ( 1201302000000,270 ), array ( 1201388400000,222 ),
                    array ( 1201474800000,439 ), array ( 1201561200000,559 ),
                    array ( 1201647600000,521 ), array ( 1201734000000,477 ),
                    array ( 1201820400000,442 ), array ( 1201906800000,252 ),
                    array ( 1201993200000,236 ), array ( 1202079600000,525 ),
                    array ( 1202166000000,477 ), array ( 1202252400000,386 ),
                    array ( 1202338800000,409 ), array ( 1202425200000,408 ),
                    array ( 1202511600000,237 ), array ( 1202598000000,193 ),
                    array ( 1202684400000,357 ), array ( 1202770800000,414 ),
                    array ( 1202857200000,393 ), array ( 1202943600000,353 ),
                    array ( 1203030000000,364 ), array ( 1203116400000,215 ),
                    array ( 1203202800000,214 ), array ( 1203289200000,356 ),
                    array ( 1203375600000,399 ), array ( 1203462000000,334 ),
                    array ( 1203548400000,348 ), array ( 1203634800000,243 ),
                    array ( 1203721200000,126 ), array ( 1203807600000,157 ),
                    array ( 1203894000000,288 )
                );
                
   //print_r($dtPengunjung[0]);
?>

        
<script type="application/javascript">

    var statLokasi = new AwesomeChart('lokasi');
    statLokasi.chartType = "pareto";
    //statLokasi.title = "Lokasi";
    statLokasi.data = <?php echo json_encode($dtLokasi['data']); ?>;
    statLokasi.labels = <?php echo json_encode($dtLokasi['label']); ?>;
    statLokasi.colors = ['#FF5600', '#F10E0E', '#9616A6','#22B5FF', '#F1730F', '#FF5063',  '#0F2BE7', '#4F03C4', '#07B2A4', '#A3B40A', '#27002C'];
    statLokasi.chartLineStrokeStyle = 'rgba(0, 102, 173, 0.5)';
    statLokasi.chartPointFillStyle = 'rgb(0, 102, 173)';
    statLokasi.draw();
    
    var statWeb = new AwesomeChart('browser');
    statWeb.chartType = "pareto";
    //statWeb.title = "Browser";
    statWeb.data = <?php echo json_encode($dtBrowser['data']); ?>;
    statWeb.labels = <?php echo json_encode($dtBrowser['label']); ?>;
    statWeb.colors = statLokasi.colors;
    statWeb.chartLineStrokeStyle = 'rgba(166, 4, 211, 0.5)';
    statWeb.chartPointFillStyle = 'rgb(166, 4, 211)';
    statWeb.draw();
    
    var statHalaman = new AwesomeChart('halaman');
    statHalaman.chartType = "pareto";
    //statHalaman.title = "Halaman";
    statHalaman.data = <?php echo json_encode($dtHalaman['data']); ?>;
    statHalaman.labels = <?php echo json_encode($dtHalaman['label']); ?>;
    statHalaman.colors = statLokasi.colors;
    statHalaman.chartLineStrokeStyle = 'rgba(154, 198, 60, 1)';
    statHalaman.chartPointFillStyle = 'rgb(154, 198, 60)';
    statHalaman.draw();
    
    
$(function () {
    var d = <?php echo json_encode($dtPengunjung); ?>;
    
    // Tabs-Web
    $( "#tabs-web" ).tabs();

    // first correct the timestamps - they are recorded as the daily
    // midnights in UTC+0100, but Flot always displays dates in UTC
    // so we have to add one hour to hit the midnights in the plot
    for (var i = 0; i < d.length; ++i)
      d[i][0] += 60 * 60 * 1000;

    // helper for returning the weekends in a period
    function weekendAreas(axes) {
        var markings = [];
        var d = new Date(axes.xaxis.min);
        // go to the first Saturday
        d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
        d.setUTCSeconds(0);
        d.setUTCMinutes(0);
        d.setUTCHours(0);
        var i = d.getTime();
        do {
            // when we don't set yaxis, the rectangle automatically
            // extends to infinity upwards and downwards
            markings.push({ xaxis: { from: i, to: i + 2 * 24 * 60 * 60 * 1000 } });
            i += 7 * 24 * 60 * 60 * 1000;
        } while (i < axes.xaxis.max);

        return markings;
    }
    
    var options = {
        xaxis: { mode: "time", tickLength: 5 },
        selection: { mode: "x" },
        grid: { markings: weekendAreas }
    };
    
    var plot = $.plot($("#placeholder"), [d], options);
    
    var overview = $.plot($("#overview"), [d], {
        series: {
            lines: { show: true, lineWidth: 1 },
            shadowSize: 0
        },
        xaxis: { ticks: [], mode: "time" },
        yaxis: { ticks: [], min: 0, autoscaleMargin: 0.1 },
        selection: { mode: "x" }
    });

    // now connect the two
    $("#placeholder").bind("plotselected", function (event, ranges) {
        // do the zooming
        plot = $.plot($("#placeholder"), [d],
                      $.extend(true, {}, options, {
                          xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
                      }));

        // don't fire event on the overview to prevent eternal loop
        overview.setSelection(ranges, true);
    });
    
    $("#overview").bind("plotselected", function (event, ranges) {
        $("#placeholder").show();
        plot.setSelection(ranges);
    });
});   
 
</script>


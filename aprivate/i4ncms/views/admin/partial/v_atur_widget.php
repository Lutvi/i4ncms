<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

<?php  echo form_open($this->config->item('admpath').'/cariwidget'); ?>
<span id="tombol" class="ui-widget-header ui-corner-all">
    <a class="atur" id="install" href="<?php echo site_url($this->config->item('admpath').'/atur_widget/install_widget'); ?>" title="Install Widget" >Install</a>
    <button id="tambah" title="Tambah Widget" >Tambah</button>
    <?php if(isset($txtcari)): ?>
	<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/atur_widget'); ?>" title="Kembali ke List" >Kembali ke List</a>
	<?php else: ?>
	<button type="button" id="refresh" title="Refresh list" >Refresh</button>
	<?php endif; ?>
    <button type="submit" id="cari" title="Cari Widget" >Cari Widget</button>
	<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama/Judul" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
</span>
<?php echo form_close(); ?>

<?php if($this->config->item('manual_install')): ?>
<div id="stylized" class="myform">
    <?php
    $attributes = array( 'id' => 'form');
    echo form_open($this->config->item('admpath').'/atur_widget/add_widget', $attributes);
    ?>
    <h1>Tambah widget baru</h1>
    <p>Masukkan data widget yang baru.</p>
    <label>Nama widget <span class="small">Nama widget yang akan dibuat</span> </label>
    <input type="text" name="widget_name" id="widget_name" value="<?php echo set_value('widget_name'); ?>" />
    <?php echo form_error('widget_name'); ?>
    <label>Judul widget ID<span class="small">Judul widget Indonesia</span> </label>
    <input type="text" name="judul_widget" id="judul_widget" value="<?php echo set_value('judul_widget'); ?>" />
    <?php echo form_error('judul_widget'); ?>
    <label>Judul widget EN <span class="small">Judul widget English</span> </label>
    <input type="text" name="judul_widget_en" id="judul_widget_en" value="<?php echo set_value('judul_widget_en'); ?>" />
    <?php echo form_error('judul_widget_en'); ?>
    <label>CSS <span class="small">File CSS widget, misal:(w01.css, w02.css,dst.)</span> </label>
    <input type="text" name="widget_css" id="widget_css" value="<?php echo set_value('widget_css'); ?>" />
    <?php echo form_error('widget_css'); ?>
    <label>JS <span class="small">File JS widget, misal:(w01.js, w02.js,dst.)</span> </label>
    <input type="text" name="widget_js" id="widget_js" value="<?php echo set_value('widget_js'); ?>" />
    <?php echo form_error('widget_js'); ?>
    <label>HTML <span class="small">HTML bila tidak menggunakan view</span> </label>
    <textarea rows="5" name="widget_html"><?php echo set_value('widget_html'); ?></textarea>
    <?php echo form_error('widget_html'); ?>
    <label>Status <span class="small">Isi (1 = on, 0 = off)</span> </label>
    <input type="text" name="widget_status" id="widget_status" value="<?php echo set_value('widget_status',0); ?>" />
    <?php echo form_error('widget_status'); ?>
    <label>Path <span class="small">Boleh(kosongkan saja).</span> </label>
    <input type="text" name="widget_path" id="widget_path" value="<?php echo set_value('widget_path'); ?>" />
    <?php echo form_error('widget_path'); ?>
    <label>Deskripsi <span class="small">Keterangan untuk widget</span> </label>
    <textarea rows="5" name="widget_desc"><?php echo set_value('widget_desc'); ?></textarea>
    <?php echo form_error('widget_desc'); ?>
    <button type="submit">Submit</button>&nbsp;&nbsp;<button type="button" id="batal">Batal</button>
    <!--<div class="spacer"></div>-->
  </form>
</div>
<?php endif; ?>

<div id="tabel">  
  <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_widget'); ?>
</div>

<?php else: ?>
<h3><?php echo $title; ?></h3>
<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>



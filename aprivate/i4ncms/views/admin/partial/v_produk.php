
<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
	<div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

<?php if ($super_admin): ?>
<?php  echo form_open($this->config->item('admpath').'/cariproduk'); ?>
<span id="tombol" class="ui-widget-header ui-corner-all">
	<a class="atur" id="install" href="<?php echo site_url($this->config->item('admpath').'/produk/tambah'); ?>" title="Tambah Produk" >Tambah</a>
	<?php if(isset($txtcari)): ?>
	<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/produk'); ?>" title="Kembali ke List" >Kembali ke List</a>
	<?php else: ?>
	<button type="button" id="refresh" title="Refresh list" >Refresh</button>
	<?php endif; ?>
    <button type="submit" id="cari" title="Cari Produk">Cari Produk</button>
	<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama ID" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
</span>
<?php echo form_close(); ?>
<?php endif; ?>

<div id="tabel"> 
  <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_produk'); ?>
</div>

<?php else: ?>
	<h3><?php echo $title; ?></h3>
	<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>

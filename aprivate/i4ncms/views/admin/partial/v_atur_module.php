
<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>

  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

<?php  echo form_open($this->config->item('admpath').'/carimodule'); ?>
<span id="tombol" class="ui-widget-header ui-corner-all">
    <a class="atur" id="install" href="<?php echo site_url($this->config->item('admpath').'/atur_module/install_module'); ?>" title="Install Module" >Install</a>
    <button id="tambah" title="Tambah Module" >Tambah</button>
    <?php if(isset($txtcari)): ?>
	<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/atur_module'); ?>" title="Kembali ke List" >Kembali ke List</a>
	<?php else: ?>
	<button type="button" id="refresh" title="Refresh list" >Refresh</button>
	<?php endif; ?>
    <button type="submit" id="cari" title="Cari Module" >Cari Module</button>
	<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama Module" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
</span>
<?php echo form_close(); ?>

<?php if($this->config->item('manual_install')): ?>
<div id="stylized" class="myform">
	<?php
    $attributes = array( 'id' => 'form');
    echo form_open($this->config->item('admpath').'/atur_module/add_module', $attributes);
    ?>
    <h1>Tambah module baru</h1>
    <p>Masukkan data module yang baru.</p>
    <label>Nama module <span class="small">Nama module yang sudah dibuat</span> </label>
    <input type="text" name="module_name" id="module_name" value="<?php echo set_value('module_name'); ?>" />
    <?php echo form_error('module_name'); ?>
    <label>CSS <span class="small">File CSS module, misal:(w01.css, w02.css,dst.)</span> </label>
    <input type="text" name="module_css" id="module_css" value="<?php echo set_value('module_css'); ?>" />
    <?php echo form_error('module_css'); ?>
    <label>JS <span class="small">File JS module, misal:(w01.js, w02.js,dst.)</span> </label>
    <input type="text" name="module_js" id="module_js" value="<?php echo set_value('module_js'); ?>" />
    <?php echo form_error('module_js'); ?>
    <label>HTML <span class="small">HTML bila tidak menggunakan view</span> </label>
    <textarea rows="5" name="module_html"><?php echo set_value('module_html'); ?></textarea>
    <?php echo form_error('module_html'); ?>
    <label>Status <span class="small">Isi (1 = on, 0 = off)</span> </label>
    <input type="text" name="module_status" id="module_status" value="<?php echo set_value('module_status',0); ?>" />
    <?php echo form_error('module_status'); ?>
    <label>Path <span class="small">misal : (mod_baru/mod_baru/index).</span> </label>
    <input type="text" name="module_path" id="module_path" value="<?php echo set_value('module_path'); ?>" />
    <?php echo form_error('module_path'); ?>
    <label>Deskripsi <span class="small">Keterangan untuk module</span> </label>
    <textarea rows="5" name="module_desc"><?php echo set_value('module_desc'); ?></textarea>
    <?php echo form_error('module_desc'); ?>
    <button type="submit">Submit</button>&nbsp;&nbsp;<button type="button" id="batal">Batal</button>
    <!--<div class="spacer"></div>-->
  </form>
</div>
<?php endif; ?>

<div id="tabel">   
  <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_module'); ?>
</div>

<?php else: ?>
<h3><?php echo $title; ?></h3>
<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>


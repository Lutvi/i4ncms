<div id="salah_db" title="Gagal Database" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Telah terjadi kesalahan database.<br /><br />
        Silahkan coba beberapa menit lagi.<br />
        Mungkin server database sedang <b>Di Restart</b>.
	</p>
</div>
<div id="gagal" title="Gagal Admin" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		<span id="info"></span>
	</p>
</div>
<div id="info-sukses" title="Aksi sukses" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		<span id="txtsukses"></span>
	</p>
</div>
<div id="nohapus" title="Perhatian" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Anda tidak memiliki otoritas untuk menghapus.!<br /><br />
        Butuh akses <b>Super Admin</b> untuk menghapus.
	</p>
</div>
<div id="noedit" title="Perhatian" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Anda tidak memiliki otoritas untuk mengubah.!<br /><br />
        Butuh akses <b><?php echo ($this->config->item('super_aktif') == TRUE)?'Super':'';?>  Admin</b> untuk mengubah.
	</p>
</div>
<div id="noatur" title="Perhatian" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Aplikasi ini statis tidak memiliki Pengaturan.!<br />
	</p>
</div>
<div id="dialog-hapus" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Item ini akan dihapus.
		<br>
		Apakah Anda setuju?
	</p>
</div>

<div id="dialog-wstatus" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Status website ini akan dirubah.
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>
<div id="dialog-wstatusscure" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Status HTTP akan dirubah,
		<br><br>
		Server Anda harus mendukung layanan SSL
		<br>
		dan <u>Harus memiliki Sertifikat SSL yang telah di verifikasi.!</u>
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>
<div id="dialog-wstatuscache" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Status Cache akan dirubah,
		<br><br>
		Cache dapat mepercepat proses memuat halaman,
		<br>
		<u>tapi data baru akan terupdate setelah masa cachenya habis.!</u>
		<br><br>
		Masa cache untuk website ini adalah <u><?php echo $this->config->item('lama_cache_front')/60; ?> menit</u>.
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>
<div id="dialog-saweb" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 90px 0;"></span>
		Status Proteksi Admin akan dirubah,
		<br><br>
		Apabila status "On" maka <u>hanya level Super Admin</u> yang memiliki Akses penuh merubah konten,
		<br><br>
		Jika "Off" maka <u>level Admin</u> bisa mengakses beberapa fitur Super Admin.
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>
<div id="dialog-iklan" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 90px 0;"></span>
		Status Iklan akan dirubah,
		<br><br>
		Apabila status "On" maka <u>iklan ditampilkan</u>,
		<br><br>
		Jika "Off" maka <u>iklan tidak ditampilkan</u>.
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>
<div id="dialog-toko" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 90px 0;"></span>
		Status Toko Online akan dirubah,
		<br><br>
		Apabila status "On" maka <u>Toko Online ditampilkan</u>,
		<br><br>
		Jika "Off" maka <u>Toko Online tidak ditampilkan</u>.
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>
<div id="dialog-cekstok" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 90px 0;"></span>
		Status cek Stok Produk akan dirubah,
		<br><br>
		Apabila status "On" maka <u>Produk tetap ditampilkan meskipun stok = 0</u>,
		<br><br>
		Jika "Off" maka <u>produk tidak ditampilan jika stok = 0</u>.
		<br><br>
		Apakah Anda  yakin?
	</p>
</div>

<!-- Order -->
<div id="dialog-lanjut" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		<span id="info"></span>
	</p>
</div>

<div id="dialog-batal" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Transaksi ini akan dibatalkan,
		Apakah Anda yakin?
	</p>
</div>

<div id="dialog-hapus-order" title="Konfirmasi" style="display:none; font-size:11px;">
	<p align="left">
		<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
		Transaksi ini akan dihapus,
		Apakah Anda yakin?
	</p>
</div>

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
	<style>
	.loadtbl {width:125%;}
	</style>
	
	<?php if ($super_admin==TRUE): ?>
	<?php  echo form_open($this->config->item('admpath').'/cariorder'); ?>
		<div style="font: 12px normal Helvetica, Arial, sans-serif;margin:10px auto;">
			<span id="tombol" class="ui-widget-header ui-corner-all">
				<?php if(isset($txtcari)): ?>
				<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/order'); ?>" title="Kembali ke List" >Kembali ke List</a>
				<?php else: ?>
				<button type="button" id="refresh" title="Refresh list" >Refresh</button>
				<?php endif; ?>
			    <button type="submit" id="cari" title="Cari No Struk">Cari No.Struk</button>
				<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik No.Struk" value="<?php echo strtoupper(humanize((isset($txtcari))?$txtcari:''));?>" style="padding:2px">
			</span>
		</div>
	<?php echo form_close(); ?>
	<?php echo form_open(); ?>
	<input type="hidden" id="aktifTxt" value="<?php echo ($this->uri->segment(3) === FALSE)?0:$this->session->userdata('aktif_tab'); ?>" />
	<?php echo form_close(); ?>
	<?php endif; ?>
	
	<div id="tabel"style="width:100%;">
	<?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_order'); ?>
	</div> <!-- End id="tabel" -->

<?php else: ?>

	<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
		<h3><?php echo $title; ?></h3>
		<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
	</div>

<?php endif; ?>

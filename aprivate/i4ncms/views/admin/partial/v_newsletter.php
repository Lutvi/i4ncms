
<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

<?php if ($super_admin==TRUE): ?>
<span id="tombol" class="ui-widget-header ui-corner-all">
    <button id="refresh" title="Refresh list" >Refresh</button>
    <button id="newsletter" onclick="javascript:alert('kirim newsletter ke semua.!'); return false;" title="Kirim Newsletter" >Kirim Newsletter</button>
    <button id="cari" title="Cari Pembeli" >Cari Pelanggan</button>
	<input type="text" id="cari-txt" name="cari-txt" autocomplete="off" style="padding:2px">
</span>

<?php  echo form_open(''); ?>
<?php echo form_close(); ?>
<?php endif; ?>

<div id="tabel"> 
  <table id="rounded-corner" summary="Menu">
    <thead>
      <tr align="center">
        <th class="rounded-company" scope="col">Nama Pelanggan</th>
        <th scope="col" width="50">Email</th>
        <th scope="col" width="80">Total Trans</th>
        <th scope="col">Kode Kupon</th>
        <th scope="col" width="50">Poin</th>
        <th scope="col" width="50">Prioritas</th>
        <th class="rounded-q4" scope="col" width="90">Opsi</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="6" class="rounded-foot-left"><em>List Langganan Newsletter yang terdaftar</em></td>
        <td class="rounded-foot-right">&nbsp;</td>
      </tr>
    </tfoot>
    <tbody>
      <?php if(count($album_web) > 0): ?>
      <?php $i=0; $jml = $this->subcribe->getCountNewsletter(); ?>
      <?php foreach ($album_web as $row): ?>
      <tr class="edit_tr <?php echo ($i%2==1?'odd':'even') ?>">
        <td><?php echo dekodeString($row->nama_lengkap); ?></td>
        <td><?php echo dekodeString($row->email_cust); ?></td>
        <td align="right"><?php echo number_format($row->total_transaksi, 0,'','.'); ?></td>
        <td><?php echo $row->kode_kupon; ?></td>
        <td><?php echo $row->jml_poin; ?></td>
        <td><?php echo $row->prioritas_cust; ?></td>
        <td align="center">
        	<a class="edit" id="<?php echo $row->id; ?>" href="#" title="Ubah status"></a>&nbsp;
          <a class="<?php echo ($super_admin==FALSE)?'dis_hapus':'hapus'; ?>" id="hapus_<?php echo $row->id; ?>" href="#" title="Hapus <?php echo humanize(dekodeString($row->nama_lengkap)); ?>"></a>&nbsp;
          <a class="atur" href="<?php echo site_url($this->config->item('admpath').'/newsletter/detail/'.$row->id.'/show'); ?>" title="Update <?php echo ucwords(dekodeString($row->nama_lengkap)); ?>"></a>
        </td>
      </tr>
      <?php $i++; ?>
      <?php endforeach; ?>
      <?php else: ?>
      <tr>
        <td colspan="7"><em>Data Kosong</em></td>
      </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>

<?php else: ?>
	<h3><?php echo $title; ?></h3>
	<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>


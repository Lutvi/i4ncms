<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">

<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
	<?php  echo form_open($this->config->item('admpath').'/carilog_user'); ?>
		<span id="tombol" class="ui-widget-header ui-corner-all">
			<?php if(isset($txtcari)): ?>
			<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/log_user'); ?>" title="Kembali ke List" >Kembali ke List</a>
			<?php else: ?>
			<button type="button" id="refresh" title="Refresh list" >Refresh</button>
			<?php endif; ?>
		    <button type="submit" id="cari" title="Cari Log">Cari Log</button>
			<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik IP/Browser-Agent/Lokasi" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
		</span>
	<?php echo form_close(); ?>
	<?php echo form_open(); ?>
	<input type="hidden" id="aktifTxt" value="<?php echo ($this->uri->segment(3) === FALSE)?0:$this->session->userdata('aktif_tab_log'); ?>" />
	<?php echo form_close(); ?>
	
	<div id="tabel">
	<?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_log_user'); ?>
	</div><!-- End #table -->
<?php else: ?>
	<h3><?php echo $title; ?></h3>
	<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
<?php endif; ?>

</div>

<!DOCTYPE html>
<html>
	<head>
	<title>List Gallery Album</title>
	<?php $this->load->view($this->config->item('admin_theme_id').'/main/head'); ?>
	</head>

	<body class="i4nCMS">
		<div id="container" class="rounded" style="width:95%">
		
		<div id="mainContentIframe">
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box'); ?>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
		
		<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
		<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
		  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

		<?php  echo form_open($this->config->item('admpath').'/carigallery'); ?>
		<div class="ui-helper-clearfix">
		<span id="tombol" class="ui-widget-header ui-corner-all">
		    <a id="install" href="<?php echo site_url($this->config->item('admpath').'/album/tambah_gallery_album/'.$id_album); ?>" title="Tambah Gallery" >Tambah</a>
		    <?php if(isset($txtcari)): ?>
		    <a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/album/tabel_gallery/'.$id_album); ?>" title="Kembali ke List" >Kembali ke List</a>
		    <?php else: ?>
		    <button type="button" id="refresh" title="Refresh list" >Refresh</button>
		    <?php endif; ?>
		    <button type="submit" id="cari" title="Cari Gallery" >Cari Gallery</button>
			<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama ID" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
			<input type="hidden" name="parent_album" id="parent_album" value="<?php echo set_value('parent_album', $id_album); ?>" />
		</span>
		</div>
		<?php echo form_close(); ?>

		<button type="button" class="scrollup">Top</button>
		<div id="tabel">
		  <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_gallery'); ?>
		</div>

		<?php else: ?>
			<h3><?php echo $title; ?></h3>
			<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
		<?php endif; ?>

		</div>
		
		</div><!-- end #mainContent -->
		</div><!-- end #container -->
	</body>
</html>

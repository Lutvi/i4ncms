<!DOCTYPE html>
<html>
	<head>
	<title>List Kategori Konten</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	<?php $this->load->view('dynamic_js'); ?>
	</head>

	<body class="i4nCMS">
		<div id="container" class="rounded" style="width:80%">

		<div id="mainContentIframe">
		<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box'); ?>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
		
		<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
		  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>
		
		<?php  echo form_open($this->config->item('admpath').'/carikategori'); ?>
			<span id="tombol" class="ui-widget-header ui-corner-all">
				<?php if(isset($txtcari)): ?>
				<a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/atur_kategori'); ?>" title="Kembali ke List" >Kembali ke List</a>
				<?php endif; ?>
				<button type="button" id="refresh" title="Refresh list" >Refresh</button>
			    <button type="submit" id="cari" title="Cari Metode">Cari Kategori</button>
				<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Label Kategori" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
			</span>
		<?php echo form_close(); ?>
		
		<button type="button" class="scrollup">Top</button>
		<div id="tabel" style="font-size:1.1em">
		  <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_kategori'); ?>
		</div>
		
		</div>
		<?php else: ?>
			<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
		<?php endif; ?>
		</div><!-- end #mainContent -->
		</div><!-- end #container -->
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
	<title>List Kategori Konten</title>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	<?php $this->load->view('dynamic_js'); ?>
	</head>

	<body class="i4nCMS">
		<div id="container" class="rounded" style="width:80%">

		<div id="mainContentIframe">
		<?php if ($super_admin && in_array(bs_kode($this->session->userdata('level'), TRUE) ,$hak)): ?>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box'); ?>
		<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>

		<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
		  <div class="pagination"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>

		<div class="btn-box">
			<span id="tombol" class="ui-widget-header ui-corner-all">
				<a id="install" href="<?php echo site_url($this->config->item('admpath').'/hbanner/tambah_hbanner'); ?>" title="Tambah Gambar Banner" >Tambah</a>
				<button title="Refresh" id="refresh">Refresh</button>
			</span>
		</div>

		<button type="button" class="scrollup">Top</button>
		<?php  echo form_open(''); ?>
		<?php echo form_close(); ?>
		<div id="tabel" style="font-size:1.1em">
		<?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_hbanner'); ?>
		</div>
		<?php else: ?>
			<?php $this->load->view('global_content/'. $this->config->item('admin_theme_id') . '/no_konten'); ?>
		<?php endif; ?>
		</div><!-- end #mainContent -->
		</div><!-- end #container -->
	</body>
</html>

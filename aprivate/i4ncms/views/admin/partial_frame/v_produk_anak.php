<!DOCTYPE html>
<html>
<head>
<title>List Produk Anak</title>
	<script>
	var baseURL = '<?php echo base_url(); ?>';
	</script>
	<?php $this->load->view('global_assets/admin_all_assets'); ?>
	<?php $this->load->view('dynamic_js'); ?>
</head>

<body class="i4nCMS">
	<div id="container" class="rounded">
	
	<div id="mainContentIframe">
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/msg_box'); ?>
	<?php $this->load->view($this->config->item('admin_theme_id').'/partial/flash_msg_box'); ?>
	
	<div style="font: 12px normal Helvetica, Arial, sans-serif; margin:20px auto;">
	  <div class="pagination" style="width:98%"><?php echo (!$page)?'<span class="page gradient">Data kurang dari '.$this->session->userdata('admperpage').' baris</span>':$page; ?></div>
	
	<?php if ($super_admin): ?>
		<?php  echo form_open($this->config->item('admpath').'/cariprod_anak'); ?>
		<div class="ui-helper-clearfix">
		<span id="tombol" class="ui-widget-header ui-corner-all">
		    <a id="install" href="<?php echo site_url($this->config->item('admpath').'/produk/tambah_anak/'.$this->uri->segment(4,$id_induk)); ?>" title="Tambah Produk" >Tambah</a>
		    <?php if(isset($txtcari)): ?>
		    <a id="kembali" href="<?php echo site_url($this->config->item('admpath').'/produk/detail_anak/'.$id_induk); ?>" title="Kembali ke List" >Kembali ke List</a>
		    <?php else: ?>
		    <button type="button" id="refresh" title="Refresh list" >Refresh</button>
		    <?php endif; ?>
		    <button type="submit" id="cari" title="Cari Produk" >Cari Produk</button>
			<input type="text" id="caritxt" name="caritxt" autocomplete="off" placeholder="Ketik Nama ID" value="<?php echo humanize((isset($txtcari))?$txtcari:'');?>" style="padding:2px">
			<input type="hidden" name="parent_id_prod" id="parent_id_prod" value="<?php echo set_value('parent_id_prod', $this->uri->segment(4,$id_induk)); ?>" />
		</span>
		</div>
		<?php echo form_close(); ?>
	<?php endif; ?>
	
	    <div id="tabel"> 
	      <?php $this->load->view($this->config->item('admin_theme_id').'/ajax/tabel_produk_anak'); ?>
	    </div>
	</div>
	
	</div><!-- end #mainContent -->
	</div><!-- end #container -->
</body>
</html>

<!-- #sidebar1 -->
<div id="sidebar1" class="rounded">
  <h3>Hallo, <?php echo $this->session->userdata('nama_admin'); ?></h3>
  <div style="text-align:right">
  <a class="atur" id="profil" href="<?php echo site_url($this->config->item('admpath').'/admincms/detail_profil/'.$this->session->userdata('idmu')); ?>" title="Update Profil Saya" >Profil</a>
  </div>
  <h3>Info aktifitas Anda.</h3>
  <ul style="margin-left:10px;margin-top:-10px">
  	<li><b>Alamat IP :</b> <?php echo $this->session->userdata('ip_address'); ?></li>
    <li><b>OS :</b> <?php echo $this->agent->platform(); ?></li>
    <li><b>Browser :</b> <br /><?php echo $this->agent->browser().' '.$this->agent->version(); ?></li>
    <li><b>Batas sesi akses :</b> <br /><?php echo date("d-m-Y h:i:s A", round($this->session->userdata('last_activity')+$this->config->item('sess_expiration'))); ?></li>
    <!-- <li><b>Sesi diupdate :</b> <br /><?php echo date("d-m-Y h:i:s A", round($this->session->userdata('last_activity'))); ?></li> -->
  </ul>
	<br />
  <h3>Info CMS.</h3>
   <ul style="margin-left:10px;margin-top:-10px">
    <li><b>Status : </b> <span style="color:#E42E2A;font-weight:bold"><?php echo (ENVIRONMENT == 'live')?$this->swb:strtoupper(ENVIRONMENT); ?></span></li>
    <li><b>Versi :</b> <?php echo $this->config->item('cms_version'); ?></li>
   </ul>
</div>
<!-- end #sidebar1 -->

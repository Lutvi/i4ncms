<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<?php 
	// Jqui
	if(!empty($js)):
		foreach($js as $item):
			if(file_exists(FCPATH.'assets/js/'.$item)): 
?>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/'.$item; ?>"></script>
<?php 
			endif;
		endforeach; 
	endif;

	// tema
	if(!empty($js_tema)):
	$js_tema = explode(',' , $js_tema);
		foreach($js_tema as $item):
			if(file_exists(FCPATH.'assets/'. $this->config->item('theme_id') .'/js/'.$item)): 
?>
	<script type="text/javascript" src="<?php echo base_url('assets/'. $this->config->item('theme_id') .'/js/'.$item); ?>"></script>
<?php 
			endif;
		endforeach; 
	endif;

	// Widget Js
	if(!empty($assets_widget)):
		foreach($assets_widget as $row): 
			$array = $row['js'];
			if(count($array) > 0): 
				$wg = explode(',' , $array);
				foreach($wg as $val):
					if(file_exists(FCPATH.'assets/js/widget/'.$row['nama_widget'].'/'.$val)):
?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/widget/'.$row['nama_widget'].'/'.$val); ?>"></script>
<?php 
					endif;
				endforeach;
			endif;
		endforeach;
	endif;
	
	// Module Js
	$mc = (!empty($tipe))?$tipe:'home';
	if( $mc == 'module' || $mc == 'home'):
	if( $mc == 'module' ):
		$assets_module = $this->cms->getModuleAsetsById($content);
	endif;
		if( !empty($assets_module)):
			foreach($assets_module as $row): 
			$array = $row['js'];
				if(count($array) > 0): 
				$md = explode(',' , $array);
					foreach($md as $val):
						if(file_exists(FCPATH.'assets/js/modul/'.$row['nama_module'].'/'.$val)):
?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/modul/'.$row['nama_module'].'/'.$val); ?>"></script>
<?php 
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
	endif;
	
	
	// fancybox plugin assets
	if( ! empty($tipe) && ($tipe === 'home' || $tipe === 'blog'|| $tipe === 'album'|| $tipe === 'video' || $tipe === 'produk' || $tipe === 'cari')):
		if(!empty($fancybox_js)):
		foreach($fancybox_js as $item):
			if(file_exists(FCPATH.'assets/js/plugins/fancybox/'.$item)):
?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/fancybox/'.$item); ?>"></script>
<?php
			endif;
		endforeach;
		endif;
		
		if(!empty($fancybox_css)):
		foreach($fancybox_css as $item):
			if(file_exists(FCPATH.'assets/js/plugins/fancybox/'.$item)):
?>
	<link href="<?php echo base_url('assets/js/plugins/fancybox/'.$item); ?>" rel="stylesheet" type="text/css" media="screen" />
<?php
			endif;
		endforeach;
		endif;
	endif;
	
	// produk plugin assets
	if(! empty($tipe) && $tipe === 'produk'):
		if(!empty($produk_js)):
		foreach($produk_js as $item):
			if(file_exists(FCPATH.'assets/js/plugins/produk/'.$item)):
?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/produk/'.$item); ?>"></script>
<?php
			endif;
		endforeach;
		endif;
		
		if(!empty($produk_css)):
		foreach($produk_css as $item):
			if(file_exists(FCPATH.'assets/js/plugins/produk/'.$item)):
?>
	<link href="<?php echo base_url('assets/js/plugins/produk/'.$item); ?>" rel="stylesheet" type="text/css" media="screen" />
<?php
			endif;
		endforeach;
		endif;
	endif;

	// IE Js
	if(!empty($js_ie)):
?>
<!--[if lte IE 8]>
<?php
		foreach($js_ie as $item):
			if(file_exists(FCPATH.'assets/js/'.$item)):
?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/'.$item); ?>"></script>
<?php 
			endif;
		endforeach;
?>
<![endif]-->
<?php
	endif; 
?>

<?php if($this->config->item('nilai_opsi_tema')): ?>
<style>
body {
<?php foreach($this->config->item('nilai_opsi_tema') as $key=>$val): ?>
<?php echo $key.':'.$val[0].';'."\n" ?>
<?php endforeach; ?>
}
</style>
<?php endif; ?>

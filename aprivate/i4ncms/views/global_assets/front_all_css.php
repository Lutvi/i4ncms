<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php 
    
    // global ui
	if(!empty($css)):
		foreach($css as $item):
			if(file_exists(FCPATH.'assets/css/'.$item)):
?>
    <link href="<?php echo base_url('assets/css/'.$item); ?>" rel="stylesheet" type="text/css" />
<?php 
			endif;
		endforeach;
	endif;

    // tema
    if(!empty($css_tema)):
    $css_tema = explode(',' , $css_tema);
		foreach($css_tema as $item):
			if(file_exists(FCPATH.'assets/'. $this->config->item('theme_id') .'/css/'.$item)):
?>
    <link href="<?php echo base_url('assets/'. $this->config->item('theme_id') .'/css/'.$item); ?>" rel="stylesheet" type="text/css" />
<?php 
			endif;
		endforeach;
	endif;

    // widget
	if(!empty($assets_widget)):
		foreach($assets_widget as $row): 
			$array = $row['css'];
			if(count($array) > 0): 
				$wg = explode(',' , $array);
				foreach($wg as $val):
					if(file_exists(FCPATH.'assets/css/widget/'.$row['nama_widget'].'/'.$val)):
?>
    <link href="<?php echo base_url('assets/css/widget/'.$row['nama_widget'].'/'.$val); ?>" rel="stylesheet" type="text/css" />
<?php 
					endif;
				endforeach;
			endif;
		endforeach;
	endif;
    
    // module
	$mc = (!empty($tipe))?$tipe:'home';
	if( $mc == 'module' || $mc == 'home'):
	if( $mc == 'module' ):
		$assets_module = $this->cms->getModuleAsetsById($content);
	endif;
		if( !empty($assets_module)):
			foreach((array)$assets_module as $row): 
				$array = $row['css'];
				if(count($array) > 0): 
					$md = explode(',' , $array);
					foreach($md as $val):
						if(file_exists(FCPATH.'assets/css/modul/'.$row['nama_module'].'/'.$val)):
?>
    <link href="<?php echo base_url('assets/css/modul/'.$row['nama_module'].'/'.$val); ?>" rel="stylesheet" type="text/css" />
<?php 
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
	endif;

	// IE - css
	if(!empty($css_ie)):
?>
<!--[if lte IE 8]>
<?php
		foreach($css_ie as $item):
			if(file_exists(FCPATH.'assets/css/'.$item)):
?>
    <link href="<?php echo base_url('assets/css/'.$item); ?>" rel="stylesheet" type="text/css" />
<?php 
			endif;
		endforeach;
?>
<![endif]-->
<?php
	endif;
?>



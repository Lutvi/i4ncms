<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php if( $this->config->item('google_analytics_code') != '') : ?>
	<?php if(isset($detail_order)) : ?>
	<!-- Ecommerce Analytics -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<?php echo $this->config->item('google_analytics_code'); ?>']);
	  _gaq.push(['_trackPageview']);
	  _gaq.push(['_set', 'currencyCode', 'IDR']);
	  _gaq.push(['_addTrans',
		'1234',           // transaction ID - required
		'<?php echo $this->config->item('site_name'); ?>',  // affiliation or store name
		'11.99',          // total - required
		'1.29',           // tax
		'5',              // shipping
		'San Jose',       // city
		'California',     // state or province
		'Indonesia'       // country
	  ]);

	   // add item might be called for every item in the shopping cart
	   // where your ecommerce engine loops through each item in the cart and
	   // prints out _addItem for each
	  _gaq.push(['_addItem',
		'1234',           // transaction ID - required
		'DD44',           // SKU/code - required
		'T-Shirt',        // product name
		'Green Medium',   // category or variation
		'11.99',          // unit price - required
		'1'               // quantity - required
	  ]);
	  _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<!-- End Ecommerce Analytics -->

	<?php else: ?>

	<!-- Pageview Analytics -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<?php echo $this->config->item('google_analytics_code'); ?>']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

	<!-- Kode analytics BArU
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', '<?php echo $this->config->item('google_analytics_code'); ?>', '<?php echo FURL; ?>');
	  ga('send', 'pageview');
	</script>
	-->
	<!-- End Pageview Analytics -->
	<?php endif; ?>
<?php endif; ?>

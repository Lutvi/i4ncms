<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php 
	if(!empty($css)):
	foreach($css as $item): 
?>
<link href="<?php echo base_url('assets/css/'.$item); ?>" rel="stylesheet" type="text/css" />
<?php 
	endforeach; 
	endif;

	if(!empty($css_ext)):
	foreach($css_ext as $item): 
?>
<link href="<?php echo base_url('assets/js/ext/'.$item); ?>" rel="stylesheet" type="text/css" />
<?php 
	endforeach; 
	endif; 

	  if($this->uri->segment(2) == 'atur_widget' && !empty($assets_widget)):
	  foreach($assets_widget as $row): 
	  $array = $row['css'];
	  if(count($array) > 0): 
	  $wg = explode(',' , $array);
	  foreach($wg as $val):
	  if(file_exists(FCPATH.'assets/css/widget/'.$row['nama_widget'].'/'.$val)):
?>
<link href="<?php echo base_url('assets/css/widget/'.$row['nama_widget'].'/'.$val); ?>" rel="stylesheet" type="text/css" />
<?php 
	  endif;
	  endforeach;
	  endif;
	  endforeach;
	  endif;

 
	  if($this->uri->segment(2) == 'atur_module' && !empty($assets_module)):
	  foreach($assets_module as $row): 
	  $array = $row['css'];
	  if(count($array) > 0): 
	  $md = explode(',' , $array);
	  foreach($md as $val):
	  if(file_exists(FCPATH.'assets/css/modul/'.$row['nama_module'].'/'.$val)):
?>
<link href="<?php echo base_url('assets/css/modul/'.$row['nama_module'].'/'.$val); ?>" rel="stylesheet" type="text/css" />
<?php 
	  endif;
	  endforeach;
	  endif;
	  endforeach;
	  endif;
?>

<?php 
	if(!empty($css_ie)):
?>
<!--[if IE]>
<?php
	foreach($css_ie as $item): 
?>
<link href="<?php echo base_url('assets/css/'.$item); ?>" rel="stylesheet" type="text/css" />
<?php 
	endforeach; 
?>
<![endif]-->
<?php	
	endif;


	if(!empty($js)):
	foreach($js as $item): 
?>
<script type="text/javascript" src="<?php echo base_url('assets/js/'.$item); ?>"></script>
<?php 
	endforeach; 
	endif;


	if(!empty($js_ie)):
?>
<!--[if lte IE 8]>
<?php
	foreach($js_ie as $item): 
?>
    <script type="text/javascript" src="<?php echo base_url('assets/js/'.$item); ?>"></script>
<?php 
	endforeach;
?>
<![endif]-->
<?php
	endif;


	  if($this->uri->segment(2) == 'atur_widget' && !empty($assets_widget)):
	  foreach($assets_widget as $row): 
	  $array = $row['js'];
	  if(count($array) > 0): 
	  $wg = explode(',' , $array);
	  foreach($wg as $val):
	  if(file_exists(FCPATH.'assets/js/widget/'.$row['nama_widget'].'/'.$val)):
?>
<script type="text/javascript" src="<?php echo base_url('assets/js/widget/'.$row['nama_widget'].'/'.$val); ?>"></script>
<?php 
	  endif;
	  endforeach;
	  endif;
	  endforeach;
	  endif;
?>
<?php 
	  if($this->uri->segment(2) == 'atur_module' && !empty($assets_module)):
	  foreach($assets_module as $row): 
	  $array = $row['js'];
	  if(count($array) > 0): 
	  $md = explode(',' , $array);
	  foreach($md as $val):
	  if(file_exists(FCPATH.'assets/js/modul/'.$row['nama_module'].'/'.$val)):
?>
<script type="text/javascript" src="<?php echo base_url('assets/js/modul/'.$row['nama_module'].'/'.$val); ?>"></script>
<?php 
	  endif;
	  endforeach;
	  endif;
	  endforeach;
	  endif;
?>

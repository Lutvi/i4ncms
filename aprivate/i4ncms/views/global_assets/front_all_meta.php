<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('./conf/metaverify.txt');
echo "\n";
$conf_meta = array(
					array('name' => 'author', 'content' => config_item('author_name')),
                    array('name' => 'application-name', 'content' => 'website ' . $this->config->item('site_name')),
                    array('name' => 'distribution', 'content' => config_item('site_distribution')),
                    array('name' => 'generator', 'content' => config_item('generator_site')),
                    array('name' => 'robots', 'content' => (isset($noindex))?'noindex':'index, follow')
                  );
	echo meta($conf_meta);

	// seo meta
	if(isset($seo))
	echo meta($seo);
	else
	echo meta($seo_default);

$esc_rich = config_item('esc_rich');
?>
<?php if(isset($tipe) && !in_array($tipe,$esc_rich)): ?>
	<!-- Microformat -->
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="<?php echo current_lang(false); ?>" />
	<meta property="og:title" content="<?php echo $this->config->item('site_name') . ' ' . (isset($title)?$title:''); ?>" />
	<meta property="og:description" content="<?php echo isset($mikro_dt_desc)?$mikro_dt_desc:$this->config->item('sort_site_description'); ?>" />
	<meta property="og:url" content="<?php echo current_url(); ?>" />
	<meta property="og:site_name" content="<?php echo $this->config->item('site_name'); ?>" />
	<meta itemprop="name" content="<?php echo $this->config->item('site_name'); ?>">
	<meta itemprop="description" content="<?php echo isset($mikro_dt_desc)?$mikro_dt_desc:$this->config->item('sort_site_description'); ?>">
	<meta itemprop="image" content="<?php echo base_url('assets/images/logo.png'); ?>">
<?php endif; ?>

<?php if(isset($tipe) && !in_array($tipe,array_merge($esc_rich,array('produk','cari','sitemap','halaman'))) && $this->uri->total_segments() <= 5): ?>
	<?php if(isset($per_page) && $per_page == 1):?>
	<link rel="base" href="<?php echo base_url(); ?>" />
	<link rel="canonical" href="<?php echo current_url(); ?>" />
<!-- Feed Konten -->
<?php if(isset($tipe) && $tipe != ''): ?>
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RSS 2.0 feed" href="<?php echo base_url(current_lang(false) .'/rss2'.$this->uri->slash_segment(2, 'leading').$this->uri->slash_segment(3, 'leading')); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> Atom feed" href="<?php echo base_url(current_lang(false) .'/atom'.$this->uri->slash_segment(2, 'leading').$this->uri->slash_segment(3, 'leading')); ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RDF/RSS 1.0 feed" href="<?php echo base_url(current_lang(false) .'/rdf'.$this->uri->slash_segment(2, 'leading').$this->uri->slash_segment(3, 'leading')); ?>" />
<?php endif; ?>
	<?php else: ?>
<!-- Feed Web -->
<?php if(isset($tipe) && $tipe != 'home' && $this->uri->total_segments() > 3): ?>
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RSS 2.0 feed" href="<?php echo base_url(current_lang(false) .'/rss2'.$this->uri->slash_segment(3, 'both').$this->uri->slash_segment(4).($this->uri->segment(5)?$this->uri->slash_segment(5):'').$this->uri->segment(2)); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> Atom feed" href="<?php echo base_url(current_lang(false) .'/atom'.$this->uri->slash_segment(3, 'both').$this->uri->slash_segment(4).($this->uri->segment(5)?$this->uri->slash_segment(5):'').$this->uri->segment(2)); ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RDF/RSS 1.0 feed" href="<?php echo base_url(current_lang(false) .'/rdf'.$this->uri->slash_segment(3, 'both').$this->uri->slash_segment(4).($this->uri->segment(5)?$this->uri->slash_segment(5):'').$this->uri->segment(2)); ?>" />
<?php elseif(isset($tipe) && $tipe != 'home' && $this->uri->total_segments() == 3 && $this->uri->segment(2) != 'posting'): ?>
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RSS 2.0 feed" href="<?php echo base_url(current_lang(false) .'/rss2/'.$this->uri->slash_segment(3).$this->uri->segment(2)); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> Atom feed" href="<?php echo base_url(current_lang(false) .'/atom/'.$this->uri->slash_segment(3).$this->uri->segment(2)); ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RDF/RSS 1.0 feed" href="<?php echo base_url(current_lang(false) .'/rdf/'.$this->uri->slash_segment(3).$this->uri->segment(2)); ?>" />
<?php else: ?>
<?php if($tipe == 'home'): ?>
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RSS 2.0 feed" href="<?php echo base_url(current_lang(false) .'/rss2'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> Atom feed" href="<?php echo base_url(current_lang(false) .'/atom'); ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $this->config->item('site_name') . ' ' . $title; ?> RDF/RSS 1.0 feed" href="<?php echo base_url(current_lang(false) .'/rdf'); ?>" />
<?php endif; ?>
<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>

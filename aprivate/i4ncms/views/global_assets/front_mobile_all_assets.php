
<?php if($tipe == 'module'):
    // Module css
    $assets_module = $this->cms->getModuleAsetsById($content);
    if( !empty($assets_module)):
        foreach((array)$assets_module as $row): 
            $array = $row['css'];
            if(count($array) > 0): 
                $md = explode(',' , $array);
                foreach($md as $val):
                    if(file_exists(FCPATH.'assets/css/modul/'.$row['nama_module'].'/'.$val)):
?>
    <link href="<?php echo base_url().'assets/css/modul/'.$row['nama_module'].'/'.$val; ?>" rel="stylesheet" type="text/css" />
<?php 
                    endif;
                endforeach;
            endif;
        endforeach;
    endif;

    // Module Js
    $assets_module = $this->cms->getModuleAsetsById($content);
    if( !empty($assets_module)):
        foreach($assets_module as $row): 
        $array = $row['js'];
            if(count($array) > 0): 
            $md = explode(',' , $array);
                foreach($md as $val):
                    if(file_exists(FCPATH.'assets/js/modul/'.$row['nama_module'].'/'.$val)):
?>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/modul/'.$row['nama_module'].'/'.$val; ?>"></script>
<?php 
                    endif;
                endforeach;
            endif;
        endforeach;
    endif;
	
endif;
?>

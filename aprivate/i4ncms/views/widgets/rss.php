<div class="sidebar rounded" style="overflow:hidden;">
	<h3 class="rounded"><?php echo $wid_title." ".$tipe_rss; ?></h3>
	<?php if($tipe_rss == 'internal'): ?>
	<?php foreach($rss_items as $item): ?>
			<h4><a href="<?php echo $item->get_permalink(); ?>" target="_blank" style="color:#0099FF"><?php echo $item->get_title(); ?></a></h4>
            <?php echo $item->get_description(); ?>
            <hr />
	<?php endforeach;?>
    <?php else: ?>
    <?php for($i=0;$i<8;$i++){ ?>
			<h4><a href="<?php echo $rss_items[$i]->get_permalink(); ?>" target="_blank" style="color:#0099FF"><?php echo $rss_items[$i]->get_title(); ?></a></h4>
            <?php echo $rss_items[$i]->get_description(); ?>
            <hr />
	<?php } ?>
    <?php endif; ?>
</div>

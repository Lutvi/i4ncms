<?php
//echo $tipe;
//print_r($content);

if ( empty($content) && empty($tipe) )
{
	// homepage -> run all active modules
	if ( count($modules) > 0 )
	{
		/*foreach($modules as $row)
		{
			$mp = $row['path_front_module'];
			echo modules::run($mp);
		}*/
	}
	else {
		$data['title'] = 'Homepage';
		$data['content'] = 'Ini blom dibuatin fungsi handlernya. klo modulnya gak ada yg aktif.';

		if ($this->config->item('slider_menu') === TRUE)
		$this->load->view_theme('polos', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
		else
		$this->load->view_theme('polos', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
	}
}
else {
	if ($tipe == 'home')
	{
		if (isset($data))
		{
			if ($this->config->item('slider_menu') === TRUE)
				$this->load->view_theme('vhome', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
				else
				$this->load->view_theme('vhome', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
		else {
			if ($this->config->item('slider_menu') === TRUE)
				$this->load->view_theme('vhome', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
				else
				$this->load->view_theme('vhome', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
	}
	elseif ($tipe == 'module')
	{
		$mp = $this->cms->getFrontModById($content);
		if ( count($mp) > 0 )
		{
			echo modules::run($mp);
		}
		else {
			$data['title'] = 'Halaman Kosong';
			$data['content'] = 'Data tidak ditemukan.';
			if (isset($data))
			{
				if ($this->config->item('slider_menu') === TRUE)
				$this->load->view_theme('polos', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
				else
				$this->load->view_theme('polos', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
			}
			else {
				if ($this->config->item('slider_menu') === TRUE)
				$this->load->view_theme('polos', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
				else
				$this->load->view_theme('polos', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
			}
		}
	}
	elseif ($tipe == 'pemesanan') {
		$data['title'] = $title;
		if(isset($isi))
		$isi = explode('/',$isi);

		if($content === 'pemesanan_produk')
		$this->load->view_theme('list_keranjang', $data, $this->config->item('theme_id').'/partial/order/');
		if($content === 'data_pemesanan' || $content === 'pembayaran_produk')
		$this->load->view_theme($isi[3], $data, $this->config->item('theme_id').'/'.$isi[1].'/'.$isi[2].'/');
	}
	elseif ($tipe == 'produk') {
		if (isset($data))
		{
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('produk', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('produk', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
		else {
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('produk', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('produk', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
	}
	elseif ($tipe == 'blog') {
		if (isset($data))
		{
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('blog', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('blog', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
		else {
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('blog', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('blog', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
	}
	elseif ($tipe == 'album') {
		if (isset($data))
		{
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('album', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('album', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
		else {
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('album', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('album', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
	}
	elseif ($tipe == 'video') {
		if (isset($data))
		{
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('video', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('video', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
		else {
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('video', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('video', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
	}
	else {
		if (isset($data))
		{
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('polos', $data, 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('polos', $data, 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
		else {
			if ($this->config->item('slider_menu') === TRUE)
			$this->load->view_theme('polos', '', 'mobile_'.$this->config->item('theme_id').'/main_default/partial/');
			else
			$this->load->view_theme('polos', '', 'mobile_'.$this->config->item('theme_id').'/main/partial/');
		}
	}
}


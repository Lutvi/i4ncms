<?php
//echo $tipe;
//print_r($content);

if ( empty($content) && empty($tipe) )
{
	// homepage -> run all active modules
	/*if ( count($modules) > 0 )
	{
		foreach($modules as $row)
		{
			$mp = $row['path_front_module'];
			echo modules::run($mp);
		}
	}
	else {
		$data['title'] = 'Homepage';
		$data['content'] = 'Ini blom dibuatin fungsi handlernya. klo modulnya gak ada yg aktif.';

		$this->load->view_theme('polos', $data, $this->config->item('theme_id').'/partial/');
	}*/

	$data['title'] = 'Homepage';
	$data['content'] = 'Ini blom dibuatin fungsi handlernya. klo modulnya gak ada yg aktif.';

	$this->load->view_theme('polos', $data, $this->config->item('theme_id').'/partial/');
}
else {
	if ($tipe == 'home')
	{
		if (isset($data))
		$this->load->view_theme('vhome', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('vhome', '', $this->config->item('theme_id').'/partial/');
	}
	elseif ($tipe == 'module')
	{
		$mp = $this->cms->getFrontModById($content);
		if ( count($mp) > 0 )
		{
			echo modules::run($mp);
		}
		else {
			$data['title'] = 'Halaman Kosong';
			$data['content'] = 'Data tidak ditemukan.';
			if (isset($data))
			$this->load->view_theme('polos', $data, $this->config->item('theme_id').'/partial/');
			else
			$this->load->view_theme('polos', '', $this->config->item('theme_id').'/partial/');
		}
	}
	elseif ($tipe == 'pemesanan') {
		$data['title'] = $title;
		if(isset($isi))
		$isi = explode('/',$isi);

		if($content === 'pemesanan_produk')
		$this->load->view_theme('list_keranjang', $data, $this->config->item('theme_id').'/partial/order/');
		if($content === 'data_pemesanan' || $content === 'pembayaran_produk')
		$this->load->view_theme($isi[3], $data, $this->config->item('theme_id').'/'.$isi[1].'/'.$isi[2].'/');
	}
	elseif ($tipe == 'produk') {
		if (isset($data))
		$this->load->view_theme('produk', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('produk', '', $this->config->item('theme_id').'/partial/');
	}
	elseif ($tipe == 'blog') {
		if (isset($data))
		$this->load->view_theme('blog', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('blog', '', $this->config->item('theme_id').'/partial/');
	}
	elseif ($tipe == 'album') {
		if (isset($data))
		$this->load->view_theme('album', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('album', '', $this->config->item('theme_id').'/partial/');
	}
	elseif ($tipe == 'video') {
		if (isset($data))
		$this->load->view_theme('video', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('video', '', $this->config->item('theme_id').'/partial/');
	}
	elseif ($tipe == 'cari') {
		if (isset($data))
		$this->load->view_theme('hasil_cari', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('hasil_cari', '', $this->config->item('theme_id').'/partial/');
	}
	elseif ($tipe == 'sitemap') {
		if (isset($data))
		$this->load->view_theme('sitemap', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('sitemap', '', $this->config->item('theme_id').'/partial/');
	}
	else {
		if (isset($data))
		$this->load->view_theme('polos', $data, $this->config->item('theme_id').'/partial/');
		else
		$this->load->view_theme('polos', '', $this->config->item('theme_id').'/partial/');
	}
}


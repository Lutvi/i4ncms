<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$allwid = $this->cms->getAllAktifWidget($this->config->item('max_widgets'),0);
if ($allwid != NULL)
{
   foreach ($allwid as $row)
   {
	  $wid_aktif = $row->nama_widget;
	  if ( ! empty(${$wid_aktif}) && ${$wid_aktif} != NULL && empty($row->html) )
	  {
		if ( file_exists( APPPATH . "widgets/" . $row->nama_widget . EXT ) )
			echo ${$wid_aktif};
		else
			continue;
	  }
	  else {
		// load template widget text_html
		if (current_lang(FALSE) === 'id')
		$jdl_widget = $row->judul_widget;
		else
		$jdl_widget = $row->judul_widget_en;

	  	$isi = array('wid_title' => $jdl_widget,'textHtml'  => $row->html);
		$this->load->view('widgets/text_html', $isi);
	  }
   }
}

<?php if( isset($css)): ?>
	<?php foreach($css as $item): ?>
		<link href="<?php echo base_url().'assets/css/'.$item; ?>" rel="stylesheet" type="text/css" />
	<?php endforeach; ?>
<?php endif; ?>

<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding:.7em;">
	<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	<strong>
	    Uppss..!
	</strong>
    <p>Tidak ada konten, hubungi Web Master <i><?php echo $this->config->item('webmaster_email'); ?></i> untuk dapatkan konten.</p>
</div>

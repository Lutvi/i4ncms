<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <title>Website dalam masa perbaikan</title>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>err_page/JamAnalog/css/sylvester.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>err_page/JamAnalog/css/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>err_page/JamAnalog/css/cssSandpaper.js"></script>	
    <script type="text/javascript" src="<?php echo base_url(); ?>err_page/JamAnalog/css/clock.js"></script>
    
    <link href="http://fonts.googleapis.com/css?family=Ubuntu&subset=latin" rel="stylesheet" type="text/css">
    <style type="text/css">
        *{font-family: 'Ubuntu', sans-serif;margin:0;padding:0}
        body {background:#ffffff;}
        #clock{position:relative;width:600px;height:600px;margin:20px auto 0 auto;background:url(<?php echo base_url(); ?>err_page/JamAnalog/images/clockface.jpg);list-style:none}
        #sec,#min,#hour{position:absolute;width:30px;height:600px;top:0px;left:285px}
        #sec{background:url(<?php echo base_url(); ?>err_page/JamAnalog/images/sechand.gif);z-index:3}
        #min{background:url(<?php echo base_url(); ?>err_page/JamAnalog/images/minhand.gif);z-index:2}
        #hour{background:url(<?php echo base_url(); ?>err_page/JamAnalog/images/hourhand.gif);z-index:1}
        #hook-just-in-case{background:#1E90FF !important; padding:10px; text-align:center !important;color:#E6E6FA !important;font:bold 16px Ubuntu, Georgia, Serif !important;position:relative;zoom:1;z-index:10000 !important;line-height:20px;}
        a{color:#ccf}
        a:hover{text-decoration:underline}
        #hook-just-in-case p{padding:0 10px;line-height:1.7em}
        
        /* Mobile Digital Clock */
        .clockStyle { text-align:center; font-size:45px; color:#276D7A; margin-top:20%;}
        #hook-just-in-case-mobile{background:#1E90FF !important; padding:10px; text-align:center !important;color:#E6E6FA !important;font:bold 12px Ubuntu, Georgia, Serif !important;position:relative;zoom:1;z-index:10000 !important;line-height:20px;}
    </style>
</head>

<body>
       
<?php if ( $this->mobiledetection->isMobile() || ENVIRONMENT === 'm-testing' ): ?>
<div id="hook-just-in-case-mobile">
    Mohon maaf, Website kami saat ini dalam masa perbaikan.<br />
    Silahkan cek beberapa saat lagi, Terima kasih.
</div> 
<div id="clockDisplay" class="clockStyle"></div>
<script type="text/javascript" language="javascript">
    function renderTime() {
        var currentTime = new Date();
        var diem = "AM";
        var h = currentTime.getHours();
        var m = currentTime.getMinutes();
        var s = currentTime.getSeconds();
        setTimeout('renderTime()',1000);
        if (h == 0) {
            h = 12;
        } else if (h > 12) { 
            h = h - 12;
            diem="PM";
        }
        if (h < 10) {
            h = "0" + h;
        }
        if (m < 10) {
            m = "0" + m;
        }
        if (s < 10) {
            s = "0" + s;
        }
        var myClock = document.getElementById('clockDisplay');
        myClock.textContent = h + ":" + m + ":" + s + " " + diem;
        myClock.innerText = h + ":" + m + ":" + s + " " + diem;
    }
renderTime();
</script>

<?php else: ?>
    <div id="hook-just-in-case">
        Mohon maaf, Website kami saat ini dalam masa perbaikan.<br />
        Silahkan cek beberapa saat lagi, Terima kasih.
    </div> 
    <ul id="clock">	
        <li id="sec" style="-webkit-transform: rotate(18deg);"></li>
        <li id="hour" style="-webkit-transform: rotate(491deg);"></li>
        <li id="min" style="-webkit-transform: rotate(132deg);"></li>
    </ul>
    
<?php endif; ?>
</body>
</html>

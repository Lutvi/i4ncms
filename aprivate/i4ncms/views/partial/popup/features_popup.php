<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Post</title>
<style type="text/css">
<!--
body {
	font: 80% Verdana, Arial, Helvetica, sans-serif;
	background: #666666;
	margin: 0; 
	padding: 0;
	text-align: center;
	color: #000000;
}
.partialRedDanio #container {
	width: 80%;  
	background: #FFFFFF;
	margin: 0 auto; 
	border: 1px solid #000000;
	text-align: left;
}
.partialRedDanio #mainContent {
	padding: 0 20px;
}
pre { display:none; }
.error { font-size:10px; color:#FF3333; }
-->
</style></head>

<body class="partialRedDanio">
<div id="container">
  <div id="mainContent">
  <?php if(isset($tipe) && $tipe == 'kirim_email'): ?>
<?php
	$attributes = array('class' => 'cmxform','name' => 'form1', 'id' => 'form1');
	echo form_open('features/send_post', $attributes);
?>
    <table width="350" border="0" align="center">
      <tr>
        <td width="200">Nama Pengirim</td>
        <td width="150">
          <input name="post_id" type="hidden" value="<?php echo $post_id; ?>" />
          <input type="text" name="e_nama_pengirim" id="e_nama_pengirim" value="<?php echo set_value('e_nama_pengirim'); ?>" />
          <?php echo form_error('e_nama_pengirim'); ?>        
       </td>
      </tr>
      <tr>
        <td>Email Pegirim</td>
        <td>
			<input type="text" name="e_pengirim" id="e_pengirim" value="<?php echo set_value('e_pengirim'); ?>" />
			<?php echo form_error('e_pengirim'); ?> 
        </td>
      </tr>
      <tr>
        <td>Email Penerima</td>
        <td>
			<input type="text" name="e_penerima" id="e_penerima" />
			<?php echo form_error('e_penerima'); ?> 
        </td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">
          <input type="submit" name="kirim_email" id="kirim_email" value="Kirim" />&nbsp;&nbsp;<input type="button" value=" Batal " onclick="window.close();return false;" />
        </div></td>
        </tr>
    </table>
    </form>
  <?php elseif(isset($tipe) && $tipe == 'sukses_kirim_email'): ?>
  		<div style="background-color:#1C9C1C; min-width:200px; min-height:49px; border:1px solid #0033FF; text-align:center; padding:25px 5px; margin:auto;">
            Info : <?php echo $info; ?>
            <hr /><form><input type="button" value=" Tutup " onclick="window.close();return false;" /></form>
        </div>
  <?php elseif(isset($tipe) && $tipe == 'gagal_kirim_email'): ?>
  		<div style="background-color:#FF3333; min-width:200px; min-height:55px; border:1px solid #0033FF; text-align:center; padding:0 5px; margin:auto;">
            <?php echo $info; ?>
            <hr /><form><input type="button" value=" Tutup " onclick="window.close();return false;" /></form>
        </div>
  <?php else: ?>
		<?php if($konten == TRUE): ?>
        <div style="position:fixed; top:60px; left:0; background-color:#66FFFF; width:65px; height:49px; border:1px solid #0033FF; text-align:center; padding:5px;">
            <form><input type="button" id="print_post" value=" Cetak " onclick="window.print();return false;" /><br /><input type="button" value=" Batal " onclick="window.close();return false;" /></form>
        </div>
        <h1><?php echo $judul; ?></h1>
        <?php echo $isi; ?>
        <hr />
        <small>
        Author : <?php echo $author; ?><br />
        Keyword : <?php echo $keyword; ?><br />
        Created Date :  <?php echo $tgl_buat; ?><br />
        Printed Date :  <?php echo $tgl_cetak; ?><br />
        </small>
        <?php else: ?>
        <h1>Konten Kosong.!!</h1>
        <?php endif; ?>
    <?php endif; ?>
  </div>
</div>
</body>
</html>

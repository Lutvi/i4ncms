<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*===================== PENGATURAN WEBSITE ================================
|--------------------------------------------------------------------------
| Site Name / Nama Website
|--------------------------------------------------------------------------
|
*/
$config['site_name'] = 'i4n-CMS';
$config['sort_site_description'] = 'My Great Site by i4n CMS';
/* Key API Meta Verify */
$config['google_analytics_code'] = '';
$config['microsoft_key'] = '';
$config['slurp_key'] = '';
/* abaikan ip crawler untuk di trace oleh sistem log */
$config['crawler'] = array('66.249.66.80');
/* Social Media */
$config['sosmed_url'] = ''; //serialize


/*
|--------------------------------------------------------------------------
| Pengurus Website
| Rubah nama dan email sesuai dengan devisi/staff yang ada
|--------------------------------------------------------------------------
*/
/* Webmaster untuk situs ini */
$config['webmaster_name'] = 'webmaster';
$config['webmaster_email'] = 'proweb21@gmail.com';

/* Pemilik situs ini */
$config['owner_name'] = 'owner';
$config['owner_email'] = 'proweb21@gmail.com';
$config['owner_info'] = FALSE; // TRUE/FALSE

/* Email sistem info/notifikasi situs ini */
$config['info_name'] = 'Info';
$config['info_email'] = 'support@proweb21.com';

/*
|--------------------------------------------------------------------------
| Keamanan dan performa Website
| path admin / SSL|HTTPS / cache frontend / iklan
|--------------------------------------------------------------------------
|
*/
$config['admpath'] = 'adminzx'; //Baca Dokumentasi untuk merubah path admin
$config['pake_ssl'] = FALSE; // TRUE|FALSE
$config['cache_front'] = FALSE; // TRUE|FALSE
$config['lama_cache_front'] = 7200; // per detik 7200 = 1jam
$config['iklan'] = TRUE; // TRUE|FALSE


/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language'] = "indonesia";  // indonesia|english
//default language abbreviation
$config['language_abbr'] = "id";  // id|en
//set available language abbreviations
$config['lang_uri_abbr'] = array("en" => "english", "id" => "indonesia");
//ignore this language abbreviation
$config['lang_ignore'] = "";
//language descriptions
$config['lang_desc']=array("en" => "English version", "id"=>"Versi Indonesia");
//language use images
$config['lang_useimg'] = TRUE; //or false to use only text


/*
|--------------------------------------------------------------------------
| Global Keyword & Description
|--------------------------------------------------------------------------
|
*/
$config['site_keyword_id'] = 'reddaniocms, modul, web, blog, widget, galleri, video, toko online, html5, responsive';
$config['site_description_id'] = 'CMS Reddanio sangat fleksibel untuk semua jenis website cms ini sangat mudah untuk di atur dan sudah terintegrasi degan fitur galleri foto dan video serta blog, toko online, widget dan modul. standart template yang digunakan adalah html5 dengan fitur responsif, dapatkan sekarang juga dan dapatkan template eksklusif dan widget';
$config['site_keyword_en'] = 'cms, reddanio, reddaniocms, modules, widgets, websites, blog, gallery, ecommerce, exclusive, templates, html5, responsive designs';
$config['site_description_en'] = 'RedDanio CMS is powerfull and flexible contents for all website categories this cms is easy to manage and also included images and videos gallery, blogs, ecommerce, widgets or modules. standart template is html5 with responsive designs, get it now for  free 2years hosting + 5 exclusive templates and more widgets.';

$config['sosmed_url'] = array (
  'twitter' => 'https://twitter.com',
  'facebook' => 'https://facebook.com',
  'gplus' => 'https://gplus.com',
);

/*
|--------------------------------------------------------------------------
| Status Website
|--------------------------------------------------------------------------
* Opsi(integer[1-3])
* 1 => Offline
* 2 => Perbaikan
* 3 => Online
*/
$config['sts_web'] = 3;


/*
|--------------------------------------------------------------------------
| Copyright / HakCipta
|--------------------------------------------------------------------------
*/
$config['copy_txt'] = '&copy; 2014 - Powered By i4n-CMS';


/*
|--------------------------------------------------------------------------
| Website Theme / Tema website
|--------------------------------------------------------------------------
*/
$config['theme_id'] = 'theme01';
$config['is_respon'] = FALSE; // TRUE|FALSE
$config['slider_menu'] = TRUE; // TRUE|FALSE
/* nama halaman statis yang sudah dipakai oleh sistem */
$config['reserved_halaman'] = array('update','pemesanan','pembayaran','cek_pesanan','en','id','halaman','pages','home','beranda','sitemap','cari','search','rss','atom','rdf');
//Ignore rich content
$config['esc_rich'] = array('pemesanan','module');
//Susunan Sitemap
//false = link +detail item konten | true = link menu halaman saja
$config['sitemap_menu'] = true;

/*
|--------------------------------------------------------------------------
| Admin Theme / Tema Admin
|--------------------------------------------------------------------------
*/
$config['admin_theme_id'] = 'admin';

/*
|--------------------------------------------------------------------------
| WYSIWYG Editor Basepath Folder / Editor Path
|--------------------------------------------------------------------------
*/
$config['fckeditor_basepath'] = '_plugins/WYSIWYG/fckeditor/';
$config['ckeditor_basepath'] = '_plugins/WYSIWYG/ckeditor';

/*
|--------------------------------------------------------------------------
| Manual install widgets/modules
| TRUE / FALSE
|--------------------------------------------------------------------------
*/
$config['manual_install'] = FALSE;

/*
|--------------------------------------------------------------------------
| Max widget yang muncul di frontpage
|--------------------------------------------------------------------------
*/
$config['max_widgets'] = 10;

/*
|--------------------------------------------------------------------------
| Max tags untuk masing-masing konten
|--------------------------------------------------------------------------
*/
$config['max_tag'] = 11;

/*
|--------------------------------------------------------------------------
| Max Kategori untuk masing-masing halaman
|--------------------------------------------------------------------------
*/
$config['max_kategori'] = 25;

/* 
| Max kapasitas file temporary untuk installer per bytes /=> 1KB = 1000 bytes 
| default = 2500000 bytes / 2,5 MB.
*/
$config['max_tmp_install'] = 2500000;


/*============================ BATAS PRIVATE SETING =======================
|--------------------------------------------------------------------------
| KEAMANAN - Key/Serial for this website = Serial hanya ada 1 di dalam jaringan.
|--------------------------------------------------------------------------
| SANGAT PENTING..!!!
| Anda harus memiliki otoritas dari web master, sebelum merubah nomor serial ini.
| Nomor serial ini sudah terintegrasi dengan sistem yang berjalan, jadi apabila
| Anda mengganti dengan sembarangan maka kemungkinan sistem Anda tidak berjalan Normal
| atau bahkan lebih fatal dari kemungkinan tersebut
| dan website tidak dapat terkoneksi dengan jaringan untuk melakukan upgrade/install.!
| duplikat untuk Nomor serial Anda => [x-JANGAN DIHAPUS-x]-- 338#17-AQCX/P85BIG --[x-JANGAN DIHAPUS-x]
*/
$config['pkey'] = '338#17-AQCX/P85BIG';
$config['max_pr'] = 5; //Max attemp invalid transmit data
$config['super_aktif'] = TRUE; //TRUE | FALSE
$config['metav_edit'] = TRUE; //Edit Meta Verify TRUE/FALSE

/*
|--------------------------------------------------------------------------
| Web Master / email@(web master)
|--------------------------------------------------------------------------
| JANGAN MERUBAH APAPUN PADA FILE INI JIKA MASIH INGIN MENGGUNAKAN RUANG LINGKUP 
| DARI KINERJA SISTEM CMS.
| Catatan :
| Sebaiknya hubungi dulu webmaster atau pembuat sebelum/ingin merubah isi file ini.
*/
/* Lisensi Software CMS ==================================================
 * JANGAN DIRUBAH..
 */
$config['cms_version'] = '1.0.0';
$config['generator_site'] = 'i4n-CMS-V.2.0';
$config['author_name'] = 'inp';
$config['author_email'] = 'fun9uy5@gmail.com';

/*
|--------------------------------------------------------------------------
| Distro / Sofware Released Version
|--------------------------------------------------------------------------
| opt : i4n-V.xx
*/
$config['site_distribution'] = 'i4n-V.2.0';

/*==========================================================================
 *--- EOL Lisensi Software CMS ---*/



/* ==== EXTRA AKSES hanya berjalan pada mode development/testing ==
|--------------------------------------------------------------------------
| FTP Access / Akun FTP
|--------------------------------------------------------------------------
| CMS Login ke FTP..!!
| Mohon tidak dirubah.
*/
$config['ftp_host_cms'] = '127.0.0.1';
$config['ftp_user_cms'] = 'inp';
$config['ftp_pass_cms'] = '123456';

/*// Testing
$config['ftp_host_cms'] = '127.0.0.1';
$config['ftp_user_cms'] = 'testing';
$config['ftp_pass_cms'] = '';*/

/* End of file site.php */
/* Location: ./application/config/site.php */

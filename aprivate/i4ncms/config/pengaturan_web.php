<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['sts_web'] = 3;
$config['site_name'] = 'Demo i4n CMS';
$config['pake_ssl'] = false;
$config['cache_front'] = true;
$config['sort_site_description'] = 'Multi bahasa 4 jenis konten dalam 1 CMS';
$config['super_aktif'] = false;
$config['iklan'] = true;
$config['toko_aktif'] = true;
$config['cek_stok_front'] = true;
$config['satuan_jenis_produk'] = 'item';
$config['satuan_produk'] = 'unit';
$config['cs_name'] = 'CS - Care';
$config['cs_email'] = 'cs@example.com';
$config['cs_telpon'] = '021-9872453';
$config['cs_sms'] = '089878889009';
$config['ppn'] = '0.10';
$config['site_keyword_id'] = 'i4ncms, i4n, cms, modul, web, blog, widget, galleri, video, toko online, html5, responsive';
$config['site_description_id'] = 'i4n CMS sangat fleksibel untuk semua jenis website cms ini sangat mudah untuk di atur dan mendukung 4 jenis konten yaitu galleri foto, video, blog dan toko online selain itu juga mendukung penambahan instalasi tema, widget dan modul seperti cms populer lainnya.';
$config['site_keyword_en'] = 'i4ncms, i4n, cms, modules, widgets, websites, blog, gallery, ecommerce, exclusive, templates, html5, responsive designs';
$config['site_description_en'] = 'i4n CMS is powerfull and flexible contents for all website categories this cms is easy to manage and support 4 types of contents such as images, videos, blogs, ecommerce and you can install more template, widgets and modules like others popular cms.';
$config['sosmed_url'] = array (
  'twitter' => 'https://twitter.com',
  'facebook' => 'https://facebook.com',
  'gplus' => 'https://gplus.com',
);

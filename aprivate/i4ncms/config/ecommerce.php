<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Web Master / email@(web master)
|--------------------------------------------------------------------------
| Catatan :
| Pehatikan opsi tipe datanya sebelum/ingin merubah isi file ini.
*/

/* Status untuk Toko Online
* Opsi : Boolean
* TRUE => fitur Toko Online ditampilkan , FALSE => fitur Toko Online tidak ditampilkan
*/
$config['toko_aktif'] = TRUE;
$config['ppn'] = abs(0.10); // desimal * 100 = n%

/* [:: HARUS DI SET SEBELUM DATA PRODUK DI INPUT ::]
 * Konfigurasi tampilkan biaya ongkir pada saat pelanggan lakukan order
 * Opsi : Boolean
 * TRUE =>  biaya ongkir ditampilkan pada saat order dan harga produk tidak terpengaruh atau ditambahkan dengan biaya ongkir tertinggi
 * FALSE => biaya ongkir tidak ditampilkan dan harga produk akan ditambahkan dengan biaya ongkir tertinggi,
 * 		    jika ongkir < biaya ongkir tertinggi maka pelanggan akan mendapatkan tampilan diskon transaksi pada saat order
 * 		    dengan perhitungan [harga_produk] - (ongkir tertinggi - ongkir tujuan)
 */
$config['tampilkan_ongkir'] = TRUE;

/*
|--------------------------------------------------------------------------
| Cek stok frontend - cek stok produk untuk ditampilkan di frontend
|--------------------------------------------------------------------------
* Opsi = Boolean
* cek_stok_front => TRUE : produk tidak ditampilan jika stok = 0 / FALSE : ditampilkan meskipun stok = 0
*/
$config['cek_stok_front'] = TRUE;

/*
|--------------------------------------------------------------------------
| Nama satuan untuk produk produk
|--------------------------------------------------------------------------
* Opsi = String
* nama_satuan => txt(di sesuaikan dengan produk yang dijual)
*/
$config['satuan_jenis_produk'] = 'item';
$config['satuan_produk'] = 'unit';


/* Detail cs situs ini */
$config['cs_name'] = 'CS';
$config['cs_email'] = 'cs@example.com';
$config['cs_telpon'] = '021-9872453';
$config['cs_sms'] = '089878889009';

/* Max Pilihan Wilayah Layanan
 * => metode pembayaran
 * => kurir pengiriman
 *  */
$config['max_wilayah'] = 100;


/*
|--------------------------------------------------------------------------
| List Dikon produk
|--------------------------------------------------------------------------
* Opsi = Array()
* desimal => txt
* contoh:
* 0.05 => 5%
*/
$config['list_diskon_prod'] = array(
								"0.05" => "5%",
								"0.10" => "10%",
								"0.15" => "15%",
								"0.16" => "16%",
								"0.17" => "17%",
								"0.18" => "18%",
								"0.19" => "19%",
								"0.20" => "20%",
								"0.21" => "21%",
								"0.22" => "22%",
								"0.23" => "23%",
								"0.24" => "24%",
								"0.25" => "25%",
								"0.30" => "30%",
								"0.35" => "35%",
								"0.40" => "30%",
								"0.45" => "45%",
								"0.50" => "50%",
								"0.55" => "55%",
								"0.60" => "60%",
								"0.65" => "65%",
								"0.70" => "70%",
								"0.75" => "75%",
								"0.80" => "80%",
								"0.85" => "85%"
							);

/*
|--------------------------------------------------------------------------
| List Jenis produk
|--------------------------------------------------------------------------
* Opsi = Array()
* isi(max 20-karakter)lowercase => label
* contoh:
* warna => Warna
*/
$config['list_jenis_prod'] = array(
								"warna" => "Warna",
								"model" => "Model",
								"ukuran" => "Ukuran",
								"tipe" => "Tipe"
							);

/*
|--------------------------------------------------------------------------
| Format alamat kirim
|--------------------------------------------------------------------------
* Opsi = String
* note:
* "\n"(tanpa spasi) == buat baris baru tanpa spasi.
* default = "Jl. _____________ Rt. ___ Rw. __\nNo. __\n(nama_kelurahan-kecamatan)";
*/
$config['format_alamat'] = "Jl. _____________ Rt. ___ Rw. __\nNo. __\n(nama_kelurahan-kecamatan)";

/*
|--------------------------------------------------------------------------
| Metode untuk pembayaran Hardcoded
|--------------------------------------------------------------------------
* Opsi = Multi Array()
* isi(max 20-karakter)lowercase => 
*                                  label(fixed tdk bisa diganti)lowercase => Nama Bebas
*                                  opsi => 
*                                          Nama Bebas => foto logo sesuaikan dgn yg ada
* note : logo yg tersedia lokasinya di (assets/images/logo-bayar), boleh ditambah/diganti tp jangan di rename.!
*   ukuran logo 160x160
*   GIRO = pos-indonesia.png, wu.png
*   BANK = bni.png, bri.png, bca.png, mandiri.gif
*   PaymentGateway = paypall.png
*   ATM = atm-bersama.png
* contoh:
* transfer => 
*             label =>  Transfer Bank
*             opsi  =>  
*                       Bank 1 => logo.jpg
*                       Bank 2 => logo.jpg
*/
/*
$config['metode_bayar'] = array(
								"giro" =>       array( "label" => "Pos Giro",
														"opsi" => array(
																	"Western Union" => "wu.png",
																	"Pos Indonesia" => "pos-indonesia.png"
																	)
												),
								"transfer" =>   array( "label" => "Transfer Bank",
														"opsi" => array(
																	"Bank BNI" => "bni.png",
																	"Bank BRI" => "bri.png",
																	"Bank BCA" => "bca.png"
																	)
												),
								"online" =>     array( "label" => "Online",
														"opsi" => array(
																	"PayPall" => "paypall.png",
																	"Veritrans" => "veritrans.png"
																	)
												),
								"cod" =>     array( "label" => "COD",
														"opsi" => array(
																	"COD" => "cod.png"
																	)
												)
							);

$config['giro_alamat'] = array(
								"Western Union" => "Kepada : PT.Testing <br>\n Jl.Kebangsaan IV No.29A<br>\n  Rt.008 Rw.09<br>\n Kebayoran Baru Jakarta Selatan<br>\n Jakarta 21567",
								"Pos Indonesia" => "Kepada : PT.Testing <br>\n Jl.Kebangsaan IV No.29A<br>\n  Rt.008 Rw.09<br>\n Kebayoran Baru Jakarta Selatan<br>\n Jakarta 21567"
							);


$config['rek_bank'] = array(
								"Bank BNI" => "A/N : Testing <br>\n No.Rek : 1111.1122.12 <br>\n Kantor Cabang : Jakarta",
								"Bank BRI" => "A/N : Testing <br>\n No.Rek : 2323.3333.443 <br>\n Kantor Cabang : Jakarta",
								"Bank BCA" => "A/N : Testing <br>\n No.Rek : 3434.343.434 <br>\n Kantor Cabang : Jakarta"
							);

$config['online_id'] = array(
								"PayPall" => "Kepada : PT.Testing <br>\n Jl.Kebangsaan IV No.29A<br>\n  Rt.008 Rw.09<br>\n Kebayoran Baru Jakarta Selatan<br>\n Jakarta 21567",
								"Veritrans" => "Kepada : PT.Testing <br>\n Jl.Kebangsaan IV No.29A<br>\n  Rt.008 Rw.09<br>\n Kebayoran Baru Jakarta Selatan<br>\n Jakarta 21567"
							);

$config['kurir_cod'] = array(
								"COD" => "No.Pegawai : 123456<br>\nNama : Ariyanto<br>\nJabatan : Pengantar Paket COD<br>\nNo.Telpon : 08999989999"
							);
*/

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|   example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|   http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|   $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
    |$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "frontpage/home";
$route['404_override'] = 'blank_page';

/*
 * Front No URI LANG
 * ---------------------------------------------------------------------------*/
$route['frontpage/home/index'] = "";
$route['home'] = $route['default_controller'];
$route['home/:num'] = $route['default_controller'];
$route['logout'] = $route['default_controller']."/logout";

// halaman ID
$route['halaman'] = $route['default_controller']."/halaman";
$route['halaman/(.*)'] = $route['default_controller']."/halaman";

// halaman EN
$route['pages'] = $route['default_controller']."/halaman";
$route['pages/(.*)'] = $route['default_controller']."/halaman";


/*
 * ecommerce 
 * ---------------------------------------------------------------------------*/
 /* AJAX  no URI LANG*/
$route['belanja'] = "frontpage/keranjang";
$route['belanja/beli'] = "frontpage/keranjang/tambah";
$route['belanja/update'] = "frontpage/keranjang/update";
$route['belanja/batal'] = "frontpage/keranjang/batal";
$route['belanja/proses_pesan'] = "frontpage/keranjang/form_pesan";
$route['belanja/list_keranjang'] = "frontpage/keranjang/list_keranjang";
$route['list_wilayah'] = "blank_page/list_wilayah";
$route['list_wilayah_admin'] = "blank_page_admin/list_wilayah";

/* Controllers no URI LANG*/
$route['produk/(.*)'] = $route['default_controller']."/halaman";

/* Form  no URI LANG**/
$route['pemesanan'] = $route['default_controller']."/halaman";
$route['pemesanan/(.*)'] = $route['default_controller']."/halaman";

/*
 * shared extra features no URI LANG
 * ---------------------------------------------------------------------------*/
// FEED
$route['rss'] = "mod_rss/mod_rss/index";
$route['atom'] = "mod_rss/mod_rss/atom";
$route['rdf'] = "mod_rss/mod_rss/rdf";

$route['features'] = "shared/post_features";
$route['post/:num'] = "shared/post_features/post_item";
$route['extra/:num/generate_pdf'] = "shared/post_features/generate_pdf";
$route['features/form_send_post/:num'] = "shared/post_features/form_send_post";
$route['features/send_post'] = "shared/post_features/send_post";
$route['features/print_post/:num'] = "shared/post_features/print_post";

/*===========================================END no URI LANG ====================*/
$route['error'] = $route['default_controller']."/sistem_error";
/*
 * error page
 * ---------------------------------------------------------------------------*/

 /*===========================================END no URI LANG ====================*/


#$route['^(\w{2})/(.*)$'] = '$2';
$route['^(\w{2})$'] = $route['default_controller'];
/*
 * Front URI LANG
 * ---------------------------------------------------------------------------*/
$route['frontpage/home/index'] = "";
$route['^(\w{2})/home'] = $route['default_controller'];
$route['^(\w{2})/home/:num'] = $route['default_controller'];
$route['^(\w{2})/logout'] = $route['default_controller']."/logout";
$route['^(\w{2})/keluar'] = $route['default_controller']."/logout";
//=== pencarian
$route['^(\w{2})/cari'] = $route['default_controller']."/cari";
$route['^(\w{2})/search'] = $route['default_controller']."/cari";

//=== sitemap
$route['^(\w{2})/sitemap'] = $route['default_controller']."/sitemap";
$route['^(\w{2})/sitemapxml'] = $route['default_controller']."/sitemap";

//=== halaman ID
$route['^(\w{2})/halaman'] = $route['default_controller']."/halaman";
$route['^(\w{2})/halaman/(.*)'] = $route['default_controller']."/halaman";
//===============
//=== halaman EN
$route['^(\w{2})/pages'] = $route['default_controller']."/halaman";
$route['^(\w{2})/pages/(.*)'] = $route['default_controller']."/halaman";

//=== Kategori
// ID
$route['^(\w{2})/kategori'] = $route['default_controller']."/kategori";
$route['^(\w{2})/kategori/(.*)'] = $route['default_controller']."/kategori";
// EN
$route['^(\w{2})/category'] = $route['default_controller']."/kategori";
$route['^(\w{2})/category/(.*)'] = $route['default_controller']."/kategori";

//=== Tag
// ID
$route['^(\w{2})/tag'] = $route['default_controller']."/tag";
$route['^(\w{2})/tag/(.*)'] = $route['default_controller']."/tag";
// EN
//$route['^(\w{2})/tags'] = $route['default_controller']."/tag";
//$route['^(\w{2})/tags/(.*)'] = $route['default_controller']."/tag";

//=== Detail Produk ID/EN
$route['^(\w{2})/pdetail'] = "frontpage/pdetail";
$route['^(\w{2})/pdetail/:any'] = "frontpage/pdetail";
//=== Blog ID/EN
$route['^(\w{2})/posting'] = "frontpage/posting";
$route['^(\w{2})/posting/:any'] = "frontpage/posting";
//=== Album ID/EN
$route['^(\w{2})/gallery'] = "frontpage/gallery";
$route['^(\w{2})/gallery/:any'] = "frontpage/gallery";
//=== Video ID/EN
$route['^(\w{2})/playlist'] = "frontpage/playlist";
$route['^(\w{2})/playlist/:any'] = "frontpage/playlist";

/*
 * Content Media
 * --------------------------------------------------------------------------*/
/* Blog URI LANG */
$route['^(\w{2})/blog/(.*)'] = $route['default_controller']."/halaman";
/* Album URI LANG */
$route['^(\w{2})/(album|albums)/(.*)'] = $route['default_controller']."/halaman";
/* Video URI LANG */
$route['^(\w{2})/(video|videos)/(.*)'] = $route['default_controller']."/halaman";


/*
 * ecommerce URI LANG
 * ---------------------------------------------------------------------------*/
 /* AJAX  URI LANG*/
$route['^(\w{2})/belanja'] = "frontpage/keranjang";
$route['^(\w{2})/belanja/beli'] = "frontpage/keranjang/tambah";
$route['^(\w{2})/belanja/update'] = "frontpage/keranjang/update";
$route['^(\w{2})/belanja/batal'] = "frontpage/keranjang/batal";
$route['^(\w{2})/belanja/proses_pesan'] = "frontpage/keranjang/form_pesan";
$route['^(\w{2})/belanja/list_keranjang'] = "frontpage/keranjang/list_keranjang";
$route['^(\w{2})/list_wilayah'] = "frontpage/keranjang/list_wilayah";
$route['^(\w{2})/list_wilayah_admin'] = "blank_page_admin/list_wilayah";

/* Controllers URI LANG ID*/
$route['^(\w{2})/(produk|product)/(.*)'] = $route['default_controller']."/halaman";

/* Form URI LANG**/
$route['^(\w{2})/pemesanan'] = $route['default_controller']."/halaman";
$route['^(\w{2})/pemesanan/(.*)'] = $route['default_controller']."/halaman";

/*
 * shared extra features URI LANG
 * ---------------------------------------------------------------------------*/
// FEED
$route['^(\w{2})/rss2(.*)'] = "mod_rss/mod_rss/index";
$route['^(\w{2})/atom(.*)'] = "mod_rss/mod_rss/atom";
$route['^(\w{2})/rdf(.*)'] = "mod_rss/mod_rss/rdf";

$route['^(\w{2})/features'] = "shared/post_features";
$route['^(\w{2})/post/:num'] = "shared/post_features/post_item";
$route['^(\w{2})/extra/:num/generate_pdf'] = "shared/post_features/generate_pdf";
$route['^(\w{2})/features/form_send_post/:num'] = "shared/post_features/form_send_post";
$route['^(\w{2})/features/send_post'] = "shared/post_features/send_post";
$route['^(\w{2})/features/print_post/:num'] = "shared/post_features/print_post";

// REST-api
$route['^(\w{2})/api/example/posts(.*)'] = "api/example/posts";

/*===========================================END URI LANG ====================*/


$admpath = 'adminzx';
/* 
 * Admin 
 * ---------------------------------------------------------------------------*/
$route[$admpath.'/login'] = $admpath."/login";

$route[$admpath] = $admpath."/dashboard";
$route[$admpath.'/:num'] = $admpath."/dashboard";
$route[$admpath.'/atur_widget/:num'] = $admpath."/atur_widget";
$route[$admpath.'/atur_widget/update_table/:num'] = $admpath."/atur_widget/update_table";
$route[$admpath.'/atur_module/:num'] = $admpath."/atur_module";
$route[$admpath.'/atur_module/update_table/:num'] = $admpath."/atur_module/update_table";
$route[$admpath.'/atur_menu/:num'] = $admpath."/atur_menu";
$route[$admpath.'/atur_menu/update_table/:num'] = $admpath."/atur_menu/update_table";
$route[$admpath.'/halaman/:num'] = $admpath."/halaman";
$route[$admpath.'/halaman/update_table/:num'] = $admpath."/halaman/update_table";

/* blog */
$route[$admpath.'/blog/:num'] = $admpath."/blog";
$route[$admpath.'/blog/update_table/:num'] = $admpath."/blog/update_table";

/* album */
$route[$admpath.'/album/:num'] = $admpath."/album";
$route[$admpath.'/album/update_table/:num'] = $admpath."/album/update_table";
/* album-gallery */
$route[$admpath.'/album/tabel_gallery/(\d+)'] = $admpath."/album/tabel_gallery/$1";
$route[$admpath.'/album/tabel_gallery/(\d+)/:num'] = $admpath."/album/tabel_gallery/$1";
$route[$admpath.'/album/update_gallery'] = $admpath."/album/update_gallery";
$route[$admpath.'/album/update_tabel_gallery/(\d+)/:num'] = $admpath."/album/update_tabel_gallery/$1";

/* video */
$route[$admpath.'/video/:num'] = $admpath."/video";
$route[$admpath.'/video/update_table/:num'] = $admpath."/video/update_table";
/* video-playlist */
$route[$admpath.'/video/playlist/tabel_playlist/(\d+)'] = $admpath."/video/tabel_playlist/$1";
$route[$admpath.'/video/playlist/tabel_playlist/(\d+)/:num'] = $admpath."/video/tabel_playlist/$1";
$route[$admpath.'/video/playlist/update_playlist'] = $admpath."/video/update_playlist";
$route[$admpath.'/video/playlist/update_tabel_playlist/(\d+)/:num'] = $admpath."/video/update_tabel_playlist/$1";

/* produk */
$route[$admpath.'/produk/:num'] = $admpath."/produk";
$route[$admpath.'/produk/update_table/:num'] = $admpath."/produk/update_table";
$route[$admpath.'/produk/detail_anak/:num/:num'] = $admpath."/produk/detail_anak";
$route[$admpath.'/produk/detail_anak/detail_anak/:num/:any/table'] = $admpath."/produk/detail_anak";

/* order-proses */
$route[$admpath.'/order/proses_bayar/:num'] = $admpath."/order";
$route[$admpath.'/order/proses_pemaketan/:num'] = $admpath."/order";
$route[$admpath.'/order/proses_pengiriman/:num'] = $admpath."/order";
$route[$admpath.'/order/proses_pending/:num'] = $admpath."/order";
/* order-status */
$route[$admpath.'/order/sukses/:num'] = $admpath."/order";
$route[$admpath.'/order/batal/:num'] = $admpath."/order";
$route[$admpath.'/order/update_table/:num'] = $admpath."/order";

/* pelanggan */
$route[$admpath.'/pelanggan/:num'] = $admpath."/pelanggan";
$route[$admpath.'/pelanggan/update_table/:num'] = $admpath."/pelanggan";

/* banner-iklan */
$route[$admpath.'/banner/iklan_sidebar/:num'] = $admpath."/banner";
$route[$admpath.'/banner/iklan_utama/:num'] = $admpath."/banner";
$route[$admpath.'/banner/iklan_atas/:num'] = $admpath."/banner";
$route[$admpath.'/banner/iklan_bawah/:num'] = $admpath."/banner";
$route[$admpath.'/banner/update_table/:num'] = $admpath."/banner";

/* newsletter */
$route[$admpath.'/newsletter/:num'] = $admpath."/newsletter";
$route[$admpath.'/newsletter/update_table/:num'] = $admpath."/newsletter";

/* admin cms */
$route[$admpath.'/admincms/:num'] = $admpath."/admincms";
$route[$admpath.'/admincms/update_table/:num'] = $admpath."/admincms";

/* atur kategori */
$route[$admpath.'/atur_kategori/:num'] = $admpath."/atur_kategori";
$route[$admpath.'/atur_kategori/update_table/:num'] = $admpath."/atur_kategori";

/* hbanner */
$route[$admpath.'/hbanner/:num'] = $admpath."/hbanner";
$route[$admpath.'/hbanner/update_table/:num'] = $admpath."/hbanner";

/* metode-bayar */
$route[$admpath.'/atur_metode_bayar/bayar_transfer/:num'] = $admpath."/atur_metode_bayar";
$route[$admpath.'/atur_metode_bayar/bayar_online/:num'] = $admpath."/atur_metode_bayar";
$route[$admpath.'/atur_metode_bayar/bayar_giro/:num'] = $admpath."/atur_metode_bayar";
$route[$admpath.'/atur_metode_bayar/bayar_cod/:num'] = $admpath."/atur_metode_bayar";
$route[$admpath.'/atur_metode_bayar/update_table/:num'] = $admpath."/atur_metode_bayar";

/* kurir pengiriman */
$route[$admpath.'/atur_kurir_pengiriman/express/:num'] = $admpath."/atur_kurir_pengiriman";
$route[$admpath.'/atur_kurir_pengiriman/regular/:num'] = $admpath."/atur_kurir_pengiriman";
$route[$admpath.'/atur_metode_bayar/bayar_giro/:num'] = $admpath."/atur_kurir_pengiriman";
$route[$admpath.'/atur_metode_bayar/bayar_cod/:num'] = $admpath."/atur_kurir_pengiriman";
$route[$admpath.'/atur_kurir_pengiriman/update_table/:num'] = $admpath."/atur_kurir_pengiriman";


//$route[$admpath.'/order/update_tabel/proses_pending/:num'] = $admpath."/order/update_table";

/* detail order */
//$route[$admpath.'/order/detail/:any'] = $admpath."/order/detail";
//$route[$admpath.'/order/detail/:any/:num'] = $admpath."/order/detail";

/* log */
//$route[$admpath.'/log_user/:num'] = $admpath."/log_user";
//$route[$admpath.'/log_user/update_table/:num'] = $admpath."/admincms";

/* FITUR ADMIN */
$route[$admpath.'/carimenu'] = $admpath."/fituradmin/carimenu";
$route[$admpath.'/carimenu/:any'] = $admpath."/fituradmin/carimenu";
$route[$admpath.'/carimenu/:any/:num'] = $admpath."/fituradmin/carimenu";
$route[$admpath.'/tblcarimenu/:any'] = $admpath."/fituradmin/tblcarimenu";
$route[$admpath.'/tblcarimenu/:any/:num'] = $admpath."/fituradmin/tblcarimenu";

$route[$admpath.'/carihalaman'] = $admpath."/fituradmin/carihalaman";
$route[$admpath.'/carihalaman/:any'] = $admpath."/fituradmin/carihalaman";
$route[$admpath.'/carihalaman/:any/:num'] = $admpath."/fituradmin/carihalaman";
$route[$admpath.'/tblcarihalaman/:any'] = $admpath."/fituradmin/tblcarihalaman";
$route[$admpath.'/tblcarihalaman/:any/:num'] = $admpath."/fituradmin/tblcarihalaman";

$route[$admpath.'/carialbum'] = $admpath."/fituradmin/carialbum";
$route[$admpath.'/carialbum/:any'] = $admpath."/fituradmin/carialbum";
$route[$admpath.'/carialbum/:any/:num'] = $admpath."/fituradmin/carialbum";
$route[$admpath.'/tblcarialbum/:any'] = $admpath."/fituradmin/tblcarialbum";
$route[$admpath.'/tblcarialbum/:any/:num'] = $admpath."/fituradmin/tblcarialbum";
//Gallery ====================|
$route[$admpath.'/carigallery'] = $admpath."/fituradmin/carigallery";
$route[$admpath.'/carigallery/:any'] = $admpath."/fituradmin/carigallery";
$route[$admpath.'/carigallery/:any/:num'] = $admpath."/fituradmin/carigallery";
$route[$admpath.'/tblcarigallery/:any'] = $admpath."/fituradmin/tblcarigallery";
$route[$admpath.'/tblcarigallery/:any/:num'] = $admpath."/fituradmin/tblcarigallery";

$route[$admpath.'/carivideo'] = $admpath."/fituradmin/carivideo";
$route[$admpath.'/carivideo/:any'] = $admpath."/fituradmin/carivideo";
$route[$admpath.'/carivideo/:any/:num'] = $admpath."/fituradmin/carivideo";
$route[$admpath.'/tblcarivideo/:any'] = $admpath."/fituradmin/tblcarivideo";
$route[$admpath.'/tblcarivideo/:any/:num'] = $admpath."/fituradmin/tblcarivideo";
//Gallery ====================|
$route[$admpath.'/cariplaylist'] = $admpath."/fituradmin/cariplaylist";
$route[$admpath.'/cariplaylist/:any'] = $admpath."/fituradmin/cariplaylist";
$route[$admpath.'/cariplaylist/:any/:num'] = $admpath."/fituradmin/cariplaylist";
$route[$admpath.'/tblcariplaylist/:any'] = $admpath."/fituradmin/tblcariplaylist";
$route[$admpath.'/tblcariplaylist/:any/:num'] = $admpath."/fituradmin/tblcariplaylist";

$route[$admpath.'/cariblog'] = $admpath."/fituradmin/cariblog";
$route[$admpath.'/cariblog/:any'] = $admpath."/fituradmin/cariblog";
$route[$admpath.'/cariblog/:any/:num'] = $admpath."/fituradmin/cariblog";
$route[$admpath.'/tblcariblog/:any'] = $admpath."/fituradmin/tblcariblog";
$route[$admpath.'/tblcariblog/:any/:num'] = $admpath."/fituradmin/tblcariblog";

$route[$admpath.'/cariproduk'] = $admpath."/fituradmin/cariproduk";
$route[$admpath.'/cariproduk/:any'] = $admpath."/fituradmin/cariproduk";
$route[$admpath.'/cariproduk/:any/:num'] = $admpath."/fituradmin/cariproduk";
$route[$admpath.'/tblcariproduk/:any'] = $admpath."/fituradmin/tblcariproduk";
$route[$admpath.'/tblcariproduk/:any/:num'] = $admpath."/fituradmin/tblcariproduk";
//Prod Anak ====================|
$route[$admpath.'/cariprod_anak'] = $admpath."/fituradmin/cariprod_anak";
$route[$admpath.'/cariprod_anak/:any'] = $admpath."/fituradmin/cariprod_anak";
$route[$admpath.'/cariprod_anak/:any/:num'] = $admpath."/fituradmin/cariprod_anak";
$route[$admpath.'/tblcariprod_anak/:any'] = $admpath."/fituradmin/tblcariprod_anak";
$route[$admpath.'/tblcariprod_anak/:any/:num'] = $admpath."/fituradmin/tblcariprod_anak";


/* order-proses */
$route[$admpath.'/cariorder'] = $admpath."/fituradmin/cariorder";
$route[$admpath.'/cariorder/proses_pending/:any'] = $admpath."/fituradmin/cari_proses_pending";
$route[$admpath.'/cariorder/proses_pending/:any/:num'] = $admpath."/fituradmin/cari_proses_pending";
$route[$admpath.'/cariorder/proses_bayar/:any'] = $admpath."/fituradmin/cari_proses_bayar";
$route[$admpath.'/cariorder/proses_bayar/:any/:num'] = $admpath."/fituradmin/cari_proses_bayar";
$route[$admpath.'/cariorder/proses_pemaketan/:any'] = $admpath."/fituradmin/cari_proses_pemaketan";
$route[$admpath.'/cariorder/proses_pemaketan/:any/:num'] = $admpath."/fituradmin/cari_proses_pemaketan";
$route[$admpath.'/cariorder/proses_pengiriman/:any'] = $admpath."/fituradmin/cari_proses_pengiriman";
$route[$admpath.'/cariorder/proses_pengiriman/:any/:num'] = $admpath."/fituradmin/cari_proses_pengiriman";
/* order-status */
$route[$admpath.'/cariorder/sukses/:any'] = $admpath."/fituradmin/cari_sukses";
$route[$admpath.'/cariorder/sukses/:any/:num'] = $admpath."/fituradmin/cari_sukses";
$route[$admpath.'/cariorder/batal/:any'] = $admpath."/fituradmin/cari_batal";
$route[$admpath.'/cariorder/batal/:any/:num'] = $admpath."/fituradmin/cari_batal";
//tblajax
$route[$admpath.'/tblcariorder/:any/:any/:num'] = $admpath."/fituradmin/tblcariorder";

/* pelanggan */
$route[$admpath.'/caripelanggan'] = $admpath."/fituradmin/caripelanggan";
$route[$admpath.'/caripelanggan/:any'] = $admpath."/fituradmin/caripelanggan";
$route[$admpath.'/caripelanggan/:any/:num'] = $admpath."/fituradmin/caripelanggan";
$route[$admpath.'/tblcaripelanggan/:any'] = $admpath."/fituradmin/tblcaripelanggan";
$route[$admpath.'/tblcaripelanggan/:any/:num'] = $admpath."/fituradmin/tblcaripelanggan";


$route[$admpath.'/caribanner'] = $admpath."/fituradmin/caribanner";
$route[$admpath.'/caribanner/iklan_sidebar/:any'] = $admpath."/fituradmin/cari_iklan_sidebar";
$route[$admpath.'/caribanner/iklan_sidebar/:any/:num'] = $admpath."/fituradmin/cari_iklan_sidebar";
$route[$admpath.'/caribanner/iklan_utama/:any'] = $admpath."/fituradmin/cari_iklan_utama";
$route[$admpath.'/caribanner/iklan_utama/:any/:num'] = $admpath."/fituradmin/cari_iklan_utama";
$route[$admpath.'/caribanner/iklan_atas/:any'] = $admpath."/fituradmin/cari_iklan_atas";
$route[$admpath.'/caribanner/iklan_atas/:any/:num'] = $admpath."/fituradmin/cari_iklan_atas";
$route[$admpath.'/caribanner/iklan_bawah/:any'] = $admpath."/fituradmin/cari_iklan_bawah";
$route[$admpath.'/caribanner/iklan_bawah/:any/:num'] = $admpath."/fituradmin/cari_iklan_bawah";
//tblajax
$route[$admpath.'/tblcaribanner/:any/:any/:num'] = $admpath."/fituradmin/tblcaribanner";


$route[$admpath.'/cariwidget'] = $admpath."/fituradmin/cariwidget";
$route[$admpath.'/cariwidget/:any'] = $admpath."/fituradmin/cariwidget";
$route[$admpath.'/cariwidget/:any/:num'] = $admpath."/fituradmin/cariwidget";
$route[$admpath.'/tblcariwidget/:any'] = $admpath."/fituradmin/tblcariwidget";
$route[$admpath.'/tblcariwidget/:any/:num'] = $admpath."/fituradmin/tblcariwidget";

$route[$admpath.'/carimodule'] = $admpath."/fituradmin/carimodule";
$route[$admpath.'/carimodule/:any'] = $admpath."/fituradmin/carimodule";
$route[$admpath.'/carimodule/:any/:num'] = $admpath."/fituradmin/carimodule";
$route[$admpath.'/tblcarimodule/:any'] = $admpath."/fituradmin/tblcarimodule";
$route[$admpath.'/tblcarimodule/:any/:num'] = $admpath."/fituradmin/tblcarimodule";

$route[$admpath.'/cariadmincms'] = $admpath."/fituradmin/cariadmincms";
$route[$admpath.'/cariadmincms/:any'] = $admpath."/fituradmin/cariadmincms";
$route[$admpath.'/cariadmincms/:any/:num'] = $admpath."/fituradmin/cariadmincms";
$route[$admpath.'/tblcariadmincms/:any'] = $admpath."/fituradmin/tblcariadmincms";
$route[$admpath.'/tblcariadmincms/:any/:num'] = $admpath."/fituradmin/tblcariadmincms";


/* log */
$route[$admpath.'/carilog_user'] = $admpath."/fituradmin/carilog_user";
$route[$admpath.'/carilog_user/log_err/:any'] = $admpath."/fituradmin/cari_log_err";
$route[$admpath.'/carilog_user/log_err/:any/:num'] = $admpath."/fituradmin/cari_log_err";
$route[$admpath.'/carilog_user/log_usr/:any'] = $admpath."/fituradmin/cari_log_usr";
$route[$admpath.'/carilog_user/log_usr/:any/:num'] = $admpath."/fituradmin/cari_log_usr";
$route[$admpath.'/carilog_user/log_bot/:any'] = $admpath."/fituradmin/cari_log_bot";
$route[$admpath.'/carilog_user/log_bot/:any/:num'] = $admpath."/fituradmin/cari_log_bot";
//tblajax
$route[$admpath.'/tblcarilog_user/:any/:any/:num'] = $admpath."/fituradmin/tblcarilog_user";


/* metode bayar */
$route[$admpath.'/cari_metodebayar'] = $admpath."/fituradmin/cari_metodebayar";
$route[$admpath.'/cari_metodebayar/bayar_transfer/:any'] = $admpath."/fituradmin/cari_bayar_transfer";
$route[$admpath.'/cari_metodebayar/bayar_transfer/:any/:num'] = $admpath."/fituradmin/cari_bayar_transfer";
$route[$admpath.'/cari_metodebayar/bayar_online/:any'] = $admpath."/fituradmin/cari_bayar_online";
$route[$admpath.'/cari_metodebayar/bayar_online/:any/:num'] = $admpath."/fituradmin/cari_bayar_online";
$route[$admpath.'/cari_metodebayar/bayar_giro/:any'] = $admpath."/fituradmin/cari_bayar_giro";
$route[$admpath.'/cari_metodebayar/bayar_giro/:any/:num'] = $admpath."/fituradmin/cari_bayar_giro";
$route[$admpath.'/cari_metodebayar/bayar_cod/:any'] = $admpath."/fituradmin/cari_bayar_cod";
$route[$admpath.'/cari_metodebayar/bayar_cod/:any/:num'] = $admpath."/fituradmin/cari_bayar_cod";
//tblajax
$route[$admpath.'/tblcari_metodebayar/:any/:any/:num'] = $admpath."/fituradmin/tblcari_metodebayar";

/* kurir */
$route[$admpath.'/carikurir'] = $admpath."/fituradmin/carikurir";
$route[$admpath.'/carikurir/:any'] = $admpath."/fituradmin/carikurir";
$route[$admpath.'/carikurir/:any/:num'] = $admpath."/fituradmin/carikurir";
$route[$admpath.'/tblcarikurir/:any'] = $admpath."/fituradmin/tblcarikurir";
$route[$admpath.'/tblcarikurir/:any/:num'] = $admpath."/fituradmin/tblcarikurir";

/* kategori */
$route[$admpath.'/carikategori'] = $admpath."/fituradmin/carikategori";
$route[$admpath.'/carikategori/:any'] = $admpath."/fituradmin/carikategori";
$route[$admpath.'/carikategori/:any/:num'] = $admpath."/fituradmin/carikategori";
$route[$admpath.'/tblcarikategori/:any'] = $admpath."/fituradmin/tblcarikategori";
$route[$admpath.'/tblcarikategori/:any/:num'] = $admpath."/fituradmin/tblcarikategori";

/***********************************
 * End FITUR ADMIN */


/* ============================================================================
 * Abaikan URL assets 
 * ---------------------------------------------------------------------------*/
$route['PIE.htc'] = "";
$route['assets/:any/:any/(.*)'] = "";

$route['m_assets/:any/:any/(.*)'] = "";

/* End of file routes.php */
/* Location: ./cms/config/routes.php */

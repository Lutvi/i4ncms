<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0664);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0775);

/*CMS*/
define('SPARKPATH', '../aprivate/sparks/');
// Name of the "system folder"
define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

// full url untuk widget or module
if( ! isset( $_SERVER["HTTPS"] ) || $_SERVER['HTTPS'] !== "on")
define('FURL', 'http://'.$_SERVER['HTTP_HOST'].'/'.str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']));
else
define('FURL', 'https://'.$_SERVER['HTTP_HOST'].'/'.str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']));

// rss/bot url to http:// aja
define('RSSURL', 'http://'.$_SERVER['HTTP_HOST'].'/'.str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']));

// cms path
define('CMSPATH', FCPATH.'app/');

define('DS', DIRECTORY_SEPARATOR); // I always use this short form in my code.
define('DIR_CMS_MODE', 0775);
define('FILE_CMS_MODE', 0664);

define('DIR_APP_MODE', 0755);
define('FILE_APP_MODE', 0644);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',			'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',			'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Basis
{
    private $iv = 'rdss0ftwar3s2013';
    private $key = 'rds3n91n3';

    function __construct()
    {
    
    }

    function enkripKode($str='',$hash='') {
      #$key = $this->hex2bin($key);
      $iv = $this->get_ivnya($hash);

      $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);

      mcrypt_generic_init($td, $this->key, $iv);
      $encrypted = mcrypt_generic($td, $str);

      mcrypt_generic_deinit($td);
      mcrypt_module_close($td);

      return bin2hex($encrypted);
    }

    function dekripKode($code='',$hash='') {
      #$key = $this->hex2bin($key);
      $code = $this->hex2bin($code);
      $iv = $this->get_ivnya($hash);

      $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);

      mcrypt_generic_init($td, $this->key, $iv);
      $decrypted = mdecrypt_generic($td, $code);

      mcrypt_generic_deinit($td);
      mcrypt_module_close($td);

      return utf8_encode(trim($decrypted));
    }

    function get_ivnya($hash='')
    {
		if(! empty($hash))
		$iv = $this->iv.$hash;
		else
		$iv = $this->iv;

		$iv = substr($iv, 0,16);

		return $iv;
    }

    protected function hex2bin($hexdata) {
      $bindata = '';

      for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
      }

      return $bindata;
    }

}

function quote($kat) 
{
	return sprintf("'%s'", $kat);
}

/* Un-tested by webmaster
class PhpStringParser
{
    protected $variables;

    public function __construct($variables = array())
    {
        $this->variables = $variables;
    }

    protected function eval_block($matches)
    {
        if( is_array($this->variables) && count($this->variables) )
        {
            foreach($this->variables as $var_name => $var_value)
            {
                $$var_name = $var_value;
            }
        }

        $eval_end = '';

        if( $matches[1] == '<?=' || $matches[1] == '<?php=' )
        {
            if( $matches[2][count($matches[2]-1)] !== ';' )
            {
                $eval_end = ';';
            }
        }

        $return_block = '';

        eval('$return_block = ' . $matches[2] . $eval_end);

        return $return_block;
    }

    public function parse($string)
    {
        return preg_replace_callback('/(\<\?=|\<\?php=|\<\?php)(.*?)\?\>/', array(&$this, 'eval_block'), $string);
    }
}
* */
